package com.mypatparentapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.clevertap.react.CleverTapModule;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.bridge.Arguments;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "mypatparentapp";
    }

    public static class DataActivityDelegate extends ReactActivityDelegate {
        private static final String ENROLLMENT = "enrolmentNo";
        private static final String COURSE_ID = "courseId";
        private static final String TARGET_ID = "targetId";
        private static final String MODE = "mode";

        private Bundle mInitialProps = null;
        private final @Nullable
        Activity mActivity;

        public DataActivityDelegate(Activity activity, String mainComponentName) {
            super(activity, mainComponentName);
            this.mActivity = activity;
        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            // bundle is where we put our alarmID with launchIntent.putExtra
            Bundle bundle = mActivity.getIntent().getExtras();
            if (bundle != null
                    || bundle.containsKey(MODE)
                    || bundle.containsKey(TARGET_ID)
                    || bundle.containsKey(COURSE_ID)
                    || bundle.containsKey(ENROLLMENT)) {
                mInitialProps = new Bundle();
                // put any initialProps here
                mInitialProps.putString(TARGET_ID, bundle.getString(TARGET_ID));
                mInitialProps.putString(COURSE_ID, bundle.getString(COURSE_ID));
                mInitialProps.putString(ENROLLMENT, bundle.getString(ENROLLMENT));
                mInitialProps.putString(MODE, bundle.getString(MODE));
            }
            super.onCreate(savedInstanceState);
            CleverTapModule.setInitialUri(mActivity.getIntent().getData());
            WebView.setWebContentsDebuggingEnabled(true);
        }
        @Override
        protected Bundle getLaunchOptions() {
            return mInitialProps;
        }
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new DataActivityDelegate(this, getMainComponentName());
    }
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        /*
         * This overrides the original intent.
         */
        setIntent(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }
}
