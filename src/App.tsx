import React, {Component} from 'react';
import { Platform, StyleSheet, View, AppState, Linking, PushNotificationIOS } from 'react-native';
import { Provider, observer } from 'mobx-react';
// @ts-ignore
import Navigation from 'react-navigation';
import FCM, { FCMEvent } from 'react-native-fcm';
import SplashScreen from 'react-native-splash-screen';
import {
	SnackBarContainer, ProgressViewContainer, DialogContainer, ModalContainer
} from './container'
import RouterGenerator from './common/Router';
import stores from './store';
import commonStores from './common-library/store';
import { colors } from './config';
import {
	setNavigationItem,
	registerKilledListener,
	registerAppListener,
	createNotification,
	showSnackbar, navigateSimple, logout
} from './services';
import { AuthNetworkModule, NetworkCallbacks } from './api-layer';
import { BaseResponse, BaseRequest } from './http-layer'
import { USER_DETAILS_TYPE, API_IDS } from './common'
import { SplashPage } from './screens';
import { setDeviceToken, getDeviceToken, handleSignIn, setNotifcationData, getAuthToken, setIsDeviceLogin } from './utils';

interface State {
	router?: any,
	stores?: any,
	token?: any,
	tokenCopyFeedback?: any,
	currentAppState?: any,
	realm?: any
	imeiLoggedIn: boolean
	enrolmentNo?: string
	targetId?: string
	courseId?: string
	testTypeDelegate?: string
}

interface Props {
	// @ts-ignore
	navigation?: Navigation
}

@observer
export default class App extends Component<Props, State> {
	constructor(props: any) {
		super(props);

		this.state = {
			router: null,
			token: '',
			tokenCopyFeedback: '',
			currentAppState: AppState.currentState,
			imeiLoggedIn: false,
			enrolmentNo: props.enrolmentNo,
			targetId: props.targetId,
			courseId: props.courseId,
			testTypeDelegate: props.mode
		}
	}

	async componentDidMount() {
		SplashScreen.hide();
		console.disableYellowBox = true;

		if (!this.state.imeiLoggedIn) {
			const authToken = await getAuthToken();
			if (authToken) {
				await this.getUserProfile()
			} else {
				await RouterGenerator((router: any) => {
					return this.updateState(router);
				})
			}
		}
		AppState.addEventListener('change', (state) => {
			// resume
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			// sleep
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});

		if (Platform.OS === 'android') {
			registerAppListener(this.props.navigation)
		}

		// for deep linking thing
		AppState.addEventListener('change', this.handleAppStateChange.bind(this));

		if (Platform.OS === 'ios') {
			PushNotificationIOS.addEventListener('register', async (token) => {
				await setDeviceToken(token)
			})
		}
		// setup fcm
		await this._setupFCM();

	}

	_showLocalNotificationWithAction(notify: any) {
		FCM.presentLocalNotification({
			title: notify.fcm.title,
			body: notify.fcm.body,
			priority: 'high',
			show_in_foreground: true,
			click_action: 'com.mypatparentapp',
			action: notify.action,
			data: notify.data,
			my_custom_data: { action: notify.action, data: notify.data }
		})
	}

	async _FCMPermission() {
		try {
			await FCM.requestPermissions();
		} catch (e) {
			console.error(e)
		}
	}

	async _setupFCM() {
		if (Platform.OS === 'android') {
			registerAppListener(this.props.navigation);

			FCM.getInitialNotification().then(notification => {
				stores.notificationStore.setPushNotificationDataAndAction(notification);
				FCM.removeAllDeliveredNotifications()
			});

			await this._FCMPermission();

			FCM.getFCMToken().then(async token => {
				await setDeviceToken(token);
				this.setState({ token: token || '' })
			});

			FCM.on(FCMEvent.Notification, async notification => {
				if (notification.local_notification) {
					await setNotifcationData(notification.my_custom_data.action);
					stores.notificationStore.setPushNotificationDataAndAction(notification);
					FCM.removeAllDeliveredNotifications();
					return
				}
				if (notification.opened_from_tray) {
					await setNotifcationData(notification.action);
					return
				}
				this._showLocalNotificationWithAction(notification)
			})
		}
	}

	updateState(router: any) {
		this.setState({
			router
		})
	}

	async getUserProfile() {
		const authNetworkModule = new AuthNetworkModule({ methodType: 'GET' }, this);
		await authNetworkModule.getProfile()
	}

	handleDeepLink = (event: any) => {
		// console.warn(`Got event, url: ${event}`)
	};

	handleAppStateChange = (currentAppState: any) => {
		this.setState({ currentAppState });
		Linking.getInitialURL().then(url => this.handleDeepLink({ url }))
	};

	componentWillUnmount() {
		AppState.removeEventListener('change', this.handleAppStateChange);
		PushNotificationIOS.removeEventListener('register', (token: any) => {
			// Want to remove token write code here
		})
	}

  render() {
		const { router: Router } = this.state;
    return (
			<Provider {...stores} {...commonStores}>
				{Router ? <View style={styles.container}>
					<Router
						ref={(ref: any) => setNavigationItem(ref)}
					/>
					<SnackBarContainer />
					<ProgressViewContainer />
					<DialogContainer />
					<ModalContainer />
				</View> : <SplashPage />}
			</Provider>
    );
  }

	async onSuccess(apiId: any, response: any) {
		switch (apiId) {
			case API_IDS.GET_PROFILE:
				if(response.data && response.data.profile) {
					stores.profileDataStore.setUserProfileDetails(response.data.profile);
					await handleSignIn(response, USER_DETAILS_TYPE.DEFAULT);
				} else {
					showSnackbar('Session Expired');
				}
				break;
		}
		await RouterGenerator((router: any) => this.updateState(router))
	}

	onSuccessUnAuthorized(apiId: any, response: any) {
		// TODO
	}

	onSuccessBadRequest(apiId: any, response: any) {
		// TODOD
	}

	async onFailure(apiId: string, request: BaseRequest) {
		await RouterGenerator((router: any) => this.updateState(router))
	}

	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: BaseResponse): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		//
	}

	onComplete() {
		//
	}
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.White }
});
