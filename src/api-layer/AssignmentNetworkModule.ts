import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class AssignmentNetworkModule extends NetworkApiWrapper {
	request = this.getRequest();
	context: any;
	constructor(requestParam, currentContext) {
		super(requestParam, currentContext);
		this.context = currentContext
	}

	async getActiveAssignments(options, prefetch) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_ACTIVE_ASSIGNMENTS,
			'apiEndpoint': API_END_POINTS.GET_ACTIVE_ASSIGNMENTS,
			'prefetch': prefetch,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async getPastAssignments(options, prefetch) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_PAST_ASSIGNMENTS,
			'apiEndpoint': API_END_POINTS.GET_PAST_ASSIGNMENTS,
			'prefetch': prefetch,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async getAllTests(options, prefetch) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_ALL_TESTS_ASSIGNMENTS,
			'apiEndpoint': API_END_POINTS.GET_ALL_TESTS_ASSIGNMENTS,
			'prefetch': prefetch,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async getAllAttempts(options) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_ALL_ATTEMPTS,
			'apiEndpoint': API_END_POINTS.GET_ALL_ATTEMPTS,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async getCourseSubjects(urlParams: string) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_COURSE_SUBJECTS, 'apiEndpoint': API_END_POINTS.GET_COURSE_SUBJECTS,
			'urlParams': urlParams
		});
		await this.requestAPIResponse()
	}

}
