import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class AuthNetworkModule extends NetworkApiWrapper {
	request = this.getRequest();
	context: any;

	constructor(requestParam: any, currentContext: any) {
		super(requestParam, currentContext);
		this.context = currentContext
	}

	async authUserSignup(urlParams: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.SIGN_UP, 'apiEndpoint': API_END_POINTS.LOGIN, 'urlParams': urlParams });
		await this.requestAPIResponse()
	}

	async authUserLogin(urlParams: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.LOGIN, 'apiEndpoint': API_END_POINTS.LOGIN, 'urlParams': urlParams });
		await this.requestAPIResponse();
	}

	async verifyOTP(urlParams: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.VERIFY_OTP, 'apiEndpoint': API_END_POINTS.VERIFY_OTP, 'urlParams': urlParams });
		await this.requestAPIResponse();
	}

	async authGoogleLogin(accessToken: string, urlParams: string) {
		await this.setCustomConfig({
			'apiId': API_IDS.SOCIAL_LOGIN,
			'apiEndpoint': API_END_POINTS.GOOGLE_LOGIN.replace('{accessToken}', accessToken),
			'urlParams': urlParams
		});
		await this.requestAPIResponse()
	}

	async authFacebookLogin(accessToken: string, urlParams: string) {
		await this.setCustomConfig({
			'apiId': API_IDS.SOCIAL_LOGIN,
			'apiEndpoint': API_END_POINTS.FACEBOOK_LOGIN.replace('{accessToken}', accessToken),
			'urlParams': urlParams
		});
		await this.requestAPIResponse()
	}

	async getProfile() {
		await this.setCustomConfig({ 'apiId': API_IDS.GET_PROFILE, 'apiEndpoint': API_END_POINTS.PROFILE });
		await this.requestAPIResponse()
	}
}
