import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

const indexCardObj = {
	'achievement': {
	  'apiId': API_IDS.ACHIEVEMENT_INDEX,
	  'apiEndpoint': API_END_POINTS.ACHIEVEMENT_INDEX
	},
	'sincerity': {
	  'apiId': API_IDS.SINCERITY_INDEX,
	  'apiEndpoint': API_END_POINTS.SINCERITY_INDEX
	},
	'improvement': {
	  'apiId': API_IDS.IMPROVEMENT_INDEX,
	  'apiEndpoint': API_END_POINTS.IMPROVEMENT_INDEX
	}
  }

export class DashboardNetworkModule extends NetworkApiWrapper {
  request = this.getRequest();
  context: any;
  constructor(requestParam: any, currentContext: any) {
    super(requestParam, currentContext);
    this.context = currentContext
  }

  async getGoalProgressData(data: any, prefetch: boolean) {
    await this.setCustomConfig({
      'apiId': API_IDS.GOAL_PROGRESS,
      'apiEndpoint': API_END_POINTS.GOAL_PROGRESS.replace('{studentId}', data.studentId),
      'urlParams': data,
      'prefetch': true,
      'cachify': { expiry: '10m' }
     });
    await this.requestAPIResponse()
  }

  async getUpcomingExamData(data: string, prefetch: Boolean) {
    await this.setCustomConfig({
      'apiId': API_IDS.UPCOMING_EXAMS,
      'apiEndpoint': API_END_POINTS.UPCOMING_EXAMS,
      'urlParams': data,
      'prefetch': true
    });
    await this.requestAPIResponse()
  }

	async getNewGoalsData(studentId: string) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_NEW_GOALS,
			'apiEndpoint': API_END_POINTS.GET_NEW_GOALS.replace('{studentId}', studentId),
			'prefetch': true
		});
		await this.requestAPIResponse();
	}

	async fetchStudentSummary(studentId: string) {
		let url = API_END_POINTS.STUDENT_SUMMARY.replace('{studentId}', studentId);
		await this.setCustomConfig({
			'apiId': API_IDS.STUDENT_SUMMARY,
			'apiEndpoint': url,
			'prefetch': true
		});
		await this.requestAPIResponse();
	}

	async fetchActivityCardData(studentId: string, goalId: string) {
		try {
			let url = API_END_POINTS.ACTIVITY_CARD_DATA.replace('{studentId}', studentId).replace('{goalId}', goalId);
			await this.setCustomConfig({
				'apiId': API_IDS.ACTIVITY_CARD_DATA,
				'apiEndpoint': url,
				'prefetch': true
			});
			await this.requestAPIResponse();
		} catch (e) {
			console.log(e);
		}
	}

	async fetchSyllabusCardData(studentId: string, goalId: string) {
		let url = API_END_POINTS.SYLLABUS_CARD_DATA.replace('{studentId}', studentId).replace('{goalId}', goalId);
		await this.setCustomConfig({
			'apiId': API_IDS.SYLLABUS_CARD_DATA,
			'apiEndpoint': url,
			'prefetch': true
		});
		await this.requestAPIResponse();
	}

	async fetchAssignmentData(studentId: string, goalId: string, startIndex: number, limit: number) {
		const url = API_END_POINTS.ASSIGNMENT_DATA.replace('{studentId}', studentId).replace('{goalId}', goalId);
		await this.setCustomConfig({
			'apiId': API_IDS.ASSIGNMENT_DATA,
			'apiEndpoint': url,
			'urlParams': JSON.stringify({
				startIndex: startIndex,
				limit: limit
			}),
			'prefetch': true
		});
		await this.requestAPIResponse();
	}

	async fetchUpcomingTests(studentId: string) {
		let url = API_END_POINTS.UPCOMING_TESTS.replace('{studentId}', studentId);
		await this.setCustomConfig({
			'apiId': API_IDS.UPCOMING_TESTS,
			'apiEndpoint': url,
			'prefetch': true
		});
		await this.requestAPIResponse();
	}

	async fetchIndexCardData(studentId: string, indexId: string) {
		const indexObj = { ...indexCardObj[indexId] }
		await this.setCustomConfig({
		  'apiId': indexObj.apiId,
		  'apiEndpoint': indexObj.apiEndpoint,
		  'cachify': {
			expiry: '1m',
			primaryKey: {
			  params: ['studentId']
			},
			isArrayResponse: false
		  },
		  'params': JSON.stringify({
			studentId: studentId
		  }),
		  'prefetch': true
		})
		await this.requestAPIResponse()
	  }
}
