import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class FeedNetworkModule extends NetworkApiWrapper {
	request = this.getRequest();
	context: any;
	constructor(requestParam: any, currentContext: any) {
		super(requestParam, currentContext);
		this.context = currentContext
	}

	async getFeedList(prefech: boolean, startIndex: number, limit: number) {
		await this.setCustomConfig({
			'apiId': API_IDS.GET_FEED_LIST,
			'apiEndpoint': API_END_POINTS.GET_FEED_LIST,
			'urlParams': JSON.stringify({
				startIndex: startIndex,
				limit: limit
      		}),
			'prefetch': prefech
		});
		await this.requestAPIResponse()
	}

	async getFeedDetail(feedId: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.GET_FEED_DETAIL,
			'apiEndpoint': API_END_POINTS.GET_FEED_DETAIL.replace('{feedId}', feedId),
			'urlParams': ''
		});
		await this.requestAPIResponse()
	}

	async updateFeedLike(feedId: string) {
		await this.setCustomConfig({
			'apiId': API_IDS.POST_FEED_LIKE,
			'apiEndpoint': API_END_POINTS.POST_FEED_LIKE.replace('{feedId}', feedId),
			'urlParams': JSON.stringify({}),
			'prefetch': true
		});
		await this.requestAPIResponse()
	}
}
