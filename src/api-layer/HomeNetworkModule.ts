import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class HomeNetworkModule extends NetworkApiWrapper {
  request = this.getRequest()
  context: any;
  constructor(requestParam: any, currentContext: any) {
    super(requestParam, currentContext);
    this.context = currentContext
  }

  async getUserGoals(prefetch: boolean, studentId: any) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_USER_GOALS,
      'apiEndpoint': API_END_POINTS.GET_NEW_GOALS.replace('{studentId}', studentId),
      'prefetch': prefetch
      // 'cachify': { expiry: '1m' }
    });
    await this.requestAPIResponse()
  }

  async getFreeTestResult(urlParams: string, prefetch: boolean) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_FREETEST_RESULT,
      'apiEndpoint': API_END_POINTS.GET_FREETEST_RESULT,
      'urlParams': urlParams,
      'prefetch': prefetch,
      'cachify': { expiry: '5m' }
    });
    await this.requestAPIResponse()
  }

}
