import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class NotificationNetworkModule extends NetworkApiWrapper {
  request = this.getRequest();
  context: any;
  constructor(requestParam: any, currentContext: any) {
    super(requestParam, currentContext);
    this.context = currentContext
  }

  async getNotificationList(prefech: boolean, startIndex: number, limit: number) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_NOTIFICATION_LIST,
      'apiEndpoint': API_END_POINTS.GET_NOTIFICATION_LIST,
      'urlParams': JSON.stringify({
				startIndex: startIndex,
				limit: limit
      }),
      'prefetch' : prefech
    });
    await this.requestAPIResponse()
  }
  async getNotificationCount(prefech: boolean) {
    await this.setCustomConfig({ 'apiId': API_IDS.GET_NOTIFICATION_COUNT,
     'apiEndpoint': API_END_POINTS.GET_NOTIFICATION_COUNT,
      'urlParams': '', 'prefetch' : prefech });
    await this.requestAPIResponse()
  }

  async setNotificationStatus(urlParams: string, prefech: boolean) {
    await this.setCustomConfig({ 'apiId': API_IDS.SET_NOTIFICATION_STATUS,
     'apiEndpoint': API_END_POINTS.SET_NOTIFICATION_STATUS,
      'urlParams': urlParams, 'prefetch' : prefech });
    await this.requestAPIResponse()
  }

  async setGlobalNotificationStatus() {
    await this.setCustomConfig({ 'apiId': API_IDS.SET_GLOBAL_NOTIFICATION_STATUS,
     'apiEndpoint': API_END_POINTS.SET_GLOBAL_NOTIFICATION_STATUS, 'prefetch' : true });
    await this.requestAPIResponse()
  }
}
