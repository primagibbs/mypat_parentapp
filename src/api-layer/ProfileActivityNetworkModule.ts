import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class ProfileActivityNetworkModule extends NetworkApiWrapper {

  request = this.getRequest()
  context: any
  constructor(requestParam, currentContext) {
    super(requestParam, currentContext)
    this.context = currentContext
  }

  async getActivity(urlParams) {
    await this.setCustomConfig({ 'apiId': API_IDS.GET_ACTIVITY, 'apiEndpoint': API_END_POINTS.GET_ACTIVITY,
    'urlParams': urlParams })
    await this.requestAPIResponse()
  }

  async uploadImage(urlParams: any) {
    await this.setCustomConfig({ 'apiId': API_IDS.UPLOAD_IMAGE, 'apiEndpoint': API_END_POINTS.UPLOAD_IMAGE, 'urlParams': urlParams });
    await this.requestAPIResponse()
  }

  async removeImage(urlParams) {
    await this.setCustomConfig({ 'apiId': API_IDS.REMOVE_IMAGE, 'apiEndpoint': API_END_POINTS.REMOVE_IMAGE, 'urlParams': urlParams })
    await this.requestAPIResponse()
  }

  async getProfile(urlParams) {
    await this.setCustomConfig({ 'apiId': API_IDS.GET_PROFILE,
    'apiEndpoint': API_END_POINTS.PROFILE,
    'urlParams': urlParams,
    'cachify': { expiry: '1m' , isArrayResponse: true}
  })
    await this.requestAPIResponse()
  }
  async getLockActivity(urlParams) {
    await this.setCustomConfig({ 'apiId': API_IDS.ACTIVITY_LOCK, 'apiEndpoint': API_END_POINTS.ACTIVITY_LOCK, 'urlParams': urlParams })
    await this.requestAPIResponse()
  }

  async getUnLockActivity(urlParams) {
    await this.setCustomConfig({ 'apiId': API_IDS.ACTIVITY_UNLOCK, 'apiEndpoint': API_END_POINTS.ACTIVITY_UNLOCK, 'urlParams': urlParams })
    await this.requestAPIResponse()
  }

  async getMyProfileDetails(userId) {
    await this.setCustomConfig({
      'apiId': API_IDS.MY_PROFILE,
      'apiEndpoint': API_END_POINTS.MY_PROFILE.replace('{userId}', userId)
    });
    await this.requestAPIResponse();
  }
}
