import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class ProfileNetworkModule extends NetworkApiWrapper {
	request = this.getRequest();
	context: any;
	constructor(requestParam: any, currentContext: any) {
		super(requestParam, currentContext);
		this.context = currentContext
	}

	async getGoals(options: any) {
		// tslint:disable-next-line:max-line-length
		await this.setCustomConfig({ 'apiId': API_IDS.GET_GOALS,
			'apiEndpoint': API_END_POINTS.GET_GOALS,
			urlParams: JSON.stringify(options.urlParams)
			// 'cachify': { expiry: '5m' }
		});
		await this.requestAPIResponse()
	}

	async setEmail(postData: any) {
		// tslint:disable-next-line:max-line-length
		await this.setCustomConfig({
			apiId: API_IDS.SET_EMAIL, apiEndpoint: API_END_POINTS.PROFILE, urlParams: postData
		});
		await this.requestAPIResponse()
	}

	async setDetails(postData: any) {
		// tslint:disable-next-line:max-line-length
		await this.setCustomConfig({
			apiId: API_IDS.UPDATE_PROFILE, apiEndpoint: API_END_POINTS.PROFILE, urlParams: postData
		});
		await this.requestAPIResponse()
	}

	async setName(postData: any) {
		// tslint:disable-next-line:max-line-length
		await this.setCustomConfig({
			apiId: API_IDS.SET_PROFILE, apiEndpoint: API_END_POINTS.PROFILE, urlParams: postData
		});
		await this.requestAPIResponse()
	}

	async setGoalsDetails(postData: any) {
		// tslint:disable-next-line:max-line-length
		await this.setCustomConfig({
			'apiId': API_IDS.UPDATE_OTHER_DETAILS,
			'apiEndpoint': API_END_POINTS.UPDATE_OTHER_DETAILS, 'urlParams': postData
		});
		await this.requestAPIResponse()
	}

	async updateEmail(data: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.UPDATE_EMAIl, 'apiEndpoint': API_END_POINTS.UPDATE_EMAIL, 'urlParams': data });
		await this.requestAPIResponse()
	}

	async updateMobile(data: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.UPDATE_MOBILE, 'apiEndpoint': API_END_POINTS.UPDATE_MOBILE, 'urlParams': data });
		await this.requestAPIResponse()
	}

	async verifyPartner(postBody: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.VERIFY_PARTNER, 'apiEndpoint': API_END_POINTS.VERIFY_PARTNER, 'urlParams': postBody });
		await this.requestAPIResponse()
	}

	async getStudent(options: any) {
		await this.setCustomConfig({
			'apiId': API_IDS.STUDENT_PROFILE,
			'apiEndpoint': API_END_POINTS.STUDENT_PROFILE,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async validateStudent(options: any) {
		await this.setCustomConfig({
			'apiId': API_IDS.VALIDATE_PROFILE,
			'apiEndpoint': API_END_POINTS.VALIDATE_PROFILE.replace('{studentInfo}', options.studentInfo)
		});
		await this.requestAPIResponse()
	}

	async getStudentByOTP(options: any) {
		// console.log(options.urlParams);
		await this.setCustomConfig({
			'apiId': API_IDS.STUDENT_PROFILE,
			'apiEndpoint': API_END_POINTS.STUDENT_PROFILE_OTP,
			urlParams: JSON.stringify(options.urlParams)
		});
		await this.requestAPIResponse()
	}

	async logout(urlParams: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.LOGOUT, 'apiEndpoint': API_END_POINTS.LOGOUT, 'urlParams': urlParams });
		await this.requestAPIResponse()
	}

	async addStudent(postBody: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.ADD_STUDENT, 'apiEndpoint': API_END_POINTS.STUDENT_PROFILE, 'urlParams': postBody });
		await this.requestAPIResponse()
	}

	async deleteStudent(studentId: string) {
		await this.setCustomConfig({ 'apiId': API_IDS.DELETE_STUDENT, 'apiEndpoint': API_END_POINTS.DELETE_STUDENT.replace('{studentId}', studentId)});
		await this.requestAPIResponse()
	}

	async sendStudentOTP(postBody: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.SEND_STUDENT_OTP, 'apiEndpoint': API_END_POINTS.SEND_STUDENT_OTP, 'urlParams': postBody });
		await this.requestAPIResponse()
	}

	async sendOTPForMobileChange(postBody: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.SEND_PROFILE_OTP, 'apiEndpoint': API_END_POINTS.SEND_PROFILE_OTP, 'urlParams': postBody });
		await this.requestAPIResponse()
	}

	async verifyOTPForMobileChange(postBody: any) {
		await this.setCustomConfig({ 'apiId': API_IDS.VERIFY_PROFILE_OTP, 'apiEndpoint': API_END_POINTS.VERIFY_PROFILE_OTP, 'urlParams': postBody });
		await this.requestAPIResponse()
	}
}
