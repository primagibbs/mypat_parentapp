import { NetworkApiWrapper } from '../common-library/api-layer'
import { API_IDS, API_END_POINTS } from '../common'

export class TestNetworkModule extends NetworkApiWrapper {
  request = this.getRequest()
  context: any
  constructor(requestParam, currentContext) {
    super(requestParam, currentContext)
    this.context = currentContext
  }

  async getMyTests(options, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_MY_TESTS,
      'apiEndpoint': API_END_POINTS.GET_MY_TESTS,
      'prefetch': prefetch,
      urlParams: JSON.stringify(options.urlParams)
    })
    await this.requestAPIResponse()
  }

  async getActiveAssignments(options, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_ACTIVE_ASSIGNMENTS,
      'apiEndpoint': API_END_POINTS.GET_ACTIVE_ASSIGNMENTS,
      'prefetch': prefetch,
      urlParams: JSON.stringify(options.urlParams)
    })
    await this.requestAPIResponse()
  }

  async getTestResult(options: any, studentId: any) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_TEST_RESULT,
      'apiEndpoint': API_END_POINTS.GET_TEST_RESULT.replace('{studentId}', studentId),
      urlParams: JSON.stringify(options.urlParams)
    });
    await this.requestAPIResponse()
  }
  async postSolutionRating(options) {
    await this.setCustomConfig({
      'apiId': API_IDS.SAVE_RATING,
      'apiEndpoint': API_END_POINTS.SAVE_RATING,
      urlParams: JSON.stringify(options.urlParams)
    })
    await this.requestAPIResponse()
  }
  async getCourseSubjects(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_COURSE_SUBJECTS, 'apiEndpoint': API_END_POINTS.GET_COURSE_SUBJECTS,
      'urlParams': urlParams,
      'prefetch': prefetch

    })
    await this.requestAPIResponse()
  }

  async getTrialTests(options) {
    // console.warn('free test params:', options)
    await this.setCustomConfig({
      'apiId': API_IDS.GET_TRIAL_TESTS,
      'apiEndpoint': API_END_POINTS.GET_TRIAL_TESTS,
      'prefetch': options.prefetch,
      urlParams: JSON.stringify(options.urlParams)
    })
    await this.requestAPIResponse()
  }

  async getTestAssignment(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_TEST_ASSIGNMENT, 'apiEndpoint': API_END_POINTS.GET_TEST_ASSIGNMENT,
      'urlParams': urlParams,
      'prefetch': prefetch,
    })
    await this.requestAPIResponse()
  }
  async getChapterTestAssignment(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_CHAPTER_TEST_ASSIGNMENT, 'apiEndpoint': API_END_POINTS.GET_CHAPTER_TEST_ASSIGNMENT,
      'urlParams': urlParams,
      'prefetch': prefetch

    })
    await this.requestAPIResponse()
  }
  async serachTestAssignment(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.SEARCH_TEST_ASSIGNMENT, 'apiEndpoint': API_END_POINTS.SEARCH_TEST_ASSIGNMENT,
      'urlParams': urlParams,
      'prefetch': prefetch
    })
    await this.requestAPIResponse()
  }

  async answerQuestion(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.ANSWER_QUESTION,
      'apiEndpoint': API_END_POINTS.ANSWER_QUESTION,
      'urlParams': urlParams,
      'prefetch': prefetch
    })
    await this.requestAPIResponse()
  }

  async finishTest(urlParams: string) {
    await this.setCustomConfig({
      'apiId': API_IDS.FINISH_TEST,
      'apiEndpoint': API_END_POINTS.FINISH_TEST,
      'urlParams': urlParams,
      cachify: {
        expiry: '1d',
        primaryKey: {
          // urlParams: ['testId', 'assignmentId']
        },
        isArrayResponse: false
      }
    })
    await this.requestAPIResponse()
  }
  async getSubjectFiltersList(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_SUBJECT_FILTERS,
      'apiEndpoint': API_END_POINTS.GET_SUBJECT_FILTERS,
      'urlParams': urlParams,
      'prefetch': prefetch
    });
    await this.requestAPIResponse()
  }
  async getWeaknessList(urlParams: string, studentId: any, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_WEAKNESS_LIST,
      'apiEndpoint': API_END_POINTS.GET_WEAKNESS_LIST.replace('{studentId}', studentId),
      'urlParams': urlParams,
      'prefetch': prefetch
    });
    await this.requestAPIResponse()
  }

  async getTestSolution(urlParams: string, prefetch) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_TEST_SOLUTION,
      'apiEndpoint': API_END_POINTS.GET_TEST_SOLUTION,
      'urlParams': urlParams,
      'prefetch': prefetch
    });
    await this.requestAPIResponse()
  }

  async getTestSolutionComparison(urlParams: string, studentId: any, prefetch: any) {
    await this.setCustomConfig({
      'apiId': API_IDS.GET_COMPARISON,
      'apiEndpoint': API_END_POINTS.GET_COMPARISON.replace('{studentId}', studentId),
      'urlParams': urlParams,
      'prefetch': prefetch,
      'sanitizeNumerals': false
    });
    await this.requestAPIResponse()
  }

  async questions(urlParams: string, prefetch) {
    // console.warn('urlParams', urlParams)
    await this.setCustomConfig({
      'apiId': API_IDS.QUESTION,
      'apiEndpoint': API_END_POINTS.QUESTION,
      'urlParams': urlParams,
      'prefetch': prefetch
    })
    await this.requestAPIResponse()
  }
}
