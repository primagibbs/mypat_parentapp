import { getAuthToken, getBaseUrl, isSignedIn, getExtraEdgeBaseUrl } from '../utils';
import { logout } from '../services';
import { networkConfig } from '../common-library/configuration';
import { PROJECT_TYPE } from '../common';

networkConfig
.setInitialRoute(() => 'LoginPage')
.setAuthToken(getAuthToken)
.setBaseURL(getBaseUrl)
.setExtraEdgeBaseURL(getExtraEdgeBaseUrl)
.setIsLoggedIn(isSignedIn)
.setLogoutHandler(() => logout)
.setProject(() => PROJECT_TYPE.MYPATPARENT)
.setNetworkConfig(() => ({
  numeralConfig: {
    sanitizeNumerals: true,
    decimalPlaces: 2
  }
}));

export * from './AuthNetworkModule'
export * from './HomeNetworkModule'
export * from './ProfileNetworkModule'
export * from './TestNetworkModule'
export * from './AssignmentNetworkModule'
export * from './ProfileActivityNetworkModule'
export * from './DashboardNetworkModule'
export * from './NotificationNetworkModule'
export * from '../common-library/api-layer'
export * from './FeedNetworkModule'
