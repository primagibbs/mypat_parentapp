const USER_INFO_TYPE = {
  EMAIL: 'email',
  MOBILE: 'mobile',
  NONE: 'none'
};

const USER_DETAILS_TYPE = {
  SIGNUP: 'signup',
  LOGIN: 'login',
  SOCIAL_LOGIN: 'social',
  DEFAULT: 'default'
};

const USER_LOGIN_TYPE = {
  SOCIAL: 'social',
  DEFAULT: 'default'
};

const COUNTRY_CODE: any = ['IN', 'AE', 'SA', 'NP', 'MY', 'MM', 'LK', 'KW', 'OM', 'SG', 'BH', 'QA', 'ZA'];

const NOTIFICATION_TYPE_IMAGES_SMALL: Map<string, any> = new Map([

]);

const NOTIFICATION_TYPE_IMAGES: Map<string, any> = new Map([

]);

const NOTIFICATION_TYPE = {
  PRODUCT_PURCHASE: 'purchaseCongratulations',
  TEST_LIVE: '',
  TEST_PREP: '',
  USER_WELCOME: 'welcome',
  QUESTION_ASK: '',
  ANSWER_RECEIVED: 'community-answer',
  UPVOTE_QUESTION: 'community-question-vote',
  DOWNVOTE_QUESTION: '',
  ANSWER_VOTE: 'community-answer-vote',
  RECEVIVED_ANSWER_ON_FOLLOWED: '',
  COMMENTS_ON_ANSWER: '',
  USER_COMMENT_ON_ANSWER: 'community-answer',
  COMMUNITY: '',
  COMPLETE_PROFILE: 'complete-profile',
  CHALLENGE: 'challenger-notification',
  ASSIGNMENT_ASSIGNED: 'assignmentAssigned'
};

const CONGRATS_PAGE_TYPE = {
  SIGNUP_MOBILE: 'signup_mobile',
  SIGNUP_EMAIL: 'signup_email',
  GOALS_PAGE: 'goals_page',
  FORGET_EMAIL: 'forget_email',
  DEFAULT: 'default'
};

const DIALOG_DESCRIPTION = {
  RESET_PASSWORD_DESCRITION: 'Your password has been reset. Please login with new credentials',
  FORGET_PASSWORD_EMAIL_DESCRITION: 'Please check your email for reset link ',
  PRODUCT_PURCHASE_DESCRITION: 'Thank you for your purchase',
  CLASS_CHANGE_DESCRIPTION: 'Changing the class will change user goals. Do you wish to continue?',
  EMAIL_VERIFICATION_DESCRIPTION: 'Email verification link sent sucessfully'
};

const VERIFICATION_PAGE_TYPE = {
  LOGIN: 'login',
  GOALS_PAGE: 'signup'
};

const SOCIAL_LOGIN_MODE = {
  GOOGLE: 'google',
  FACEBOOK: 'facebook'
};
const TAB_BAR_PAGE = {
  EXPLORE: 0,
  TEST: 1,
  COMMUNITY: 2,
  MORE: 3
};
const FROM_PAGE_TYPE = {
  FORGOT_PASSWORD_PAGE_EMAIL: 1,
  FORGOT_PASSWORD_PAGE_MOBILE: 2,
  MOBILE_VERIFICATION_PAGE: 3,
  PENDING_VERIFICATION_PAGE: 4,
  GOALS_PAGE: 5,
  SIGN_UP_PAGE: 6,
  LOGIN_PAGE: 7,
  RESET_PASSWORD_PAGE: 8,
  CHECKOUT_PAGE: 9,
  EDIT_PROFILE_PAGE: 10,
  DEFAULT: 11,
  HELP_AND_SUPPORT_MOBILE_VERIFICATION: 12
};

const TYPE = {
  MAIN: 'mainTest',
  CONCEPT: 'conceptTest',
  CHAP: 'chapTest',
  ASSIGNMENT: 'assignment'
};

const TEST_TYPE = {
  MAIN_TEST: 'Main Test',
  CONCEPT_TEST: 'Concept Test',
  CHAP_TEST: 'Chap Test',
  SAMPLE_TEST: 'Sample Test'
};

const SUBJECT_TYPE = {
  MATH: 'mathematics',
  CHEM: 'chemistry',
  PHY: 'physics',
  ALL: 'all',
  BIOLOGY: 'BIOLOGY'
};

const API_IDS = {
  GET_ALL_ATTEMPTS: 'GET_ALL_ATTEMPTS',
  GET_COURSE_SUBJECTS: 'GET_COURSE_SUBJECTS',
  GET_ACTIVE_ASSIGNMENTS: 'GET_ACTIVE_ASSIGNMENTS',
  GET_PAST_ASSIGNMENTS: 'GET_PAST_ASSIGNMENTS',
  GET_ALL_TESTS_ASSIGNMENTS: 'GET_ALLTESTS_ASSIGNMENTS',
  FIITJEE_RESEND_OTP: 'FIITJEE_RESEND_OTP',
  GET_MY_TESTS: 'GET_MY_TESTS',
  REMOVE_IMAGE: 'REMOVE_IMAGE',
  GET_TEST_RESULT: 'GET_TEST_RESULT',
  SAVE_RATING: 'SAVE_RATING',
  GET_TRIAL_TESTS: 'GET_TRIAL_TESTS',
  UPDATE_EMAIl: 'UPDATE_EMAIl',
  UPDATE_MOBILE: 'UPDATE_MOBILE',
  SIGN_UP: 'SIGN_UP',
  UPLOAD_IMAGE: 'UPLOAD_IMAGE',
  LOGIN: 'LOGIN',
  VERIFY_OTP: 'VERIFY_OTP',
  SEND_OTP_MOBILE: 'SEND_OTP_MOBILE',
  GET_GOALS: 'GET_GOALS',
  GET_GOALS_TYPE: 'GET_GOALS_TYPE',
  GET_CLASSES: 'GET_CLASSES',
  UPDATE_OTHER_DETAILS: 'UPDATE_OTHER_DETAILS',
  SEND_EMAIL_VERIFY: 'SEND_EMAIL_VERIFY',
  RESEND_LINK: 'RESEND_LINK',
  GET_USER_GOALS: 'GET_USER_GOALS',
  CAPTURE_PAYMENT: 'CAPTURE_PAYMENT',
  GET_FREETEST_RESULT: 'GET_FREETEST_RESULT',
  SOCIAL_LOGIN: 'SOCIAL_LOGIN',
  GET_PROFILE: 'GET_PROFILE',
  SET_PROFILE: 'SET_PROFILE',
  SET_EMAIL: 'SET_EMAIL',
	UPDATE_PROFILE: 'UPDATE_PROFILE',
  GET_ACTIVITY: 'ACTIVITY',
  ACTIVITY_LOCK: 'ACTIVITY_LOCK',
  ACTIVITY_UNLOCK: 'ACTIVITY_UNLOCK',
  VERIFY_PARTNER: 'VERIFY_PARTNER',
  UPCOMING_EXAMS: 'UPCOMING_EXAMS',
  GOAL_PROGRESS: 'GOAL_PROGRESS',
  GET_NOTIFICATION_LIST: 'GET_NOTIFICATION_LIST',
  GET_NOTIFICATION_COUNT: 'GET_NOTIFICATION_COUNT',
  SET_NOTIFICATION_STATUS: 'SET_NOTIFICATION_STATUS',
  SET_GLOBAL_NOTIFICATION_STATUS: 'SET_GLOBAL_NOTIFICATION_STATUS',
  GET_LOCATION_ADDRESS: 'GET_LOCATION_ADDRESS',
  GET_TEST_ASSIGNMENT: 'GET_TEST_ASSIGNEMET',
  GET_CHAPTER_TEST_ASSIGNMENT: 'GET_CHAPTER_TEST_ASSIGNMENT',
  SEARCH_TEST_ASSIGNMENT: 'SEARCH_TEST_ASSIGNMENT',
  ANSWER_QUESTION: 'ANSWER_QUESTION',
  DOWNLOADED_TEST: 'DOWNLOADED_TEST',
  FINISH_TEST: 'FINISH_TEST',
  GET_SUBJECT_FILTERS: 'GET_SUBJECT_FILTERS',
	GET_WEAKNESS_LIST: 'GET_WEAKNESS_LIST',
	GET_TEST_SOLUTION: 'GET_TEST_SOLUTION',
	GET_COMPARISON: 'GET_COMPARISON',
  QUESTION: 'QUESTION',
	STUDENT_PROFILE: 'STUDENT_PROFILE',
	VALIDATE_PROFILE: 'VALIDATE_PROFILE',
	ADD_STUDENT: 'ADD_STUDENT',
	LOGOUT: 'LOGOUT',
	DELETE_STUDENT: 'DELETE_STUDENT',
	SEND_STUDENT_OTP: 'SEND_STUDENT_OTP',
	GET_FEED_LIST: 'GET_FEED_LIST',
	GET_FEED_DETAIL: 'GET_FEED_DETAIL',
	POST_FEED_LIKE: 'POST_FEED_LIKE',
	SEND_PROFILE_OTP: 'SEND_PROFILE_OTP',
	VERIFY_PROFILE_OTP: 'VERIFY_PROFILE_OTP',

  /** New dashboard API_Ids */
  GET_NEW_GOALS: 'GET_NEW_GOALS',
  STUDENT_SUMMARY: 'STUDENT_SUMMARY',
  ACTIVITY_CARD_DATA: 'ACTIVITY_CARD_DATA',
  SYLLABUS_CARD_DATA: 'SYLLABUS_CARD_DATA',
  ASSIGNMENT_DATA: 'ASSIGNMENT_DATA',
  MY_PROFILE: 'MY_PROFILE',
  UPCOMING_TESTS: 'UPCOMING_TESTS',
  ACHIEVEMENT_INDEX: 'ACHIEVEMENT_INDEX',
  SINCERITY_INDEX: 'SINCERITY_INDEX',
  IMPROVEMENT_INDEX: 'IMPROVEMENT_INDEX'
};

const API_END_POINTS = {
  GET_ALL_ATTEMPTS: 'allTestAttempt',
  GET_COURSE_SUBJECTS: 'getCourseSubjects',
  GET_ACTIVE_ASSIGNMENTS: 'getActiveAssignmentsNew',
  GET_PAST_ASSIGNMENTS: 'getPastAssignments',
  GET_ALL_TESTS_ASSIGNMENTS: 'getAssignmentMain',
  GET_ASSIGNMENTS_CONCEPTS: 'getAssignment',
  GET_MY_TESTS: 'myTestNew',
  REMOVE_IMAGE: 'removeProfilePic',
  OTP_POINT: 'otpPartner',
  GET_TEST_RESULT: 'api/v1/student/{studentId}/testResult',
  SAVE_RATING: 'saveRating',
  GET_TRIAL_TESTS: 'getTrialTest',
  UPDATE_MOBILE: 'updateMobile',
  UPLOAD_IMAGE: 'profile/image/_upload',
  VERIFY_OTP: 'registration/verifyOtp',
  SEND_OTP_MOBILE: 'sendOtpMobile',
  GET_GOALS: 'getGoals',
  GET_GOALS_TYPE: 'getGoalsType',
  GET_CLASSES: 'getClass',
  GET_USER_GOALS: 'userGoals',
  CAPTURE_PAYMENT: 'capturePayment',
  GET_FREETEST_RESULT: 'recentFreeTest',
  UPDATE_OTHER_DETAILS: 'updateOtherDetails',
  SEND_EMAIL_VERIFY: 'sendVerificationMail',
  UPDATE_EMAIL: 'updateEmail',
  LOGIN: 'registration/sendOTP',
  FACEBOOK_LOGIN: 'registration/facebook?code={accessToken}',
  GOOGLE_LOGIN: 'registration/google?code={accessToken}',
  PROFILE: 'profile',
  GET_ACTIVITY: 'activity',
  ACTIVITY_LOCK: 'lockPastActivity',
  ACTIVITY_UNLOCK: 'unlockPastActivity',
  VERIFY_PARTNER: 'partnerVerify',
  UPCOMING_EXAMS: 'upcomingExams',
  GOAL_PROGRESS: 'goalProgress',
  GET_NOTIFICATION_COUNT: 'notification/count',
  GET_NOTIFICATION_LIST: 'notification',
  SET_NOTIFICATION_STATUS: 'setNotificationStatus',
  SET_GLOBAL_NOTIFICATION_STATUS: 'notification/read',
  GET_LOCATION_ADDRESS: 'maps/api/geocode/json',
  GET_TEST_ASSIGNMENT: 'findTestAssignmentsNew',
  GET_CHAPTER_TEST_ASSIGNMENT: 'getChapterTestAssignmentsNew',
  SEARCH_TEST_ASSIGNMENT: 'searchTestAssignmentsNew',
  ANSWER_QUESTION: 'answer',
  DOWNLOADED_TEST: 'downloadedTest',
  FINISH_TEST: 'finish',
  GET_SUBJECT_FILTERS: 'api/v1/getSubjectFilter',
	GET_WEAKNESS_LIST: 'api/v1/student/{studentId}/weakness',
	GET_TEST_SOLUTION: 'getSolution',
	GET_COMPARISON: 'api/v1/student/{studentId}/comparison',
  QUESTION: 'question',
	STUDENT_PROFILE: 'profile/student',
	LOGOUT: 'logout',
	DELETE_STUDENT: 'profile/student/{studentId}',
	VALIDATE_PROFILE: 'profile/student/validate-info?studentInfo={studentInfo}',
	STUDENT_PROFILE_OTP: 'profile/student/verifyOTP',
	SEND_STUDENT_OTP: 'profile/student/sendOTP',
	SEND_PROFILE_OTP: 'profile/mobile/sendOTP',
	VERIFY_PROFILE_OTP: 'profile/mobile/verifyOTP',
	GET_FEED_LIST: 'smartParenting',
	GET_FEED_DETAIL: 'smartParenting/content?id={feedId}',
  POST_FEED_LIKE: 'smartParenting/{feedId}/like',
  GET_NEW_GOALS: 'api/v1/student/{studentId}/goals',
  STUDENT_SUMMARY: 'api/v1/student/{studentId}/summary',
  ACTIVITY_CARD_DATA: 'api/v1/student/{studentId}/target/{goalId}/activity',
  SYLLABUS_CARD_DATA: 'api/v1/student/{studentId}/target/{goalId}/syllabus',
  ASSIGNMENT_DATA: 'api/v1/student/{studentId}/target/{goalId}/assignmentHistory',
  MY_PROFILE: 'api/v2/user/{userId}/myprofile',
  UPCOMING_TESTS: 'api/v1/student/{studentId}/upcomingTests',
  ACHIEVEMENT_INDEX: 'api/v1/student/:studentId/achievementIndex',
  SINCERITY_INDEX: 'api/v1/student/:studentId/sincerityIndex',
  IMPROVEMENT_INDEX: 'api/v1/student/:studentId/improvementIndex',
};

const offlineMsg = 'You are offline';
const TAB_TYPE = {
  ASSIGNMENT: 'Assigned',
  MAIN_TESTS: 'Main Tests',
  OTHERS: 'Others',
  ACTIVE: 'ACTIVE',
  PAST: 'PAST',
  ALL_TESTS: 'ALL_TESTS'
};

const TEST_LIST_ACTION = {
  APPLY_FILTER: 'ApplyFilter',
  RESET_FILTER: 'ResetFilter',
  SET_PAGINATION: 'SetPagination',
  RESET_PAGINATION: 'ResetPagination',
  START_SEARCH: 'StartReach',
  END_SEARCH: 'EndSerach'
};

const SCORE_TYPE = {
  TOTAL_SCORE: 'totalScore',
  ACCURACY: 'accuracy',
  ATTEMPT_RATE: 'attemptRate'
};

const SUBJECT_COLOR: Map<string, any> = new Map([
  ['Physics', '#5F93DE'],
  ['Mathematics', '#F2999B'],
  ['English', '#D1C4E5'],
  ['Logical Reasoning', '#FFEA60'],
  ['Chemistry', '#FFD467'],
  ['Social Science', '#5F93DE'],
  ['Mat', '#D1C4E5'],
  ['Sat', '#F2999B'],
  ['Biology', '#FFD467'],
  ['Part I', '#5F93DE'],
  ['Part Ii', '#F2999B'],
  ['Part Iii', '#D1C4E5'],
  ['Part Iv', '#FFEA60']
]);

const OFFLINE_TEST_STORAGE_LIMITS = {
  main : 5,
  concept : 20
};
const KEYBOARD_INPUT_OFFSET=40;
const IS_DEV_ENV = false;
// const BASE_URL = 'http://staging-api.mypat.in/parentApp';
// const COMMON_BASE_URL = 'http://staging-api.mypat.in/parentApp'; 

// const BASE_URL = 'http://34.87.120.217:9005/parentApp';
// const COMMON_BASE_URL = 'http://34.87.120.217:9005/parentApp'; 

const BASE_URL = 'https://v2api2-prod.mypat.in/parentApp';
const COMMON_BASE_URL = 'https://v2api2-prod.mypat.in/parentApp'; 


const EXTRA_EDGE_BASE_URL = 'https://prodapi.extraaedge.com/api/WebHook/add?AuthToken=MYPATB2C-14-06-2017&Source=mypatb2c';
// const BUILD_VERSION = '1.0.0';

export {
  SUBJECT_TYPE, API_END_POINTS,
  API_IDS, USER_INFO_TYPE, FROM_PAGE_TYPE, SOCIAL_LOGIN_MODE,
  USER_DETAILS_TYPE, TYPE, CONGRATS_PAGE_TYPE, USER_LOGIN_TYPE,
  VERIFICATION_PAGE_TYPE, DIALOG_DESCRIPTION, TAB_TYPE, BASE_URL, COMMON_BASE_URL, EXTRA_EDGE_BASE_URL, TAB_BAR_PAGE, SCORE_TYPE, TEST_TYPE,
  SUBJECT_COLOR, TEST_LIST_ACTION,
  NOTIFICATION_TYPE, NOTIFICATION_TYPE_IMAGES, NOTIFICATION_TYPE_IMAGES_SMALL,
  IS_DEV_ENV, offlineMsg, COUNTRY_CODE, OFFLINE_TEST_STORAGE_LIMITS,
  KEYBOARD_INPUT_OFFSET
}
