export const icons = {
	NETWORK_ERROR_ICON: require('../../images/noconnectionerror.png'),
	PAGER_IMG1: require('../../images/screenone.png'),
	PAGER_IMG2: require('../../images/screentwo.png'),
	PAGER_IMG3: require('../../images/screenthree.png'),
	TICK_IMG: require('../../images/tick.png'),
	APP_BLUE_HEADER: require('../../images/header-logo.png'),
	USER_ICON: require('../../images/user-icon.png'),
	STUDENT_PROFILE_LINE1_ICON: require('../../images/student-profile-1.png'),
	STUDENT_PROFILE_LINE2_ICON: require('../../images/student-profile-2.png'),
	STUDENT_PROFILE_LINE3_ICON: require('../../images/student-profile-3.png'),
	FATHER_ICON: require('../../images/father.png'),
	MOTHER_ICON: require('../../images/mother.png'),
	SIBLING_ICON: require('../../images/sibling.png'),
	GUARDIAN_ICON: require('../../images/guardian.png'),
	LEFT_ARROW: require('../../images/left-arrow.png'),
	RIGHT_ARROW: require('../../images/right-arrow.png'),
	ACCESS_CODE_WEB: require('../../images/access-code-web.png'),
	ACCESS_CODE_APP: require('../../images/access-code-app.png'),
	LIKE_ICON: require('../../images/like-icon.png'),
	LIKED_ICON: require('../../images/liked-icon.png'),
	OR_LINE_ICON: require('../../images/or-line.png'),
	PENCIL_ICON: require('../../images/pencil-icon.png'),
	STEP_DONE_ICON: require('../../images/step-done.png'),
	TOUR_ONE: require('../../images/tour1.png'),
	TOUR_TWO: require('../../images/tour2.png'),
	TOUR_THREE: require('../../images/tour3.png'),
	TOUR_FOUR: require('../../images/tour4.png'),
	TOUR_FEED: require('../../images/tour-feed.png'),
	TOUR_HISTORY: require('../../images/tour-history.png'),
	NO_NOTIFICATION: require('../../images/no_notification.png'),
	NO_HISTORY: require('../../images/empty-history.png'),
	NO_FEED: require('../../images/no_feed.png'),
	ADD_NEW_STUDENT: require('../../images/member.png'),
	PROFILE_ICON: require('../../images/profile-pic.png'),
	ADD_STUDENT: require('../../images/add-student.png'),
	LOGOUT_ICON: require('../../images/icon_logout.png'),
	DELETE_STUDENT_POPUP: require('../../images/icon_warning.png'),
	SHARE_ICON: require('../../images/share-icon.png'),
	CLOSEWHITE_ICON: require('../../images/close.png'),
	HEADER_CUSTOM_ICON: require('../../images/headercustom_image.png'),
	PROFILE_HEADER: require('../../images/profile-header.png'),
	HOME_HEADER: require('../../images/home-header.png'),
	HOME_HEADER_TABLET: require('../../images/home-header-tablet.png'),
	HOME_PROFILE_BADGE: require('../../images/student-badge.png'),
	COMPARISON_HEADER_ICON: require('../../images/comparison_header_image.png'),
	PERFORMANCE_HEADER_ICON: require('../../images/performance_header_image.png'),
	SOLUTION_HEADER_ICON: require('../../images/solution_header_image.png'),
	UNATTEMPT_SMILE_ICON: require('../../images/unattmpted_smile.png'),
	CORECT_ICON_WHITE: require('../../images/correct_white_icon.png'),
	SMILEY_ICON: require('../../images/smiley_icon.png'),
	SMILEY_NEW_ICON: require('../../images/smiley.png'),
	SMILEY_SAD_ICON: require('../../images/sad-face.png'),
	SWOT_HEADER_ICON: require('../../images/swot_header_icon.png'),
	STRENTH_WEAKNESS_ICON: require('../../images/strenth_weakness_icon.png'),
	COMPARE_TOPPER_ICON: require('../../images/compare_topper_icon.png'),
	COMPARE_AVERAGE_ICON: require('../../images/compare_average_icon.png'),
	COMPARE_YOU_TIMEICON: require('../../images/compare_you_timeicon.png'),
	COMPARE_AVERAGE_TIMEICON: require('../../images/compare_average_timeicon.png'),
	COMPARE_TOPPER_TIMEICON: require('../../images/compare_topper_timeicon.png'),
	CIRCLEIMAGE_ICON: require('../../images/circle-image.png'),
	NOTESTICON: require('../../images/no_test_paper.png'),
	BACK_ARROW_WHITE: require('../../images/left-white-arrow.png'),
	BACK_ARROW_BLUE: require('../../images/left-blue-arrow.png'),
	CORRECT_ICON: require('../../images/correct.png'),
	RESULT_PERFORMANCE_ICON: require('../../images/result_performance_icon.png'),
	RESULT_THUMBDOWN_ICON: require('../../images/resultthumb_down_icon.png'),
	RESULT_THUMBUP_ICON: require('../../images/resultthumb_up_icon.png'),
	RESULT_FILTER_ICON: require('../../images/result_filter.png'),
	SAINT_ICON: require('../../images/saint_icon.png'),
	CONGRATULATIONS_ICON: require('../../images/congratulations_icon.png'),
	INCORRECT_ICON: require('../../images/incorrect_icon.png'),
	ARROW_UP_ICON: require('../../images/arrow-up.png'),
	CROSSGREY_ICON: require('../../images/cross.png'),
	RESULT_COMPARISON_ICON: require('../../images/result_comparison_icon.png'),
	FEED_DEFAULT_ICON: require('../../images/feed-default-icon.png'),
	// Common Library
	MYPAT_ICON: require('../../images/logo_mypat.png'),
	DEFAULT_PROFILE_ICON: require('../../images/profile_avatar.png'),
	MYPAT_WHITE_ICON: require('../../images/mypat_icon.png'),
	DOWNLOAD_NET_ICON: require('../../images/download_server_icon.png'),
	DOWNLOAD_FILE_ICON: require('../../images/downloadFile_icon.png'),
	DOWNLOAD_MOBILE_ICON: require('../../images/downloadMobileIcon.png'),
	SEARCH_ICON: require('../../images/search.png'),
	BACK_ARROW: require('../../images/arrow.png'),
	HEADER_SERACH_ICON: require('../../images/search_header.png'),
	GOALICON_ICON: require('../../images/goalicon.png'),
	HEADERGOAL_ICON: require('../../images/headergoal_Icon.png'),
	NAVTOP_ICON: require('../../images/navigation_top.png'),
	BACK_WHITE_ICON: require('../../images/arrow_white.png')
};
