// @ts-ignore
import { StackNavigator } from 'react-navigation';
import {
  getTabBarViews, getTabBarTitles, isSignedIn, getParentContainable, getUserDetails, hasCompleteWalkThroughPage
} from '../utils'
import {
  OTPVerificationPage, CongratsPage, CongratsAfterAddStudentPage, LoginPage, SignupPage, TabBarPage, WalkThroughPage,
  TermsOfUsePage, PrivacyPolicyPage, AddInfoPage, SplashPage, FeedDetailPage,
	AddEmailPage, AddProfilePage1, AddProfilePage2, AddProfilePage3, AddProfileOTPPage, AddProfileViewPage, UpdateProfileOTPPage,
  NotificationListPage
} from '../screens';
import { colors } from '../config';
import { HEADER_TITLE_TYPE, HEADER_BACKGROUND_TYPE } from '../common';
import { setInitialRoute } from '../services';
import { ResultLandingScreenPage } from '../screens/testresult';
import { ResultPerformancePage } from '../screens/testresult';
import { ResultComparisonPage, SolutionDetailPage, ComparisonFilterPage, ResultSwotPage, ResultSwotDetailPage } from '../screens/testresult';
import { ResultSolutionPage } from '../screens/testresult/ResultSolutionPage';
import ActivityChartPage from '../components/dashboard/screens/ActivityChartPage';

const termsOfUseScreen = {
  screen: getParentContainable(TermsOfUsePage, {
    showHeader: true,
    headerConfiguration: {
      // headerTitle: 'Terms of Use',
      headerTitleType: HEADER_TITLE_TYPE.TEXT,
      iconConfigurations: {
        shouldShowBackIcon: true
      },
      headerTitleColor: colors.White,
      textButtonTitleColor: colors.White,
      headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const privacyPolicyScreen = {
  screen: getParentContainable(PrivacyPolicyPage, {
    showHeader: true,
    headerConfiguration: {
      // headerTitle: 'Privacy Policy',
      headerTitleType: HEADER_TITLE_TYPE.TEXT,
      iconConfigurations: {
        shouldShowBackIcon: true
      },
      headerTitleColor: colors.White,
      textButtonTitleColor: colors.White,
      headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false

  }
};

const splashPageScreen = {
  screen: getParentContainable(SplashPage, { showHeader: false, offline: true }),
  navigationOptions: {
		// @ts-ignore
    header: null,
    gesturesEnabled: false
  }
};

const walkThroughScreen = {
  screen: getParentContainable(WalkThroughPage, { showHeader: false, offline: true }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const homeScreen = {
  screen: getParentContainable(TabBarPage, {
    offline: true,
    showHeader: false,
    showTabBar: true,
    tabBarProvider: getTabBarViews,
    headerConfiguration: {
      headerBackground: colors.White,
      headerBackgroundType: HEADER_BACKGROUND_TYPE.NORMAL,
      iconConfigurations: {
        shouldShowBackIcon: false,
        shouldShowCartIcon: false,
        shouldShowNotificationIcon: false,
        shouldHaveCustomFilterClick: false
      },
      dropdownConfiguration: {
        shouldShowDropdownRight: true,
        dropdownTitle: '',
        dropdownTitleColor: colors.White
      }
    },
    tabBarTitles: getTabBarTitles()
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const otpVerificationScreen = {
	screen: getParentContainable(OTPVerificationPage, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false
			},
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: false
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addInfoScreen = {
	screen: getParentContainable(AddInfoPage, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addEmailScreen = {
	screen: getParentContainable(AddEmailPage, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addProfile1Screen = {
	screen: getParentContainable(AddProfilePage1, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addProfile2Screen = {
	screen: getParentContainable(AddProfilePage2, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addProfileOTPScreen = {
	screen: getParentContainable(AddProfileOTPPage, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const updateProfileOTPScreen = {
	screen: getParentContainable(UpdateProfileOTPPage, {
		showHeader: false,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addProfileViewScreen = {
	screen: getParentContainable(AddProfileViewPage, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const addProfile3Screen = {
	screen: getParentContainable(AddProfilePage3, {
		showHeader: true,
		offline: true ,
		headerConfiguration: {
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: true
			},
			textButtonTitle: 'Skip',
			titleColor: colors.White,
			headerTitleColor: colors.Black,
			textButtonTitleColor: colors.Black,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
			backIconThemeDark: true
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const congratsScreen = {
  screen: getParentContainable(CongratsPage, {
    showHeader: false,
    headerConfiguration: {
      iconConfigurations: {
        shouldShowBackIcon: true,
        shouldHaveCustomBackClick: true
      },
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White,
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const notificationListPage = {
  screen: getParentContainable(NotificationListPage, {
    showHeader: true,
    headerConfiguration: {
      headerTitle: 'Notification',
      headerTitleType: HEADER_TITLE_TYPE.TEXT,
      headerTitleColor: colors.White,
      iconConfigurations: {
        shouldShowBackIcon: false,
        shouldShowCartIcon: false,
        shouldShowNotificationIcon: false,
        shouldHaveCustomBackClick: false,
        shouldHaveDropDownClick: false
      },
      headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const loginScreen = {
  screen: getParentContainable(LoginPage, {
    showHeader: true,
    offline: true,
    headerConfiguration: {
      iconConfigurations: {
        shouldShowBackIcon: false,
        shouldShowCartIcon: false,
        shouldShowNotificationIcon: false,
				shouldShowProfileIcon: false
      },
      headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerBackground: colors.White
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const signupScreen = {
  screen: getParentContainable(SignupPage, {
    showHeader: true,
		offline: true,
    headerConfiguration: {
      iconConfigurations: {
        shouldShowBackIcon: false,
        shouldShowCartIcon: false,
        shouldShowNotificationIcon: false,
				shouldShowProfileIcon: false
      },
      headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
      headerBackground: colors.White
    }
  }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const congratsAfterGoalScreen = {
  screen: getParentContainable(CongratsAfterAddStudentPage, { showHeader: false }),
  navigationOptions: {
		// @ts-ignore
  	header: null,
    gesturesEnabled: false
  }
};

const activityChartPage = {
  screen: ActivityChartPage,
  navigationOptions: {
		// @ts-ignore
    header: null,
    gesturesEnabled: false
  }
};

const feedDetailPage = {
	screen: getParentContainable(FeedDetailPage, {
		offline: true,
		showHeader: false,
		showTabBar: false,
		headerConfiguration: {
			headerBackground: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.NORMAL,
			iconConfigurations: {
				shouldShowBackIcon: false,
				shouldShowCartIcon: false,
				shouldShowNotificationIcon: false,
				shouldHaveCustomFilterClick: false
			},
			dropdownConfiguration: {
				shouldShowDropdownRight: true,
				dropdownTitle: '',
				dropdownTitleColor: colors.White
			}
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultLandingScreen = {
	screen: getParentContainable(ResultLandingScreenPage, {
		showHeader: false,
		offline: true,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultPerformanceScreen = {
	screen: getParentContainable(ResultPerformancePage, {
		showHeader: false,
		offline: true,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerStatusColor: '#9480F2'
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultComparisonScreen = {
	screen: getParentContainable(ResultComparisonPage, {
		showHeader: false,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerStatusColor: '#4D6094'
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultSolutionScreen = {
	screen: getParentContainable(ResultSolutionPage, {
		showHeader: false,
		offline: true,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerStatusColor: '#058DC4'
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultSwotScreen = {
	screen: getParentContainable(ResultSwotPage, {
		showHeader: false,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE,
			headerStatusColor: '#E2675D'
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const testResultSwotDetailScreen = {
	screen: getParentContainable(ResultSwotDetailPage, {
		showHeader: false,
		headerConfiguration: {
			headerTitle: 'Edit Other Details',
			headerTitleType: HEADER_TITLE_TYPE.TEXT,
			iconConfigurations: {
				shoulShowBackIcon: true,
				shouldShowCartIcon: false,
				shouldShowFilterIcon: false,
				shouldShowTextButtonIcon: true,
				shouldShowNotificationIcon: false,
				shouldHaveCustomTextButtonClick: false,
				shouldHaveCustomBackClick: true
			},
			titleColor: colors.White,
			headerTitleColor: colors.White,
			textButtonTitleColor: colors.White,
			headerBackgroundType: HEADER_BACKGROUND_TYPE.IMAGE
		}
	}),
	navigationOptions: {
		// @ts-ignore
		header: null,
		gesturesEnabled: false
	}
};

const routes = {
  LoginPage: loginScreen,
  SignupPage: signupScreen,
  HomePage: homeScreen,
  WalkThroughPage: walkThroughScreen,
  OTPVerificationPage: otpVerificationScreen,
  AddInfoPage: addInfoScreen,
	AddEmailPage: addEmailScreen,
	AddProfile1Page: addProfile1Screen,
	AddProfile2Page: addProfile2Screen,
	AddProfileOTPPage: addProfileOTPScreen,
	UpdateProfileOTPPage: updateProfileOTPScreen,
	AddProfileViewPage: addProfileViewScreen,
	AddProfile3Page: addProfile3Screen,
  CongratsPage: congratsScreen,
  CongratsAfterGoalPage: congratsAfterGoalScreen,
  TermsOfUsePage: termsOfUseScreen,
  PrivacyPolicyPage: privacyPolicyScreen,
  SplashPage: splashPageScreen,
  NotificationListPage: notificationListPage,
  ActivityChartPage: activityChartPage,
	FeedDetailPage: feedDetailPage,
	ResultLandingScreenPage: testResultLandingScreen,
	ResultPerformancePage: testResultPerformanceScreen,
	ResultComparisonPage: testResultComparisonScreen,
	ResultSolutionPage: testResultSolutionScreen,
	ResultSwotPage: testResultSwotScreen,
	ResultSwotDetailPage: testResultSwotDetailScreen
};

async function getNavigator(cb: any) {
  isSignedIn().then(async isLoggedIn => {
    const initialRouteName = isLoggedIn ? await getOnGoingPage() : await getOnBoardPage();
    setInitialRoute(initialRouteName);
    cb(StackNavigator(routes, { initialRouteName }))
  })
}

async function getOnBoardPage() {
  const isCompleteWalkThroughPage = await hasCompleteWalkThroughPage();
  if (isCompleteWalkThroughPage) {
    return 'SignupPage'
  }
  return 'WalkThroughPage'
}

async function getOnGoingPage() {
  const promises = [
		getUserDetails()
  ];

  const responses = await Promise.all(promises);
  let userDetails:any = responses[0];
  if (userDetails) {
		userDetails = JSON.parse(userDetails);
	}

	// @ts-ignore
	if (!userDetails.profile.isVerified) {
    return getOnBoardPage();
  } else {
			// @ts-ignore
			if (userDetails.profile && userDetails.profile.name === "") {
				return 'AddInfoPage';
			// @ts-ignore
			} else if (userDetails.profile && userDetails.profile.email === "") {
				return 'AddEmailPage';
			// @ts-ignore
			} else if(userDetails.profile.studentProfiles && userDetails.profile.studentProfiles.length === 0) {
				return 'AddProfile1Page';
			}else if (userDetails.profile.studentProfiles 
					&& userDetails.profile.studentProfiles.length === 1 
					&& !userDetails.profile.studentProfiles[0].isVerified) {
				return 'AddProfileViewPage'
			} else {
				return 'HomePage';
			}
	}
}

export default getNavigator
