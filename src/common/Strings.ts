export const strings  = {
    NETWORK_ERROR_HEADING: 'OOPS! No internet found',
    NETWORK_ERROR_MSG: 'Check your connection and try again'
};
