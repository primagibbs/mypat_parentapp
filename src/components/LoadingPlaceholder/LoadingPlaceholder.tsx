import React, { Component } from 'react';
import {
  View
} from 'react-native';
// @ts-ignore
import Placeholder from 'rn-placeholder';

import { widthPercentage } from '../../common';

interface Props {
  count?: number
  loading: boolean
  size?: number
}

export class LoadingPlaceholder extends Component<Props> {
  constructor(props: any) {
    super(props)
  }

  placeholders(count: number, loading: boolean, size: number) {
    let loadingCards = [];
    const width = widthPercentage(90);
    for (let i = 0; i < count; i++) {
      loadingCards.push(
        <View style={{ flex: 1, width, alignSelf: 'center', marginTop: 5 }}>
          <Placeholder
            size={size}
            animate='fade'
            lineNumber={3}
            lineSpacing={8}
            lastLineWidth='60%'
            onReady={!loading}
            style={{ flex: 1, width, alignSelf: 'center', marginTop: 5 }}
          />
        </View>
      )
    }

    return loadingCards
  }

  render() {
    const { count = 9, loading = true, size = 90 } = this.props;
    return loading ? (
      <View>{this.placeholders(count, loading, size)}</View>
    ) : null
  }
}
