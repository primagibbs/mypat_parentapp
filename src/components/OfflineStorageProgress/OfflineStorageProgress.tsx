import React, { Component } from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'
import { inject, observer } from 'mobx-react'
import { OfflineStorageProgressStore } from '../../store'

import { OFFLINE_TEST_STORAGE_LIMITS, isIphone, getIPhoneCategorization, IPHONE_CATEGORIZE, widthPercentage } from '../../common'
import { fetchDownloadedTestsCountBySubCategory } from '../../persistence/utils'
import { isTablet } from 'react-native-device-info'

interface Props {
  offlineStorageProgressStore?: OfflineStorageProgressStore
  count?: number
  size?: number
  showLegend?: boolean
}

interface State {
  style?: any
}

@inject('offlineStorageProgressStore')
@observer
export class OfflineStorageProgress extends Component<Props, State> {
  constructor(props) {
    super(props)
    this.state = {
      style: getStyle()
    }
  }

  async componentDidMount() {
    const { offlineStorageProgressStore } = this.props
    await offlineStorageProgressStore.getConceptTestCount()
    await offlineStorageProgressStore.getMainTestCount()
  }

  legends() {
    const { showLegend } = this.props
    const { style } = this.state
    if (showLegend) {
      return (<View style={style.legendsContainer}>
        <View style={style.mainTestLegendContainer}>
          <View style={style.mainTestLegend}></View>
          <Text style={style.mainTestLegendText}>Main Tests</Text>
        </View>
        <View style={style.conceptTestLegendContainer}>
          <View style={style.conceptTestLegend}></View>
          <Text style={style.conceptTestLegendText}>Concept Tests</Text>
        </View>
      </View>)
    } else {
      return null
    }
  }

  offlineStorageHeading() {
    const { showLegend } = this.props
    const { style } = this.state
    if (showLegend) {
      return (<View>
        <Text style={style.mainHeading}>Your Downloaded Tests</Text>
      </View>)
    } else {
      return null
    }
  }

  getfontValueSmall() {
    let fontValue = 12
    if (isIphone()) {
      switch (getIPhoneCategorization()) {
        case IPHONE_CATEGORIZE.IPHONE_4_5_SERIES:
          fontValue = 10
          break
        default:
          break
      }
    }
    return fontValue
  }

  getfontValue() {
    let fontValue = 17
    if (isIphone()) {
      switch (getIPhoneCategorization()) {
        case IPHONE_CATEGORIZE.IPHONE_4_5_SERIES:
          fontValue = 14
          break
        default:
          break
      }
    }
    return fontValue
  }

  getwidthValue() {
    let fontValue = 110
    if (isIphone()) {
      switch (getIPhoneCategorization()) {
        case IPHONE_CATEGORIZE.IPHONE_4_5_SERIES:
          fontValue = 100
          break
        default:
          break
      }
    }
    return fontValue
  }

  render() {
    const fontValuesmall = this.getfontValueSmall()
    const fontValue = this.getfontValue()
    const widthValue = this.getwidthValue()
    const { style } = this.state
    const { showLegend, offlineStorageProgressStore } = this.props
    let scaledMainTestCount = (offlineStorageProgressStore.mainTestCount / OFFLINE_TEST_STORAGE_LIMITS.main) / 2
    let scaledConceptTestCount = (offlineStorageProgressStore.conceptTestCount / OFFLINE_TEST_STORAGE_LIMITS.concept) / 2
    // console.warn('scaledMainTestCount', scaledMainTestCount)
    // console.warn('scaledConceptTestCount', scaledConceptTestCount)
    if (showLegend) {
    return (
      <View style={style.container}>
        <View>
          <Text style={[style.subHeading, {fontSize: isTablet() ? 20 : fontValue}]}>Offline Storage</Text>
        </View>
        <View style={{ width: widthPercentage(100) - 20, flexDirection: 'row', backgroundColor: '#1768C4'}}>
            <Animated.View style={[style.bar, style.main, {width: scaledMainTestCount * (widthPercentage(100) - 20)}]}/>
            <Animated.View style={[style.bar, style.concept, {width: scaledConceptTestCount * (widthPercentage(100) - 20)}]}/>
          </View>
        {this.legends()}
      </View>
    )
    } else {
      return <View style={style.headerContainer}>
        <Text style={[style.subHeading, { fontSize: isTablet() ? 14 : fontValuesmall}]}>Offline Storage</Text>
        <View style={{ width: isTablet() ? 150 : widthValue, flexDirection: 'row', backgroundColor: '#1768C4' }}>
          <Animated.View style={[style.headerBar, style.main, { width: scaledMainTestCount * (isTablet() ? 150 : widthValue)}]} />
          <Animated.View style={[style.headerBar, style.concept, { width: scaledConceptTestCount * (isTablet() ? 150 : widthValue)}]} />
        </View>
      </View>
    }
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    backgroundColor: '#1D7DEA',
    padding: 10
  },
  headerContainer: {
    padding: 10
  },
  mainHeading: {
    fontWeight: '500',
    fontSize: 28,
    lineHeight: 28,
    color: '#FFFFFF',
    paddingTop: 10,
    paddingBottom: 10
  },
  subHeading: {
    letterSpacing: 0.5,
    color: '#FFFFFF',
    paddingBottom: 5
  },
  legendsContainer: {
    flexDirection: 'row'
  },
  mainTestLegendContainer: {
    flexDirection: 'row',
    paddingTop: isTablet() ? 8 : 5,
    paddingRight: 5,
    marginRight: 25
  },
  conceptTestLegendContainer: {
    flexDirection: 'row',
    paddingTop: isTablet() ? 8 : 5,
    paddingRight: 5,
    marginRight: 25
  },
  mainTestLegend: {
    width: 10,
    height: 10,
    backgroundColor: '#ECCF35',
    marginTop: isTablet() ? 6 : 3,
    marginRight: 3
  },
  conceptTestLegend:  {
    width: 10,
    height: 10,
    backgroundColor: '#8ECE30',
    marginTop: isTablet() ? 6 : 3,
    marginRight: 3
  },
  mainTestLegendText: {
    color: '#FFFFFF',
    fontSize: isTablet() ? 15 : 12
  },
  conceptTestLegendText: {
    color: '#FFFFFF',
    fontSize: isTablet() ? 15 : 12
  },
  headerBar: {
    alignSelf: 'center',
    borderRadius: 0,
    height: 3,
    marginRight: 0
  },
  bar: {
    alignSelf: 'center',
    borderRadius: 0,
    height: 5,
    marginRight: 0
  },
  main: {
    backgroundColor: '#ECCF35'
  },
  concept: {
    backgroundColor: '#8ECE30'
  },
})
