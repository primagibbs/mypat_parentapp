import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableWithoutFeedback, Platform, ActivityIndicator } from 'react-native'
import { inject, observer } from 'mobx-react'
import { colors, dimens } from '../../config'
import { ActivityCardDataStore } from '../../store'
import { widthPercentage, isTablet } from '../../common'
// @ts-ignore
import FusionCharts from 'react-native-fusioncharts'
import DashboardInfotip from './components/DashboardInfotip'

interface Props {
	activityCardDataStore?: ActivityCardDataStore
	containerStyle?: any
	selectedYAxis?: string
	backgroundColor?: string
}
interface State {
	styles?: any
}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	container: {
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	graphContainer: {
		width: widthPercentage(90),
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10
	},
	emptyContainer: {
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	emptyState: {
		width: 150,
		height: 100
	},
	emptyText: {
		fontSize: isTablet() ? 20 : 16,
		color: colors.HeaderColor,
		lineHeight: isTablet() ? 20 : 16,
		marginTop: 13
	},
	emptySubText: {
		fontSize: isTablet() ? 16 : 13,
		color: colors.SubHeaderColor,
		opacity: 0.54,
		lineHeight: isTablet() ? 20 : 16,
		marginTop: 5,
    marginHorizontal: 20,
    textAlign: 'center'
	},
	cardHeader: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%'
	},
	cardTitle: {
		fontSize: isTablet() ? 21 : 16,
		color: colors.ContentColor
	},
	cardSubtitle: {
		fontSize: isTablet() ? 15 : 12,
		color: colors.ContentColor,
		paddingTop: 8
	},
	seriesTitle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: 10,
		marginTop: 16
	},
	seriesHeader: {
		marginHorizontal: 10,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	headerColor: {
		width: 8,
		height: 8,
		borderRadius: 4,
		marginRight: 3
	},
	seriesName: {
		fontSize: isTablet() ? 15 : 12,
		color: colors.HeaderColor
	}
});

@inject('activityCardDataStore')
@observer
export default class ActivityChart extends Component<Props, State> {
	libraryPath: any;

	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle()
		};
		this.libraryPath = Platform.select({
			// Specify fusioncharts.html file location
			android: {
				uri: 'file:///android_asset/fusioncharts.html'
			},
			ios: require('../../../assets/fusioncharts.html')
		})
	}

	getActivityOrderedKeys(): any {
		const defaultOrder = [ 'userScore', 'avgScore', 'topperScore'  ]
		return defaultOrder
	}	

	createDataSource(): any {
		const { activityCardDataStore } = this.props;
		const { activityData, colorMap, seriesNameMap, defaultYAxis, subject } = activityCardDataStore;
		let seriesMap = {}, yAxis = defaultYAxis, categories = [], tempActivityData = [...activityData];
		tempActivityData.forEach((row) => {
			let xAxis = row.testName;
			categories.push({
				label: xAxis
			});
			let yData = row.subjects[subject] ? row.subjects[subject][yAxis] : [];
			if (yData) {
				this.getActivityOrderedKeys().forEach((yKey) => {
					if (!seriesMap[yKey]) {
						seriesMap[yKey] = []
					}
					seriesMap[yKey].push({
						value: Math.floor(yData[yKey] * 100) / 100
					})
				})
			}
		});

		let dataset = Object.keys(seriesMap).map((series) => {
			return {
				seriesname: seriesNameMap[series],
				data: seriesMap[series],
				color: colorMap[series]
			}
		});
		return {
			chart: {
				showhovereffect: '0',
				drawcrossline: '1',
				showlegend: '0',
				maxLabelHeight: 20,
				theme: 'fusion',
				showtooltip: '0',
				slantLabels: '0',
				rotateLabels: '0',
				labeldisplay: 'auto',
				useEllipsesWhenOverflow:  '1',
                maxLabelWidthPercent: 33
			},
			categories: [{
				category: categories
			}],
			dataset
		}
	}

	render() {
		const { styles } = this.state;
		const { containerStyle, activityCardDataStore } = this.props;
		let dataSource = this.createDataSource();
		return (
			<View style={[containerStyle, styles.container]}>
				<View style={styles.cardHeader}>
					<Text style={styles.cardTitle}>Activity Graph</Text>
					<DashboardInfotip tooltipText={'View and compare your child\'s performance across attempted assignments'} />
				</View>
				<View style={styles.cardHeader}>
					<Text style={styles.cardSubtitle}>Score % vs Tests (click to view details)</Text>
				</View>
				<View style={styles.seriesTitle}>
					{dataSource.dataset.map(d => (
						<View key={d.seriesname} style={styles.seriesHeader}>
							<View style={[styles.headerColor, { backgroundColor: d.color }]}></View>
							<Text style={styles.seriesName}>{d.seriesname}</Text>
						</View>
					))}
				</View>

				<View style={styles.graphContainer}>
          { activityCardDataStore.isChartLoading ?
            <ActivityIndicator size='small' />
            :
            dataSource.dataset.length > 0 ?
						<FusionCharts
							type='msline'
							width={isTablet() ? widthPercentage(90) : widthPercentage(80)}
							height={300}
							dataFormat='json'
							dataSource={this.createDataSource()}
							libraryPath={this.libraryPath} // set the libraryPath property
						/> :
						<View style={styles.emptyContainer}>
							<Image source={require('../../../images/no-assignments.png')} style={styles.emptyState} resizeMode='contain' />
							<Text style={styles.emptyText}>There is no activity</Text>
							<Text style={styles.emptySubText}>Ask your child to take a test to start viewing the activity graph</Text>
						</View>
					}
				</View>
			</View>
		)
	}

}
