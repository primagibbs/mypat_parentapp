import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableWithoutFeedback, Image } from 'react-native'
import { dimens, colors } from '../../config'
import {widthPercentage, isTablet, verticalScale, icons} from '../../common'
import moment from 'moment'

interface Props {
	item: any,
	containerStyle?: any,
	onPress?: any
}
interface State {
	styles?: any
}

const getStyle = () => StyleSheet.create({
	container: {
		alignItems: 'flex-start',
		justifyContent: 'center',
		paddingVertical: 20,
		paddingLeft: 15,
		paddingRight: 15
	},
	titleRow: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '100%',
		marginBottom: 8
	},
	triangle: {
		width: verticalScale(12),
		height: verticalScale(12),
		// marginRight: 8,
		transform: [
			{rotate: '90deg'}
		]
	},
	cardTitle: {
		fontSize: isTablet() ? verticalScale(10) : verticalScale(16),
		lineHeight: 24,
		color: colors.SubHeaderColor,
		width: '80%'
	},
	dateText: {
		fontSize: isTablet() ? verticalScale(8) : verticalScale(10),
		color: colors.ContentColor,
		marginBottom: 13
	},
	scoreText: {
		fontSize: isTablet() ? verticalScale(15) : verticalScale(20),
		color: colors.SubHeaderColor,
	},
	dueDateLabel: {
		fontSize: isTablet() ? verticalScale(8) : verticalScale(10),
		color: '#EB1919',
		paddingTop: 4,
		paddingBottom: 4
	}
});

export class AssignmentCard extends Component<Props, State> {

	constructor(props: Props, state: State) {
		super(props, state);
		this.state = {
			styles: getStyle()
		}
	}

	_onPress = () => {
		if (this.props.onPress) {
			this.props.onPress(this.props.item)
		}
	};

	renderCardTitle = () => {
		const { styles } = this.state;
		const { item } = this.props;
		const title = (
			<Text numberOfLines={1} style={styles.cardTitle}>{item.title}</Text>
		);
		return (
			<View style={styles.titleRow}>
				{title}
				{item.isAttempted ? <Image source={icons.ARROW_UP_ICON} style={styles.triangle} /> : null }
			</View>
		)
	};

	renderTime = () => {
		const { styles } = this.state;
		const { item } = this.props;

		let timeText = (item.isAttempted ? 'Attempted on ' : 'Due date ') + moment(item.date).format('DD MMM YYYY')
		return (
			<Text
				numberOfLines={1}
				style={[styles.dateText, { marginBottom: 0 }]}>
				{timeText}
			</Text>
		)
	};

	renderScoreRow = () => {
		const { styles } = this.state;
		const { item } = this.props;
		let currentDate = new Date().getTime();
		let scoreText = '', scoreStyle = {};
		if(item.isAttempted) {
			scoreText = item.score ? `${item.score.user}/${item.score.total}` : '';
			scoreStyle = styles.scoreText
		} else if(currentDate > item.date) {
			scoreText = 'Due date missed';
			scoreStyle = styles.dueDateLabel
		}
		return (
			<View style={[styles.titleRow, {marginBottom: 3}]}>
				<Text numberOfLines={1} style={scoreStyle}>
					{scoreText}
				</Text>
				{this.renderTime()}
			</View>
		)
	};

	render() {
		const { styles } = this.state;
		const { item, containerStyle } = this.props;
		return (
			<TouchableWithoutFeedback onPress={() => item.isAttempted ? this._onPress() : null}>
				<View style={[containerStyle, styles.container]}>
					{this.renderCardTitle()}
					{this.renderScoreRow()}
				</View>
			</TouchableWithoutFeedback>
		)
	}
}
