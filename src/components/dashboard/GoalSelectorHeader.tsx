import React, { Component } from 'react'
import { TouchableWithoutFeedback, View, Text, StyleSheet, ScrollView, FlatList } from 'react-native';
import { NewGoalsDataStore } from '../../store';
import { inject, observer } from 'mobx-react';
import { colors } from '../../config';
import { isPortrait, verticalScale, widthPercentage } from "../../common-library/common";

interface Props {
	newGoalsDataStore?: NewGoalsDataStore
}
interface State {
	styles: any
}

@inject('newGoalsDataStore')
@observer
export class GoalSelectorHeader extends Component<Props, State> {

	constructor(props: Props, state: State) {
		super(props, state);

		this.state = {
			styles: getStyle()
		}
	}

	_onGoalClicked = async (goalId: any) => {
		await this.props.newGoalsDataStore.onGoalSwitch(goalId);
	};

	renderButton = (item, isLastItem = false) => {
		const { styles } = this.state;
		return (
			<TouchableWithoutFeedback key={item.id} onPress={this._onGoalClicked.bind(this, item.id)}>
				<View style={[styles.buttonView, item.selected ? styles.activeButtonView : null, isLastItem ? { marginRight: 0 } : null ]}>
					<Text numberOfLines={1} style={[styles.buttonText, item.selected ? styles.activeText : null]}>
						{item.title}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		)
	};

	getGoalName = (goal) => goal.goalCode + ' - ' + goal.goalYear;

	formatData = (data) => {
		const { newGoalsDataStore } = this.props;
		return data.map((goal) => ({
			id: goal.targetId,
			title: this.getGoalName(goal),
			selected: newGoalsDataStore.activeGoal && newGoalsDataStore.activeGoal.targetId === goal.targetId
		}))
	};

	render() {
		const goalsList = this.props.newGoalsDataStore.goals;

		if (goalsList && goalsList.length) {
			let data = this.formatData(goalsList);
			return (
				<FlatList
					horizontal
					showsHorizontalScrollIndicator={false}
					style={{ marginTop: 12 }}
					data={data}
					renderItem={({ item, index }) => this.renderButton(item, index === data.length - 1)}
				/>
			)
		} else {
			return null;
		}
	}
}

const getStyle = () => StyleSheet.create({

	buttonView: {
		marginRight: 20,
		width: 150,
		paddingHorizontal: 15,
		height: 36,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 18,
		borderColor: '#BEBEBE',
		borderWidth: 1,
		backgroundColor: colors.White
	},
	activeButtonView: {
		backgroundColor: '#2E93FB'
	},
	buttonText: {
		fontSize: 17,
		color: colors.SubHeaderColor
	},
	activeText: {
		color: colors.White
	}
});
