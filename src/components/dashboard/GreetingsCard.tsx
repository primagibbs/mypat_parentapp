import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableWithoutFeedback, Image, ImageBackground } from 'react-native'
import { colors } from '../../config'
import { ProfileDataStore, NewGoalsDataStore } from '../../store'
import { inject, observer } from 'mobx-react'
import { widthPercentage, verticalScale, icons, isTablet } from '../../common'
import { capitalizeWords } from '../../utils/app-utils'

interface Props {
	profileDataStore?: ProfileDataStore,
	newGoalsDataStore?: NewGoalsDataStore,
	onAddStudent: any
}
interface State {
	styles?: any,
	contentHeight: number
}

const getStyle = () => StyleSheet.create({
	headerBackground: {
		height: widthPercentage(100) * 0.75,
		width: widthPercentage(100)
	},
	profileContainer: {
		width: widthPercentage(100),
		paddingVertical: 20,
		paddingHorizontal: 15,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	greetText: {
		fontWeight: "500",
		fontSize: verticalScale(26),
		color: colors.White,
		width: '110%'
	},
	greetMsg: {
		width: '75%',
		fontSize: verticalScale(13),
		lineHeight: verticalScale(13) * 1.5,
		color: colors.White
	},
	profileImage: {
		width: verticalScale(72),
		height: verticalScale(72),
		borderRadius: verticalScale(36),
		borderWidth: verticalScale(8),
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: 'rgba(255, 255, 255, 0.4)'
	},
	topLeftOverlay: {
		position: 'absolute',
		top: 0,
		left: 0
	},
	addBtn: {
		width: 20,
		height: 20
	},
	badgeContainer: {
		position: 'absolute',
		bottom: -8,
		right: -8,
		width: verticalScale(26),
		height: verticalScale(26),
		borderRadius: verticalScale(13),
		borderWidth: verticalScale(2),
		borderColor: 'rgba(255, 255, 255, 0.4)',
		alignItems: 'center',
		justifyContent: 'center'
	},
	badgeIcon: {
		width: verticalScale(24),
		height: verticalScale(24),
		borderRadius: verticalScale(12)
	}
});

@inject('profileDataStore', 'newGoalsDataStore')
@observer
export default class GreetingsCard extends Component<Props, State> {
	userName: string;
	headerHeight: number;
	constructor(props: Props, state: State) {
		super(props, state);
		this.state = {
			styles: getStyle(),
			contentHeight: 50
		};
		this.headerHeight = 0
	}

	_onHeaderLayout = ({ nativeEvent } : { nativeEvent : any}) => {
		this.headerHeight = nativeEvent.layout.height;
	};

	_onTextLayout = ({ nativeEvent } : { nativeEvent : any}) => {
		this.setState({
			contentHeight: nativeEvent.layout.height + this.headerHeight + 40
		})
	};

	renderGreetingsText() {
		const { styles } = this.state;
		const { newGoalsDataStore, profileDataStore } = this.props;
		const { selectedStudent } = profileDataStore;
		let studentName = selectedStudent && selectedStudent.name ? selectedStudent.name.split(' ')[0] : '';
		return (
			<View onLayout={this._onTextLayout} style={[{
				paddingLeft: 18,
				width: widthPercentage(90),
				justifyContent: 'center',
			}]}>
				<Text style={[styles.greetText, { marginBottom: 16 }]}>{studentName ? capitalizeWords(studentName) + '\'s progress chart' : ''}</Text>
				<Text style={[styles.greetMsg]}>{newGoalsDataStore.greetingMsg}</Text>
			</View>);
	}

	renderProfileImage() {
		const { styles } = this.state;
		const { selectedStudent } = this.props.profileDataStore;

		return <View style={styles.profileImage}>
			{selectedStudent && selectedStudent.image !== null ?
				<Image style={{ width: verticalScale(64), height: verticalScale(64), borderRadius: verticalScale(32) }} source={{ uri: selectedStudent.image }} resizeMode={'contain'} /> :
				<Image style={{ width: verticalScale(64), height: verticalScale(64) }} source={icons.PROFILE_ICON} resizeMode={'contain'} />
			}
			<View style={styles.badgeContainer}>
				<Image source={icons.HOME_PROFILE_BADGE} style={styles.badgeIcon} resizeMode='contain' />
			</View>
		</View>;
	}

	_onAddStudent = () => {
		this.props.onAddStudent()
	};

	renderAddStudent() {
		return (
			<TouchableWithoutFeedback onPress={() => this._onAddStudent()}>
				<Image style={{ width: verticalScale(34), height: verticalScale(34) }} source={icons.ADD_NEW_STUDENT} resizeMode={'contain'} />
			</TouchableWithoutFeedback>
		)
	}

	render() {
		const { styles, contentHeight } = this.state
		let contentWidth = isTablet() ? contentHeight / 0.60 : contentHeight / 0.75, containerHeight, containerWidth
		if (contentWidth < widthPercentage(100)) {
			containerWidth = widthPercentage(100);
			containerHeight = isTablet() ? widthPercentage(100) * 0.60 : widthPercentage(100) * 0.75
		} else {
			containerWidth = contentWidth
			containerHeight = contentHeight
		}
		return (
			<ImageBackground
				source={isTablet() ? icons.HOME_HEADER_TABLET : icons.HOME_HEADER}
				resizeMode='cover'
				imageStyle={{
					resizeMode: "cover",
					alignSelf: "flex-end",
					flexDirection: 'column',
				}}
				style={[styles.headerBackground, { width: containerWidth, height: containerHeight }]}>
					<View onLayout={this._onHeaderLayout} style={styles.profileContainer}>
						{this.renderProfileImage()}
						{this.renderAddStudent()}
					</View>
					{this.renderGreetingsText()}
			</ImageBackground >
		)
	}
}
