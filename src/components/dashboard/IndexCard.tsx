import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import { colors } from '../../config'
import { observer, inject } from 'mobx-react'
import { NewGoalsDataStore } from '../../store'
import ProgressBar from './components/ProgressBar'
import { isTablet } from '../../common'
import DashboardInfotip from './components/DashboardInfotip';

interface Props {
  indexId: string,
  containerStyle?: any,
  newGoalsDataStore?: NewGoalsDataStore
}
interface State {
  styles?: any
}

const indexCards = {
  achievement: {
    label: 'Achievement Index',
    info: 'Achievement Index shows the chances of success in your defined goal based on the performance in the recent tests.',
    imgSrc: require('../../../images/achievement-index.png')
  },
  sincerity: {
    label: 'Sincerity Index',
    info: 'Sincerity Index shows the level of effort you are putting in comparison to others who are preparing the same goal on myPAT',
    imgSrc: require('../../../images/sincerity-index.png')
  },
  improvement: {
    label: 'Improvement Index',
    info: 'Improvement Index is the measure of score improvements by you from one test to another in the recent tests',
    imgSrc: require('../../../images/improvement-index.png')
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    // height: 80
  },
  imageBg: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 100,
    height: '90%',
    opacity: 0.05
  },
  titleRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 22,
    width: '100%'
  },
  cardTitle: {
    fontSize: isTablet() ? 21 : 16,
    color: colors.HeaderColor
  },
  percentText: {
    fontSize: isTablet() ? 24 : 19,
    color: colors.HeaderColor
  },
  scoreText: {
    fontSize: isTablet() ? 21 : 16,
    marginBottom: 7,
    marginTop: 16
  }
})

@inject('newGoalsDataStore')
@observer
export class IndexCard extends Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle()
    }
  }

  renderIndexProgress = (progressData: any, color: string) => {
    return (
      <ProgressBar 
        progress={progressData.score} 
        color={color}
        progressData={progressData.score ? `${Math.round(progressData.score)}%` : '0%'} />
    )
  }

  render() {
    const { styles } = this.state
    const { indexId, containerStyle, newGoalsDataStore } = this.props
    const indexObj = indexCards[indexId];
    const { indexData } = newGoalsDataStore
    const data = indexData[indexId]
    let color = data && data.score > 60 ? '#ACD559' : data && data.score > 40 ? '#FBCF47' : '#FA6947'
    return (
      <View style={[containerStyle, styles.container]}>
        <View style={styles.imageBg}>
          <Image style={styles.image} source={indexObj.imgSrc} resizeMode='contain' />
        </View>
        <View style={styles.titleRow}>
          <Text style={styles.cardTitle}>{indexObj.label}</Text>
          {/* <Text style={styles.percentText}>{data ? `${Math.round(data.score)}%` : ''}</Text> */}
          <DashboardInfotip tooltipText={indexObj.info} />
        </View>
        {data ? this.renderIndexProgress(data, color) : null}
        {data ? <Text style={[styles.scoreText, { color: color }]}>{data.message}</Text> : null }
      </View>
    )
  }
}
