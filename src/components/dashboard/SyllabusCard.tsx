import React, { Component } from 'react'
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native'
import { dimens, colors } from '../../config'
import { NewGoalsDataStore } from '../../store'
import { inject, observer } from 'mobx-react'
// @ts-ignore
import Pie from 'react-native-pie';
import { isTablet } from '../../common'
import DashboardInfotip from './components/DashboardInfotip';

interface Props {
  newGoalsDataStore?: NewGoalsDataStore
  containerStyle?: any
}
interface State {
  styles?: any
}

const getStyle = () => StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  cardTitle: {
    fontSize: isTablet() ? 21 : 16,
    color: colors.ContentColor
  },
  cardBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
    paddingRight: 30,
    marginVertical: 20
  },
  innerContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  innerTextPercent: {
    backgroundColor: 'transparent',
    color: colors.HeaderColor,
    fontSize: isTablet() ? 45 : 36,
    textAlign: 'center'
  },
  innerText: {
    backgroundColor: 'transparent',
    color: colors.HeaderColor,
    fontSize: isTablet() ? 21 : 16,
    textAlign: 'center'
  },
  cardFooter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  footerText: {
    fontSize: isTablet() ? 21 : 16,
    color: colors.HeaderColor
  },
  conceptDetails: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  conceptsHorizontal: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: '100%'
  },
  conceptContainer: {
    margin: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1
  },
  conceptValue: {
    fontSize: isTablet() ? 30 : 24,
    fontWeight: '500',
    lineHeight: isTablet() ? 36 : 30
  },
  conceptLabel: {
    fontSize: isTablet() ? 15 : 12,
    color: colors.HeaderColor,
    opacity: 0.56
  }
});

@inject('newGoalsDataStore')
@observer
export class SyllabusCard extends Component<Props, State> {

  concepts = {
    strongConcepts: {
      color: '#ACD559',
      label: 'Strong Concepts'
    },
    averageConcepts: {
      color: '#FBCF47',
      label: 'Average Concepts'
    },
    weakConcepts: {
      color: '#FA6947',
      label: 'Weak Concepts'
    }
  };

  constructor(props: Props, state: State) {
    super(props, state);
    this.state = {
      styles: getStyle()
    }
  }

  practisedText = () => {
    const { newGoalsDataStore } = this.props;
    if (newGoalsDataStore.syllabusData) {
      const { totalConcepts, attemptedConcepts } = newGoalsDataStore.syllabusData;
      return `${attemptedConcepts} out of ${totalConcepts} concepts practiced`
    }
    return ''
  };

  formatData = () => {
    const { newGoalsDataStore } = this.props;
    let seriesData = [], colorsData = [],
      syllabusData = newGoalsDataStore.syllabusData;
    if(syllabusData.attemptedConcepts || syllabusData.attemptedConcepts === 0) {
      let percent = Math.round(syllabusData.attemptedConcepts * 100 / syllabusData.totalConcepts);
      seriesData.push(percent);
      colorsData.push('#2E93FB');
      seriesData.push(100 - percent);
      colorsData.push('#DAECFF');
    }
    return {
      seriesData,
      colorsData,
      leftConcepts: (syllabusData.totalConcepts - syllabusData.attemptedConcepts)
    }
  };

  renderConcept = (conceptKey) => {
    const { styles } = this.state
    let conceptObj = this.concepts[conceptKey]
    const { syllabusData } = this.props.newGoalsDataStore
    return (
      <View style={styles.conceptContainer}>
        <Text style={[styles.conceptValue, { color: conceptObj.color }]}>{syllabusData[conceptKey]}</Text>
        <Text style={styles.conceptLabel}>{conceptObj.label}</Text>
      </View>
    )
  }
  renderCardFooter() {
    const { styles } = this.state
    return (
      <View style={styles.cardFooter}>
        <Text style={styles.footerText}>{this.practisedText()}</Text>
      </View>
    )
  }

  renderTabletView() {
    const { styles } = this.state
    return (
      <View style={[styles.conceptDetails, { width: '60%'}]}>
        <View style={styles.conceptsHorizontal}>
          {this.renderConcept('strongConcepts')}
          {this.renderConcept('weakConcepts')}
        </View>
        {this.renderCardFooter()}
      </View>
    )
  }

  renderPhoneView() {
    const { styles } = this.state
    return (
      <View style={styles.conceptDetails}>
        {this.renderConcept('strongConcepts')}
        {this.renderConcept('weakConcepts')}
      </View>
    )
  }

  render() {
    const { styles } = this.state
    const { containerStyle, newGoalsDataStore } = this.props
    let isLoading = newGoalsDataStore.isSyllabusLoading
    const { seriesData, colorsData, leftConcepts } = this.formatData()
    const progressPercent = seriesData.length > 0 ? Math.floor(seriesData[0]) : 0;
    let circleRadius = isTablet() ? 75 : 60
    let innerRadius = circleRadius - 5
    
    return (
      <View style={[styles.container, containerStyle]}>
        <View style={styles.cardHeader}>
          <Text style={styles.cardTitle}>Syllabus Progress</Text>
          <DashboardInfotip tooltipText={'View the syllabus covered by your ward categorized as per the performance'} />
        </View>

        {isLoading ? <ActivityIndicator size='small' /> :
          <View style={styles.cardBody}>
            {seriesData.length > 0 ?
              <View style={styles.syllabusPie}>
                <Pie
                  radius={circleRadius}
                  innerRadius={circleRadius - 5}
                  series={seriesData}
                  colors={colorsData}
                  backgroundColor='#fff' />
                <View style={[styles.innerContainer, { maxWidth: circleRadius * 2, maxHeight: circleRadius * 2 }]}>
                  <Text style={[styles.innerTextPercent, { maxWidth: (innerRadius - 5) * 2, maxHeight: (innerRadius - 5) * 2 }]}>
                      {`${progressPercent}%`}
                  </Text>
                  <Text style={[styles.innerText, { maxWidth: (innerRadius - 5) * 2, maxHeight: (innerRadius - 5) * 2 }]}>
                      {`Complete`}
                  </Text>
                </View>
              </View>
              :
              null
            }
            {isTablet() ? this.renderTabletView() : this.renderPhoneView()}
          </View>
        }
        {isTablet() || isLoading ? null : this.renderCardFooter()}
      </View>
    )
  }
}
