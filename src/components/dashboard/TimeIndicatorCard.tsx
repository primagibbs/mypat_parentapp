import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import { dimens, colors } from '../../config'
import { NewGoalsDataStore } from '../../store'
import { inject, observer } from 'mobx-react'
import { isTablet } from '../../common'

interface Props {
  newGoalsDataStore?: NewGoalsDataStore
  containerStyle?: any,
  onLayout?: any
}
interface State {
  styles?: any
}

const getStyle = () => StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  imageWrapper: {
    marginRight: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    height: dimens.size40,
    width: dimens.size40
  },
  textWrapper: {
    flex: 1
  },
  titleText: {
    fontSize: isTablet() ? 24 : 19,
    color: '#2D7CD4',
    fontWeight: '500',
    marginBottom: 4
  },
  subtitleText: {
    color: colors.HeaderColor,
    fontSize: isTablet() ? 15 : 12,
    opacity: 0.54
  }
})

@inject('newGoalsDataStore')
@observer
export default class TimeIndicatorCard extends Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle()
    }
  }

  render() {
    const { styles } = this.state
    const { newGoalsDataStore } = this.props
    return (
        <View style={[styles.container, this.props.containerStyle]}>
          <View style={styles.imageWrapper}>
            <Image style={styles.imageStyle} resizeMode='contain' source={require('../../../images/time_left.png')} />
          </View>
          <View style={styles.textWrapper}>
            <Text style={styles.titleText}>{newGoalsDataStore.examTimeLeft}</Text>
            <Text style={styles.subtitleText}>{newGoalsDataStore.examDateText}</Text>
          </View>
        </View>
    )
  }
}
