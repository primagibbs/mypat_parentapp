import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableWithoutFeedback } from 'react-native'
import { dimens, colors } from '../../config'
import { widthPercentage, verticalScale, isTablet } from '../../common'
import { getRemainingTime, getTestTimeData } from '../../persistence/utils'

interface Props {
  item: any,
  containerStyle?: any,
  onViewSyllabus: any
}

const styles = StyleSheet.create({
  container: {
    width: widthPercentage(isTablet() ? 40 : 70),
    height: isTablet() ? 200: 160,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingVertical: isTablet() ? 34 : verticalScale(23),
	  paddingHorizontal: 10,
    margin: 4
  },
  headerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  headerLeft: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
    overflow: 'hidden'
  },
  statusContainer: {
    borderRadius: 2,
    paddingHorizontal: 8,
    paddingVertical: 6,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  statusText: {
    color: colors.White,
    fontSize: isTablet() ? 14 : 11
  },
  timeText: {
    color: colors.SubHeaderColor,
    fontSize: isTablet() ? 15 : 12,
    flex: 1
  },
  goalContainer: {
    height: isTablet() ? verticalScale(20) : verticalScale(20),
    paddingHorizontal: 5,
    borderWidth: 1,
    borderColor: 'rgba(153, 153, 153, 0.8)',
    borderRadius: isTablet() ? verticalScale(20)/2 : verticalScale(20)/2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  goalText: {
    fontSize: isTablet() ? 12 : 9,
    color: colors.ContentColor
  },
  testContainer: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1
  },
  testName: {
    fontSize: isTablet() ? 24 : 19,
    lineHeight: isTablet() ? 32 : 27,
    color: colors.SubHeaderColor
  },
  btnContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  attemptBtn: {
    flex: 1,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1D7DEA',
    borderRadius: 4
  },
  attemptText: {
    fontSize: isTablet() ? 21 : 17,
    fontWeight: '500',
    color: colors.White
  },
  disabled: {
    backgroundColor: '#DFDFDF'
  }
})

export class UpcomingTestCard extends Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state)
  }

  async componentDidMount() {
    const { item } = this.props
    let startTime = null
    let testId = item.id
    let assignmentId = item.assignmentId
    if (item.status === 'Past') {
    	assignmentId = undefined
    }
    const remainingTime = await getRemainingTime(testId, assignmentId)
    const testTimeData = await getTestTimeData(testId, assignmentId)

    if (testTimeData && testTimeData[0] !== undefined) {
      startTime = testTimeData[0].startTime
    }

    this.setState({
      localRemainingTime: remainingTime,
      testStartTime: startTime
    })
  }

  _onViewSyllabus = () => {
    this.props.onViewSyllabus(this.props.item)
  }

  render() {
    const { item, containerStyle } = this.props
    let isUpcoming = item.status === 'Upcoming'
	  let isPast = item.status === 'Past'

    return (
      <TouchableWithoutFeedback onPress={this._onViewSyllabus}>
        <View style={[containerStyle, styles.container]}>
          <View style={styles.testContainer}>
            <Text numberOfLines={2} style={styles.testName}>{item.test.name}</Text>
          </View>
          <View style={styles.headerRow}>
            <View style={styles.headerLeft}>
              <View style={[styles.statusContainer, {backgroundColor: isPast ? '#999999' : isUpcoming ? '#FF830F' : '#64D739'}]}>
                <Text style={styles.statusText}>{item.status}</Text>
              </View>
              <Text numberOfLines={1} style={styles.timeText}>{item.since}</Text>
            </View>
            <View>
              <View style={styles.goalContainer}>
                <Text style={styles.goalText}>{item.goalName}</Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
