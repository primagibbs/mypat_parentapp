import React, { Component } from 'react'
import { View, StyleSheet, Text, FlatList } from 'react-native'
import { dimens, colors } from '../../config'
import { widthPercentage, isTablet } from '../../common'
import commonStores from '../../common-library/store'
import { inject, observer } from 'mobx-react'
import stores, { NewDashboardStore } from '../../store'
import { UpcomingTestCard } from './UpcomingTestCard'
import { getUserId, getSelectedStudent } from '../../utils'
import { navigateSimple, showLoader, hideLoader } from '../../services'

interface Props {
	containerStyle?: any,
	newDashboardStore?: NewDashboardStore,
	navigation: any
}

interface State {
	styles?: any
}
const getStyle = () => StyleSheet.create({
	container: {
		alignItems: 'flex-start',
		justifyContent: 'space-between',
		width: '100%'
	},
	cardHeader: {
		fontSize: isTablet() ? 21 : 16,
		color: colors.HeaderColor,
		width: '100%'
	},
	dateText: {
		fontSize: isTablet() ? 15 : 12,
		color: colors.ContentColor,
		marginBottom: 13
	},
	scoreText: {
		fontSize: isTablet() ? 30 : 24,
		color: colors.SubHeaderColor,
		marginBottom: 3
	},
	scoreLabel: {
		fontSize: isTablet() ? 15 : 12,
		color: colors.HeaderColor,
		opacity: 0.56
	},
	detailsText: {
		fontSize: isTablet() ? 21 : 16,
		color: '#1D7DEA'
	}
})

@inject('newDashboardStore')
@observer
export class UpcomingTestsWidget extends Component<Props, State> {
	upcomingTestId = '000000000000000000000000'
	constructor(props: Props, state: State) {
		super(props, state)
		this.state = {
			styles: getStyle()
		}
	}

	getDisplayTime = (scheduleDate, status) => {
		let deltaSecs = (new Date()).getTime() - (new Date(scheduleDate)).getTime()
		if (status === 'Upcoming') {
			deltaSecs *= -1
		}
		let timeStr = ''
		if (deltaSecs > 30 * 24 * 3600000) {
			timeStr = Math.floor(deltaSecs / (30 * 24 * 3600000)) + ' months'
		} else if (deltaSecs > 24 * 3600000) {
			timeStr = Math.floor(deltaSecs / (24 * 3600000)) + ' days'
		} else if (deltaSecs > 3600000) {
			timeStr = Math.floor(deltaSecs / 3600000) + ' hrs'
		} else if (deltaSecs > 60000) {
			timeStr = Math.floor(deltaSecs / 60000) + ' mins'
		} else {
			timeStr = Math.floor(deltaSecs / 1000) + ' secs'
		}
		switch (status) {
			case 'Upcoming': {
				let scheduledDate = new Date(scheduleDate)
				timeStr = scheduledDate.toDateString()
				break
			}
			case 'Live': {
				timeStr = 'Since ' + timeStr
				break
			}
			case 'Past': {
				timeStr = timeStr + ' ago'
				break
			}
			default:
				break
		}
		return timeStr
	}

	formatData = (upcomingTests) => {
		return [...upcomingTests].map((item) => {
			let exam = Object.assign({}, item, item.test)
			exam.syllabus = {}
			exam.syllabus.name = item.test.name
			exam.name = item.test.name
			exam.testName = item.test.name
			// tslint:disable-next-line:triple-equals
			if (item.packageBought == false)
				exam.syllabus.syllabusData = item.syllabus.text
			else
				exam.syllabus.syllabusData = item.syllabus
			exam.syllabus.testCode = item.test.testCode
			exam.syllabus.testType = item.test.type
			if (new Date(item.scheduleDate).getTime() > new Date().getTime() || item.test.id === this.upcomingTestId) {
				exam.status = 'Upcoming'
				exam.testPriority = 1
			} else {
				exam.status = item.alreadyAttempted ? 'Past' : 'Live'
				exam.testPriority = item.alreadyAttempted ? -1 : 0
			}
			if (item.expired)
				exam.status = 'Past'
			exam.since = this.getDisplayTime(item.scheduleDate, exam.status)
			exam.uniqueKey =  '' + exam.packageId + exam.id + exam.name + exam.scheduleDate
			return exam
		})
	}

	viewSyllabus = (item) => {
		// should open Syllabus pop up
		const { navigation } = this.props
		navigateSimple(navigation, 'SyllabusPage', {
			test: item
		})
	}

	renderItem = (item, isLastCard = false) => {
		const { containerStyle } = this.props
		return (
			<UpcomingTestCard
				key={item.uniqueKey}
				item={item}
				onViewSyllabus={this.viewSyllabus}
				containerStyle={[containerStyle, !isLastCard ? { marginRight: 20 } : null ]} />
		)
	}

	renderEmptyCard = () => {
		const { styles } = this.state
		const { containerStyle } = this.props
		return (
		<View style={[containerStyle, {
			width: widthPercentage(90),
			height: 120,
			alignItems: 'center',
			justifyContent: 'center'
		}]}>
			<Text style={[styles.detailsText, { color: colors.SubHeaderColor } ]}>No upcoming tests</Text>
		</View>
		)
	}

	render() {
		const { styles } = this.state
		const { newDashboardStore } = this.props
		let formattedData = this.formatData(newDashboardStore.upcomingTests)
		return (
			<View style={styles.container}>
				<Text style={styles.cardHeader}>Live and Upcoming Tests</Text>
				<FlatList
					horizontal
					showsHorizontalScrollIndicator={false}
					style={{ marginTop: 15 }}
					ListEmptyComponent={this.renderEmptyCard()}
					data={formattedData}
					renderItem={({ item, index }) => this.renderItem(item, index === formattedData.length - 1)}
				/>
			</View>
		)
	}
}
