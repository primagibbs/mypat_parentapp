import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { inject, observer } from 'mobx-react';
import { colors, dimens } from '../../../config';
import Tooltip from 'rn-tooltip'
import { isTablet, verticalScale } from '../../../common';

interface Props {
	tooltipText: string
}
interface State {
  styles?: any,
  contentHeight: number
}

export default class DashboardInfotip extends Component<Props, State> {
  
  constructor(props: Props) {
    super(props)
    this.state = {
	  styles: getStyle(),
	  contentHeight: 70
    };
  }
  
  _onLayout = ({ nativeEvent } : { nativeEvent : any }) => {
	  this.setState({
		contentHeight: nativeEvent.layout.height
	  })
  }

  render = () => {
	const { tooltipText } = this.props;
	const { styles, contentHeight } = this.state;
	return( 
		<Tooltip
			containerStyle={styles.tipContainer}
			backgroundColor={'#333333'}
			withOverlay={false}
			height={ contentHeight + 20 }
			width={verticalScale(200)}
			popover={<Text onLayout={this._onLayout} style={styles.textStyle}>{tooltipText}</Text>}>
			<View style={{width: verticalScale(20), height: verticalScale(20), justifyContent: 'center', alignItems: 'center'}}>
				<Image source={require('../../../../images/info_icon.png')} style={styles.infoIcon} resizeMode='contain' />
			</View>
		</Tooltip>
		);

  }

}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	infoIcon: {
		width: isTablet() ? verticalScale(12) : verticalScale(16),
		height: isTablet() ? verticalScale(12) : verticalScale(16),
		padding: 4
	},
	tipContainer: {
		borderRadius: 4,
		paddingVertical: 10,
		paddingHorizontal: 12
	},
	textStyle: {
		fontSize: isTablet() ? verticalScale(8) : verticalScale(12),
		lineHeight: isTablet() ? verticalScale(12) : verticalScale(16),
		color: colors.White
	}

});
