import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { dimens, colors } from '../../../config'
import { widthPercentage, isTablet } from '../../../common'

interface Props {
  items: any[],
  onSelect: any
}

const screenHeight = Dimensions.get('screen').height

const styles = StyleSheet.create({
  goalsWrapper: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: colors.White,
    maxHeight: screenHeight * .4,
    width: widthPercentage(100)
  },
  goalRow: {
    paddingVertical: dimens.size16,
    // paddingLeft: dimens.size6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#C2D1DD'
  },
  goaltitle: {
    fontSize: isTablet() ? 25 : 20,
    color: colors.ContentColor
  },
  activeText: {
    color: '#1D7DEA',
    fontWeight: 'bold'
  }
});

export default class FilterSelectorModal extends Component<Props> {

  render() {
    const { items, onSelect } = this.props
    return (
      <TouchableWithoutFeedback>
        <View style={styles.goalsWrapper}>
          <ScrollView>
            {items.map(({id, title, selected}, i) => (
              <View key={id || i}>
                <TouchableWithoutFeedback onPress={() => onSelect({id, title})}>
                  <View style={styles.goalRow}>
                    <Text style={[styles.goaltitle, selected ? styles.activeText : {}]}>
                      {title}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            ))}
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
