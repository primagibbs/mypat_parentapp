import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { colors } from '../../../config'
import { verticalScale, isTablet } from '../../../common'

interface Props {
  color: string,
  progress: string,
  containerStyle?: any,
  progressData?: string
}

const styles = StyleSheet.create({
  progressContainer: {
    width: '100%',
    height: 4,
    borderRadius: 8,
    backgroundColor: '#DBECFF'
  },
  progressBar: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0
  },
  progressData: {
    position: 'absolute',
    top: isTablet() ? -22 : -15,
    bottom:  isTablet() ? -22 : -15,
    right: 0,
    width: isTablet() ? 44: 30,
    height: isTablet() ? 44: 30,
    backgroundColor: colors.White,
    borderRadius: isTablet() ? 22: 15,
    borderWidth: 2,
    borderColor: '#666666',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progressText:{
    fontSize: isTablet() ?  14 :  10,
    color: '#333333',
    borderColor: colors.Yellow,
    // fontWeight: 'normal'
  }
})

export default class ProgressBar extends Component<Props> {

  render() {
    const { color, progress, progressData, containerStyle } = this.props
    const intProgress = Math.round(parseFloat(progress))
    const circleRadiusOffset = isTablet() ? -22 : -15
    let circleAdditionalStyle =  intProgress > 50 ? 
                                  { right: (100-intProgress) + '%', borderColor: color, marginLeft:  circleRadiusOffset } : 
                                  { left:  intProgress + '%', borderColor: color, marginLeft:  circleRadiusOffset }
    if(intProgress < 5){
      circleAdditionalStyle.marginLeft = 0
    }
    return (
      <View style={[styles.progressContainer, containerStyle]}>
        <View style={[styles.progressBar, { width: progress + '%', backgroundColor: color }]} />
        <View style={[styles.progressData, circleAdditionalStyle]}>
          <Text style={styles.progressText} >
              {progressData}
          </Text>
        </View>
      </View>
    )
  }
}
