import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableWithoutFeedback } from 'react-native'
import { dimens } from '../../../config';
import { showModal } from '../../../services';
import FilterSelectorModal from './FilterSelectorModal';

interface Props {
	selectedFilter: string,
	items: any[],
	onSelect: any,
	color?: string
}
interface State {
  styles?: any
  arrowUp: boolean
}

export class SelectorFilter extends Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
	  styles: getStyle(),
	  arrowUp: false
    }
  }

  goalDropDownClicked() {
    this.setState({
      arrowUp: true
    });
    showModal(
      <FilterSelectorModal items={this.props.items} onSelect={this.props.onSelect} />,
      {
        positioning: 'bottom',
        animationType: 'slide',
        hideDialogOnTouchOutside: true,
        onModalClose: () => {
          this.setState({
            arrowUp: false
          });
        }
      }
    );
  }

  renderArrow() {
	  const { color } = this.props;
    const { styles, arrowUp } = this.state;
    if (!arrowUp) {
      return <View style={[styles.triangle_Down, { borderTopColor: color|| '#666666'}]} />;
    } else {
      return <View style={[styles.triangle_Up, { borderBottomColor: color|| '#666666'}]} />;
    }
  }

  render() {
    const { styles } = this.state;
    const { selectedFilter, color } = this.props;
    return (
      <TouchableWithoutFeedback onPress={() => this.goalDropDownClicked()}>
        <View style={styles.container}>
          <Text style={[styles.titleText, { color: color || '#666666' }]}>{selectedFilter}</Text>
          {this.renderArrow()}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10
  },
  titleText: {
    fontSize: dimens.size15,
    fontWeight: '500'
  },
  triangle_Down: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 10,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderTopColor: '#666666',
    marginLeft: 10,
    marginTop: 3
  },
  triangle_Up: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 10,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#666666',
    marginLeft: 10,
    marginTop: 3
  },
});
