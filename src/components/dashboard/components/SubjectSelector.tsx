import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { hideModal } from '../../../services';
import { SelectorFilter } from '../components/SelectorFilter';
import { ActivityCardDataStore } from '../../../store';

interface Props {
	activityCardDataStore?: ActivityCardDataStore
	onSelect: any
}
interface State {
	selectedSubject: any
}

@inject('activityCardDataStore')
@observer
export class SubjectSelector extends Component<Props, State> {

  constructor(props: Props, state: State) {
	super(props, state);
	this.state = {
		selectedSubject: props.activityCardDataStore.subjects[0]
	}
  }

  formatData = (data) => {
	const { selectedSubject } = this.state;
    return data.map((sub) => ({
      id: sub.id,
      title: sub.label,
      selected: selectedSubject.id === sub.id
    }));
  }

  _setSubject = ({id}) => {
    const { activityCardDataStore } = this.props;
    let selectedArr = activityCardDataStore.subjects.filter((sub) => sub.id === id);
    if(selectedArr && selectedArr.length > 0) {
		this.setState({
			selectedSubject: selectedArr[0]
		}, () => {
			this.props.onSelect && this.props.onSelect(selectedArr[0].id);
		});
    }
    hideModal();
  };

  render() {
	const { activityCardDataStore } = this.props;
	const { selectedSubject } = this.state;
    return (
      <SelectorFilter
        items={this.formatData(activityCardDataStore.subjects)}
        selectedFilter={selectedSubject? selectedSubject.label : ''}
        onSelect={this._setSubject}
        color={'#fff'}
        />
      );
  }
}
