import React, { Component } from 'react';
import {View, Text, TouchableWithoutFeedback, ScrollView, StyleSheet, Image, Dimensions} from 'react-native';
import { dimens, colors } from '../../../config';
import { inject, observer } from 'mobx-react';
import {heightPercentage, icons, isPortrait, verticalScale, widthPercentage} from '../../../common';
import { hideModal, navigateSimple } from '../../../services';
import { ProfileDataStore } from '../../../store';
import { getStudents, setSelectedStudent, getSelectedStudent } from '../../../utils';

interface Props {
	profileDataStore?: ProfileDataStore,
	navigation?: any
}

interface State {
	isValid: boolean
	isChanged: boolean,
	students: any
}

@inject('profileDataStore')
@observer
export default class AddStudent extends Component<Props, State> {

	constructor(props: any) {
		super(props);
		this.state = {
			isValid: true,
			isChanged: false,
			students: [],
		};
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);

		const students = await getStudents();
		this.setState({students});
	}

	renderHeader = () => {
		return (
			<View style={styles.headerContainer}>
				<Text style={styles.headerText}>Student profiles</Text>
			</View>
		)
	};

	selectStudent = async (studentId: any) => {
		const { profileDataStore } = this.props;
		await setSelectedStudent(studentId);
		const student = await getSelectedStudent();
		profileDataStore.setSelectedStudent(student);
		hideModal();
	};

	_onAddStudent = async () => {
		hideModal();
		navigateSimple(this.props.navigation, 'AddProfile1Page');
	};

	renderProfileImage(image: any) {
		return <View>
			{image !== null ?
				<Image style={{ width: verticalScale(62), height: verticalScale(62), borderRadius: verticalScale(32) }} source={{ uri: image }} resizeMode={'contain'} /> :
				<Image style={{ width: verticalScale(62), height: verticalScale(62) }} source={icons.PROFILE_ICON} resizeMode={'contain'} />
			}
		</View>;
	}

	render() {
		const { students } = this.state;
		return (
			<TouchableWithoutFeedback>
				<View style={styles.wrapper}>
					{this.renderHeader()}
					<ScrollView>
						<View style={styles.inputContainer}>
							{
								students.length > 0 && students.map((student: any, index: number) => {
									return <TouchableWithoutFeedback key={index} onPress={() => this.selectStudent(student.id)}>
										<View style={styles.gridItem} >
											{this.renderProfileImage(student.image)}
											<Text numberOfLines={1} style={{
												color: colors.HeaderColor,
												textAlign: 'center',
												marginTop: 10,
												textTransform: 'capitalize'}}>{student.name}</Text>
										</View>
									</TouchableWithoutFeedback>
								})
							}
							<TouchableWithoutFeedback onPress={() => this._onAddStudent()}>
								<View style={styles.gridItem}>
									<Image source={icons.ADD_STUDENT} style={{width: verticalScale(62), height: verticalScale(62)}} resizeMode={'contain'} />
									<Text style={{color: colors.HeaderColor, marginTop: 10, textAlign: 'center'}}>Add student</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</ScrollView>
					<TouchableWithoutFeedback onPress={hideModal}>
						<View style={styles.closeIcon}>
							<Text style={styles.closeText}>Done</Text>
						</View>
					</TouchableWithoutFeedback>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const screenHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
	wrapper: {
		borderRadius: 6,
		backgroundColor: colors.White,
		height: screenHeight*.85,
		width: widthPercentage(90)
	},
	headerContainer: {
		marginTop: 50,
		marginBottom: 50,
		alignItems: 'center'
	},
	closeIcon: {
		borderTopWidth: 1,
		borderTopColor: '#CBCBCB',
		justifyContent: 'center',
		alignItems: 'center'
	},
	closeText: {
		fontSize: verticalScale(13),
		lineHeight: verticalScale(13),
		textAlign: 'center',
		color: colors.ParentBlue,
		paddingTop: 20,
		paddingBottom: 20
	},
	headerText: {
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(dimens.size24) : widthPercentage(3.4),
		color: colors.HeaderColor,
		justifyContent: 'center',
		alignItems: 'center'
	},
	headerLink: {
		fontSize: isPortrait() ? verticalScale(dimens.size13) : widthPercentage(2.2),
		lineHeight: 13,
		color: '#999999',
		marginRight: 10,
	},
	inputContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		flexWrap: 'wrap',
		alignItems: 'center',
		paddingHorizontal: 16
	},
	gridItem: {
		flexBasis: '50%',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 12,
		paddingBottom: 12
	},
	dashboardInput: {
		color: colors.MettalicGrey,
		height: 40,
		width: '100%',
	},
	deleteText: {
		fontSize: dimens.size13,
		lineHeight: 14,
		color: '#FA4A4A',
		marginTop: 20,
	}
});
