import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { dimens, colors } from '../../../config'
import { hideModal } from '../../../services'
import {heightPercentage, widthPercentage, isTablet, verticalScale, isPortrait, icons} from '../../../common'
import { inject, observer } from 'mobx-react'
import { ProfileDataStore } from "../../../store";
import {getSelectedStudent, setSelectedStudent} from "../../../utils";

interface Props {
  student: any,
	profileDataStore?: ProfileDataStore,
}

interface State {
}

@observer
@inject('profileDataStore')
export default class DeleteStudentModal extends Component<Props, State> {

  _cancelAction = () => {
    hideModal()
  };

	selectStudent = async () => {
		const { profileDataStore } = this.props;
		const selectedStudent = await getSelectedStudent();
		await setSelectedStudent(selectedStudent.id);
		profileDataStore.setSelectedStudent(selectedStudent);
	};

  _removeAction = async () => {
		const { profileDataStore, student } = this.props;
		await profileDataStore.deleteStudent(student.id);
		await this.selectStudent();
    hideModal()
  };

  render() {
    return (
      <TouchableWithoutFeedback>
        <View style={styles.wrapper}>
          <ScrollView>
            <TouchableWithoutFeedback>
              <View style={styles.unlockContainer}>
                <View style={styles.assetContainer}>
                  <Image source={icons.DELETE_STUDENT_POPUP} resizeMode='contain' style={styles.asset} />
                </View>
                <View style={styles.titleContainer}>
                  <Text style={styles.cardTitle}>Delete?</Text>
                  <Text style={styles.cardSubtitle}>{`Are you sure you want to remove this student?`}</Text>
                  <Text style={[styles.cardSubtitle, { marginTop: 0 }]}>{`This action cannot be undone.`}</Text>
                </View>
                <View style={styles.unlockActions}>
                  <TouchableOpacity onPress={this._removeAction}>
                    <View style={styles.actionContainer}>
                      <Text style={styles.actionText}>Yes</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this._cancelAction}>
                    <View style={[styles.actionContainer, styles.primaryActionContainer]}>
                      <Text style={[styles.actionText, styles.primaryActionText]}>No</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}



const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 6,
    backgroundColor: colors.White,
    maxHeight: heightPercentage(50),
    width: widthPercentage(90)
  },
  unlockContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
    paddingHorizontal: 8,
    width: '100%'
  },
  assetContainer: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginBottom: 12
  },
  asset: {
    width: 50,
    height: 50
  },
  titleContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30
  },
  cardTitle: {
		fontSize: isPortrait() ? verticalScale(18) : widthPercentage(2.6),
    color: colors.ParentBlue,
  },
  cardSubtitle: {
    color: colors.ContentColor,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
    lineHeight: 22,
    textAlign: 'center',
    marginTop: 9
  },
  unlockActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10
  },
  actionContainer: {
  	width: verticalScale(80),
		height: verticalScale(36),
    paddingVertical: 9,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginHorizontal: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.ContentColor
  },
  primaryActionContainer: {
    backgroundColor: '#1D7DEA',
    borderColor: '#1D7DEA'
  },
  actionText: {
		fontSize: verticalScale(14),
    opacity: 0.89,
    color: colors.SubHeaderColor,
    fontWeight: 'bold'
  },
  primaryActionText: {
    color: colors.White
  }
});
