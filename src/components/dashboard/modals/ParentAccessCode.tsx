import React, { Component } from 'react';
import {View, Text, TouchableWithoutFeedback, StyleSheet, Image, ScrollView, TouchableOpacity} from 'react-native';
import { dimens, colors } from '../../../config';
import { hideModal } from '../../../services';
import {heightPercentage, icons, isPortrait, verticalScale, widthPercentage} from '../../../common';

interface Props {
}

interface State {
}

export default class ParentAccessCode extends Component<Props, State> {

	constructor(props: any) {
		super(props);
	}

	render() {
		return (
			<View style={styles.wrapper}>
				<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
					<TouchableOpacity>
						<TouchableWithoutFeedback>
							<View style={styles.innerContainer}>
								<View style={styles.headerContainer}>
									<Text numberOfLines={1} style={styles.headerText}>Where to find Parent Access Code?</Text>
								</View>
								<View style={styles.actionContainer}>
									<View style={styles.web}>
										<View>
											<Text style={{
												color: colors.SubHeaderColor,
												fontWeight: '500',
												paddingLeft: verticalScale(10),
												paddingRight: verticalScale(10),
												marginBottom: verticalScale(17),
												fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)}}>In myPAT website</Text>
											<Text style={{
												color: colors.SubHeaderColor,
												marginBottom: verticalScale(6),
												paddingLeft: verticalScale(10),
												paddingRight: verticalScale(10),
												fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2) }}>Step 1: Go to myPAT.in and log into the student's account.</Text>
											<Text style={{
												color: colors.SubHeaderColor,
												paddingLeft: verticalScale(10),
												paddingRight: verticalScale(10),
												fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2) }}>Step 2: Click on the profile icon on the top right screen of the dashboard and click on the 'Parent Access Code'.</Text>
										</View>
										<View style={styles.imageContainer}>
											<Image source={icons.ACCESS_CODE_WEB} style={styles.image} resizeMode={'contain'} />
										</View>
									</View>

									<View style={styles.app}>
										<Text style={{
											color: colors.SubHeaderColor,
											fontWeight: '500',
											paddingLeft: verticalScale(10),
											paddingRight: verticalScale(10),
											marginBottom: verticalScale(17),
											fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)}}>In myPAT app</Text>
										<Text style={{
											color: colors.SubHeaderColor,
											paddingLeft: verticalScale(10),
											paddingRight: verticalScale(10),
											marginBottom: verticalScale(6),
											fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2) }}>Step 1: Go to the myPAT and log into the student's account.</Text>
										<Text style={{
											color: colors.SubHeaderColor,
											paddingLeft: verticalScale(10),
											paddingRight: verticalScale(10),
											fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2) }}>Step 2: Click on the 'More' menu item and then tap on the 'Parent Access Code'.</Text>
										<View style={styles.imageContainer}>
											<Image source={icons.ACCESS_CODE_APP} style={styles.image} resizeMode={'contain'} />
										</View>
									</View>
								</View>
							</View>
						</TouchableWithoutFeedback>
					</TouchableOpacity>
				</ScrollView>
				<TouchableWithoutFeedback onPress={hideModal}>
					<View style={styles.closeIcon}>
						<Text style={styles.closeText}>Done</Text>
					</View>
				</TouchableWithoutFeedback>
			</View>

		);
	}
}

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		borderRadius: 6,
		marginTop: 50,
		marginBottom: 50,
		backgroundColor: colors.White,
		width: widthPercentage(90),
		height: '100%'
	},
	innerContainer: {
		paddingLeft: verticalScale(20),
		paddingRight: verticalScale(20),
	},
	headerContainer: {
		marginTop: verticalScale(30),
		marginBottom: verticalScale(16)
	},
	headerText: {
		fontSize: isPortrait() ? verticalScale(dimens.size16) : widthPercentage(2.4),
		color: colors.HeaderColor
	},
	actionContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	imageContainer: {
		marginTop: verticalScale(10)
	},
	image: {
		width: widthPercentage(80),
		height: widthPercentage(80)
	},
	web: {
		marginBottom: verticalScale(55)
	},
	app: {
		marginBottom: 55,
		paddingLeft: verticalScale(10),
		paddingRight: verticalScale(10)
	},
	closeIcon: {
		borderTopWidth: 1,
		borderTopColor: '#CBCBCB',
		justifyContent: 'center',
		alignItems: 'center'
	},
	closeText: {
		fontSize: isPortrait() ? verticalScale(dimens.size13) : widthPercentage(2.3),
		textAlign: 'center',
		color: colors.ParentBlue,
		paddingTop: 15,
		paddingBottom: 15
	}
});
