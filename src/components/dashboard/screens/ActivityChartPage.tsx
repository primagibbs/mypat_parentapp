import React, { Component } from 'react'
import {
	View,
	Text,
	Dimensions,
	TouchableOpacity,
	Image,
	StyleSheet,
	ScrollView,
	TouchableWithoutFeedback,
	BackHandler,
	Platform,
	ImageBackground, SafeAreaView
} from 'react-native'
// @ts-ignore
import Navigation from 'react-navigation'
import { inject, observer } from 'mobx-react'
import { goBack } from '../../../services'
import {widthPercentage, isTablet, icons, verticalScale} from '../../../common'
import { colors } from '../../../config'
import { ActivityCardDataStore } from '../../../store'
import { SubjectSelector } from '../components/SubjectSelector'
import Tooltip from 'rn-tooltip'
import {getSelectedStudent} from "../../../utils";
import background from '../../../../assets/fusioncharts/vendors/fc-timeseries/src/viz/timeseries/factories/background'

interface Props {
  navigation: Navigation
  activityCardDataStore?: ActivityCardDataStore
}
interface State {
  styles?: any,
  selectedTab: any,
  selectedSubject: string,
	contentHeight: number
	selectedStudent: any;
}

const screenHeight = Dimensions.get('screen').height;

@inject('activityCardDataStore')
@observer
export default class ActivityChartPage extends Component<Props, State> {

  userId: string;
  libraryPath: any;

  constructor(props: Props) {
    super(props);
    this.state = {
      styles: getStyle(),
      selectedTab: props.activityCardDataStore.activityTabs[0],
      selectedSubject: props.activityCardDataStore.subjects[0].id,
			contentHeight: 70,
			selectedStudent: {}
    };
    this.libraryPath = Platform.select({
      // Specify fusioncharts.html file location
      android: {
        uri: 'file:///android_asset/fusioncharts.html'
      },
      ios: require('../../../../assets/fusioncharts.html')
    })
  }

  async componentDidMount() {
    Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
    BackHandler.addEventListener('hardwareBackPress', this._onBackPress);

		const student = await getSelectedStudent();
		this.setState({ selectedStudent: student });
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPress)
  }

  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  _onBackPress = () => {
    return goBack(this.props.navigation)
  };

  renderBackIcon() {
    const { styles } = this.state;
    return (
      <TouchableOpacity style={[styles.iconStyle, styles.spaciousIconStyle]} onPress={() => this._onBackPress()}>
        <Image
          style={[styles.iconStyle, { padding: 5 }]}
          source={require('../../../../images/arrow_white.png')}
        />
      </TouchableOpacity>
    )
  }

  renderHeaderTitle = () => {
    const { styles } = this.state;
    return (
      <Text style={styles.headerTestName}>Activity Graph</Text>
    )
  };

  _selectTab = (tab: any) => {
    this.setState({
      selectedTab: tab
    })
  };

  createDataSource = () => {
    const { selectedTab, selectedSubject } = this.state;
    const { activityCardDataStore } = this.props;
    const { activityData } = activityCardDataStore;

    return [...activityData].map((row) => {
			if (row.subjects[selectedSubject] && row.subjects[selectedSubject][selectedTab.id]) {
				return {
					testName: row.testName,
					user: Math.round(row.subjects[selectedSubject][selectedTab.id][`user${selectedTab.suffix}`]),
					avg: Math.round(row.subjects[selectedSubject][selectedTab.id][`avg${selectedTab.suffix}`]),
					topper: Math.round(row.subjects[selectedSubject][selectedTab.id][`topper${selectedTab.suffix}`])
				}
			} else {
				return {
					testName: row.testName,
					user: 0,
					avg: 0,
					topper: 0
				}
			}
		});
  };

  _onSubjectChange = (subjectId: string) => {
    this.setState({
      selectedSubject: subjectId
    })
  };

	_onLayout = ({ nativeEvent } : { nativeEvent : any }) => {
		this.setState({
			contentHeight: nativeEvent.layout.height
		})
	};

	getTableLabel = (label: string) => {
		console.log("Table label", label);
		if(label == "Speed"){
			return  `${label} (Questions/10 mins)`	
		}
		return  `${label} (in %)`
	}

	_renderTable = (activityData: any) => {
		const { styles, contentHeight, selectedTab, selectedStudent } = this.state;


		return <ScrollView style={{ backgroundColor: colors.White, height: screenHeight, padding: 10 }}>
			<View style={{marginTop: verticalScale(10)}}>
				<Text style={{
					color: colors.HeaderColor,
					fontSize: verticalScale(13),
					lineHeight: verticalScale(15),
					marginBottom: verticalScale(10)}}>{this.getTableLabel(selectedTab.label)}</Text>
				<View style={{flex: 1, flexDirection: 'row', backgroundColor: "#E6E6E6", justifyContent: 'center', paddingVertical: 13, paddingLeft: 12 }}>
					<View style={{width: "53%", alignItems: 'flex-start'}}>
						<Text style={{color: colors.HeaderColor, fontSize: verticalScale(12)}}>Test</Text>
					</View>
					<View style={{width: "14%"}}><Text style={styles.arContextText} numberOfLines={2}>{selectedStudent.name}</Text></View>
					<View style={{width: "17%"}}><Text style={styles.arContextText}>{'Batch\nAvg.'}</Text></View>
					<View style={{width: "16%"}}><Text style={styles.arContextText}>{'Batch\nTopper'}</Text></View>
				</View>
				{
					activityData.map((item: any, index: number) => {
						return <View style={{
							backgroundColor: index % 2 === 0 ? "#F5F7FB" : colors.White,
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'center',
							paddingVertical: 12,
							paddingLeft: 12,
							borderColor: '#D3E2F7',
							borderWidth: 0.5}}>
							<View style={{width: "53%"}}>
								<Tooltip
									containerStyle={styles.tipContainer}
									backgroundColor={'#333333'}
									withOverlay={false}
									height={ contentHeight + 20 }
									width={verticalScale(200)}
									popover={<Text onLayout={this._onLayout} style={styles.textStyle}>{item.testName}</Text>}>
										<Text numberOfLines={1} style={{fontSize: verticalScale(12),
										color: colors.HeaderColor}}>{item.testName}</Text>
								</Tooltip>
							</View>
							<View style={{width: "14%", alignContent: "center"}}><Text style={styles.arContextText}>{item.user}</Text></View>
							<View style={{width: "17%", alignContent: "center"}}><Text style={styles.arContextText}>{item.avg}</Text></View>
							<View style={{width: "16%", alignContent: "center"}}><Text style={styles.arContextText}>{item.topper}</Text></View>
						</View>
					})
				}
			</View>
		</ScrollView>
  };

  render() {
    const { styles, selectedTab } = this.state;
    const { activityTabs } = this.props.activityCardDataStore;
    // if(cardsTabsIds.length === 0) {
    //   return this.renderActivityIndicator()
    // }
    let dataSource = this.createDataSource();
    return (
			<SafeAreaView style={{flex: 1}}>
				<View style={styles.container}>
					<ImageBackground style={styles.headerBackground} source={icons.HEADER_CUSTOM_ICON} />
					<View style={styles.titleRow}>
						<View style={styles.headerRow}>
							{this.renderBackIcon()}
							{this.renderHeaderTitle()}
						</View>
						<SubjectSelector onSelect={this._onSubjectChange} />
					</View>
					<ScrollView horizontal style={styles.tabularView}>
						{
							activityTabs.map(tab => {
								return (
									<TouchableWithoutFeedback onPress={() => this._selectTab(tab)}>
										<View style={styles.tabItem}>
											<Text style={[styles.tabText, selectedTab.id === tab.id ? styles.selectedTab : null]}>
												{tab.label ? tab.label.toUpperCase() : ''}
											</Text>
										</View>
									</TouchableWithoutFeedback>
								)
							})
						}
					</ScrollView>
					<ScrollView style={styles.pageBody} contentContainerStyle={{
						alignItems: 'center',
						justifyContent: 'center'}}>
						{dataSource.length > 0 ?
							this._renderTable(dataSource)
							:
							<View style={styles.emptyState}>
								<View style={styles.emptyAssetContainer}>
									<Image source={require('../../../../images/activity-chart-details-empty.png')}
												 resizeMode='contain' style={styles.emptyAsset} />
								</View>
								<Text style={styles.emptyStateTitle}>There is No Activity Graph</Text>
								<Text style={styles.emptyStateSubtitle}>Take a test and jumpstart your progress</Text>
							</View>

						}
					</ScrollView>
				</View>
			</SafeAreaView>
    )
  }
}

const getStyle = () => StyleSheet.create({
	iconStyle: {
		width: isTablet() ? 30 : 24,
		height: isTablet() ? 30 : 24
	},
	spaciousIconStyle: {
		width: 40,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		flex: 1
	},
	chartContainer: {
		flex: 1,
		height: '100%',
		width: '100%',
		alignItems: 'flex-start',
		padding: 10
	},
	headerBackground: {
		height: widthPercentage(100) * 0.3375,
		minHeight: 180,
		width: widthPercentage(100),
		position: 'absolute',
		backgroundColor: '#0B5FCB'
	},
	headerTestName: {
		fontSize: isTablet() ? 30 : 24,
		marginLeft: 10,
		color: colors.White,
		lineHeight: isTablet() ? 36 : 30,
		width: widthPercentage(100) - 70
	},
	titleRow: {
		height: 60,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	headerRow: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		width: '40%'
	},
	rowView: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	tabularView: {
		height: 60,
		marginLeft: 10,
	},
	tabItem: {
		// minWidth: isTablet() ? widthPercentage(25) : widthPercentage(25),
		alignItems: 'center',
		justifyContent: 'center',
		paddingRight: isTablet()? 36  : 20,
		// backgroundColor: colors.White
	},
	tabText: {
		fontSize: isTablet() ? 21 : 16,
		color: colors.White,
		opacity: 0.6
	},
	selectedTab: {
		opacity: 1
	},
	pageBody: {
		marginTop: 10,
		width: widthPercentage(100),
		height: screenHeight - 200,
		backgroundColor: '#0B5FCB'
	},
	loading: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.White
	},
	emptyState: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	emptyAssetContainer: {
		width: widthPercentage(60),
		height: widthPercentage(60),
		borderRadius: widthPercentage(30),
		backgroundColor: colors.White,
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: 30
	},
	emptyAsset: {
		width: '100%',
		height: '100%'
	},
	emptyStateTitle: {
		fontSize: isTablet() ? 24 : 19,
		lineHeight: 36,
		color: colors.White
	},
	emptyStateSubtitle: {
		fontSize: isTablet() ? 20 : 16,
		color: colors.White,
		opacity: 0.54
	},
	seriesTitle: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		height: 40,
		width: '100%'
	},
	seriesHeader: {
		marginHorizontal: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		maxWidth: '30%'
	},
	headerColor: {
		width: 8,
		height: 8,
		borderRadius: 4,
		marginRight: 3
	},
	seriesName: {
		fontSize: isTablet() ? 15 : 12,
		color: colors.White
	},
	arContextText: {
		fontSize: verticalScale(12),
		color: colors.HeaderColor,
		textAlign: "center"
	},
	tipContainer: {
		borderRadius: 4,
		paddingVertical: 10,
		paddingHorizontal: 12
	},
	textStyle: {
		fontSize: verticalScale(12),
		lineHeight: verticalScale(16),
		color: colors.White
	}
});
