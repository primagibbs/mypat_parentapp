import React, { Component } from 'react'
import { View, Text, Dimensions, StyleSheet,
	ActivityIndicator, BackHandler, ImageBackground, FlatList } from 'react-native'
import { inject, observer } from 'mobx-react'
import { goBack, navigateSimple } from '../../../services'
import { widthPercentage, isTablet, isPortrait, verticalScale, icons } from '../../../common'
import { colors } from '../../../config'
import stores, { NewGoalsDataStore } from '../../../store'
import { AssignmentCard } from '../AssignmentCard'
import { NoDataFoundComponent } from '../../others'

interface Props {
	navigation: any
	newGoalsDataStore?: NewGoalsDataStore
}
interface State {
	styles?: any
}

@inject('newGoalsDataStore')
@observer
export default class AssignmentsPage extends Component<Props, State> {

	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	async componentDidMount() {
		const { newGoalsDataStore } = this.props;
		newGoalsDataStore.resetAssignmentsData();

		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this._onBackPress);
		await this.fetchAssignments()
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this._onBackPress)
	}

	fetchAssignments = async () => {
		const { newGoalsDataStore } = this.props
		await newGoalsDataStore.fetchAssignmentsPaginated()
	};

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	_onBackPress = () => {
		return goBack(this.props.navigation)
	};

	renderHeaderTitle = () => {
		const { styles } = this.state;
		return (
			<View style={styles.heading}>
				<Text style={styles.headingTitle}>Past Assignments</Text>
				<Text style={styles.itemDesc}>Performance in all assignments</Text>
			</View>
		)
	};

	renderActivityIndicator = () => {
		const { styles } = this.state;
		return <View style={[styles.loading, { flex: 1 }]}>
			<ActivityIndicator size='large' />
		</View>
	};

	_goToResultScreen = (data: any) => {
		stores.testAttemptStore.setTestStats(
			data.title,
			data.testId,
			data.attemptId,
			stores.testAssignmentStore.context.goalID,
			data.assignmentId,
			data.testType,
			null,
			null
		);
		navigateSimple(this.props.navigation, 'ResultLandingScreenPage')
	};

	renderItem = (item: any, isLastIndex: boolean) => {
		const { styles } = this.state;
		return (
			<AssignmentCard item={item} containerStyle={[styles.itemContainer, isLastIndex ?
				{ marginBottom: verticalScale(25) } : null]} onPress={this._goToResultScreen} />
		)
	};


	renderNoNotification() {
		const { styles } = this.state;
		return (
			<View style={styles.noNotification}>
				<NoDataFoundComponent
					headingText= {'There is no history available'}
					descText= 'Assignment details will be available once the student attempts a test'
					iconName= {icons.NO_HISTORY} />
			</View>
		)
	}

	render() {
		const { styles } = this.state;
		let { assignments, loadingAssignments } = this.props.newGoalsDataStore;
		return (
			<View style={styles.container}>
				<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground}>
					<View style={styles.headerRow}>
						{this.renderHeaderTitle()}
					</View>
				</ImageBackground>
				<View style={styles.pageBody}>
					{(assignments.length > 0 || loadingAssignments) ?

						<View style={{flex: 1}}>
							<FlatList
								renderItem={({ item, index }) => this.renderItem(item, index === assignments.length - 1)}
								data={assignments}
								keyExtractor={(item, index) => item.testId + index}
								onEndReachedThreshold={0.1}
								onEndReached={this.fetchAssignments}
								/>
							{
								loadingAssignments ?
								this.renderActivityIndicator() :
								null
							}
						</View>
						:
						this.renderNoNotification()
					}
				</View>
			</View>
		)
	}
}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	iconStyle: {
		width: isTablet() ? 30 : 24,
		height: isTablet() ? 30 : 24
	},
	spaciousIconStyle: {
		width: 40,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		flex: 1
	},
	headerBackground: {
		flex: 1,
		height: widthPercentage(40),
		width: widthPercentage(100)
	},
	headingTitle: {
		fontSize: verticalScale(24),
		fontWeight: "500",
		color: colors.White,
		marginTop: 20,
		marginBottom: 10
	},
	sectionHeader: {
		fontSize: verticalScale(16),
		color: colors.SubHeaderColor,
		marginLeft: verticalScale(10),
		marginBottom: verticalScale(13),
		fontWeight: '500'
	},
	itemDesc: {
		fontSize: verticalScale(13),
		color: colors.White,
		marginBottom: 25
	},
	headerRow: {
		flex: 1,
		marginLeft: verticalScale(10),
		marginBottom: 22
	},
	allFeeds: {
		width: '100%',
		height: '93%',
		paddingBottom: 10,
		marginTop: 20
	},
	pageBody: {
		flex: 1,
		position: 'absolute',
		top: verticalScale(100),
		left: 0,
		right: 0,
		bottom: 0
	},
	itemContainer: {
		width: widthPercentage(95),
		alignSelf: 'center',
		borderRadius: 4,
		marginBottom: 10,
		backgroundColor: colors.White,
		elevation: isTablet() ? 2 : 4,
		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		padding: 16
	},
	loading: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.White
	},
	noNotification: {
		position: 'absolute',
		top: 0,
		left: 10,
		right: 10,
		bottom: 0,
		backgroundColor: colors.White,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		shadowColor: 'rgba(0, 0, 0, 0.199926)',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		borderRadius: 6
	}
});
