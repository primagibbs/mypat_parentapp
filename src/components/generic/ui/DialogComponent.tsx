import React from 'react'
import { StyleSheet, View, Text, Image, TextInput, TouchableWithoutFeedback, TouchableOpacity }
  from 'react-native'
import { Navigation } from 'react-navigation'
import { Dialog } from 'react-native-simple-dialogs'

import { heightPercentage, widthPercentage, verticalScale, isPortrait, isTablet } from '../../../common'
import { Button } from '../../../components'
import { colors } from '../../../config'
import { FROM_PAGE_TYPE } from '../../../common'
import { isEmpty } from '../../../utils'

interface Props {
  showDialog: boolean
  onContinueClicked: () => void,
  titleText: string
  descriptionText: string,
  buttonLabel: string,
  imageUrl: any
  cancelButtonVisible: boolean,
  onCancelClicked: () => void,
  hideDialogOnTouchOutside: boolean
}

interface State {
  styles?: any
}

export class DialogComponent extends React.Component<Props, State> {

  constructor(props, state) {
    super(props, state)
    this.state = {
      styles: getStyles(isEmpty(this.props.titleText))
    }
  }

  renderDialog() {
    const { styles } = this.state
    return <Dialog
      visible={this.props.showDialog}
      contentStyle={[styles.dialog]}
      onTouchOutside={() => this.props.hideDialogOnTouchOutside ? this.props.onCancelClicked() : ''}
      animationType='fade'>
      <Image
        style={[styles.righticon]}
        source={this.props.imageUrl}
        resizeMode='contain' />
        {this.renderTitleText()}
        <View style={{ backgroundColor: colors.White, flexDirection: 'row', padding: widthPercentage(1) }}>
      <Text style={styles.descriptionText}>{this.props.descriptionText} </Text>
      </View>
      <View style={{ backgroundColor: colors.White, flexDirection: 'row', padding: widthPercentage(1) }}>
        <Button
          style={styles.LoginButton}
          textStyle={styles.textSampleStyle}
          onPress={() => this.props.onContinueClicked()} title={this.props.buttonLabel} />
        {this.renderCancelButton()}
      </View>
    </Dialog>
  }

  renderTitleText = () => !isEmpty(this.props.titleText) ? <Text style={this.state.styles.titleText}>{this.props.titleText} </Text> : undefined

  renderCancelButton() {
    const { styles } = this.state
    if (this.props.cancelButtonVisible) {
      return <Button
      style={styles.LoginButton}
      textStyle={styles.textSampleStyle}
        onPress={() => this.onCancelClicked()} title='Cancel' />
    }
    return null
  }

  onCancelClicked(): any {
    this.props.onCancelClicked()
  }

  render() {
    return (
      <View style={{ backgroundColor: colors.White }}>
        {this.renderDialog()}
      </View>
    )
  }
}

const getStyles = (isTitleTextEmpty) => StyleSheet.create({
  dialog: {
    backgroundColor: colors.White,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: isPortrait() ? verticalScale(20) : widthPercentage(5.2)
  },
  righticon: {
    height: 105,
    width: 105,
    marginTop: verticalScale(15)
  },
  LoginButton: {
    backgroundColor: 'rgb(33,150,243)',
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: verticalScale(20),
    width: widthPercentage(40),
    marginLeft: widthPercentage(1),
    marginRight: widthPercentage(1),
    height: isTablet() ? isPortrait() ? 55 : 55 : isPortrait() ? 45 : 45
  },
  textSampleStyle: {
    fontSize: isTablet() ? isPortrait() ? 25 : 25 : isPortrait() ? 20 : 20
  },
  titleText: {
    fontSize: isPortrait() ? verticalScale(18) : widthPercentage(3.2),
    fontFamily: 'HelveticaNeue',
    textAlign: 'center',
    backgroundColor: colors.White,
    fontWeight: 'bold',
    marginBottom: 10,
    lineHeight: isPortrait() ? verticalScale(18) : widthPercentage(3.2)
  },
  descriptionText: {
    fontSize: isPortrait() ? verticalScale(14) : widthPercentage(3.2),
    fontFamily: 'HelveticaNeue',
    textAlign: 'center',
    fontWeight: isTitleTextEmpty ? 'bold' : 'normal',
    backgroundColor: colors.White,
    lineHeight: isPortrait() ? verticalScale(18) : widthPercentage(3.2)
  }
})