import React from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View, processColor, Dimensions, SafeAreaView
} from 'react-native'
import { verticalScale, widthPercentage, isPortrait, isTablet } from '../../../common-library/common'
import {PieChart} from 'react-native-charts-wrapper'
import { colors } from '../../../config'
interface Props {
  graphDataValues?: GraphData, // Use Case: See dummy data formate in DummyDataComponent -function: renderGroupBarChart()
  xLabel?: string,
  yLabel?: string
}
interface XAxisData {
  label: string,
  xAxis: any[]
}
interface GraphData {
  yAxis: YAxisData[],
}
interface YAxisData {
  config: {}
  label: string,
  yAxis: {}
}
interface YAxis {
  yAxis: {},
  marker: {}
}
const DEFAULT_GRAPH_SETTINGS = {
  xAxis: {},
  yAxis: {
    right: {
      enabled: false,
      drawLabels: false,
      drawAxisLine: true,
      drawGridLines: false,
      axisLineColor: processColor('transparent'),
      textColor: processColor('white'),
      axisMaximum: 0,
      axisMinimum: 0
    },
    left: {
      axisLineColor: processColor('transparent'),
      gridColor: processColor('white'),
      labelCountForce: true,
      textColor: processColor('white'),
      drawAxisLine: true,
      axisMaximum: 100,
      axisMinimum: 0

    }
  },
  selectedEntry: '',
  data: {},
  legend: {},
  marker: {}
}
interface State {
  styles: any,
  data: any,
  xAxis: any,
  yAxis: any,
  legend: any,
  marker?: any,
  yAxisTitle: any,
  xAxisTitle: any,
  highlights: any
}

export class PieChartComponent extends React.Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      legend: {
        enabled: false
      },
      xAxis: DEFAULT_GRAPH_SETTINGS.xAxis,
      yAxis: DEFAULT_GRAPH_SETTINGS.yAxis,
      data: DEFAULT_GRAPH_SETTINGS.data,
      marker: {
        enabled: true,
        markerColor: processColor('#F0C0FF8C'),
        textColor: processColor('white'),
        markerFontSize: 14
      },
      yAxisTitle: 'NO. OF STUDENTS ->',
      xAxisTitle: 'MARKS ->',
      highlights: ''
    }
  }

  _orientationChangeListener = () => {
    this.setState({
      styles: getStyle()
    })
  }
  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener)
  }
  componentDidMount() {
    Dimensions.addEventListener('change', this._orientationChangeListener)
    this.setGraphData(this.props)
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.graphDataValues !== nextProps.graphDataValues) {
      this.setGraphData(nextProps)
    }
  }
  setGraphData(props) {
    const graphDataValues = props.graphDataValues
    const xLabel = props.xLabel
    const yLabel = props.yLabel
     let yDatasSetValues = []
     for (let value of graphDataValues.yAxis[0].yAxis) {
    yDatasSetValues.push({
      value: value,
      label: ''
    })
  }
    this.setState ({
        legend: {
          enabled: false,
          textSize: 15,
          orientation: 'HORIZONTAL',
          wordWrapEnabled: true
        },
        data: {
          dataSets: [{
            values: yDatasSetValues,
            label: '',
            config: graphDataValues.yAxis[0].config
          }]
        }
      })
  }
  handleSelect() {
      // TODO
  }
  render() {
    const { styles } = this.state
    return (
        <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <PieChart
            style={styles.chart}
            logEnabled={true}
            chartBackgroundColor={processColor('white')}
            data={this.state.data}
            chartDescription={{ text: '' }}
            legend={this.state.legend}
            animation={{ durationX: 1500 }}
            entryLabelColor={processColor(colors.Transparent)}
            entryLabelTextSize={20}
            drawEntryLabels={true}
            rotationEnabled={false}
            rotationAngle={45}
            usePercentValues={true}
            styledCenterText={{text: '', color: processColor('white'), size: 1}}
            centerTextRadiusPercent={0}
            holeRadius={0}
            holeColor={processColor('#f0f0f0')}
            transparentCircleRadius={1}
            transparentCircleColor={processColor('#f0f0f088')}
            maxAngle={360}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      </SafeAreaView>
    )
  }
}

const getStyle = () => StyleSheet.create({
  bgView: {
    height: isPortrait() ? widthPercentage(50) : widthPercentage(50),
    width: isPortrait() ? widthPercentage(100) : widthPercentage(100),
    paddingLeft: isPortrait() ? widthPercentage(5) : widthPercentage(4),
    marginTop: 10,
    position: 'relative'
  },
  container: {
    flex: 1,
    backgroundColor: '#429CC0'
  },
  chart: {
    flex: 1
  },
  yLableStyle: {
    position: 'absolute',
    marginLeft: 5,
    color: 'white',
    top: isTablet() ? isPortrait() ? widthPercentage(-28) : widthPercentage(-22) : isPortrait() ? widthPercentage(20) : widthPercentage(20),
    left: isTablet() ? isPortrait() ? widthPercentage(-11) : widthPercentage(-8) : isPortrait() ? widthPercentage(-13.5) : widthPercentage(-8),
    transform: [{ rotate: '-90deg' }]
  },
  xLableStyle: {
    color: 'white',
    marginTop: 10,
    width: widthPercentage(100),
    textAlign: 'center'
  }
})