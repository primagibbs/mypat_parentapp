import React, { Component } from 'react'
import {icons, widthPercentage, isTablet, verticalScale} from '../../common'
import {BackHandler, View, Image, Text, StyleSheet, Modal, Dimensions} from 'react-native';
// @ts-ignore
import { SafeAreaView } from 'react-navigation';
import { colors } from '../../config';
const { width, height } = Dimensions.get('window');
import {Button} from "../../common-library/components";
import { setAppTourComplete } from '../../utils'

interface Props {
	student?: any
}

interface State {
	styles?: any,
	modalVisible?: boolean,
	step?: number
}

export class AppTourComponent extends Component<Props, State> {

	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle(),
			modalVisible: true,
			step: 0
		}
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	_renderProfileImage() {
		const { styles } = this.state;
		const { student } = this.props;

		return <View style={styles.profileImage}>
			{student && student.image !== null ?
				<Image style={styles.studentPicture} source={{ uri: student.image }} resizeMode={'contain'} /> :
				<Image style={{ width: verticalScale(72), height: verticalScale(72) }} source={icons.PROFILE_ICON} resizeMode={'contain'} />
			}
		</View>;
	}

	_renderAddStudentIcon() {
		return <Image style={{
			width: verticalScale(34),
			height: verticalScale(34),
			alignSelf: 'flex-end'
		}} source={icons.ADD_NEW_STUDENT} resizeMode={'contain'} />
	}

	_renderStep2() {
		return <View style={{
			position: 'absolute',
			bottom: -50,
			left: -20
		}}>
			<View>
				<Image style={{
					width: verticalScale(142),
					height: verticalScale(145),
					left: 10,
					marginBottom: -10
				}} source={icons.TOUR_THREE} resizeMode={'contain'} />
				<Image style={{
					width: verticalScale(40),
					height: verticalScale(32),
					left: '52%'
				}} source={icons.TOUR_FEED} resizeMode={'contain'} />
			</View>
		</View>
	}

	_renderStep3() {
		return <View style={{
			position: 'absolute',
			bottom: -50,
			left: '28%'
		}}>
			<View>
				<Image style={{
					width: verticalScale(142),
					height: verticalScale(149),
					marginBottom: 21
				}} source={icons.TOUR_FOUR} resizeMode={'contain'} />
				<Image style={{
					width: verticalScale(40),
					height: verticalScale(32),
					left: '28	%'
				}} source={icons.TOUR_HISTORY} resizeMode={'contain'} />
			</View>
		</View>
	}

	async onNextPress() {
		const { step, modalVisible } = this.state;
		if (step < 3) {
			this.setState({ step: step+1 });
		} else {
			await this._onTourDone(!modalVisible);
		}
	}

	_onTourDone = async (visible: boolean) => {
		await setAppTourComplete();
		this.setState({modalVisible: visible});
	};

	render() {
		const { styles, modalVisible, step } = this.state;
		return <Modal
			animationType="fade"
			transparent={true}
			visible={this.state.modalVisible}
			onRequestClose={async () => {
				await this._onTourDone(!modalVisible);
			}}>
			<SafeAreaView style={{flex: 1}}>
				<View style={styles.overlay}>

					<View style={{height: '50%', paddingVertical: 20, paddingHorizontal: 15}}>
						{
							step === 0 && <>
								{this._renderProfileImage()}
								<Image style={{
									width: verticalScale(163),
									height: verticalScale(141),
									marginLeft: 40,
									marginTop: verticalScale(6)
								}} source={icons.TOUR_ONE} resizeMode={'contain'} />
							</>
						}
						{
							step === 1 && <>
								{this._renderAddStudentIcon()}
								<Image style={{
									width: verticalScale(212),
									height: verticalScale(214),
									marginRight: 15,
									marginTop: verticalScale(17),
									alignSelf: 'flex-end'
								}} source={icons.TOUR_TWO} resizeMode={'contain'} />
							</>
						}
					</View>

					<View style={styles.tourBottomView}>
						<View style={{flexGrow: 1}} />
						<View style={styles.tourButtonContainer}>
							{
								step === 0 && <Button
									style={styles.skipTourButton}
									title='Skip'
									textStyle={styles.skipButton}
									onPress={async () => {
										await this._onTourDone(!modalVisible);
									}}
								/>
							}
							{
								(step === 1 || step === 2 || step === 3) && <View style={{width: '48%'}} />
							}
							{
								step === 2 && this._renderStep2()
							}
							{
								step === 3 && this._renderStep3()
							}
							<Button
								style={styles.nextTourButton}
								title={step === 3 ? 'Done' : 'Next'}
								textStyle={styles.nextButton}
								onPress={async () => {
									await this.onNextPress();
								}}
							/>
						</View>
					</View>
				</View>
			</SafeAreaView>
		</Modal>
	}
}

const getStyle = () => StyleSheet.create({
	profileImage: {
		width: verticalScale(72),
		height: verticalScale(72),
		borderRadius: verticalScale(36),
		backgroundColor: 'rgba(255, 255, 255, 0.4)',
		justifyContent: 'center',
		alignItems: 'center'
	},
	studentPicture: {
		width: verticalScale(64),
		height: verticalScale(64),
		borderRadius: verticalScale(32)
	},
	overlay: {
		backgroundColor: 'rgba(51,51,51, 0.9)',
		width: width,
		height: '100%'
	},
	tourBottomView: {
		height: '50%'
	},
	tourButtonContainer: {
		flexDirection: 'row',
		marginHorizontal: verticalScale(24),
		justifyContent: 'space-between',
		marginBottom: 60
	},
	skipTourButton: {
		width: '48%',
		borderRadius: 4,
		fontSize: verticalScale(13),
		lineHeight: verticalScale(15),
		fontWeight: '500',
		paddingHorizontal: verticalScale(35),
		paddingVertical: verticalScale(10),
		borderColor: colors.White,
		backgroundColor: 'transparent',
		marginRight: 18,
		borderWidth: 1,
	},
	nextTourButton: {
		width: '48%',
		borderRadius: 4,
		fontSize: verticalScale(13),
		lineHeight: verticalScale(15),
		fontWeight: '500',
		paddingHorizontal: verticalScale(35),
		paddingVertical: verticalScale(10),
		backgroundColor: colors.White
	},
	skipButton: {
		fontSize: verticalScale(13),
		fontWeight: '500',
		borderRadius: 4,
		textAlign: 'center',
		color: colors.White
	},
	nextButton: {
		fontSize: verticalScale(13),
		fontWeight: '500',
		borderRadius: 4,
		textAlign: 'center',
		color: colors.ParentBlue
	}
});
