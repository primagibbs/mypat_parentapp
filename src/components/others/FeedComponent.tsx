import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
	AppState,
	ScrollView,
	TouchableWithoutFeedback
} from 'react-native';
// @ts-ignore
import Navigation from 'react-navigation';
import { observer, inject } from 'mobx-react';
import moment from 'moment'
import { colors, dimens } from '../../config';
import {
	widthPercentage,
	isPortrait,
	TAB_TYPE,
	TYPE,
	TAB_BAR_PAGE,
	icons,
	isTablet,
	heightPercentage,
	verticalScale
} from '../../common';
import stores, { FeedStore, NetworkDataStore } from '../../store';
import commonStores from '../../common-library/store';
import { navigateSimple } from '../../services';
import { NoDataFoundComponent } from './NoDataFoundComponent';
import { LoadingPlaceholder } from '../LoadingPlaceholder';

interface Props {
	// @ts-ignore
	navigation?: Navigation,
	feedList?: any,
	feedStore?: FeedStore
	networkDataStore?: NetworkDataStore
}

interface State {
	styles?: any
}

@inject('feedStore', 'networkDataStore')
@observer
export class FeedComponent extends Component<Props, State> {
	courseId: any;

	constructor(props: Props, state: State) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	async componentDidMount() {
		const { feedStore, navigation } = this.props;
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));

		feedStore.setNavigationObject(navigation);
		await feedStore.getFeedList();
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	renderNoFeed() {
		const { styles } = this.state;
		return (
			<View style={styles.noFeed}>
				<NoDataFoundComponent
					headingText= {'There are no feeds available'}
					descText= 'The feed will be added soon as new blogs/ articles are available'
					iconName= {icons.NO_FEED}/>
			</View>
		)
	}

	renderFeedIcon(image: string) {
		const { styles } = this.state;
		if (image) {
			return <Image
				style={styles.profileIcon}
				source={{uri: image}}
			/>
		} else {
			return <Image
				style={styles.profileIcon}
				source={icons.FEED_DEFAULT_ICON}
			/>
		}
	}

	formatDate(date: any) {
		let notificationDate = new Date(date);
		const dateText =  moment(notificationDate).from(new Date());
		let numberPattern = /\d+/g;
		const time = dateText.match(numberPattern);
		if (dateText.indexOf('minutes') !== -1 && Number(time) <= 5) {
			return 'Recently'
		}
		return dateText
	}

	renderListView(feedList: any) {
		const { styles } = this.state;

		// @ts-ignore
		return feedList.map(feed => <TouchableWithoutFeedback onPress={() => this.redirectToPage(feed.id)} key={feed.id}>
				<View style={styles.listView}>
					<View style={styles.cardStyle}>
						<View>
							{this.renderFeedIcon(feed.image)}
						</View>
						<View style={styles.textFeedStyle}>
							<Text style={styles.descriptionText}>{`${feed.title}`}</Text>
						</View>
						<View style={{}}>
							<Image
								style={styles.nextIcon}
								source={icons.ARROW_UP_ICON}
								resizeMode={'contain'}
							/>
						</View>
					</View>
					<View style={{ flex: 1, paddingBottom: 11 }}>
						<Text style={styles.dateText}>{this.formatDate(feed.postedOn)}</Text>
					</View>
				</View>
			</TouchableWithoutFeedback>
		)
	}

	redirectToPage(feed: any) {
		const { feedStore } = this.props;
		feedStore.redirectPage(feed)
	}

	isCloseToBottom = (nativeEvent: any) => {
		const paddingToBottom = 20;
		return nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
			nativeEvent.contentSize.height - paddingToBottom;
	};

	render() {
		const { styles } = this.state;
		const feedList = this.props.feedStore.feedList;
		const status = commonStores.networkDataStore.isNetworkConnected;
		const isLoading = this.props.feedStore.isLoading;
		if (status === true) {
			if (feedList && feedList.length) {
				return <ScrollView
					onScroll={({nativeEvent}) => {
						if (this.isCloseToBottom(nativeEvent)) {
							this.props.feedStore.getFeedList(true);
						}
					}}
					scrollEventThrottle={400}
					style={styles.scrollView}>
					{this.renderListView(this.props.feedStore.feedList)}
				</ScrollView>
			} else {
				return isLoading ? <View style={{ flex: 1 }}>
					<LoadingPlaceholder
						loading={isLoading}
						count={6}
						size={100}
					/>
				</View> : this.renderNoFeed();
			}
		} else {
			return this.renderNoFeed();
		}
	}
}

const getStyle = () => StyleSheet.create({
	cardStyle: {
		flex: 1,
		flexDirection: 'row',
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 15,
		alignItems: 'center'
	},
	noFeed: {
		flex: 1,
		position: 'absolute',
		top: 130,
		left: 10,
		right: 10,
		bottom: 0,
		backgroundColor: colors.White,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		shadowColor: 'rgba(0, 0, 0, 0.199926)',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		borderRadius: 6
	},
	scrollView: {
		flex: 1,
		position: 'absolute',
		top: verticalScale(100),
		left: 0,
		right: 0,
		bottom: 0
	},
	listView: {
		marginBottom: 10,
		backgroundColor: colors.White,
		width: widthPercentage(95),
		alignSelf: 'center',

		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 2 : 4,
		shadowRadius: 2,
		borderRadius: 4
	},
	textFeedStyle: {
		flex: 1,
		marginTop: 10,
		marginBottom: 10,
		marginLeft: 8,
		paddingHorizontal: 8
	},
	descriptionText: {
		fontSize: isTablet() ? verticalScale(12) : verticalScale(16),
		color: colors.SubHeaderColor
	},
	dateText: {
		textAlign: 'right',
		fontSize: isTablet() ? verticalScale(8) : verticalScale(10),
		color: colors.SubHeaderColor,
		marginRight: 13
	},
	more: {
		marginTop: 5,
		marginRight: 10,
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		color: colors.PrimaryBlue
	},
	container: {
		flexDirection: 'row',
		alignItems: 'flex-start',
		alignSelf: 'flex-start'
	},
	dateStyle: {
		color: colors.SubHeaderColor,
		fontSize: 12,
		fontWeight: 'bold'
	},
	titleStyle: {
		color: colors.ContentColor,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)
	},
	descriptionStyle: {
		color: colors.Black,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		fontWeight: 'bold'
	},
	parentView: {
		flexDirection: 'column'
	},
	profileIcon: {
		width: verticalScale(50),
		height: verticalScale(50),
		borderRadius: verticalScale(25)
	},
	notificationIconSmall: {
		height: 20,
		width: 20,
		marginLeft: 10,
		marginRight: 10
	},
	notificationIcon: {
		height: 20,
		width: 20,
		marginLeft: 10,
		marginRight: 10,
		marginTop: 10,
		marginBottom: 10
	},
	nextIcon: {
		width: verticalScale(12),
		height: verticalScale(12),
		marginRight: 0,
		marginLeft: 5,
		transform: [
			{rotate: '90deg'}
		]
	}
});
