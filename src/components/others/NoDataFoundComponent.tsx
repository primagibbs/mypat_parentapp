import React, { Component } from 'react'
import { icons, widthPercentage, isTablet } from '../../common'
import { BackHandler, View, Image, Text, StyleSheet, Platform } from 'react-native';
import { colors } from '../../config';

interface Props {
  headingText?: string,
  descText?: string,
  iconName?: string
}

interface State {
  styles?: any,
  headingText?: string,
  descText?: string,
  iconName?: any
}

const DEFAULT_HEADING_MSG = 'No test paper';
const DEFAULT_MSG = 'There is no test paper matched your search.' +
  'Try using search options to other test';
const DEFAULT_ICON = icons.NOTESTICON;

export class NoDataFoundComponent extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      styles: getStyle(),
      headingText: this.props.headingText || DEFAULT_HEADING_MSG,
      descText: this.props.descText || DEFAULT_MSG,
      iconName: this.props.iconName || DEFAULT_ICON
    }
  }

  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  render() {
    const { styles, headingText, descText, iconName } = this.state;
    return <View style={styles.container}>
      <Image style={[styles.bannerImage]}
        source={iconName} />
      <Text style={styles.headingtext}>
        {headingText}
      </Text><Text style={styles.text}>
        {descText}
      </Text></View>
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
		paddingLeft: 20,
    paddingRight: 20
  },
  bannerImage: {
    width: isTablet() ? 320 : 160,
    height: isTablet() ? 280 : 140,
		marginBottom: 40
  },
  headingtext: {
    fontSize: 18,
    textAlign: 'center',
    color: colors.SubHeaderColor,
    letterSpacing: 0.3
  },
  text: {
    fontSize: 14,
    textAlign: 'center',
    color: colors.SubHeaderColor,
		opacity: 0.54,
    marginTop: 15,
    lineHeight: 18,
    letterSpacing: 0.3,
		paddingLeft: 40,
		paddingRight: 40
  }
});
