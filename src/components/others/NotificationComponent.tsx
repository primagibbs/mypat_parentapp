import React, { Component } from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, Dimensions, AppState, ScrollView} from 'react-native';
// @ts-ignore
import Navigation from 'react-navigation';
import { observer, inject } from 'mobx-react';
import moment from 'moment';
import { colors, dimens } from '../../config';
import {
	widthPercentage,
	isPortrait,
	icons,
	verticalScale
} from '../../common';
import { NotificationStore, NetworkDataStore } from '../../store';
import commonStores from '../../common-library/store';
import { NoDataFoundComponent } from './NoDataFoundComponent';
import { LoadingPlaceholder } from '../LoadingPlaceholder';

interface Props {
	// @ts-ignore
	navigation?: Navigation,
	notificationList?: any,
	notificationStore?: NotificationStore
	networkDataStore?: NetworkDataStore
}

interface State {
	styles?: any
}

@inject('notificationStore', 'networkDataStore')
@observer
export class NotificationComponent extends Component<Props, State> {
	courseId: any;

	constructor(props: Props, state: State) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	async componentDidMount() {
		const { notificationStore, navigation } = this.props;
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});
		notificationStore.setNavigationObject(navigation);
		await notificationStore.getNotificationList();
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	renderNoNotification() {
		const { styles } = this.state;
		return (
			<View style={styles.noNotification}>
				<NoDataFoundComponent
					headingText= {'No Notifications Yet'}
					descText= 'Stay tuned! Notification about your kids activity will show up here'
					iconName= {icons.NO_NOTIFICATION}/>
			</View>
		)
	}

	renderListView(notificationList: any) {
		const { styles } = this.state;

		// @ts-ignore
		return notificationList.map((notification, index) => <TouchableOpacity key={index} onPress={() => this.redirectToPage(notification)}>
				<View style={styles.listView} key={notification.id}>
					<View style={ index % 2 === 0 ? styles.cardStyle : styles.unReadCardStyle}>
						<View style={{ flexDirection: 'row', marginTop: 10}}>
							<View style={{justifyContent: 'center'}}>
								{this.renderNotificationIcon(notification.userImage)}
							</View>
							<View style={styles.textNotificationStyle}>
								{ notification.userName ? <Text style={styles.userName}>{`${notification.userName} `}</Text> : null }
								<Text style={styles.descriptionText}>{`${notification.notification}`}</Text>
							</View>
						</View>
						<View style={{ marginTop: 10, alignItems: 'flex-end'}}>
							{this.renderNotificationSmallIcon(notification.smallIcon)}
							<Text style={styles.dateText}>{this.dateFormat(notification.createdAt)}</Text>
						</View>
					</View>
				</View>
			</TouchableOpacity>
		)
	}

	redirectToPage(notification: any) {
		const { notificationStore } = this.props;
		notificationStore.redirectPage(notification)
	}

	dateFormat(date: any) {
		let notificationDate = new Date(date);
		const dateText =  moment(notificationDate).from(new Date());
		let numberPattern = /\d+/g;
		const time = dateText.match(numberPattern);
		if (dateText.indexOf('minutes') !== -1 && Number(time) <= 5) {
			return 'Recently'
		}
		return dateText
	}

	renderNotificationIcon(image: string) {
		const { styles } = this.state;
		return <View>
			<Image
				style={styles.profileIcon}
				source={{uri: image}}
			/>
		</View>
	}

	renderNotificationSmallIcon(image: string) {
		const { styles } = this.state;
		if (image) {
			return <Image
				style={styles.notificationIconSmall}
				// @ts-ignore
				source={image}
			/>
		}
		return null
	}
	isCloseToBottom = (nativeEvent: any) => {
		const paddingToBottom = 20;
		return nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
			nativeEvent.contentSize.height - paddingToBottom;
	};
	  

	render() {
		const { styles } = this.state;
		// this.props.notificationStore.totalNotificationCount = 0;
		const notificationList = this.props.notificationStore.notificationList;
		const status = commonStores.networkDataStore.isNetworkConnected;
		const isLoading = this.props.notificationStore.isLoading;
		if (status === true) {
			if (notificationList && notificationList.length) {
				return (
					<ScrollView 
						onScroll={({nativeEvent}) => {
							if (this.isCloseToBottom(nativeEvent)) {
								this.props.notificationStore.getNotificationList(true);
							}
						}}
					  	scrollEventThrottle={400}
						style={styles.scrollView}>
						{this.renderListView(this.props.notificationStore.notificationList)}
					</ScrollView>
					);
			} else {
				return isLoading ? <View style={{ flex: 1 }}>
					<LoadingPlaceholder
						loading={isLoading}
						count={6}
						size={100}
					/>
				</View> : this.renderNoNotification();
			}
		} else {
			return this.renderNoNotification();
		}
	}
}

const getStyle = () => StyleSheet.create({
	scrollView: {
		flex: 1,
		position: 'absolute',
		top: verticalScale(70),
		bottom: 0,
		backgroundColor: colors.White,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		shadowColor: 'rgba(0, 0, 0, 0.199926)',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		borderRadius: 6
	},
	content: {
		flex: 1,
	},
	unReadCardStyle: {
		flex: 1,
		backgroundColor: colors.UnreadNotification,
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 15,
		paddingBottom: 15
	},
	cardStyle: {
		flex: 1,
		backgroundColor: colors.White,
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 15,
		paddingBottom: 15
	},
	listView: {
		flexDirection: 'row',
		borderBottomColor: colors.LightGrayOpacity50,
		borderBottomWidth: 2,
		borderRadius: 4,
		width: widthPercentage(100),
		alignSelf: 'center'
	},
	textNotificationStyle: {
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
		marginTop: 10,
		marginBottom: 10
	},
	userName: {
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		color: colors.HeaderColor,
		fontWeight: 'bold'
	},
	descriptionText: {
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		color: colors.SubHeaderColor
	},
	dateText: {
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		color: colors.SubHeaderColor
	},
	nobordercardStyle: {
		flex: 1,
		backgroundColor: colors.White,
		flexDirection: 'column',
		paddingTop: 10,
		paddingBottom: 10,
		marginRight: 15,
		marginLeft: 10
	},
	more: {
		marginTop: 5,
		marginRight: 10,
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		color: colors.PrimaryBlue
	},
	newsTitle: {
		marginLeft: 5,
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		color: colors.HeaderColor,
		fontSize: dimens.size19,
		fontWeight: 'bold'
	},
	itemCode: {
		borderColor: colors.GrayUnderLine,
		backgroundColor: colors.White,
		margin: 10,
		borderWidth: 1,
		paddingBottom: 10,
		borderRadius: 5
	},
	container: {
		flexDirection: 'row',
		alignItems: 'flex-start',
		alignSelf: 'flex-start'
	},
	dateStyle: {
		color: colors.SubHeaderColor,
		fontSize: 12,
		fontWeight: 'bold'
	},
	titleStyle: {
		color: colors.ContentColor,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)
	},
	descriptionStyle: {
		color: colors.Black,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		fontWeight: 'bold'
	},
	cardView: {
		borderWidth: 1,
		borderColor: colors.BorderColor,
		borderBottomWidth: 0,
		shadowColor: colors.ShadowColor,
		backgroundColor: colors.White,
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		elevation: 2,
		shadowRadius: 0,
		marginRight: widthPercentage(2),
		marginLeft: widthPercentage(2),
		marginTop: 10,
		marginBottom: 10,
		paddingTop: 10
	},
	parentView: {
		flexDirection: 'column'
	},
	profileIcon: {
		width: verticalScale(50),
		height: verticalScale(50),
		marginLeft: 15,
		marginRight: 15,
		borderRadius: verticalScale(25)
	},
	notificationIconSmall: {
		height: 20,
		width: 20,
		marginLeft: 10,
		marginRight: 10
	},
	notificationIcon: {
		height: 20,
		width: 20,
		marginLeft: 10,
		marginRight: 10,
		marginTop: 10,
		marginBottom: 10
	},
	noNotification: {
		position: 'absolute',
		top: verticalScale(70),
		left: 10,
		right: 10,
		bottom: 0,
		backgroundColor: colors.White,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		shadowColor: 'rgba(0, 0, 0, 0.199926)',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		borderRadius: 6
	}
});
