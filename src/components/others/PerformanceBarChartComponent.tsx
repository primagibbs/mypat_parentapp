import React, { Component } from 'react'
import { Animated, View, Text, StyleSheet, TouchableHighlight, Dimensions, processColor } from 'react-native'
import { inject } from 'mobx-react'
import { HorizontalBarChart } from 'react-native-charts-wrapper'
import { take, _ } from 'lodash'
import { dimens, colors } from '../../config'
import { widthPercentage, isPortrait, SUBJECT_COLOR, SCORE_TYPE } from '../../common'
import { displayPercentage, capitalizeWords } from '../../utils'
import { PerformanceStore } from '../../store'

interface Props {
  physics?: number
  chemistry?: number
  maths?: number
  currentContext?: any,
  subjectList?: any,
  performanceStore?: PerformanceStore,
  graphDataValues?: any
  graphType?: any
}
interface State {
  physics?: any,
  chemistry?: any,
  maths?: any,
  styles?: any,
  legend?: any,
  data?: any,
  xAxis?: any,
  yAxis?: any,
  highlights?: any
  maxValue?: any,
  marker?: any
}
@inject('performanceStore')
export class PerformanceBarChartComponent extends Component<Props, State> {
  speedXaxisCount: 6
  maxCount
  constructor(props: Props, state: State) {
    super(props, state)
    const { physics, chemistry, maths } = this.props
    const width = { physics: physics, chemistry: chemistry, maths: maths }
    this.state = {
      physics: new Animated.Value(width.physics),
      chemistry: new Animated.Value(width.chemistry),
      maths: new Animated.Value(width.maths),
      styles: getStyle(),
      maxValue: 1,
      legend: {},
      data: {},
      xAxis: {},
      yAxis: {},
      marker: {
        enabled: true,
        digits: 1,
        backgroundTint: processColor('teal'),
        markerColor: processColor('#F0C0FF8C'),
        textColor: processColor('red')
      }
    }
  }
  async componentDidMount() {
    Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
    this.setGraphData(this.props)
  }
  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.subjectList !== nextProps.subjectList) {
      this.setGraphData(nextProps)
    }
  }
  setGraphData(props) {
    const { subjectList, performanceStore, graphType } = props
    let scoreArray = []
    let subjectNameArray = []
    let colorArray = []
    let axisMax = 100
    let drowValue = false
    if (graphType === 'result') {
      axisMax = 100
      subjectList.map((subjectData) => {
          let score = subjectData.stats.marksPercentage || 0.0
          score = parseFloat(score.toFixed(2))
          scoreArray.push(score || 0.0)
        subjectNameArray.push(this._getSubjectName(subjectData.name))
        const colorCode = SUBJECT_COLOR.get(capitalizeWords(subjectData.name)) ||  '#F2999B'
        colorArray.push(processColor(colorCode))
      })
      scoreArray.map((score) => {
        if (score !== 0) {
          drowValue = true
        }
      })
      this.setState({
        data: {
          dataSets: [{
            values: scoreArray,
            label: '',
            config: {
              colors: colorArray,
              barSpacePercent: 40,
              drawValues: drowValue,
              valueFormatter: true,
              valueFormatterPattern: axisMax,
              drawRadius: 5,
              drawCircle: true,
              scatterShape: 'CIRCLE',
              cornerRadius: 7
            }
          }],
          config: {
            barWidth: 0.2,
            barLinejoin: 'round',
            barCornner: true,
            barRadius: true,
            circleRadius: 0.2,
            drawCircle: true,
            borderRadius: 0.2,
            borderWidth: 2,
            borderColor: processColor('black'),
            cornerRadius: 5,
            drawRadius: 7
          }
        },
        xAxis: {
          valueFormatter: subjectNameArray,
          position: 'BOTTOM',
          granularityEnabled: true,
          granularity: 1,
          drawGridLines: false,
          avoidFirstLastClipping: true
        },
        yAxis: {
          left: {
            axisMinimum: 0,
            enabled: false,
            axisMaximum: axisMax
          },
          right: {
            axisMinimum: 0,
            axisMaximum: axisMax,
            drawGridLines: false,
            drawLabels: true,
            labelCount: 5,
            granularityEnabled: true,
            granularity: 1
          }
        }
      })
    } else {
      if (performanceStore.getCurrentTestType() === 'attemptRate') {
        axisMax = performanceStore.maxValue
      } else {
        axisMax = 100
      }
    subjectList.map((subjectData) => {
      if (performanceStore.getCurrentTestType() === 'attemptRate') {
        let score =  subjectData.score * 60 || 0
        score = parseFloat(score.toFixed(2))
        scoreArray.push(score)
      } else {
        let score = subjectData.score || 0
        score = parseFloat(score.toFixed(2))
        scoreArray.push(subjectData.score || 0)
      }
      subjectNameArray.push(this._getSubjectName(subjectData.subjectName))
      const colorCode = SUBJECT_COLOR.get(subjectData.subjectName) ||  '#F2999B'
      colorArray.push(processColor(colorCode))
    })
    scoreArray.map((score) => {
      if (score !== 0) {
        drowValue = true
      }
    })
    this.setState({
      data: {
        dataSets: [{
          values: scoreArray,
          label: '',
          config: {
            colors: colorArray,
            barSpacePercent: 40,
            drawValues: drowValue,
            valueFormatter: true,
            valueFormatterPattern: axisMax,
            drawRadius: 5,
            drawCircle: true,
            scatterShape: 'CIRCLE',
            cornerRadius: 7
          }
        }],
        config: {
          barWidth: 0.2,
          barLinejoin: 'round',
          barCornner: true,
          barRadius: true,
          circleRadius: 0.2,
          drawCircle: true,
          borderRadius: 0.2,
          borderWidth: 2,
          borderColor: processColor('black'),
          cornerRadius: 5,
          drawRadius: 7
        }
      },
      xAxis: {
        valueFormatter: subjectNameArray,
        position: 'BOTTOM',
        granularityEnabled: true,
        granularity: 1,
        drawGridLines: false,
        avoidFirstLastClipping: true
      },
      yAxis: {
        left: {
          axisMinimum: 0,
          enabled: false,
          axisMaximum: axisMax
        },
        right: {
          axisMinimum: 0,
          axisMaximum: axisMax,
          drawGridLines: false,
          drawLabels: true,
          labelCount: 5,
          granularityEnabled: true,
          granularity: 1
        }
      }
    })
  }
  }
  _getSubjectName(subject) {
    if (subject === 'Mathematics') {
      return subject.substring(0, 4).toUpperCase()
    } else
    if (subject.length > 3 ) {
      const subjName = subject.substring(0, 3)
      return (subjName).toUpperCase()
    } else {
      return (subject).toUpperCase()
    }
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  renderPhysicsbar(subjectData) {
    const { styles } = this.state
    const { performanceStore } = this.props
    if (performanceStore.getCurrentTestType() === 'attemptRate') {
      return (<View style={styles.barBgView}>
        <Animated.View style={[styles.bar, styles.physics, { width: (subjectData * 60) }]} />
      </View>)
    } else {
      return (<View style={styles.barBgView}>
        <Animated.View style={[styles.bar, styles.physics, { width: subjectData }]} />
      </View>)
    }
  }

  renderChembar(subjectData) {
    const { styles } = this.state
    return (<View style={styles.barBgView}>
      <Animated.View style={[styles.bar, styles.chemistry, { width: subjectData }]} />
    </View>)
  }

  renderMathsbar(subjectData) {
    const { styles } = this.state
    return (<View style={styles.barBgView}>
      <Animated.View style={[styles.bar, styles.maths, { width: subjectData }]} />
    </View>)
  }

  render() {
    const { styles } = this.state
    const { performanceStore } = this.props
    let labelText
    const scoreType = performanceStore.getCurrentTestType()
    switch (scoreType) {
      case SCORE_TYPE.TOTAL_SCORE:
        labelText = 'Your Total Score in (%)'
        break
      case SCORE_TYPE.ACCURACY:
        labelText = 'Accuracy Rate in (%)'
        break
      case SCORE_TYPE.ATTEMPT_RATE:
        labelText = 'Speed (Questions/Min)'
        break
      default:
        break
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.containerChart}>
          <HorizontalBarChart
            style={styles.chart}
            data={this.state.data}
            xAxis={this.state.xAxis}
            yAxis={this.state.yAxis}
            marker={this.state.marker}
            animation={{ durationX: 1000 }}
            chartDescription={{ text: '' }}
            legend={{ enabled: false }}
            gridBackgroundColor={processColor('white')}
            drawBarShadow={false}
            drawValueAboveBar={true}
            drawHighlightArrow={true}
            highlights={this.state.highlights}
            drawGridBackground={false}
            borderColor={processColor('teal')}
            borderWidth={0}
            drawBorders={false}
            axisRight={false}
            touchEnabled={false}
            dragEnabled={false}
            scaleXEnabled={true}
            scaleYEnabled={true}
            pinchZoom={false}
            doubleTapToZoomEnabled={false}
            dragDecelerationEnabled={true}
            dragDecelerationFrictionCoef={0.99}
            keepPositionOnRotation={false}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
        <Text style={styles.lableStyle}> {labelText}</Text>
      </View>)
  }

  renderPercentageLabels() {
    const { styles, maxValue } = this.state
    const { performanceStore } = this.props
    return (<View style={styles.dataScale}>
      <Text style={styles.dataScaleType}>{displayPercentage(0)}</Text>
      <Text style={styles.dataScaleType}>{displayPercentage(20)}</Text>
      <Text style={styles.dataScaleType}>{displayPercentage(40)}</Text>
      <Text style={styles.dataScaleType}>{displayPercentage(60)}</Text>
      <Text style={styles.dataScaleType}>{displayPercentage(80)}</Text>
      <Text style={styles.dataScaleType}>{displayPercentage(100)}</Text>
    </View>
    )
  }
  speedXaxis(array) {
    const { styles } = this.state
    const speedView = _.chain(array)
      .take(array.length)
      .map((data, index) => this.renderXaxis(data, index))
      .value()
    return <View style={styles.dataScale}>
      {speedView}
    </View>
  }
  renderXaxis(data, index) {
    const { styles } = this.state
    return <Text style={styles.dataScaleType}>{displayPercentage(data)}</Text>
  }
  renderBarItems() {
    const { styles } = this.state
    const { subjectList } = this.props
    const barViews = _.chain(subjectList)
      .take(subjectList.length)
      .map((subjectData, index) => this.renderBar(subjectData, index))
      .value()
    return <View style={styles.cardView}>
      <View style={styles.itemCode}>
        {barViews}
      </View >
      {this.renderPercentageLabels()}
    </View>
  }

  renderBar(subjectData, index) {
    const { styles } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.item}>
          <Text style={styles.label}>{subjectData.subjectName}</Text>
          <View style={styles.data}>
            {this.renderPhysicsbar(subjectData.score || 0)}
            <Text style={styles.dataNumber}>  </Text>
          </View>
        </View>
      </View>
    )
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginTop: 6
  },
  // Item
  item: {
    flexDirection: 'row',
    marginTop: 6,
    marginBottom: 5,
    width: widthPercentage(100)
  },
  label: {
    color: colors.Black,
    width: 90,
    fontSize: 13,
    position: 'relative',
    top: -6
  },
  data: {
    flex: 1,
    flexDirection: 'row'
  },
  dataScale: {
    flexDirection: 'row',
    marginTop: 6,
    marginBottom: 5,
    paddingLeft: 90,
    width: widthPercentage(100)
  },
  dataScaleType: {
    color: colors.Black,
    fontSize: dimens.size13,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: widthPercentage(3)
  },
  dataNumber: {
    color: colors.Black,
    fontSize: 14
  },
  barBgView: {
    width: isPortrait() ? widthPercentage(60) : widthPercentage(80),
    backgroundColor: colors.GrayUnderLine,
    height: 10,
    borderRadius: 5
  },
  bar: {
    width: widthPercentage(100),
    borderRadius: 5,
    height: 10,
    paddingLeft: 0,
    marginRight: 2,
    paddingBottom: 2
  },
  physics: {
    backgroundColor: colors.ProgressBarOrange
  },
  chemistry: {
    backgroundColor: colors.ProgressBarBlue
  },
  maths: {
    backgroundColor: colors.ProgressBarYellow
  },
  containerChart: {
    height: isPortrait() ? widthPercentage(50) : widthPercentage(50),
    width: isPortrait() ? widthPercentage(95) : widthPercentage(90),
    marginTop: 5
    },
  chart: {
    height: isPortrait() ? widthPercentage(45) : widthPercentage(45),
    width: isPortrait() ? widthPercentage(85) : widthPercentage(85),
    backgroundColor: 'white'
  },
  lableStyle: {
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 20
  }
})