import React, { Component } from 'react'
import { View } from 'react-native'
import { WebView } from 'react-native-webview';

interface Props {
    uri: string,
    onMessage?: () => void
}

interface State {
}

export class WebviewComponent extends Component<Props, State> {
    myWebviewRef
    constructor(props: Props, state: State) {
        super(props, state)
        // this.onWebViewMessage = this.onWebViewMessage.bind(this)
    }

    // TODO: Asheesh Sample Implementation of onMessage

    // handleDataReceived(msgData) {
    //     // handle incoming data here
    // }
    // onWebViewMessage(event) {
    //     let msgData
    //     try {
    //         msgData = JSON.parse(event.nativeEvent.data)
    //     } catch (err) {
    //         return
    //     }
    //     switch (msgData.targetFunc) {
    //         case 'handleDataReceived':
    //             this[msgData.targetFunc].apply(this, [msgData])
    //             break
    //         default:
    //             break
    //     }
    // }

    postToWeb(data) {
        if (typeof data !== 'string') {
            data = JSON.stringify(data)
        }
        this.myWebviewRef.postMessage(data)
    }
    render() {
        return (
            <View>
                <WebView
                    ref={webview => {
                        this.myWebviewRef = webview
                    }}
                    scrollEnabled={false}
                    source={{ uri: this.props.uri }}
                    onMessage={() => this.props.onMessage}
                />
            </View>
        )
    }
}
