import React, { Component } from 'react'
import {View, Text, StyleSheet, Image, TextInput, TouchableOpacity, TouchableWithoutFeedback} from 'react-native'
import { inject, observer } from 'mobx-react'
import { colors } from '../../config'
import BottomSheet from 'react-native-bottomsheet';
import ImagePicker from "react-native-image-crop-picker";
import {isPortrait, isTablet, verticalScale, widthPercentage, icons} from '../../common';
import { ProfileDataStore } from '../../store';
import {showSnackbar} from "../../common-library/services";

interface Props {
	profileDataStore?: ProfileDataStore,
	isFormEditable: any,
	isViewDetails: any,
	_onLayout?: any,
	navigation?: any
}

interface State {
	styles?: any,
	parentInfo: any,
	editMode: boolean,
	viewDetails: boolean
}

@inject('profileDataStore')
@observer
export default class ParentProfileComponent extends Component<Props, State> {
	profileImage: any;
	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle(),
			parentInfo: {
				name: '',
				email: '',
				mobile: '',
				location: '',
				occupation: '',
			},
			editMode: false,
			viewDetails: false
		};
		props.isFormEditable && props.isFormEditable(false);
		props.isViewDetails && props.isViewDetails(false);
	}

	onPressViewDetails = () => {
		this.props.isViewDetails && this.props.isViewDetails(true);
		this.setState({
			viewDetails: true
		})
	};

	resetForm = () => {
		this.setState({
			parentInfo: {
				name: '',
				email: '',
				mobile: '',
				location: '',
				occupation: '',
			}
		})
	};

	onPressSaveDetails = async () => {
		let userDetails = {};
		let updatedParentInfo = this.state.parentInfo;
		const profileDetails = this.props.profileDataStore.userProfileDetails || {};
		Object.keys(updatedParentInfo).forEach((key) => {
			if(updatedParentInfo[key] && updatedParentInfo[key] !== profileDetails[key]) {
				// @ts-ignore
				userDetails[key] = updatedParentInfo[key]
			}
		});
		console.log(userDetails);

		// @ts-ignore
		const mobile = userDetails.mobile;
		// @ts-ignore
		delete userDetails.mobile;
		delete userDetails.userImage;

		if(Object.keys(userDetails).length > 0) {
			await this.props.profileDataStore.updateUser(userDetails)
		}

		if (mobile) {HomeDataStore
			const { profileDataStore, navigation } = this.props;
			profileDataStore.setNavigationObject(navigation);
			await profileDataStore.sendOTPForMobileChange(mobile)
		}
		this.setState({
			editMode: false,
			viewDetails: false
		});
		this.props.isFormEditable && this.props.isFormEditable(false);
		this.props.isViewDetails && this.props.isViewDetails(false);
		this.resetForm();
	};

	onPressEditDetails = () => {
		this.resetForm();
		const profileDetails = this.props.profileDataStore.userProfileDetails || {};
		this.props.isFormEditable && this.props.isFormEditable(true);
		this.setState({
			parentInfo: {...profileDetails},
			editMode: true,
			viewDetails: true
		});
	};

	updateValue = (key: any, event: any) => {
		if(event.nativeEvent) {
			let parentInfo = this.state.parentInfo;
			parentInfo[key] = event.nativeEvent.text;
			this.setState({
				parentInfo: parentInfo
			})
		}
	};

	renderProfileImage = () => {
		const { profileDataStore: { userProfileDetails } } = this.props;
		const { editMode, styles } = this.state;
		const imageView = (
			<View style={{ width: verticalScale(70), height: verticalScale(70), overflow: 'hidden'}}>
				{
					userProfileDetails && userProfileDetails.userImage ? <Image source={{uri: userProfileDetails.userImage}} borderRadius={verticalScale(35)} style={styles.userProfileImage} resizeMode={'cover'} /> :
						<Image source={icons.USER_ICON} style={styles.userProfileImage} resizeMode={'contain'} />
				}
				{editMode ? <Image source={require('../../../images/image_edit_icon.png')} style={styles.userProfileImageEdit} resizeMode='contain' /> : <View/> }
			</View>
		);
		return editMode ? (
			<TouchableOpacity onPress={() => this._onCameraBtnClicked()}>
				{imageView}
			</TouchableOpacity>
		) : imageView
	};

	_onCameraBtnClicked() {
		console.log("onCameraBtnClicked");
		this.openActionSheet({
			cropit: true,
			circular: true
		})
	}

	// @ts-ignore
	openActionSheet({ cropit, circular = false }) {
		const { profileDataStore } = this.props;
		let profileImage = profileDataStore.profileImageUrl;
		if (profileImage && profileImage.length > 0) {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Remove Photo', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 3
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		} else {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 2
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		}
	}

	// @ts-ignore
	pickCamera({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openCamera({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch((e) => {
			console.log("error", e);
			// @ts-ignore
			alert('Image Upload Cancelled')
		})
	}

	// @ts-ignore
	pickGallery({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch(e => {
			console.log("error", e);
		})
	}

	// @ts-ignore
	openActionSheet({ cropit, circular = false }) {
		const { profileDataStore } = this.props;
		let profileImage = profileDataStore.profileImageUrl;
		if (profileImage && profileImage.length > 0) {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Remove Photo', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 3
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		} else {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 2
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		}
	}

	render() {
		const { styles, editMode, parentInfo } = this.state;
		const profileDetails = this.props.profileDataStore.userProfileDetails || {};
		return(
				<View style={styles.container} onLayout={this.props._onLayout ? this.props._onLayout : null}>
					<View style={{flex: 1, position: 'absolute', zIndex: 9999, top: verticalScale(-35), alignSelf: 'center'}}>
						{this.renderProfileImage()}
					</View>
					<View style={styles.nonEditableProfile}>

						<View style={styles.nameContainer}>
							<View style={styles.nameEmptyView} />
							{
								(!editMode && !this.state.viewDetails) ?
									(<Text numberOfLines={1} style={styles.nameStatic}>{parentInfo.name || profileDetails.name || ''}</Text>)
									: <View/>
							}
							<TouchableOpacity style={styles.editProfileButtonContainer} onPress={editMode ? this.onPressSaveDetails: this.onPressEditDetails}>
								{
									(editMode) ?
										(<Text style={styles.textButton}>Save</Text>) :
										(<Image source={require('../../../images/pencil-icon.png')} style={styles.editProfileButton} />)
								}
							</TouchableOpacity>
						</View>
						{
							(!editMode && !this.state.viewDetails) ?
								(
									<TouchableOpacity onPress={this.onPressViewDetails}>
										<Text style={styles.textButton}>View Details</Text>
									</TouchableOpacity>
								)
								: <View/>
						}
					</View>
					{
						(this.state.viewDetails) ?
							(<View style={styles.rowView}>
								<View style={styles.editableProfileContainer}>
									<View style={styles.editableprofileItem}>
										<Image resizeMode='contain' source={require('../../../images/icon_edit_profile_name.png')} style={styles.editableprofileItemIcon} />
										<TextInput
											placeholder={'Your name'}
											style={styles.editableprofileItemInput}
											value={editMode ? parentInfo.name || '' : profileDetails.name || ''}
											onChange={this.updateValue.bind(this, 'name')}
											underlineColorAndroid='transparent'
											editable={editMode}
											placeholderTextColor='rgba(153, 153, 153, 0.54)'/>
									</View>
									<View style={styles.editableprofileItem}>
										<Image resizeMode='contain' source={require('../../../images/icon_edit_profile_mail.png')} style={styles.editableprofileItemIcon} />
										<TouchableOpacity
											style={{flex: 1}}
											onPress={() => {
											if (editMode && !parentInfo.isEmailEditable) {
												showSnackbar('Updating your email ID will disable your account since you used social login to register your account');
											}
										}}>
											<TextInput
												placeholder={'Email Id'}
												style={styles.editableprofileItemInput}
												value={editMode ? parentInfo.email || '' : profileDetails.email || ''}
												onChange={this.updateValue.bind(this, 'email')}
												underlineColorAndroid='transparent'
												editable={editMode && parentInfo.isEmailEditable}
												placeholderTextColor='rgba(153, 153, 153, 0.54)'/>
										</TouchableOpacity>
									</View>
									<View style={styles.editableprofileItem}>
										<Image resizeMode='contain' source={require('../../../images/icon_edit_profile_mobile.png')} style={styles.editableprofileItemIcon} />
										<TextInput
											placeholder={'8888888888'}
											style={styles.editableprofileItemInput}
											value={editMode ? parentInfo.mobile || '' : profileDetails.mobile || ''}
											keyboardType={'number-pad'}
											onChange={this.updateValue.bind(this, 'mobile')}
											underlineColorAndroid='transparent'
											editable={editMode}
											placeholderTextColor='rgba(153, 153, 153, 0.54)'/>
									</View>
									<View style={styles.editableprofileItem}>
										<Image resizeMode='contain' source={require('../../../images/icon_edit_profile_location.png')} style={styles.editableprofileItemIcon} />
										<TextInput
											placeholder={'Location'}
											style={styles.editableprofileItemInput}
											value={editMode ? parentInfo.location || '' : profileDetails.location || ''}
											onChange={this.updateValue.bind(this, 'location')}
											underlineColorAndroid='transparent'
											editable={editMode}
											placeholderTextColor='rgba(153, 153, 153, 0.54)'/>
									</View>
									<View style={styles.editableprofileItem}>
										<Image resizeMode='contain' source={require('../../../images/icon_edit_profile_business.png')} style={styles.editableprofileItemIcon} />
										<TextInput
											placeholder={'Occupation'}
											style={styles.editableprofileItemInput}
											value={editMode ? parentInfo.occupation || '' : profileDetails.occupation || ''}
											onChange={this.updateValue.bind(this, 'occupation')}
											underlineColorAndroid='transparent'
											editable={editMode}
											placeholderTextColor='rgba(153, 153, 153, 0.54)'/>
									</View>

								</View>
							</View> ) :
							<View />
					}
				</View>
			
		)
	}
}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	container: {
		flex: 1,
		marginHorizontal: 10,
		marginBottom: 23,
		backgroundColor: colors.White,
		borderRadius: 4,
		elevation: isTablet() ? 2 : 4,
		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		paddingBottom: 17
	},
	rowView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	nonEditableProfile: {
		flex: 1,
		paddingTop: verticalScale(38),
		justifyContent: 'center',
		alignItems: 'center'
	},
	nameEmptyView:{
		width: 30
	},
	editProfileButtonContainer:{
		alignItems: 'flex-end',
		padding: 8
	},
	editProfileButton:{
		width: 16,
		height: 16,
		padding: 4
	},
	nameContainer:{
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignSelf: 'stretch',
		alignItems: 'center',
		paddingRight: 26,
		paddingLeft: 26,
		// marginTop: 8,
		
	},
	nameStatic:{
		justifyContent: 'center',
		color: colors.SubHeaderColor,
		fontSize: isTablet() ? verticalScale(13) : verticalScale(16),
		lineHeight: verticalScale(16),
		marginTop: isTablet() ? 12 : 6,
		marginBottom: 12,
		textAlign: 'center',
		alignItems: 'center',
		alignSelf: 'center'
	},
	textButton:{
		justifyContent: 'center',
		color: colors.ParentBlue,
		fontSize: isTablet() ? verticalScale(10) : verticalScale(13),
		lineHeight: verticalScale(13),
		textAlign: 'center',
		marginBottom: 14
	},
	editableProfileContainer:{
		alignSelf: 'stretch',
		flexDirection: 'column',
		justifyContent: 'center',
		flex: 1,
		alignItems: 'center',
		paddingLeft: 20,
		paddingRight: 20,
		marginBottom: 16
	},
	editableprofileItem:{
		flexDirection: 'row',
		alignSelf: 'stretch',
		alignItems: 'center',
		marginTop: 10
	},
	editableprofileItemIcon:{
		height: 20,
		width: 20,
		marginRight: 16
	},
	editableprofileItemInput:{
		borderBottomColor: 'rgba(153, 153, 153, 0.54)',
		borderBottomWidth: 0.5,
		color: colors.SubHeaderColor,
		marginBottom: 0,
		fontSize: 16,
		flex: 1,
		paddingVertical: 8
	},
	userProfileImage: {
		width: verticalScale(70),
		height: verticalScale(70)
	},
	userProfileImageEdit: {
		width: 28,
		height: 28,
		position: 'absolute',
		bottom: 0,
		right: 0
	}
});
// All Item StyleSheet
