import React, { Component } from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity, Alert, ScrollView, TouchableWithoutFeedback} from 'react-native'
import { observer, inject } from 'mobx-react'
import { colors } from '../../config'
import {capitalizeWords, cleanStores, handleSignOut} from '../../utils'
import {navigateSimple, resetRouterSimple} from '../../services'
import {icons, isPortrait, isTablet, SOCIAL_LOGIN_MODE, verticalScale, widthPercentage} from '../../common'
import { ProfileDataStore, SocialDataStore } from '../../store';
import { showModal } from '../../services'
import DeleteStudentModal from '../dashboard/modals/DeleteStudentModal'

interface Props {
	navigation: any,
	profileDataStore?: ProfileDataStore,
	socialDataStore?: SocialDataStore
}

interface State {
	styles?: any
}

@inject('profileDataStore', 'socialDataStore')
@observer
export default class StudentsComponent extends Component<Props, State> {

	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	onPressDeleteStudent = (student: any) => {
		showModal(
			<DeleteStudentModal student={student} />,
			{
				hideDialogOnTouchOutside: true,
				positioning: 'center'
			}
		)
	};

	onPressAddStudent = () => {
		navigateSimple(this.props.navigation, 'AddProfile1Page', {page: 'profile'})
	};

	_logoutTransition = async () => {
		const { navigation, socialDataStore, profileDataStore } = this.props;
		if (socialDataStore.userData.type === SOCIAL_LOGIN_MODE.GOOGLE) {
			socialDataStore.googleSignOut();
		}
		await profileDataStore.logout();
		handleSignOut();
		cleanStores();
		resetRouterSimple(navigation, 0, 'SignupPage');
	};

	onPressLogout = async () => {
		Alert.alert(
			'Logout',
			'Are you sure you want to logout ?',
			[
				{ text: 'Cancel' },
				{ text: 'Yes', onPress: async () => await this._logoutTransition() }
			],
			{ cancelable: false }
		);
	};

	renderStudents = (userProfileDetails: any) => {
		const { styles } = this.state;
		const students = userProfileDetails ? userProfileDetails.studentProfiles || [] : [];
		let showDelete = students.length > 1;
		return students.map((student: any, index: number) => {
			return (
				<View style={[styles.studentContainer]} key={index}>
					<View style={styles.studentMainContainer}>
						<View style={{ marginRight: 15, overflow: 'hidden' }}>
							{
								student.image ?
									(<Image source={{ uri: student.image }} borderRadius={verticalScale(15)} style={styles.studentIcon} resizeMode='cover' />) :
									(<Image source={icons.USER_ICON} style={styles.studentIcon} resizeMode='contain' />)
							}
						</View>
						<View style={styles.studentDetails}>
							<Text numberOfLines={1} style={[styles.studentName]}>{capitalizeWords(student.name)}</Text>
							{showDelete ?
								<TouchableOpacity onPress={this.onPressDeleteStudent.bind(this, student)}>
									<Text style={styles.deleteText}>Remove</Text>
								</TouchableOpacity>
								:
								<View/>
							}
						</View>
					</View>
					<View style={styles.separatorView}>
						<View style={styles.emptyIcon} />
						<View style={styles.separatorBorder} />
					</View>
				</View>
			)
		})
	};

	renderAddStudent = () => {
		const { styles } = this.state;
		return (
			<View style={[styles.studentContainer, { paddingBottom: 10 }]}>
				<TouchableOpacity onPress={this.onPressAddStudent}>
					<View style={styles.studentMainContainer}>
						<View style={{ marginRight: 15 }}>
							<Image source={require('../../../images/icon_add_student.png')} style={styles.studentIcon} resizeMode={'contain'} />
						</View>
						<View style={styles.studentDetails}>
							<Text numberOfLines={1} style={[styles.studentName]}>Add another student</Text>
						</View>
					</View>
				</TouchableOpacity>
			</View>
		)
	};


	render() {
		const { styles } = this.state;
		const { profileDataStore: { userProfileDetails } } = this.props;
		return (
			<View style={styles.container}>
				<Text numberOfLines={1} style={styles.studentsCardTitle}>Student Profiles</Text>
				<ScrollView style={styles.scrollView}>
					<View style={styles.studentsCard}>
						{this.renderStudents(userProfileDetails)}
						{this.renderAddStudent()}
					</View>
					<TouchableWithoutFeedback onPress={this.onPressLogout}>
						<View style={styles.logoutContainer}>
							<View style={[{ marginRight: 15, width: verticalScale(30), height: 24, justifyContent: 'center', alignItems: 'center' }]}>
								<Image source={icons.LOGOUT_ICON} style={styles.logoutIcon} resizeMode='contain' />
							</View>
							<Text numberOfLines={1} style={styles.logoutText}>Logout</Text>
						</View>
					</TouchableWithoutFeedback>
				</ScrollView>
			</View>
		)
	}
}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	container: {
		marginVertical: 22,
		marginHorizontal: 10
	},
	scrollView: {
	},
	studentsCardTitle: {
		fontSize: isTablet() ? 21 : 17,
		marginBottom: 10,
		color: colors.ContentColor,
		marginLeft: 10
	},
	studentsCard: {
		width: '100%',
		alignSelf: 'center',
		backgroundColor: colors.White,
		borderRadius: 5,
		marginBottom: 10,

		elevation: 2,
		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.5,
		shadowRadius: 1,
	},
	studentContainer: {
		paddingHorizontal: 9,
		paddingTop: 15,
		width: '100%'
	},
	studentMainContainer: {
		paddingBottom: 15,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	studentDetails: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginRight: 5
	},
	separatorView: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	emptyIcon: {
		width: 30,
		marginRight: 15
	},
	separatorBorder: {
		flex: 1,
		marginRight: 5,
		borderBottomWidth: 1,
		borderColor: 'rgba(153, 153, 153, 0.54)'
	},
	studentIcon: {
		width: isTablet() ? verticalScale(24): verticalScale(30),
		height: isTablet() ? verticalScale(24): verticalScale(30),
	},
	studentName: {
		color: colors.SubHeaderColor,
		fontSize: isTablet() ? 20 : 16,
		lineHeight: isTablet() ? 30 : 24,
		textAlign: 'center'
	},
	deleteText: {
		color: '#1D7DEA',
		fontSize: isTablet() ? 15 : 12,
	},
	logoutContainer: {
		width: '100%',
		alignSelf: 'center',
		paddingVertical: 16,
		paddingHorizontal: 9,
		flexDirection: 'row',
		marginBottom: 10,
		backgroundColor: colors.White,
		borderRadius: 5,

		elevation: 2,
		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.5,
		shadowRadius: 1
	},
	logoutIcon: {
		width: 18,
		height: 24
	},
	logoutText:{
		color: '#777777',
		fontSize: isTablet() ? 20 : 16,
		lineHeight: isTablet() ? 30 : 24,
		flex: 1,
		textAlign: 'left'
	}
});
// All Item StyleSheet
