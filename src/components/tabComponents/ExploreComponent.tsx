import React, { Component } from 'react';
// @ts-ignore
import Navigation from 'react-navigation';
import {
	View, StyleSheet, Text, Dimensions,
	Alert, AppState, Image, TouchableOpacity
} from 'react-native';
import { inject, observer } from 'mobx-react';

import { OnClick, RefreshableScrollView } from '../../components';
import { colors } from '../../config';
import { navigateSimple, showModal } from '../../services';
import {
	HomeDataStore,
	GoalsDataStore,
	PerformanceStore,
	HeaderDataStore,
	AppDataStore,
	NetworkDataStore,
	NewDashboardStore,
	NewGoalsDataStore,
	ProfileActivityStore,
	ProfileDataStore
} from '../../store';
import { verticalScale, widthPercentage, icons, isTablet } from '../../common';
import commonStores from '../../common-library/store';
import stores, { NotificationStore } from '../../store';
import {
	getNotifcationData, setNotifcationData, setAllGoals, getSelectedStudent, getClassName, hasCompleteAppTour, capitalizeWords
} from '../../utils';
import { GoalSelectorHeader } from '../dashboard/GoalSelectorHeader';
import TimeIndicatorCard from '../dashboard/TimeIndicatorCard';
import AddStudent from '../dashboard/modals/AddStudent';
import ActivityChart from '../dashboard/ActivityChart';
import { SyllabusCard } from '../dashboard/SyllabusCard';
import { UpcomingTestsWidget } from '../dashboard/UpcomingTestsWidget';
import { AppTourComponent } from '../others';
import GreetingsCard from '../dashboard/GreetingsCard';
import { IndexCard } from '../dashboard/IndexCard';

interface Props {
	homeDataStore?: HomeDataStore,
	profileActivityStore?: ProfileActivityStore,
	newDashboardStore?: NewDashboardStore,
	goalsDataStore?: GoalsDataStore,
	newGoalsDataStore?: NewGoalsDataStore,
	performanceStore?: PerformanceStore,
	navigation?: Navigation,
	headerDataStore?: HeaderDataStore,
	appDataStore?: AppDataStore,
	notificationStore?: NotificationStore,
	networkDataStore?: NetworkDataStore,
	profileDataStore?: ProfileDataStore
}
interface State {
	styles?: any,
	language?: string,
	userName?: string,
	imageLoadfailed?: boolean,
	userClass?: string,
	selectedStudent?: any,
	isTourComplete?: boolean
}
@inject('homeDataStore', 'goalsDataStore', 'profileActivityStore',
	'performanceStore', 'headerDataStore', 'appDataStore', 'notificationStore',
	'networkDataStore', 'newDashboardStore', 'newGoalsDataStore', 'profileDataStore')
@observer
export default class ExploreComponent extends Component<Props, State> implements OnClick {
	isNetworkAvailable: boolean = true;
	userId: any;
	userName: any;
	cardRenderer: any;
	cardOrder: any[];

	onClickListItem(rowData: any): void {
		Alert.alert(rowData.title)
	}
	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyle(),
			userName: 'NA',
			imageLoadfailed: false,
			userClass: '',
			selectedStudent: {},
			isTourComplete: true
		};

		this.cardOrder = [
			{
				id: 'goal_time_tracker_widget',
				renderer: this.renderTimeLeftIndicator.bind(this)
			},
			{
				id: 'activity_graph_widget',
				renderer: this.renderActivityCard.bind(this),
				onPress: () => this._goToActivityChartPage()
			},
			{
				id: 'syllabus_progress_widget',
				renderer: this.renderSyllabusCard.bind(this)
			},
			{
				id: 'achievement_index_widget',
				renderer: this.renderIndexCard.bind(this, 'achievement')
			},
			{
				id: 'improvement_index_widget',
				renderer: this.renderIndexCard.bind(this, 'improvement'),
				removable: true
			},
			{
				id: 'sincerity_index_widget',
				renderer: this.renderIndexCard.bind(this, 'sincerity')
			},
			{
				id: 'upcoming_tests_widget',
				renderer: this.renderUpcomingTests.bind(this)
			}
		];
	}

	async componentDidMount() {
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));

		const selectedStudent = await getSelectedStudent();
		if (selectedStudent.goal) {
			await setAllGoals(selectedStudent.goal);
		}

		// App Tour
		const isTourComplete = await hasCompleteAppTour();
		this.setState({
			selectedStudent,
			isTourComplete: !!isTourComplete
		});
		this.props.profileDataStore.setSelectedStudent(selectedStudent);
		await this.hydratePage(selectedStudent);
		await this.redirectPage();
	}

	componentDidUpdate(prevProps: any, prevState: any) {
		if (prevProps.profileDataStore.selectedStudent && prevProps.profileDataStore.selectedStudent.id != this.state.selectedStudent.id) {
			this.setState({
				selectedStudent: prevProps.profileDataStore.selectedStudent
			}, () => {
				this.hydratePage(prevProps.profileDataStore.selectedStudent);
			});
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
	}

	async hydratePage(selectedStudent: any) {
		await this.props.newGoalsDataStore.resetActionGoals();
		this.props.newGoalsDataStore.getGoals(selectedStudent.id);
		this.props.homeDataStore.getUserGoalsListData(true, selectedStudent.id);
		this.props.goalsDataStore.fetchGoals();
		this.props.newDashboardStore.fetchUpcomingTests(selectedStudent.id);

		commonStores.genericTabBarStore.setPageAction(true);

		await this.props.notificationStore.getNotificationCount();
		stores.notificationStore.setNavigationObject(this.props.navigation);

		const userClass = await getClassName();
		this.setState({
			userClass: userClass
		});

		await this.props.profileActivityStore.getProfile();
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	async redirectPage() {
		const notificationData: any = await getNotifcationData();
		await setNotifcationData('');
		switch (notificationData) {
			case 'purchaseCongratulations':
				break;
			case 'challengee-notification':
				break;
			case 'welcome':
				break;
			default:
				break
		}
	}

	renderGoalSelectHeader() {
		const { styles } = this.state;
		const { selectedStudent } = this.props.profileDataStore;
		let studentName = selectedStudent && selectedStudent.name ? selectedStudent.name.split(' ')[0] : '';
		return (
			<View style={[styles.fullWidth, styles.childrenPadding, { marginBottom: 8, marginTop: 17 }]}>
				<Text style={styles.performanceTitle}>{`${studentName ? capitalizeWords(studentName) + '\'s Performance' : ''}`}</Text>
				<GoalSelectorHeader />
			</View>
		);
	}

	_onAddStudent() {
		showModal(
			<AddStudent navigation={this.props.navigation} />,
			{
				hideDialogOnTouchOutside: true,
				positioning: 'center'
			}
		);
	}

	_goToActivityChartPage() {
		navigateSimple(this.props.navigation, 'ActivityChartPage');
	}

	_goToProgressDetails() {
		navigateSimple(this.props.navigation, 'KnowYourProgressPage');
	}

	_openAssignmentsPage() {
		navigateSimple(this.props.navigation, 'AssignmentsPage');
	}

	renderTimeLeftIndicator() {
		const { styles } = this.state;
		return (
			<View style={styles.cardContainer}>
				<TimeIndicatorCard containerStyle={styles.cardStyle} />
			</View>
		);
	}

	renderActivityCard() {
		const { styles } = this.state;
		return (
			<View style={styles.cardContainer}>
				<ActivityChart containerStyle={styles.cardStyle} />
			</View>
		)
	}

	renderSyllabusCard() {
		const { styles } = this.state;
		return (
			<View style={styles.cardContainer}>
				<SyllabusCard containerStyle={styles.cardStyle} />
			</View>
		)
	}

	renderUpcomingTests() {
		const { styles } = this.state;
		return (
			<View style={[styles.cardContainer, styles.upcomingCardContainer]}>
				<UpcomingTestsWidget navigation={this.props.navigation} containerStyle={styles.cardStyle} />
			</View>
		)
	}

	renderIndexCard(indexId: string) {
		const { styles } = this.state
		return (
		  <View style={[styles.cardContainer, {marginBottom: 2}]}>
			<IndexCard indexId={indexId} containerStyle={styles.cardStyle} />
		  </View>
		)
	}

	// @ts-ignore
	createDashboardCardView = ({ id, renderer, onPress }) => {
		let cardView = renderer();
		if (cardView) {
			return (
				<TouchableOpacity
					key={id}
					activeOpacity={1}
					onPress={onPress ? onPress : null}>
					{cardView}
				</TouchableOpacity>
			)
		}
		return cardView
	};

	renderDashboardCards = () => {
		let cardsToDisplay = [];
		for (let i = 0; i < this.cardOrder.length; i++) {
			cardsToDisplay.push(this.createDashboardCardView(this.cardOrder[i]))
		}
		return cardsToDisplay
	};

	render() {
		const { styles, isTourComplete } = this.state;
		const selectedStudent = this.props.profileDataStore.selectedStudent;

		return <RefreshableScrollView
			onRefresh={() => {
				return new Promise(async (resolve, reject) => {
					await this.hydratePage(selectedStudent);
					resolve()
				})
			}}
			style={styles.mainContainer}
			showsVerticalScrollIndicator={false}
		>
			<View style={styles.container}>
				<GreetingsCard onAddStudent={() => this._onAddStudent()} />
				{this.renderGoalSelectHeader()}
				<View>
					<View style={{ width: widthPercentage(90), height: 1 }} />
					{this.renderDashboardCards()}
				</View>
			</View>
			{!isTourComplete && <AppTourComponent student={selectedStudent} /> }
		</RefreshableScrollView>
	}
}

// All Item StyleSheet
const getStyle = () => StyleSheet.create({
	mainContainer: {
		flex: 1,
		backgroundColor: colors.WhiteGrayish
	},
	container: {
		flex: 1,
	},
	headerBackground: {
		height: widthPercentage(75),
		width: widthPercentage(100)
	},
	fullWidth: {
		width: widthPercentage(100)
	},
	childrenPadding: {
		paddingHorizontal: isTablet() ? 20 : 10
	},
	performanceTitle: {
		fontSize: isTablet() ? 21 : 16,
		color: colors.HeaderColor
	},
	cardContainer: {
		marginHorizontal: widthPercentage(5),
		marginVertical: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	upcomingCardContainer: {
		marginHorizontal: 0,
		paddingHorizontal: isTablet() ? 20 : 10,
		paddingVertical: 12,
		backgroundColor: colors.White,
		marginTop: 20
	},
	cardStyle: {
		backgroundColor: colors.White,
		borderRadius: 4,
		elevation: 2,
		shadowColor: 'rgba(0, 0, 0, 0.12)',
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		width: widthPercentage(95),
		padding: 16
	}
});
// All Item StyleSheet
