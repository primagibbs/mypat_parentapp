import React, { Component } from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';
// @ts-ignore
import { Navigation } from 'react-navigation';
import {FeedComponent} from "../others";
import {colors} from "../../common-library/config";
import {isPortrait, verticalScale, widthPercentage} from "../../common-library/common";
import {icons} from "../../common";

interface Props {
  navigation?: Navigation
}
interface State {
  styles?: any
}
export default class FeedComponentPage extends Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state);
    this.state = {
      styles: getStyle()
    }
  }

  render() {
  	const { styles } = this.state;
    return (
			<View style={styles.container}>
				<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground}>
					<View style={styles.heading}>
						<Text style={styles.headingTitle}>Smart Parenting</Text>
						<Text style={styles.itemDesc}>We help you understand your kids better</Text>
					</View>
				</ImageBackground>
				<FeedComponent navigation={this.props.navigation} />
			</View>
    );
  }
}

const getStyle = () => StyleSheet.create({
	container: {
		flex: 1
	},
	headerBackground: {
		flex: 1,
		height: widthPercentage(40),
		width: widthPercentage(100)
	},
	heading: {
		flex: 1,
		marginLeft: verticalScale(10),
		marginBottom: 22
	},
	headingTitle: {
		fontFamily: 'Roboto',
		fontSize: verticalScale(24),
		fontWeight: "500",
		color: colors.White,
		marginTop: 20,
		marginBottom: 10

	},
	itemDesc: {
		fontSize: verticalScale(13),
		color: colors.White,
		marginBottom: 25
	}
});
