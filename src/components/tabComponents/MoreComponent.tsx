import React, { Component } from 'react'
import {
	ScrollView, View, Text, StyleSheet, TouchableOpacity, Image,
	Alert, Linking, Platform, AppState, ImageBackground
} from 'react-native'
import {inject, observer} from 'mobx-react'
// @ts-ignore
import { Navigation } from 'react-navigation'
// @ts-ignore
import BottomSheet from 'react-native-bottomsheet';
import ImagePicker from "react-native-image-crop-picker";
import { colors } from '../../config'
import {logout, resetRouterSimple} from '../../services'
import commonStores from '../../common-library/store'
import {
	widthPercentage, verticalScale, scale, isPortrait,
	isTablet, icons, COMMON_BASE_URL, offlineMsg, SOCIAL_LOGIN_MODE
} from '../../common';
import store, {NetworkDataStore, ProfileDataStore} from '../../store'
import ParentProfileComponent from '../parentsProfile/ParentProfileComponent';
import StudentsComponent from '../parentsProfile/StudentsComponent';

interface Props {
	navigation?: Navigation
	networkDataStore?: NetworkDataStore,
	profileDataStore?: ProfileDataStore
}

interface State {
	editMode: boolean,
	viewMode: boolean,
	profileHeight: any
}

@inject('networkDataStore', 'profileDataStore')
@observer
export default class MoreComponent extends Component<Props, State> {
	profileImage: any;
	constructor(props: Props, state: State) {
		super(props, state);
		this.state = {
			editMode: false,
			viewMode: false,
			profileHeight: 60
		};
	}

	async componentWillMount() {
		commonStores.genericTabBarStore.setPageAction(true);
	}

	async componentDidMount() {
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		})
	}

	// @ts-ignore
	pickCamera({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openCamera({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch((e) => {
			console.log("error", e);
			// @ts-ignore
			alert('Image Upload Cancelled')
		})
	}

	// @ts-ignore
	pickGallery({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch(e => {
			console.log("error", e);
		})
	}

	// @ts-ignore
	openActionSheet({ cropit, circular = false }) {
		const { profileDataStore } = this.props;
		let profileImage = profileDataStore.profileImageUrl;
		if (profileImage && profileImage.length > 0) {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Remove Photo', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 3
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		} else {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 2
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		}
	}

	_onCameraBtnClicked() {
		console.log("onCameraBtnClicked");
		this.openActionSheet({
			cropit: true,
			circular: true
		})
	}

	isFormEditable = (isEdit = false) => {
		this.setState({
			editMode: isEdit
		})
	};

	isViewDetails = (isView = false) => {
		this.setState({
			viewMode: isView
		})
	};

	onProfileLayout = ({ nativeEvent } : { nativeEvent : any }) => {
		console.log('nativeEvent.layout.height', nativeEvent.layout.height)
		this.setState({
			profileHeight: nativeEvent.layout.height
		});
	}

	renderProfileImage = () => {
		const { profileDataStore: { userProfileDetails } } = this.props;
		const { editMode } = this.state;
		const imageView = (
			<View style={{ width: verticalScale(70), height: verticalScale(70), overflow: 'hidden'}}>
				{
					userProfileDetails && userProfileDetails.userImage ? <Image source={{uri: userProfileDetails.userImage}} borderRadius={verticalScale(35)} style={styles.userProfileImage} resizeMode={'cover'} /> :
						<Image source={icons.USER_ICON} style={styles.userProfileImage} resizeMode={'contain'} />
				}
				{editMode ? <Image source={require('../../../images/image_edit_icon.png')} style={styles.userProfileImageEdit} resizeMode='contain' /> : <View/> }
			</View>
		);
		return editMode ? (
			<TouchableOpacity onPress={() => this._onCameraBtnClicked()}>
				{imageView}
			</TouchableOpacity>
		) : imageView
	};

	render() {
		const { profileHeight } = this.state;
		return (
			<ScrollView style={styles.container}>
				<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground} />
				{/* <View style={{flex: 1, position: 'absolute', zIndex: 9999, top: verticalScale(45), alignSelf: 'center'}}>
					{this.renderProfileImage()}
				</View> */}
				<View style={styles.pageBody}>
					<ParentProfileComponent
						_onLayout={this.onProfileLayout}
						isViewDetails={this.isViewDetails}
						isFormEditable={this.isFormEditable}
						navigation={this.props.navigation} />
				</View>
				<View style={{ flex: 1, marginTop: profileHeight - 40 }}>
					<StudentsComponent navigation={this.props.navigation} />
				</View>
			</ScrollView>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F2F7FC'
	},
	headerBackground: {
		height: isTablet() ? widthPercentage(30): widthPercentage(40),
		width: widthPercentage(100)
	},
	pageBody: {
		position: 'absolute',
		top: isTablet() ? verticalScale(84) : verticalScale(84),
		left: 0,
		right: 0,
		backgroundColor: colors.Transparent,
		paddingVertical: 5
	},
	separator: {
		marginTop: 10,
		marginBottom: 10
	},
	userProfileImage: {
		width: verticalScale(70),
		height: verticalScale(70)
	},
	userProfileImageEdit: {
		width: 28,
		height: 28,
		position: 'absolute',
		bottom: 0,
		right: 0
	}
});
