import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { ParentTabBarStore } from '../../common-library/store'

interface Props {
  tabIndex: number
  component: any
  navigation?: any
	parentTabBarStore?: ParentTabBarStore
}
interface State {
}

@inject('parentTabBarStore')
@observer
export class TabBarViewComponent extends Component<Props, State> {
  constructor(props: Props, state: State) {
    super(props, state)
  }

  render() {
    const { parentTabBarStore, navigation, tabIndex } = this.props;
    if (parentTabBarStore.isPageHyderated(tabIndex)) {
      return <this.props.component navigation={navigation} key={tabIndex} />
    } else {
      return null
    }
  }
}
