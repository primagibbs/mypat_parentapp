import React from 'react'
import { inject, observer } from 'mobx-react'

import { DialogStore } from '../store'
import { DialogComponent } from '../components'
// @ts-ignore
import { Navigation } from 'react-navigation'

interface Props {
  dialogStore?: DialogStore
}

/**
 * TODO: Make this more reusable. For now, this behaves like an alert popup.
 */
@inject('dialogStore')
@observer
export class DialogContainer extends React.Component<Props> {

  renderDialog() {
    const { dialogStore } = this.props;
    return <DialogComponent
      showDialog={dialogStore.isVisible}
      onContinueClicked={() => dialogStore.onButtonClick(dialogStore.logic)}
      titleText={dialogStore.titleText}
      descriptionText={dialogStore.descriptiontext}
      buttonLabel={dialogStore.buttonLabel}
      imageUrl={dialogStore.imageUrl}
      cancelButtonVisible={dialogStore.cancelButtonVisible}
      onCancelClicked={() => dialogStore.hideDialog()}
      hideDialogOnTouchOutside={dialogStore.hideDialogOnTouchOutside}
    />
  }

  render() {
    const { dialogStore } = this.props;
    return dialogStore.isVisible ? this.renderDialog() : null
  }

}
