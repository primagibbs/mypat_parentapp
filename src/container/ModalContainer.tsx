import React from 'react'
import { View, StyleSheet, Modal, TouchableWithoutFeedback } from 'react-native'
import { inject, observer } from 'mobx-react'

import { CustomModalStore } from '../store'

interface Props {
	customModalStore?: CustomModalStore,
}
interface State {
	styles?: any
}

@inject('customModalStore')
@observer
export class ModalContainer extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props);
		this.state = {
			styles: getStyles()
		};
	}

	renderModalView = () => {
		const positionStyle = {
			top: 'flex-start',
			center: 'center',
			bottom: 'flex-end',
		};
		const { customModalStore } = this.props;
		const { styles } = this.state;
		return (<Modal
			transparent
			visible={customModalStore.isVisible}
			animationType={customModalStore.animationType}
			onRequestClose={() => { if (customModalStore.onBackClicked) { customModalStore.onBackClicked(); } else { customModalStore.hideModal(); } }}
		>
			<TouchableWithoutFeedback onPress={() => { if (customModalStore.hideDialogOnTouchOutside) { customModalStore.hideModal(); } }}>
				<View style={[styles.modalContainer, { justifyContent: positionStyle[customModalStore.positioning] }]}>
					{customModalStore.element}
				</View>
			</TouchableWithoutFeedback>
		</Modal>);
	};

	render() {
		return this.renderModalView();
	}

}

const getStyles = () => StyleSheet.create({
	dialog: {
		margin: 0
	},
	modalContainer: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.65)'
	}
});
