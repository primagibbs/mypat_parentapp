import { Model, ModelConfiguration, Schema, getModelProperties } from './types'

const getGenericAPISchema = (name): Schema => ({
  name,
  isSchemaLess: true,
  properties: getModelProperties({
    body: 'string'
  })
})

const getGenericModel = (name): Model => ({
  schema: getGenericAPISchema(name),
  configuration: {}
})

export interface ITestTimerModel {
  key?: string
  startTime?: Date
  systemTime: Date,
  mobileUpTime: number,
  endTime?: Date,
  testDuration: number
}

// tslint:disable-next-line:variable-name
export const TestTimerModel = {
  schema: {
    name: 'TestTimerModel',
    primaryKey: 'id',
    properties: {
      id: 'string',
      startTime: 'date',
      systemTime: 'date',
      mobileUpTime: 'int',
      expiry: 'date',
      testDuration: 'int' // mins
    }
  },
  configuration: {}
}

export interface ITestAttemptModel {
  id?: string
  uniqueTestKey?: string
  qId?: string
  expiry?: Date
  body: string
  systemTime: Date
  isConsumed?: boolean
  attemptId: any
}

// tslint:disable-next-line:variable-name
export const TestAttemptModel = {
  schema: {
    name: 'TestAttemptModel',
    primaryKey: 'id',
    properties: {
      id: 'string',
      body: 'string',
      expiry: 'date',
      systemTime: 'date',
      isConsumed: 'bool',
      attemptId: 'string'
    }
  },
  configuration: {}
}

// tslint:disable-next-line:variable-name
export const TestAnswerModel = {
  schema: {
    name: 'TestAnswerModel',
    properties: {
      id: 'string', // test id, assignment id and qId
      uniqueTestKey: 'string', // test id, assignment id
      qId: 'string',
      expiry: 'date',
      body: 'string',
      systemTime: 'date'
    }
  },
  configuration: {}
}

// tslint:disable-next-line:variable-name
export const DownloadedTestModel = {
  schema: {
    name: 'DownloadedTestModel',
    primaryKey: 'id',
    properties: {
      id: 'string',
      expiry: 'date',
      body: 'string',
      type: 'string',
      status: 'string',
      // subject: 'string',
      attempt: 'string',
      testName: 'string',
      category: 'string',
      subCategory: 'string',
      attempted: 'bool'
    }
  },
  configuration: {}
};

// tslint:disable-next-line:variable-name
export const DownloadTestModel = {
  schema: {
    name: 'DownloadTestModel',
    isSchemaLess: true,
    isProcessable: true,
    primaryKey: 'id',
    properties: {
      id: 'string',
      body: 'string',
      addedAt: 'date',
      modifiedAt: 'date',
      expiry: 'date',
      isProcessed: 'bool'
    }
  },
  configuration: {}
}

const modelsMap = {}

export const NEWS_MODEL = 'News'
export const PROFILE_MODEL = 'profile'
export const PERFORMANCE_DASHBOARD_MODEL = 'testScore'
export const PROGRESS_DASHBOARD_MODEL = 'ProgressDashboard'
export const LATEST_TEST_RESULT_MODEL = 'lastUserPaperAttempt'
export const USER_GOALS_MODEL = 'userGoals'
export const REDIS_DATA_MODEL = 'RedisData'
export const GOALS_DATA_MODEL = 'getGoals'
export const GET_CLASS_MODEL = 'getClass'
export const GET_ACTIVITY_MODEL = 'activity'

export const models = [
  NEWS_MODEL,
  PROFILE_MODEL,
  PERFORMANCE_DASHBOARD_MODEL,
  PROGRESS_DASHBOARD_MODEL,
  LATEST_TEST_RESULT_MODEL,
  USER_GOALS_MODEL,
  REDIS_DATA_MODEL,
  GOALS_DATA_MODEL,
  GET_CLASS_MODEL,
  GET_ACTIVITY_MODEL
]

const getAPIModel = name => {
  if (!modelsMap[name]) {
    const model = getGenericModel(name)
    modelsMap[name] = model
  }
  return modelsMap[name]
}
export default getAPIModel
