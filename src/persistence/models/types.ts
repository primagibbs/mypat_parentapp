const defaultProperties = {
  id: 'string',
  addedAt: 'date',
  modifiedAt: 'date',
  expiry: 'date'
}

export interface Schema {
  name: string
  primaryKey?: string
  properties: any
  isSchemaLess?: boolean
  isProcessable?: boolean
}

export interface ModelConfiguration {
  isCache?: boolean
}

export interface Model {
  schema: Schema
  configuration: ModelConfiguration
}

export const getModelProperties = (props: any) => ({
  ...props,
  ...defaultProperties
})