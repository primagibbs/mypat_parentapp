import Realm from 'realm';
import moment from 'moment'
import lzstring from 'lz-string'
import { cloneDeep, get, set, values } from 'lodash'
import RNElapsedRealtime from 'react-native-elapsed-realtime';

import { API_IDS, IS_DEV_ENV } from '../../common';
import commonStores from '../../common-library/store'
import stores from '../../store'
import { Model, ModelConfiguration, Schema } from '../models/types'
import getAPIModel, {
  PROGRESS_DASHBOARD_MODEL,
  ITestTimerModel,
  ITestAttemptModel,
  TestTimerModel,
  TestAttemptModel,
  TestAnswerModel,
  DownloadedTestModel,
  DownloadTestModel,
  GOALS_DATA_MODEL,
  GET_CLASS_MODEL,
  GET_ACTIVITY_MODEL
} from '../models/api-models'
import {
  downloadImage
} from '../../services/ImageService'
import { showLoader, hideLoader, createNotification } from '../../services';

/*
  This it to purge cache, simply increasing the count shall purge cache
  TODO: Still need to figure it out in detail.
*/
const REAL_SCHEMA_VERSION = 1;

// the only instance of realm for better performance
let realmInstance: any;
let isSyncing = false;

const DEFAULT_EXPIRATION_TIME = '1d'

export const persistenceModelAPIMap = {
  [API_IDS.GOAL_PROGRESS]: getAPIModel(PROGRESS_DASHBOARD_MODEL),
  [API_IDS.GET_GOALS]: getAPIModel(GOALS_DATA_MODEL),
  [API_IDS.GET_CLASSES]: getAPIModel(GET_CLASS_MODEL),
  [API_IDS.GET_ACTIVITY]: getAPIModel(GET_ACTIVITY_MODEL)
};

function str2ab(str) {
  const buf = new ArrayBuffer(str.length * 2) // 2 bytes for each char
  const bufView = new Uint16Array(buf)
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i)
  }
  return buf
}

interface RealmConfig {
  schema?: Schema[]
  encryptionKey?: ArrayBuffer
  inMemory?: boolean
  deleteRealmIfMigrationNeeded?: boolean
}

let progressValue

export function getProgressValue(){
     return this.progressValue
}

interface SaveAPIData {
  pk?: string
  isUpdate?: boolean
  expiry?: string
  delete?: boolean
  stringifyBody?: boolean
  setProcessed?: boolean
}

interface Query {
  filter: string,
  params: any[]
}

const defaultRealmConfig: Partial<RealmConfig> = {
  // encryptionKey: str2ab('thelastjedi'),
  // inMemory: IS_DEV_ENV
  // deleteRealmIfMigrationNeeded: IS_DEV_ENV
}

const getRealmConfig = (): Partial<RealmConfig> => {
  const schemas = Object.keys(persistenceModelAPIMap)
    .map(apiId => persistenceModelAPIMap[apiId].schema)
  return {
    schema: [
      ...schemas,
      TestTimerModel.schema,
      TestAnswerModel.schema,
      TestAttemptModel.schema,
      DownloadedTestModel.schema
    ],
    ...defaultRealmConfig
  }
};

export const getPersistenceModelByAPI = (apiId) => {
  return persistenceModelAPIMap[apiId]
};

export const convertToArray = (input) => {
  let dataArray = input
  if (!Array.isArray(input)) {
    dataArray = [input]
  }
  return dataArray
};

const partitionTimeString = timeString => {
  const reg = /(\d*)(\w)/g
  let match
  const matches = []
  // tslint:disable-next-line:no-conditional-assignment
  while (match = reg.exec(timeString)) {
    if (match && match.length && match.length > 2) {
      matches.push(match[1])
      matches.push(match[2])
    }
  }
  return matches
}

const getTimePartsFromExpirationString = (expirationString: string) => {
  const reg = /(\d*\w)/g
  let match
  const matches = []
  // tslint:disable-next-line:no-conditional-assignment
  while (match = reg.exec(expirationString)) {
    matches.push(match[0])
  }
  return matches.map(timeString => partitionTimeString(timeString))
};

const rollMomentDate = (timePart, date) => {
  if (date) {
    const [num, type] = timePart
    // validate num
    if (isNaN(num)) {
      throw new Error('Invalid number part of time string: ' + num)
    }
    switch (type) {
      case 'd':
        return date.add(num, 'days')
      case 'h':
        return date.add(num, 'hours')
      case 'm':
        return date.add(num, 'minutes')
      default:
        return date.add(num, 'days')
    }
  }
  return null
}

/**
 * Only rolling by days, hours and minutes is allowed.
 * You may use strings like, '4d', '5h', '20m'.
 * You may also choose to combine these strings as '4d5h', '5h20m' or even '4d5h20m'
 * @param expirationString
 * @param time the time from when the expiration date is to be created
 */
const getExpirationDate = (expirationString: string, time?) => {
  const parts = getTimePartsFromExpirationString(expirationString || DEFAULT_EXPIRATION_TIME)
  // validate all time parts
  const now = time ? moment(time) : moment()
  const finalDate = parts.reduce((rolledDate, timePart) => {
    return rollMomentDate(timePart, rolledDate)
  }, now)
  return finalDate.toDate()
}

class RealmItem {
  realm: any;
  next: any;
  constructor(realm: any) {
    this.realm = realm
  }
}

class OperandQuery extends RealmItem {
  _label: string
  constructor(realm, label: string) {
    super(realm)
    this._label = label
  }
  filter(filter: string, params) {
    const filterQuery = new FilterQuery(this, filter, params)
    this.next = filterQuery
    return filterQuery
  }
  get operand() {
    return this._label
  }
}

class FilterQuery extends RealmItem {
  _filter: string;
  _params: any[];
  next: any;

  constructor(realm, filter, params) {
    super(realm)
    this._filter = filter
    this._params = params
  }
  and() {
    const operandQuery = new OperandQuery(this.realm, 'AND')
    this.next = operandQuery
    return operandQuery
  }
  or() {
    const operandQuery = new OperandQuery(this.realm, 'OR')
    this.next = operandQuery
    return operandQuery
  }

  get filter() {
    return this._filter
  }
  get params() {
    return this._params
  }
}

class RealmQuery {
  next
  filter(filter: string, params) {
    const filterQuery = new FilterQuery(this, filter, params)
    this.next = filterQuery
    return filterQuery
  }
  public getQuery() {
    if (!this.next) {
      return null
    }
    const does$$Exists = str => str.indexOf('$$') >= 0
    let paramCounter = -1
    let filter = ''
    let params = []
    let pointer: FilterQuery|OperandQuery = this.next
    // tslint:disable-next-line:no-conditional-assignment
    while (pointer) {
      if (pointer instanceof FilterQuery) {
        let pointerFilter = pointer.filter
        let has$$ = does$$Exists(pointerFilter)
        while (has$$) {
          pointerFilter = pointerFilter.replace('$$', `$${++paramCounter}`)
          has$$ = does$$Exists(pointerFilter)
        }
        filter += pointerFilter
        if (pointer.params && pointer.params.length) {
          pointer.params.forEach(param => params.push(param))
        }
      } else if (pointer instanceof OperandQuery) {
        filter += ` ${pointer.operand} `
      }
      pointer = pointer.next
    }
    if ((paramCounter === -1 ? 0 : paramCounter + 1 ) !== params.length) {
      throw new Error('Realm query param variables and params count do not match.')
    }
    return {
      filter, params
    }
  }
}

const open = async (model: Model) => {
  if (realmInstance) {
    return realmInstance
  }
  return new Promise<any>((resolve, reject) => {
    Realm.open(getRealmConfig())
      .then(r => {
        realmInstance = r
        resolve(realmInstance)
      })
      .catch(error => console.warn(error))
  })
}

const write = async (model: Model, action: Function) => {
  const realm = await open(model)
  realm.write(() => action(realm))
  return true
}

/**
 * Handles the create and update of schemaless.
 * @param model
 * @param data
 * @param options
 *  stringifyBody: false, if no stringification is required
 *  setProcessed: true, sets the isProcessed field to true
 *  isUpate: true, updates the object instead of create
 * @param time
 */
const schemalessCreation = (model: Model, data: any, options: SaveAPIData, time: Date): Function => {
  const expirationTime = getExpirationDate(options && options.expiry ? options.expiry : null, time)
  return realm => {
    data.forEach(item => {
      let body
      if (options.stringifyBody !== false) {
        body = JSON.stringify(item)
      } else {
        body = item.body
      }
      const dataToSave: any = {
        id: options.pk, body, modifiedAt: time, expiry: expirationTime
      }
      if (model.schema.isProcessable) {
        dataToSave.isProcessed = false
      }
      if (options.setProcessed) {
        dataToSave.isProcessed = true
      }
      if (!options.isUpdate) {
        dataToSave.addedAt = time
        realm.create(model.schema.name, dataToSave)
      } else {
        if (!dataToSave.addedAt) {
          dataToSave.addedAt = time
        }
        realm.create(model.schema.name, dataToSave, options.isUpdate)
      }
    })
  }
}

const create = async (model: Model, data: any, options: SaveAPIData) => {
  const isSchemaLess: boolean = model.schema.isSchemaLess
  const time = new Date()
  let action: Function
  if (isSchemaLess) {
    action = schemalessCreation(model, data, options, time)
  } else {
    const expirationTime = getExpirationDate(options && options.expiry ? options.expiry : null, time)
    action = (realm): void => data.forEach(
      (item) => {
        if (options.delete !== false) {
          realm.delete(realm.objects(model.schema.name))
        }
        item.modifiedAt = time
        item.expiry = expirationTime
        if (!item.id) {
          item.id = item._id
        }
        if (options.pk) {
          item.id = options.pk
        } else {
          item.id = time.toISOString()
        }
        if (options.isUpdate) {
          if (!item.addedAt) {
            item.addedAt = time
          }
          realm.create(model.schema.name, item, true)
        } else {
          item.addedAt = time
          realm.create(model.schema.name, item)
        }
      }
    )
  }

  return await write(model, action)
}

const fetch = async (model: Model, id?: string, showExpired: boolean = true) => {
  const realm = await open(model)
  const time = new Date()
  if (realm) {
    if (!id) {
      const query = new RealmQuery()
      query.filter('expiry > $$', [time])
      const queryFilter = query.getQuery()
      const results = realm
        .objects(model.schema.name)
      if (!showExpired) {
        return results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
      } else {
        return results
      }
    } else {
      const query = new RealmQuery()
      const queryPart = query.filter('id == $$', [id])
      if (!showExpired) {
        queryPart.and()
          .filter('expiry > $$', [time])
      }
      const queryFilter = query.getQuery()
      const results = realm
        .objects(model.schema.name)
      return results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
    }
  }
  return null
}

const remove = async (model: Model, options: { id?: string, deleteExpired?: boolean } = {}) => {
  // console.warn('REMOVING MODEL: ', model.schema.name, options.id)
  const time = new Date()
  const query: any = new RealmQuery()
  let queryPointer: any
  let predicateCount = 0
  if (options.deleteExpired !== undefined) {
    queryPointer = query.filter('expiry < $$', [time])
    predicateCount++
  }
  if (options.id !== undefined) {
    if (predicateCount > 0) {
      queryPointer = queryPointer.and()
      queryPointer = queryPointer.filter('id == $$', [options.id])
    } else {
     // console.warn('query pointer has been set')
      query.filter('id == $$', [options.id])
    }
    predicateCount++
  }
  const queryFilter = query.getQuery()
  const action: Function = (realm): void => {
    const results = realm.objects(model.schema.name)
    let data
    if (predicateCount > 0) {
      data = results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
    } else {
      data = results
    }
    if (data) {
      // console.log('#### deleted', model.schema.name)
      realm.delete(data)
    }
  }
  return await write(model, action)
}

/**
 * Saves and returns true if saved successfully.
 * @param apiId The API whose data is to be stored.
 * @param data the data that is to be stored
 * @param options: Refer SaveAPIData interface
 */
export const saveAPIData = async (apiId: string, data: any, options: SaveAPIData = {}) => {
  // console.warn('saveAPIData Called: ', JSON.stringify(data, null, 2))
  const model: Model = getPersistenceModelByAPI(apiId)
  if (data && model) {
    const dataArray = convertToArray(data)
    return await create(model, dataArray, options)
  }
  return false
}

/**
 * Returns null if the data doesn't exist.
 * @param apiId The id of the API
 * @param id? the pk of the object to be
 * @param options? isArrayResponse, id
 */
export const getAPIData = async (apiId: string, options: any = {}) => {
  const model: Model = getPersistenceModelByAPI(apiId)
  if (!model) {
    return null
  }
  if (commonStores.networkDataStore.isNetworkConnected) {
    const hasBeenDeleted = remove(model, { id: options.id, deleteExpired: true })
  }
  let data: any = await fetch(model, options.id)
  if (model.schema.isSchemaLess) {
    if (data && data.length) {
      data = data.map(item => JSON.parse(item.body))
    }
  }
  return data && Object.keys(data).length ?
    (options.isArrayResponse === false ? data[0] : data) :
    null
}

/**
 * Returns true if the data is successfully removed.
 * @param apiId The API_ID of the data
 * @param id? the pk of the data to be removed.
 */
export const deleteAPIData = async (apiId: string, id?: any) => {
  // console.warn('Delete Api id', apiId)
  const model: Model = getPersistenceModelByAPI(apiId)
  // console.warn('delete model', model)
  if (!model) {
    return null
  }
  try {
    return await remove(model, { id })
  } catch (error) {
    // console.warn('Error to delete test', error)
    return false
  }
}

export const isTestDownloaded = async (testId: string, assignmentId: string) => {
  const model: Model = getPersistenceModelByAPI(API_IDS.DOWNLOAD_TEST)
  const key = testId + (!assignmentId ? undefined : assignmentId)
  // const allDownloaded = await fetch(model)
  let data: any = await fetch(model, key)
  if (model.schema.isSchemaLess) {
    if (data && data.length) {
      data = data.map(item => JSON.parse(item.body))
    }
  }
  return (data && !!Object.keys(data).length)
}

export const deleteDownloadedTest = async (testId: string, assignmentId: string) => {
  console.log('testId**********', testId, assignmentId)
  const model: Model = DownloadedTestModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  await remove(model, { id: key, deleteExpired: false })
  const tests = await fetch(model, key)
  console.log('testId key', key, tests)
  return await remove(DownloadedTestModel, { id: key })

  // console.log('****** downloaded test length', key, JSON.stringify(tests[0], null, 2))
};

export const deleteTest = async (testId: string, assignmentId: string) => {
  // console.log('deleteTest testId ++++++++++++++++++++++', testId, assignmentId)
  const key = testId + (!assignmentId ? undefined : assignmentId)
  // Gaurav: Be careful, the following two lines are similar but not same
  await deleteAPIData(API_IDS.DOWNLOAD_TEST, key)
  // console.warn('deleteAPIData first')
  // await deleteDownloadedTest(testId, assignmentId)
  // console.warn('deleteAPIData second')
  // return await deleteAPIData(API_IDS.DOWNLOADED_TEST, key)
}

export const getMobileUpTime = async () => {
  const duration = await RNElapsedRealtime.getElapsedRealtime()
  return duration * 1000
}

const saveModel = async (key: string, obj: any, model: Model, options?: SaveAPIData) => {
  let saveOptions: any = {
    pk: key,
    expiry: '1000d'
  }
  if (options) {
    saveOptions = {
      ...options,
      ...saveOptions
    }
  }
  await create(model, [{
    ...obj,
    id: key
  }], saveOptions)
}

export const deleteTestTime = async (testId: string, assignmentId: string) => {
  const model: Model = TestTimerModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  let vals = await fetch(model)
  // console.warn('remaining test times before', vals && vals.length)
  await remove(model, { id: key })
  vals = await fetch(model)
  // console.warn('remaining test times after', vals && vals.length)
}

export const saveTestTime = async (testId: string, assignmentId: string, obj: ITestTimerModel, options?: SaveAPIData) => {
  const model: Model = TestTimerModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  await deleteTestTime(testId, assignmentId)
  await saveModel(key, obj, model, { delete: false })
  // const all = await fetch(model, key)
  // console.warn('saved Test times', all && all.length)
}

export const getTestTimeData = async (testId: string, assignmentId: string) => {
  const model: Model = TestTimerModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const saveTestTimeData = await fetch(model, key)
  // console.warn('key: ', key)
  // console.warn('getTestTimeData(): ', JSON.stringify(saveTestTimeData, null, 2))
  if (saveTestTimeData && saveTestTimeData.length) {
    const data = realmToPojo(saveTestTimeData)
    return data
  }
  return null
}

const deleteModelById = async (testId: string, assignmentId: string, model: Model) => {
  // console.warn('deleting test time')
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  await remove(model, { id: key, deleteExpired: false })
}

export const validateTestTime = async (testId: string, assignmentId: string, obj: ITestTimerModel) => {
  const model: Model = TestTimerModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  // console.warn('validation key', key)
  const validTestTime = await fetch(model, key)
  // console.warn('validTestTime', JSON.stringify(validTestTime.map(item => realmToPojo(item)), null, 2))
  // console.warn('existing', JSON.stringify(obj, null, 2))
  if (validTestTime && validTestTime.length) {
    const existingTime: ITestTimerModel = validTestTime[0]
    const diffInUpTime: number = obj.mobileUpTime - existingTime.mobileUpTime
    // console.warn('diff in up time', diffInUpTime)
    const diffInSystemTime: number = obj.systemTime.getTime() - existingTime.systemTime.getTime()
    // console.warn('diff in system time', diffInSystemTime)
    if (Math.abs(diffInUpTime - diffInSystemTime) < (10 * 1000)) {
      return true
    }
  }
  return false
}

export const getRemainingTime = async (testId: string, assignmentId: string, nowDate?: Date) => {
  const model: Model = TestTimerModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  const validTestTime = await fetch(model, key)
  if (validTestTime && validTestTime.length) {
    const existingTime: ITestTimerModel = validTestTime[0]
    const now: any = nowDate || new Date()
    // const testEndMillis = (existingTime.startTime.getTime() + (existingTime.testDuration * 1000))
    // const remainingMillis = testEndMillis - (now.getTime())
    const timeDeduct = Math.trunc((now.getTime() - existingTime.startTime.getTime()) / 1000) * 1000
    return existingTime.testDuration * 1000 - (Math.trunc(timeDeduct / 1000) * 1000)
  }
  return null
}

/**
 *
 * Save RedisData Locally
 */
export const saveTestAttemptData = async (testId: string, assignmentId: string, attemptId: string, obj: any) => {
  const model: Model = TestAttemptModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  await create(model, [{
    id: key,
    body: JSON.stringify(obj),
    attemptId: attemptId
  }], { pk: key, expiry: '1000d' })
}

export const deleteTestAttemptData = async (testId: string, assignmentId: string) => {
  const model: Model = TestAttemptModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  await remove(model, { id: key, deleteExpired: false })
}

export const saveTestAttempt = async (testId: string, assignmentId: string, obj: ITestAttemptModel) => {
  const model: Model = TestAttemptModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  await saveModel(key, obj, model, { isUpdate: obj.isConsumed })
}

export const saveTestAnswer = async (testId: string, assignmentId: string, obj: ITestAttemptModel) => {
  const model: Model = TestAnswerModel
  const qId = obj.qId
  const uniqueTestKey = testId + (!assignmentId ? undefined : assignmentId)
  const key = testId + (!assignmentId ? undefined : assignmentId) + qId
  obj.uniqueTestKey = uniqueTestKey
  await saveModel(key, obj, model, { delete: false })

  const savedAnswers = await fetchAllTestAnswers(testId, assignmentId)
}

export const deleteTestAttempt = async (testId: string, assignmentId: string) => {
  const model: Model = TestAttemptModel
  await deleteModelById(testId, assignmentId, model)
}

export const deleteTestAnswer = async (testId: string, assignmentId: string) => {
  const model: Model = TestAnswerModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  const options: any = {
    id: key
  }
  const time = new Date()
  const query: any = new RealmQuery()
  let queryPointer: any
  let predicateCount = 0
  if (options.id !== undefined) {
    queryPointer = query.filter('uniqueTestKey == $$', [options.id])
  }
  const queryFilter = query.getQuery()
  const action: Function = (realm): void => {
    const results = realm.objects(model.schema.name)
    const data = results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
    if (data) {
      realm.delete(data)
    }
  }
  return await write(model, action)
}

export const fetchAllTestAttempts = async () => {
  const model: Model = TestAttemptModel
  const realm = await open(model)
  const query = new RealmQuery()
  query.filter('isConsumed == $$', [false])
  const queryFilter = query.getQuery()
  const results = realm
    .objects(model.schema.name)
  return await results.filtered.apply(
    results,
    [queryFilter.filter, ...queryFilter.params]
  )
}

const fetchAttemptedTest = async (model: Model, attemptId?: string) => {
  const realm = await open(model)
  if (realm) {
    const query = new RealmQuery()
    query.filter('attemptId == $$', [attemptId])
    const queryFilter = query.getQuery()
    const attempts = realm
      .objects(model.schema.name)
    return attempts.filtered.apply(attempts, [queryFilter.filter, ...queryFilter.params])
  }
  return null
}

export const fetchTestAttempts = async (attemptId: string) => {
  const model: Model = TestAttemptModel
  try {
    const attempts = await fetchAttemptedTest(model, attemptId)
    if (attempts && attempts.length) {
      return attempts
    }
    return []
  } catch (error) {
    // integrate sentry
  }
  return []
}

const fetchTestAnswers = async (model: Model, id?: string) => {
  const realm = await open(model)
  const time = new Date()
  if (realm) {
    const query = new RealmQuery()
    query.filter('uniqueTestKey == $$', [id])
    const queryFilter = query.getQuery()
    const results = realm
      .objects(model.schema.name)
    return results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
  }
  return null
}

export const fetchAllTestAnswers = async (testId, assignmentId) => {
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const model: Model = TestAnswerModel
  try {
    const answers = await fetchTestAnswers(model, key)
    if (answers && answers.length) {
      return answers
    }
    return []
  } catch (error) {
    // integrate sentry
  }
  return []
}

export const deleteLocalTestAnswers = async (testId: string, assignmentId: string) => {
  const model: Model = TestAnswerModel
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  try {
    await remove(model, { id: key })
  } catch (error) {
    console.warn(error)
  }
}

// TODO: Remove this, it can be removed
export const fetchAllTestAnswersByQId = async (testId, assignmentId, qId) => {
  const model: Model = TestAttemptModel
  const realm = await open(model)
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const query = new RealmQuery()
  query
    .filter('id == $$', [key])
    .and()
    .filter('qId == $$', [qId])
  const queryFilter = query.getQuery()
  let results = realm.objects(model.schema.name)
  results = results.filtered.apply(
    results,
    [queryFilter.filter, ...queryFilter.params]
  )
  return results
}

export const removeTestAnswersByQId = async (qId) => {
  const model: Model = TestAnswerModel
  const query = new RealmQuery()
  query.filter('qId == $$', [qId])
  const queryFilter = query.getQuery()
  const action: Function = (realm): void => {
    const results = realm.objects(model.schema.name)
    const data = results.filtered.apply(results, [queryFilter.filter, ...queryFilter.params])
    if (data) {
      realm.delete(data)
    }
  }
  return await write(model, action)
}

export const saveDownloadedTest = async (testId: string, assignmentId: string, test: any) => {
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  await create(model, [test], { pk: key, expiry: '1000d', delete: false })
  // const tests = await fetch(DownloadedTestModel, key)
  return true
}

export const getDownloadedTestItemData = async (testId: string, assignmentId: string) => {
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const tests = await fetch(DownloadedTestModel, key)
  if (tests && tests.length) {
    return realmToPojo(tests[0])
  }
  return null
}

export const isTestItemDownloaded = async (testId: string, assignmentId: string) => {
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const tests = await fetch(DownloadedTestModel, key)
  if (tests && tests.length) {
    return true
  }
  return false
}

export const updateDownloadedTestItemData = async (testId: string, assignmentId: string, status: string) => {
  // This is to update status of assignment chap test. To be done in offline mode. Status to be modified from status 'live' to 'past'
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const tests = await fetch(DownloadedTestModel, key)
  // console.warn('##updateDownloadedTestItemData()')
  if (tests && tests.length) {
    let data = realmToPojo(tests[0])
    let body = JSON.parse(data.body)

    data.status = status
    body.status = status
    data.body = JSON.stringify(body)
    await saveModel(key, data, model, { isUpdate: true, delete: false })
  }
}

export const updateDownloadedTestAttempted = async (testId: string, assignmentId: string, attemptData: any, overwrite) => {
  // To update test data parameter - attempted to true when user starts a test
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const downloadedData = await fetch(DownloadedTestModel, key)

  // console.warn('updateDownloadedTestAttempted: KEY ', key)
  // console.warn('updateDownloadedTestAttempted: ', JSON.stringify(downloadedData, null, 2))

  if (downloadedData && downloadedData.length) {
    let data = downloadedData[0]
    data = realmToPojo(data)
    let body = JSON.parse(data.body)
    // console.warn('BEFORE ADDING Attempt Count: ', body.attempts.length)
    if (!overwrite) {
      body.attempts.push(attemptData)
    } else if (attemptData) {
      body.attempts.forEach((attempt, index) => {
        if (attempt._id === attemptData._id && attempt.status === 'notFinished') {
          body.attempts[index] = attemptData
        }
      })
    }
    data.body = JSON.stringify(body)
    await saveModel(key, data, model, { isUpdate: true, delete: false })
  }

  const key1 = testId + (!assignmentId ? undefined : assignmentId)
  const downloadedData1 = await fetch(DownloadedTestModel, key)
  if (downloadedData && downloadedData.length) {
    let data = downloadedData[0]
    data = realmToPojo(data)
    let body = JSON.parse(data.body)
    // console.warn('After ADDING Attempt Count: ', body.attempts.length)
  }
}

export const isTestAttempted = async (testId: string, assignmentId: string) => {
  // To check if test is attempted or note
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const downloadedData = await fetch(DownloadedTestModel, key)
  let attempts
  if (downloadedData && downloadedData.length) {
    const data = downloadedData[0]
    let body = JSON.parse(data.body)
    attempts = body.attempts
  }

  if (attempts && attempts.length > 0) {
    return attempts[attempts.length - 1].attempted
  } else {
    return false
  }
}

export const isTestSubmitted = async (testId: string, assignmentId: string) => {
  // To check if test is attempted or note
  const model: Model = DownloadedTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const downloadedData = await fetch(DownloadedTestModel, key)
  if (downloadedData && downloadedData.length) {
    const data = downloadedData[0]
    let body = JSON.parse(data.body)
    return body.isSubmitted
  } else {
    return false
  }
}

export const fetchDownloadedTestsByCategory = async (options = { category: null }) => {
  const model: Model = DownloadedTestModel
  const realm = await open(model)
  const query = new RealmQuery()
  query
    .filter('category == $$', [options.category])
  const queryFilter = query.getQuery()
  let results = realm.objects(model.schema.name)
  results = results.filtered.apply(
    results,
    [queryFilter.filter, ...queryFilter.params]
  )
  return results ? results.map(item => JSON.parse(item.body)) : []
}

export const fetchDownloadedTestsCountBySubCategory = async (options = { category: null }) => {
  const model: Model = DownloadedTestModel
  const realm = await open(model)
  const query = new RealmQuery()
  query
    .filter('subCategory == $$', [options.category])
  const queryFilter = query.getQuery()
  let results = realm.objects(model.schema.name)
  results = results.filtered.apply(
    results,
    [queryFilter.filter, ...queryFilter.params]
  )
  return results ? results.length : 0
}
export const fetchDownloadedTestsCountByCategory = async (options = { category: null }) => {
  const model: Model = DownloadedTestModel
  const realm = await open(model)
  const query = new RealmQuery()
  query
  .filter('category == $$', [options.category])
  const queryFilter = query.getQuery()
  let results = realm.objects(model.schema.name)
  results = results.filtered.apply(
    results,
    [queryFilter.filter, ...queryFilter.params]
  )
  return results ? results.length : 0
}

export function realmToPojo(realmObject) {
  return JSON.parse(JSON.stringify(realmObject))
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

async function downloadOptionsImages({ qId, uncompressed, testId, assignmentId, questionMap, key }) {
  const folderKey: string = testId + (!assignmentId ? undefined : assignmentId)
  await asyncForEach(uncompressed.contentImages[qId][key], async (contentURI) => {
    const fileDetails = await downloadImage(contentURI, testId, assignmentId)
    // console.warn('new file', fileDetails)
    const qData = questionMap[qId]
    const contents: any = qData.data.qData.content
    // console.warn('contents', JSON.stringify(contents, null, 2))
    await asyncForEach(contents, async (content) => {
      if (key.indexOf('optionsContent') > -1 || key.indexOf('matrixOptionContent') > -1) {
        const value = get(content, key)
        // console.warn('Content download', key, value)
        if (value) {
          set(content, key, value.replace(contentURI, `./${folderKey}/${fileDetails.file}`))
        }
      }
    })
  })
}

async function downloadQuestionContent({ qId, uncompressed, testId, assignmentId, questionMap, key }) {
  const folderKey: string = testId + (!assignmentId ? undefined : assignmentId)
  await asyncForEach(uncompressed.contentImages[qId][key], async (contentURI) => {
    const fileDetails = await downloadImage(contentURI, testId, assignmentId)
    // console.warn('new file', fileDetails)
    const qData = questionMap[qId]
    const contents: any = qData.data.qData.content
    // console.warn('contents', JSON.stringify(contents, null, 2))
    contents.forEach((content, index) => {
      // console.warn('content 123', JSON.stringify(content, null, 2))
      if (content && content[key]) {
        contents[index][key] = content[key].replace(contentURI, `./${folderKey}/${fileDetails.file}`)
        // console.warn('content[key]', contents[index][key])
      }
    })
  })
}

const downloadImages = ({
  qId, uncompressed, testId, assignmentId, questionMap, updateProgress
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (uncompressed.contentImages[qId]) {
        const start = async () => {
          const keys = Object.keys(uncompressed.contentImages[qId])
          await asyncForEach(keys, async (contentKey) => {
            if (contentKey === 'questionContent') {
              await downloadQuestionContent({
                qId, uncompressed, testId, assignmentId, questionMap, key: 'questionContent'
              })
            }
            // console.warn(contentKey, 'for loop')
            if (contentKey.indexOf('optionsContent') > -1 || contentKey.indexOf('matrixOptionContent') > -1) {
              await downloadOptionsImages({
                qId, uncompressed, testId, assignmentId, questionMap, key: contentKey
              })
            }
          })
          // await downloadContentItem({
          //   qId, uncompressed, testId, assignmentId, questionMap, key: 'solutionContent'
          // })
          updateProgress()
          resolve()
        }
        start()
      } else {
        updateProgress()
        resolve()
      }
    } catch (error) {
      reject()
    }
  })
}

export async function updateDownloadedTest(testId: string, assignmentId: string, isTestAttempt: boolean) {
  const model: Model = getPersistenceModelByAPI(API_IDS.DOWNLOAD_TEST)
  const key: string = testId + (!assignmentId ? undefined : assignmentId)
  const downloadedTestRealm = await fetch(model, key)
  if (downloadedTestRealm && downloadedTestRealm.length) {
    // console.warn('downloaded test mila')
    // console.warn('downloadedTestRealm[0].isProcessed: ', downloadedTestRealm[0].isProcessed)
    if (downloadedTestRealm[0].isProcessed) {
      return null
    }
   // showLoader()
    const test = cloneDeep(realmToPojo(downloadedTestRealm[0]))
    let uncompressed = lzstring.decompressFromEncodedURIComponent(test.body) // rishabh
    // console.warn('decompressed the new body')
    uncompressed = JSON.parse(uncompressed)
    const questionMap = {}
    const promises = []
    const questionDatas = null
    uncompressed.questionData.qDatas.forEach((qData: any) => {
      questionMap[qData.data.qId] = qData
    })
    if (uncompressed.contentImages) {
      let progress = 0
      let totalImagesToDownload = 0
      const updateProgress = () => {
        progress++
        const progressPercentage = (progress / totalImagesToDownload) * 100
        stores.testDownloadMetaStore.setProgress(progressPercentage)
        if (isTestAttempt) {
        showLoader()
        hideLoader()
        }
      // console.warn(`Downloading images for test - ${parseInt(`${progressPercentage}`, 10)}%`)
      }
      Object.keys(uncompressed.contentImages).forEach((qId) => {
        const totalImagesInQuestion = values(uncompressed.contentImages[qId])
          .map(item => item && item.length || 0)
          .reduce((sum, a) => sum + a, 0)
         totalImagesToDownload += 1
        promises.push(downloadImages({
          qId, uncompressed, testId, assignmentId, questionMap, updateProgress
        }))
      })
    //  console.warn('total progress', totalImagesToDownload)
      try {
        await Promise.all(promises)
      } catch (error) {
     //   console.warn('Image downloading and replacement failed.')
      }
      // console.warn(key, JSON.stringify(uncompressed.questionData, null, 2))
     // console.warn('before delete')
      await deleteTest(testId, assignmentId)
     // console.warn('after delete')
      // let tests = await fetch(model, key)
      const stringifiedUncompressed = JSON.stringify(uncompressed)
      await saveModel(key, {
        ...test,
        body: JSON.stringify(lzstring.compressToEncodedURIComponent(stringifiedUncompressed)),
        isProcessed: true
      }, model, { isUpdate: true, delete: false, stringifyBody: false, setProcessed: true })
      // const tests = await fetch(model, key)
      // hideLoader()
      // console.warn('newly saved test', tests.length)
      stores.testDownloadMetaStore.resetProgress()
      return stringifiedUncompressed
    }
  }
  // console.warn('kuchh bhi ni mila')
  return null
}

export const saveDownloadTest = async (testId: string, assignmentId: string, test: any) => {
  const model: Model = DownloadTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  console.warn('saveDownloadTest')
  await create(model, [test], { pk: key, expiry: '1000d', delete: false, stringifyBody: false })
  return true
}

export const getDownloadTestData = async (testId: string, assignmentId: string) => {
  const model: Model = DownloadTestModel
  const key = testId + (!assignmentId ? undefined : assignmentId)
  const tests = await fetch(model, key)
  if (tests && tests.length) {
    return realmToPojo(tests[0])
  }
  return null
};

export default {
  saveAPIData,
  getAPIData
}
