import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, Keyboard, Dimensions, BackHandler, Image, TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { H1, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons
} from '../common';
import { colors } from '../config'
import {goBack, navigateSimple} from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { ProfileDataStore, AppDataStore } from '../store'
import {getStudents} from "../utils";

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	isError?: boolean
	styles?: any,
	countryCode?: any
	addCountryCode?: boolean
	cca2?: any
}

@inject('profileDataStore')
@observer
export class AddEmailPage extends Component<Props, State> {
	valRef: any = undefined;
	pageType: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			isError: false,
			styles: getStyle(),
			countryCode: '',
			addCountryCode: false,
			cca2: ''
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	onTextChange(userInfo: any) {
		this.setState({
			isError: false,
			textValue: userInfo.toLowerCase()
		});
	}

	render() {
		const { isError, styles , textValue } = this.state;
		return (
			<View style={styles.container}>
				<KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
					<View>
						<H1 style={styles.item} numberOfLines={1}>Enter your email ID</H1>
						<Text style={styles.itemDesc}>We will share important updates related to your child on this email ID</Text>
						<View style={styles.textFieldStyle}>
							<View style={{flex: 1, justifyContent: 'center'}}>
								<TextInput
									value={textValue}
									onChangeText={text => this.onTextChange(text)}
									autoCorrect={false}
									autoCapitalize={'none'}
									maxLength={100}
									onFocus={() => this.onFocus()}
									blurOnSubmit={false}
									placeholder={'Enter Here'}
									keyboardType={'email-address'}
									returnKeyType='done'
									ref={name => { this.valRef = name }}
									onSubmitEditing={this._onSubmitEditing.bind(this, 'email')}
									style={[styles.textInput, {
										borderColor: isError
											? 'rgba(235, 25, 25, 0.54)'
											: 'rgba(153, 153, 153, 0.54)',
										borderWidth: 1
									}]}
								/>
								{
									isError && <Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5), marginTop: 6}}>Please enter the correct email</Text>
								}
							</View>
						</View>
					</View>
				</KeyboardAwareScrollView>
				<View style={styles.bottomView}>
					<View style={{flex: 1, justifyContent: 'center'}}>
						<TouchableWithoutFeedback onPress={this.onBackPress.bind(this)}>
							<View style={{flexDirection: 'row'}}>
								<Image
									style={styles.arrowIcon}
									source={icons.LEFT_ARROW} />
								<Image
									style={[styles.arrowIcon, {
										marginRight: verticalScale(8)
									}]}
									source={icons.LEFT_ARROW} />
								<Text style={styles.back}>Back</Text>
							</View>
						</TouchableWithoutFeedback>
					</View>
					<View style={{flex: 1, justifyContent: 'center'}}>
						<TouchableWithoutFeedback onPress={this.onNextPress.bind(this)}>
							<View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
								<Text style={styles.next}>Next</Text>
								<Image
									style={[styles.arrowIcon, {
										marginLeft: verticalScale(8),
										left: 0
									}]}
									source={icons.RIGHT_ARROW} />
								<Image
									style={[styles.arrowIcon, {
										left: 0
									}]}
									source={icons.RIGHT_ARROW} />
							</View>
						</TouchableWithoutFeedback>
					</View>
				</View>
			</View>
		)
	}

	validateEmail(email: string) {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	validateField() {
		const { textValue } = this.state;
		if (textValue && !this.validateEmail(textValue)) {
			this.setState({
				isError: true
			});
			return false;
		} else {
			return true;
		}
	}

	async _onSubmitEditing(field: string) {
		if (this.validateField() && field === 'email') {
			await this.onNextPress()
		}
	}

	onFocus() {
		this.setState({
			isError: false
		})
	}

	onBackPress() {
		this.onBackPressed();
	}

	async onNextPress() {
		Keyboard.dismiss();
		const { profileDataStore } = this.props;
		const { textValue } = this.state;

		if (textValue) {
			if (this.validateField()) {
				await profileDataStore.updateEmail({
					email: textValue
				});
			}
		} else {
			const students = await getStudents();
			console.log("##STUDENT", students);
			if (students && students.length > 0) {
				if (students 
					&& students.length === 1
					&& !students[0].isVerified) {
					let data = students[0];
					navigateSimple(this.props.navigation, 'AddProfileViewPage', {data});
				}else{
					navigateSimple(this.props.navigation, 'HomePage');
				}
			} else {
				navigateSimple(this.props.navigation, 'AddProfile1Page');
			}
		}
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30
	},
	textInput: {
		width: '100%',
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: verticalScale(14),
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		paddingVertical: 10
	},
	userIcon: {
		flex: 1,
		width: 50,
		height: 50,
		marginTop: 5,
		marginBottom: 15,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	itemDesc: {
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4),
		lineHeight: verticalScale(21),
		color: colors.ContentColor,
		marginTop: verticalScale(16),
		width: widthPercentage(80)
	},
	textFieldStyle: {
		marginTop: verticalScale(30)
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1)
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 1,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		fontFamily: 'Roboto',
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	}
});
