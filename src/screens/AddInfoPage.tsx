import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, TouchableOpacity, ScrollView, Keyboard, Dimensions,
	BackHandler, KeyboardAvoidingView, Image, TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react'
// @ts-ignore
import BottomSheet from 'react-native-bottomsheet';
import { H1, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons
} from '../common';
import { getDeviceCountryCode } from '../utils';
import { colors } from '../config'
import { goBack } from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { AppDataStore, ProfileDataStore } from '../store'
import ImagePicker from "react-native-image-crop-picker";

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	isError?: boolean
	styles?: any,
	countryCode?: any
	addCountryCode?: boolean
	cca2?: any
}

@inject('profileDataStore', 'appDataStore')
@observer
export class AddInfoPage extends Component<Props, State> {
	valRef: any = undefined;
	pageType: any;
	profileImage: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			isError: false,
			styles: getStyle(),
			countryCode: '',
			addCountryCode: false,
			cca2: ''
		}
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		const countryCode = getDeviceCountryCode();
		this.setState({countryCode : countryCode});
		profileDataStore.setNavigationObject(this.props.navigation);

		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onTextChange(userInfo: any) {
		this.setState({ textValue: userInfo });
	}

	// @ts-ignore
	pickCamera({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openCamera({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch((e) => {
			console.log("error", e);
			// @ts-ignore
			alert('Image Upload Cancelled')
		})
	}

	// @ts-ignore
	pickGallery({ cropit, circular }) {
		const { profileDataStore } = this.props;
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: cropit,
			cropperCircleOverlay: circular,
			compressImageMaxWidth: 640,
			compressImageMaxHeight: 480,
			compressImageQuality: 0.5,
			compressVideoPreset: 'MediumQuality',
			includeBase64: true,
			includeExif: true
		}).then(async (image: any) => {
			this.profileImage = {
				uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height
			};
			await profileDataStore.uploadImage('jpeg', this.profileImage.uri)
		}).catch(e => {
			console.log("error", e);
		})
	}

	// @ts-ignore
	openActionSheet({ cropit, circular = false }) {
		const { profileDataStore } = this.props;
		let profileImage = profileDataStore.profileImageUrl;
		if (profileImage && profileImage.length > 0) {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Remove Photo', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 3
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		} else {
			BottomSheet.showBottomSheetWithOptions({
				options: ['Camera', 'Gallery', 'Cancel'],
				title: 'Select Photo',
				dark: false,
				cancelButtonIndex: 2
			}, (value: number) => {
				if (value === 0) {
					this.pickCamera({ cropit, circular })
				} else if (value === 1) {
					this.pickGallery({ cropit, circular })
				}
			})
		}
	}

	onCameraBtnClicked() {
		this.openActionSheet({
			cropit: true,
			circular: true
		})
	}

	hasNumber(name: string) {
		return /\d/.test(name);
	}

	validateField() {
		const { textValue } = this.state;
		if (textValue.length === 0 || this.hasNumber(textValue)) {
			this.setState({
				isError: true
			});
			return false;
		} else {
			return true;
		}
	}

	async _onSubmitEditing(field: string) {
		if (this.validateField() && field === 'name') {
			await this.onNextPress()
		}
	}

	onFocus() {
		this.setState({
			isError: false
		})
	}

	async onNextPress() {
		Keyboard.dismiss();
		const { profileDataStore } = this.props;

		if (this.validateField()) {
			await profileDataStore.updateName({
				name: this.state.textValue
			});
		}
	}

	render() {
		const { isError, styles , textValue } = this.state;
		const { profileDataStore } = this.props;

		return (
			<View style={styles.container}>
				<KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
					<View>
						<H1 style={styles.item} numberOfLines={1}>Help us know you better</H1>
						<View style={styles.textFieldStyle}>
							<TouchableWithoutFeedback onPress={() => this.onCameraBtnClicked()}>
								<View style={styles.userIcon}>
									{
										profileDataStore.profileImageUrl ? <Image
											style={styles.iconImage}
											source={{uri: profileDataStore.profileImageUrl}}
											resizeMode='contain'
										/> :
										<View>
											<Image
												style={styles.iconImage}
												source={icons.USER_ICON}
												resizeMode='contain'
											/>
											<Image source={require('../../images/image_edit_icon.png')} style={styles.userProfileImageEdit} resizeMode='contain' />
										</View>
									}

								</View>
							</TouchableWithoutFeedback>
							<View style={{flex: 1, justifyContent: 'center'}}>
								<TextInput
									value={textValue}
									onChangeText={text => this.onTextChange(text)}
									autoCorrect={false}
									autoCapitalize={'none'}
									maxLength={100}
									onFocus={() => this.onFocus()}
									blurOnSubmit={false}
									placeholder={'Your name'}
									returnKeyType='done'
									ref={name => { this.valRef = name }}
									onSubmitEditing={this._onSubmitEditing.bind(this, 'name')}
									style={[styles.textInput, {
										borderColor: isError
											? 'rgba(235, 25, 25, 0.54)'
											: 'rgba(153, 153, 153, 0.54)',
										borderWidth: 1
									}]}
								/>
								{
									isError && <Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5), marginTop: 6}}>Please enter your name</Text>
								}
							</View>
						</View>
					</View>
				</KeyboardAwareScrollView>
				<View style={styles.bottomView}>
					<View style={{flex: 1, justifyContent: 'center'}}>
						<TouchableWithoutFeedback onPress={this.onNextPress.bind(this)}>
							<View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
								<Text style={styles.next}>Next</Text>
								<Image
									style={[styles.arrowIcon, {
										marginLeft: verticalScale(8)
									}]}
									source={icons.RIGHT_ARROW} />
								<Image
									style={styles.arrowIcon}
									source={icons.RIGHT_ARROW} />
							</View>
						</TouchableWithoutFeedback>
					</View>
				</View>
			</View>
		)
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30
	},
	textInput: {
		width: '100%',
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		paddingVertical: 10
	},
	userIcon: {
		flex: 1,
		marginTop: verticalScale(5),
		marginBottom: verticalScale(15),
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		overflow: 'hidden'
	},
	iconImage: {
		width: verticalScale(50),
		height: verticalScale(50),
		borderRadius: verticalScale(25)
	},
	textFieldStyle: {
		marginTop: widthPercentage(5)
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1)
	},
	bottomView: {
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2)
	},
	next: {
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	userProfileImageEdit:{
		width: 20,
		height: 20,
		position: 'absolute',
		bottom: 0,
		right: 0
	}
});
