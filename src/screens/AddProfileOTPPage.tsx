import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, TouchableOpacity, ScrollView, Keyboard, Dimensions,
	BackHandler, Platform, Image, TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react';
import OtpInputs from 'react-native-otp-inputs'
import { H3, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons, isTablet
} from '../common';
import { colors } from '../config'
import {navigateSimple, goBack, showModal} from '../services'
import { ProfileDataStore} from '../store'
import {CodeMismatch} from "./CodeMismatch";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import RNOtpVerify from "react-native-otp-verify";

interface Props {
	profileDataStore?: ProfileDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	errors?: any
	styles?: any,
	studentInfo?: string
	mobileExists?: any,
	mobileNumber?: string,
	countdown?: number,
	timer?: any
}

@inject('profileDataStore')
@observer
export class AddProfileOTPPage extends Component<Props, State> {
	pageType: any;
	otpRef: any;
	otpProgress: number = 0;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			errors: {
				name: ''
			},
			timer: null,
			countdown: 30,
			styles: getStyle(),
			studentInfo: this.props.navigation.getParam('studentInfo'),
			mobileExists: this.props.navigation.getParam('mobileExists'),
			mobileNumber: this.props.navigation.getParam('mobileNumber')
		};
		this.otpRef = React.createRef();
	}

	async componentDidMount() {
		if (Platform.OS === 'android') {
			await this.startListeningForOtp();
		}
		const { profileDataStore, navigation } = this.props;
		profileDataStore.setNavigationObject(navigation);
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}
		if(profileDataStore.isOTPWrong){
			profileDataStore.setIsOTPWrong();
		}

		await profileDataStore.sendStudentOTP(this.state.studentInfo);
		let timer = setInterval(this.tick, 1000);

		this.setState({
			timer
		})
	}

	otpHandler = async (message: string) => {
		const readOtp = /(\d{4})/g.exec(message);
		if (readOtp) {
			const otp = readOtp[1];
			if (otp) {
				this.otpRef.current.state.otpCode = otp.split("").map((num) => {
					return num.toString();
				});
				RNOtpVerify.removeListener();
				Keyboard.dismiss();
				await this.onNextPress(otp);
			}
		}
	};

	startListeningForOtp = () =>
		RNOtpVerify.getOtp()
			.then(p => RNOtpVerify.addListener(this.otpHandler))
			.catch(p => console.log(p));

	tick = () => {
		if (this.state.countdown !== 0) {
			this.setState({
				countdown: this.state.countdown - 1
			});
		} else {
			clearInterval(this.state.timer);
		}
	};

	resendOTP = async () => {
		Keyboard.dismiss();
		const { studentInfo } = this.state;
		const { profileDataStore } = this.props;

		if (profileDataStore.isOTPWrong) {
			profileDataStore.setIsOTPWrong();
		}

		let timer = setInterval(this.tick, 1000);
		this.setState({
			timer,
			countdown: 30
		});

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}

		await profileDataStore.sendStudentOTP(studentInfo);
	};

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
		if (Platform.OS === 'android') {
			RNOtpVerify.removeListener();
		}
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	onOTPChange(userInfo: any) {
		this.setState({ textValue: userInfo });
		if(userInfo.length == 4){
			this.onNextPress(userInfo);
		}
	}

	onAccessBtnPress = () => {
		Keyboard.dismiss();

		navigateSimple(this.props.navigation, 'AddProfile2Page', {
			studentInfo: this.state.studentInfo,
			mobileExists: this.state.mobileExists,
			mobileNumber: this.state.mobileNumber
		});
	};

	_onCodeMismatch() {
		const data = {
			icon: icons.SMILEY_SAD_ICON,
			heading: 'Code Mismatch!',
			subHeading: 'Please recheck the number & code entered',
			btnText: 'OK'
		};
		showModal(
			<CodeMismatch
				navigation={this.props.navigation}
				nextPage={'AddProfile1Page'}
				data={data} />,
			{
				hideDialogOnTouchOutside: false,
				positioning: 'center'
			}
		);
	}

	_renderAccessCodeOption = () => <View style={this.state.styles.renderLoginStyle}>
		<View style={{ flexDirection: 'row' }}>
			<TouchableOpacity onPress={this.onAccessBtnPress.bind(this)} style={{ height: verticalScale(50) }}>
				<Text style={{
					fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
					color: colors.ParentBlue,
					marginTop: 20
				}}>Verify with Parent Access code</Text>
			</TouchableOpacity>
		</View>
	</View>;

	_maskNumber(number: string) {
		return number.replace(/\d(?=\d{4})/g, "*");
	}

	render() {
		const { countdown, styles, mobileNumber } = this.state;
		const isError = this.props.profileDataStore.isOTPWrong;
		return (
			<>
				<View style={styles.wizard}>
					<View style={{flexDirection: 'row'}}>
						<View style={styles.stepOne}>
							<Image source={icons.STEP_DONE_ICON} style={{
								width: verticalScale(12),
								height: verticalScale(9)
							}} resizeMode={'cover'} />
						</View>
						<View style={styles.stepTwoContainer}>
							<View style={styles.stepOne}>
								<Text style={{
									textAlign: 'center',
									fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									color: colors.White}}>2</Text>
							</View>
							<View style={{
								width: verticalScale(4),
								height: verticalScale(5),
								marginLeft: verticalScale(22),
								backgroundColor: colors.ParentBlue}} />
						</View>
						<Text style={styles.wizardText}>Verify Student</Text>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 30}}>
							<View style={styles.pendingStep}>
								<Text style={{fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									textAlign: 'center',
									color: 'rgba(51, 51, 51, 0.54)'}}>3</Text>
							</View>
						</View>
					</View>
					<View>

					</View>
					<Image
						style={styles.profileLine}
						source={icons.STUDENT_PROFILE_LINE2_ICON}
						resizeMode='cover'
					/>
				</View>
				<View style={styles.container}>
					<KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' behavior="padding">
						<View>
							<H3 style={styles.item} numberOfLines={1}>Verify with OTP</H3>
							<View>
								<Text style={[styles.itemDesc, {
									marginBottom: verticalScale(13)
								}]}>OTP has been sent on the mobile no. or email ID that the student uses to log into myPAT account.</Text>
								<Text style={styles.itemDesc}>{this._maskNumber(mobileNumber)}</Text>
							</View>
							<View style={styles.textFieldStyle}>
								<View style={{ flexDirection: 'row', position: 'relative', width: widthPercentage(90)}}>
									<View>
										<OtpInputs
											ref={this.otpRef}
											clearTextOnFocus={true}
											inputStyles={[styles.otpInput, isError ? styles.errorInput : '']}
											inputContainerStyles={styles.otpInputContainer}
											handleChange={code => this.onOTPChange(code)}
											numberOfInputs={4}
										/>
									</View>
									<View style={{justifyContent: 'flex-end', paddingBottom: 0}}>
										{
											countdown !== 0 && <Text style={{
												fontWeight: 'bold',
												color: colors.SubHeaderColor,
												minWidth: verticalScale(62),
												textAlign: 'center',
												fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
												marginLeft: verticalScale(10),
												lineHeight: verticalScale(18)}}>{`00:${countdown < 10 ? '0' + countdown : countdown}`}</Text>
										}
										{
											countdown === 0 && <TouchableOpacity onPress={() => this.resendOTP()}>
												<Text style={{
													fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.2),
													fontFamily: 'Roboto',
													color: colors.ParentBlue,
													textAlign: 'center',
													marginLeft: verticalScale(10),
													lineHeight: verticalScale(13)}}>Resend OTP</Text>
											</TouchableOpacity>
										}
									</View>
								</View>
								{
									isError && <View style={{flexDirection: 'row'}}>
										<Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.4), marginTop: 6}}>Wrong OTP entered.</Text>
										<TouchableOpacity onPress={this.resendOTP.bind(this)}>
											<Text style={{color: colors.ParentBlue, fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4), marginTop: 6}}>Try again.</Text>
										</TouchableOpacity>
									</View>
								}
							</View>
							{/* <TouchableOpacity onPress={this.onNextPress.bind(this)}>
								<Text style={styles.styleButton}>
									VERIFY NOW
								</Text>
							</TouchableOpacity> */}
						</View>
						{this._renderAccessCodeOption()}
					</KeyboardAwareScrollView>
					{this.props.profileDataStore.isCodeMismatch && this._onCodeMismatch()}
					<View style={styles.bottomView}>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onBackPress.bind(this)}>
								<View style={{flexDirection: 'row'}}>
									<Image
										style={styles.arrowIcon}
										source={icons.LEFT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											marginRight: verticalScale(8)
										}]}
										source={icons.LEFT_ARROW} />
									<Text style={styles.back}>Back</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</View>
			</>
		)
	}

	onFocus() {
		this.setState({
			errors: {
				name: ''
			}
		})
	}

	onBackPress() {
		this.onAccessBtnPress();
	}

	async onNextPress(otp: any) {
		Keyboard.dismiss();
		const { profileDataStore } = this.props;
		await profileDataStore.findStudentByOTP(this.state.studentInfo, otp);
		if(this.props.profileDataStore.isOTPWrong){
			this.otpRef.current.reset();
		}else{
			profileDataStore.setIsOTPWrong();
		}
	}
}

const getStyle = () => StyleSheet.create({
	wizard: {
		backgroundColor: 'white',
		paddingTop: heightPercentage(6),
		width: widthPercentage(100),
		paddingLeft: 8,
		paddingRight: 8
	},
	stepTwoContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	stepOne: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: verticalScale(20),
		backgroundColor: colors.ParentBlue,
		textAlign: 'center',
		textAlignVertical: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		color: 'white',
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2)
	},
	pendingStep: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: 10,
		marginBottom: 4,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		textAlign: 'center',
		textAlignVertical: 'center',
		color: 'rgba(51, 51, 51, 0.54)',
		backgroundColor: colors.White,
		shadowColor: 'rgba(0, 0, 0, 0.25)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: isTablet() ? 2 : 4,
	},
	wizardText: {
		marginLeft: 14,
		paddingTop: 5,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		color: colors.HeaderColor
	},
	profileLine: {
		width: widthPercentage(100),
		height: 3,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		backgroundColor: colors.White,
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30,
		paddingBottom: 60
	},
	textInput: {
		width: widthPercentage(80),
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		borderWidth: 1,
		borderRadius: 4,
		paddingLeft: 10
	},
	itemDesc: {
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: colors.ContentColor,
		marginTop: verticalScale(13),
		marginBottom: verticalScale(17),
		width: widthPercentage(80)
	},
	itemSkip: {
		fontSize: 15,
		color: colors.PrimaryBlue,
		padding: 2,
		margin: 7,
		marginTop: 16,
		alignItems: 'flex-end',
		textAlign: 'right'
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1)
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: colors.White,
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		fontFamily: 'Roboto',
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	styleButton: {
		width: widthPercentage(86),
		marginTop: 20,
		marginBottom: verticalScale(15),
		padding: 10,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4,
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4),

		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 2 : 4,
		shadowRadius: 2
	},
	otpInputContainer: {
		borderBottomWidth: 0,
		width: verticalScale(37),
		height: verticalScale(36),
		margin: 0,
		marginRight: verticalScale(6)
	},
	otpInput: {
		fontSize: verticalScale(14),
		width: verticalScale(37),
		height: verticalScale(36),
		justifyContent: 'center',
		borderWidth: 1,
		borderRadius: 4,
		marginBottom: 0,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		paddingTop: 0,
		paddingBottom: 0
	}
});
