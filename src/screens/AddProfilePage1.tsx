import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, TouchableOpacity, ScrollView, Keyboard, Dimensions,
	BackHandler, Platform, Image, TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { H3, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons, isTablet
} from '../common';
import {getStudents} from '../utils';
import { colors } from '../config'
import {goBack, resetRouterSimple} from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { ProfileDataStore, AppDataStore } from '../store'
import { KEYBOARD_INPUT_OFFSET } from '../common/Constant';

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	styles?: any,
	isError?: boolean
	countryCode?: any
	previousPage?: boolean
}

@inject('profileDataStore', 'appDataStore')
@observer
export class AddProfilePage1 extends Component<Props, State> {
	valRef: any = undefined;
	pageType: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: this.props.navigation.getParam('studentInfo') ? this.props.navigation.getParam('studentInfo') : '',

			isError: false,
			styles: getStyle(),
			countryCode: '',
			previousPage: this.props.navigation.getParam('page') ? this.props.navigation.getParam('page') : ''
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = async () => {
		const { navigation } = this.props;
		const { previousPage } = this.state;
		const students = await getStudents();
		if (students && students.length > 0) {
			if (previousPage) {
				return goBack(navigation);
			} else {
				resetRouterSimple(navigation, 0, 'HomePage');
			}
		} else {
			return goBack(navigation);
		}
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);
		if (profileDataStore.isCodeMismatch) {
			profileDataStore.setCodeMismatch();
		}
		if (profileDataStore.isStudentAdded) {
			profileDataStore.setIsStudentAdded();
		}

		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	onTextChange(userInfo: any) {
		this.setState({ textValue: userInfo.toLowerCase() });
	}

	validateField() {
		if (this.state.textValue.length === 0) {
			this.setState({
				isError: true
			});
			return false;
		}
		return true;
	}

	async _onSubmitEditing(field: string) {
		if(this.validateField() && field === 'studentInfo') {
			await this.onNextPress();
		}
	}

	onFocus() {
		this.setState({
			isError: false
		})
	}

	async onBackPress() {
		await this.onBackPressed();
	}

	async onNextPress() {
		Keyboard.dismiss();

		if (this.validateField()) {
			const { profileDataStore } = this.props;
			await profileDataStore.validateStudentInfo(this.state.textValue);
		}
	}

	render() {
		const { styles, textValue, isError } = this.state;
		return (
			<>
				<View style={styles.wizard}>
					<View style={{flexDirection: 'row'}}>
						<View style={styles.stepOneContainer}>
							<View style={styles.stepOne}>
								<Text style={{
									textAlign: 'center',
									fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									color: colors.White}}>1</Text>
							</View>
							<View style={{width: verticalScale(4), height: verticalScale(5), backgroundColor: colors.ParentBlue}} />
						</View>
						<Text style={styles.addStudentText}>Add Student</Text>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 30}}>
							<View style={styles.pendingStep}>
								<Text style={{fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									textAlign: 'center',
									color: 'rgba(51, 51, 51, 0.54)'}}>2</Text>
							</View>
							<View style={styles.pendingStep}>
								<Text style={{fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									textAlign: 'center',
									color: 'rgba(51, 51, 51, 0.54)'}}>3</Text>
							</View>
						</View>
					</View>

					<Image
						style={styles.profileLine}
						source={icons.STUDENT_PROFILE_LINE1_ICON}
					/>
				</View>
				<View style={styles.container}>
					<KeyboardAwareScrollView 
						showsVerticalScrollIndicator={false} 
						keyboardShouldPersistTaps='handled'
						enableOnAndroid={true}
						extraHeight={ KEYBOARD_INPUT_OFFSET }>
						<View>
							<H3 style={styles.item} numberOfLines={1}>Help us locate the student account</H3>
							<Text style={styles.itemDesc}>Enter the Mobile no./Email ID/ Enrollment no that the student uses to log into myPAT account.</Text>
							<View style={styles.textFieldStyle}>
								<View style={{flex: 1, justifyContent: 'center'}}>
									<TextInput
										value={textValue}
										onChangeText={text => this.onTextChange(text)}
										autoCorrect={false}
										autoCapitalize={'none'}
										maxLength={100}
										onFocus={() => this.onFocus()}
										blurOnSubmit={false}
										placeholder={'Enter here'}
										returnKeyType='done'
										ref={name => { this.valRef = name }}
										onSubmitEditing={this._onSubmitEditing.bind(this, 'studentInfo')}
										style={[styles.textInput, {
											borderColor: isError
												? 'rgba(235, 25, 25, 0.54)'
												: 'rgba(153, 153, 153, 0.54)',
											borderWidth: 1
										}]}
									/>
									{
										isError && <Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5), marginTop: 6}}>Please enter mobile no./ email id or enrollment no</Text>
									}
								</View>
							</View>
						</View>
					</KeyboardAwareScrollView>
					<View style={styles.bottomView}>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onBackPress.bind(this)}>
								<View style={{flexDirection: 'row'}}>
									<Image
										style={styles.arrowIcon}
										source={icons.LEFT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											marginRight: verticalScale(8)
										}]}
										source={icons.LEFT_ARROW} />
									<Text style={styles.back}>Back</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onNextPress.bind(this)}>
								<View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
									<Text style={styles.next}>Next</Text>
									<Image
										style={[styles.arrowIcon, {
											marginLeft: verticalScale(8),
											left: 0
										}]}
										source={icons.RIGHT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											left: 0
										}]}
										source={icons.RIGHT_ARROW} />
								</View>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</View>
			</>
		)
	}
}

const getStyle = () => StyleSheet.create({
	wizard: {
		backgroundColor: 'white',
		paddingTop: heightPercentage(6),
		width: widthPercentage(100)
	},
	stepOneContainer: {
		marginLeft: verticalScale(22),
		alignItems: 'center',
		justifyContent: 'center'
	},
	stepOne: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		backgroundColor: colors.ParentBlue,
		justifyContent: 'center'
	},
	addStudentText: {
		paddingTop: 5,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		color: colors.HeaderColor,
		marginLeft: 14
	},
	pendingStep: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		justifyContent: 'center',
		marginLeft: 10,
		marginBottom: 4,
		backgroundColor: colors.White,
		...Platform.select({
			ios: {
				shadowColor: 'rgba(0, 0, 0, 0.25)',
				shadowOffset: { width: 2, height: 2 },
				shadowOpacity: 0.8,
				shadowRadius: 2,
			},
			android: {
				elevation: isTablet() ? 2 : 4
			},
		})
	},
	profileLine: {
		width: widthPercentage(100),
		height: 3,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30
	},
	textInput: {
		fontFamily: 'Roboto',
		width: '100%',
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		paddingVertical: 10
	},
	itemDesc: {
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: colors.ContentColor,
		marginTop: 5,
		marginBottom: 5,
		width: widthPercentage(80)
	},
	textFieldStyle: {
		marginTop: widthPercentage(5)
	},
	item: {
		fontFamily: 'Roboto',
		fontSize: verticalScale(16),
		lineHeight: verticalScale(16),
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1)
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: colors.White,
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6),
		...Platform.select({
			ios: { fontFamily: 'Arial', },
			android: { fontFamily: 'Roboto' }
		})
	}
});
