import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, TouchableOpacity, ScrollView, Keyboard, Dimensions,
	BackHandler, Platform, Image, TouchableWithoutFeedback, KeyboardAvoidingView
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { inject, observer } from 'mobx-react';
import { H3, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons, isTablet
} from '../common';
import { colors } from '../config'
import {goBack, showModal, navigateSimple} from '../services'
import { AppDataStore, ProfileDataStore} from '../store'
import ParentAccessCode from "../components/dashboard/modals/ParentAccessCode";
import {Button} from "../common-library/components";
import {CodeMismatch} from "./CodeMismatch";
import { KEYBOARD_INPUT_OFFSET } from '../common/Constant';

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	errors?: any
	styles?: any,
	studentInfo?: string,
	mobileExists?: boolean,
	mobileNumber?: string
}

@inject('profileDataStore', 'appDataStore')
@observer
export class AddProfilePage2 extends Component<Props, State> {
	pageType: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			errors: {
				name: ''
			},
			styles: getStyle(),
			studentInfo: this.props.navigation.getParam('studentInfo'),
			mobileExists: this.props.navigation.getParam('mobileExists'),
			mobileNumber: this.props.navigation.getParam('mobileNumber')
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	_openParentAccessModal() {
		showModal(
			<ParentAccessCode />,
			{
				hideDialogOnTouchOutside: false,
				positioning: 'center'
			}
		)
	}

	onOTPChange(userInfo: any) {
		this.setState({ textValue: userInfo });
	}

	onOTPBtnPress = () => {
		Keyboard.dismiss();

		navigateSimple(this.props.navigation,'AddProfileOTPPage', {
			studentInfo: this.state.studentInfo,
			mobileExists: this.state.mobileExists,
			mobileNumber: this.state.mobileNumber
		});
	};

	_onCodeMismatch() {
		const data = {
			icon: icons.SMILEY_SAD_ICON,
			heading: 'Code Mismatch!',
			subHeading: 'Please recheck the number & code entered',
			btnText: 'OK'
		};
		showModal(
			<CodeMismatch
				navigation={this.props.navigation}
				nextPage={'AddProfile1Page'}
				data={data}
				params={{
					studentInfo: this.state.studentInfo
				}}
			/>,
			{
				hideDialogOnTouchOutside: false,
				positioning: 'center'
			}
		);
	}

	_renderOtpOption = () => <View style={this.state.styles.renderLoginStyle}>
		<View style={{ flexDirection: 'row' }}>
			<Text style={{
				fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
				color: colors.ContentColor
			}}>Don't have Parent access code?</Text>
			<TouchableOpacity onPress={this.onOTPBtnPress.bind(this)} style={{ height: verticalScale(50) }}>
				<Text style={{
					fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
					color: colors.ParentBlue
				}}> Click here for OTP</Text>
			</TouchableOpacity>
		</View>
	</View>;

	render() {
		const { styles, mobileExists } = this.state;
		return (
			<>
				<View style={styles.wizard}>
					<View style={{flexDirection: 'row'}}>
						<View style={styles.stepOne}>
							<Image source={icons.STEP_DONE_ICON} style={{
								width: verticalScale(12),
								height: verticalScale(9)
							}} resizeMode={'cover'} />
						</View>
						<View style={styles.stepTwoContainer}>
							<View style={styles.stepOne}>
								<Text style={{
									textAlign: 'center',
									fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									color: colors.White}}>2</Text>
							</View>
							<View style={{
								width: verticalScale(4),
								height: verticalScale(5),
								marginLeft: verticalScale(22),
								backgroundColor: colors.ParentBlue}} />
						</View>
						<Text style={styles.wizardText}>Verify Student</Text>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 30}}>
							<View style={styles.pendingStep}>
								<Text style={{fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									textAlign: 'center',
									color: 'rgba(51, 51, 51, 0.54)'}}>3</Text>
							</View>
						</View>
					</View>
					<View>

					</View>
					<Image
						style={styles.profileLine}
						source={icons.STUDENT_PROFILE_LINE2_ICON}
						resizeMode='cover'
					/>
				</View>
				<KeyboardAwareScrollView showsVerticalScrollIndicator={false}
										contentContainerStyle={styles.container} 
										enableOnAndroid={true}
										extraHeight={ KEYBOARD_INPUT_OFFSET }
										keyboardShouldPersistTaps='always'>
					<ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}>
						<View>
							<H3 style={styles.item} numberOfLines={1}>Enter Parent Access Code</H3>
							<TouchableOpacity onPress={() => {this._openParentAccessModal()}}>
								<Text style={styles.itemDesc}>Where can I find the Parent Access Code?</Text>
							</TouchableOpacity>
							<View style={{flex: 1, justifyContent: 'center'}}>
								<TextInput
									placeholder='Enter Parent Access Code'
									onChangeText={(userInfo: string) => this.onOTPChange(userInfo)}
									autoCorrect={false}
									onFocus={() => this.onFocus()}
									blurOnSubmit={true}
									returnKeyType='next'
									maxLength={6}
									onSubmitEditing={this.onNextPress.bind(this)}
									autoCapitalize={'none'}
									clearButtonMode={'while-editing'}
									style={styles.textInput}
								/>
							</View>
							<Button
								onPress={this.onNextPress.bind(this)}
								style={styles.styleButton}
								title='VERIFY NOW'
								textStyle={styles.buttonNext}
							/>
						</View>
						{mobileExists && this._renderOtpOption()}
						{this.props.profileDataStore.isCodeMismatch && this._onCodeMismatch()}
					</ScrollView>
					<View style={styles.bottomView}>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onBackPress.bind(this)}>
								<View style={{flexDirection: 'row'}}>
									<Image
										style={styles.arrowIcon}
										source={icons.LEFT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											marginRight: verticalScale(8)
										}]}
										source={icons.LEFT_ARROW} />
									<Text style={styles.back}>Back</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</KeyboardAwareScrollView>
			</>
		)
	}

	onFocus() {
		this.setState({
			errors: {
				name: ''
			}
		})
	}

	onBackPress() {
		this.onBackPressed();
	}

	async onNextPress() {
		Keyboard.dismiss();
		const { profileDataStore } = this.props;
		let accessCode = this.state.textValue ? this.state.textValue.toUpperCase() : '';
		await profileDataStore.findStudent(this.state.studentInfo, accessCode);
	}
}

const getStyle = () => StyleSheet.create({
	wizard: {
		backgroundColor: 'white',
		paddingTop: heightPercentage(6),
		width: widthPercentage(100)
	},
	stepTwoContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	stepOne: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: verticalScale(20),
		backgroundColor: colors.ParentBlue,
		textAlign: 'center',
		textAlignVertical: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		color: 'white',
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2)
	},
	pendingStep: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		justifyContent: 'center',
		marginLeft: 10,
		marginBottom: 4,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		textAlign: 'center',
		textAlignVertical: 'center',
		color: 'rgba(51, 51, 51, 0.54)',
		backgroundColor: colors.White,
		...Platform.select({
			ios: {
				shadowColor: 'rgba(0, 0, 0, 0.25)',
				shadowOffset: { width: 2, height: 2 },
				shadowOpacity: 0.8,
				shadowRadius: 2,
			},
			android: {
				elevation: isTablet() ? 2 : 4,
			},
		})
	},
	wizardText: {
		marginLeft: 14,
		paddingTop: 5,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		color: colors.HeaderColor
	},
	profileLine: {
		width: widthPercentage(100),
		height: 3,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		flex: 1,
		backgroundColor: colors.White,
		paddingLeft: 30,
		paddingRight: 30,
		paddingBottom: 60
	},
	textInput: {
		fontFamily: 'Roboto',
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		width: '100%',
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		paddingVertical: 10
	},
	itemDesc: {
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: colors.ParentBlue,
		marginTop: isTablet() ? 20 : verticalScale(13),
		marginBottom: isTablet() ? 55 : verticalScale(37),
		width: widthPercentage(80)
	},
	itemSkip: {
		fontSize: 15,
		color: colors.PrimaryBlue,
		padding: 2,
		margin: 7,
		marginTop: 16,
		alignItems: 'flex-end',
		textAlign: 'right'
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1)
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: colors.White,
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		fontFamily: 'Roboto',
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	styleButton: {
		width: '100%',
		marginTop: 20,
		marginBottom: 30,
		padding: 10,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4,
		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 1 : 4,
		shadowRadius: 2
	},
	buttonNext: {
		color: colors.White,
		fontSize: isTablet() ? verticalScale(14) : verticalScale(14),
		justifyContent: 'center'
		// marginTop: isTablet() ? isPortrait() ? -5 : -5 : isPortrait() ? -5 : -5
	},
	otpInputContainer: {
		borderBottomWidth: 0,
		marginLeft: 0
	},
	otpInput: {
		width: verticalScale(37),
		height: verticalScale(36),
		borderWidth: 1,
		borderRadius: 4,
		marginRight: 4,
		borderColor: 'rgba(153, 153, 153, 0.54)'
	}
});
