import React from 'react';
import { Component } from 'react';
import {
	View,
	StyleSheet,
	TouchableOpacity,
	Keyboard,
	Dimensions,
	BackHandler,
	Platform,
	Image,
	ScrollView,
	TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { H3, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons
} from '../common';
import { colors } from '../config'
import {resetRouterSimple, goBack, showModal, showSnackbar, navigateSimple} from '../services'
import { AppDataStore, ProfileDataStore} from '../store'
import {CongratsAfterAddStudentPage} from "./CongratsAfterAddStudentPage";

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	errors?: any
	styles?: any,
	studentInfo?: any
	cca2?: any,
	families?: any
}

@inject('profileDataStore', 'appDataStore')
@observer
export class AddProfilePage3 extends Component<Props, State> {
	pageType: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			errors: {
				name: ''
			},
			styles: getStyle(),
			cca2: '',
			studentInfo: this.props.navigation.getParam('studentInfo') ? this.props.navigation.getParam('studentInfo') : {},
			families: [
				{
					key: 'father',
					name: 'Father',
					icon: icons.FATHER_ICON
				},
				{
					key: 'mother',
					name: 'Mother',
					icon: icons.MOTHER_ICON
				},
				{
					key: 'sibling',
					name: 'Sibling',
					icon: icons.SIBLING_ICON
				},
				{
					key: 'guardian',
					name: 'Guardian',
					icon: icons.GUARDIAN_ICON
				}
			]
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		// navigateSimple(this.props.navigation,'AddProfile1Page');
		const { navigation } = this.props;
		return goBack(navigation, 'AddProfile1Page');
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async componentDidMount() {
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	onFamilySelect(family: any) {
		this.setState({ textValue: family.toLowerCase() });
	};

	_onStudentAdded() {
		const { studentInfo } = this.state;
		showModal(
			<CongratsAfterAddStudentPage
				data={studentInfo}
				navigation={this.props.navigation} />,
			{
				hideDialogOnTouchOutside: false,
				positioning: 'center'
			}
		);
	}

	render() {
		const { studentInfo, styles , families, textValue } = this.state;
		return (
			<>
				<View style={styles.wizard}>
					<View style={{flexDirection: 'row'}}>
						<View style={styles.stepOne}>
							<Image source={icons.STEP_DONE_ICON} style={{
								width: verticalScale(12),
								height: verticalScale(9)
							}} resizeMode={'cover'} />
						</View>
						<View style={styles.stepOne}>
							<Image source={icons.STEP_DONE_ICON} style={{
								width: verticalScale(12),
								height: verticalScale(9)
							}} resizeMode={'cover'} />
						</View>
						<View style={{alignItems: 'center',
							justifyContent: 'center'}}>
							<View style={styles.stepOne}>
								<Text style={{
									textAlign: 'center',
									fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
									color: colors.White}}>3</Text>
							</View>
							<View style={{
								width: verticalScale(4),
								height: verticalScale(5),
								marginLeft: verticalScale(22),
								backgroundColor: colors.ParentBlue}} />
						</View>
						<Text style={styles.wizardText}>Add your relation</Text>
					</View>
					<View>

					</View>
					<Image
						style={styles.profileLine}
						source={icons.STUDENT_PROFILE_LINE3_ICON}
						resizeMode='cover'
					/>
				</View>
				<View style={styles.container}>
					<ScrollView showsVerticalScrollIndicator={false}>
						<View>
							<H3 style={styles.item}>Please select your relation with {studentInfo.name}</H3>
							<View style={styles.textFieldStyle}>
								{
									textValue === '' &&
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', width: '100%'}}>
										{
											families.map((family: any, index: number) => {
												return (
													<TouchableOpacity key={index} onPress={() => this.onFamilySelect(family.key)}>
														<View style={styles.userIconContainer}>
															<Image
																style={styles.userIcon}
																source={family.icon}
																resizeMode='contain'
															/>
															<Text style={styles.familyName}>{family.name}</Text>
														</View>
													</TouchableOpacity>
												)
											})
										}
									</View>
								}
								{
									textValue !== '' &&
									<View>
										{
											families.map((family: any, index: number) => {
												if (family.key === textValue) {
													return (
														<TouchableOpacity key={index} onPress={() => this.onFamilySelect(family.key)}>
															<View style={styles.userIconContainer}>
																<Image
																	style={[styles.userIcon, {
																		width: verticalScale(59),
																		height: verticalScale(69),
																	}]}
																	source={family.icon}
																	resizeMode='contain'
																/>
																<Text style={[styles.familyName, {
																	textAlign: 'center',
																	fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
																}]}>{family.name}</Text>
															</View>
														</TouchableOpacity>
													)
												} else {
													return null;
												}
											})
										}
										<View style={{flex: 1,
											justifyContent: 'space-between',
											flexDirection: 'row',
											width: widthPercentage(90),
											paddingLeft: verticalScale(10),
											paddingRight: verticalScale(10)}}>
											{
												families.map((family: any, index: number) => {
													if (family.key !== textValue) {
														return (
															<TouchableOpacity key={index} onPress={() => this.onFamilySelect(family.key)}>
																<View style={[styles.userIconContainer, {
																	opacity: 0.3
																}]}>
																	<Image
																		style={styles.userIcon}
																		source={family.icon}
																		resizeMode='contain'
																	/>
																	<Text style={[styles.familyName, {
																		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2)
																	}]}>{family.name}</Text>
																</View>
															</TouchableOpacity>
														)
													} else {
														return null;
													}
												})
											}
										</View>
									</View>
								}
							</View>
						</View>
						{this.props.profileDataStore.isStudentAdded && this._onStudentAdded()}
					</ScrollView>
					<View style={styles.bottomView}>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onBackPressed}>
								<View style={{flexDirection: 'row'}}>
									<Image
										style={styles.arrowIcon}
										source={icons.LEFT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											marginRight: verticalScale(8)
										}]}
										source={icons.LEFT_ARROW} />
									<Text style={styles.back}>Back</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
						<View style={{flex: 1, justifyContent: 'center'}}>
							<TouchableWithoutFeedback onPress={this.onNextPress.bind(this)}>
								<View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
									<Text style={styles.next}>Next</Text>
									<Image
										style={[styles.arrowIcon, {
											marginLeft: verticalScale(8),
											left: 0
										}]}
										source={icons.RIGHT_ARROW} />
									<Image
										style={[styles.arrowIcon, {
											left: 0
										}]}
										source={icons.RIGHT_ARROW} />
								</View>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</View>
			</>
		)
	}

	onFocus() {
		this.setState({
			errors: {
				name: ''
			}
		})
	}

	async onNextPress() {
		const { profileDataStore } = this.props;
		if (this.state.textValue) {
			await profileDataStore.addStudent({
				studentId: this.state.studentInfo.id,
				relation: this.state.textValue,
				accessCode: this.state.studentInfo.accessCode
			});
		} else {
			showSnackbar("Please select the relation.");
		}
	}
}

const getStyle = () => StyleSheet.create({
	wizard: {
		backgroundColor: 'white',
		paddingTop: heightPercentage(6),
		width: widthPercentage(100)
	},
	stepOne: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: verticalScale(20),
		backgroundColor: colors.ParentBlue,
		textAlign: 'center',
		textAlignVertical: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2)
	},
	wizardText: {
		marginLeft: 14,
		paddingTop: 5,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		color: colors.HeaderColor
	},
	profileLine: {
		width: widthPercentage(100),
		height: 3,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingHorizontal: 30,
		paddingBottom: 60
	},
	userIconContainer: {
		marginTop: verticalScale(20),
		marginRight: verticalScale(20),
		flex: 1,
		alignItems: 'center'
	},
	userIcon: {
		width: verticalScale(48),
		height: verticalScale(56),
		marginBottom: 5
	},
	textFieldStyle: {
		marginTop: widthPercentage(5),
		paddingRight: 10,
		paddingLeft: 10
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1),
		lineHeight: 25
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: colors.White,
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	familyName: {
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.3),
		color: colors.ContentColor
	}
});
