import React from 'react';
import { Component } from 'react';
import {
	TextInput, View, StyleSheet, TouchableOpacity, ScrollView, Keyboard, Dimensions,
	BackHandler, Platform, Image, TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react';
import { H3, Text } from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons, isTablet
} from '../common';
import {getStudents} from '../utils';
import { colors } from '../config'
import {resetRouterSimple, goBack, navigateSimple} from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { AppDataStore, ProfileDataStore} from '../store'

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	textValue?: string,
	errors?: any
	styles?: any,
	countryCode?: any
	addCountryCode?: boolean
	cca2?: any,
	studentInfo: any
}

@inject('profileDataStore', 'appDataStore')
@observer
export class AddProfileViewPage extends Component<Props, State> {
	valRef: any = undefined;
	pageType: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: '',
			errors: {
				name: ''
			},
			styles: getStyle(),
			countryCode: '',
			addCountryCode: false,
			cca2: '',
			studentInfo: this.props.navigation.getParam('data')
		};
		console.log("AddProfileViewPage constructor", this.props.navigation.getParam('data'));
	}

	async componentDidMount() {
		console.log("AddProfileViewPage componentDidMount");
		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);
		this.forceUpdate();
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation, 'AddProfile1Page')
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	_renderStudentImage() {
		const { styles, studentInfo } = this.state;

		return <View style={styles.profileImage}>
			{studentInfo && studentInfo.image !== null ?
				<Image style={{width: verticalScale(48), height: verticalScale(48), borderRadius: verticalScale(32)}} source={{uri: studentInfo.image}} resizeMode={'contain'} /> :
				<Image style={{width: verticalScale(48), height: verticalScale(48)}} source={icons.PROFILE_ICON} resizeMode={'contain'} />
			}
		</View>;
	}

	_renderStudentGoals() {
		const { studentInfo } = this.state;
		return studentInfo.goal.map((goal: any) => {
			return `${goal.name} `
		})
	}

	render() {
		const { styles, studentInfo } = this.state;
		console.log("AddProfileViewPage render", studentInfo);
		if(studentInfo){
			return (
				<>
					<View style={styles.wizard}>
						<View style={{flexDirection: 'row'}}>
							<View style={styles.stepOne}>
								<Image source={icons.STEP_DONE_ICON} style={{
									width: verticalScale(12),
									height: verticalScale(9)
								}} resizeMode={'cover'} />
							</View>
							<View style={styles.stepTwoContainer}>
								<View style={styles.stepOne}>
									<Text style={{
										textAlign: 'center',
										fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
										color: colors.White}}>2</Text>
								</View>
								<View style={{
									width: verticalScale(4),
									height: verticalScale(5),
									marginLeft: verticalScale(22),
									backgroundColor: colors.ParentBlue}} />
							</View>
							<Text style={styles.wizardText}>Verify Student</Text>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 30}}>
								<View style={styles.pendingStep}>
									<Text style={{fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
										textAlign: 'center',
										color: 'rgba(51, 51, 51, 0.54)'}}>3</Text>
								</View>
							</View>
						</View>
						<Image
							style={styles.profileLine}
							source={icons.STUDENT_PROFILE_LINE2_ICON}
							resizeMode='cover'
						/>
					</View>
					<View style={styles.container}>
						<KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
							<View>
								<View>
									<Text style={[styles.itemDesc, {
										marginTop: isPortrait() ? heightPercentage(9) : heightPercentage(1),
										marginBottom: verticalScale(13),
										alignSelf: 'center',
									}]}>Successfully added the student</Text>
								</View>
								<View style={{
									flex: 1,
									width: widthPercentage(80),
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignSelf: 'center',
									// marginLeft: verticalScale(6),
									marginBottom: verticalScale(30)}}>
									<View style={{flex: 1}}>
										<H3 style={styles.item}>{studentInfo.name}</H3>
									</View>
									<View style={{flex: 1, alignItems: 'flex-end'}}>
										<Image source={icons.TICK_IMG} style={{height: verticalScale(25), width: verticalScale(25)}}/>
									</View>
								</View>
								
								<View style={styles.profileContainer}>
									{this._renderStudentImage()}
									<View style={{
										marginTop: verticalScale(9),
										marginRight: verticalScale(13),
										width: 0,
										flexGrow: 1,
										flex: 1
									}}>
										<Text style={{color: colors.SubHeaderColor,
											fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
											marginBottom: verticalScale(5)
										}}>{studentInfo.name}</Text>
										<Text style={{
											color: colors.SubHeaderColor,
											fontSize: isPortrait() ? verticalScale(10) : widthPercentage(2.2)
										}}>
											Goal: {this._renderStudentGoals()}
										</Text>
									</View>
								</View>
							</View>
						</KeyboardAwareScrollView>
						<View style={styles.bottomView}>
							<View style={{flex: 1, justifyContent: 'center'}}>
								{
									studentInfo.isVerified ?
									(<TouchableWithoutFeedback onPress={this.onBackPressed}>
										<View style={{flexDirection: 'row'}}>
											<Image
												style={styles.arrowIcon}
												source={icons.LEFT_ARROW} />
											<Image
												style={[styles.arrowIcon, {
													marginRight: verticalScale(8)
												}]}
												source={icons.LEFT_ARROW} />
											<Text style={styles.back}>Back</Text>
										</View>
									</TouchableWithoutFeedback> ) :
									null
								}

							</View>
							<View style={{flex: 1, justifyContent: 'center'}}>
								<TouchableWithoutFeedback onPress={this.onNextPress.bind(this)}>
									<View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
										<Text style={styles.next}>Next</Text>
										<Image
											style={[styles.arrowIcon, {
												marginLeft: verticalScale(8),
												left: 0
											}]}
											source={icons.RIGHT_ARROW} />
										<Image
											style={[styles.arrowIcon, {
												left: 0
											}]}
											source={icons.RIGHT_ARROW} />
									</View>
								</TouchableWithoutFeedback>
							</View>
						</View>
					</View>
				</>
			)
		}else{
			return null
		}
	}

	onFocus() {
		this.setState({
			errors: {
				name: ''
			}
		})
	}

	async onNextPress() {
		Keyboard.dismiss();
		navigateSimple(this.props.navigation, 'AddProfile3Page', {
			studentInfo: this.state.studentInfo
		});
	}
}

const getStyle = () => StyleSheet.create({
	wizard: {
		backgroundColor: 'white',
		paddingTop: heightPercentage(6),
		width: widthPercentage(100)
	},
	stepTwoContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	stepOne: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: verticalScale(20),
		backgroundColor: colors.ParentBlue,
		textAlign: 'center',
		textAlignVertical: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		color: 'white',
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2)
	},
	pendingStep: {
		height: verticalScale(20),
		width: verticalScale(20),
		borderRadius: verticalScale(10),
		marginLeft: 10,
		marginBottom: 4,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		textAlign: 'center',
		textAlignVertical: 'center',
		color: 'rgba(51, 51, 51, 0.54)',
		backgroundColor: colors.White,
		shadowColor: 'rgba(0, 0, 0, 0.25)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: isTablet() ? 2 : 4
	},
	wizardText: {
		marginLeft: 14,
		paddingTop: 5,
		fontSize: isPortrait() ? verticalScale(12) : widthPercentage(2.2),
		color: colors.HeaderColor
	},
	profileLine: {
		width: widthPercentage(100),
		height: 3,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30
	},
	itemDesc: {
		width: widthPercentage(80),
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: colors.ContentColor,
		// marginTop: isTablet() ? 20 : verticalScale(13),
		// marginBottom: isTablet() ? 55 : verticalScale(37),
	},
	itemSkip: {
		fontSize: 15,
		color: colors.PrimaryBlue,
		padding: 2,
		margin: 7,
		marginTop: 16,
		alignItems: 'flex-end',
		textAlign: 'right'
	},
	item: {
		color: colors.HeaderColor
	},
	profileContainer: {
		flex: 1,
		flexDirection: 'row',
		width: widthPercentage(80),
		paddingTop: verticalScale(12),
		paddingBottom: verticalScale(12),
		paddingLeft: verticalScale(20),

		shadowColor: 'rgba(0, 0, 0, 0.14)',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		elevation: 2,
		shadowRadius: 4,
		borderRadius: 4,

		alignSelf: 'center',

		marginBottom: 10,
		backgroundColor: colors.White,
	},
	profileImage: {
		marginRight: verticalScale(30)
	},
	bottomView: {
		flex: 1,
		flexDirection: 'row',
		width: widthPercentage(100),
		paddingLeft: 30,
		paddingRight: 30,
		height: 60,
		borderTopWidth: 0.73,
		borderTopColor: 'rgba(153, 153, 153, 0.1)',
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0
	},
	arrowIcon: {
		width: verticalScale(8),
		height: verticalScale(10),
		top: verticalScale(6),
		right: verticalScale(2),
		left: verticalScale(2)
	},
	back: {
		textAlign: 'left',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	next: {
		fontFamily: 'Roboto',
		textAlign: 'right',
		color: colors.ParentBlue,
		fontSize: isPortrait() ? verticalScale(16) : widthPercentage(2.6)
	},
	styleButton: {
		width: widthPercentage(86),
		marginTop: 20,
		marginBottom: verticalScale(15),
		padding: 10,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4,
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4),

		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 2 : 4,
		shadowRadius: 2
	},
	otpInputContainer: {
		borderBottomWidth: 0,
		marginLeft: 0
	},
	otpInput: {
		width: verticalScale(37),
		height: verticalScale(36),
		borderWidth: 1,
		borderRadius: 4,
		marginRight: 4,
		borderColor: 'rgba(153, 153, 153, 0.54)'
	}
});
