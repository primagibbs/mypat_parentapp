import React, { Component } from 'react'
import {
	Text,
	View,
	StyleSheet,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	BackHandler,
	Image,
	Platform,
	TouchableWithoutFeedback
} from 'react-native';
import {inject, observer} from 'mobx-react'
import { get } from 'lodash'

import {
	verticalScale, widthPercentage, heightPercentage, isPortrait, isTablet, CONGRATS_PAGE_TYPE, icons
} from '../common'
import { colors } from '../config'
import { navigateSimple, resetRouterSimple, goBack, hideModal } from '../services';
import {getSelectedStudent, getStudents, setSelectedStudent} from "../utils";
import { ProfileDataStore } from '../store';

interface Props {
	data?: any,
	navigation?: any,
	nextPage?: string,
	profileDataStore?: ProfileDataStore
}

interface State {
	styles?: any,
	students?: any
}

@inject('profileDataStore')
@observer
export class CongratsAfterAddStudentPage extends Component<Props, State> {
	pageType: any;

	constructor(props: any) {
		super(props);
		this.state = {
			styles: getStyle(),
			students: []
		}
	}

	async componentDidMount() {
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed)

		const { profileDataStore } = this.props;
		profileDataStore.setNavigationObject(this.props.navigation);

		const students = await getStudents();
		this.setState({students});
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		this.pageType = get(navigation, 'state.params.pageType');
		switch (this.pageType) {
			case CONGRATS_PAGE_TYPE.SIGNUP_EMAIL:
				navigateSimple(navigation, 'SignupPage');
				return true;
			default:
				return goBack(navigation)
		}
	};

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	async onDonePress() {
		const { navigation, data } = this.props;
		const { profileDataStore } = this.props;
		await setSelectedStudent(data.id);
		const student = await getSelectedStudent();
		profileDataStore.setSelectedStudent(student);
		hideModal();
		resetRouterSimple(navigation, 0, 'HomePage')
	}

	_onAddStudent = async () => {
		hideModal();
		resetRouterSimple(this.props.navigation, 0, 'AddProfile1Page');
	};

	renderProfileImage(image: any) {
		return <View>
			{image !== null ?
				<Image style={{ width: verticalScale(62), height: verticalScale(62), borderRadius: verticalScale(31) }} source={{ uri: image }} resizeMode={'contain'} /> :
				<Image style={{ width: verticalScale(62), height: verticalScale(62) }} source={icons.PROFILE_ICON} resizeMode={'contain'} />
			}
		</View>;
	}

	render() {
		const { styles, students } = this.state;
		const { data } = this.props;

		return (
			<TouchableWithoutFeedback>
				<View style={styles.wrapper}>
					<Image
						style={styles.mypatLogo}
						source={icons.CORRECT_ICON}
						resizeMode={'contain'}
					/>
					<Text style={styles.congratulations}>{'Congratulations'}</Text>
					<Text style={styles.description}>{`You have successfully added ${data.name}'s profile`}</Text>

					<ScrollView>
						<View style={styles.innerContainer}>
							{
								students.length > 0 && students.map((student: any) => {
									return <View style={styles.gridItem}>
											{this.renderProfileImage(student.image)}
											<Text numberOfLines={1} style={{color: colors.HeaderColor, textAlign: 'center', textTransform: 'capitalize'}}>{student.name}</Text>
										</View>
								})
							}
							{/*<View style={styles.gridItem}>
								{this.renderProfileImage(data.image)}
								<Text style={{color: colors.HeaderColor, textAlign: 'center', textTransform: 'capitalize'}}>{data.name}</Text>
							</View>*/}
							<TouchableWithoutFeedback onPress={() => this._onAddStudent()}>
								<View style={styles.gridItem}>
									<Image source={icons.ADD_STUDENT} style={{
										width: verticalScale(62),
										height: verticalScale(62)
									}} resizeMode={'contain'} />
									<Text style={{color: colors.HeaderColor, textAlign: 'center'}}>Add new student</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</ScrollView>
					<TouchableWithoutFeedback onPress={this.onDonePress.bind(this)}>
						<View style={styles.closeIcon}>
							<Text style={styles.closeText}>Done</Text>
						</View>
					</TouchableWithoutFeedback>
				</View>
			</TouchableWithoutFeedback>
		)
	}
}

const screenHeight = Dimensions.get('window').height;
const getStyle = () => StyleSheet.create({
	wrapper: {
		borderRadius: 6,
		backgroundColor: colors.White,
		height: screenHeight*.85,
		width: widthPercentage(90)
	},
	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: isTablet() ? isPortrait() ? widthPercentage(30) : widthPercentage(15) : isPortrait() ? widthPercentage(20) : widthPercentage(10)
	},
	description: {
		alignSelf: 'center',
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.2),
		width: widthPercentage(80),
		marginTop: verticalScale(10),
		marginBottom: verticalScale(10),
		lineHeight: isPortrait() ? verticalScale(20) : widthPercentage(2.5),
		color: colors.SubHeaderColor
	},
	innerContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		flexWrap: 'wrap',
		alignItems: 'center',
		paddingHorizontal: 16
	},
	congratulations: {
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: isPortrait() ? verticalScale(24) : widthPercentage(3.2),
		marginTop: isPortrait() ? verticalScale(30) : widthPercentage(3.2),
		textAlign: 'center',
		color: colors.HeaderColor
	},
	styleButton: {
		marginTop: 12,
		marginBottom: 20,
		padding: 10,
		paddingLeft: 15,
		paddingRight: 15,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4,
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)
	},
	icon: {
		marginTop: isPortrait ? verticalScale(130) : verticalScale(0)
	},
	gridItem: {
		flexBasis: '50%',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 12,
		paddingBottom: 12
	},
	mypatLogo: {
		marginTop: isPortrait() ? verticalScale(40) : widthPercentage(3.2),
		width: verticalScale(40),
		height: verticalScale(40),
		alignSelf: 'center'
	},
	closeIcon: {
		borderTopWidth: 1,
		borderTopColor: '#CBCBCB',
		justifyContent: 'center',
		alignItems: 'center'
	},
	closeText: {
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		textAlign: 'center',
		color: colors.ParentBlue,
		paddingTop: 15,
		paddingBottom: 15
	}
});
