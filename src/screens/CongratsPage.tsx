import React, { Component } from 'react'
import {
	Text,
	View,
	StyleSheet,
	Dimensions,
	BackHandler,
	Image,
	TouchableWithoutFeedback
} from 'react-native';
import {inject, observer} from 'mobx-react'
import { get } from 'lodash'

import { verticalScale, widthPercentage, isPortrait, isTablet, CONGRATS_PAGE_TYPE
} from '../common'
import { Button } from '../components'
import { colors } from '../config'
import { navigateSimple, resetRouterSimple, goBack, hideModal } from '../services';
import {ProfileDataStore} from "../store";

interface Props {
	profileDataStore?: ProfileDataStore
	data?: any,
	navigation?: any,
	nextPage?: string,
	params?: any
}

interface State {
	styles?: any
}

@inject('profileDataStore')
@observer
export class CongratsPage extends Component<Props, State> {
	pageType: any;

	constructor(props: any) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	componentDidMount() {
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed)
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		this.pageType = get(navigation, 'state.params.pageType');
		switch (this.pageType) {
			case CONGRATS_PAGE_TYPE.SIGNUP_EMAIL:
				navigateSimple(navigation, 'SignupPage');
				return true;
			default:
				return goBack(navigation)
		}
	};

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	onDonePress() {
		const { navigation, nextPage, params, data, profileDataStore } = this.props;
		if (data.userProfile && (data.userProfile.name === "" || data.userProfile.name === null)) {
			resetRouterSimple(navigation, 0, 'AddInfoPage', params);
		} else if (data.userProfile && (data.userProfile.email === "" || data.userProfile.email === null)) {
			resetRouterSimple(navigation, 0, 'AddEmailPage', params);
		} else if (data.userProfile && (data.userProfile.studentProfiles && data.userProfile.studentProfiles.length === 0)) {
			resetRouterSimple(navigation, 0, 'AddProfile1Page', params);
		} else {
			resetRouterSimple(navigation, 0, nextPage, params);
		}
		if (profileDataStore.isModal) {
			profileDataStore.setIsModal();
		}
		hideModal();
	}

	render() {
		const { styles } = this.state;
		const { data } = this.props;

		return (
			<TouchableWithoutFeedback>
				<View style={styles.itemCode}>
					<Image
						style={styles.mypatLogo}
						source={data.icon}
						resizeMode={'contain'}
					/>
					<Text style={styles.congratulations}>{data.heading}</Text>
					<Text style={styles.description}>{data.subHeading}</Text>
					<View style={{minWidth: '40%', maxWidth:'60%' ,flex: 1,
						alignSelf: 'center'}}>
						<Button
							onPress={() => this.onDonePress()}
							style={styles.styleButton}
							title={data.btnText}
							textStyle={styles.buttonText}
						/>
					</View>
				</View>
			</TouchableWithoutFeedback>
		)
	}
}

const screenHeight = Dimensions.get('window').height;
const getStyle = () => StyleSheet.create({
	itemCode: {
		borderRadius: 6,
		backgroundColor: colors.White,
		height: screenHeight*.50,
		width: widthPercentage(90)
	},
	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: isTablet() ? isPortrait() ? widthPercentage(30) : widthPercentage(15) : isPortrait() ? widthPercentage(20) : widthPercentage(10)
	},
	description: {
		alignSelf: 'center',
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.2),
		width: widthPercentage(80),
		marginTop: verticalScale(10),
		marginBottom: verticalScale(10),
		lineHeight: isPortrait() ? verticalScale(20) : widthPercentage(2.5),
		color: colors.LightGrey
	},
	congratulations: {
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: isPortrait() ? verticalScale(18) : widthPercentage(3.2),
		fontWeight: '500',
		marginTop: isPortrait() ? verticalScale(30) : widthPercentage(3.2),
		textAlign: 'center',
		color: colors.ParentBlue
	},
	styleButton: {
		marginTop: 12,
		marginBottom: 20,
		padding: 10,
		paddingLeft: 15,
		paddingRight: 15,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4
	},
	buttonText: {
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5)
	},
	icon: {
		marginTop: isPortrait ? verticalScale(130) : verticalScale(0)
	},
	mypatLogo: {
		marginTop: isPortrait() ? verticalScale(40) : widthPercentage(3.2),
		width: widthPercentage(36),
		height: widthPercentage(18),
		alignSelf: 'center'
	}
});
