import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	ImageBackground,
	AppState,
	Dimensions, Image, TouchableOpacity, Linking, BackHandler, ScrollView
} from 'react-native';
// @ts-ignore
import Share from 'react-native-share';
// @ts-ignore
import { Navigation } from 'react-navigation';
import {H1, H3} from "native-base";
import {colors} from "../common-library/config";
import {isPortrait, verticalScale, widthPercentage, isTablet} from "../common-library/common";
import {inject, observer} from "mobx-react";
import {FeedStore, NetworkDataStore} from "../store";
import commonStores from "../common-library/store";
import {LoadingPlaceholder} from "../components/LoadingPlaceholder";
import {icons} from "../common";
import {goBack} from "../common-library/services";
import {AutoHeightWebview} from "../common-library/components";

interface Props {
	navigation?: Navigation,
	feedDetail?: any,
	feedStore?: FeedStore
	networkDataStore?: NetworkDataStore
}
interface State {
	styles?: any,
	feedId?: string
}

const formatDate = (date: any) => {
	date = new Date(date);
	const monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	];

	const day = date.getDate();
	const monthIndex = date.getMonth();
	const year = date.getFullYear();

	return `${monthNames[monthIndex]} ${day}, ${year}`;
};

const stripHTMLTag = (str: string) => {
	if ((str === null) || (str === ''))
		return false;
	else
		return str.replace(/<\/?[^>]+(>|$)/g, "");
};

const webViewStyle = `<style type="text/css">
	@import url(http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300ita‌​lic,400italic,500,500italic,700,700italic,900italic,900);
	html, body, html * {
  	font-family: 'Roboto', sans-serif;
	}
	body, li{
		font-size: 16px !important;
		line-height: 24px !important;
	}
	.blog, a {
		font-size: 16px;
		line-height: 24px;
		text-decoration: none;
		font-weight: 400;
		color: #666666;
		font-family: 'Roboto', sans-serif;
  }
</style>`;


const getHTML = (content: string) => {
	if (content) {
		const htmlContent = '<div class="blog">' + content + '</div>';

		return `<!DOCTYPE html>
                      <html>
                      <head>
                      	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
						${webViewStyle}
                      </head>
                      <body>${htmlContent}</body>
                      </html>`
	}
	return null
};

@inject('feedStore', 'networkDataStore')
@observer
export class FeedDetailPage extends Component<Props, State> {

	constructor(props: Props, state: State) {
		super(props, state);
		this.state = {
			styles: getStyle(),
			feedId: this.props.navigation.getParam('feedId')
		}
	}

	async componentDidMount() {
		const { feedStore, navigation } = this.props;
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);

		feedStore.setNavigationObject(navigation);
		await feedStore.getFeedDetail(this.state.feedId);
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	async onLikeToggle(feedId: string) {
		const { feedStore, navigation } = this.props;

		feedStore.setNavigationObject(navigation);
		await feedStore.toggleFeedLike(feedId);
	}

	redirectToPage() {
		const { navigation } = this.props;
		return goBack(navigation);
	}

	onShareBtnClicked(feed: any) {
		const shareOptions = {
			title: feed.title,
			message: feed.title,
			url: feed.url,
			subject: 'Smart Parenting'
		};
		Share.open(shareOptions)
	}

	renderDetailView(feedDetail: any) {
		const { styles } = this.state;
		let author = feedDetail.author ? feedDetail.author.toLowerCase() : "";
		let isMypatAuthor = false;
		if(author.indexOf("mypat") >=0){
			author = "myPAT";
			isMypatAuthor = true;
		}else{
			author = feedDetail.author;
		}
		return <View style={styles.listView} key={feedDetail.id}>
			<ScrollView style={{flex: 1}}>
				<View style={{}}>
					<H3 style={styles.feedTitle}>{feedDetail.title}</H3>
					<Text style={[styles.subtitle, {fontWeight: '300'}, !isMypatAuthor ? {textTransform: "capitalize"} : {}]}>
						{`By ${author}. ${formatDate(feedDetail.postedOn)}`}
					</Text>
				</View>

				<AutoHeightWebview
					autoHeight={true}
					javaScriptEnabled={true}
					disableLinks={true}
					scrollEnabled={true}
					style={{ width: '90%',
						backgroundColor: colors.Transparent,
						fontSize: isTablet() ? verticalScale(14): verticalScale(13),
						marginRight: verticalScale(13),
						marginLeft: verticalScale(13),
						marginBottom: 17}}
					source={{ html: getHTML(feedDetail.content) }}
					decelerationRate='normal'
					bounces={true}
				/>
				<View style={{
					flexDirection: "row",
					alignItems: 'center',
					marginRight: verticalScale(20),
					marginBottom: verticalScale(20),
					marginLeft: verticalScale(20)}}>
					<TouchableOpacity onPress={() => this.onLikeToggle(feedDetail.id)}>
						{
							feedDetail.liked === false &&
							<Image
								resizeMode='contain'
								source={icons.LIKE_ICON}
								style={{width: verticalScale(33), height: verticalScale(33), marginRight: 20}} />
						}
						{
							feedDetail.liked !== false &&
							<Image
								resizeMode='contain'
								source={icons.LIKED_ICON}
								style={{width: verticalScale(33), height: verticalScale(33), marginRight: 20}} />
						}
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.onShareBtnClicked(feedDetail)}>
						<Image
							resizeMode='contain'
							source={icons.SHARE_ICON}
							style={{width: verticalScale(24), height: verticalScale(26)}} />
					</TouchableOpacity>
				</View>
				<View style={{}}>
					<Text style={[styles.subtitle, {fontWeight : '500'}]}>Reference-</Text>
					<TouchableOpacity onPress={() => Linking.openURL(feedDetail.url)}>
						<Text style={[styles.subtitle, {
							marginBottom: verticalScale(20),
							fontSize: isTablet() ? verticalScale(9) : verticalScale(12),
							fontWeight: '300'
						}]}>{feedDetail.url}</Text>
					</TouchableOpacity>
				</View>
			</ScrollView>
		</View>;
	}

	render() {
		const { styles } = this.state;
		const feedDetail = this.props.feedStore.feedDetail;

		const status = commonStores.networkDataStore.isNetworkConnected;
		const isLoading = this.props.feedStore.isLoading;
		if (status === true) {
			return (
				<View style={styles.container}>
					<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground}>
						<View style={styles.heading}>
							<View style={{flexDirection: 'row'}}>
								<TouchableOpacity onPress={() => this.redirectToPage()}>
									<Image source={icons.BACK_ARROW_WHITE} style={{marginLeft: 15, marginTop: 18, width: 29, height: 24}} />
								</TouchableOpacity>
								<H1 style={styles.headingTitle}>Smart Parenting</H1>
							</View>
						</View>
					</ImageBackground>
					<View style={styles.scrollView}>
						{
							feedDetail && Object.keys(feedDetail).length !== 0 && this.renderDetailView(feedDetail)
						}
						{
							!feedDetail && Object.keys(feedDetail).length == 0 && <View style={{ flex: 1 }}>
								<LoadingPlaceholder
									loading={isLoading}
									count={6}
									size={100}
								/>
							</View>
						}
					</View>
				</View>
			);
		} else {
			return <View style={{ flex: 1 }}>
				<LoadingPlaceholder
					loading={isLoading}
					count={6}
					size={100}
				/>
			</View>
		}
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		flex: 1
	},
	heading: {
		flex: 1
	},
	scrollView: {
		flex: 1,
		position: 'absolute',
		top: verticalScale(70),
		left: 10,
		right: 10,
		bottom: 0,

		backgroundColor: colors.White,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		shadowColor: 'rgba(0, 0, 0, 0.199926)',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		borderRadius: 6
	},
	headingTitle: {
		fontSize: verticalScale(24),
		lineHeight: verticalScale(24),
		fontWeight: '500',
		color: colors.White,
		marginTop: 21,
		marginBottom: 20,
		marginLeft: 18
	},
	headerBackground: {
		flex: 1,
		height: widthPercentage(40),
		width: widthPercentage(100)
	},
	itemDesc: {
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		color: colors.White,
		marginBottom: 5,
		marginLeft: 15,
		width: widthPercentage(80)
	},
	listView: {
		flex: 1
	},
	feedTitle: {
		fontSize: isTablet() ? verticalScale(16) : verticalScale(20),
		fontWeight: '500',
		marginTop: verticalScale(20),
		marginRight: verticalScale(20),
		marginLeft: verticalScale(20),
		marginBottom: verticalScale(20),
		textAlign: 'center',
		lineHeight: 30,
		color: colors.SubHeaderColor
	},
	subtitle: {
		marginRight: isTablet() ? verticalScale(13) : verticalScale(20),
		marginLeft: verticalScale(13),
		color: colors.SubHeaderColor,
		fontSize: isTablet() ? verticalScale(10) : verticalScale(13),
		marginBottom: verticalScale(5)
	},
	content: {
		fontSize: isTablet() ? verticalScale(13) : verticalScale(13),
		color: colors.SubHeaderColor,
		fontFamily: 'Roboto',
		fontWeight: '300',
		lineHeight: verticalScale(20)
	}
});
