import React, { Component } from 'react';
import {
	StyleSheet, View, Text, TouchableOpacity, Dimensions, Keyboard, Image, BackHandler, Platform, AppState, TextInput
} from 'react-native';
// @ts-ignore
import Navigation from 'react-navigation';
import { inject, observer } from 'mobx-react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from '../components';
import { colors } from '../config'
import { ProfileDataStore, NetworkDataStore } from '../store';
import { verticalScale, widthPercentage, heightPercentage, isPortrait, isTablet, icons, offlineMsg} from '../common';
import stores from '../store';
import { validateMobileNumber, isPhoneNumber, getDeviceCountryCode } from '../utils';
import transport from '../common-library/events';
import { goBack, showSnackbar } from '../services';
import { USER_INFO_TYPE } from '../common';
import commonStores from '../common-library/store';
import {H1} from "native-base";

interface Props {
	userInfo?: string,
	profileDataStore?: ProfileDataStore
	// @ts-ignore
	navigation?: Navigation
	networkDataStore?: NetworkDataStore
}

interface State {
	userInfo?: string
	isError?: boolean
	selection?: any
	userId?: any
	profileDataStore?: ProfileDataStore
	orientation?: string
	styles?: any
	isTearDownRequested?: boolean
	countryCode?: any
	addCountryCode?: boolean
	errorMessage?: any
}

const KEYBOARDREF = Keyboard;

@inject('profileDataStore', 'networkDataStore')
@observer
export class LoginPage extends Component<Props, State> {
	constructor(props: any) {
		super(props);
		this.state = {
			userInfo: '',
			userId: this.props.navigation.getParam('userId'),
			isError: false,
			selection: {
				start: 0,
				end: 0
			},
			styles: getStyle(),
			isTearDownRequested: false,
			countryCode: '',
			addCountryCode: false,
			errorMessage: ''
		};
	}

	componentDidMount() {
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});

		const countryCode = getDeviceCountryCode();
		this.setState({ countryCode: countryCode });
		Dimensions.addEventListener('change', this._orientationChangeListener);
		this.props.profileDataStore.setNavigationObject(this.props.navigation);
		stores.socialDataStore.setNavigationObject(this.props.navigation);
		stores.socialDataStore.setupGoogleSignIn();
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
		transport.setData('Page Viewed', { 'Page Name': 'other-sign-in', 'Device Type': Platform.OS });
		transport.post()
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
		Dimensions.removeEventListener('change', this._orientationChangeListener)
	}

	_orientationChangeListener = () => {
		this.setState({
			styles: getStyle()
		})
	};

	renderSocialButtons = () => {
		const { styles } = this.state;
		return <View style={styles.buttonView}>
			<View style={{ flex: 10 }} />
			<Button
				transparent={true}
				onPress={this.onFacebookBtnPress.bind(this)}
				style={styles.socialButton}
				iconLeft={true}>
				<View>
					<Image
						style={this.state.styles.socialImage}
						source={require('../../images/facebook.png')}
						resizeMode={'contain'}
					/>
				</View>
			</Button>
			<Button
				transparent={true}
				onPress={this.onGoogleBtnPress.bind(this)}
				style={styles.socialButton}
				iconLeft={true}
			>
				<View>
					<Image
						style={this.state.styles.socialImage}
						source={require('../../images/google.png')}
						resizeMode={'contain'}
					/>
				</View>
			</Button>
			<View style={{ flex: 10 }} />
		</View>
	};

	onFocus() {
		this.setState({
			isError: false,
			errorMessage: ''
		})
	}

	async _onSubmitEditing() {
		await this.onLoginPress();
	}

	onTextChange(userInfo: any) {
		this.setState({ userInfo: userInfo.toLowerCase() });
		const isValidPhone = isPhoneNumber(userInfo);
		this.setState({ addCountryCode: isValidPhone })
	}

	renderForm = () => {
		const { styles, userId, isError, errorMessage } = this.state;

		const loginButton = (<Button
			onPress={this.onLoginPress.bind(this)}
			style={this.state.styles.loginButtonView}
			title='NEXT'
			textStyle={styles.buttonNext}
		/>);

		return <View style={styles.formView}>
			<TextInput
				placeholder='Enter Mobile No.'
				onChangeText={(userInfo: any) => this.onTextChange(userInfo)}
				autoCorrect={false}
				onFocus={() => this.onFocus()}
				keyboardType={'number-pad'}
				blurOnSubmit={true}
				returnKeyType='next'
				onSubmitEditing={this._onSubmitEditing.bind(this)}
				autoCapitalize={'none'}
				clearButtonMode={'while-editing'}
				style={styles.textInput}
			/>
			{
				isError && <Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5), marginTop: 6}}>{errorMessage}</Text>
			}
			{loginButton}
		</View>
	};

	renderLoginButton = (styles: any) => <View style={styles.renderRegisterStyle}>
		<Text style={styles.newtomypat}>Dont have an account?</Text>
		<TouchableOpacity onPress={this.onRegisterBtnPress.bind(this)} style={{ height: verticalScale(50) }}>
			<Text style={styles.newtoregister}> Register here</Text>
		</TouchableOpacity>
	</View>;

	validateForm = () => {
		const { userInfo } = this.state;
		this.props.profileDataStore.setUserInfoType(USER_INFO_TYPE.MOBILE);

		if (userInfo.length !== 10) {
			this.setState({
				isError: true,
				errorMessage: 'Please enter a valid mobile no.'
			});
		} else {
			// validate user info
			const userInfoError = validateMobileNumber(userInfo);

			if (userInfoError) {
				this.props.profileDataStore.setUserInfoType(USER_INFO_TYPE.NONE);
				// set errors
				this.setState({
					isError: true,
					errorMessage: userInfoError
				});
				return false;
			} else {
				return true;
			}
		}
	};

	onLoginPress = async () => {
		Keyboard.dismiss();
		const { networkDataStore } = this.props;
		if (networkDataStore.isNetworkConnected) {
			if (this.validateForm()) {
				const { userInfo, userId } = this.state;
				const { profileDataStore } = this.props;
				await profileDataStore.updateUserDataViaLogin({
					userInfo,
					userId
				})
			}
		} else {
			showSnackbar(offlineMsg)
		}
	};

	onRegisterBtnPress = () => {
		const { networkDataStore } = this.props;
		if (networkDataStore.isNetworkConnected) {
			transport.setData('Login', { 'Action': 'Clicked Register', 'Device Type': Platform.OS });
			transport.post();
			this.onFocus();
			KEYBOARDREF.dismiss();
			this.props.profileDataStore.gotoRegisterScreen()
		} else {
			showSnackbar(offlineMsg)
		}
	};

	onFacebookBtnPress = () => {
		this.onFocus();
		const { networkDataStore } = this.props;
		if (networkDataStore.isNetworkConnected) {
			stores.socialDataStore.fbSignIn()
		} else {
			showSnackbar(offlineMsg)
		}
	};

	onGoogleBtnPress = async () => {
		const { networkDataStore } = this.props;
		this.onFocus();
		if (networkDataStore.isNetworkConnected) {
			await stores.socialDataStore.googleSignIn()
		} else {
			showSnackbar(offlineMsg)
		}
	};

	render() {
		const { styles, userId } = this.state;

		return (
			<View style={styles.mainView}>
				<KeyboardAwareScrollView showsVerticalScrollIndicator={false}
																 enableOnAndroid={true}
																 keyboardShouldPersistTaps='always'>
					{
						!userId && this.renderSocialButtons()
					}
					{
						!userId && <View style={styles.orContainer}>
							<Image source={icons.OR_LINE_ICON} style={styles.orLine} />
							<Text style={styles.textStyleOR}>OR</Text>
						</View>
					}
					{
						userId && <View>
							<H1 style={styles.item}>Enter mobile no.</H1>
						</View>
					}
					{this.renderForm()}
					{
						!userId && this.renderLoginButton(styles)
					}
				</KeyboardAwareScrollView>
			</View>
		)
	}
}

const getStyle = () => StyleSheet.create({
	mainView: {
		flex: 1,
		backgroundColor: colors.White,
		paddingTop: verticalScale(5),
		paddingLeft: 30,
		paddingRight: 30
	},
	buttonView: {
		flexDirection: 'row',
		alignSelf: 'center',
		marginTop: verticalScale(15),
		justifyContent: 'center'
	},
	loginButtonView: {
		width: '100%',
		alignSelf: 'center',
		backgroundColor: colors.ParentBlue,
		marginTop: verticalScale(10),
		height: isTablet() ? isPortrait() ? 55 : 55 : isPortrait() ? 45 : 45,
		borderRadius: 4,
		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 1 : 4,
		shadowRadius: 2
	},
	buttonNext: {
		fontSize: isTablet() ? widthPercentage(2.5) : widthPercentage(2.5),
		fontWeight: '500',
		lineHeight: isTablet() ? isPortrait() ? 35 : 35 : isPortrait() ? 30 : 30,
		marginTop: isTablet() ? isPortrait() ? -5 : -5 : isPortrait() ? -5 : -5
	},
	textStyle: {
		fontFamily: 'Roboto',
		textAlign: 'center',
		marginTop: verticalScale(20),
		marginBottom: verticalScale(0),
		fontSize: isPortrait() ? verticalScale(15) : widthPercentage(2.5)
	},
	newtomypat: {
		opacity: 0.8,
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5)
	},
	newtoregister: {
		opacity: 0.8,
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5),
		color: colors.ParentBlue
	},
	orContainer: {
		position: 'relative',
		marginTop: widthPercentage(5),
		marginBottom: widthPercentage(5),
	},
	orLine: {
		alignSelf: 'center',
		width: widthPercentage(25)
	},
	textStyleOR: {
		alignSelf: 'center',
		top: -12.5,
		left: 0,
		right: 0,
		width: 30,
		height: 25,
		borderRadius: 12.5,
		paddingLeft: 6,
		paddingRight: 2,
		paddingTop: 2,
		paddingBottom: 2,
		backgroundColor: colors.White,
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2)
	},
	item: {
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1),
		marginBottom: 50
	},
	socialButton: {
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
		paddingLeft: isTablet() ? isPortrait() ? widthPercentage(2) : widthPercentage(1.5) : isPortrait() ? widthPercentage(2.8) : widthPercentage(2),
		paddingRight: isTablet() ? isPortrait() ? widthPercentage(2) : widthPercentage(1.5) : isPortrait() ? widthPercentage(2.8) : widthPercentage(2)
	},
	socialButtonText: {
		paddingLeft: isTablet() ? isPortrait() ? widthPercentage(1.5) : widthPercentage(1) : isPortrait() ? widthPercentage(2) : widthPercentage(1.5)
	},
	forgotPasswordStyle: {
		marginTop: heightPercentage(3),
		flexDirection: 'column',
		alignSelf: 'center',
		justifyContent: 'center'
	},
	renderRegisterStyle: {
		width: widthPercentage(80),
		marginTop: verticalScale(53),
		paddingBottom: verticalScale(2),
		flexDirection: 'row'
	},
	forgotPasswordText: {
		alignSelf: 'center',
		color: 'rgb(90,167,245)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		marginTop: verticalScale(5)
	},
	formView: {
		marginTop: verticalScale(0)
	},
	togglePasswordStyle: {
		position: 'absolute',
		top: isTablet() ? isPortrait() ? 5 : 5 : isPortrait() ? 15 : 15,
		right: 0,
		opacity: 0.6
	},
	socialImage: {
		width: verticalScale(30),
		height: verticalScale(30),
		paddingRight: 18
	},
	toggleIcon: {
		width: isPortrait() ? verticalScale(28) : widthPercentage(4),
		height: isPortrait() ? verticalScale(28) : widthPercentage(4)
	},
	textInput: {
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		width: '100%',
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		paddingVertical: 10
	}
});
