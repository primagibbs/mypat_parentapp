import React, { Component } from 'react';
import {StyleSheet, View, ImageBackground} from 'react-native';
import { NotificationComponent } from '../components/others';
// @ts-ignore
import Navigation from "react-navigation";
import { H1 } from 'native-base';
import {NotificationStore, TabBarDataStore} from '../store';
import {isPortrait, verticalScale, widthPercentage} from "../common-library/common";
import {colors} from "../common-library/config";
import {inject} from "mobx-react";
import {icons} from "../common";

interface Props {
	// @ts-ignore
	navigation?: Navigation
	notificationStore?: NotificationStore
	tabBarDataStore?: TabBarDataStore
}

@inject('notificationStore', 'tabBarDataStore')
export class NotificationListPage extends Component<Props> {
	constructor(props: Props) {
		super(props)
	}

	async componentDidMount() {
		const { notificationStore, tabBarDataStore } = this.props;
		if (tabBarDataStore.notificationCount > 0) {
			await notificationStore.updateGlobalStatusNotification();
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground}>
					<View style={styles.heading}>
						<H1 style={styles.headingTitle}>Notifications</H1>
					</View>
				</ImageBackground>
				<NotificationComponent navigation={this.props.navigation} />
			</View>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	headerBackground: {
		flex: 1,
		height: widthPercentage(40),
		width: widthPercentage(100)
	},
	heading: {
		flex: 1
	},
	headingTitle: {
		fontSize: verticalScale(24),
		lineHeight: verticalScale(24),
		fontWeight: '500',
		color: colors.White,
		marginTop: verticalScale(22),
		marginBottom: verticalScale(22),
		marginLeft: verticalScale(15)
	}
});
