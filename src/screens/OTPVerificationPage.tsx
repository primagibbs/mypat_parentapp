import React from 'react';
import { Component } from 'react';
import {
	View, StyleSheet, TouchableOpacity, Keyboard, Dimensions,
	BackHandler, Platform, Image
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { H1, Text } from 'native-base';
// @ts-ignore
import { TextField } from 'react-native-material-textfield';
import OtpInputs from 'react-native-otp-inputs';
import RNOtpVerify from 'react-native-otp-verify';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, isTablet, icons
} from '../common';
import { getMobileObj } from '../utils'
import { colors } from '../config'
import { goBack, showSnackbar, showModal} from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { AppDataStore, ProfileDataStore } from '../store'
import { CongratsPage } from "../screens/";
import { KEYBOARD_INPUT_OFFSET } from '../common';
import console = require('console');

interface Props {
	profileDataStore?: ProfileDataStore
	appDataStore?: AppDataStore
	textValue?: string,
	navigation?: any
}

interface State {
	otp?: number
	mobile?: string
	userId?: any
	isValid?: boolean
	styles?: any
	countryCode?: any
	addCountryCode?: boolean
	countdown?: number,
	timer?: any,
	previousScreen?: string
}

const ERR_MSG_VAL = 'Invalid input provided';
const REGEX_VALID_OTP = /^[0-9]{4}$/;

@inject('profileDataStore', 'appDataStore')
@observer
export class OTPVerificationPage extends Component<Props, State> {
	otpRef: any;
	// @ts-ignore
	pageType;

	congratsShown: boolean;
	otpProgress: number = 0;
	constructor(props: any) {
		super(props);
		this.state = {
			otp: undefined,
			mobile: this.props.navigation.getParam('mobile'),
			previousScreen: this.props.navigation.getParam('previous_screen'),
			userId: null,
			isValid: false,
			styles: getStyle(),
			countryCode: '',
			addCountryCode: false,
			timer: null,
			countdown: 30
		};
		this.otpRef = React.createRef();
		this.congratsShown = false;
	}

	async componentDidMount() {
		if (Platform.OS === 'android') {
			await this.startListeningForOtp();
		}
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		this.props.profileDataStore.setNavigationObject(this.props.navigation);
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}

		const responses = await getMobileObj();
		let userId;
		if (responses && responses.userId) {
			userId = responses.userId;
		}

		let timer = setInterval(this.tick, 1000);

		this.setState({
			userId,
			timer
		})
		//hash generation is one time process. Build generated from different systems will have different hashcodes
		// RNOtpVerify.getHash()
		// 	.then(console.log)
		// 	.catch(console.log)
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
		clearInterval(this.state.timer);
		if (Platform.OS === 'android') {
			RNOtpVerify.removeListener();
		}
	}

	otpHandler = async (message: string) => {
		const readOtp = /(\d{4})/g.exec(message);
		if (readOtp) {
			const otp = readOtp[1];
			if (otp) {
				this.otpRef.current.state.otpCode = otp.split("").map((num) => {
					return num.toString();
				});
				RNOtpVerify.removeListener();
				Keyboard.dismiss();
				await this.onNextPress(otp);
			}
		}
	};

	startListeningForOtp = () =>
		RNOtpVerify.getOtp()
			.then(p => RNOtpVerify.addListener(this.otpHandler))
			.catch(p => console.log(p));

	tick = () => {
		if (this.state.countdown !== 0) {
			this.setState({
				countdown: this.state.countdown - 1
			});
		} else {
			clearInterval(this.state.timer);
		}
	};

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	async onOTPChange(otp: any) {
		if(otp) this.otpProgress++;
		// if (this.props.profileDataStore.isOTPWrong) {
		// 	this.props.profileDataStore.setIsOTPWrong();
		// }
		this.setState({ otp: otp });
		if (otp.length === 4) {
			this.otpProgress = 0;
			await this.onNextPress(otp);
		}
	}

	validateRegex = (input: any, regex: any) => {
		return !!regex.test(input);
	};

	validateInputValue = (otp: any) => {
		let isOTPValid = this.validateRegex(otp, REGEX_VALID_OTP);

		if (!isOTPValid) {
			showSnackbar(ERR_MSG_VAL);
		} else {
			return true;
		}
	};

	async onNextPress(otp: any) {
		Keyboard.dismiss();
		if (this.validateInputValue(otp)) {
			const { profileDataStore } = this.props;

			await profileDataStore.verifyOTP({
				mobile: this.state.mobile,
				OTP: otp,
				userId: this.state.userId
			});
			console.log("VerifyResponse", this.props.profileDataStore.isOTPWrong);
			if (this.props.profileDataStore.isOTPWrong) {
				this.otpRef.current.reset();
			} else {
				profileDataStore.setIsOTPWrong();
			}
		} else {
			this.otpRef.current.reset();
		}
	}

	resendOTP = async () => {
		Keyboard.dismiss();
		const { mobile } = this.state;
		const { profileDataStore } = this.props;

		if (profileDataStore.isOTPWrong) {
			profileDataStore.setIsOTPWrong();
		}

		let timer = setInterval(this.tick, 1000);
		this.setState({
			timer,
			countdown: 30
		});

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}

		await profileDataStore.updateUserDataViaSignup({
			userInfo: mobile
		});
	};

	_onAccountCreation() {
		const { navigation, profileDataStore } = this.props;
		const data = {
			icon: icons.SMILEY_NEW_ICON,
			heading: 'Congratulations!',
			subHeading: 'Your account has been created',
			btnText: 'CONTINUE',
			userProfile: profileDataStore.userProfileDetails ? profileDataStore.userProfileDetails : ''
		};
		if(!this.congratsShown) {
			this.congratsShown = true;
			showModal(
				<CongratsPage
					navigation={navigation}
					nextPage={'AddInfoPage'}
					data={data} />,
				{
					hideDialogOnTouchOutside: false,
					positioning: 'center'
				}
			);
		}
	}

	_onMobileEdit() {
		this.onBackPressed();
	}

	_maskNumber(number: string) {
		return number.replace(/\d(?=\d{4})/g, "*");
	}

	render() {
		const { mobile, styles, countdown, previousScreen } = this.state;
		const isError = this.props.profileDataStore.isOTPWrong;
		const showVerifyButton = false;//!isError && (otp !== undefined && otp.toString().length === 4);
		return (
			<View style={styles.container}>
				<KeyboardAwareScrollView
						showsVerticalScrollIndicator={false}
						enableOnAndroid={true}
						keyboardShouldPersistTaps='handled'
						// behavior="padding"
						extraHeight={ KEYBOARD_INPUT_OFFSET }>
					<View style={{paddingBottom: 50}}>
						{
							previousScreen && previousScreen === 'signup' ? <H1 numberOfLines={1} style={styles.item}>Verify your mobile no.</H1> : <H1 style={styles.item}>Enter OTP</H1>
						}

						{
							previousScreen && previousScreen === 'signup' ? <Text style={styles.itemDesc}>
								OTP has been sent to your mobile no.
							</Text> : <Text style={styles.itemDesc}>
								OTP has been sent to your registered no.
							</Text>
						}
						<View style={{flexDirection: 'row', alignItems: 'center'}}>
							{
								previousScreen && previousScreen === 'signup' ? <Text style={styles.itemDesc}>{mobile}</Text> : <Text style={styles.itemDesc}>{this._maskNumber(mobile)}</Text>
							}
							<TouchableOpacity onPress={this._onMobileEdit.bind(this)}>
								<Image source={icons.PENCIL_ICON} style={styles.pencilIcon} />
							</TouchableOpacity>
						</View>
						<View style={styles.textFieldStyle}>
							<View style={{ flexDirection: 'row', position: 'relative', width: widthPercentage(90)}}>
								<View>
									<OtpInputs
										ref={this.otpRef}
										clearTextOnFocus={true}
										inputStyles={[styles.otpInput, isError ? styles.errorInput : '']}
										inputContainerStyles={styles.otpInputContainer}
										handleChange={code => this.onOTPChange(code)}
										numberOfInputs={4}
									/>
								</View>
								<View style={{justifyContent: 'flex-end', paddingBottom: 1}}>
								{
									countdown !== 0 && <Text style={{
										fontWeight: 'bold',
										color: colors.SubHeaderColor,
										minWidth: verticalScale(62),
										textAlign: 'center',
										fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
										marginLeft: verticalScale(10),
										lineHeight: verticalScale(18)}}>{`00:${countdown < 10 ? '0' + countdown : countdown}`}</Text>
								}
								{
									countdown === 0 && <TouchableOpacity onPress={() => this.resendOTP()}>
										<Text style={{
											fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.2),
											fontFamily: 'Roboto',
											color: colors.ParentBlue,
											textAlign: 'center',
											marginLeft: verticalScale(10),
											lineHeight: verticalScale(13)}}>Resend OTP</Text>
									</TouchableOpacity>
								}
								</View>
							</View>
							{
								isError && <View style={{flexDirection: 'row'}}>
									<Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.4), marginTop: 6}}>Wrong OTP entered.</Text>
									<TouchableOpacity onPress={this.resendOTP.bind(this)}>
										<Text style={{color: colors.ParentBlue, fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4), marginTop: 6}}>Try again.</Text>
									</TouchableOpacity>
								</View>
							}
						</View>
						{/* {
							showVerifyButton && <Button
								onPress={async () => { await this.onNextPress() }}
								style={styles.styleButton}
								title='VERIFY NOW'
								textStyle={styles.buttonVerifyNow}
							/>
						} */}
					</View>
				</KeyboardAwareScrollView>
				{
					this.props.profileDataStore.isModal && this._onAccountCreation()
				}
			</View>
		)
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30
	},
	textFieldView: {
		marginTop: verticalScale(35),
		justifyContent: 'space-between',
		width: widthPercentage(64),
		marginLeft: widthPercentage(18),
		marginRight: widthPercentage(18)
	},
	itemDesc: {
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		color: colors.SubHeaderColor,
		marginTop: 5,
		marginBottom: 5
	},
	pencilIcon: {
		marginVertical: verticalScale(8),
		marginHorizontal: verticalScale(12),
		width: verticalScale(12),
		height: verticalScale(12)
	},
	textFieldStyle: {
		marginTop: widthPercentage(5)
	},
	item: {
		color: colors.HeaderColor,
		marginTop: heightPercentage(10),
		marginBottom: 20
	},
	buttonVerifyNow: {
		fontSize: verticalScale(14),
		lineHeight: isTablet() ? isPortrait() ? 35 : 35 : isPortrait() ? 30 : 30,
		marginTop: isTablet() ? isPortrait() ? -5 : -5 : isPortrait() ? -5 : -5
	},
	styleButton: {
		width: '100%',
		fontWeight: '500',
		backgroundColor: colors.ParentBlue,
		alignSelf: 'center',
		marginTop: verticalScale(10),
		height: isTablet() ? isPortrait() ? 55 : 55 : isPortrait() ? 45 : 45,
		borderRadius: 4,
		shadowColor: 'rgba(0, 0, 0, 0.2)',

		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 2 : 4,
		shadowRadius: 2
	},
	otpInputContainer: {
		borderBottomWidth: 0,
		width: verticalScale(37),
		height: verticalScale(36),
		margin: 0,
		marginRight: verticalScale(6)
	},
	otpInput: {
		fontSize: verticalScale(14),
		width: verticalScale(37),
		height: verticalScale(36),
		justifyContent: 'center',
		borderWidth: 1,
		borderRadius: 4,
		marginBottom: 0,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		paddingTop: 0,
		paddingBottom: 0
	},
	errorInput: {
		borderColor: '#EB1919'
	}
});
