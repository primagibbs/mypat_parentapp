import React, { Component } from 'react'
import { View, StyleSheet, BackHandler, Linking, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
// @ts-ignore
import Navigation from 'react-navigation';
import { goBack, showSnackbar } from '../services'
import { COMMON_BASE_URL } from '../common'

interface Props {
  navigation?: Navigation
}

export class PrivacyPolicyPage extends Component<Props> {

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPressed)
  }

  onBackPressed = () => {
    const { navigation } = this.props
    return goBack(navigation)
  }
  handleDataReceived(msgData) {
    const newsUrl = msgData.data.url
    if (newsUrl) {
      Linking.canOpenURL(newsUrl).then(supported => {
        if (supported) {
          Linking.openURL(newsUrl)
        } else {
          showSnackbar('No Browser Available')
        }
      })
    } else {
      showSnackbar('No Details Available')
    }

  }
  onWebViewMessage(event: any) {
    let msgData;
    try {
      msgData = JSON.parse(event.nativeEvent.data)
    } catch (err) {
      return
    }
    switch (msgData.targetFunc) {
      case 'handleDataReceived':
        this[msgData.targetFunc].apply(this, [msgData]);
        break;
      default:
        break
    }
  }
  onError(err) {
   // console.warn('err', err)
  }

  ActivityIndicatorLoadingView() {
    return (
      <ActivityIndicator
        color='#009688'
        size='large'
        style={styles.ActivityIndicatorStyle}
      />
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <WebView
          style={{ flex: 1 }}
          source={{ uri: 'https://mypat.in/m.privacy-policy' }}
          onMessage={this.onWebViewMessage.bind(this)}
          onError={this.onError.bind(this)}
          renderLoading={this.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
		paddingTop: 10
  },
  ActivityIndicatorStyle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
}
})
