import React, { Component } from 'react';
import {
	StyleSheet, View, Text, TouchableOpacity, Dimensions, Keyboard, TextInput, Image, BackHandler, Platform
} from 'react-native';
import { inject, observer } from 'mobx-react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import transport from '../common-library/events';
import { navigateSimple, goBack } from '../services';
import { Button } from '../components';
import { ProfileDataStore } from '../store';
import { verticalScale, widthPercentage, heightPercentage, isPortrait, isTablet, icons, COUNTRY_CODE } from '../common';
import { validateMobileNumber, getDeviceCountryCode, isPhoneNumber } from '../utils';
import stores from '../store';
import { USER_INFO_TYPE } from '../common';
import { KEYBOARD_INPUT_OFFSET } from '../common/Constant';
import {colors} from "../common-library/config";
import CountryPicker from 'react-native-country-picker-modal'
import { relative } from 'path';
import console = require('console');

interface Props {
	userInfo?: string,
	profileDataStore?: ProfileDataStore,
	navigation?: any
}

interface State {
	userInfo?: string,
	isError?: boolean
	profileDataStore?: ProfileDataStore
	styles?: any
	countryCode?: any
	errorMessage?: string
	addCountryCode?: boolean
	cca2: string
	countryObj?: any
}

const KEYBOARDREF = Keyboard;

@inject('profileDataStore')
@observer
export class SignupPage extends Component<Props, State> {
	countryPickerRef: any;
	constructor(props: any) {
		super(props);
		this.state = {
			userInfo: '',
			isError: false,
			styles: getStyle(),
			countryCode: '91',
			addCountryCode: true,
			countryObj: '',
			errorMessage: '',
			cca2: 'IN',
		}
		this.countryPickerRef = React.createRef();
	}

	async componentDidMount() {
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));

		const countryCode = getDeviceCountryCode() || "91";
		this.setState({countryCode : "91"});

		this.props.profileDataStore.setNavigationObject(this.props.navigation);
		stores.socialDataStore.setNavigationObject(this.props.navigation);

		await stores.socialDataStore.setupGoogleSignIn();

		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
		transport.setData('Page Viewed', { 'Page Name': 'sign-up', 'Device Type': Platform.OS });
		transport.post();
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		this.setState({
			userInfo: '',
			isError: false
		})
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	renderSocialButtons = () => {
		const { styles } = this.state;
		return (<View style={this.state.styles.buttonView}>
			<View style={{flex: 10}}/>
			<Button
				transparent={true}
				onPress={this.onFacebookBtnPress.bind(this)}
				style={styles.socialButton}
				iconLeft={true}>
				<View>
					<Image
						style={this.state.styles.socialImage}
						source={require('../../images/facebook.png')}
						resizeMode={'contain'}
					/>
				</View>
			</Button>
			<Button
				transparent={true}
				onPress={this.onGoogleBtnPress.bind(this)}
				style={styles.socialButton}
				iconLeft={true}
			>
				<View>
					<Image
						style={this.state.styles.socialImage}
						source={require('../../images/google.png')}
						resizeMode={'contain'}
					/>
				</View>
			</Button>
			<View style={{flex: 10}}/>
		</View>)
	};

	onFocus() {
		this.setState({
			isError: false,
			errorMessage: ''
		})
	}

	async _onSubmitEditing(field: string) {
		if (field === 'mobile') {
			await this.onRegisterPress()
		}
	}

	onTextChange(userInfo: any) {
		this.setState({ userInfo: userInfo });
		const isValidPhone = isPhoneNumber(userInfo);
		// this.setState({ addCountryCode: isValidPhone })
	}

	renderForm = () => {
		const { styles, isError, errorMessage } = this.state;
		return <View style={styles.formView}>
			<View style={styles.phoneNumberContainer}>
				<View style={styles.countryCodeContainer}>
					{this.renderCountryCodeButton()}
				</View>
				<TextInput
					placeholder='Enter Mobile No.'
					onChangeText={(userInfo: string) => this.onTextChange(userInfo)}
					autoCorrect={false}
					onFocus={() => this.onFocus()}
					blurOnSubmit={true}
					returnKeyType='next'
					onSubmitEditing={this._onSubmitEditing.bind(this, 'mobile')}
					autoCapitalize={'none'}
					keyboardType={'number-pad'}
					clearButtonMode={'while-editing'}
					style={styles.textInput}
				/>
			</View>
			{
				isError && <Text style={styles.errorMessage}>{errorMessage}</Text>
			}
			<Button
				onPress={() => this.onRegisterPress()}
				style={styles.registerButtonView}
				title='NEXT'
				textStyle={styles.buttonNext}
			/>
		</View>
	};

	renderTerms = () => <View style={this.state.styles.renderTermsStyle}>
		<Text style={this.state.styles.termsCondition}>By Signing Up you agree to our</Text>
		<View style={{ flexDirection: 'row', alignSelf: 'center' }}>
			<TouchableOpacity onPress={() => this.onTermsBtnPress()}>
				<Text style={this.state.styles.termsConditionBlue}> T&C</Text>
			</TouchableOpacity>
			<Text style={this.state.styles.termsConditionAnd}> & </Text>
			<TouchableOpacity onPress={this.onPrivacyBtnPress.bind(this)}>
				<Text style={this.state.styles.termsConditionBlue}>Privacy Policy</Text>
			</TouchableOpacity>
		</View>
	</View>;

	renderLoginButton = () => <View style={this.state.styles.renderLoginStyle}>
		<View style={{ flexDirection: 'row', alignSelf: 'center' }}>
			{/* <Text style={[this.state.styles.termsConditionAnd, {
				fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5)
			}]}>Already have an account?</Text>
			<TouchableOpacity onPress={this.onLoginBtnPress.bind(this)} style={{ height: verticalScale(50) }}>
				<Text style={[this.state.styles.termsConditionBlue, {
					fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.5)
				}]}> Sign In here</Text>
			</TouchableOpacity> */}
		</View>
	</View>;

	renderCountryCodeButton() {
		const { addCountryCode, countryCode, styles } = this.state;
		if (addCountryCode) {
		return <View style={{flexDirection: 'row', alignItems: 'center', paddingRight: 4, marginTop: -4 }}>
				<CountryPicker
						ref={this.countryPickerRef}
						countryList={COUNTRY_CODE}
						value={countryCode}
						onChange={value => { this.setState({ cca2: value.cca2, countryCode: value.callingCode }) }}
						cca2={this.state.cca2}
						styles={{alignItems: 'center', backgroundColor: '#4287f5', flexDirection: "column", alignSelf : 'center', paddingBottom: 4}}
						hideAlphabetFilter={true}/>
				<Text style={styles.textCountryCode}>{'+' + countryCode}</Text>

			</View>
		}
		return null
	}

	validateForm = () => {
		const { userInfo } = this.state;
		this.props.profileDataStore.setUserInfoType(USER_INFO_TYPE.MOBILE);

		if (userInfo.length !== 10) {
			this.setState({
				isError: true,
				errorMessage: 'Please enter a valid mobile no.'
			});
		} else {
			// validate user info
			const userInfoError = validateMobileNumber(userInfo);

			if (userInfoError) {
				this.props.profileDataStore.setUserInfoType(USER_INFO_TYPE.NONE);
				// set errors
				this.setState({
					isError: true,
					errorMessage: userInfoError
				});
				return false;
			} else {
				return true;
			}
		}
	};

	async onRegisterPress() {
		Keyboard.dismiss();
		if (this.validateForm()) {
			const { userInfo, countryCode } = this.state;
			const { profileDataStore } = this.props;
			await profileDataStore.updateUserDataViaSignup({
				userInfo,
				countryCode
			})
		}
	}

	onTermsBtnPress = () => {
		KEYBOARDREF.dismiss();
		this.onFocus();
		navigateSimple(this.props.navigation, 'TermsOfUsePage')
	};

	onPrivacyBtnPress = () => {
		KEYBOARDREF.dismiss();
		this.onFocus();
		navigateSimple(this.props.navigation, 'PrivacyPolicyPage')
	};

	onLoginBtnPress = () => {
		KEYBOARDREF.dismiss();
		transport.setData('Signup', { 'Action': 'Clicked Login', 'Device Type': Platform.OS });
		transport.post();
		this.props.profileDataStore.gotoLoginScreen()
	};

	onFacebookBtnPress = () => {
		this.onFocus();
		stores.socialDataStore.fbSignIn()
	};

	onGoogleBtnPress = () => {
		this.onFocus();
		stores.socialDataStore.googleSignIn()
	};

	render() {
		const { styles } = this.state;

		return (
			<View style={styles.mainView}>
				<KeyboardAwareScrollView showsVerticalScrollIndicator={false}
																 enableOnAndroid={true}
																 extraHeight={KEYBOARD_INPUT_OFFSET}
																 keyboardShouldPersistTaps='handled'>
					{this.renderSocialButtons()}
					<View style={styles.orContainer}>
						<Image source={icons.OR_LINE_ICON} style={styles.orLine} />
						<View style={styles.orTextContainer}>
							<Text style={styles.textStyleOR}>OR</Text>
						</View>
					</View>
					{this.renderForm()}
					{this.renderTerms()}
					{this.renderLoginButton()}
				</KeyboardAwareScrollView>
			</View>
		)
	}
}

const getStyle = () => StyleSheet.create({
	mainView: {
		flex: 1,
		backgroundColor: colors.White,
		paddingTop: verticalScale(5),
		paddingLeft: 30,
		paddingRight: 30,
		justifyContent: 'center'
	},
	buttonView: {
		flexDirection: 'row',
		alignSelf: 'center',
		marginTop: verticalScale(10),
		justifyContent: 'center'
	},
	textInput: {
		fontFamily: 'Roboto',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		// width: '100%',
		flex: 1,
		paddingHorizontal: 8,
		paddingVertical: 10
	},
	textCountryCode:{
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		color: '#333333',
		alignSelf: 'center',
		paddingHorizontal: 4,
		paddingTop: 4
	},
	countryCodeContainer: {
		alignItems: 'center',
		zIndex: 999,
		paddingVertical: 4,
		borderRightColor: "#dedede",
		borderRightWidth: 1
	},
	countryPicker: {
		zIndex: 999,
	},
	textStyle: {
		fontFamily: 'Roboto',
		textAlign: 'center',
		marginTop: verticalScale(20),
		marginBottom: verticalScale(5),
		fontSize: isPortrait() ? verticalScale(15) : widthPercentage(2.5)
	},
	formView: {
		marginTop: verticalScale(2),
		position: 'relative'
	},
	phoneNumberContainer: {
		borderColor: 'rgba(153, 153, 153, 0.54)',
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: 12,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	renderTermsStyle: {
		width: widthPercentage(100),
		marginTop: verticalScale(53),
		paddingLeft: 8,
		paddingRight: 8,
		flexDirection: 'row',
		alignSelf: 'center',
		justifyContent: 'center'
	},
	renderLoginStyle: {
		marginTop: 10,
		flexDirection: 'row',
		alignSelf: 'center'
	},
	socialButton: {
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
		paddingLeft: isTablet() ? isPortrait() ? widthPercentage(2) : widthPercentage(1.5) : isPortrait() ? widthPercentage(2.8) : widthPercentage(2),
		paddingRight: isTablet() ? isPortrait() ? widthPercentage(2) : widthPercentage(1.5) : isPortrait() ? widthPercentage(2.8) : widthPercentage(2)
	},
	orContainer: {
		position: 'relative',
		marginTop: widthPercentage(5),
		marginBottom: widthPercentage(5),
	},
	orLine: {
		alignSelf: 'center',
		width: widthPercentage(25)
	},
	orTextContainer: {
		top: -12.5,
		left: 0,
		right: 0,
		height: 25,
		width: 40,
		borderRadius: 12.5,
		paddingLeft: 2,
		paddingRight: 2,
		paddingBottom: 2,
		backgroundColor: colors.White,
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center'
	},
	textStyleOR: {
		fontFamily: 'Roboto',
		fontSize: verticalScale(11)
	},
	registerButtonView: {
		width: '100%',
		backgroundColor: colors.ParentBlue,
		alignSelf: 'center',
		marginTop: verticalScale(10),
		height: isTablet() ? 55 : 45,
		borderRadius: 4,
		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 1 : 4,
		shadowRadius: 2
	},
	buttonNext: {
		fontSize: isTablet() ?  21: 16,
		fontWeight: '500',
		marginTop: isTablet() ? isPortrait() ? -5 : -5 : isPortrait() ? -5 : -5
	},
	togglePasswordStyle: {
		position: 'absolute',
		top: isTablet() ? isPortrait() ? 5 : 5 : isPortrait() ? 15 : 15,
		right: 0,
		opacity: 0.6
	},
	socialImage: {
		width: verticalScale(30),
		height: verticalScale(30),
		paddingRight: 18
	},
	toggleIcon: {
		width: isPortrait() ? verticalScale(28) : widthPercentage(4),
		height: isPortrait() ? verticalScale(28) : widthPercentage(4)
	},
	termsCondition: {
		fontFamily: 'Roboto',
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: '#666666'
	},
	termsConditionAnd: {
		fontFamily: 'Roboto',
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: '#666666'
	},
	termsConditionBlue: {
		fontFamily: 'Roboto',
		textAlign: 'center',
		fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
		color: colors.ParentBlue
	},
	errorMessage:{
		color: '#EB1919', 
		fontSize: isTablet() ? 18 : 14, 
		marginTop: 6
	}
});
