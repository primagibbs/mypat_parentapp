import React, { Component } from 'react';
import {
	View, Text, Dimensions, StyleSheet, Platform, ActivityIndicator
} from 'react-native';
import { widthPercentage } from '../common';
import { colors } from '../config';

interface State {
	styles?: any
}
interface Props {
	navigation?: any
}

const dim = Dimensions.get('screen');

export class SplashPage extends Component<Props, State> {
	constructor(props: any) {
		super(props);
		this.state = {
			styles: getStyle()
		}
	}

	componentDidMount() {
		Dimensions.addEventListener('change', this._orientationChangeListener)
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener)
	}

	_orientationChangeListener = () => {
		this.setState({
			styles: getStyle()
		})
	};

	renderLoader() {
		const { styles } = this.state;
		return <View style={styles.container}>
			<ActivityIndicator color={colors.White} size={'large'} />
		</View>
	}

	renderCopyrightLine() {
		const { styles } = this.state;
		if (Platform.OS !== 'ios') {
			return <Text style={styles.text}> © Powered By Edfora</Text>
		}
		return null
	}

	render() {
		const { styles } = this.state;
		return <View style={styles.mainView}>
			{/* {this.renderLoader()} */}
			{this.renderCopyrightLine()}
		</View>
	}
}

const getStyle = () => StyleSheet.create({
	mainView: {
		flex: 1
	},
	imgView: {
		flex: 1,
		width: undefined,
		height: undefined,
		resizeMode: 'stretch'
	},
	text: {
		color: colors.White,
		fontSize: 12,
		textAlign: 'center',
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		opacity: 1.0,
		bottom: 40,
		left: widthPercentage(50) - 60,
		zIndex: 1000
	},
	container: {
		position: 'absolute',
		flex: 1,
		width: '100%',
		height: '100%',
		backgroundColor: colors.Transparent,
		alignItems: 'center',
		justifyContent: 'center',
		opacity: 1.0,
		top: 60,
		zIndex: 1000
	}
});
