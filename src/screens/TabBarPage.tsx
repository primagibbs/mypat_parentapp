import { View, BackHandler, Platform } from 'react-native'
import React, { Component } from 'react'
// @ts-ignore
import { Navigation } from 'react-navigation';
import { inject, observer } from 'mobx-react'
import commonStores from '../common-library/store'

import {
	AppDataStore, HeaderDataStore, TestsStore, PerformanceStore,
	GoalsDataStore, AssignmentStore, HomeDataStore, GenericTabBarStore, TabBarDataStore
} from '../store'
import { OnDialogCallbacks } from '../components'
import {
	getGoalsData, getPackages, setDropDownGoalId, setDropDownGoalName, setSelectedGoal
} from '../utils';
import { goBack, navigateSimple, showGoalDropDown } from '../services'
import transport from '../common-library/events'
interface Props {
	navigation?: Navigation,
	headerDataStore?: HeaderDataStore
	appDataStore?: AppDataStore
	testsStore?: TestsStore,
	assignmentStore?: AssignmentStore
	performanceStore?: PerformanceStore
	goalsDataStore?: GoalsDataStore
	homeDataStore?: HomeDataStore
	genericTabBarStore?: GenericTabBarStore,
	tabBarDataStore?: TabBarDataStore
}
interface State {
}

@inject('headerDataStore', 'appDataStore', 'testsStore', 'assignmentStore', 'performanceStore',
	'goalsDataStore', 'homeDataStore', 'genericTabBarStore', 'tabBarDataStore')
@observer
export class TabBarPage extends Component<Props, State> implements OnDialogCallbacks {
	goalListData?: any;
	disabledPackagesArr: any;

	constructor(props: Props, state: State) {
		super(props, state)
	}

	onClose() {

	}

	async onItemClicked(data: any, show: any) {
		console.log("onItemClicked", data );
		if (data && data.name && data._id) {
			await this.renderGoalsData(data)
		}
		this.props.appDataStore.setDropdownClicked();
		this.props.goalsDataStore.hideGoalDropDown();
	}

	async renderGoalsData(data: any) {
		const { performanceStore, homeDataStore, headerDataStore, goalsDataStore } = this.props;
		this.disabledPackagesArr = await getGoalsData();
		headerDataStore.setdropdownRightTitle(data.name);
		headerDataStore.setdropdownRightTargetGoal(data);
		await goalsDataStore.setCurrentGoalExpired(data);
		await setSelectedGoal(data);
		homeDataStore.setGoalId(data._id);
		performanceStore.setGoalId(data._id);
		performanceStore.setGoalName(data.name);
		goalsDataStore.setSelectedGoalId(data._id);

		setDropDownGoalId(data._id);
		setDropDownGoalName(data.name);

		performanceStore.getUpcomingExam(performanceStore.getGoalId(), true);

		await homeDataStore.isOldMyPat(headerDataStore.getDropdownRightTargetGoal());
		performanceStore.getGoalProgress(performanceStore.getSwitchType(), performanceStore.getGoalId(), true);
	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed)
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}

	handleDropdownClick() {
		const { goalsDataStore } = this.props;
		this.goalListData = goalsDataStore.getGoalsData();

		if (this.props.appDataStore.dropdownRightClicked && this.goalListData && this.goalListData.length >= 1) {
			showGoalDropDown('', this, '1', '', true, this.goalListData, 'name', this.disabledPackagesArr)
		} else  {
			this.props.goalsDataStore.hideGoalDropDown();
			return null;
		}
	}

	render() {
		return (
			<View>
				{this.handleDropdownClick()}
			</View>
		)
	}
}
