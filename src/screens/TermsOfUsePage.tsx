import React, { Component } from 'react'
import { View, StyleSheet, BackHandler, Linking, Platform, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
import { goBack, showSnackbar } from '../services'
// @ts-ignore
import Navigation from 'react-navigation'
import { COMMON_BASE_URL } from '../common'
import transport from '../common-library/events'

interface Props {
	navigation?: Navigation
}

export class TermsOfUsePage extends Component<Props> {

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
		transport.setData('Page Viewed', { 'Page Name': 'terms-of-use', 'Device Type': Platform.OS })
		transport.post()
	}

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed)
	}
	handleDataReceived(msgData) {
		const newsUrl = msgData.data.url;
		if (newsUrl) {
			Linking.canOpenURL(newsUrl).then(supported => {
				if (supported) {
					Linking.openURL(newsUrl)
				} else {
					showSnackbar('No Browser Available')
				}
			})
		} else {
			showSnackbar('No Details Available')
		}

	}
	onWebViewMessage(event) {
		let msgData;
		try {
			msgData = JSON.parse(event.nativeEvent.data)
		} catch (err) {
			return
		}
		switch (msgData.targetFunc) {
			case 'handleDataReceived':
				this[msgData.targetFunc].apply(this, [msgData]);
				break
			default:
				break
		}
	}
	onError(err) {
		// console.warn('err', err)
	}
	ActivityIndicatorLoadingView() {
		return (
			<ActivityIndicator
				color='#009688'
				size='large'
				style={styles.ActivityIndicatorStyle}
			/>
		)
	}

	render() {
		return (
			<View style={styles.container}>
				<WebView
					style={{ flex: 1 }}
					source={{ uri: 'https://mypat.in/m.terms-of-use' }}
					onMessage={this.onWebViewMessage.bind(this)}
					onError={this.onError.bind(this)}
					renderLoading={this.ActivityIndicatorLoadingView}
					startInLoadingState={true}
				/>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10
	},
	ActivityIndicatorStyle: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center'
	}
});
