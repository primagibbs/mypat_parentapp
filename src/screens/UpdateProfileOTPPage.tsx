import React from 'react';
import { Component } from 'react';
import {
	View, StyleSheet, TouchableOpacity, Keyboard, Dimensions, BackHandler, Image
} from 'react-native'
import { inject, observer } from 'mobx-react';
import OtpInputs from 'react-native-otp-inputs'
import {H2, Text} from 'native-base';
import {
	widthPercentage, heightPercentage, verticalScale, isPortrait, icons, isTablet
} from '../common';
import { colors } from '../config'
import {goBack} from '../services'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { ProfileDataStore} from '../store'

interface Props {
	profileDataStore?: ProfileDataStore
	navigation?: any
}

interface State {
	textValue?: number,
	styles?: any,
	mobileNumber?: number,
	countdown?: number,
	timer?: any
}

@inject('profileDataStore')
@observer
export class UpdateProfileOTPPage extends Component<Props, State> {
	pageType: any;
	otpRef: any;
	constructor(props: any) {
		super(props);
		this.state = {
			textValue: null,
			countdown: 30,
			timer: null,
			styles: getStyle(),
			mobileNumber: this.props.navigation.getParam('mobileNumber')
		};
		this.otpRef = React.createRef();
	}

	async componentDidMount() {
		const { profileDataStore, navigation } = this.props;
		profileDataStore.setNavigationObject(navigation);
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}

		let timer = setInterval(this.tick, 1000);
		this.setState({
			timer
		})
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this));
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
		clearInterval(this.state.timer);
	}

	tick = () => {
		if (this.state.countdown !== 0) {
			this.setState({
				countdown: this.state.countdown - 1
			});
		} else {
			clearInterval(this.state.timer);
		}
	};

	onBackPressed = () => {
		const { navigation } = this.props;
		return goBack(navigation)
	};

	_orientationChangeListener() {
		Keyboard.dismiss();
		this.setState({
			styles: getStyle()
		})
	}

	onOTPChange(userInfo: any) {
		if (this.props.profileDataStore.isOTPWrong) {
			this.props.profileDataStore.setIsOTPWrong();
		}
		this.setState({ textValue: userInfo });
	}

	resendOTP = async () => {
		Keyboard.dismiss();
		const { mobileNumber } = this.state;
		const { profileDataStore, navigation } = this.props;

		if (profileDataStore.isOTPWrong) {
			profileDataStore.setIsOTPWrong();
		}

		let timer = setInterval(this.tick, 1000);
		this.setState({
			timer,
			countdown: 30
		});

		if (this.otpRef && this.otpRef.current) {
			this.otpRef.current.reset();
		}

		profileDataStore.setNavigationObject(navigation);
		await profileDataStore.sendOTPForMobileChange(mobileNumber);
	};

	_maskNumber(number: number) {
		return number.toString().replace(/\d(?=\d{4})/g, "*");
	}

	redirectToPage() {
		this.onBackPressed();
	}

	async onNextPress() {
		Keyboard.dismiss();
		const { profileDataStore } = this.props;
		await profileDataStore.verifyOTPForMobileChange(this.state.mobileNumber, this.state.textValue);
	}

	render() {
		const { styles, mobileNumber, countdown, textValue } = this.state;
		const isError = this.props.profileDataStore.isOTPWrong;
		const showVerifyButton = !isError && (textValue !== null && textValue.toString().length === 4);
		return (
			<>
				<View style={styles.container}>
					<View style={styles.heading}>
						<View style={{flexDirection: 'row'}}>
							<TouchableOpacity onPress={() => this.redirectToPage()}>
								<Image source={icons.BACK_ARROW_BLUE} style={{marginTop: 29, width: 24, height: 19}} />
							</TouchableOpacity>
						</View>
					</View>
					<KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
						<View>
							<H2 style={styles.item}>Enter OTP</H2>
							<View>
								<Text style={styles.itemDesc}>OTP has been sent to your registered mobile no.</Text>
								<Text style={styles.itemDesc}>{this._maskNumber(mobileNumber)}</Text>
							</View>
							<View style={styles.textFieldStyle}>
								<View style={{ flexDirection: 'row', position: 'relative', width: widthPercentage(90)}}>
									<View style={{ width: widthPercentage(50) }}>
										<OtpInputs
											ref={this.otpRef}
											clearTextOnFocus={true}
											inputStyles={[styles.otpInput, isError ? styles.errorInput : '']}
											inputContainerStyles={styles.otpInputContainer}
											handleChange={code => this.onOTPChange(code)}
											numberOfInputs={4}
										/>
									</View>
									<View style={{justifyContent: 'flex-end', paddingBottom: 10}}>
										{
											countdown !== 0 && <Text style={{
												fontWeight: 'bold',
												color: colors.SubHeaderColor,
												minWidth: verticalScale(62),
												textAlign: 'center',
												fontSize: isPortrait() ? verticalScale(11) : widthPercentage(2.2),
												marginLeft: verticalScale(10),
												lineHeight: verticalScale(18)}}>{`00:${countdown < 10 ? '0' + countdown : countdown}`}</Text>
										}
										{
											countdown === 0 && <TouchableOpacity onPress={() => this.resendOTP()}>
												<Text style={{
													fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.2),
													fontFamily: 'Roboto',
													color: colors.ParentBlue,
													textAlign: 'center',
													marginLeft: verticalScale(10),
													lineHeight: verticalScale(13)}}>Resend OTP</Text>
											</TouchableOpacity>
										}
									</View>
									{
										isError && <View style={{flexDirection: 'row'}}>
											<Text style={{color: '#EB1919', fontSize: isPortrait() ? verticalScale(13) : widthPercentage(2.4), marginTop: 6}}>Wrong OTP entered.</Text>
											<TouchableOpacity onPress={this.resendOTP.bind(this)}>
												<Text style={{color: colors.ParentBlue, fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4), marginTop: 6}}>Try again.</Text>
											</TouchableOpacity>
										</View>
									}
								</View>
							</View>
							{showVerifyButton && <TouchableOpacity
											onPress={this.onNextPress.bind(this)}>
								<Text style={styles.styleButton}>
									SUBMIT
								</Text>
							</TouchableOpacity>}
						</View>
					</KeyboardAwareScrollView>
				</View>
			</>
		)
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		backgroundColor: colors.White,
		flex: 1,
		paddingLeft: 30,
		paddingRight: 30,
		height: '90%'
	},
	textInput: {
		width: widthPercentage(80),
		borderColor: 'rgba(153, 153, 153, 0.54)',
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		borderWidth: 1,
		borderRadius: 4,
		paddingLeft: 10
	},
	itemDesc: {
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4),
		color: colors.ContentColor,
		marginBottom: verticalScale(12),
		width: widthPercentage(80)
	},

	item: {
		fontSize: isPortrait() ? verticalScale(20) : widthPercentage(3.2),
		color: colors.HeaderColor,
		marginTop: isPortrait() ? heightPercentage(10) : heightPercentage(1),
		marginBottom: verticalScale(16)
	},
	styleButton: {
		width: widthPercentage(86),
		marginTop: 20,
		marginBottom: verticalScale(15),
		padding: 10,
		textAlign: 'center',
		backgroundColor: colors.ParentBlue,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 4,
		color: colors.White,
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.4),

		shadowColor: 'rgba(0, 0, 0, 0.2)',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.8,
		elevation: isTablet() ? 2 : 4,
		shadowRadius: 2
	},
	otpInputContainer: {
		borderBottomWidth: 0,
		margin: 0
	},
	otpInput: {
		fontSize: isPortrait() ? verticalScale(14) : widthPercentage(2.5),
		width: verticalScale(37),
		height: verticalScale(36),
		justifyContent: 'center',
		borderWidth: 1,
		borderRadius: 4,
		marginRight: 5,
		borderColor: 'rgba(153, 153, 153, 0.54)',
		paddingTop: 0,
		paddingBottom: 0
	},
	errorInput: {
		borderColor: '#EB1919'
	},
	textFieldStyle: {
		marginTop: widthPercentage(5)
	}
});
