import React from 'react';
import { Component } from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { resetRouterSimple } from '../services'
import { icons } from '../common'
import { setCompleteWalkThroughPage } from '../utils'
import { PagerView, DotIndicatorView } from '../common-library/components/pager'

export const pagerView1 = <PagerView mainTxt='Stay Connected'
  subTxt='Be up to date with all tests and assignments shared with your child'
  imgUrl={icons.PAGER_IMG1}
  index='0' />;
export const pagerView2 = <PagerView mainTxt='Track Goal Progress'
  subTxt="Monitor your child's progress with crucial performance metrics"
  imgUrl={icons.PAGER_IMG2}
  index='0' />;
export const pagerView3 = <PagerView mainTxt='Smart Parenting'
  subTxt='Learn tricks from experts to be a smart parent'
  imgUrl={icons.PAGER_IMG3}
  index='0' />;

interface State {
  styles?: any
}
interface Props {
  navigation?: any
}

export class WalkThroughPage extends Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      styles: getStyle()
    }
  }

  componentDidMount() {
    Dimensions.addEventListener('change', this._orientationChangeListener)
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener)
  }

  _orientationChangeListener = () => {
    this.setState({
      styles: getStyle()
    })
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={[this.state.styles.mainView, StyleSheet.absoluteFill]}>
        <DotIndicatorView
          pagerViews={[pagerView1, pagerView2, pagerView3]}
          onComplete={async () => {
            await setCompleteWalkThroughPage();
            resetRouterSimple(navigation, 0, 'SignupPage')
          }}
        />
      </View>
    )
  }

}
const getStyle = () => StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 20,
    paddingRight: 20
  }
});
