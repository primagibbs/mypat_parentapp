import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Platform,
  ScrollView,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  FlatList,
  Animated,
  ActivityIndicator,
  ImageBackground
} from 'react-native'
import {
  heightPercentage,
  widthPercentage,
  isPortrait,
  isTablet,
  icons,
  BIGGER_PHONE_CATEGORIZE,
  getBiggerCategorization,
  verticalScale
} from '../../common'
import { colors } from '../../common-library/config'
import { dimens } from '../../config'
import stores from '../../store'
import { navigateSimple, goBack, showSnackbar } from '../../services'
import { CheckBox, Radio, Button } from 'native-base'

interface Props {
  navigation?: any
  comparisonByData: any[]
  comparisonOfData: any[]
  selectedCategory: any
  selectedComparisonParam: any
  onChange: (selectedCategory, selectedComparisonParam, isAppliedFilter) => void
  onClose: (selectedCategory, selectedComparisonParam, showFilter) => void
  dismiss: () => void
  isFilterApplied: boolean
}

interface State {
  styles?: any
  selectedCategory?: any
  selectedComparisonParam?: any
  isLoading?: boolean
  isFilterAppliedState?: boolean
}

const FILTER_ITEM = {
  SCORE: { key: ['selectAnswer'], title: 'Score' },
  ATTEMPT_RATE: { key: ['taggedconcept'], title: 'Attempt Rate' },
  ACCURACY_RATE: { key: ['selectAnswer'], title: 'Accuracy Rate' },
  NEGATIVE_MARKS: { key: ['selectAnswer'], title: 'Negative Marks' },
  TIME: { key: ['taggedconcept'], title: 'Time' },
  SUBJECT: { key: ['selectAnswer'], title: 'Subject' },
  DIFFICULTY: { key: ['taggedconcept'], title: 'Difficulty' },
  QUESTION_TYPE: { key: ['selectAnswer'], title: 'Question Type' },
  SKILL: { key: ['taggedconcept'], title: 'Skill' }
}
export class ComparisonFilterPage extends Component<Props, State> {
  animatedValue = new Animated.Value(1)
  headerFontSize = new Animated.Value(24)
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      isLoading: false,
      selectedComparisonParam: props.selectedComparisonParam,
      selectedCategory: props.selectedCategory,
      isFilterAppliedState: props.isFilterApplied
    }
  }
  componentDidMount() {
    Dimensions.addEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    // BackHandler.addEventListener('hardwareBackPress', () =>
    //   this.onBackPressed()
    // )
  }

  componentWillUnmount() {
    Dimensions.removeEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    // BackHandler.removeEventListener('hardwareBackPress', () =>
    //   this.onBackPressed()
    // )
  }

  // onBackPressed() {
  //   const { navigation } = this.props
  //   return goBack(navigation)
  // }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  onFilterIconPress = () => {
    navigateSimple(this.props.navigation, 'ComparisonFilterPage')
  }

  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }

  layout = (data, index) => {
    return { length: 80, offset: 80 * index, index }
  }

  setSelectedComparisonBy(selection) {
    stores.testAttemptStore.setSelectedCategory(selection)
    this.setState({
      selectedComparisonParam: selection
    })
  }

  setSelectedCategory(selection) {
    this.setState({
      selectedCategory: selection
    })
  }

  handleApplyFilter() {
    const { onChange } = this.props
    const { selectedCategory, selectedComparisonParam } = this.state
    if (onChange) {
      onChange(selectedCategory, selectedComparisonParam, true)
    }
  }
  closeFilter() {
    const { dismiss } = this.props
    // if (dismiss) {
    //   dismiss()
    // }
  }

  getSelectedCatagory() {
    const { selectedCategory } = this.state
    // if (selectedCategory) {
    //   return selectedCategory
    // }
    if (stores.testAttemptStore.comparisonData && stores.testAttemptStore.comparisonData.categories) {
      return stores.testAttemptStore.comparisonData.categories[0]
    }
  }
  getSelectedComparisonParam() {
    const { selectedComparisonParam } = this.state
    // if (selectedComparisonParam) {
    //   return selectedComparisonParam
    // }
    if (stores.testAttemptStore.comparisonData && stores.testAttemptStore.comparisonData.compareBy) {
      return stores.testAttemptStore.comparisonData.compareBy[0]
    }
  }

  handleResetFilter() {
    const { onChange , onClose, isFilterApplied} = this.props
  //  const { selectedCategory, selectedComparisonParam } = this.state
    const selectedCategory = this.getSelectedCatagory()
    const selectedComparisonParam = this.getSelectedComparisonParam()
    if (isFilterApplied) {
      if (onClose) {
        onClose(selectedCategory, selectedComparisonParam, true)
        onChange(selectedCategory, selectedComparisonParam, false)
      }
    } else {
      showSnackbar('Please apply filter first !')
    }
  }

  renderRadioButton(isSelected) {
    if (Platform.OS === 'ios') {
      // tslint:disable-next-line:no-unused-expression
      return (<View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
        <CheckBox checked={isSelected} />
      </View>)
    } else {
      // tslint:disable-next-line:no-unused-expression
      return <Radio selected={isSelected} />
    }
  }

  renderItem(item, selection, temporarySelection, updateHandler) {
    const { selectedComparisonParam } = this.props
    const { styles } = this.state
    if (item && temporarySelection) {
    return (
      <TouchableOpacity
        key={selectedComparisonParam.key}
        style={styles.rowView}
        onPress={() => updateHandler(item)}
      >
        {this.renderRadioButton(item.key === temporarySelection.key)}
        <Text style={styles.filterItemTextStyle}>{item.value}</Text>
      </TouchableOpacity>
    )
    }
    return null
  }

  renderComaprisonBy() {
    const { comparisonByData, selectedComparisonParam } = this.props
    const { styles, selectedComparisonParam: selected } = this.state
    const filterViews = comparisonByData.map((item) => {
      return this.renderItem(item, selectedComparisonParam, selected, this.setSelectedComparisonBy.bind(this))
    })
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Comparison By</Text>
        </View>
        {filterViews}
      </View>
    )
  }

  renderComaprisonOf() {
    const { comparisonOfData, selectedCategory } = this.props
    const { styles, selectedCategory: selected } = this.state
    const filterViews = comparisonOfData.map((item) => {
      return this.renderItem(item, selectedCategory, selected, this.setSelectedCategory.bind(this))
    })
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Comparison Of</Text>
        </View>
        {filterViews}
      </View>
    )
  }

  renderFooterView() {
    const { styles } = this.state
    return <View style={styles.footerView}>
      <Button
        style={styles.singleBtnStyle}
        onPress={this.handleApplyFilter.bind(this)}
      >
        <Text style={styles.buttonText}>FILTER APPLY</Text>
      </Button>
    </View>
  }

  render() {
    const { onClose, dismiss} = this.props
    const { styles, isLoading, isFilterAppliedState} = this.state
    const heightValues = this.getHeightValue()
    return (
      <View
        style={[
          styles.container,
          { paddingTop: Platform.OS === 'ios' ? heightValues : 0, paddingBottom: 60, position: 'relative' }
        ]}
      >
       <ImageBackground
            style={{ width: widthPercentage(100), padding: 10, paddingBottom: 120, position: 'relative', flexDirection: 'row' }}
            source={icons.HEADER_CUSTOM_ICON}
          >
            <TouchableOpacity onPress={() => this.props.dismiss()}>
              <Image
                source={icons.CLOSEWHITE_ICON}
                style={[
                  styles.performanceIcon,
                  { marginRight: 10, marginTop: 6 },
                  {
                    height: 22,
                    width: 22
                  }
                ]}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{ position: 'absolute', top: 10, right: 10, zIndex: 999 }}
              onPress={() => this.handleResetFilter()}
            >
              <Text style={{ color: colors.White, fontSize: 18 }}>
                Reset Filter
            </Text>
            </TouchableOpacity>
            <Text
              style={{ fontSize: 24, color: colors.White }}>
              Filter
          </Text>
          </ImageBackground>
          {
            isLoading ?
              <ActivityIndicator size='large' /> :
              <ScrollView style={[styles.bottomView, {marginTop: -80}]}>
                <View style={[styles.parallaxView, { marginTop: 10 }]}>
                  {this.renderComaprisonOf()}
                </View>
                <View style={[styles.parallaxView, { marginTop: 10 }]}>
                  {this.renderComaprisonBy()}
                </View>
              </ScrollView>
          }
        {this.renderFooterView()}
      </View>
    )
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.White
  },
  headerView: {
    position: 'absolute',
    flexDirection: 'row',
    width: widthPercentage(100),
    padding: 10
    // paddingBottom: 100
  },
  headerStrip: {
    position: 'absolute',
    flexDirection: 'row',
    width: widthPercentage(100),
    padding: 10,
    paddingBottom: 100,
    zIndex: 5
  },
  parallaxView: {
    borderWidth: 1,
    borderRadius: 6,
    borderColor: colors.White,
    borderBottomWidth: 1,
    shadowColor: colors.ShadowColor,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    shadowRadius: 4,
    marginTop: 125,
    margin: 10,
    padding: 20,
    backgroundColor: colors.White
  },
  bottomView: {
    flex: 1,
    marginTop: 10
  },
  pageNameStyle: {
    fontSize: 24,
    color: colors.White,
    // lineHeight: 40
    width: widthPercentage(100) - 120
  },
  resultHeader: {
    color: '#323232',
    fontSize: 14,
    lineHeight: 20
  },
  resultValue: {
    color: '#000000',
    fontSize: 26,
    lineHeight: 30
  },
  backIcon: {
    width: 30,
    height: 30
  },
  profileIcon: {
    width: 50,
    height: 50
  },
  submitButtonView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  profileView: {
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center'
  },
  subjectText: {
    color: colors.White,
    fontSize: 20,
    padding: 10
  },
  subjectMainView: {
    padding: 10,
    width: widthPercentage(100),
    flex: 1
  },
  cardHeaderText: {
    flexDirection: 'row',
    paddingBottom: 20,
    justifyContent: 'space-between'
  },
  comparisonTextView: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  profileTypeText: {
    color: colors.Black,
    fontSize: 14
  },
  comparisonHeaderView: {
    flexDirection: 'row',
    paddingBottom: 10,
    justifyContent: 'space-between'
  },
  comparisonMaincontentView: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between'
  },
  circleTextView: {
    width: 40,
    height: 40,
    // borderRadius: 30,
    // borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  textValueStyle: {
    color: colors.Black,
    fontWeight: 'bold',
    fontSize: 20
  },
  circleText: {
    color: colors.Black,
    fontWeight: 'bold'
  },
  rowView: {
    flexDirection: 'row',
    height: verticalScale(48),
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  filterItemTextStyle: {
    paddingLeft: 15,
    color: '#666666',
    fontSize: 16
  },
  footerView: {
    marginTop: 10,
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 9
  },
  singleBtnStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.PrimaryBlue
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16
  }
})
