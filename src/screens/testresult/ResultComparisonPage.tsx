import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Platform,
  ScrollView,
  Dimensions,
  BackHandler,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Animated,
  ActivityIndicator
} from 'react-native'
import {
  heightPercentage,
  widthPercentage,
  isPortrait,
  isTablet,
  icons,
  BIGGER_PHONE_CATEGORIZE,
  getBiggerCategorization,
  isIphone,
  IPHONE_CATEGORIZE,
  getIPhoneCategorization
} from '../../common'
import { colors } from '../../common-library/config'
import { dimens } from '../../config'
import { TestAttemptStore, FILTER_KEYS } from '../../store'
import { navigateSimple, goBack } from '../../services'
import { ComparisonFilterPage } from './index'
import { displayData, calculateCircleHeight } from '../../utils'

interface Props {
  navigation?: any
  testAttemptStore?: TestAttemptStore
}

interface State {
  styles?: any
  selectedCategory?: any
  selectedComparisonParam?: any
  showFilter?: false
  isLoading?: boolean
  isFilterApplied?: boolean
}

const CIRCLE_ITEM = {
  YELLOW: { backgroundcolor: '#F8BA1C', color: 'black' },
  BLUE: { backgroundcolor: '#4A90E2', color: 'white' },
  GREEN: { backgroundcolor: '#7ED321', color: 'black' }
}
const TIME_ITEM = {
  YELLOW: { url: icons.COMPARE_YOU_TIMEICON, color: '#F8BA1C' },
  BLUE: { url: icons.COMPARE_AVERAGE_TIMEICON, color: '#4A90E2' },
  GREEN: { url: icons.COMPARE_TOPPER_TIMEICON, color: '#7ED321' }
}
@inject('testAttemptStore')
@observer
export class ResultComparisonPage extends Component<Props, State> {
  animatedValue = new Animated.Value(1)
  headerFontSize = new Animated.Value(24)
  _onBackPressed
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      isLoading: false,
      showFilter: false,
      isFilterApplied: false
    }
    this._onBackPressed = this.onBackPressed.bind(this)
  }
  componentDidMount() {
    Dimensions.addEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
  }
  componentWillMount() {
    // TODO: Gaurav: Remove this when getting rid of mocking
    const { testAttemptStore } = this.props
    testAttemptStore.setSelectedCategory(FILTER_KEYS.TIME_TAKEN)
    testAttemptStore.getTestComparison(testAttemptStore.testId, testAttemptStore.courseId, testAttemptStore.attemptId)
    this.animatedValue.addListener(
      ({ value }) => {
        console.log('VALUE: ', value)
      }
    )
  }
  componentWillUnmount() {
    Dimensions.removeEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
  }

  onBackPressed() {
    const { navigation } = this.props
    return goBack(navigation)
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  getSelectedCatagory() {
    const { selectedCategory } = this.state
    const { testAttemptStore } = this.props
    if (selectedCategory) {
      return selectedCategory
    }
    if (testAttemptStore.comparisonData && testAttemptStore.comparisonData.categories) {
      return testAttemptStore.comparisonData.categories[0]
    }
  }
  getSelectedComparisonParam() {
    const { selectedComparisonParam } = this.state
    const { testAttemptStore } = this.props
    if (selectedComparisonParam) {
      return selectedComparisonParam
    }
    if (testAttemptStore.comparisonData && testAttemptStore.comparisonData.compareBy) {
      return testAttemptStore.comparisonData.compareBy[0]
    }
  }
  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }

  setSelectedCategory(category, comapreBy) {
    const that = this
    that.setState({
      selectedCategory: category,
      isLoading: true
    }, () => setTimeout(() => {
      that.setState({
        isLoading: false
      })
    }, 1500))
  }

  renderCategories(categories, selectedCategory, selectedComparisonParam) {
    const { styles } = this.state
    return categories.map(category => (
      <TouchableOpacity
        style={{ flexDirection: 'row' }}
        key={category.key}
        onPress={() => this.setSelectedCategory(category, selectedComparisonParam)}
      >
        <Text style={[styles.subjectText, {
          fontWeight: selectedCategory.key === category.key ? '900' : '300'
        }]}>{(category.value).toUpperCase()}</Text>
      </TouchableOpacity>
    ))
  }

  renderCategoryScrollView(categories, selectedCategory, selectedComparisonParam) {
    const { styles } = this.state
    return (
      <View style={styles.subjectMainView}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.submitButtonView}>
            {this.renderCategories(categories, selectedCategory, selectedComparisonParam)}
          </View>
        </ScrollView>
      </View>
    )
  }

  renderProfileItem() {
    const { styles } = this.state
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
        <View style={styles.profileView}>
          <Image
            source={icons.PROFILE_ICON}
            style={[styles.profileIcon, { borderColor: colors.Yellow, marginBottom: 5, borderWidth: 2, borderRadius: 30 }]}
          />
          <Text style={styles.profileTypeText}>You</Text>
        </View>
        <View style={styles.profileView}>
          <Image
            source={icons.COMPARE_TOPPER_ICON}
            style={[styles.profileIcon, { marginBottom: 5, borderRadius: 30 }]}
          />
          <Text style={styles.profileTypeText}>Topper</Text>
        </View>
        <View style={styles.profileView}>
          <Image
            source={icons.COMPARE_AVERAGE_ICON}
            style={[styles.profileIcon, { marginBottom: 5, borderRadius: 30 }]}
          />
          <Text style={styles.profileTypeText}>Average</Text>
        </View>
      </View>
    )
  }

  renderProfileView() {
    const { styles } = this.state
    return (
      <View
        style={{
          padding: 10,
          flex: 1,
          height: isTablet() ? 90 : 70,
          paddingLeft: isTablet() ? 60 : 40,
          paddingRight: isTablet() ? 60 : 40
        }}
      >
        {this.renderProfileItem()}
      </View>
    )
  }

  renderDataPoint(data, maxData, transformer, stuCategory) {
    const { styles } = this.state
    let denominatorView = null
    const { testAttemptStore } = this.props
    const { selectedCategory } = testAttemptStore
    // {selectedCategory === FILTER_KEYS.MARKS_SCORED && this.renderCircleMainView()}
    // {selectedCategory === FILTER_KEYS.TIME_TAKEN && this.renderTimeMainView()}
    if (maxData !== null && maxData !== undefined) {
      denominatorView = <View style={{ justifyContent: 'center' }}>
        <Text style={styles.percentageText}>/{maxData}</Text>
      </View>
    }
    const dataValue = transformer ? transformer(data) : data
    switch (selectedCategory) {
      case FILTER_KEYS.TIME_TAKEN:
        return <View style={styles.dataView}>
          {this.renderTimeView(displayData(dataValue), stuCategory)}
          {denominatorView}
        </View>
        break
      case FILTER_KEYS.ACCURACY:
        return <View style={styles.dataView}>
          {this.renderCircleView(displayData(dataValue), stuCategory)}
          <Text style={styles.percentageText}>%</Text>
        </View>
        break
      case FILTER_KEYS.SPEED:
        return <View style={styles.dataView}>
          {this.renderTimeView(displayData(dataValue), stuCategory)}
          {denominatorView}
        </View>
        break
      case FILTER_KEYS.MARKS_SCORED:
        return <View style={styles.dataView}>
          {this.renderCircleView(displayData(dataValue), stuCategory, maxData)}
          {denominatorView}
        </View>
        break
      case FILTER_KEYS.ATTEMPT_PERC:
        return <View style={styles.dataView}>
          {this.renderCircleView(displayData(dataValue), stuCategory)}
          <View style={{ justifyContent: 'center' }}>
            <Text style={styles.percentageText}>%</Text>
          </View>
        </View>
        break
      default:
        return <View style={styles.dataView}>
          {this.renderTimeView(displayData(dataValue), stuCategory)}
          {denominatorView}
        </View>
        break
    }
    return null
  }

  circleColor(val) {
    if (val === 'you') {
      return CIRCLE_ITEM.YELLOW
    } else if (val === 'topper') {
      return CIRCLE_ITEM.BLUE
    } else {
      return CIRCLE_ITEM.GREEN
    }
  }

  renderCircleView(data, stucategory, maxData = null) {
    const { styles } = this.state
    // Change in circleheight will change filed color in circle
    const circleViewWH = isTablet() ? 50 : 50
    let circleHeight = maxData && maxData <= Math.floor(data) ? circleViewWH : calculateCircleHeight(data, circleViewWH)
    if(isNaN(circleHeight)) {
      circleHeight = circleViewWH;
    }
    // console.warn('circleHeight ' , circleHeight)
    const type = this.circleColor(stucategory)
    return (<ImageBackground source={icons.CIRCLEIMAGE_ICON}
      style={[styles.comparisonTextView, { padding: 2, borderRadius: 35, position: 'relative' }]}>
       <View style={[styles.circleTextView, { width: circleViewWH, height: circleViewWH }, { position: 'relative' }]}>
          <View style={[styles.filledCircle, { backgroundColor: type.backgroundcolor, height: circleHeight, width: circleViewWH }]}>
          </View>
          <View style={[styles.textCircle, { width: circleViewWH, height: circleViewWH }]}>
            <Text style={styles.circleText}>{data}</Text>
          </View>
          </View>
      </ImageBackground>
    )
  }

  timerColor(val) {
    if (val === 'you') {
      return TIME_ITEM.YELLOW
    } else if (val === 'topper') {
      return TIME_ITEM.BLUE
    } else {
      return TIME_ITEM.GREEN
    }
  }

  getfontValue() {
    let fontValue = 12
    if (isIphone()) {
      switch (getIPhoneCategorization()) {
        case IPHONE_CATEGORIZE.IPHONE_4_5_SERIES:
          fontValue = 11
          break
        default:
          break
      }
    }
    return fontValue
  }

  renderTimeView(data, stuCategory) {
    const fontValuesmall = this.getfontValue()
    const { styles } = this.state
    const type = this.timerColor(stuCategory)
    return (<View style={styles.comparisonTextView}>
      <View
        style={{ justifyContent: 'center', marginRight: 5 }}>
        <Image
          source={type.url}
          style={styles.timeIcon}
        />
      </View>
      <View
        style={{ justifyContent: 'center' }}>
        <Text style={[styles.textValueStyle, {
          color: type.color,
          fontSize: isTablet() ? 18 : fontValuesmall
        }]}>{data}</Text>
      </View>
    </View>
    )
  }

  renderComaprisonCardView({ item }) {
    const { testAttemptStore } = this.props
    const { selectedCategory } = testAttemptStore
    const { styles } = this.state
    const image = item.comparison(item.you, item.average) ?
      <Image
        source={icons.RESULT_THUMBUP_ICON}
        style={[styles.backIcon, {}]}
      /> :
      <Image
        source={icons.RESULT_THUMBDOWN_ICON}
        style={[styles.backIcon, {}]}
      />
    const youMax = item.maxDataKey ? item.maxDataKey : null
    const topperMax = item.maxDataKey ? item.maxDataKey : null
    const averageMax = item.maxDataKey ? item.maxDataKey : null
    return (
      <View
        style={[styles.parallaxView, { flex: 1, marginTop: 10, paddingTop: 0, padding: 10 }]}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: isTablet() ? 20 : 16, color: '#333333' }}>{item.name}</Text>
        </View>
        <View style={styles.comparisonMaincontentView}>
          <View>
            {this.renderDataPoint(item.you, youMax, item.transformer, 'you')}
          </View>
          <View>
            {this.renderDataPoint(item.topper, topperMax, item.transformer, 'topper')}</View>
          <View>
            {this.renderDataPoint(item.average, averageMax, item.transformer, 'average')}</View>
        </View>
      </View>
    )
  }

  layout = (data, index) => {
    return { length: 80, offset: 80 * index, index }
  }

  _animateScroll(e) {
    // let threshold = width / 5
    let y = e.nativeEvent.contentOffset.y
    // Animated.timing(this.state.colors[index], {
    //   toValue: target,
    //   duration: 180
    // }).start()
    // let target = null

    // x = x * -1

    // if (x > -50 && this._target !== GREY) {
    //   target = GREY
    // } else if (x < -50 && x > -threshold && this._target !== GREEN) {
    //   target = GREEN
    // } else if (x < -threshold && this._target !== RED) {
    //   target = RED
    // }

    // if (target !== null) {
    //   this._target = target
    //   this._targetIndex = index

    if (y > 0) {
      Animated.timing(this.animatedValue, {
        toValue: 0,
        duration: 5,
        useNativeDriver: true
      }).start()
    } else {
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 5,
        useNativeDriver: true
      }).start()
    }
    // }

  }

  setFilters(selectedCategory, selectedComparisonParam, isAppliedFilter) {
    const that = this
    that.setState({
      selectedCategory,
      selectedComparisonParam,
      isLoading: true,
      isFilterApplied: isAppliedFilter,
      showFilter: false
    }, () => setTimeout(() => {
      that.setState({
        isLoading: false
      })
    }, 1500))
  }

  toggleFilter(selectedCategory, selectedComparisonParam, showFilter) {
    const that = this
    that.setState({
      selectedCategory,
      selectedComparisonParam,
      isLoading: true,
      showFilter: showFilter,
      isFilterApplied: false
    })
  }
  openFilter(showfilter) {
    this.setState({
      showFilter: showfilter
    })
  }
  closeFilter() {
    this.setState({
      showFilter: false,
      isLoading: false
    })
  }

  renderFilterAppliedView(listData) {
    const { styles } = this.state
    if (listData && listData.length > 0) {
      return (
        <View>
          <Animated.Image
            source={icons.RESULT_FILTER_ICON}
            style={[
              styles.backIcon,
              { marginRight: 10, marginTop: 10 },
              {
                height: 30,
                width: 30
              }
            ]}
          />
          {this.state.isFilterApplied ? 
            <View
            style={{
              width: 10, position: 'absolute', top: 10, right: 10, zIndex: 999,
              height: 10, backgroundColor: colors.Red, borderRadius: 20
            }} /> : null
          }
        </View>
      );
    }
    return null
  }
  renderFilter(categories, compareByData, selectedCategory, selectedComparisonParam) {
    const { showFilter, isFilterApplied } = this.state
    if (!showFilter) {
      return null
    }
    return <ComparisonFilterPage
      comparisonByData={compareByData}
      comparisonOfData={categories}
      selectedCategory={selectedCategory}
      selectedComparisonParam={selectedComparisonParam}
      onChange={this.setFilters.bind(this)}
      onClose={() => this.toggleFilter.bind(this)}
      dismiss={this.closeFilter.bind(this)}
      isFilterApplied={isFilterApplied}
    />
  }

  render() {
    const { styles, isLoading, showFilter } = this.state
    const { testAttemptStore } = this.props
    const heightValues = this.getHeightValue()
    const imageBackgroundHeight = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [160, 50],
      extrapolate: 'clamp'
    })
    const imageBackgroundZIndex = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [0, 5],
      extrapolate: 'clamp'
    })
    // const imageBackgroundHeight = this.animatedValue.interpolate({
    //   inputRange: [0, 40],
    //   outputRange: [200, 50],
    //   extrapolate: 'clamp'
    // })
    // const subjectLeftPosition = this.animatedValue.interpolate({
    //   inputRange: [0, 40],
    //   outputRange: [0, widthPercentage(100) + 10],
    //   extrapolate: 'clamp'
    // })
    // const subjectOpacity = this.animatedValue.interpolate({
    //   inputRange: [0, 30],
    //   outputRange: [1, 0],
    //   extrapolate: 'clamp'
    // })
    // const subjectTop = this.animatedValue.interpolate({
    //   inputRange: [0, 30],
    //   outputRange: [60, 50],
    //   extrapolate: 'clamp'
    // })
    // const imageBackgroundZIndex = this.animatedValue.interpolate({
    //   inputRange: [0, 40],
    //   outputRange: [0, 5],
    //   extrapolate: 'clamp'
    // })
    // const headerFontSize = this.animatedValue.interpolate({
    //   inputRange: [0, 40],
    //   outputRange: [24, 14],
    //   extrapolate: 'clamp'
    // })
    // const backIconSize = this.animatedValue.interpolate({
    //   inputRange: [0, 40],
    //   outputRange: [30, 18],
    //   extrapolate: 'clamp'
    // })
    const comparisonData = testAttemptStore.comparisonData
    const listData = []
    let categories = []
    let compareByData = []
    const selectedCategory = this.getSelectedCatagory()
    const selectedComparisonParam = this.getSelectedComparisonParam()
    if (comparisonData && comparisonData.data) {
      categories = comparisonData.categories
      compareByData = comparisonData.compareBy
      comparisonData.data.you[selectedCategory.key].forEach((item, index) => {
        listData.push({
          name: item.name,
          you: item[selectedComparisonParam.key],
          topper: comparisonData.data.topper[selectedCategory.key][index][selectedComparisonParam.key],
          average: comparisonData.data.average[selectedCategory.key][index][selectedComparisonParam.key],
          transformer: selectedComparisonParam.transformer,
          maxDataKey: selectedComparisonParam.maxDataKey ?
            item[selectedComparisonParam.maxDataKey] :
            null,
          comparison: selectedComparisonParam.comparison
        })
      })
    }
    const comparisonView = <View
      style={[
        styles.container,
        { paddingTop: Platform.OS === 'ios' ? heightValues : 0 }
      ]}
    >
      <Animated.Image
        style={[
          styles.headerView,
          {
            height: imageBackgroundHeight,
            zIndex: imageBackgroundZIndex
          }
        ]}
        source={icons.COMPARISON_HEADER_ICON}
      />
      <View style={styles.headerStrip}>
        <TouchableOpacity onPress={() => this.onBackPressed()}>
          <Animated.Image
            source={icons.BACK_WHITE_ICON}
            style={[
              styles.performanceIcon,
              { marginRight: 5, marginTop: 5 },
              {
                height: 30,
                width: 30
              }
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{ position: 'absolute', top: 0, right: 0, zIndex: 999 }}
          onPress={() => this.openFilter(true)}
        >
          {this.renderFilterAppliedView(listData)}
        </TouchableOpacity>
        <Animated.Text
          style={[
            styles.headerTestName,
            {
              fontSize: 24 // headerFontSize
            }
          ]}
        >
          Comparison
          </Animated.Text>
        <Animated.View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: 40, // subjectTop,
            left: 0, // subjectLeftPosition,
            opacity: this.animatedValue // subjectOpacity
          }}
        >
          {this.renderCategoryScrollView(categories, selectedCategory, selectedComparisonParam)}
        </Animated.View>
      </View>
      <View
        style={[
          styles.parallaxView,
          { padding: 0, flexDirection: 'column' }
        ]}
      >
        <View style={{ flexDirection: 'row' }}>
          {this.renderProfileView()}
        </View>
      </View>
      {listData.length === 0 ?
        <View style={{ flex: 1, height: heightPercentage(100) - 200, padding: 20, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{
            fontSize: 18, lineHeight: 22,
            textAlign: 'center'
          }}>Excited? Your result is being calculated, please refresh after sometime.</Text>
        </View> :
        <Animated.ScrollView
          scrollEventThrottle={16}
          // onScroll={Animated.event([
          // { nativeEvent: { contentOffset: { y: this.animatedValue } } }
          // { useNativeDriver: true, isInteraction: true }
          // ])}
          // onScroll={this._animateScroll.bind(this)}
          showsVerticalScrollIndicator={false}
          style={{ zIndex: 2 }}
        >
          {
            isLoading ?
              <ActivityIndicator size='large' /> :
              <View style={styles.bottomView}>
                <FlatList
                  data={listData}
                  renderItem={item => this.renderComaprisonCardView(item)}
                  showsVerticalScrollIndicator={false}
                  key='flatlist'
                  keyExtractor={(item, index) => item.name}
                  scrollEventThrottle={1}
                  getItemLayout={this.layout}
                  initialNumToRender={5}
                />
              </View>
          }
        </Animated.ScrollView>
      }
    </View>
    const finalView = showFilter ?
      this.renderFilter(categories, compareByData, selectedCategory, selectedComparisonParam) :
      comparisonView
    return finalView
  }
}

const getStyle = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: colors.White
    },
    headerView: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10
      // paddingBottom: 100
    },
    headerStrip: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10,
      paddingBottom: 100,
      zIndex: 5
    },
    parallaxView: {
      borderRadius: 6,
      borderTopWidth: 1,
      borderTopColor: colors.GrayUnderLine,
      shadowColor: colors.GrayUnderLine,
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowRadius: 3.84,
      elevation: 5,
      marginTop: 95,
      margin: 10,
      padding: 20,
      backgroundColor: colors.White
    },
    bottomView: {
      flex: 1,
      marginTop: 10,
      flexDirection: 'column'
    },
    headerTestName: {
      fontSize: 24,
      color: colors.White,
      // lineHeight: 40
      width: widthPercentage(100) - 120
    },
    resultHeader: {
      color: '#323232',
      fontSize: 14,
      lineHeight: 20
    },
    resultValue: {
      color: '#000000',
      fontSize: 26,
      lineHeight: 30
    },
    backIcon: {
      width: 30,
      height: 30
    },
    profileIcon: {
      width: isTablet() ? 48 : 36,
      height: isTablet() ? 48 : 36
    },
    submitButtonView: {
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    profileView: {
      flexDirection: 'column',
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center'
    },
    subjectText: {
      color: colors.White,
      fontSize: isTablet() ? 20 : 16,
      paddingHorizontal: 10,
      paddingVertical: 10
    },
    subjectMainView: {
      padding: 10,
      paddingLeft: 0,
      width: widthPercentage(100),
      flex: 1
    },
    cardHeaderText: {
      flexDirection: 'row',
      paddingBottom: 20,
      justifyContent: 'space-between'
    },
    dataTextView: {
      flexDirection: 'row',
      justifyContent: 'center'
    },
    profileTypeText: {
      color: '#999999',
      fontSize: isTablet() ? 16 : 12
    },
    comparisonHeaderView: {
      flexDirection: 'row',
      paddingVertical: 10,
      paddingHorizontal: 5,
      justifyContent: 'space-between'
    },
    comparisonMaincontentView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      flex: 1
    },
    dataView: {
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      flexDirection: 'row'
    },
    textValueStyle: {
      color: '#333333'
    },
    dataText: {
      color: colors.SubHeaderColor,
      textAlign: 'center',
      marginLeft: -2
    },
    borderView: {
      margin: 5,
      width: widthPercentage(33.3) - 20,
      paddingBottom: 5,
      justifyContent: 'center',
      borderBottomWidth: 8
    },
    comparisonTextView: {
      flexDirection: 'row',
      justifyContent: 'center',
      padding: 5
    },
    circleTextView: {
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      overflow: 'hidden'
    },
    circleText: {
      color: colors.SubHeaderColor,
      fontSize: isTablet() ? 15 : 12
    },
    textCircle: {
      position: 'absolute',
      left: 0,
      bottom: 0,
      top: 0,
      right: 0,
      zIndex: 10,
      borderRadius: 30,
      alignItems: 'center',
      justifyContent: 'center'
    },
    filledCircle: {
      position: 'absolute',
      left: 0,
      bottom: 0,
      zIndex: 9
    },
    timeIcon: {
      width: isTablet() ? 30 : 23,
      height: isTablet() ? 30 : 23
    },
    percentageText: {
      fontSize: isTablet() ? 18 : 14,
      color: '#666666',
      textAlign: 'center',
      marginLeft: 5
    }
  })
