import React, {Component} from 'react'
import {inject, observer} from 'mobx-react'
import {
	ActivityIndicator,
	AppState,
	BackHandler,
	Dimensions,
	Image,
	ImageBackground,
	NetInfo,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import {
	BIGGER_PHONE_CATEGORIZE,
	getBiggerCategorization,
	heightPercentage,
	icons, isPortrait,
	isTablet,
	offlineMsg,
	TAB_BAR_PAGE, verticalScale,
	widthPercentage
} from '../../common'
import {colors} from '../../common-library/config'
import {dimens} from '../../config'
import {GenericTabBarStore, NetworkDataStore, TestAttemptStore, ProfileDataStore} from '../../store'
import {getPreviousRouteName, goBack, navigateSimple, resetRouterSimple, showSnackbar} from '../../services'
import commonStores from '../../common-library/store'

interface Props {
	navigation?: any
	testAttemptStore?: TestAttemptStore,
	genericTabBarStore?: GenericTabBarStore
	networkDataStore?: NetworkDataStore,
	profileDataStore?: ProfileDataStore
}

interface State {
	styles?: any
	loading: boolean,
	isAttemptClick: boolean
	isTearDownRequested?: boolean
}

@inject('testAttemptStore', 'genericTabBarStore', 'networkDataStore', 'profileDataStore')
@observer
export class ResultLandingScreenPage extends Component<Props, State> {
	_onBackPressed: any;
	refreshApi: any;
	constructor(props: Props, state: State) {
		super(props, state);
		this.state = {
			styles: getStyle(),
			loading: true,
			isAttemptClick: false,
			isTearDownRequested: false
		};
		this._onBackPressed = this.onBackPressed.bind(this);
		this.refreshApi = this.hydratePage.bind(this)
	}

	async componentDidMount() {
		NetInfo.isConnected.addEventListener('connectionChange', this.refreshApi)
		const { testAttemptStore } = this.props;
		const prevRouteName = getPreviousRouteName();
		AppState.addEventListener('change', state => {
			if (state === 'active') {
				commonStores.networkDataStore.establishNetListener()
			}
			if (state === 'background') {
				commonStores.networkDataStore.removeNetListener()
			}
		});
		if (prevRouteName !== 'TestEngineLandingPage') {
			await testAttemptStore.fetchTestResult(testAttemptStore.testId, testAttemptStore.attemptId)
		}
		await this.props.testAttemptStore.offlineSync();
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
		BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
		BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
	}

	onBackPressed() {
		// console.warn('back pressed from result landing page')
		this.setState({
			isTearDownRequested: true
		});
		const { navigation, genericTabBarStore } = this.props;
		const prevRouteName = getPreviousRouteName();
		// console.warn('prevRouteName', prevRouteName)
		if (prevRouteName === 'TestEngineLandingPage' || prevRouteName === 'TestEnginePage') {
			setTimeout(() => {
				resetRouterSimple(navigation, 0, 'HomePage');
				genericTabBarStore.goToPage(TAB_BAR_PAGE.TEST)
			}, 600);
			return false
		} else {
			return goBack(navigation)
		}
	}

	async hydratePage() {
		const { testAttemptStore } = this.props;
		await testAttemptStore.offlineSync()
	}

	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}

	getHeightValue() {
		let heightValue = 20;
		switch (getBiggerCategorization()) {
			case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
				heightValue = 40;
				break;
			default:
				break
		}
		return heightValue
	}

	onPerformancePress = () => {
		const { testAttemptStore } = this.props;
		const performance = testAttemptStore.getTestPerformance();
		if (performance) {
			navigateSimple(this.props.navigation, 'ResultPerformancePage')
		} else {
			showSnackbar('This result is not available in offline mode.')
		}
	};

	onComparisonPress = () => {
		if (this.props.networkDataStore.isNetworkConnected) {
			navigateSimple(this.props.navigation, 'ResultComparisonPage')
		} else {
			showSnackbar(offlineMsg)
		}
	};

	onSwotPress = () => {
		if (this.props.networkDataStore.isNetworkConnected) {
			navigateSimple(this.props.navigation, 'ResultSwotPage')
		} else {
			showSnackbar(offlineMsg)
		}
	};

	_attemptTest() {
		this.setState((prevState) => ({
			isAttemptClick: !prevState.isAttemptClick
		}))
	}

	renderFullBoxTablet(performance: any) {
		const { marksScored, maxMarks } = performance;
		const { styles } = this.state
		return <View style={styles.parallaxView}>
			<View style={{ flexDirection: 'row', padding: 30, paddingBottom: 30, paddingTop: 35}}>
				<View style={{ justifyContent: 'center' }}>
					<Image
						source={icons.SAINT_ICON}
						style={{ width: 158, height: 180,
							marginRight: 40}}
					/>
				</View>
				<View style={{width: widthPercentage(100) - 320, flexDirection: 'column'}}>
					<View style={{ justifyContent: 'center' }}>
						<Text style={{ color: '#323C47', fontSize: 28,
							lineHeight: 35}}>
							Let's explore your performance</Text>
					</View>
					<View style={{ justifyContent: 'center', paddingTop: 5 }}>
						<Text style={{ color: '#666666', fontSize: 20,
							lineHeight: 26}}>
							It's Result time! Refer to crucial test performance metrics below that point out your strengths, weaknesses and improvement
							areas. Review your mistakes and attempt better next time.
						</Text>
					</View>
					<View style={{ flexDirection: 'row', paddingTop: 20 }}>
						<View style={{ flexDirection: 'column', flex: 0.5, justifyContent: 'center' }}>
							<Text style={styles.resultHeader}>Score</Text>
							<Text style={styles.resultValue}>{marksScored}/<Text style={{ fontSize: 28 }}>{maxMarks}</Text></Text>
						</View>
						{/* <View style={{ flexDirection: 'column', flex: 0.5, justifyContent: 'center' }}>
        <Text style={styles.resultHeader}>Percentile</Text>
        <Text style={styles.resultValue}>14/360</Text>
      </View> */}
					</View>
				</View>

			</View>
		</View>
	}

	renderFullBoxMobile(performance: any) {
		const { marksScored, maxMarks } = performance;
		const { styles } = this.state;
		const { selectedStudent } = this.props.profileDataStore;

		return <View style={styles.parallaxView}>
			<View style={{ flexDirection: 'row', padding: 10, paddingTop: 0, paddingBottom: 20, paddingLeft: 0 }}>
				<View style={{ }}>
					<Image
						source={icons.SAINT_ICON}
						style={{ width: 79, height: 90, marginRight: 10 }}
					/>
				</View>
				<View style={{ justifyContent: 'center', flex: 1 }}>
					<Text style={{ color: '#323C47', fontSize: 24, lineHeight: 30 }}>
						{`${selectedStudent && selectedStudent.name ? selectedStudent.name + '\'s' : ''}`} performance is awesome
					</Text>
				</View>
			</View>
			<View style={{ flexDirection: 'row', flex: 1 }}>
				<Text style={{ color: '#666666', fontSize: isTablet() ? 20 : 16,
					lineHeight: isTablet() ? 22 : 18 }}>
					It's Result time! Refer to crucial test performance metrics below that point out your strengths, weaknesses and improvement
					areas. Review your mistakes and attempt better next time.
				</Text>
			</View>
			<View style={{ flexDirection: 'row', paddingTop: 10, width: widthPercentage(100) - 32 }}>
				<View style={{ flexDirection: 'column', flex: 0.8, justifyContent: 'center' }}>
					<Text style={styles.resultHeader}>Score</Text>
					<Text style={styles.resultValue}>{marksScored}/<Text style={{ fontSize: 28 }}>{maxMarks}</Text></Text>
				</View>
			</View>
		</View>
	}

	renderFooterView() {
		const { styles } = this.state
		const { networkDataStore } = this.props
		if (!networkDataStore.isNetworkConnected) {
			return <View style={styles.footerView}>
		<Text style={styles.footerText}>Note: Detailed analysis will be generated when you go online</Text>
		</View>
		} else {
			return null
		}
	}

	renderBackIcon() {
		return (
			<TouchableOpacity onPress={() => this.onBackPressed()}>
				<Image source={icons.BACK_ARROW_WHITE} style={{marginTop: 23, width: 29, height: 24}} />
			</TouchableOpacity>
		)
	}

	_renderPerformanceBlock() {
		const { styles } = this.state;
		const { selectedStudent } = this.props.profileDataStore;

		return (<TouchableOpacity style={[styles.parallaxView, { height: 100,
		marginTop: 10, margin: 0,
		width: (widthPercentage(95) - 10)/2, flexDirection: 'row'
	}]} onPress={() => this.onPerformancePress()}>
	<View style={{ justifyContent: 'center' }}>
	<Image
		source={icons.RESULT_PERFORMANCE_ICON}
		style={styles.performanceIcon}
		/>
	</View>
	<View style={{ justifyContent: 'center' }}>
	<Text style={[styles.cardText,
		{ paddingLeft: 10,
			width: widthPercentage(50) - 70 }]}>{`${selectedStudent && selectedStudent.name ? selectedStudent.name + '\'s' : ''}`}</Text>
	<Text style={[styles.cardText, { paddingLeft: 10, width: widthPercentage(50) - 70 }]}>Performance</Text>
		</View>
		</TouchableOpacity>)
	}

	_renderSwotButton() {
		const { testAttemptStore, networkDataStore } = this.props;
		const { styles } = this.state;
		let isDisable;
		if (networkDataStore.isNetworkConnected) {
			isDisable = (testAttemptStore.testType === 'conceptTest' || testAttemptStore.testType === 'concept');
		} else {
			isDisable = true
		}

		return (
			<View style={[styles.parallaxView, {height: 100,
		marginTop: 10, margin: 0,
		width: (widthPercentage(95) - 10)/2, flexDirection: 'row'
	}, isDisable ? styles.disabled : styles.enabled ]}>
	<TouchableOpacity style={{ flexDirection: 'row' }}
		disabled={isDisable}
		onPress={() => this.onSwotPress()}>
	<View style={{ justifyContent: 'center' }}>
	<Image
		source={icons.STRENTH_WEAKNESS_ICON}
		style={[styles.performanceIcon, { width: 35, height: 35 }]}
		/>
	</View>
	<View style={{ justifyContent: 'center' }}>
	<Text style={[styles.cardText, { paddingLeft: 10, width: widthPercentage(50) - 70 }]}>Strength and Weakness</Text>
		</View>
		</TouchableOpacity>
		</View>
	)
	}

	_renderComparisionBlock() {
		const { styles } = this.state;
		const { networkDataStore } = this.props;
		const isDisable = !networkDataStore.isNetworkConnected;
		return (
			<View style={[styles.parallaxView, {height: 100,
		marginTop: 10, margin: 0, marginLeft: 10,
		width: (widthPercentage(95) - 10)/2, flexDirection: 'row'
	}, isDisable ? styles.disabled : styles.enabled ]}>
	<TouchableOpacity
		style={{ flexDirection: 'row' }}
		onPress={() => this.onComparisonPress()}>
	<View style={{ justifyContent: 'center', flexWrap: 'wrap' }}>
	<Image
		source={icons.RESULT_COMPARISON_ICON}
		style={[styles.performanceIcon, { width: 37, height: 32.333 }]}
		/>
	</View>
	<View style={{ justifyContent: 'center' }}>
	<Text style={[styles.cardText, { paddingLeft: 10, width: widthPercentage(50) - 72 }]}>Comparison</Text>
		</View>
		</TouchableOpacity>
		</View>
	)
	}

	render() {
		const { styles, isAttemptClick, isTearDownRequested } = this.state;
		const { testAttemptStore, networkDataStore } = this.props;
		const { loading } = this.state;
		const performance = testAttemptStore.getTestPerformance();
		const { testName } = performance;
		const heightValues = this.getHeightValue();
		const isDisable = !networkDataStore.isNetworkConnected;

		return (
			<View style={[styles.container, { paddingTop: Platform.OS === 'ios' ? heightValues : 0 }]}>
				<ImageBackground source={icons.PROFILE_HEADER} resizeMode='cover' style={styles.headerBackground}>
					<View style={styles.headerRow}>
						{this.renderBackIcon()}
						<Text style={styles.headerTestName}>{testName}</Text>
					</View>
				</ImageBackground>
				<ScrollView style={styles.scrollView}>
					{isTablet() ? this.renderFullBoxTablet(performance) : this.renderFullBoxMobile(performance)}
					<View style={styles.bottomView}>
						<View style={{ flexDirection: 'row', width: widthPercentage(100) }}>
							{this._renderPerformanceBlock()}
							{this._renderComparisionBlock()}
						</View>

						<View style={{ flexDirection: 'row', width: widthPercentage(100) }}>
							{this._renderSwotButton()}
						</View>
					</View>
					{this.renderFooterView()}
				</ScrollView>
			</View>
		)
	}
}

const getStyle = () => StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: colors.White
	},
	scrollView: {
		flex: 1,
		position: 'absolute',
		top: verticalScale(100),
		left: 10,
		right: 10,
		bottom: 0,
	},
	disabled: {
		opacity: 0.2
	},
	enabled: {
		opacity: 1
	},
	headerRow: {
		flex: 1,
		flexDirection: 'row',
		marginLeft: widthPercentage(2.5),
		marginBottom: 22
	},
	headerBackground: {
		flex: 1,
		height: widthPercentage(40),
		width: widthPercentage(100)
	},
	headerView: {
		flexDirection: 'row',
		padding: 10,
		paddingBottom: isTablet() ? 200 : 80
	},
	parallaxView: {
		padding: isTablet() ? 20 : 10,
		backgroundColor: colors.White,

		borderWidth: dimens.size1,
		borderRadius: dimens.size4,
		borderColor: colors.BorderColor,
		borderBottomWidth: dimens.size0,

		shadowColor: colors.ShadowColor,
		shadowOffset: { width: dimens.size0, height: dimens.size1 },
		shadowOpacity: 0.2,
		elevation: dimens.size4,
		shadowRadius: dimens.size2,
		width: widthPercentage(95)
	},
	bottomView: {
		flex: 1,
		marginBottom: 10,
		flexDirection: 'column'
	},
	headerTestName: {
		fontSize: verticalScale(24),
		marginTop: verticalScale(10),
		marginLeft: verticalScale(10),
		color: colors.White,
		lineHeight: verticalScale(30),
		width: widthPercentage(100) - 70,
		maxHeight: verticalScale(90)
	},
	resultHeader: {
		color: '#777777',
		fontSize: isTablet() ? 20 : 16,
		lineHeight: 20
	},
	resultValue: {
		color: '#4a4a4a',
		fontSize: 32,
		lineHeight: 40
	},
	performanceIcon: {
		width: 30,
		height: 30
	},
	closeIcon: {
		width: 20,
		height: 20
	},
	loaderView: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	footerHeader: {
		color: colors.Black,
		fontSize: 20
	},
	attempButtonview: {
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		padding: 10
	},
	attemptButtonDisabled: {
		backgroundColor: colors.PrimaryBlue,
		flexDirection: 'row',
		alignSelf: 'center',
		borderWidth: 0,
		padding: 15,
		paddingLeft: 30,
		paddingRight: 30,
		justifyContent: 'center',
		borderRadius: 30
	},
	attemptTextDisabled: {
		color: colors.White,
		fontSize: 17
	},
	attemptButton: {
		backgroundColor: colors.White,
		flexDirection: 'row',
		alignSelf: 'center',
		borderWidth: 1,
		padding: 15,
		paddingLeft: 30,
		paddingRight: 30,
		justifyContent: 'center',
		borderRadius: 30,
		borderColor: colors.GrayUnderLine
	},
	attemptText: {
		color: '#777777',
		fontSize: 17
	},
	attemptDate: {
		color: colors.LightGrey,
		fontSize: 13,
		marginTop: 10
	},
	cardText: {
		color: '#777777',
		fontSize: isTablet() ? 20 : 16,
		lineHeight: isTablet() ? 22 : 18
	},
	footerView: {
		padding: 15,
		flex: 1
	},
	footerText: {
		color: colors.Gray,
		fontSize: 16,
		textAlign: 'left'
	}
});
