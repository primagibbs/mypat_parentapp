import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
  StyleSheet,
  View, processColor, Text, Platform, Dimensions, BackHandler, TouchableOpacity, Animated
}
  from 'react-native'
import {
  widthPercentage, icons, BIGGER_PHONE_CATEGORIZE, getBiggerCategorization, SUBJECT_COLOR
} from '../../common'
import { colors } from '../../common-library/config'
import { TestAttemptStore } from '../../store'
import { goBack } from '../../services'
import { PieChartComponent } from '../../components/generic/ui/PieChartComponent'
import { capitalizeWords } from '../../utils'
import { PerformanceBarChartComponent } from '../../components/others'

interface Props {
  navigation?: any
  testAttemptStore?: TestAttemptStore
}

interface State {
  styles?: any,
  xAxisSubjectArray?: any
}

@inject('testAttemptStore')
@observer
export class ResultPerformancePage extends Component<Props, State> {
  animatedValue = new Animated.Value(0)
  _onBackPressed
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle()
    }
    this._onBackPressed = this.onBackPressed.bind(this)
  }
  componentDidMount() {
    const { testAttemptStore } = this.props
    const subjectList = testAttemptStore.getSubjectList()
    this.setState({
      xAxisSubjectArray: subjectList
    })
    Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
  }
  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
  }

  onBackPressed() {
    const { navigation } = this.props
    return goBack(navigation)
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }
  renderPieChartLegend() {
    const { xAxisSubjectArray } = this.state
    const { testAttemptStore } = this.props
    if (xAxisSubjectArray && testAttemptStore.testType !== 'chap') {
      return xAxisSubjectArray.map((item, index) => this.getSubjectLegend(item, index))
    }
    return null
  }
  getSubjectLegend(item, index) {
    const colorCode = SUBJECT_COLOR.get(capitalizeWords(item)) || '#F2999B'
    return <View
      style={{ flexDirection: 'row', justifyContent: 'center', padding: 10 }}
      key={item + index}
    >
      <View style={{ justifyContent: 'center' }}>
        <View style={{ height: 15, width: 15, backgroundColor: colorCode }}>
        </View>
      </View>
      <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
        <Text style={{ fontSize: 14, color: '#999999' }}>{item}</Text>
      </View>
    </View>
  }

  renderPieChart() {
    const { styles } = this.state
    const { testAttemptStore } = this.props
    const subjectProgress = testAttemptStore.getSectionsStats()
    // console.warn('subjectProgress: ', JSON.stringify(subjectProgress, null, 2))
    // tslint:disable-next-line:max-line-length
    if (subjectProgress && (testAttemptStore.testType !== 'chap' && testAttemptStore.testType !== 'concept' && testAttemptStore.testType !== 'conceptTest')) {
      let yAxisTotalQuestion = []
      let xAxisSubjectArray = []
      let colorCodeArray = []
      for (let subjectData of subjectProgress) {
        yAxisTotalQuestion.push(subjectData.stats.timeTakenPercentage || 0)
        xAxisSubjectArray.push(capitalizeWords(subjectData.name))
      }
      for (let subject of xAxisSubjectArray) {
        colorCodeArray.push(processColor(SUBJECT_COLOR.get(subject) || '#F2999B'))
      }
      const graphDataValue = yAxisTotalQuestion
      // console.warn('graphDataValue: ', JSON.stringify(graphDataValue, null, 2))
      const yAxis = [{
        config: {
          colors: colorCodeArray,
          valueTextSize: 20,
          valueTextColor: processColor(colors.Transparent),
          sliceSpace: 5,
          selectionShift: 13,
          valueFormatter: "#.#'%'",
          valueLineColor: processColor(colors.Transparent),
          valueLinePart1Length: 0.5
        },
        label: '',
        yAxis: graphDataValue
      }]
      const graphData = {
        yAxis: yAxis
      }
      const add = (a, b) => a + b
      const sum = graphDataValue.reduce(add, 0)
      if (sum !== 0) {
        return <View style={{ flexDirection: 'row', padding: 20, width: widthPercentage(100) - 32, height: 300 }}>
          <View style={styles.graphcontainer}>
            <PieChartComponent
              graphDataValues={graphData}
            />
          </View>
        </View>
      }
      return <View style={{ justifyContent: 'center', flex: 1 }}>
        <Text style={{ fontSize: 20, textAlign: 'center', lineHeight: 22 }}>There is no sufficient data to show your time strategy.</Text></View>
    }
    return null
  }
  renderGroupBarChart() {
    const { testAttemptStore } = this.props
    let subjectProgress = testAttemptStore.getSectionsStats()
    // console.warn('Original subjectProgress: ', JSON.stringify(subjectProgress, null, 2))
    // console.warn('Testype for bar graph: ', JSON.stringify(testAttemptStore.testType, null, 2))
    // tslint:disable-next-line:max-line-length
    if (subjectProgress && testAttemptStore.testType !== 'chap' && testAttemptStore.testType !== 'concept' && testAttemptStore.testType !== 'conceptTest') {
      return <View style={{ flexDirection: 'row', padding: 20, flex: 1 }}>
        <PerformanceBarChartComponent
          currentContext={this}
          subjectList={subjectProgress}
          graphType='result'
        />
      </View>
      // let yAxisTotalQuestion = []
      // let xAxisSubjectArray = []
      // for (let subjectData of subjectProgress) {
      //   xAxisSubjectArray.push(capitalizeWords(subjectData.name))
      //   yAxisTotalQuestion.push({
      //     values: [subjectData.stats.totalMarks],
      //     label: 'Total Marks',
      //     config: {
      //       drawValues: false,
      //       colors: [processColor('#D0E3FF')]
      //     }
      //   })
      // }
      // let yAxisAttemptedQuestion = []
      // for (let attemptedData of subjectProgress) {
      //   yAxisAttemptedQuestion.push({
      //     values: [attemptedData.stats.marksPercentage],
      //     label: 'Attempted',
      //     config: {
      //       drawValues: false,
      //       colors: [processColor(colors.SkyeBlue)]
      //     }
      //   })
      // }
      // let i = 0, j = 0, k = 0
      // let n1 = yAxisTotalQuestion.length
      // let n2 = yAxisTotalQuestion.length
      // let finalyAxis = []
      // while (i < n1 && j < n2) {
      //   finalyAxis[k++] = yAxisTotalQuestion[i++]
      //   finalyAxis[k++] = yAxisAttemptedQuestion[j++]
      // }
      // // Store remaining elements of first array
      // while (i < n1)
      //   finalyAxis[k++] = yAxisTotalQuestion[i++]
      // // Store remaining elements of second array
      // while (j < n2)
      //   finalyAxis[k++] = yAxisAttemptedQuestion[j++]
      // const yAxisData = [{
      //   config: {
      //     barWidth: 0.1,
      //     group: {
      //       fromX: 0,
      //       groupSpace: 0.0,
      //       barSpace: 0.3
      //     }
      //   },
      //   label: 'Marks %',
      //   yAxis: finalyAxis
      // }]

      // const xAxisData = [{
      //   label: 'Subject',
      //   xAxis: xAxisSubjectArray

      // }]
      // const graphData = {
      //   xAxis: xAxisData,
      //   yAxis: yAxisData
      // }
      // return (
      //   <GroupBarChartComponent
      //     graphDataValues={graphData}
      //     xLabel={''}
      //     yLabel={'Marks %'}
      //   />
      // )
    }
    return null
  }
  renderLegendPurple() {
    return <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 10 }}>
      <View style={{ justifyContent: 'center' }}>
        <View style={{ height: 15, width: 15, backgroundColor: '#d1e3ff' }}>
        </View>
      </View>
      <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
        <Text style={{ fontSize: 15, color: '#999999' }}>Total Marks</Text>
      </View>
    </View>
  }

  renderLegendBlue() {
    return <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 10 }}>
      <View style={{ justifyContent: 'center' }}>
        <View style={{ height: 15, width: 15, backgroundColor: '#145bd3' }}>
        </View>
      </View>
      <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
        <Text style={{ fontSize: 15, color: '#999999' }}>Correct(%)</Text>
      </View>
    </View>
  }

  render() {
    const { testAttemptStore } = this.props
    const {
      questionsAttempted,
      totalQuestions,
      correctQuestions,
      attemptRate,
      accuracyRate,
      totalTimeTaken,
      wastedTime,
      speed,
      marksScored,
      maxMarks,
      correctQuestionsPercentage
    } = testAttemptStore.getTestPerformance()
    const { styles } = this.state
    const heightValues = this.getHeightValue()
    const imageBackgroundHeight = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [145, 50],
      extrapolate: 'clamp'
    })
    const imageBackgroundZIndex = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [0, 5],
      extrapolate: 'clamp'
    })
    const headerFontSize = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [24, 14],
      extrapolate: 'clamp'
    })
    const backIconSize = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [30, 18],
      extrapolate: 'clamp'
    })
    return (<View style={styles.container}>
      <Animated.Image
        style={[
          styles.headerView,
          {
            height: imageBackgroundHeight,
            zIndex: imageBackgroundZIndex
          }
        ]}
        source={icons.PERFORMANCE_HEADER_ICON}
      />
      <View style={[styles.headerStrip, { paddingTop: Platform.OS === 'ios' ? heightValues : 0 }]}>
        <TouchableOpacity onPress={() => this.onBackPressed()}>
          <Animated.Image
            source={icons.BACK_WHITE_ICON}
            style={[
              styles.performanceIcon, { marginRight: 10, marginTop: 12 },
              {
                height: backIconSize,
                width: backIconSize
              }
            ]}
          />
        </TouchableOpacity>
        <Animated.Text
          style={[
            styles.headerTestName,
            {
              fontSize: headerFontSize
            }
          ]}
        >
          Your Performance
        </Animated.Text>
      </View>
      <Animated.ScrollView
        scrollEventThrottle={1}
        onScroll={
          Animated.event([
            { nativeEvent: { contentOffset: { y: this.animatedValue } } }
          ])
        }
        showsVerticalScrollIndicator={false}
        style={{ zIndex: 4 }}
      >
        <View style={[styles.parallaxView, { padding: 0, marginTop: Platform.OS === 'ios' ? 100 : 80 }]}>
          <View style={{ flexDirection: 'row', padding: 15, width: widthPercentage(100) - 32 }}>
            <Text style={[styles.resultValue, { fontSize: 20 }]}>Marks</Text>
          </View>
          <View style={{
            backgroundColor: '#ffffff',
            flex: 1,
            flexWrap: 'wrap',
            flexDirection: 'row',
            paddingTop: 15
          }}>
            {/* {this.renderLegendPurple()}
            {this.renderLegendBlue()} */}
          </View>
            {this.renderGroupBarChart()}
          <View style={{
            flexDirection: 'column', backgroundColor: '#FAFCFE', paddingTop: 20, paddingBottom: 20
          }}>
            <View style={{ flexDirection: 'row', backgroundColor: '#FAFCFE' }}>
              <View style={{
                flexDirection: 'column', padding: 10, flex: 0.5, borderRightWidth: 1, borderRightColor: colors.GrayUnderLine,
                justifyContent: 'center'
              }}>
                <Text style={styles.resultValue}>{marksScored}<Text style={{ fontSize: 20 }}>/{maxMarks}</Text></Text>
                <Text style={styles.resultHeader}>Total Score</Text>
              </View>
              <View style={{ flexDirection: 'column', flex: 0.5, padding: 10, justifyContent: 'center' }}>
                <Text style={styles.resultValue}>{correctQuestionsPercentage + '%'}</Text>
                <Text style={styles.resultHeader}>Correct</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', backgroundColor: '#FAFCFE' }}>
              <View style={{
                flexDirection: 'column', padding: 10, flex: 0.5, borderRightWidth: 1, borderRightColor: colors.GrayUnderLine,
                justifyContent: 'center'
              }}>
                <Text style={styles.resultValue}>{attemptRate.toFixed(2)}%
                {/* <Text style={{ fontSize: 20, color: '#818181' }}>({questionsAttempted}/{totalQuestions})</Text> */}
                </Text>
                <Text style={styles.resultHeader}>Attempt Rate</Text>
              </View>
              <View style={{ flexDirection: 'column', flex: 0.5, padding: 10, justifyContent: 'center' }}>
                <Text style={styles.resultValue}>
                  {accuracyRate ? accuracyRate.toFixed(2) : 0}%
                  {/* <Text style={{ fontSize: 20, color: '#818181' }}>
                    ({correctQuestions}/{questionsAttempted})</Text> */}
                </Text>
                <Text style={styles.resultHeader}>Accuracy Rate</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.bottomView}>
          <View style={[styles.parallaxView, { flex: 1, marginTop: 0, padding: 0 }]}>
            <View style={{ flexDirection: 'row', padding: 10, width: widthPercentage(100) - 32 }}>
              <Text style={[styles.resultValue, { fontSize: 20 }]}>Time Strategy</Text>
            </View>
            <View style={{
              backgroundColor: '#ffffff',
              flex: 1,
              flexWrap: 'wrap',
              flexDirection: 'row',
              paddingTop: 15
            }}>
              {this.renderPieChartLegend()}
            </View>
              {this.renderPieChart()}
            <View style={{
              flexDirection: 'column', backgroundColor: '#FAFCFE', paddingTop: 20, paddingBottom: 20
            }}>
              <View style={{ flexDirection: 'row', backgroundColor: '#FAFCFE' }}>
                <View style={{
                  flexDirection: 'column', padding: 10, borderRightWidth: 1, borderRightColor: colors.GrayUnderLine,
                  justifyContent: 'center'
                }}>
                  <Text style={styles.resultValue}>{totalTimeTaken}</Text>
                  <Text style={styles.resultHeader}>Time Taken</Text>
                </View>
                <View style={{ flexDirection: 'column', padding: 10, justifyContent: 'center' }}>
                  <Text style={styles.resultValue}>{wastedTime}</Text>
                  <Text style={styles.resultHeader}>Wasted Time</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', backgroundColor: '#FAFCFE' }}>
                <View style={{ flexDirection: 'column', padding: 10, justifyContent: 'center' }}>
                  <Text style={styles.resultValue}>{speed.toFixed(2)}
                    <Text style={{ fontSize: 20, color: '#818181' }}> /Min</Text>
                  </Text>
                  <Text style={styles.resultHeader}>Speed</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Animated.ScrollView>
    </View >
    )
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.White
  },
  graphcontainer: {
    flex: 1
  },
  headerView: {
    position: 'absolute',
    flexDirection: 'row',
    width: widthPercentage(100),
    padding: 10
    // paddingBottom: 100
  },
  headerStrip: {
    position: 'absolute',
    flexDirection: 'row',
    width: widthPercentage(100),
    padding: 10,
    paddingBottom: 100,
    zIndex: 11
  },
  parallaxView: {
    borderWidth: 1,
    borderRadius: 6,
    borderColor: colors.White,
    borderBottomWidth: 1,
    shadowColor: colors.ShadowColor,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    shadowRadius: 4,
    marginTop: 100,
    margin: 15,
    padding: 15,
    backgroundColor: colors.White
  },
  bottomView: {
    flex: 1,
    marginTop: 20,
    flexDirection: 'row'
  },
  headerTestName: {
    fontSize: 24,
    color: colors.White,
    paddingTop: 10
    // lineHeight: 40
  },
  resultHeader: {
    color: '#666666',
    fontSize: 14,
    lineHeight: 20
  },
  resultValue: {
    color: '#000000',
    fontSize: 26,
    lineHeight: 30
  },
  performanceIcon: {
    width: 30,
    height: 30
  },
  chart: {
    flex: 1
  }
})
