import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import _ from 'lodash'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Platform,
  ScrollView,
  Dimensions,
  BackHandler,
  ImageBackground,
  TouchableOpacity,
  Animated,
  Button,
  ActivityIndicator
} from 'react-native'
import MathJax from 'react-native-mathjax'
import {
  heightPercentage,
  widthPercentage,
  isPortrait,
  isTablet,
  icons,
  BIGGER_PHONE_CATEGORIZE,
  getBiggerCategorization,
  verticalScale
} from '../../common'
import { colors } from '../../common-library/config'
import { dimens } from '../../config'
import { navigateSimple, goBack, showSnackbar } from '../../services'
import { TestAttemptStore } from '../../store'
import { NoDataFoundComponent } from '../../components'
import { CheckBox } from 'react-native-elements'

interface Props {
  navigation?: any
  testAttemptStore?: TestAttemptStore
  isModalVisible?: any
}

interface State {
  styles?: any,
  selectedSubject?: any,
  isLoading?: any
  isModalVisible?: any
  blockHeight: number,
  selectedComparisonParam: any
  isChecked?: boolean,
  correct?: any,
  incorrect?: any,
  unattempted?: any,
  easy?: any,
  medium?: any,
  difficult?: any,
  smcq?: any,
  mmcq?: any,
  trueFalse?: any,
  integer?: any,
  numerical?: any
  matrixType?: any,
  solutionList?: any,
  isFilterApplied?: boolean
  isWebViewReady?: boolean
}

const correctPredicate = item => item.isCorrect === true
const incorrectPredicate = item => item.isIncorrect === true
const unattemptedPredicate = item => item.isAttempted === false
const integerPredicate = item => item.questionType === 'Integer'
const numericalPredicate = item => item.questionType === 'Numerical'
const smcqPredicate = item => item.questionType === 'SMCQ'
const mmcqPredicate = item => item.questionType === 'MMCQ'
const matrixPredicate = item => item.questionType === 'Matrix'
const trueFalsePredicate = item => item.questionType === 'True/False'

const FILTER_ITEM = {
  TRUE_FALSE: { key: ['questionType'], title: 'True & False' }
}

const getColorIndicator = (isCorrect, isIncorrect, isAttempted) => {
  if (isCorrect) {
    return colors.GreenProgressBarColor
  } else if (!isAttempted) {
    return colors.White
  } else {
    return colors.RedProgressBarColor
  }
}

@inject('testAttemptStore')
@observer
export class ResultSolutionPage extends Component<Props, State> {
  _onBackPressed
  animatedValue = new Animated.Value(0)
  webViewStyle = `<style type="text/css">
    html {
      overflow: scroll;
      background-color: white;
    }
    body{animation: fadeIn 1s}
@keyframes fadeIn{
	0%{opacity:0;background:#000}
	90%{opacity:0}
	100%{opacity:1;background:#fff}
}
div p, div b, div span, div b {
  padding: 0 !important;
  margin: 0 !important;
}
    div.content{
      font-size: 16px;
      line-height: 18px;
      width: 100%;
      color: black;
      height: 40px;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
    }
    div.content > p {
      font-size: 16px
    }
    .correctOption {
      margin: 2px;
      padding: 2px;
      border-radius: 4px;
      border: 1px solid #000000;
      font-size: 16px
    }
    .option {
      margin: 2px;
      padding: 2px;
      font-size: 16px
    }
    </style>`
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      isLoading: true,
      isModalVisible: false,
      blockHeight: 0,
      selectedComparisonParam: [],
      solutionList: [],
      isFilterApplied: false,
      correct: false,
      incorrect: false,
      unattempted: false,
      easy: false,
      medium: false,
      difficult: false,
      smcq: false,
      mmcq: false,
      trueFalse: false,
      matrixType: false,
      numerical: false,
      integer: false,
      isWebViewReady: false
    }
    this._onBackPressed = this.onBackPressed.bind(this)
  }
  async componentDidMount() {
    const { testAttemptStore } = this.props
    await testAttemptStore.getSubjectFilterList(testAttemptStore.testId)
    if (testAttemptStore.subjectList && testAttemptStore.subjectList.length > 0) {
      testAttemptStore.getTestSolution(testAttemptStore.testId, testAttemptStore.attemptId, testAttemptStore.subjectList[0]._id, '2').then(() => {
        const solutionList = testAttemptStore.getSolutionsData()
        this.setState({
          solutionList: solutionList,
          isLoading: false
        })
      })
    }
    Dimensions.addEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
  }
  componentWillUnmount() {
    Dimensions.removeEventListener(
      'change',
      this._orientationChangeListener.bind(this)
    )
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
  }

  onBackPressed() {
    const { navigation } = this.props
    return goBack(navigation)
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  onLoad() {
    setTimeout(() => {
      this.setState({
        isWebViewReady: true
      })
    }, 1000)
  }

  onSolutionCardPress = (solution, index) => {
    navigateSimple(this.props.navigation, 'SolutionDetailPage', { solution: solution, index: index })
  }
  getSelectedSubject() {
    const { selectedSubject } = this.state
    const { testAttemptStore } = this.props
    const subjectList = testAttemptStore.getSubjectFilterData()
    if (selectedSubject) {
      return selectedSubject
    }
    if (subjectList) {
      return subjectList[0]
    }
  }
  getSelectedComparisonParam() {
    const { selectedComparisonParam } = this.state
    const { testAttemptStore } = this.props
    if (selectedComparisonParam) {
      return selectedComparisonParam
    }
    if (testAttemptStore.comparisonData && testAttemptStore.comparisonData.compareBy) {
      return testAttemptStore.comparisonData.compareBy[0]
    }
  }
  setSelectedResponseType(selection) {
    this.setState({
      selectedComparisonParam: selection
    })
  }
  setSelectedDifficultyLevel(selection) {
    this.setState({
      selectedComparisonParam: selection
    })
  }
  setSelectedQuestionType(selection) {
    this.setState({
      selectedComparisonParam: selection
    })
  }
  setSelectedSkills(selection) {
    this.setState({
      selectedComparisonParam: selection
    })
  }
  setSelectedTimeTaken(selection) {
    this.setState({
      selectedComparisonParam: selection
    })
  }
  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }

  renderSubjectList(subject, selectedSubject) {
    const { styles } = this.state
    return subject.map(subjectObj => (
      <TouchableOpacity
        style={{ flexDirection: 'row' }}
        key={subjectObj}
        onPress={() => this.setSelectedSubject(subjectObj)}
      >
        <Text style={[styles.subjectText, {
          fontWeight: selectedSubject === subjectObj ? '900' : '300'
        }]}>{(subjectObj.name).toUpperCase()}</Text>
      </TouchableOpacity>
    ))
  }

  setSelectedSubject(subject) {
    const { testAttemptStore } = this.props
    const { isFilterApplied } = this.state
    const that = this
    that.setState({
      selectedSubject: subject,
      isLoading: true
    }, () => setTimeout(() => {
      that.setState({
        isLoading: false
      })
    }, 500))
    testAttemptStore.getTestSolution(testAttemptStore.testId, testAttemptStore.attemptId, subject._id, '2').then(() => {
      const solutionList = testAttemptStore.getSolutionsData()
      this.applyFilterButtonClick(false, solutionList)
    })
  }

  renderSubjectScrollView(subjects, selectedSubject) {
    if (subjects && subjects.length > 0) {
      const { styles } = this.state
      return (
        <View
          style={{
            padding: 10,
            paddingLeft: 0,
            marginTop: Platform.OS === 'ios' ? 40 : 30,
            width: widthPercentage(100),
            flex: 1
          }}
        >
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={this.state.styles.submitButtonView}>
              {this.renderSubjectList(subjects, selectedSubject)}
            </View>
          </ScrollView>
        </View>
      )
    }
    return null
  }
  getQuestionHtml(text, index, options) {
    if (text) {
      // let start = text.indexOf('<span class="math-tex">')
      // let end = text.indexOf('</span>')
      // let test = text.substring(start, end)
      // test = 'You answered this question' + test.replace('/', '//')
      if (index) {
        text = ` <div style="display: block; width: 100%;">
        <div style="display: inline-block; float: left; width: 30px; ">
   ${text}.
    </div>
    <div style="float: left; width: calc(100% - 30px); display: inline-block;">
    ${index}
     </div>
    </div>`
      } else {
        text = `${text}`
      }
      const htmlContent = `<div style="width: 100%; display: block; position: relative;">
            <div class="math-tex content" style="background-color: #ffffff; overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical; position: relative; margin-left: 0px;">
            <div style="height: 40px; display: block; width: 100%;
            position: absolute; top: 0; left: 0; z-index: 9999;
             background-image: linear-gradient(to bottom, rgba(255,0,0,0), rgba(255,255,255,1));">
           </div>
            ${text}${options ? '<br />' + options : ''}</div>
            </div>
            `
      return htmlContent
    }
    return null
  }

  getHTML(text, index, options) {
    const { testAttemptStore } = this.props
    const baseUrl = testAttemptStore.getFilesBaseUrl()
    if (text) {
      // let start = text.indexOf('<span class="math-tex">')
      // let end = text.indexOf('</span>')
      // let test = text.substring(start, end)
      // test = 'You answered this question' + test.replace('/', '//')
      const htmlContent = `
          <html>
          <head>
          ${this.webViewStyle}
          </head>
          <body>
          ${this.getQuestionHtml(text, index, options)}
          </body>
          </html>`
      return htmlContent
    }
    return null
  }
  renderQuestionContent(questionNo, questionContent) {
    const { testAttemptStore } = this.props
    const { isWebViewReady } = this.state
    const baseUrl = testAttemptStore.getFilesBaseUrl()
    const scalesPageToFit = Platform.OS === 'android'
    return <View style={{ width: widthPercentage(100) - 50, height: 50 }}>
      {!isWebViewReady ? <ActivityIndicator size='large' /> : null}
      <MathJax
        html={this.getHTML(questionNo, questionContent, '')}
        mathJaxOptions={{
          messageStyle: 'none',
          showMathMenu: false,
          extensions: ['tex2jax.js'],
          jax: ['input/TeX', 'output/HTML-CSS'],
          CommonHTML: { linebreaks: { automatic: true } },
          SVG: { linebreaks: { automatic: true } },
          'HTML-CSS': {
            linebreaks: {
              automatic: true
            }
          },
          tex2jax: {
            inlineMath: [['$', '$'], ['\\(', '\\)']],
            displayMath: [['$$', '$$'], ['\\[', '\\]']],
            processEscapes: true,
            process: 'none',
            showMathMenu: false
          }
        }}
        baseUrl={baseUrl}
        heightChangeHandler={() => this.onLoad()}
      // messageHandler={(data) => this.messageHandler(data, currentQuestion.qId)}
      />
    </View>

  }
  renderNoDataView(txt) {
    const { solutionList, isLoading } = this.state
    if (this.state.isFilterApplied === true) {
      return <View style={{
        flex: 1, alignItems: 'center', paddingTop: isTablet() ? heightPercentage(25) : heightPercentage(25),
        justifyContent: 'center', padding: 10
      }}>
        <NoDataFoundComponent
          headingText={'No Result Found'}
          descText={`There are no question found for this Filter.`}
          iconName={icons.NOTESTICON} />
      </View>
    }
    return isLoading > 0 ? <View style={{
      marginTop: heightPercentage(50) - 250, flex: 1, alignItems: 'center',
      justifyContent: 'center', padding: 10
    }}><ActivityIndicator size='large' />
    </View> : <View style={{
      flex: 1, marginTop: isTablet() ? heightPercentage(50) - 250 : heightPercentage(50) - 100,
      alignItems: 'center', alignSelf: 'center',
      justifyContent: 'center', padding: 10
    }}>
        <Text style={{ fontSize: 20, color: colors.Black, textAlign: 'center' }}>The Solution will be displayed after the window ends.</Text>
      </View>
  }
  renderComaprisonCardView(solution, index) {
    const { styles } = this.state
    const cardColor = getColorIndicator(solution.isCorrect, solution.isIncorrect, solution.isAttempted)
    return (
      <View style={[styles.parallaxView, { flexDirection: 'column' }]} key={solution.questionNo}>
        <TouchableOpacity
          style={{
            paddingLeft: 0,
            padding: 10,
            paddingTop: 0,
            borderLeftWidth: 10,
            borderLeftColor: cardColor,
            borderRadius: 5
          }}
          onPress={() => this.onSolutionCardPress(solution, index)}
        >
          <View style={{ flexDirection: 'column', flex: 1, height: 110 }}>
            <View style={styles.cardQuestionView}>
              {this.renderQuestionContent(solution.questionNo, solution.questionContent)}
            </View>
            <View
              style={{ flexDirection: 'column', marginTop: 10 }}
            >
              <View style={[styles.comparisonTextView]}>
                <Text style={styles.resultValue}>{solution.time}</Text>
              </View>
              {/* <View style={styles.comparisonTextView}>
              <Text style={styles.resultValue}>{solution.subject}</Text>
              <Text style={styles.resultHeader}>{solution.difficultyLevel}</Text>
            </View> */}
              {/* <View style={styles.comparisonTextView}>
              <Text style={styles.resultValue}>100%</Text>
              <Text style={styles.resultHeader}>Attempt By</Text>
            </View> */}
              <View style={styles.comparisonTextView}>
                <Text style={styles.resultHeader}>Time Taken</Text>
                {
                  solution.difficultyLevel ?
                    <Text numberOfLines={1} style={styles.difficultyText}>{solution.difficultyLevel}</Text>
                    :
                    null
                }
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
  onFilterIconPress = () => {
    navigateSimple(this.props.navigation, 'ComparisonFilterPage')
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    })
  }
  isChecked(key) {
    return this.state[key]
  }
  updateFilter(key) {
    //
    switch (key) {
      case 'correct':
        this.setState({ correct: !this.state.correct })
        break
      case 'incorrect':
        this.setState({ incorrect: !this.state.incorrect })
        break
      case 'unattempted':
        this.setState({ unattempted: !this.state.unattempted })
        break
      case 'easy':
        this.setState({ easy: !this.state.easy })
        break
      case 'medium':
        this.setState({ medium: !this.state.medium })
        break
      case 'difficult':
        this.setState({ difficult: !this.state.difficult })
        break
      case 'smcq':
        this.setState({ smcq: !this.state.smcq })
        break
      case 'mmcq':
        this.setState({ mmcq: !this.state.mmcq })
        break
      case 'matrixType':
        this.setState({ matrixType: !this.state.matrixType })
        break
      case 'trueFalse':
        this.setState({ trueFalse: !this.state.trueFalse })
        break
      case 'integer':
        this.setState({ integer: !this.state.integer })
        break
      case 'numerical':
        this.setState({ numerical: !this.state.numerical })
        break
      default:
        break
    }

  }
  renderAttemptedRadioButton(title) {
    return (<View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
      <CheckBox
        checked={this.isChecked(title)}
        onPress={() => this.updateFilter(title)}
        containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
        checkedColor='#396bd4'
      />
    </View>
    )
  }

  renderComaprisonByItem(type) {
    const { styles } = this.state
    return (
      <TouchableOpacity style={styles.rowView}>
        {this.renderAttemptedRadioButton(type.title)}
        <Text style={styles.filterItemTextStyle}>{type.title}</Text>
      </TouchableOpacity>
    )
  }

  renderResponseType() {
    const { styles } = this.state
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Response Type</Text>
        </View>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('correct')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('correct')}
              onPress={() => this.updateFilter('correct')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Correct</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('incorrect')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('incorrect')}
              onPress={() => this.updateFilter('incorrect')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Incorrect</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('unattempted')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('unattempted')}
              onPress={() => this.updateFilter('unattempted')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Unattempted</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderDifficultyLevel() {
    const { styles } = this.state
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Difficulty Level</Text>
        </View>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('easy')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('easy')}
              onPress={() => this.updateFilter('easy')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Easy</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('medium')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('medium')}
              onPress={() => this.updateFilter('medium')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Medium</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('difficult')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('difficult')}
              onPress={() => this.updateFilter('difficult')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Difficult</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderQuestionType() {
    const { styles } = this.state
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Question Type</Text>
        </View>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('smcq')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('smcq')}
              onPress={() => this.updateFilter('smcq')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>SMCQ</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('mmcq')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('mmcq')}
              onPress={() => this.updateFilter('mmcq')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>MMCQ</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('matrixType')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('matrixType')}
              onPress={() => this.updateFilter('matrixType')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Matrix</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('trueFalse')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('trueFalse')}
              onPress={() => this.updateFilter('trueFalse')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>{FILTER_ITEM.TRUE_FALSE.title}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('numerical')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('numerical')}
              onPress={() => this.updateFilter('numerical')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Numerical</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('integer')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('integer')}
              onPress={() => this.updateFilter('integer')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Integer</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderSkills() {
    const { styles } = this.state
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Skills</Text>
        </View>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('analytical')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('analytical')}
              onPress={() => this.updateFilter('analytical')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Analytical</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('conceptual')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('conceptual')}
              onPress={() => this.updateFilter('conceptual')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Conceptual</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('intuitive')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('intuitive')}
              onPress={() => this.updateFilter('intuitive')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Intuitive</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderTimeTaken() {
    const { styles } = this.state
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column'
        }}
      >
        <View style={styles.comparisonHeaderView}>
          <Text style={{ fontSize: 22, color: colors.Black }}>Time Taken</Text>
        </View>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('minTomax')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('minTomax')}
              onPress={() => this.updateFilter('minTomax')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Min to Max</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rowView}
          onPress={() => this.updateFilter('maxTomin')}>
          <View style={{ width: 30, paddingRight: 25, marginLeft: -10 }}>
            <CheckBox
              checked={this.isChecked('maxTomin')}
              onPress={() => this.updateFilter('maxTomin')}
              containerStyle={{ width: 45, height: 45, backgroundColor: colors.White, borderWidth: 0 }}
              checkedColor='#396bd4'
            />
          </View>
          <Text style={styles.filterItemTextStyle}>Max to min</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderFooterView() {
    if (this.state.isModalVisible) {
      const { styles } = this.state
      return <View style={styles.footerView}>
        <TouchableOpacity style={styles.singleBtnStyle}
          onPress={() => this.applyFilterButtonClick()}>
          <Text style={styles.buttonText}>FILTER APPLY</Text>
        </TouchableOpacity>
      </View>
    } else {
      return null
    }
  }

  applyFilterButtonClick(showMessage = true, solutions = null) {
    const { testAttemptStore } = this.props
    const { correct, incorrect, unattempted, smcq, mmcq, trueFalse, matrixType, numerical, integer } = this.state
    if (this.checkIfAnyFilterItemSelected()) {
      const solutionList = solutions || testAttemptStore.getSolutionsData()
      const data = solutionList.filter((item) => {
        const responsePredicates = []
        const questionTypePredicates = []
        if (correct) {
          responsePredicates.push(correctPredicate)
        }
        if (incorrect) {
          responsePredicates.push(incorrectPredicate)
        }
        if (unattempted) {
          responsePredicates.push(unattemptedPredicate)
        }
        if (numerical) {
          questionTypePredicates.push(numericalPredicate)
        }
        if (integer) {
          questionTypePredicates.push(integerPredicate)
        }
        if (smcq) {
          questionTypePredicates.push(smcqPredicate)
        }
        if (mmcq) {
          questionTypePredicates.push(mmcqPredicate)
        }
        if (matrixType) {
          questionTypePredicates.push(matrixPredicate)
        }
        if (trueFalse) {
          questionTypePredicates.push(trueFalsePredicate)
        }

        const questionTypeSatisfied = !questionTypePredicates.length || questionTypePredicates.reduce((accum, predicate) => {
          return accum || predicate(item)
        }, false)
        const responseTypeSatisfied = !responsePredicates.length || responsePredicates.reduce((accum, predicate) => {
          return accum || predicate(item)
        }, false)
        return questionTypeSatisfied && responseTypeSatisfied
      }).map((item) => {
        return { ...item }
      })
      this.setState({
        isFilterApplied: true,
        isModalVisible: false,
        solutionList: data
      })
    } else {
      if (showMessage) {
        showSnackbar('Please choose any option to filter')
      } else {
        this.setState({
          solutionList: solutions
        })
      }
    }
  }
  closeFilter() {
    this.setState({
      isModalVisible: false
    })
  }
  renderModal = () => {
    if (this.state.isModalVisible) {
      const heightValues = this.getHeightValue()
      const { styles, isLoading } = this.state
      return <View style={this.state.styles.modalView}>
        <View style={styles.overlayView}></View>
        <ScrollView style={styles.mainContentView}>
          <ImageBackground
            style={{ width: widthPercentage(100), padding: 10, paddingBottom: 120, position: 'relative', flexDirection: 'row' }}
            source={icons.HEADER_CUSTOM_ICON}
          >
            <TouchableOpacity onPress={() => this.closeFilter()}>
              <Image
                source={icons.CLOSEWHITE_ICON}
                style={[
                  styles.performanceIcon,
                  { marginRight: 10, marginTop: 6 },
                  {
                    height: 22,
                    width: 22
                  }
                ]}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{ position: 'absolute', top: 10, right: 10, zIndex: 999 }}
              onPress={() => this.resetFilter()}
            >
              <Text style={{ color: colors.White, fontSize: 18 }}>
                Reset Filter
            </Text>
            </TouchableOpacity>
            <Text
              style={{ fontSize: 24, color: colors.White }}>
              Filter
          </Text>
          </ImageBackground>
          <View style={[styles.bottomView, { marginTop: -80 }]}>
            <View style={[styles.parallaxView, { marginTop: 10, padding: 10 }]}>
              {this.renderResponseType()}
            </View>
            {/* <View style={[styles.parallaxView, { marginTop: 10, padding: 10 }]}>
              {this.renderDifficultyLevel()}
            </View> */}
            <View style={[styles.parallaxView, { marginTop: 10, padding: 10 }]}>
              {this.renderQuestionType()}
            </View>
            {/* <View style={[styles.parallaxView, { marginTop: 10, padding: 10 }]}>
              {this.renderSkills()}
            </View>
            <View style={[styles.parallaxView, { marginTop: 10, padding: 10 }]}>
              {this.renderTimeTaken()}
            </View> */}
          </View>
        </ScrollView>
        {this.renderFooterView()}
      </View>
    } else {
      return null
    }
  }
  resetFilter() {
    const { testAttemptStore } = this.props
    testAttemptStore.resetSolutionFilter()
    this.setState({
      correct: false,
      incorrect: false,
      unattempted: false,
      easy: false,
      medium: false,
      difficult: false,
      smcq: false,
      mmcq: false,
      trueFalse: false,
      matrixType: false,
      numerical: false,
      integer: false,
      isFilterApplied: false
    })
    const solutionList = testAttemptStore.getSolutionsData()
    this.setState({
      solutionList: solutionList
    })
  }
  checkIfAnyFilterItemSelected() {
    const { correct, incorrect, unattempted, smcq, mmcq, trueFalse, matrixType, numerical, integer } = this.state

    if (!correct && !incorrect && !unattempted && !smcq && !mmcq && !trueFalse && !matrixType && !numerical && !integer) {
      return false
    }
    return true
  }
  render() {
    const { styles, isModalVisible, solutionList, isLoading } = this.state
    const { testAttemptStore } = this.props
    //  const solutionsData = testAttemptStore.getSolutionsData()
    const heightValues = this.getHeightValue()
    const subjectList = testAttemptStore.getSubjectFilterData()
    const selectedSubject = this.getSelectedSubject()
    const imageBackgroundHeight = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [145, 50],
      extrapolate: 'clamp'
    })
    const imageBackgroundZIndex = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [0, 5],
      extrapolate: 'clamp'
    })
    // const subjectLeftPosition = this.animatedValue.interpolate({
    //     inputRange: [0, 40],
    //     outputRange: [0, widthPercentage(100) + 10],
    //     extrapolate: 'clamp'
    // })
    // const subjectOpacity = this.animatedValue.interpolate({
    //     inputRange: [0, 30],
    //     outputRange: [1, 0],
    //     extrapolate: 'clamp'
    // })
    // const subjectTop = this.animatedValue.interpolate({
    //     inputRange: [0, 30],
    //     outputRange: [10, 0],
    //     extrapolate: 'clamp'
    // })
    // const imageBackgroundZIndex = this.animatedValue.interpolate({
    //     inputRange: [0, 40],
    //     outputRange: [0, 5],
    //     extrapolate: 'clamp'
    // })
    // const headerFontSize = this.animatedValue.interpolate({
    //     inputRange: [0, 40],
    //     outputRange: [24, 14],
    //     extrapolate: 'clamp'
    // })
    // const backIconSize = this.animatedValue.interpolate({
    //     inputRange: [0, 40],
    //     outputRange: [30, 18],
    //     extrapolate: 'clamp'
    // })
    return <View style={styles.container}>
      <Animated.Image
        style={[
          styles.headerView,
          {
            height: imageBackgroundHeight,
            zIndex: imageBackgroundZIndex
          }
        ]}
        source={icons.SOLUTION_HEADER_ICON}
      />
      <View
        style={[
          styles.headerStrip,
          { paddingTop: Platform.OS === 'ios' ? heightValues : 0 }
        ]}
      >
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: Platform.OS === 'ios' ? heightValues : 0,
            left: 10,
            zIndex: 99999
          }}
          onPress={() => this.onBackPressed()}
        >
          <Animated.Image
            source={icons.BACK_WHITE_ICON}
            style={[
              styles.performanceIcon,
              { marginRight: 10, marginTop: 12 },
              {
                height: 30,
                width: 30
              }
            ]}
          />
        </TouchableOpacity>
        <Animated.Text
          style={[
            styles.headerTestName,
            {
              fontSize: 24
            }
          ]}
        >
          Solution
          </Animated.Text>
        <TouchableOpacity
          style={{ position: 'absolute', top: 0, right: 0, zIndex: 999 }}
          onPress={() => this.toggleModal()}
        >
          {this.renderFilterAppliedView()}
          <Animated.Image
            source={icons.RESULT_FILTER_ICON}
            style={[
              styles.backIcon,
              { marginRight: 10, marginTop: 10 },
              {
                height: 30,
                width: 30
              }
            ]}
          />
        </TouchableOpacity>
        <Animated.View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: 10,
            left: 0,
            opacity: 1
          }}
        >
          {this.renderSubjectScrollView(subjectList, selectedSubject)}
        </Animated.View>
      </View>
      <View style={{ flex: 1 }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[
            styles.bottomView,
            { marginTop: Platform.OS === 'ios' ? 100 : 100 }
          ]}
        >
          {(solutionList && solutionList.length) ?
            solutionList.map((item, index) => {
              return this.renderComaprisonCardView(item, index)
            }) : this.renderNoDataView('No Data found')}
        </ScrollView>
      </View>
      {this.renderModal()}
    </View>
    // }
  }

  showCardView(solutionList) {
    if (solutionList && solutionList.length) {
      solutionList.map((item, index) => {
        return this.renderComaprisonCardView(item, index)
      })
    } else {
      return null
    }
    return null
  }
  renderFilterAppliedView() {
    const { isFilterApplied } = this.state
    if (isFilterApplied) {
      return <View
        style={{
          width: 10, position: 'absolute', top: 10, right: 10, zIndex: 999,
          height: 10, backgroundColor: colors.Red, borderRadius: 20
        }}>
      </View>
    }
    return null
  }
}

const getStyle = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: colors.White
    },
    headerView: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10
      // paddingBottom: 100
    },
    headerStrip: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10,
      paddingBottom: 100,
      zIndex: 5
    },
    parallaxView: {
      borderWidth: 1,
      borderColor: colors.BorderColor,
      borderBottomWidth: 1,
      shadowColor: colors.ShadowColor,
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      elevation: 2,
      shadowRadius: 4,
      margin: 15,
      backgroundColor: colors.White,
      flex: 1,
      marginTop: 0,
      paddingLeft: 0,
      borderRadius: 5
    },
    bottomView: {
      flex: 1,
      marginTop: 115,
      flexDirection: 'column'
    },
    headerTestName: {
      fontSize: 24,
      color: colors.White,
      paddingTop: 10,
      paddingLeft: 40
      // lineHeight: 40
    },
    resultHeader: {
      color: '#999999',
      fontSize: 13,
      lineHeight: 20
    },
    resultValue: {
      color: '#000000',
      fontSize: 21,
      lineHeight: 30
    },
    backIcon: {
      width: 30,
      height: 30
    },
    profileIcon: {
      width: 50,
      height: 50
    },
    submitButtonView: {
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    profileView: {
      flexDirection: 'column',
      paddingLeft: 15,
      paddingRight: 15,
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center'
    },
    subjectText: {
      color: colors.White,
      fontSize: 20,
      padding: 10
    },
    subjectMainView: {
      padding: 10,
      width: widthPercentage(100),
      flex: 1
    },
    cardQuestionView: {
      flexDirection: 'row',
      marginLeft: -10,
      flex: 1
    },
    comparisonTextView: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    rowView: {
      flexDirection: 'row',
      height: verticalScale(48),
      alignItems: 'center',
      backgroundColor: '#ffffff'
    },
    filterItemTextStyle: {
      paddingLeft: 15,
      color: '#666666',
      fontSize: 16
    },
    footerView: {
      marginTop: 10,
      flex: 1,
      flexDirection: 'row',
      position: 'absolute',
      bottom: 0,
      left: 0,
      zIndex: 999999
    },
    singleBtnStyle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.PrimaryBlue,
      padding: 15
    },
    buttonText: {
      color: 'white',
      textAlign: 'center',
      fontSize: 16
    },
    comparisonHeaderView: {
      flexDirection: 'row',
      paddingBottom: 10,
      justifyContent: 'space-between'
    },
    modalView: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    overlayView: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: colors.White,
      zIndex: 99
    },
    mainContentView: {
      position: 'absolute',
      left: 0,
      top: 0,
      bottom: 50,
      right: 0,
      width: widthPercentage(100),
      zIndex: 100,
      backgroundColor: colors.White
    },
    difficultyText: {
      fontSize: 14,
      color: '#4A4A4A',
      lineHeight: 16
    }
  })
