import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
	StyleSheet,
	View,
	Text,
	Platform,
	ScrollView,
	Dimensions,
	BackHandler,
	TouchableOpacity,
	Animated
} from 'react-native'
import {
	widthPercentage,
	icons,
	BIGGER_PHONE_CATEGORIZE,
	getBiggerCategorization,
	verticalScale,
	PROGRESS_BAR_TYPE
} from '../../common'
import { colors } from '../../common-library/config'
import { goBack } from '../../services'
import { TestAttemptStore } from '../../store'
import { ProgressBarComponent } from '../../components'
interface Props {
	navigation?: any
	testAttemptStore?: TestAttemptStore
	isModalVisible?: any
}

interface State {
	styles?: any,
	selectedSubject?: any
	isLoading?: any
	isModalVisible?: any
	blockHeight: number
	weaknesslistItem: any
	ChapterNameHeader: string
}

const getStyle = () =>
	StyleSheet.create({
		container: {
			flex: 1,
			flexDirection: 'column',
			backgroundColor: colors.White
		},
		headerView: {
			position: 'absolute',
			flexDirection: 'row',
			width: widthPercentage(100),
			padding: 10
			// paddingBottom: 100
		},
		headerStrip: {
			position: 'absolute',
			flexDirection: 'row',
			width: widthPercentage(100),
			padding: 10,
			paddingBottom: 50,
			zIndex: 5
		},
		parallaxView: {
			borderWidth: 1,
			borderColor: colors.BorderColor,
			borderBottomWidth: 1,
			shadowColor: colors.ShadowColor,
			shadowOffset: { width: 0, height: 2 },
			shadowOpacity: 0.2,
			elevation: 2,
			shadowRadius: 4,
			margin: 15,
			backgroundColor: colors.White,
			flex: 1,
			marginTop: 0,
			paddingLeft: 0,
			borderRadius: 5
		},
		bottomView: {
			flex: 1,
			marginTop: 115,
			flexDirection: 'column'
		},
		headerTestName: {
			fontSize: 24,
			color: colors.White,
			paddingTop: 4,
			paddingLeft: 40
			// lineHeight: 40
		},
		resultHeader: {
			color: '#323232',
			fontSize: 13,
			lineHeight: 20
		},
		resultValue: {
			color: '#000000',
			fontSize: 22,
			lineHeight: 30
		},
		backIcon: {
			width: 30,
			height: 30
		},
		profileIcon: {
			width: 50,
			height: 50
		},
		submitButtonView: {
			alignSelf: 'center',
			justifyContent: 'center',
			alignItems: 'center',
			flexDirection: 'row'
		},
		profileView: {
			flexDirection: 'column',
			paddingLeft: 15,
			paddingRight: 15,
			alignItems: 'center',
			alignContent: 'center',
			justifyContent: 'center'
		},
		subjectText: {
			color: colors.White,
			fontSize: 20,
			padding: 10
		},
		subjectMainView: {
			padding: 10,
			width: widthPercentage(100),
			flex: 1
		},
		cardQuestionView: {
			flexDirection: 'row',
			flex: 1,
			height: 100,
			marginLeft: -10,
			marginBottom: 10
		},
		comparisonTextView: {
			flexDirection: 'column',
			justifyContent: 'center'
		},
		rowView: {
			flexDirection: 'row',
			height: verticalScale(48),
			alignItems: 'center',
			backgroundColor: '#ffffff'
		},
		filterItemTextStyle: {
			paddingLeft: 15,
			color: '#666666',
			fontSize: 16
		},
		footerView: {
			marginTop: 10,
			flex: 1,
			flexDirection: 'row'
		},
		singleBtnStyle: {
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
			backgroundColor: colors.PrimaryBlue,
			padding: 15
		},
		buttonText: {
			color: 'white',
			textAlign: 'center',
			fontSize: 16
		},
		comparisonHeaderView: {
			flexDirection: 'row',
			paddingBottom: 10,
			justifyContent: 'space-between'
		},
		comparisonMaincontentView: {
			flexDirection: 'row',
			padding: 10,
			justifyContent: 'space-between'
		},
		circleTextView: {
			width: 40,
			height: 40,
			// borderRadius: 30,
			// borderWidth: 1,
			justifyContent: 'center',
			alignItems: 'center',
			alignContent: 'center'
		},
		textValueStyle: {
			color: colors.Black,
			fontWeight: 'bold',
			fontSize: 20
		},
		circleText: {
			color: colors.Black,
			fontWeight: 'bold'
		},
		modalView: {
			position: 'absolute',
			top: 0,
			left: 0,
			bottom: 0,
			right: 0
		},
		overlayView: {
			position: 'absolute',
			top: 0,
			left: 0,
			bottom: 0,
			right: 0,
			backgroundColor: colors.Black,
			opacity: 0.6,
			zIndex: 99
		},
		mainContentView: {
			position: 'absolute',
			left: 0,
			top: 0,
			bottom: 0,
			right: 0,
			width: widthPercentage(100),
			zIndex: 100,
			backgroundColor: colors.White
		},
		tabTextStyle: {
			fontSize: 20,
			color: '#666666',
			fontWeight: 'bold'
		},
		submitButton: {
			backgroundColor: colors.PrimaryBlue,
			flexDirection: 'row',
			alignSelf: 'center',
			borderWidth: 0,
			padding: 10,
			paddingLeft: 30,
			paddingRight: 30,
			width: widthPercentage(70),
			justifyContent: 'center',
			borderRadius: 20
		},
		subjectButton: {
			backgroundColor: colors.White,
			flexDirection: 'row',
			alignSelf: 'center',
			padding: 7,
			paddingLeft: 30,
			paddingRight: 30,
			marginRight: 10,
			justifyContent: 'center',
			borderRadius: 5,
			borderColor: colors.Black,
			borderWidth: 1
		},
		submitTextStyle: {
			fontSize: 15,
			fontWeight: 'bold',
			color: colors.White,
			textAlign: 'center',
			lineHeight: 16
		},
		footerButtonView: {
			position: 'absolute',
			bottom: 0,
			left: 0,
			zIndex: 9,
			width: widthPercentage(100),
			backgroundColor: colors.White,
			padding: 10
		}
	})
@inject('testAttemptStore')
@observer
export class ResultSwotDetailPage extends Component<Props, State> {
	animatedValue = new Animated.Value(0)
	webViewStyle = `<style type="text/css">
    html {
      overflow: scroll;
      background-color: white;
    }
    div.content{
      font-size: 18px,
      width: 100%;
      color: gray;
      height: 70px
    }
    div.content > p {
      display: inline;
      font-size: 16px
    }
    .correctOption {
      margin: 2px;
      padding: 2px;
      border-radius: 4px;
      border: 1px solid #000000;
      font-size: 16px
    }
    .option {
      margin: 2px;
      padding: 2px;
      font-size: 16px
    }
    </style>`
	constructor(props: Props, state: State) {
		super(props, state)
		this.state = {
			styles: getStyle(),
			isLoading: false,
			isModalVisible: false,
			blockHeight: 0,
			weaknesslistItem: '',
			ChapterNameHeader: ''
		}
	}
	componentDidMount() {
		Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
		BackHandler.addEventListener('hardwareBackPress', () => this.onBackPressed())
		const { weaknesslistItem , chapterNameHeader} = this.props.navigation.state.params
		this.setState({ weaknesslistItem : weaknesslistItem ,
			ChapterNameHeader : chapterNameHeader})
	}
	componentWillUnmount() {
		Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
		BackHandler.removeEventListener('hardwareBackPress', () => this.onBackPressed())
	}
	onBackPressed() {
		const { navigation } = this.props
		return goBack(navigation)
	}
	_orientationChangeListener() {
		this.setState({
			styles: getStyle()
		})
	}
	getHeightValue() {
		let heightValue = 20
		switch (getBiggerCategorization()) {
			case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
				heightValue = 40
				break
			default:
				break
		}
		return heightValue
	}
	renderComaprisonCardView(item) {
		const { styles } = this.state
		return <View style={[styles.parallaxView, { flexDirection: 'column' }]} key={item.conceptName}>
			<View style={{ padding: 10, paddingTop: 5, paddingBottom: 5, borderRadius: 5 }}>
				<View style={{ flexDirection: 'column', flex: 1 }}>
					<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
						<View style={[styles.comparisonTextView]}>
							<Text style={styles.cardHeaderText}>{item.conceptName}</Text>
						</View>
						{/* ToDo Circle progress bar here  */}
						<View style={[styles.circleView, { position: 'relative' }]}>
							<ProgressBarComponent
								type={PROGRESS_BAR_TYPE.CIRCULAR}
								progressVal={item.weakness / 100}
								circleSize={60}
								shouldShowTextInsideCircle={false} />
							<View style={{
								width: 50, height: 50, position: 'absolute', top: 0, left: 0, zIndex: 999,
								alignItems: 'center', justifyContent: 'center'
							}}>
								<Text style={{ color: colors.PrimaryBlue }}>{item.weakness}%</Text>
							</View>
						</View>
						{/* ToDo Circle progress bar here  */}
					</View>
				</View>
			</View>
		</View>
	}
	render() {
		const { styles, weaknesslistItem, ChapterNameHeader } = this.state
		const heightValues = this.getHeightValue()
		return (
			<View style={styles.container}>
				<Animated.Image
					style={[
						styles.headerView,
						{
							height: 100,
							zIndex: 0
						}
					]}
					source={icons.SWOT_HEADER_ICON}
				/>
				<View
					style={[
						styles.headerStrip,
						{ paddingTop: Platform.OS === 'ios' ? heightValues : 0 }
					]}
				>
					<TouchableOpacity
						style={{
							position: 'absolute',
							top: Platform.OS === 'ios' ? heightValues : 0,
							left: 10,
							zIndex: 99999
						}}
						onPress={() => this.onBackPressed()}
					>
						<Animated.Image
							source={icons.BACK_WHITE_ICON}
							style={[
								styles.performanceIcon,
								{ marginRight: 10, marginTop: 6 },
								{
									height: 30,
									width: 30
								}
							]}
						/>
					</TouchableOpacity>
					<Animated.Text
						style={[
							styles.headerTestName,
							{
								fontSize: 24
							}
						]}
					>
						{ChapterNameHeader && ChapterNameHeader.length ? ChapterNameHeader : 'Weakness'}
					</Animated.Text>
				</View>
				<View style={{ flex: 1 }}>
					<ScrollView
						showsVerticalScrollIndicator={false}
						style={[styles.bottomView, { marginTop: Platform.OS === 'ios' ? 70 : 70 }]}>
						{weaknesslistItem && weaknesslistItem.length ?
							weaknesslistItem.map(item => {
								return this.renderComaprisonCardView(item)
							}) :
							null
						}
					</ScrollView>
				</View>
			</View>
		)
	}
}
