import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
  StyleSheet,
  View,
  Text,
  Platform,
  ScrollView,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  Animated,
  StatusBar,
  ActivityIndicator
} from 'react-native'
import {
  widthPercentage,
  icons,
  BIGGER_PHONE_CATEGORIZE,
  getBiggerCategorization,
  verticalScale,
  PROGRESS_BAR_TYPE,
  heightPercentage
} from '../../common'
import { colors } from '../../common-library/config'
import { navigateSimple, goBack } from '../../services'
import { TestAttemptStore } from '../../store'
import {  ProgressBarComponent, NoDataFoundComponent } from '../../components'
interface Props {
  navigation?: any
  testAttemptStore?: TestAttemptStore
}

interface State {
  styles?: any,
  selectedCharacter?: any,
  isLoading?: any,
  weaknessListData: any
}

@inject('testAttemptStore')
@observer
export class ResultSwotPage extends Component<Props, State> {
  animatedValue = new Animated.Value(0);
  _onBackPressed;
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      isLoading: true,
      weaknessListData: []
    };
    this._onBackPressed = this.onBackPressed.bind(this)
  }

  componentDidMount() {
    const { testAttemptStore } = this.props;
    testAttemptStore.getWeaknessList(testAttemptStore.testId, testAttemptStore.attemptId , testAttemptStore.courseId).then(() => {
      const swotList = testAttemptStore.getWeaknessListData();
        this.setState({
          weaknessListData: swotList,
          isLoading: false
        })
    });

    Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
  }
  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
  }
  onBackPressed() {
    const { navigation } = this.props
    return goBack(navigation)
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }
  getSelectedSwot() {
    const { selectedCharacter } = this.state
    const swotList = ['WEAKNESSES', 'STRENGTHS']
    if (selectedCharacter) {
      return selectedCharacter
    }
    if (swotList) {
      return swotList[0]
    }
  }

  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }
  // swotList, selectedSwot
  renderSwotList(swotList, selectedSwot) {
    const { styles } = this.state
    if (swotList && swotList.length > 0 ) {
      return swotList.map(mObj => (
        <TouchableOpacity
          style={{ flexDirection: 'row' }}
          key={mObj}
          onPress={() => this.setSelectedSubject(mObj)}
        >
          <Text style={[styles.subjectText, {
            fontWeight: selectedSwot === mObj ? '900' : '300'
          }]}>{mObj}</Text>
        </TouchableOpacity>
      ))
    } else {
      return null
    }
  }

  setSelectedSubject(subject) {
    const {testAttemptStore} = this.props
    const that = this
    that.setState({
      selectedCharacter: subject,
      weaknessListData: [],
      isLoading: true
    }, () => setTimeout(() => {
    const swotList = subject === 'STRENGTHS' ?  testAttemptStore.getStrenghtListData() : testAttemptStore.getWeaknessListData()
   this.setState({
     weaknessListData: swotList,
     isLoading: false

   })
    }, 500))
  }
  renderSwotView(swotList, selectedSwot) {
    return (
      <View
        style={{
          padding: 10,
          paddingLeft: 0,
          marginTop: Platform.OS === 'ios' ? 40 : 30,
          width: widthPercentage(100),
          flex: 1
        }}
      >
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={this.state.styles.submitButtonView}>
            {this.renderSwotList(swotList, selectedSwot)}
          </View>
        </ScrollView>
      </View>
    )
  }
  renderChapterCardView(dataItem) {
    const selectedSwot = this.getSelectedSwot()
    const colorVal = selectedSwot === 'WEAKNESSES' ? '#ed4242' : '#2d90f7'
    const { styles } = this.state
    return (
      <View style={[styles.parallaxView, { flexDirection: 'column' }]} key={dataItem.chapterName}>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}
            >
              <View style={[styles.comparisonTextView, {width: widthPercentage(100) - 110, paddingRight: 10}]}>
                <Text style={styles.cardHeaderText}>{dataItem.conceptName}</Text>
                <Text style={styles.cardSubHeaderText}>{dataItem.chapterName}</Text>
              </View>
              {/* ToDo Circle progress bar here  */}
              <View style={[styles.circleView, {position: 'relative', justifyContent: 'center'}]}>
              <View style={{justifyContent: 'center'}}>
               <ProgressBarComponent
               color={colorVal}
               unfilledColor={'#e9f4ff'}
              type={PROGRESS_BAR_TYPE.CIRCULAR}
              progressVal={dataItem.conceptWeakness / 100}
              circleSize={55}
              shouldShowTextInsideCircle={false} />
                <View style={{
                  width: 55, height: 55, position: 'absolute', top: 0, left: 0, zIndex: 999,
                   alignItems: 'center', justifyContent: 'center'
                }}>
                  <Text style={{ color: dataItem.weaknessColor, fontSize: 13 }}>{dataItem.conceptWeakness}%</Text>
                </View>
                </View>
              </View>
            </View>
          </View>
      </View>
    )
  }
  renderItemList() {
    const {styles, weaknessListData} = this.state
    return <View style={{ flex: 1 }}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={[
              styles.bottomView,
              { marginTop: Platform.OS === 'ios' ? 100 : 100 }
            ]}>
             {(weaknessListData && weaknessListData.length) ?
              weaknessListData.map((item, index) => {
                return this.renderChapterCardView(item)
              }) : this.renderNoDataView() }
          </ScrollView>
        </View>
  }
  showSelectedSwot() {
    const selectedSwot = this.getSelectedSwot()
    if (selectedSwot === 'WEAKNESSES') {
        return 'No Weaknesses found'
    } else {
      return 'No Strengths found'
    }
  }
  renderNoDataView() {
    const { isLoading } = this.state
      return isLoading ? <View style={{
        marginTop: heightPercentage(50) - 250, flex: 1, alignItems: 'center',
        justifyContent: 'center', padding: 10
      }}><ActivityIndicator size='large' />
      </View> : <View style={{
        height: heightPercentage(100) - 100, flex: 1, alignItems: 'center',
        justifyContent: 'center', padding: 10
      }}><Text style={{ fontSize: 20, color: colors.Black }}>{this.showSelectedSwot()} </Text>
        </View>
  }
  render() {
    const { styles, weaknessListData} = this.state
    const heightValues = this.getHeightValue()
    const swotList = ['WEAKNESSES' , 'STRENGTHS']
    const selectedSwot = this.getSelectedSwot()
    const imageBackgroundHeight = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [160, 50],
      extrapolate: 'clamp'
    })
    const imageBackgroundZIndex = this.animatedValue.interpolate({
      inputRange: [0, 40],
      outputRange: [0, 5],
      extrapolate: 'clamp'
    })
    return (
      <View style={styles.container}>
        <Animated.Image
          style={[ styles.headerView, { height: imageBackgroundHeight,
            zIndex: imageBackgroundZIndex }]}
          source={icons.SWOT_HEADER_ICON}/>
        <View style={[ styles.headerStrip, { paddingTop: Platform.OS === 'ios' ? heightValues : 0 }]}>
          <TouchableOpacity
            style={{ position: 'absolute', top: Platform.OS === 'ios' ? heightValues : 0, left: 10, zIndex: 99999 }}
            onPress={() => this.onBackPressed()}>
            <Animated.Image
              source={icons.BACK_WHITE_ICON}
              style={[
                styles.performanceIcon,
                { marginRight: 10, marginTop: 12 },
                {
                  height: 30,
                  width: 30
                }
              ]}
            />
          </TouchableOpacity>
          <Animated.Text style={[ styles.headerTestName, { fontSize: 24 }]}>SWOT</Animated.Text>
          <View
            style={{ position: 'absolute', top: 20, right: 10, zIndex: 999 }}
          >
           <Text style={{fontSize: 16, color: colors.White}}>ALL</Text>
          </View>
          <Animated.View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              top: 10,
              left: 10,
              opacity: 1
            }}
          >
            {this.renderSwotView(swotList, selectedSwot)}
          </Animated.View>
        </View>
        {this.renderItemList()}
      </View>
    )
  }
}

const getStyle = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: colors.White
    },
    headerView: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10
      // paddingBottom: 100
    },
    headerStrip: {
      position: 'absolute',
      flexDirection: 'row',
      width: widthPercentage(100),
      padding: 10,
      paddingBottom: 50,
      zIndex: 5
    },
    parallaxView: {
      borderWidth: 1,
      borderColor: colors.BorderColor,
      borderBottomWidth: 1,
      shadowColor: colors.ShadowColor,
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      elevation: 2,
      shadowRadius: 4,
      margin: 15,
      backgroundColor: colors.White,
      flex: 1,
      marginTop: 0,
      paddingLeft: 0,
      borderRadius: 5
    },
    bottomView: {
      flex: 1,
      marginTop: 115,
      flexDirection: 'column'
    },
    headerTestName: {
      fontSize: 24,
      color: colors.White,
      paddingTop: 10,
      paddingLeft: 40
      // lineHeight: 40
    },
    cardHeaderText: {
      color: '#000000',
      fontSize: 16,
      lineHeight: 24
    },
    cardSubHeaderText: {
      color: '#323232',
      fontSize: 13,
      lineHeight: 20
    },
    backIcon: {
      width: 30,
      height: 30
    },
    profileIcon: {
      width: 50,
      height: 50
    },
    submitButtonView: {
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    profileView: {
      flexDirection: 'column',
      paddingLeft: 15,
      paddingRight: 15,
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center'
    },
    subjectText: {
      color: colors.White,
      fontSize: 20,
      padding: 10
    },
    subjectMainView: {
      padding: 10,
      width: widthPercentage(100),
      flex: 1
    },
    cardQuestionView: {
      flexDirection: 'row',
      flex: 1,
      height: 100,
      marginLeft: -10,
      marginBottom: 10
    },
    comparisonTextView: {
      flexDirection: 'column',
      justifyContent: 'center'
    },
    rowView: {
      flexDirection: 'row',
      height: verticalScale(48),
      alignItems: 'center',
      backgroundColor: '#ffffff'
    },
    filterItemTextStyle: {
      paddingLeft: 15,
      color: '#666666',
      fontSize: 16
    },
    footerView: {
      marginTop: 10,
      flex: 1,
      flexDirection: 'row'
    },
    singleBtnStyle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.PrimaryBlue,
      padding: 15
    },
    buttonText: {
      color: 'white',
      textAlign: 'center',
      fontSize: 16
    },
    comparisonHeaderView: {
      flexDirection: 'row',
      paddingBottom: 10,
      justifyContent: 'space-between'
    },
    comparisonMaincontentView: {
      flexDirection: 'row',
      padding: 10,
      justifyContent: 'space-between'
    },
    circleTextView: {
      width: 40,
      height: 40,
      // borderRadius: 30,
      // borderWidth: 1,
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center'
    },
    textValueStyle: {
      color: colors.Black,
      fontWeight: 'bold',
      fontSize: 20
    },
    circleText: {
      color: colors.Black,
      fontWeight: 'bold'
    },
    modalView: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    overlayView: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: colors.Black,
      opacity: 0.6,
      zIndex: 99
    },
    mainContentView: {
      position: 'absolute',
      left: 0,
      top: 0,
      bottom: 0,
      right: 0,
      width: widthPercentage(100),
      zIndex: 100,
      backgroundColor: colors.White
    },
    tabTextStyle: {
      fontSize: 20,
      color: '#666666',
      fontWeight: 'bold'
    },
    submitButton: {
      backgroundColor: colors.PrimaryBlue,
      flexDirection: 'row',
      alignSelf: 'center',
      borderWidth: 0,
      padding: 10,
      paddingLeft: 30,
      paddingRight: 30,
      width: widthPercentage(70),
      justifyContent: 'center',
      borderRadius: 20
    },
    subjectButton: {
      backgroundColor: colors.White,
      flexDirection: 'row',
      alignSelf: 'center',
      padding: 7,
      paddingLeft: 30,
      paddingRight: 30,
      marginRight: 10,
      justifyContent: 'center',
      borderRadius: 5,
      borderColor: colors.Black,
      borderWidth: 1
    },
    submitTextStyle: {
      fontSize: 15,
      fontWeight: 'bold',
      color: colors.White,
      textAlign: 'center',
      lineHeight: 16
    },
    footerButtonView: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      zIndex: 9,
      width: widthPercentage(100),
      backgroundColor: colors.White,
      padding: 10
    }
  })
