import React, { Component } from 'react'
import {
  StyleSheet, View, Text, Image,
  Platform, Dimensions, BackHandler, ImageBackground,
  TouchableOpacity, Animated, ScrollView, FlatList, ActivityIndicator
} from 'react-native'
import { inject, Provider } from 'mobx-react'
import { join, debounce } from 'lodash'
import {
  heightPercentage, widthPercentage, icons, BIGGER_PHONE_CATEGORIZE, getBiggerCategorization
} from '../../common'
import { colors, dimens } from '../../common-library/config'
import { goBack, showSnackbar } from '../../services'
import { TestAttemptStore } from '../../store'
import { TestEngineQuestionParentComponent, SolutionQuestionParentComponent } from '../../components'
import { TextField } from 'react-native-material-textfield'
import MathJax from 'react-native-mathjax'

interface Props {
  navigation?: any
  testAttemptStore?: TestAttemptStore
  isModalVisible?: any
  isHowIsVisible?: any
  isSelected?: any
  isQuestionSelected?: any
  isAnswerSelected?: any
  blockHeight: number
  starCount?: any
}

interface State {
  styles?: any,
  isModalVisible?: any
  isHowIsVisible?: any
  isShowSolution?: boolean,
  selectedSectionIndex?: any,
  sections: any[],
  isLoading: boolean
  isQuestionSelected?: any
  isAnswerSelected?: any
  isSelected?: any
  blockHeight: number
  solution: any,
  isEnableSubmitButton?: boolean,
  answerTxt?: any,
  questionTxt?: any
  starCount?: any
  isWebViewReady?: boolean
}

const DROPDOWN_ITEM = {
  QUESTION: { key: ['selectAnswer'], title: 'Question' },
  ANSWER: { key: ['taggedconcept'], title: 'Answer' }
}

@inject('testAttemptStore')
export class SolutionDetailPage extends Component<Props, State> {
  onViewChangesDebounce
  scrollView
  currentQuestionIndex
  questionRef = undefined
  answerRef = undefined
  boundMarkAnswer
  _onBackPressed
  webViewStyle = `<style type="text/css">
  body{
    -webkit-user-select: none; /* Chrome all / Safari all */
    -moz-user-select: none; /* Firefox all */
    -ms-user-select: none; /* IE 10+ */
    user-select: none; /* Likely future */
  }
    html {
      overflow: scroll;
      background-color: white;
    }
    div.content{
      margin-top: -10px;
      margin-bottom: 10px;
      margin-left: -10px;
      margin-right: -20px;
      font-size: 18px,
      width: 100%;
      color: gray;
      padding: 15px
    }
    div.content > p {
      display: inline;
      font-size: 16px
    }
    .correctOption {
      margin: 2px;
      padding: 2px;
      border-radius: 4px;
      border: 1px solid #000000;
      font-size: 16px
    }
    .option {
      margin: 2px;
      padding: 2px;
      font-size: 16px
    }
    </style>`
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      styles: getStyle(),
      isShowSolution: false,
      isModalVisible: false,
      isHowIsVisible: false,
      starCount: 3.5,
      blockHeight: 0,
      selectedSectionIndex: 0,
      sections: props.testAttemptStore.sectionList.map(item => ({
        id: item.id,
        name: item.name,
        sectionId: item.sectionId,
        subSectionId: item.subSectionId
      })),
      isLoading: false,
      solution: '',
      isWebViewReady: false
    }
    this.onViewChangesDebounce = debounce(
      this.onViewableItemsChanged.bind(this),
      300
    )
    this.boundMarkAnswer = this.showRatePopup.bind(this)
    this._onBackPressed = this.onBackPressed.bind(this)
  }
  componentDidMount() {
    Dimensions.addEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.addEventListener('hardwareBackPress', this._onBackPressed)
    const { solution, index } = this.props.navigation.state.params
    this.currentQuestionIndex = index
    this.setState({ solution: solution })
  }
  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._orientationChangeListener.bind(this))
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed)
  }

  onBackPressed() {
    const { navigation } = this.props
    return goBack(navigation)
  }
  _orientationChangeListener() {
    this.setState({
      styles: getStyle()
    })
  }

  onLoad() {
    setTimeout(() => {
      this.setState({
        isWebViewReady: true
      })
    })
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    })
  }
  toggleHowIsModal = () => {
    this.setState({
      isHowIsVisible: !this.state.isHowIsVisible
    })
  }

  onLayoutParentEvent(event) {
    const { height } = event.nativeEvent.layout
    this.setState({
      blockHeight: height
    })
  }

  onQuestionClicked() {
    this.setState({ isQuestionSelected: !this.state.isQuestionSelected })
    this.setState({ isSelected: !this.state.isSelected })
  }
  onAnswerClicked() {
    this.setState({ isAnswerSelected: !this.state.isAnswerSelected })
    this.setState({ isSelected: !this.state.isSelected })
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    })
  }

  getQuestionHtml(text, index, options) {
    if (text) {
      // let start = text.indexOf('<span class="math-tex">')
      // let end = text.indexOf('</span>')
      // let test = text.substring(start, end)
      // test = 'You answered this question' + test.replace('/', '//')
      if (index) {
        text = `<b>Q${index}.</b> ${text}`
      } else {
        text = `${text}`
      }
      const htmlContent = `
            <div class="math-tex content" style="background-color: #ffffff;
            margin-left: -5, padding: 10px">${text}${options ? '<br />' + options : ''}</div>`
      return htmlContent
    }
    return null
  }

  getHTML(text, index, options) {
    if (text) {
      // let start = text.indexOf('<span class="math-tex">')
      // let end = text.indexOf('</span>')
      // let test = text.substring(start, end)
      // test = 'You answered this question' + test.replace('/', '//')
      const htmlContent = `
          <html>
          <head>
          ${this.webViewStyle}
          </head>
          <body>
          ${this.getQuestionHtml(text, index, options)}
          </body>
          </html>`
      return htmlContent
    }
    return null
  }

  getOptionClass(option, correctOptions) {
    const correctValues = correctOptions.filter(x => x.value === option.id)
    return correctValues.length ? 'correctOption' : 'option'
  }

  getOptions(rowData: any) {
    const correctOptions = rowData
    const optionArray = rowData
    let choicesHTML = ''
    if (optionArray && optionArray.length) {
      choicesHTML = join(optionArray.map(option =>
        `<div class='${this.getOptionClass(option, correctOptions)}'>
              <li style="margin:5px 0; padding: 20px 10px;">
              <div style=" display: inline-block; width: 100%; position: relative; vertical-align: text-top;">
              <div style=" display: block; width: 50px; height: 50px; border: 1px solid #e2e2e2;
              border-radius: 100%; position: absolute; top: -15px; left: -52px;
              z-index: 999;">
              </div>
        ${option.value.replace('<p>', '<p style="display:inline; padding-left: 20px;  font-size: 20px, font-weight: normal;font-style: normal">')}
                </div>
               </li>
            </div>`
      ), '')
      return `
            <div class='options'>
              <div style='margin-top: 5px; position:relative'>
          <ol type='A' style='font-weight: bold; font-size: 20px; vertical-align:text-top; position: absolute; top: 0; left: 0;
          z-index: 1000;'>
            ${choicesHTML}
          </ol>
              </div>
            </div>
          `
    }
    return ''
  }
  renderSolutionView(item) {
    const { isShowSolution, isWebViewReady } = this.state
    const { testAttemptStore} = this.props
    const answer = item.solutionContent
    const baseUrl = testAttemptStore.getFilesBaseUrl()
    // console.warn('item', item)
    if (isShowSolution) {
      if (answer && (answer.indexOf('<p>') !== -1) || (answer.indexOf('<p') !== -1)) {
        let htmQuestion = answer
        const scalesPageToFit = Platform.OS === 'android'
        if (!scalesPageToFit) {
          return <View style={{ flex: 1 }}>
          {!isWebViewReady ? <ActivityIndicator size='small' /> : null}
           <MathJax
            html={this.getHTML(htmQuestion, 0, '')}
            mathJaxOptions={{
              messageStyle: 'none',
              showMathMenu: false,
              extensions: ['tex2jax.js'],
              jax: ['input/TeX', 'output/HTML-CSS'],
              CommonHTML: { linebreaks: { automatic: true } },
              SVG: { linebreaks: { automatic: true } },
              'HTML-CSS': {
                linebreaks: {
                  automatic: true
                }
              },
              tex2jax: {
                inlineMath: [['$', '$'], ['\\(', '\\)']],
                displayMath: [['$$', '$$'], ['\\[', '\\]']],
                processEscapes: true,
                process: 'none',
                showMathMenu: false
              }
            }}
            heightChangeHandler={() => this.onLoad()}
            // messageHandler={(data) => this.messageHandler(data, currentQuestion.qId)}
            baseUrl={baseUrl}
          />
          </View>
        }
        return <View style={{ flex: 1 }}>
        {!isWebViewReady ? <ActivityIndicator size='small' /> : null}
        <MathJax
          html={this.getHTML(htmQuestion, 0, '')}
          mathJaxOptions={{
            messageStyle: 'none',
            showMathMenu: false,
            extensions: ['tex2jax.js'],
            jax: ['input/TeX', 'output/HTML-CSS'],
            CommonHTML: { linebreaks: { automatic: true } },
            SVG: { linebreaks: { automatic: true } },
            'HTML-CSS': {
              linebreaks: {
                automatic: true
              }
            },
            tex2jax: {
              inlineMath: [['$', '$'], ['\\(', '\\)']],
              displayMath: [['$$', '$$'], ['\\[', '\\]']],
              processEscapes: true,
              process: 'none',
              showMathMenu: false
            }
          }}
          heightChangeHandler={() => this.onLoad()}
          // messageHandler={(data) => this.messageHandler(data, currentQuestion.qId)}
           baseUrl={baseUrl}
        />
        </View>
      }
      return (<View style={{ flex: 1, height: heightPercentage(20) }}>
        <Text style={{ flex: 1, fontSize: 13, fontWeight: 'bold', alignSelf: 'flex-start' }}>
          {`Q${0}. ${answer ? answer : ''}`}
        </Text>
      </View>
      )
    }
    return null
  }

  renderComaprisonCardView() {
    const { styles, solution } = this.state
    // const { solution } = this.props.navigation.state.params
    return (<View style={styles.DefaulView}>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ marginTop: 12 }}>
          <TouchableOpacity onPress={() => this.onBackPressed()}>
            <Image
              source={icons.CROSSGREY_ICON}
              style={[styles.backIcon, {}]}
            />
          </TouchableOpacity>
        </View>
        <View style={{ justifyContent: 'space-between', marginLeft: 10, flexDirection: 'row', width: widthPercentage(100) - 90 }}>
          <View style={styles.comparisonTextView}>
            <Text style={styles.resultValue}>{solution.time}</Text>
            <Text style={styles.resultHeader}>Time Taken</Text>
          </View>
          {
            solution.difficultyLevel ?
              <View style={[styles.comparisonTextView, { justifyContent: 'flex-end' }]}>
                <Text style={styles.difficultyText}>{solution.difficultyLevel}</Text>
              </View> :
              null
          }
        </View>
      </View>
    </View>
    )
  }
  renderSkillLevelView() {
    const { styles } = this.state
    return (<View style={[styles.DefaulView, { padding: 5, paddingBottom: 20, paddingTop: 20 }]}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={styles.SkillTextView}>
          <Text style={styles.skillText}>Skill-Conceptual</Text>
        </View>
        <View style={styles.SkillTextView}>
          <Text style={styles.skillText}>Difficulty-Easy</Text>
        </View>
        <View style={[styles.SkillTextView, { borderRightWidth: 0 }]}>
          <Text style={styles.skillText}>Bohr's Theory</Text>
        </View>
      </View>
    </View>
    )
  }
  renderMarksOptain(item) {
    const { styles } = this.state
    if (item.marks < 0) {
      return <View style={styles.marksObtainedView}>
        <Text style={styles.marksObtainedText}>Marks Obtained</Text>
        <Text style={styles.marksObtainedValue}>{item.marks}(Negative Marks)</Text>
      </View>
    } else if (item.marks === 0) {
      return null
    } else {
      if (item.ispartial) {
        return <View style={styles.marksObtainedView}>
          <Text style={styles.marksObtainedText}>Marks Obtained</Text>
          <Text style={styles.marksObtainedValue}>{'+' + item.marks}(Partial Marks)</Text>
        </View>
      }
      return <View style={styles.marksObtainedView}>
        <Text style={styles.marksObtainedText}>Marks Obtained</Text>
        <Text style={styles.marksObtainedValue}>{'+' + item.marks}</Text>
      </View>
    }
  }
  renderCongratulationView(item) {
    const { styles } = this.state
    // console.warn('Item', item)
    if (item.isCorrect) {
      return <View style={{ justifyContent: 'center', padding: 20, flexDirection: 'row' }}>
        <Image
          source={icons.CONGRATULATIONS_ICON}
          style={styles.congratulationIconStyle}
        />
        <View style={{ flexDirection: 'column', paddingLeft: 10 }}>
          <Text style={styles.congratulationsText}>Congratulations</Text>
          <Text style={styles.marksObtainedText}>Your answer is correct</Text>
        </View>
      </View>
    } else if (!item.isAttempted) {
      return <View style={{ justifyContent: 'center', padding: 20, flexDirection: 'row' }}>
        <Image
          source={icons.UNATTEMPT_SMILE_ICON}
          style={styles.congratulationIconStyle}
        />
        <View style={{ flexDirection: 'column', paddingLeft: 10 }}>
          <Text style={styles.unattemptedText}>Unattempted Question!</Text>
          <Text style={styles.marksObtainedText}>Keep at it and you will improve</Text>
        </View>
      </View>
    } else {
      return <View style={{ justifyContent: 'center', padding: 20, flexDirection: 'row' }}>
        <Image
          source={icons.INCORRECT_ICON}
          style={styles.congratulationIconStyle}
        />
        <View style={{ flexDirection: 'column', paddingLeft: 10 }}>
          <Text style={styles.incorrectText}>Oops! Incorrect answer</Text>
          <Text style={styles.marksObtainedText}>Keep at it and you will improve</Text>
        </View>
      </View>
    }
  }
  renderFooterSolutionView(item) {
    const { styles, isShowSolution } = this.state
    const solutionBtnText = isShowSolution ? 'HIDE SOLUTION' : 'VIEW SOLUTION'
    return (<View style={{ width: widthPercentage(100) }}>
      <View style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between' }}>
        <View style={[styles.marksWrongView, { justifyContent: 'flex-start' }]}>
          {this.renderMarksOptain(item)}
        </View>
        <View style={styles.marksWrongView}>
          <TouchableOpacity onPress={() => this.submitReport()}>
            <Text style={styles.somethingWrongText}>Something Wrong?</Text>
          </TouchableOpacity>
        </View>
      </View>
      {this.renderCongratulationView(item)}
      <View style={{ width: widthPercentage(100) }}>
        <TouchableOpacity style={this.state.styles.viewsolutionButton}
          onPress={() => this.onShowSolutionBtnClick()}>
          <Text style={styles.viewsolutionTextStyle}>{solutionBtnText}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ width: widthPercentage(100), height: isShowSolution ? 200 : 0 }}>
        {this.renderSolutionView(item)}
      </View>
    </View>
    )
  }

  renderPrevButton() {
    const { styles, solution } = this.state
    const style = this.currentQuestionIndex > 0 ? styles.nextPreviousTextStyle : styles.nextPreviousGreyTextStyle
    if (this.currentQuestionIndex !== 0) {
      return <View style={{ borderRadius: 30, backgroundColor: colors.PrimaryBlue }}>
        <TouchableOpacity
          style={this.state.styles.nextPreviousButton}
          onPress={() => this.onPressPrev()}
        >
          <Text style={style}>{'Question ' + (solution.questionNo - 1)}</Text>
        </TouchableOpacity>
      </View>
    }
    return <View style={{ borderRadius: 30, backgroundColor: colors.Transparent }}>
      <TouchableOpacity
        style={this.state.styles.nextPreviousGreyButton}
      >
        <Text style={styles.nextPreviousGreyTextStyle}></Text>
      </TouchableOpacity>
    </View>
  }
  renderNextButton() {
    const { styles, solution } = this.state
    const { testAttemptStore } = this.props
    const solutionList = testAttemptStore.getSolutionsData()
    const style = this.currentQuestionIndex < solutionList.length ? styles.nextPreviousTextStyle : styles.nextPreviousGreyTextStyle
    if (this.currentQuestionIndex < solutionList.length - 1) {
      return <View style={{ borderRadius: 30, backgroundColor: colors.PrimaryBlue }}>
        <TouchableOpacity
          style={this.state.styles.nextPreviousButton}
          onPress={() => this.onPressNext()}
        >
          <Text style={style}>{'Question ' + (solution.questionNo + 1)}</Text>
        </TouchableOpacity>
      </View>
    }
    return null
    // return <TouchableOpacity
    //   style={this.state.styles.nextPreviousGreyButton}>
    //   <Text style={styles.nextPreviousGreyTextStyle}>Next</Text>
    // </TouchableOpacity>
  }
  renderFooterView() {
    const { testAttemptStore } = this.props
    const { styles } = this.state
    return <View style={styles.footerButtonView}>
      <View style={this.state.styles.nextPreviousButtonView}>
        {this.renderPrevButton()}
        {this.renderNextButton()}
      </View>
    </View>
  }

  onPressNext() {
    const { testAttemptStore } = this.props
    const solutionList = testAttemptStore.getSolutionsData()
    if (this.currentQuestionIndex < solutionList.length - 1) {
      this.currentQuestionIndex++
      this.setState({
        solution: solutionList[this.currentQuestionIndex],
        isShowSolution: false,
        isWebViewReady: false
      })
    }
  }
  onPressPrev() {
    const { testAttemptStore } = this.props
    const solutionList = testAttemptStore.getSolutionsData()
    if (this.currentQuestionIndex > 0) {
      this.currentQuestionIndex--
    }
    this.setState({
      solution: solutionList[this.currentQuestionIndex],
      isShowSolution: false,
      isWebViewReady: false
    })
  }
  onShowSolutionBtnClick() {
    this.setState({
      isShowSolution: !this.state.isShowSolution
    })
    if (!this.state.isShowSolution) {
      this.setState({
        isWebViewReady: false
      })
    }
  }

  getHeightValue() {
    let heightValue = 20
    switch (getBiggerCategorization()) {
      case BIGGER_PHONE_CATEGORIZE.BIGGER_SERIES_2:
        heightValue = 40
        break
      default:
        break
    }
    return heightValue
  }
  onViewableItemsChanged = ({ viewableItems, changed }) => {
    const indices = viewableItems.map(item => item.index)
    let currentIndex
    let prevIndex
    if (indices.length === 1) {
      currentIndex = indices[0]
    } else {
      currentIndex = indices[1]
    }
    const { testAttemptStore } = this.props
    const { sections } = this.state
    this.currentQuestionIndex = currentIndex - 1
    const sectionInfo = testAttemptStore.setCurrentQuestionByIndex(
      this.currentQuestionIndex
    )
    const index = sections.findIndex(item => {
      if (item.subSectionId) {
        return item.subSectionId === sectionInfo.subSectionId
      }
      return item.sectionId === sectionInfo.sectionId
    })
    if (index > -1) {
      this.setState({
        selectedSectionIndex: index
      })
    }
  }
  renderMainContentInnerView() {
    const { testAttemptStore } = this.props
    const { isLoading, styles } = this.state
    const questions = testAttemptStore.getSolutionsData()
    if (!questions || !questions.length) {
      return null
    }
    return (
      <View style={[styles.contentView, { padding: 0 }]}>
        <View
          style={{
            backgroundColor: colors.White,
            borderBottomWidth: 1,
            borderBottomColor: colors.GrayUnderLine
          }}
        >
        </View>
        <FlatList
          ref={ref => {
            testAttemptStore.flatList = ref
          }}
          onViewableItemsChanged={this.onViewChangesDebounce}
          data={questions}
          renderItem={item => this.renderQuestionItem(item)}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          key='flatlist'
          style={{ height: '100%' }}
          decelerationRate={0.2}
          pagingEnabled={true}
          keyExtractor={(item, index) => item.qId}
          scrollEventThrottle={14}
          getItemLayout={this.layout}
        />
      </View>
    )
  }

  renderQuestionRadioButton() {
    const { isQuestionSelected } = this.state
    return <View style={{
      width: 30, height: 30, borderRadius: 20,
      backgroundColor: isQuestionSelected ? colors.Green : '#F7F7F7', borderWidth: isQuestionSelected ? 0 : 1,
      borderColor: '#D7D7D7', alignItems: 'center', justifyContent: 'center'
    }}>
      {!isQuestionSelected ? null :
        <Image source={icons.CORECT_ICON_WHITE}
          style={{
            width: 15, height: 15, alignSelf: 'center', alignItems: 'center'
          }}
        />
      }
    </View>
  }

  renderAnswerRadioButton() {
    const { isAnswerSelected } = this.state
    return <View style={{
      width: 30, height: 30, borderRadius: 20,
      backgroundColor: isAnswerSelected ? colors.Green : '#F7F7F7', borderWidth: isAnswerSelected ? 0 : 1,
      borderColor: '#D7D7D7', alignItems: 'center', justifyContent: 'center'
    }}>
      {!isAnswerSelected ? null :
        <Image source={icons.CORECT_ICON_WHITE}
          style={{
            width: 15, height: 15, alignSelf: 'center', alignItems: 'center'
          }}
        />
      }
    </View>
  }

  renderQuestionTabItem(type) {
    const { styles } = this.state
    const { isQuestionSelected } = this.state
    return (
      <View style={styles.rowView}>
        <View style={{
          flexDirection: 'row', flex: 1, padding: 10, backgroundColor: colors.White,
          justifyContent: 'space-between'
        }}>
          <Text style={styles.tabTextStyle}>{type.title}</Text>
          {this.renderQuestionRadioButton()}
        </View>
        <View style={{ flex: 1 }}>
          {!isQuestionSelected ? null :
            <View style={{ marginTop: 0, marginBottom: 0 }}>
              <TextField
                placeholder={'Suggest edit...'}
                multiline={true}
                lineWidth={0}
                containerStyle={styles.textContainerStyle}
                inputContainerStyle={styles.textInputMultilineStyle}
                titleTextStyle={styles.inputTextStyle}
                onChangeText={questionTxt => [this.setState({ questionTxt: questionTxt })]}
                ref={question => { this.questionRef = question }}
              />
            </View>
          }
        </View>
      </View>
    )
  }

  renderAnswerTabItem(type) {
    const { styles } = this.state
    const { isAnswerSelected } = this.state
    return (
      <View style={styles.rowView}>
        <View style={{
          flexDirection: 'row', flex: 1, padding: 10, backgroundColor: colors.White,
          justifyContent: 'space-between'
        }}>
          <Text style={styles.tabTextStyle}>{type.title}</Text>
          {this.renderAnswerRadioButton()}
        </View>
        <View style={{ flex: 1 }}>
          {!isAnswerSelected ? null :
            <View style={{ marginTop: 0, marginBottom: 0 }}>
              <TextField
                placeholder={'Suggest edit...'}
                multiline={true}
                lineWidth={0}
                underlineColorAndroid={colors.Transparent}
                containerStyle={styles.textContainerStyle}
                inputContainerStyle={styles.textInputMultilineStyle}
                titleTextStyle={styles.inputTextStyle}
                onChangeText={answerTxt => [this.setState({ answerTxt: answerTxt })]}
                ref={answer => { this.answerRef = answer }}
              />
            </View>
          }
        </View>
      </View>
    )
  }

  renderModal = () => {
    if (this.state.isModalVisible) {
      const { styles, isEnableSubmitButton } = this.state
      return <View style={this.state.styles.modalView}>
        <TouchableOpacity style={styles.overlayView} onPress={() => this.toggleModal()}></TouchableOpacity>
        <View onLayout={(event) => this.onLayoutParentEvent(event)} style={[styles.mainContentView,
        { top: heightPercentage(43) - (this.state.blockHeight / 2) }]}>
          <Text style={[styles.tabTextStyle, { fontSize: 23, textAlign: 'center' }]}>Something Wrong?</Text>
          <TouchableOpacity style={{
            width: 25, height: 25,
            position: 'absolute', top: 10, right: 10, zIndex: 101
          }} onPress={() => this.toggleModal()}>
            <Image
              source={icons.CROSSGREY_ICON}
              style={{ width: 25, height: 25 }}
            />
          </TouchableOpacity>
          <View style={{
            width: widthPercentage(100) - 40, justifyContent: 'center', alignItems: 'center',
            padding: 20, flexDirection: 'column'
          }}>
            <TouchableOpacity onPress={() => this.onQuestionClicked()}>
              {this.renderQuestionTabItem(DROPDOWN_ITEM.QUESTION)}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onAnswerClicked()}>
              {this.renderAnswerTabItem(DROPDOWN_ITEM.ANSWER)}
            </TouchableOpacity>
          </View>
          <View style={this.state.styles.submitButtonView}>
            <TouchableOpacity
              disabled={isEnableSubmitButton}
              style={this.state.styles.submitButton}
              onPress={() => this.submitReport()}
            >
              <Text style={styles.submitTextStyle}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    } else {
      return null
    }
  }
  submitReport() {
    const { testAttemptStore } = this.props
    const { solution } = this.state
    testAttemptStore.submitQuestionAsReport(testAttemptStore.testId, testAttemptStore.attemptId, solution.questionId, true)
  }

  renderHowisSolution = () => {
    if (this.state.isHowIsVisible) {
      const { styles } = this.state
      return <View style={this.state.styles.modalView}>
        <TouchableOpacity style={styles.overlayView} onPress={() => this.toggleHowIsModal()}></TouchableOpacity>
        <View onLayout={(event) => this.onLayoutParentEvent(event)} style={[styles.mainContentView,
        { top: heightPercentage(43) - (this.state.blockHeight / 2) }]}>
          <Text style={[styles.tabTextStyle, { fontSize: 26, textAlign: 'center' }]}>How is the Solution?</Text>
          <TouchableOpacity style={{
            width: 25, height: 25,
            position: 'absolute', top: 20, right: 10, zIndex: 101
          }} onPress={() => this.toggleHowIsModal()}>
            <Image
              source={icons.CROSSGREY_ICON}
              style={{ width: 25, height: 25 }}
            />
          </TouchableOpacity>
          <View style={{
            width: widthPercentage(100) - 40, justifyContent: 'center', alignItems: 'center',
            padding: 20, flexDirection: 'column'
          }}>
            <Image
              source={icons.SMILEY_ICON}
              style={{ width: 70, height: 70 }}
            />
            <Text style={styles.okTextStyle}>OK, OK</Text>
          </View>
          <View style={{
            flex: 1, paddingLeft: 20, backgroundColor: '#F7F5F5', paddingRight: 20, borderWidth: 1, borderColor: colors.GrayUnderLine,
            borderRadius: 80
          }}>
            <StarRating
              disabled={false}
              fullStarColor={'#FFD983'}
              emptyStarColor={'#C4C4C4'}
              starSize={25}
              starStyle={{
                padding: 10, borderWidth: 1, backgroundColor: '#ffffff',
                marginLeft: 5, marginRight: 5, borderColor: colors.LightGrey, borderRadius: 50
              }}
              maxStars={5}
              rating={this.state.starCount}
              selectedStar={(rating) => this.onStarRatingPress(rating)}
            />
          </View>
          <View style={styles.okSubmitButtonView}>
            <TouchableOpacity
              onPress={() => this.toggleHowIsModal()}>
              <Text style={styles.okTextStyle}>OK</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.onPressSubmitRating()}>
              <Text style={styles.rateSubmitText}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    } else {
      return null
    }
  }
  onPressSubmitRating() {
    const { testAttemptStore } = this.props
    const { solution, starCount } = this.state
    if (starCount > 0) {
      testAttemptStore.giveSolutionRating(
        testAttemptStore.testId,
        testAttemptStore.attemptId,
        solution.questionId, true, starCount)
    } else {
      showSnackbar('please rate the solution')
    }
  }
  showRatePopup() {
    this.setState({
      isHowIsVisible: !this.state.isHowIsVisible
    })
  }
  renderQuestionItem(item) {
    const { testAttemptStore } = this.props
    return (<View style={{ width: widthPercentage(100) }}>
      <View style={{ width: widthPercentage(100) }}>
        <SolutionQuestionParentComponent
          markAnswer={this.boundMarkAnswer}
          questionSolution={item}
          baseUrl={testAttemptStore.getFilesBaseUrl()}
        />
      </View>
      <View style={{ width: widthPercentage(100), paddingBottom: 20 }}>
        {this.renderFooterSolutionView(item)}
      </View>
    </View>)
    return null
  }
  layout = (data, index) => {
    const widthScreen = widthPercentage(100)
    return { length: widthScreen, offset: widthScreen * index, index }
  }
  render() {
    const { styles, solution, isShowSolution } = this.state
    const heightValues = this.getHeightValue()
    return <View style={[styles.container, { paddingTop: Platform.OS === 'ios' ? heightValues : 0, position: 'relative' }]}>
      <ScrollView style={{ height: heightPercentage(100), marginBottom: 80 }}
        ref={ref => this.scrollView = ref}
        onContentSizeChange={(contentWidth, contentHeight) => {
          if (isShowSolution) {
            this.scrollView.scrollToEnd({ animated: true })
          }
        }}
      >
        {this.renderComaprisonCardView()}
        <View style={styles.conceptQuestioView}>
          <View style={styles.conceptView}>
            <Text style={{ color: colors.White }}>{solution.questionType}</Text>
          </View>
          {solution.conceptsIncluded ?
            <View style={[styles.conceptView, { width: widthPercentage(100) - 120, alignItems: 'flex-end', backgroundColor: colors.White }]}>
              <Text numberOfLines={1} style={{ color: colors.Gray, fontSize: 16, textAlign: 'right' }}>{solution.conceptsIncluded}</Text>
            </View> : null
          }
        </View>
        {this.renderQuestionItem(solution)}
      </ScrollView>
      {/* {this.renderModal()} */}
      {this.renderHowisSolution()}
      {this.renderFooterView()}
    </View>
  }
}

const getStyle = () => StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.White
  },
  DefaulView: {
    backgroundColor: colors.White,
    width: widthPercentage(100),
    padding: 20,
    paddingBottom: 0
  },
  resultHeader: {
    color: '#999999',
    fontSize: 13,
    lineHeight: 20
  },
  resultValue: {
    color: '#000000',
    fontSize: 22,
    lineHeight: 30
  },
  skillText: {
    color: colors.LightGrey,
    fontSize: 12,
    lineHeight: 17
  },
  backIcon: {
    width: 30,
    height: 30
  },
  submitButtonView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  comparisonTextView: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  SkillTextView: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRightWidth: 1,
    borderRightColor: colors.Black,
    width: widthPercentage(33.3333)
  },
  marksObtainedView: {
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  marksObtainedText: {
    color: colors.LightGrey,
    fontSize: 16
  },
  marksObtainedValue: {
    color: colors.Black,
    fontSize: 16,
    padding: 5
  },
  marksWrongView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: widthPercentage(50),
    paddingRight: 20
  },
  somethingWrongText: {
    color: '#1D7DEA',
    fontSize: 16
  },
  congratulationsText: {
    color: colors.GreenProgressBarColor,
    fontSize: 18
  },
  unattemptedText: {
    color: colors.GraphOrange,
    fontSize: 18
  },
  incorrectText: {
    color: colors.LightRed,
    fontSize: 18
  },
  congratulationIconStyle: {
    width: 50,
    height: 50
  },
  viewsolutionButtonView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 20
  },
  viewsolutionButton: {
    backgroundColor: colors.White,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1,
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30,
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: colors.PrimaryBlue
  },
  viewsolutionTextStyle: {
    fontSize: 16,
    color: colors.PrimaryBlue,
    textAlign: 'center',
    lineHeight: 18
  },
  nextPreviousButtonView: {
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  nextPreviousButton: {
    backgroundColor: colors.White,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: 15,
    paddingLeft: 30,
    paddingRight: 30,
    justifyContent: 'center',
    borderRadius: 30,
    margin: 1
  },
  nextPreviousTextStyle: {
    fontSize: 18,
    color: colors.PrimaryBlue,
    textAlign: 'center',
    lineHeight: 20
  },
  nextPreviousGreyButton: {
    backgroundColor: colors.White,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: 15,
    paddingLeft: 30,
    paddingRight: 30,
    justifyContent: 'center',
    borderRadius: 30,
    margin: 1
  },
  nextPreviousGreyTextStyle: {
    fontSize: 18,
    color: colors.GrayUnderLine,
    textAlign: 'center',
    lineHeight: 20
  },
  textContainerStyle: {
    marginBottom: -10,
    paddingBottom: 20,
    backgroundColor: '#F8F7F7'
  },
  textInputStyle: {
    flex: 1,
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 18,
    padding: 8,
    paddingLeft: 0,
    borderRadius: 6,
    height: 40,
    paddingTop: 8,
    paddingBottom: 8
  },
  textInputMultilineStyle: {
    flex: 1,
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 18,
    padding: 8,
    paddingLeft: 0,
    borderRadius: 6,
    paddingTop: 8,
    paddingBottom: 8,
    backgroundColor: '#F8F7F7'
  },
  inputTextStyle: {
    fontSize: 12,
    fontWeight: '500',
    lineHeight: 24
  },
  modalView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  overlayView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: colors.Black,
    opacity: 0.6,
    zIndex: 99
  },
  mainContentView: {
    position: 'absolute',
    left: 0,
    zIndex: 100,
    padding: 20,
    paddingTop: 10,
    backgroundColor: colors.White
  },
  rowView: {
    width: widthPercentage(100) - 30,
    margin: 15,
    backgroundColor: colors.LightGrayOpacity50,
    borderRadius: 5,
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderColor: colors.BorderColor,
    borderTopWidth: dimens.size0,
    shadowColor: colors.ShadowColor,
    shadowOffset: { width: dimens.size0, height: dimens.size2 },
    shadowOpacity: 0.2,
    elevation: dimens.size2,
    shadowRadius: dimens.size2
  },
  tabTextStyle: {
    fontSize: 20,
    color: '#666666',
    fontWeight: 'bold'
  },
  submitButton: {
    backgroundColor: colors.PrimaryBlue,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0,
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30,
    width: widthPercentage(70),
    justifyContent: 'center',
    borderRadius: 20
  },
  subjectButton: {
    backgroundColor: colors.White,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: 7,
    paddingLeft: 30,
    paddingRight: 30,
    marginRight: 10,
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: colors.Black,
    borderWidth: 1
  },
  submitTextStyle: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.White,
    textAlign: 'center',
    lineHeight: 16
  },
  rateSubmitText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.PrimaryBlue,
    textAlign: 'center',
    lineHeight: 24,
    margin: 10
  },
  okTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#999999',
    textAlign: 'center',
    lineHeight: 24,
    margin: 10
  },
  footerButtonView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 9,
    width: widthPercentage(100),
    backgroundColor: colors.White,
    padding: 10
  },
  okSubmitButtonView: {
    flex: 1,
    marginTop: 20,
    marginBottom: 20,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  difficultyText: {
    fontSize: 14,
    color: '#4A4A4A',
    lineHeight: 16,
    marginRight: 10
  },
  conceptQuestioView: {
    flex: 1,
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: 10,
    flexDirection: 'row',
    padding: 10
  },
  conceptView: {
    backgroundColor: colors.Gray,
    borderRadius: 15,
    alignItems: 'center',
    padding: 5,
    paddingBottom: 2,
    paddingTop: 2
  }
})
