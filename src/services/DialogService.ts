import Stores from '../store'

export function showDialog(navigation , titleText ,  descriptionText,
  buttonLabel, imageUrl, buttonClick, cancelButtonVisible , hideDialogOnTouchOutside, onCancelClicked = () => {/**/}) {
  Stores.dialogStore.showDialog(navigation, titleText , descriptionText,
    buttonLabel, imageUrl, buttonClick, cancelButtonVisible , hideDialogOnTouchOutside, onCancelClicked)
}

export function hideDialog() {
  Stores.dialogStore.hideDialog()
}

export function setNavigationObject(navigation) {
  this.navigation = navigation
}