import Stores from '../store'

export function showGoalDropDown(navigation, currentContext, touchType, componentX, isHorizontal, dataSource, keyOfText, disabledDataSource ) {
  Stores.goalsDataStore.showGoalDropDown(navigation, currentContext, touchType, componentX, isHorizontal, dataSource, keyOfText, disabledDataSource)
}