import RNFetchBlob from 'react-native-fetch-blob'

const fs = RNFetchBlob.fs

const APP_FOLDER = fs.dirs.SDCardApplicationDir

async function createFolder(folder) {
  try {
    const fileExists = await fs.exists(folder)
    if (!fileExists) {
      const response = await fs.mkdir(folder)
    }
  } catch (error) {
    // gobbling up already exists error, fs.exists doesn't seem to be working
    // console.warn('Folder already exists.')
  }
}

async function prepareFileName(uri, testId, assignmentId) {
  const decodedUri = decode(uri)
  const testKey = testId + (!assignmentId ? undefined : assignmentId)
  const folder = `${APP_FOLDER}/${testKey}`
  await createFolder(folder)
  const fileName = decodedUri.replace('https://question-uploader.s3.us-west-2.amazonaws.com/TestPapers/', '')
  const newFileName = `${folder}/${fileName}`
  const fileParts = newFileName.split('/')
  const file = fileParts[fileParts.length - 1]
  return {
    folder,
    file
  }
}

function decode(val) {
  return decodeURIComponent(val.replace(/\+/g,  ' '))
}

export async function downloadImage(uri, testId, assignmentId) {
  let imagePath = null
  await createFolder(APP_FOLDER)
  const imagePathDetail = await prepareFileName(uri, testId, assignmentId)
  try {
    const finalImagePath = `${imagePathDetail.folder}/${imagePathDetail.file}`
    const fileExists = await fs.exists('file://' + finalImagePath)
   // console.warn('Downloading image at:', finalImagePath)
    if (!fileExists) {
      const resp = await RNFetchBlob.config({
        useDownloadManager: true,
        path: finalImagePath
      }).fetch('GET', uri, {
        'Cache-Control': 'no-store'
      })
    }
  } catch (err) {
    console.warn(err)
    // console.warn('Failed to download image.')
  }
  return imagePathDetail
}
