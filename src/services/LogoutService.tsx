import React from 'react'
import { Alert, Platform } from 'react-native'

import { handleSignOut, cleanStores, setUserId } from '../utils'
import { resetRouterSimple } from '../services';

export function logout(navigation: any, isAutoLogin: boolean) {
	if (isAutoLogin) {
		logoutTransition(navigation);
		return
	}
	logoutTransitionPopup(navigation)
}

function logoutTransition(navigation: any) {
	handleSignOut();
	cleanStores();
	resetRouterSimple(navigation, 0, 'SignupPage')
}

function logoutTransitionPopup(navigation: any) {
	Alert.alert(
		'Logout',
		'Are you sure you want to logout ?',
		[
			{ text: 'Cancel' },
			{ text: 'Yes', onPress: () => logoutTransition(navigation) }
		],
		{ cancelable: false }
	)
}
