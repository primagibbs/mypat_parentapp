import Stores from '../store'

export function showModal(element, modalOptions) {
  Stores.customModalStore.showModal(element, modalOptions);
}

export function hideModal() {
  Stores.customModalStore.hideModal();
}