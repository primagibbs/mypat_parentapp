import FCM from 'react-native-fcm'
import { icons } from '../common'

function formatDate(date) {
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let ampm = hours >= 12 ? 'pm' : 'am'
    hours = hours % 12
    hours = hours ? hours : 12 // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes
    const strTime = hours + ':' + minutes
    return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()} ${strTime}`
  }

export const createNotification = ({ title = '', text, time = null, sound = null, id = null, progress = null }) => {
   // console.warn('##### ID ai kya?', id)
    const notificationId = id || new Date().valueOf().toString()
   // console.warn('##### Notification Id', notificationId)
    const options: any = {
        channel: 'default',
        id: notificationId,
        title,
        body: text, // as FCM payload (required)
        // sound: 'bell.mp3', // "default" or filename
        priority: 'max', // as FCM payload
        // click_action: 'com.myapp.MyCategory', // as FCM payload - this is used as category identifier on iOS.
        badge: 10, // as FCM payload IOS only, set 0 to clear badges
        number: 10, // Android only
        // ticker: 'My Notification Ticker', // Android only
        // auto_cancel: true, // Android only (default true)
        // large_icon:
        //   'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg', // Android only
        // icon: 'ic_launcher', // as FCM payload, you can relace this with custom icon you put in mipmap
        // big_text: 'Show when notification is expanded', // Android only
        // sub_text: 'Downloading ', // Android only
        color: 'black', // Android only
        vibrate: 500, // Android only default: 300, no vibration if you pass 0
        wake_screen: true, // Android only, wake up screen when notification arrives
        group: 'group', // Android only
        // picture:
        // 'https://mypat-v2-prod.s3.amazonaws.com/webcontent/images/mypat-logo.png', // Android only bigPicture style
        // ongoing: true, // Android only
        // my_custom_data: 'my_custom_field_value', // extra data you want to throw
        lights: true, // Android only, LED blinking (default false)
        show_in_foreground: false, // notification when app is in foreground (local & remote)
        local_only: false
    }
    if (progress) {
        options.progress = progress
        options.progress_max = 100

    }
    FCM.presentLocalNotification(options)
    return notificationId
}

export const deleteLocalNotification = (id) => {
    FCM.cancelLocalNotification(id)
}
