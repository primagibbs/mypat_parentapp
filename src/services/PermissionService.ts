import { PermissionsAndroid } from 'react-native'

async function requestPermission(permission, title, message) {
  try {
    const hasPermission = await PermissionsAndroid.check(permission)
    if (hasPermission) {
      return true
    }
    const granted = await PermissionsAndroid.request(
      permission,
      {
        title,
        message
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.warn(err)
    return false
  }
}

export async function requestWriteStoragePermission() {
  return await requestPermission(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    'myPAT App Download Premission',
    'myPAT app needs to write to your storage to offer offine experience'
  )
}

export async function requestReadPhoneStatePermission() {
  return await requestPermission(
    PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
    'myPAT App Read Phone State',
    'myPAT app needs to read phone state for debugging'
  )
}