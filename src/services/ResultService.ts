import Stores from '../store'

export function showResultScreen(navigation, testType, assignmentId, testName, attempts, courseId, testId, tabType = undefined, isAssignment) {
  Stores.resultStore.showResultScreen(navigation, testType, assignmentId, testName, attempts, courseId, testId, tabType, isAssignment)
}