export * from './DialogService'
export * from './ResultService'
export * from './Listeners'
export * from '../common-library/services'
export * from './LogoutService'
export * from './GoalDropDownService'
export * from './NotificationService'
export * from './ModalService'

import { isNcrptype, getShowTabType} from '../utils'
import { networkConfig } from '../common-library/configuration'
import CacheFunctions from '../persistence/utils/index'

networkConfig
.setNcrpProvider(isNcrptype)
.setTabTypeProvider(getShowTabType)
.setInitialRoute(() => 'SignupPage')
.setCacheProvider(() => CacheFunctions);
