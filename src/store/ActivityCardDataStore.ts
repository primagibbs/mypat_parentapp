import { action, observable } from 'mobx';
import { BaseRequest } from '../http-layer';
import { API_IDS } from '../common'
import { NetworkCallbacks, DashboardNetworkModule } from '../api-layer';

let colors = {
	userAccuracy: '#2E93FB',
	userSpeed: '#2E93FB',
	userScore: '#2E93FB',
	topperAccuracy: '#9F51CF',
	topperSpeed: '#9F51CF',
	topperScore: '#9F51CF',
	avgAccuracy: '#95BE42',
	avgSpeed: '#95BE42',
	avgScore: '#95BE42',
};

let detailsColors = {
	userAccuracy: '#FFFFFF',
	userSpeed: '#FFFFFF',
	userScore: '#FFFFFF',
	topperAccuracy: '#95BE42',
	topperSpeed: '#95BE42',
	topperScore: '#95BE42',
	avgAccuracy: '#FBD249',
	avgSpeed: '#FBD249',
	avgScore: '#FBD249',
}

let seriesNameMap = {
	userAccuracy: 'Your child',
	userSpeed: 'Your child',
	userScore: 'Your child',
	topperAccuracy: 'Batch topper',
	topperSpeed: 'Batch topper',
	topperScore: 'Batch topper',
	avgAccuracy: 'Batch average',
	avgSpeed: 'Batch average',
	avgScore: 'Batch average',
};

const DEFAULT_SETTING = {
	activityData: [],
	defaultYAxis: 'scorePercentage',
	selectedYAxis: '',
	subject: 'all',
	defaultDataSource: undefined,
	seriesNameMap: seriesNameMap,
	colorMap: colors,
	isChartLoading: false,
	activityTabs: [{
		id: 'scorePercentage',
		suffix: 'Score',
		label: 'Score'
	}, {
		id: 'accuracy',
		suffix: 'Accuracy',
		label: 'Accuracy Rate'
	}, {
		id: 'speed',
		suffix: 'Speed',
		label: 'Speed'
	}],
	detailsColors: detailsColors,
	subjects: [{
		id: 'all',
		label: 'All Subjects'
	}, {
		id: 'mathematics',
		label: 'Mathematics'
	}, {
		id: 'physics',
		label: 'Physics'
	}, {
		id: 'chemistry',
		label: 'Chemistry'
	}]
};

export class ActivityCardDataStore implements NetworkCallbacks {

	@observable activityData: any[];
	@observable defaultYAxis: string;
	@observable selectedYAxis: string;
	@observable subject: string;
	@observable defaultDataSource: any;
	@observable isChartLoading: boolean
	colorMap: any;
	detailsColors: any;
	seriesNameMap: any;
	activityTabs: any[];
	subjects: any[];

	constructor() {
		this.init();
	}

	@action
	init() {
		Object.keys(DEFAULT_SETTING).forEach((key) => {
			// @ts-ignore
			this[key] = DEFAULT_SETTING[key];
		});
	}

	@action
	setActivityCardData(data: any) {
		this.activityData = data;
		this.isChartLoading = false;
	}

	@action
	onSuccess(apiId: any, response: any): void {
		this.navigatePage(apiId, response);
	}

	navigatePage(apiId: any, response: any) {
		// console.warn('TEST SCORE DATA', JSON.stringify(response, null, 2))
		switch (apiId) {
			case API_IDS.ACTIVITY_CARD_DATA:
				this.setActivityCardData(response.data);
				break;
			default:
				break
		}
	}

	@action
	async fetchActivityCard(studentId: any, goalId: any) {
		try {
			const config = { 'methodType': 'GET' };
			const getDashboardDataModule = new DashboardNetworkModule(config, this);
			this.isChartLoading = true;
			await getDashboardDataModule.fetchActivityCardData(studentId, goalId);
		} catch (e) {
			console.log(e);
		}
	}

	onSuccessBadRequest(apiId: string, response: any) {
		this.isChartLoading = false;
		console.log('bad', apiId);
	}

	onSuccessUnAuthorized(apiId: any, response: any) {
		//
		this.isChartLoading = false;
	}

	onFailure(apiId: string, request: BaseRequest) {
		// TODO OnFailure Implementation
		this.isChartLoading = false;
		// console.log('fail', apiId);
	}

	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: any): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		// TODO generalValidationError Implementation
	}

	onComplete() {
		// TODO onComplete Implementation
	}

	getDefaultDataSource() {
		return this.defaultDataSource ? this.defaultDataSource.slice() : undefined;
	}
}
