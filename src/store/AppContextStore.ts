import { observable, action, toJS } from 'mobx'

const DEFAULTS = {
  targetExams: []
}

export class AppContextStore {
  @observable targetExams
  @observable targetExamSelection

  constructor() {
    this.init()
  }

  @action
  init() {
    this.targetExams = DEFAULTS.targetExams
  }

  getTargetExamSelection() {
    return undefined
  }
}
