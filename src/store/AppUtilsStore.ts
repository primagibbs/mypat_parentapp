import { observable, action, toJS } from 'mobx'
import { NetworkCallbacks } from '../api-layer'
import { BaseRequest } from '../http-layer'
import { API_IDS } from '../common'

const DEFAULTS = {
    location: undefined
}

export class AppUtilsStore implements NetworkCallbacks {
    @observable location: string

    constructor() {
        this.init()
    }

    @action
    init() {
        this.location = DEFAULTS.location
    }

    onSuccess(apiId: string, response: any) {
        //
    }
    @action
    onSuccessUnAuthorized(apiId: string, response: any) {
        // Location Hack
        switch (apiId) {
            case API_IDS.GET_LOCATION_ADDRESS:
                if (response && response.results && response.results[0].formatted_address) {
                    this.setUserLocation(response.results[0].formatted_address)
                }
                break
            default:
                break
        }
    }

    @action
    setUserLocation(location) {
        this.location = location
    }
    onSuccessBadRequest(apiId: string, response: any) {
        //
    }
    onFailure(apiId: string, request: BaseRequest) {
        //
    }
    onComplete() {
        //
    }
    validateRequestParams() {
        return true
    }
    validateResponseParams(response: any) {
        return true
    }
    generalValidationError(type: string, error: String): void {
      //  console.warn(error)
    }
}
