import { action, computed, ObservableMap, observable, toJS } from 'mobx'
import { concat, get, set, toLower, values } from 'lodash'
import { Navigation } from 'react-navigation'

import { API_IDS, TYPE, FROM_PAGE_TYPE, SUBJECT_TYPE, DIALOG_DESCRIPTION, icons, USER_DETAILS_TYPE, TAB_TYPE } from '../common'
import { NetworkCallbacks, TestNetworkModule, AssignmentNetworkModule } from '../api-layer'
import { BaseRequest, BaseResponse } from '../http-layer'

interface TestResult {
  testId?: any,
  testName?: string,
  attempts?: any
}

const DEFAULT_SETTING = {
  navigation: Navigation,
  trialTestList: [],
  tests: {
    [TYPE.CONCEPT]: undefined,
    [TYPE.MAIN]: undefined
  },
  context: {
    testType: TYPE.MAIN,
    tabType: undefined,
    subject: undefined,
    goal: undefined
  },
  testResult: undefined,
  isLoading: false,
  filters: () => new ObservableMap({
    main: undefined,
    concept: undefined,
    isLocked: undefined,
    isAvailable: undefined,
    attempted: undefined,
    search: undefined
  }),
  internalFilters: () => new ObservableMap({
    isLocked: undefined,
    isAvailable: undefined,
    attempted: undefined,
    search: undefined
  }),
  externalFilters: () => new ObservableMap({
    main: false,
    concept: false
  }),
  upcomingAssignmentCount: undefined,
  pastAssignmentCount: undefined,
  combinedTestCount: undefined,
  conceptTestCount: undefined,
  showFilterSelection: false,
  courseSubjects: () => new ObservableMap()
}
const INTERNAL_FILTER = 'internalFilter'
const EXTERNAL_FILTER = 'externalFilter'
const isAttemptedPredicate = test => !!test.attempted
const isAvailablePredicate = test => !test.isLocked && test.availability === 'Live'
const isLockedPredicate = test => !!test.isLocked ||
  test.availability === 'Scheduled'

const searchPredicate = (test, searchTerm) => {
  const testName = get(test, 'test.name') || get(test, 'testName')
  if (searchTerm) {
    return toLower(testName).search(toLower(searchTerm)) > -1
  }
  return true
}

const predicates = {
  isAvailable: {
    condition: '||',
    predicate: isAvailablePredicate
  },
  attempted: {
    condition: '||',
    predicate: isAttemptedPredicate
  },
  isLocked: {
    condition: '||',
    predicate: isLockedPredicate
  },
  search: {
    condition: '&&',
    predicate: searchPredicate
  }
}

const forceUpdate = {
  ACTIVE: undefined,
  PAST: undefined,
  ALL_TESTS: undefined
}

export class AssignmentStore implements NetworkCallbacks {
  tabType
  navigation: Navigation
  @observable idHydrated = false
  // Trial test list that is to be rendered in component
  @observable filterDataList

  // poor man's cache for keeping purchased tests
  @observable assignments = {}
  @observable tests = {}
  @observable courseSubjects
  context
  goalName
  @observable activeAssignmentCount
  @observable pastAssignmentCount
  @observable allTestsAssignemntsCount
  @observable testResult
  @observable isLoading
  @observable selectedClassId
  @observable selectedClass
  @observable selectedProductId
  @observable selectedTargetExamCourseId
  @observable selectedTargetExam
  @observable targetExams
  @observable popupListRenderingData
  @observable filters
  @observable internalFilters
  @observable externalFilters

  @observable resultValues: TestResult = {
    testId: '',
    testName: '',
    attempts: []
  }
  @observable showResult: boolean
  @observable showFilterSelection: boolean
  @observable showTrialPage: boolean

  @observable productData: any

  testType
  data
  activeServerTime
  pastServerTime
  allTestServerTime
  attemptId

  constructor() {
    this.init()
  }
  async getFiitjeeLoginType() {
    return await isFiitjeeLoginType()
  }
  @action
  toggleFilterSelection(): void {
    this.showFilterSelection = !this.showFilterSelection
  }

  setNavigationObject(navigation) {
    this.navigation = navigation
  }
  @action
  setTestResultValues(testId: any, testName: string, attempts: any): any {
    this.resultValues.attempts = attempts
    this.resultValues.testId = testId
    this.resultValues.testName = testName
  }

  @action
  showResultScreen(show: boolean) {
    this.showResult = show
  }

  getTestResultValues(): any {
    return this.resultValues
  }

  showFilterSelectionBox(): boolean {
    return this.showFilterSelection
  }
  // applyfilters
  @action
  async setFilters(types = new ObservableMap()) {
    let typesVal = toJS(types)
    this.filters = DEFAULT_SETTING.filters()
    this.internalFilters = DEFAULT_SETTING.internalFilters()
    if (types && types.size) {
      this.isLoading = true
      Object.keys(typesVal).forEach(key => {
        if (typesVal[key] === INTERNAL_FILTER) {
          this.internalFilters.set(key, true)
        } else {
          this.externalFilters.set(key, true)
        }
        this.filters.set(key, true)
      })
      if (this.tabType === TAB_TYPE.ALL_TESTS) {
        await this.fetchAllTests(false)
      } else {
        await this.fetchAssignments(false)
      }
    }
  }

  removeFilter(type) {
    this.filters.set(type, false)
  }

  resetFilters() {
    this.setFilters(new ObservableMap({
      concept: false
    }))
    this.internalFilters = DEFAULT_SETTING.internalFilters()
    this.externalFilters = DEFAULT_SETTING.externalFilters()
    this.setTestType(TYPE.CONCEPT)
    this.setSubject(SUBJECT_TYPE.ALL)
    this.fetchAssignments(false)
    this.getCourseSubjects()
  }

  @action
  setExamPopupData() {
    this.popupListRenderingData = this.targetExams
  }

  @action init() {
    const self = this
    this.navigation = DEFAULT_SETTING.navigation
    this.context = DEFAULT_SETTING.context
    this.testResult = DEFAULT_SETTING.testResult
    this.isLoading = DEFAULT_SETTING.isLoading
    this.filters = DEFAULT_SETTING.filters()
    this.allTestsAssignemntsCount = DEFAULT_SETTING.combinedTestCount
    this.activeAssignmentCount = DEFAULT_SETTING.upcomingAssignmentCount
    this.pastAssignmentCount = DEFAULT_SETTING.pastAssignmentCount
    this.assignments = {}
    this.tests = {}
    this.internalFilters = DEFAULT_SETTING.internalFilters()
    this.externalFilters = DEFAULT_SETTING.externalFilters()
    this.courseSubjects = new ObservableMap()
    this.tabType = undefined
    this.goalName = undefined
    this.activeServerTime = undefined
    this.pastServerTime = undefined
    this.allTestServerTime = undefined
    this.setFilters(new ObservableMap({
      concept: false
    }))
  }

  getFilters() {
    return toJS(this.filters)
  }

  getInternalFilters() {
    return toJS(this.internalFilters)
  }

  getExternalFilters() {
    return toJS(this.externalFilters)
  }

  getTestType() {
    return this.context.testType
  }
  setAttemptId(attemptId) {
    this.attemptId = attemptId
  }
  getAttemptId() {
    return this.attemptId
  }

  isFilterApplied() {
    const filters = this.getInternalFilters()
    return Object.keys(filters).filter(key => filters[key]).length
  }

  applyFilters = (filterPredicates, test, defaultFilterBool) => {
    return filterPredicates.reduce((bool, filterObj) => {
      const { predicate, filter } = filterObj
      if (predicate.condition === '||') {
        return predicate.predicate(test, filter) || bool
      } else {
        return predicate.predicate(test, filter) && bool
      }
    }, defaultFilterBool)
  }

  getList() {
    const subject = this.context.subject
    const goal = this.context.goal
    const tabType = this.tabType
    const filters = this.getInternalFilters()
    const testList = get(this.assignments, this.getCachePath(tabType, this.context.testType, goal, subject))
    if (testList && testList.data && this.isFilterApplied() && tabType === TAB_TYPE.ALL_TESTS) {
      // filter implementation
      // (test) => predicates[key].predicate(test, filters[key], predicates[key].condition)
      const appliedFilters = Object.keys(filters)
        .filter(key => filters[key])
      const filterPredicates = appliedFilters.map(key => ({
        predicate: predicates[key],
        filter: filters[key]
      }))
      const hasSearchFilter = !!filters.search
      let defaultFilterBool = hasSearchFilter && appliedFilters.length === 1

      const testVal = testList.data.filter(test => this.applyFilters(filterPredicates, test, defaultFilterBool))
      return testVal
    }
    return testList && testList.data ? testList.data : []
  }

  getValue(value: any, defaultValue: any = undefined): any {
    if (value) {
      return toJS(value)
    }
    return defaultValue
  }

  @action
  setTestType(testType) {
    if (testType) {
      this.context.testType = testType
    }
  }

  @action
  setGoal(goal) {
    this.context.goal = goal
  }

  @action
  setGoalName(goalName) {
    this.goalName = goalName
  }

  @action
  setTabType(tabType) {
    this.tabType = tabType
  }

  @action
  setSubject(subject) {
    this.context.subject = subject
  }

  @action
  setLoader(bool) {
    this.isLoading = bool
  }

  setActiveServerTime(time) {
    this.activeServerTime = time
  }

  getActiveServerTime() {
    return this.activeServerTime
  }

  setPastServerTime(time) {
    this.pastServerTime = time
  }

  getPastServerTime() {
    return this.pastServerTime
  }

  setAllTestServerTime(time) {
    this.allTestServerTime = time
  }

  getAllTestServerTime() {
    return this.allTestServerTime
  }

  @action
  setAssignmentCounts(testlist) {
    switch (this.tabType) {
      case TAB_TYPE.ACTIVE:
        this.activeAssignmentCount = testlist.length
        break
      case TAB_TYPE.PAST:
        this.pastAssignmentCount = testlist.length
        break
      case TAB_TYPE.ALL_TESTS:
        this.allTestsAssignemntsCount = testlist.length
        break
      default:
        break
    }
  }
  @action
  setAssignmentServerTime(testlist) {
    switch (this.tabType) {
      case TAB_TYPE.ACTIVE:
        this.setActiveServerTime(get(testlist.data, 'serverTime'))
        break
      case TAB_TYPE.PAST:
        this.setPastServerTime(get(testlist.data, 'serverTime'))
        break
      default:
        this.setAllTestServerTime(get(testlist.data, 'serverTime'))
        break
    }
  }

  getTestCounts() {
    return {
      [TAB_TYPE.ACTIVE]: this.activeAssignmentCount,
      [TAB_TYPE.PAST]: this.pastAssignmentCount,
      [TAB_TYPE.ALL_TESTS]: this.allTestsAssignemntsCount
    }
  }
  getPackageList() {
    return toJS(this.productData)
  }

  @action
  onSuccess(apiId: any, response: any): void {
    const { data } = response.data
    const self = this
    switch (apiId) {
      case API_IDS.GET_COURSE_SUBJECTS:
        this.courseSubjects = new ObservableMap()
        response.data[0].data.forEach(element => {
          this.courseSubjects.set(element.name, false)
        })
        break
      case API_IDS.GET_ACTIVE_ASSIGNMENTS:
      case API_IDS.GET_PAST_ASSIGNMENTS:
      case API_IDS.GET_ALL_TESTS_ASSIGNMENTS:
        this.setAssignmentServerTime(response)
        this.storeAssignmenttDetails(response)
        break
      default:
        break
    }
    self.isLoading = false
  }

  storeAssignmenttDetails(response) {
    forceUpdate[this.tabType] = false
    const subject = this.context.subject
    const goal = this.context.goal
    const tabType = this.tabType
    const testType = this.context.testType
    switch (testType) {
      case TYPE.CONCEPT:
        if (subject === SUBJECT_TYPE.ALL) {
          set(this.assignments, this.getCachePath(tabType, testType, goal, SUBJECT_TYPE.ALL), response.data)
        } else {
          set(this.assignments, this.getCachePath(tabType, testType, goal, subject), response.data)
        }
        break
      default:
        set(this.assignments, this.getCachePath(tabType, testType, goal, subject), response.data)
        break
    }
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    switch (apiId) {
      case API_IDS.GET_ALL_TESTS_ASSIGNMENTS:
        this.isLoading = false
        break
      default:
        break
    }
    //
  }

  onSuccessBadRequest(apiId: any, response: any) {
    //
  }

  onFailure(apiId: string, request: BaseRequest): void {
    this.isLoading = false
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: BaseResponse): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }

  // Finds the path where the tests are placed inside the cache
  getCachePath(tabType, testType, goalID, subject) {
    switch (testType) {
      case TYPE.CONCEPT:
      case TYPE.MAIN:
        return [tabType, testType, goalID, testType === TYPE.CONCEPT ? subject : undefined]
      default:
        return ['tests', goalID]
    }
  }
  setForceUpdate(key) {
    forceUpdate[key] = true
  }
  resetSearchFilter() {
    this.filters.set('search', false)
  }

  @action
  async fetchAssignments(preFetch = false) {
    this.resetSearchFilter()
    if (this.context.testType === 'main') {
      this.setSubject(undefined)
    }
    if (this.context.testType) {
      let tests = null
      if (!forceUpdate[this.tabType]) {
        tests = get(this.assignments, this.getCachePath(this.tabType, this.context.testType, this.context.goal, this.context.subject))
      }
      this.isLoading = true
      if (!tests) {
        // to be able to use this in onSuccess method
        this.context = {
          testType: this.context.testType,
          subject: this.context.subject,
          goal: this.context.goal,
          orgId: 'FIITJEE'
        }
        try {
          const config = { methodType: 'GET' }
          const assignmentNetworkModule = new AssignmentNetworkModule(config, this)
          let urlParam
          if (this.context.subject) {
            urlParam = {
              urlParams: {
                testType: this.context.testType,
                goalId: this.context.goal,
                subject: this.context.subject,
                orgId: 'FIITJEE'
              }
            }
          } else {
            urlParam = {
              urlParams: {
                testType: this.context.testType,
                goalId: this.context.goal,
                orgId: 'FIITJEE'
              }
            }
          }
          switch (this.tabType) {
            case TAB_TYPE.ACTIVE:
              await assignmentNetworkModule.getActiveAssignments(urlParam, preFetch)
              break
            case TAB_TYPE.PAST:
              await assignmentNetworkModule.getPastAssignments(urlParam, preFetch)
              break
            default:
              break
          }
        } catch (error) {
          this.isLoading = false
        }
      } else {
        switch (this.tabType) {
          case TAB_TYPE.ACTIVE:
            this.activeAssignmentCount = tests.data ? tests.data.length : 0
            break
          case TAB_TYPE.PAST:
            this.pastAssignmentCount = tests.data ? tests.data.length : 0
            break
          default:
            break

        }
        this.isLoading = false
      }
    }
  }

  @action
  fetchAllTests(preFetch = false) {
    this.resetSearchFilter()
    if (this.context.testType === 'main') {
     // this.setSubject(undefined)
    }
    if (this.context.testType) {
      let tests = null
      if (!forceUpdate[this.tabType]) {
        tests = get(this.assignments, this.getCachePath(this.tabType, this.context.testType, this.context.goal, this.context.subject))
      }

      this.isLoading = true
      if (!tests) {
        // to be able to use this in onSuccess method
        this.context = {
          testType: this.context.testType,
          subject: this.context.subject,
          goal: this.context.goal,
          goalName: this.goalName,
          orgId: 'FIITJEE'
        }
        try {
          const config = { methodType: 'GET' }
          const testNetworkModule = new AssignmentNetworkModule(config, this)
          let urlParam
          if (this.context.subject) {
            urlParam = {
              urlParams: {
                testType: this.context.testType,
                goalId: this.context.goal,
                goalName: this.goalName,
                subject: this.context.subject,
                orgId: 'FIITJEE'
              }
            }
          } else {
            urlParam = {
              urlParams: {
                testType: 'main',
                goalId: this.context.goal,
                goalName: this.goalName,
                orgId: 'FIITJEE'
              }
            }
          }
          testNetworkModule.getAllTests(urlParam, preFetch)
        } catch (error) {
          this.isLoading = false
        }
      } else {
        this.allTestsAssignemntsCount = tests.data ? tests.data.length : 0
        this.isLoading = false
      }
    }
  }

  updateSubjectFilter(subjectVal) {
    let subjectList = toJS(this.courseSubjects)
    this.courseSubjects = new ObservableMap()
    Object.keys(subjectList).forEach(subject => {
      if (subjectVal.match(subject)) {
        this.courseSubjects.set(subject, true)
      } else {
        this.courseSubjects.set(subject, false)
      }
    })
  }

  resetSubjectFilter() {
    let subjectList = toJS(this.courseSubjects)
    this.courseSubjects = new ObservableMap()
    Object.keys(subjectList).forEach(subject => {
      this.courseSubjects.set(subject, false)
    })
  }

  setSearchFilter(searchTerm) {
    this.internalFilters.set('search', searchTerm || false)
  }

  @action
  async getCourseSubjects() {
    let courses = []
    courses[0] = this.context.goal
    const config = { 'methodType': 'POST' }
    const getQueryString = JSON.stringify({ courseIds: courses })
    const homeNetworkModule = new AssignmentNetworkModule(config, this)
    await homeNetworkModule.getCourseSubjects(getQueryString)
  }

  onComplete() {
    //
  }
}
