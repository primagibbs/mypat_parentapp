
import { observable } from 'mobx'
interface AttemptListData {
  id?: string
  totalScore?: number
  score?: number
  correct?: number
  incorrect?: number
  improvement?: number
  totalTime?: string
  attemptNumber?: number
  isAssinment?: boolean
  attemptDate?: Date
}
export class AttemptList {
  @observable id: string
  @observable totalScore: number
  @observable score: number
  @observable correct: number
  @observable incorrect: number
  @observable improvement: number
  @observable totalTime: string
  @observable attemptNumber: number
  @observable isAssinment: boolean
  @observable attemptDate: Date

  constructor(attemptData: AttemptListData) {
    this.id = attemptData.id || ''
    this.score = attemptData.score || 0
    this.totalScore = attemptData.totalScore || 0
    this.correct = attemptData.correct || 0
    this.incorrect = attemptData.incorrect || 0
    this.improvement = attemptData.improvement || 0
    this.totalTime = attemptData.totalTime || ''
    this.attemptNumber = attemptData.attemptNumber || 0
    this.isAssinment = attemptData.isAssinment ? true : false
    this.attemptDate = attemptData.attemptDate || new Date()
  }

}