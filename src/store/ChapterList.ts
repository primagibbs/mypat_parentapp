import { observable } from 'mobx'
import { TestList } from './TestList'

interface ChapterListData {
  id?: string
  name?: string
  testCount?: number
  attemptedTestCount?: number
  subject?: any
  conceptCode?: any
  tests?: TestList[]
  type: string
}
export class ChapterList {
  @observable id?: string
  @observable name?: string
  @observable testCount?: number
  @observable attemptedTestCount  ?: number
  @observable tests?: TestList[]
  @observable type: string
  @observable conceptCode?: any
  @observable subject?: any

  constructor(chapterData: ChapterListData) {
    this.id = chapterData.id || ''
    this.name = chapterData.name || ''
    this.testCount = chapterData.testCount || 0
    this.attemptedTestCount = chapterData.attemptedTestCount || 0
    this.tests = chapterData.tests
    this.conceptCode = chapterData.conceptCode || ''
    this.type = chapterData.type || 'chapter'
    this.subject = chapterData.subject || ''
  }
}