import { observable, action } from 'mobx'

const DEFAULT_SETTING = {
  isVisible: false,
  hideDialogOnTouchOutside: undefined,
  animationType: 'fade',
  positioning: 'center',
  onModalClose: undefined,
  onBackClicked: undefined,
  element: undefined
};

export class CustomModalStore {
  @observable isVisible;
  @observable hideDialogOnTouchOutside;
  @observable onModalClose;
  @observable animationType;
  @observable onBackClicked;
  @observable positioning;
  @observable element: React.Component;

  constructor() {
    this.init()
  }

  @action init() {
    Object.keys(DEFAULT_SETTING).forEach((key) => {
      this[key] = DEFAULT_SETTING[key];
    });
  }

  @action showModal(element, {positioning, onBackClicked, hideDialogOnTouchOutside, animationType, onModalClose}) {
    this.isVisible = true;
    this.element = element;
    this.positioning = positioning;
    this.onBackClicked = onBackClicked;
    this.onModalClose = onModalClose;
    this.hideDialogOnTouchOutside = hideDialogOnTouchOutside;
    this.animationType = animationType;
  }

  @action hideModal() {
    if (this.onModalClose) {
      this.onModalClose()
    }
    this.init()
  }
}
