import { observable, action } from 'mobx'
import {isEmpty} from '../utils'

const DEFAULT_SETTING = {
  imageUrl: undefined,
  navigation: undefined,
  buttonLabel: undefined,
  titletext: undefined,
  descriptiontext: undefined,
  isVisible: false,
  cancelButtonVisible: false,
  logic: undefined,
  hideDialogOnTouchOutside: undefined
}

export class DialogStore {
  @observable isVisible = false
  imageUrl
  navigation
  titleText
  descriptiontext
  buttonLabel
  hideDialogOnTouchOutside
  onCancelClicked
  @observable cancelButtonVisible
  logic: () => any

  constructor() {
    this.init()
  }

  @action init() {
    this.imageUrl = DEFAULT_SETTING.imageUrl
    this.navigation = DEFAULT_SETTING.navigation
    this.buttonLabel = DEFAULT_SETTING.buttonLabel
    this.titleText = DEFAULT_SETTING.titletext
    this.descriptiontext = DEFAULT_SETTING.descriptiontext
    this.isVisible = DEFAULT_SETTING.isVisible
    this.cancelButtonVisible = DEFAULT_SETTING.cancelButtonVisible
    this.logic = DEFAULT_SETTING.logic
  }

  @action showDialog(navigation, titleText ,  descriptiontext, buttonLabel, imageUrl, buttonClick,
      cancelButtonVisible, hideDialogOnTouchOutside, onCancelClicked) {
    this.isVisible = true
    this.navigation = navigation
    this.titleText = titleText
    this.descriptiontext = descriptiontext
    this.buttonLabel = buttonLabel
    this.imageUrl = imageUrl
    this.logic = buttonClick
    this.cancelButtonVisible = cancelButtonVisible
    this.hideDialogOnTouchOutside = hideDialogOnTouchOutside
    this.onCancelClicked = onCancelClicked
  }

  @action hideDialog() {
    if (this.onCancelClicked) {
      this.onCancelClicked()
    }
    this.isVisible = false
  }

  @action onButtonClick(logic: () => any) {
    if (isEmpty(this.titleText)) {
      this.hideDialog()
    }
    logic()
  }
}