import { Navigation } from 'react-navigation'
import { action, observable, computed, toJS, ObservableMap } from 'mobx'
import { differenceWith, get, isEqual, map, omit, unionBy } from 'lodash'
import { Platform } from 'react-native'
import { resetRouterSimple, navigateSimple, showSnackbar, goBack } from '../services'
import { ProfileNetworkModule } from '../api-layer'
import { BaseRequest, BaseResponse } from '../http-layer'
import { clone } from 'lodash'
import { NetworkCallbacks } from '../api-layer'
import {
  getEmailId, setCompleteGoal, setGoalsData, setAllGoals, getDisabledPackages,
  updateEmailDetails, updateMobileDetails, handleSignIn, getGoalsData,
  getProfileData,
  isFiitjeeLoginType,
  setDropDownGoalId,
  setDropDownGoalName,
  getPackages,
  getTargetExam,
  getLoggedInUser
} from '../utils'
import { showDialog, hideDialog } from '../services'
import {
  icons, USER_INFO_TYPE, FROM_PAGE_TYPE, CONGRATS_PAGE_TYPE, DIALOG_DESCRIPTION, API_IDS,
  API_END_POINTS, USER_DETAILS_TYPE, RESPONSE_ERROR_CODE, TYPE, TAB_TYPE
} from '../common'
import { getMobileNumber, getMobileVerified } from '../utils'
import extraEdgeService from '../extraedge'
import commonStores from '../common-library/store'
import stores from '../store'
import transport from '../common-library/events'
const convertObjectToElementsArray = (obj) => map(obj, (value, prop) => ({ prop, value }))

interface UserObject {
  name: string
  email: string
  userMobile: string,
  countryCode: string,
  userLocation: any
  userClass: any
  school: any
  userGoals: any
  partner: any
}
interface Class {
  _id: string
  class: string
}

interface UserClass {
  classId: string
  name: string
  sessionYear: number
}

interface UserGoals {
  courseId: string
  name: string
  targetYear: string
  courseType: CourseType
}

interface CourseType {
  id: string
  name: string
}
const DEFAULT_SETTING = {
  currentGoalSelection: [],
  classDataResponse: [{ '_id': '', 'class': '' }],
  partnerDataResponse: [{ 'createdAt': '', 'name': '', '_id': '' }],
  isLoading: false,
  isEmailVerified: false,
  isMobileVerified: false,
  isPartnerVerified: false,
  classDataResponseMap: {},
  partnerDataResponseMap: {},
  navigation: undefined,
  classList: [],
  selectedClass: undefined,
  classGoalsCache: () => new ObservableMap(),
  countryCode: undefined
}

const getDefaultDetails = () => ({
  name: undefined,
  email: undefined,
  userMobile: undefined,
  countryCode: undefined,
  userLocation: {},
  userClass: {},
  school: {},
  userGoals: [],
  partner: {}
})

export class EditProfileDataStore implements NetworkCallbacks {
  /*
   * Caches the state and selection of goals when the user
   * lands on the edit profile page.
   */
  @observable classDataResponse = [{ '_id': '', 'class': '' }]
  @observable partnerDataResponse = [{ 'createdAt': '', 'name': '', '_id': '' }]
  @observable classList: any[]
  @observable selectedClass: Class
  @observable classGoalsCache: any
  @observable isLoading: boolean
  @observable isEmailVerified: boolean
  @observable isMobileVerified: boolean
  @observable isPartnerVerified: boolean = false
  @observable userMobile: string = ''
  @observable countryCode: string = ''
  userClass: UserClass
  currentGoalSelection
  callType
  currentSelectedGoalData
  // maps the class _id to its object
  classDataResponseMap = {}
  partnerDataResponseMap = {}
  tempObj: UserObject = getDefaultDetails()
  asyncStorageObj: UserObject = getDefaultDetails()
  navigation: Navigation
  latitude: any
  longitude: any

  constructor() {
    this.init()
  }

  @action init() {
    this.classList = DEFAULT_SETTING.classList
    this.selectedClass = DEFAULT_SETTING.selectedClass
    this.classGoalsCache = DEFAULT_SETTING.classGoalsCache()
    this.asyncStorageObj = getDefaultDetails()
    this.tempObj = getDefaultDetails()
    this.navigation = DEFAULT_SETTING.navigation
    this.currentGoalSelection = DEFAULT_SETTING.currentGoalSelection
    this.classDataResponse = DEFAULT_SETTING.classDataResponse
    this.partnerDataResponse = DEFAULT_SETTING.partnerDataResponse
    this.isLoading = DEFAULT_SETTING.isLoading
    this.isEmailVerified = DEFAULT_SETTING.isEmailVerified
    this.isMobileVerified = DEFAULT_SETTING.isMobileVerified
    this.isPartnerVerified = DEFAULT_SETTING.isPartnerVerified
    this.classDataResponseMap = DEFAULT_SETTING.classDataResponseMap
    this.partnerDataResponseMap = DEFAULT_SETTING.partnerDataResponseMap
  }

  @action updateUserMobile(mobile, isUpdateSingle, countryCode) {
    if (isUpdateSingle) {
      this.userMobile = mobile
      this.countryCode = countryCode
      return
    }
    this.userMobile = mobile
    this.countryCode = countryCode
    this.asyncStorageObj.countryCode = countryCode
    this.asyncStorageObj.userMobile = mobile
  }

  @action
  setMobileVerified(value) {
    this.isMobileVerified = value
  }
  setNavigationObject(navigation) {
    this.navigation = navigation
  }

  setClassDataResponse(response) {
    this.classDataResponse = response
    if (this.classDataResponse) {
      response.forEach(item => {
        this.classDataResponseMap[item._id] = item
      })
    }
  }

  @action hasMobileChanged(value) {
    this.isMobileVerified = isEqual(value, this.asyncStorageObj.userMobile)
  }

  @action hasEmailChanged(value) {
    this.isEmailVerified = isEqual(value, this.asyncStorageObj.email)
  }

  @action
  updateUserDetails() {
    this.callType = 'edit_profile_goals_data'
    const config = { methodType: 'PUT' }
    const profileNetworkModule = new ProfileNetworkModule(config, this)
    // convert objects to arrays of elements
    const tempObjElements = convertObjectToElementsArray(this.tempObj)
    const asyncStorageObjElements = convertObjectToElementsArray(this.asyncStorageObj)
    // find whatever props have been updated
    const updatedProps = differenceWith(tempObjElements, asyncStorageObjElements, isEqual)
    // map the changed elements back to an object
    const updatedObj = {}
    updatedProps.forEach(item => {
      updatedObj[item.prop] = item.value
    })
    const postBody = omit(updatedObj, ['partner'])
    profileNetworkModule.setDetails(JSON.stringify(postBody))
  }
  @action
  async updateUserGoalDetails(userGoals, currentlyGoals) {
    this.currentSelectedGoalData = currentlyGoals
    this.callType = 'header_goals_data'
    const uesrData = await getLoggedInUser()
    const userObject = await JSON.parse(uesrData)
    try {
      const config = { 'methodType': 'PUT' }
      const postBody = JSON.stringify({ 'userClass': userObject.class, 'userGoals': userGoals })
      const profileNetworkModule = new ProfileNetworkModule(config, this)
      await profileNetworkModule.setGoalsDetails(postBody)
    } catch (error) {
      //
    }
  }

  @action
  updateVerificationStatus(response) {
    this.isMobileVerified = get(response, 'userId.mobile.isVerified')
    this.isEmailVerified = get(response, 'userId.email.isVerified')
  }

  @action
  async getClassList() {
    const config = { methodType: 'GET' }
    const profileNetworkModule = new ProfileNetworkModule(config, this)
    await profileNetworkModule.getClasses()
  }

  @action
  getGoalsData() {
    // get goals
  }

  @action
  async updateEmail(emailVal) {
    try {
      const config = { 'methodType': 'PUT' }
      const postBody = JSON.stringify({ 'email': emailVal })
      const profileNetworkModule = new ProfileNetworkModule(config, this)
      await profileNetworkModule.updateEmail(postBody)
    } catch (error) {
      //
    }
  }

  @action
  async updateMobile(mobileNumber: string) {
    const config = { 'methodType': 'PUT' }
    const postBody = JSON.stringify({ 'mobileNumber': mobileNumber, 'type': null })
    const profileNetworkModule = new ProfileNetworkModule(config, this)
    await profileNetworkModule.updateMobile(postBody)
  }

  @action
  async verifyPartner(partnerObj) {
    const config = { 'methodType': 'POST' }
    const profileNetworkModule = new ProfileNetworkModule(config, this)
    const postBody = JSON.stringify({
      'enrollmentno': partnerObj.enrollment,
      'password': partnerObj.password,
      'username': partnerObj.name
    })
    await profileNetworkModule.verifyPartner(JSON.stringify(postBody))
  }

  async renderGoalsData(data) {
    const { homeDataStore, performanceStore, goalsDataStore } = stores
    const { headerDataStore } = commonStores
    headerDataStore.setdropdownRightTitle(data.name)
    headerDataStore.setdropdownRightTargetGoal(data)
    homeDataStore.setGoalId(data.courseId || data._id)
    performanceStore.setGoalId(data.courseId || data._id)
    performanceStore.setGoalName(data.name)
    goalsDataStore.setSelectedGoalId(data.courseId || data._id)
    await homeDataStore.getFreeTestResult(true);

    setDropDownGoalId(data.courseId || data._id)
    setDropDownGoalName(data.name)
    performanceStore.getUpcomingExam(stores.performanceStore.getGoalId(), true)
    performanceStore.getPerformance(stores.performanceStore.getCurrentTest(),
      performanceStore.getCurrentTestType(), stores.performanceStore.getGoalId(), true)
    await homeDataStore.isOldMyPat(commonStores.headerDataStore.getDropdownRightTargetGoal())
    performanceStore.getGoalProgress(stores.performanceStore.getSwitchType(), stores.performanceStore.getGoalId(), true)

  }
  getTargetExams(rowData) {
    return rowData.map(function (elem) {
      return elem.name
    }).join(', ')
  }
  @action
  async onSuccess(apiId: any, response: any) {
    switch (apiId) {
      case API_IDS.GET_CLASSES:
        this.setClassDataResponse(response.data)
        break
      case API_IDS.GET_GOALS:
        await this.getFinalGoals(response.data)
        break
      case API_IDS.UPDATE_OTHER_DETAILS:
        if (this.asyncStorageObj.userClass !== response.data.userClass) {
          this.updateClassStatus(response)
        }
        handleSignIn({
          ...response,
          data: {
            ...response.data,
            goals: response.data.goals.reverse()
          }
        }, USER_DETAILS_TYPE.DEFAULT,
          response.data && response.data.goals && response.data.goals.length)
        this.updateVerificationStatus(response.data)
        // TODO destructuring here
        this.asyncStorageObj.name = response.data.userId.name.firstName
        this.asyncStorageObj.email = response.data.userId.email.id
        this.updateUserMobile(response.data.userId.mobile.number, false, response.data.userId.mobile.countryCode)
        this.asyncStorageObj.userLocation = response.data.location
        this.asyncStorageObj.userClass = response.data.userClass
        this.asyncStorageObj.school = response.data.school
        this.asyncStorageObj.partner = response.data.partner
        const partnerInstitute = response.data.partner.name ? response.data.partner.name : ''
        transport.setData('Site', {
          'Name': response.data.userId.name.firstName,
          'email': response.data.userId.email.id, 'phone': response.data.userId.mobile.number
        })
        transport.onUserLoginPush()
        transport.setData('profileData', {
          'Location': response.data.location.address.firstLine, 'Lat': response.data.location.latitute,
          'Long': response.data.location.longitute, 'School': response.data.school.name,
          'Class': response.data.userClass.name,
          'Target Exams': this.getTargetExams(response.data.goals),
          'Partner Institute': partnerInstitute
        })
        transport.setProfile()
        transport.setData('Edit Profile', {
          'Action': 'Clicked Save', 'Partner Institute': response.data.partner.partnerId ? 'Yes' : 'No', 'Device Type': Platform.OS
        })
        transport.post()
        stores.goalsDataStore.fetchGoals()
        if (this.callType !== 'header_goals_data') {
          { this.renderGoalsData(response.data.goals[0]) }
          goBack(this.navigation)
        } else {
          // todo for header goals data
          { this.renderGoalsData(this.currentSelectedGoalData) }
        }
        break
      default:
        break
    }
  }

  async updateClassStatus(response) {
    const { data } = response
    const promises = [
      getMobileVerified(),
      getMobileNumber(),
      getEmailId()
    ]
    const responses = await Promise.all(promises)
    const isMobileVerified = responses[0]
    const mobileNumber = responses[1]
    const email = responses[2]
    const { userClass } = response.data
    const userclass = userClass && userClass.name
    const queryString = JSON.stringify({
      'MobileVerified': isMobileVerified || '',
      'MobileNumber': mobileNumber || '',
      'Email': email || 'noemail',
      'Field2': userclass || 'noclass'
    })
    extraEdgeService.setReqConfig(queryString)
    extraEdgeService.hitGetRequest()
  }

  @computed get goalsList(): any[] {
    if (this.selectedClass) {
      return toJS(this.classGoalsCache.get(this.selectedClass._id) || [])
    }
    return []
  }

  isGoalSelectedPredicate = (x: any): boolean => x.selected

  @action
  async fetchGoals(selectedClass) {
    try {
      const config = { 'methodType': 'GET' }
      const verifyNetworkModule = new ProfileNetworkModule(config, this)
      await verifyNetworkModule.getGoals({
        urlParams: {
          class: selectedClass._id
        }
      })
    } catch (error) {
      //
    }
  }

  @action
  selectClass(classObject) {
    this.selectedClass = classObject
    this.fetchGoals(this.selectedClass)
  }

  @action
  toggleGoalSelection(index) {
    // just flip the switch in the cache itself
    const targetGoals = toJS(this.classGoalsCache.get(this.selectedClass._id))
    targetGoals[index].selected = !targetGoals[index].selected
    this.classGoalsCache.set(this.selectedClass._id, targetGoals)
  }

  getValue(value: any, defaultValue: any = undefined): any {
    if (value) {
      return toJS(value)
    }
    return defaultValue
  }

  // goalsList is the response fetched from the API
  async getFinalGoals(goalsList) {
    if (goalsList && goalsList.length > 0) {
      const disabledPackagesArr = await getDisabledPackages()
      const myGoals = await getGoalsData()
      if (myGoals) {
        let enabledGoalsArr = goalsList.map((item) => ({
          ...item,
          enabled: true
        }))
        let disabledGoalsArr = []
        if (disabledPackagesArr && disabledPackagesArr.length) {
          disabledGoalsArr = disabledPackagesArr.map((item) => ({
            ...item,
            courseId: item._id || item.courseId,
            enabled: false,
            selected: true
          }))
        }
        let finalGoalsArr = unionBy(disabledGoalsArr, enabledGoalsArr, '_id')
        finalGoalsArr.sort((goalA, goalB) => {
          if (goalA.enabled && !goalB.enabled) {
            return 1
          } else if (!goalA.enabled && goalB.enabled) {
            return -1
          }
          return 0
        })
        // enable my goals
        finalGoalsArr = finalGoalsArr.map(goal => {
          if (
            myGoals && myGoals.filter(myGoal => {
              return get(myGoal, 'courseId') === goal._id
            }).length
          ) {
            goal.selected = true
          }
          return goal
        })
        this.currentGoalSelection = finalGoalsArr
        this.classGoalsCache.set(this.selectedClass._id, finalGoalsArr)
      }
    }
  }
  setCourseName(item) {
    if (item.name.includes('JEE(ADVANCED)')) {
      return item.name.replace('ADVANCED', 'A')
    } else if (item.name.includes('JEE(MAIN)') || item.name.includes('JEE (MAIN)')) {
      return item.name.replace('MAIN', 'M')
    } else if (item.name.includes('JEE (main)')) {
      return item.name.replace('main', 'M')
    }
    return item.name
  }
  @action
  getSelectedGoalList() {
    let userGoals: UserGoals[] = [{ courseId: '', name: '', targetYear: '', courseType: { id: '', name: '' } }]

    const targetGoals = toJS(this.classGoalsCache.get(this.selectedClass._id))
    userGoals = targetGoals
      .filter(item => item.selected)
      .map((item, index) => ({
        courseId: item._id,
        name: this.setCourseName(item),
        targetYear: item.targetYear,
        courseType: { id: item.courseType ? item.courseType.id : '', name: item.courseType ? item.courseType.name : '' }
      })
      )
    this.updateGoals(userGoals)
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    switch (apiId) {
      case API_IDS.UPDATE_OTHER_DETAILS:
        showSnackbar(response.message)
        break
      case API_IDS.VERIFY_PARTNER:
        if (response.code === RESPONSE_ERROR_CODE.FAILURE) {
          showSnackbar(response.message)
        }
        break
      default:
        break
    }
  }

  onSuccessBadRequest(apiId: any, response: any) {
    showSnackbar(response.message)
  }

  onFailure(apiId: string, response) {
    //
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: BaseResponse): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }

  onComplete() {
    //
  }

}
