import { action, observable } from 'mobx';

import { showSnackbar, navigateSimple } from '../services';
import { BaseRequest } from '../http-layer';
import {FeedNetworkModule, NetworkCallbacks} from '../api-layer';
import { API_IDS } from '../common';

const DEFAULT_SETTING = {
	// @ts-ignore
	feedList: [],
	isLoading: false,
	feedDetail: {},
	feedEndReached: false
};

export interface Feed {
	id: string
	image: string
	title: string
	postedOn: any
	liked: any
}

export class FeedStore implements NetworkCallbacks {
	courseId: any;
	@observable feedList: any;
	@observable feedDetail: any;
	feedData: any;
	navigation: any;
	feedEndReached: boolean;
	@observable isLoading: any;
	constructor() {
		this.init()
	}

	@action init() {
		this.feedList = DEFAULT_SETTING.feedList;
		this.feedData = DEFAULT_SETTING.feedList;
		this.feedDetail = DEFAULT_SETTING.feedDetail;
		this.isLoading = DEFAULT_SETTING.isLoading;
		this.feedEndReached = DEFAULT_SETTING.feedEndReached;
	}

	fillFeedArray(feedData: any) {
		if(feedData.length === 0) {
			this.feedEndReached = true;
		} else {
			this.feedList = this.feedList.concat(feedData.map((feedItem: any) => this.createFeedArray(feedItem)))
		}
	}

	createFeedArray(feedItem: any) {
		let feedObj: Feed;
		feedObj = {
			id: feedItem.id,
			title: feedItem.title,
			image: feedItem.image,
			postedOn: feedItem.postedOn
		};
		return feedObj
	}

	@action
	async getFeedList(prefetch = false) {
		if(this.feedEndReached || this.isLoading) {
			return;
		}
		this.isLoading = true;
		const config = { 'methodType': 'GET' };
		const feedNetworkModule = new FeedNetworkModule(config, this);
		await feedNetworkModule.getFeedList(prefetch, this.feedList.length, 10);
	}

	@action
	async getFeedDetail(feedId: any) {
		this.isLoading = true;
		const config = { 'methodType': 'GET' };
		const feedNetworkModule = new FeedNetworkModule(config, this);
		await feedNetworkModule.getFeedDetail(feedId);
	}

	@action
	async toggleFeedLike(feedId: any) {
		this.isLoading = false;
		const config = { 'methodType': 'POST' };
		const feedNetworkModule = new FeedNetworkModule(config, this);
		await feedNetworkModule.updateFeedLike(feedId)
	}

	@action
	async onSuccess(apiId: any, response: any): void {
		this.isLoading = false;
		switch (apiId) {
			case API_IDS.GET_FEED_LIST:
				this.fillFeedArray(response.data);
				break;
			case API_IDS.GET_FEED_DETAIL:
				this.feedDetail = response.data;
				break;
			case API_IDS.POST_FEED_LIKE:
				this.feedDetail.liked = response.data === 'liked';
				break;
			default:
				break
		}
	}

	onSuccessBadRequest(apiId: string, response: any) {
		this.isLoading = false;
		showSnackbar(response.message)
		//  throw new Error("Method not implemented.");
	}

	onSuccessUnAuthorized(apiId: any, response: any) {
		this.isLoading = false
		//
	}

	onFailure(apiId: string, request: BaseRequest) {
		this.isLoading = false
		// TODO OnFailure Implementation
	}

	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: any): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		// TODO generalValidationError Implementation
	}

	onComplete() {
		// TODO onComplete Implementation
	}

	setNavigationObject(navigation: any) {
		this.navigation = navigation
	}

	@action
	redirectPage(feed: any) {
		navigateSimple(this.navigation, 'FeedDetailPage', {feedId: feed})
	}
}
