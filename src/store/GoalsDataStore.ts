import { observable, action } from 'mobx'
import { toJS } from 'mobx'
import { get, unionBy, _ } from 'lodash'

import { NetworkCallbacks, ProfileNetworkModule } from '../api-layer'
import { BaseRequest, BaseResponse } from '../http-layer'
// tslint:disable-next-line:max-line-length
import { getDisabledPackages, getGoalsData, getClass, getAllGoalsData, getPackages, setDropDownGoalId, setDropDownGoalName, getSelectedGoal, getNewUserGoalsData } from '../utils'
import { API_IDS, RESPONSE_ERROR_CODE } from '../common'
import stores from '../store'
import commonStores from '../common-library/store'
import { showSnackbar } from '../services'

const DEFAULT_SETTING = {
	goalsData: [],
	selectedClass: [],
	packagesBought: []
}

export class GoalsDataStore implements NetworkCallbacks {
	@observable isVisible = false;
	@observable goalsData;
	@observable selectedClass;
	@observable packagesBought;
	@observable selectedGoal;
	@observable currentGoal;
	@observable currentGoalIsExpired = false;
	navigation;
	currentContext;
	touchType;
	componentX;
	isHorizontal;
	dataSource;
	keyOfText;
	disabledDataSource;

	getGoalsData() {
		return toJS(this.goalsData)
	}

	getPackagesBought() {
		return toJS(this.packagesBought)
	}

	constructor() {
		this.init()
		this.initData()
	}

	@action init() {
		this.goalsData = DEFAULT_SETTING.goalsData;
		this.selectedClass = DEFAULT_SETTING.selectedClass;
		this.packagesBought = DEFAULT_SETTING.packagesBought
	}

	@action
	async initData() {
		await this.setFinalGoals(await getAllGoalsData());
		this.packagesBought = await getPackages()
	}

	@action
	async fetchGoals() {
		// console.warn('fetch goals')
		try {
			const selectedClass = await getClass();
			if (selectedClass && selectedClass.classId) {
				this.selectedClass = selectedClass;
				const config = { 'methodType': 'GET' };
				const verifyNetworkModule = new ProfileNetworkModule(config, this);
				await verifyNetworkModule.getGoals({
					urlParams: {
						class: selectedClass.classId
					}
				})
			}
		} catch (error) {
			//
		}
	}

	getTargetsString() {
		const goalsData = this.getGoalsData()
		if (goalsData && goalsData.length) {
			let goalsList = [];
			goalsData.map((item) => {
				if (item.selected) {
					goalsList.push(item)
				}
			})
			return goalsList.map(item => item.name).join(', ')
		}
		return null
	}

	@action
	createUserSelectedGoalsList() {
		let goalsList = [] // for updated entries
		let goalsData = this.getGoalsData()
		goalsData.map((item) => {
			const currentTarget = commonStores.headerDataStore.getDropdownRightTargetGoal()
			if (currentTarget.name !== item.name && item.selected) {
				goalsList.push(item)
			}
		})
		return goalsList
	}

	@action
	createUserDisabledGoalsList() {
		let disabledGoalsList = [] // for updated entries
		let goalsData = this.getGoalsData()
		goalsData.map((item) => {
			if (!item.selected) {
				disabledGoalsList.push(item)
			}
		});
		return disabledGoalsList
	}

	@action
	setPackagesBought(packages: any) {
		this.packagesBought = packages
	}

	async getFinalGoals(goalsList: any) {
		const disabledPackagesArr = await getDisabledPackages();
		console.log("disabledPackagesArr", disabledPackagesArr);

		const myGoals = await getGoalsData();
		console.log("myGoals", myGoals);
		if (myGoals) {
			// @ts-ignore
			let enabledGoalsArr = goalsList.map((item) => ({
				...item,
				enabled: true
			}));

			let disabledGoalsArr = [];
			if (disabledPackagesArr && disabledPackagesArr.length) {
				disabledGoalsArr = disabledPackagesArr.map((item) => ({
					...item,
					courseId: item._id,
					enabled: false,
					selected: true
				}))
			}

			let finalGoalsArr = unionBy(enabledGoalsArr, disabledGoalsArr, '_id')
			finalGoalsArr.sort((goalA, goalB) => {
				if (goalA.enabled && !goalB.enabled) {
					return 1
				} else if (!goalA.enabled && goalB.enabled) {
					return -1
				}
				return 0
			});

			// enable my goals
			finalGoalsArr = finalGoalsArr.map(goal => {
				if (
					myGoals && myGoals.filter(myGoal => {
						return get(myGoal, 'courseId') === goal._id
					}).length
				) {
					goal.selected = true
				}
				return goal
			});

			finalGoalsArr = finalGoalsArr.map(goal => ({
				...goal,
				priorty: this.setPriority(goal),
				name: this.setCourseName(goal)
			}));

			const goalData = finalGoalsArr.sort(function (obj1, obj2) {
				return obj1.priorty - obj2.priorty
			});
			finalGoalsArr = goalData.map(goal => ({
				...goal,
				priorty: this.setPriorityAfterPurchase(goal)
			}));

			this.goalsData = finalGoalsArr.sort(function (obj1, obj2) {
				return obj1.priorty - obj2.priorty
			})
		}
	}
	// goalsList is the response fetched from the API
	@action
	async setFinalGoals(goalsList: any) {
		if (!goalsList) {
			return null
		}

		const myGoals = await getGoalsData();
		if (goalsList) {
			let finalGoalsArr = goalsList.map((item: any) => ({
				...item,
				enabled: true
			}));

			finalGoalsArr.sort((goalA: any, goalB: any) => {
				if (goalA.enabled && !goalB.enabled) {
					return 1
				} else if (!goalA.enabled && goalB.enabled) {
					return -1
				}
				return 0
			});

			// enable my goals
			// @ts-ignore
			/*finalGoalsArr = finalGoalsArr.map(goal => {
				// @ts-ignore
				if (myGoals && myGoals.filter(myGoal => {
						return get(myGoal, 'courseId') === goal._id
					}).length
				) {
					goal.selected = true
				}
				return goal
			});

			// @ts-ignore
			finalGoalsArr = finalGoalsArr.map(goal => ({
				...goal,
				priorty: this.setPriority(goal),
				name: this.setCourseName(goal)
			}));

			// @ts-ignore
			const goalData = finalGoalsArr.sort(function (obj1, obj2) {
				return obj1.priorty - obj2.priorty
			});
			// @ts-ignore
			finalGoalsArr = goalData.map(goal => ({
				...goal,
				priorty: this.setPriorityAfterPurchase(goal)
			}));

			// @ts-ignore
			finalGoalsArr.sort(function (obj1, obj2) {
				return obj1.priorty - obj2.priorty
			});*/

			this.goalsData = finalGoalsArr;
			const selectedGoal = await getSelectedGoal();

			// set header exam in top right dropdown
			if (this.currentGoal) {
				await setDropDownGoalId(this.currentGoal.courseId || this.currentGoal._id);
				setDropDownGoalName(this.currentGoal.name);
				this.setSelectedGoalId(this.currentGoal.courseId || this.currentGoal._id);
				commonStores.headerDataStore.setdropdownRightTitle(this.currentGoal.name);
				commonStores.headerDataStore.setdropdownRightTargetGoal(this.currentGoal);
				await this.setCurrentGoalExpired(this.currentGoal);
				this.currentGoal = undefined
			} else {
				if (selectedGoal) {
					await setDropDownGoalId(selectedGoal.courseId || selectedGoal._id);
					setDropDownGoalName(selectedGoal.name);
					this.setSelectedGoalId(selectedGoal.courseId || selectedGoal._id);
					commonStores.headerDataStore.setdropdownRightTitle(selectedGoal.name);
					commonStores.headerDataStore.setdropdownRightTargetGoal(selectedGoal);
					await this.setCurrentGoalExpired(selectedGoal)
				} else {
					if (this.goalsData && this.goalsData.length
						&& get(this.goalsData, '[0].name')) {

						await this.setCurrentGoalExpired(this.goalsData[0]);
						await setDropDownGoalId(this.goalsData[0].courseId);
						setDropDownGoalName(this.goalsData[0].name);
						await this.setSelectedGoalId(this.goalsData[0].courseId);
						commonStores.headerDataStore.setdropdownRightTitle(this.goalsData[0].name);
						commonStores.headerDataStore.setdropdownRightTargetGoal(this.goalsData[0])
					}
				}
			}
		}
	}

	setCourseName(item: any) {
		if (item.name.includes('JEE(ADVANCED)')) {
			return item.name.replace('ADVANCED', 'A')
		} else if (item.name.includes('JEE(MAIN)') || item.name.includes('JEE (MAIN)')) {
			return item.name.replace('MAIN', 'M')
		} else if (item.name.includes('JEE (main)')) {
			return item.name.replace('main', 'M')
		}
		return item.name
	}

	setPriority(item: any) {
		if (item.name.includes('JEE(A)') || item.name.includes('JEE(ADVANCED)')) {
			return 1
		} else if (item.name.includes('JEE(M)') || item.name.includes('JEE(MAIN)')) {
			if (!item.enabled) {
				return 1
			}
			return 2
		} else if (item.name.includes('BITSAT')) {
			if (!item.enabled) {
				return 1
			}
			return 3
		} else {
			return 4
		}
	}

	setPriorityAfterPurchase(item: any) {
		if (item.name.includes('JEE(A)') || item.name.includes('JEE(ADVANCED)')) {
			return 1
		} else if (item.name.includes('JEE(M)') || item.name.includes('JEE(MAIN)')) {
			if (!item.enabled) {
				return 1
			}
			return 2
		} else if (item.name.includes('BITSAT')) {
			if (!item.enabled) {
				return 1
			}
			return 3
		} else {
			if (!item.enabled) {
				return 1
			}
			return 4
		}
	}

	@action
	showGoalDropDown(navigation, currentContext, touchType, componentX, isHorizontal, dataSource, keyOfText, disabledDataSource) {
		// console.warn('goal data store')
		this.isVisible = true;
		this.navigation = navigation;
		this.currentContext = currentContext;
		this.touchType = touchType;
		this.componentX = componentX;
		this.isHorizontal = isHorizontal;
		this.keyOfText = keyOfText;
		this.disabledDataSource = disabledDataSource;
		this.dataSource = dataSource;
		commonStores.headerDataStore.isTriangleDown = false
	}
	@action hideGoalDropDown() {
		this.isVisible = false;
		commonStores.headerDataStore.isTriangleDown = true
	}
	@action
	setSelectedGoalId(goalId) {
		this.selectedGoal = goalId
	}

	getFirstCourseId() {
		return this.selectedGoal
	}
	getIsGoalExpired() {
		return this.currentGoalIsExpired
	}
	async setCurrentGoalExpired(goalData) {
		const userGoals = await getNewUserGoalsData()
		if (userGoals && userGoals.length) {
			const selectedGoal = userGoals.filter(goal => goal.courseId === this.selectedGoal)
			if (selectedGoal && selectedGoal.length > 0) {
				const expiryDate = new Date(selectedGoal[0].expiryDate)
				const currentDate = new Date()
				if (expiryDate !== null) {
					if (currentDate <= expiryDate) {
						this.currentGoalIsExpired = false
					} else {
						this.currentGoalIsExpired = true
					}
				}
			}
		} else {
			this.currentGoalIsExpired = false
		}
	}

	@action
	async onSuccess(apiId: string, response: any) {
		switch (apiId) {
			case API_IDS.GET_GOALS:
				console.warn('GET_GOALS ', response);
				await this.getFinalGoals(response.data);
				break;
			default:
				break
		}
	}

	onSuccessUnAuthorized(apiId: any, response: any) {
		//
	}

	onSuccessBadRequest(apiId: any, response: any) {
		//
	}

	onFailure(apiId: string, request: BaseRequest) {
		//
	}
	onComplete() {
		//
	}
	validateRequestParams(): boolean {
		return true
	}
	validateResponseParams(response: any): boolean {
		return true
	}
	generalValidationError(type: string, error: String): void {
		//
	}
}
