import { action, observable } from 'mobx'

import { NetworkCallbacks, HomeNetworkModule, ProfileActivityNetworkModule } from '../api-layer'
import { BaseResponse, BaseRequest } from '../http-layer'
import { API_IDS, RESPONSE_ERROR_CODE, USER_DETAILS_TYPE } from '../common'
import {
  getName, getProfileImageURL, setNewUserGoalsData, getNewUserGoalsData, getUserId, handleSignIn
} from '../utils'
import stores from '../store'

const DEFAULT_SETTING = {
	// @ts-ignore
  examType: undefined,
  shouldCardUpdate: false,
	// @ts-ignore
  userName: undefined,
	// @ts-ignore
  profileImageUrl: undefined,
  shouldShowProductList: false,
	// @ts-ignore
  questionDataUpdate: undefined,
	// @ts-ignore
  productData: undefined,
  isFromProductModule: false,
	// @ts-ignore
  lastAttemptData: undefined,
	// @ts-ignore
  goalId: undefined,
  forceUpdateAndroid: false,
  forceUpdateIos: false,
  showOldMypat: false
};

export class HomeDataStore implements NetworkCallbacks {
  @observable newsList: any;
  @observable examType: any;
  @observable shouldCardUpdate: any;
  @observable userName: string;
  @observable profileImageUrl: string;
  @observable shouldShowProductList: any;
  @observable questionDataUpdate: any;
  @observable productData: any;
  @observable isFromProductModule: boolean;
  @observable lastAttemptData: any;
  @observable userGoalsList: any;
  @observable showOldMypat: any;
  goalId: any;
  tabType: any;
  buttonName: string;

  i = 0;

  // Force Update Stuff
  @observable forceUpdateAndroid: any;
  @observable forceUpdateIos: any;
  @observable latestBreakingVersionAndroid: any;
  @observable latestBreakingVersionIOS: any;
  @observable appVersionAndroid: any;
  @observable appVersionIOS: any;
  @observable isLoading: any;
  @observable iosAppUrl: any;
  @observable androidAppUrl: any;

  constructor() {
    this.init()
  }

  @action init() {
    this.examType = DEFAULT_SETTING.examType;
    this.shouldCardUpdate = DEFAULT_SETTING.shouldCardUpdate;
    this.userName = DEFAULT_SETTING.userName;
    this.profileImageUrl = DEFAULT_SETTING.profileImageUrl;
    this.shouldShowProductList = DEFAULT_SETTING.shouldShowProductList;
    this.questionDataUpdate = DEFAULT_SETTING.questionDataUpdate;
    this.productData = DEFAULT_SETTING.productData;
    this.isFromProductModule = DEFAULT_SETTING.isFromProductModule;
    this.lastAttemptData = DEFAULT_SETTING.lastAttemptData;
    this.forceUpdateAndroid = DEFAULT_SETTING.forceUpdateAndroid;
    this.forceUpdateIos = DEFAULT_SETTING.forceUpdateIos;
    this.isLoading = true;
    this.showOldMypat = DEFAULT_SETTING.showOldMypat;
    this.tabType = 'Default';
  }

  @action
  async getUserGoalsListData(prefetch = false, studentId: any) {
    const config = { 'methodType': 'GET' };
    const homeNetworkModule = new HomeNetworkModule(config, this);
    await homeNetworkModule.getUserGoals(prefetch, studentId);
  }

  @action setProfileImageUrl(img: any) {
    this.profileImageUrl = img
  }
  @action setShowOldMypatVisible(val: any) {
    this.showOldMypat = val
  }

  async isOldMyPat(goaldata: any) {
    const newUserData = await getNewUserGoalsData();
    if (newUserData) {
      for (let i = 0; i < newUserData.length; i++) {
        let item = newUserData[i];
        if (item.targetId === goaldata.courseId) {
          if (item.isDisable) {
            this.setShowOldMypatVisible(true);
            return
          }
        }
      }
    }
    this.setShowOldMypatVisible(false)
  }

  @action
  async getProfile() {
    const config = { 'methodType': 'GET' };
    const userId = await getUserId();
    const getQueryString = JSON.stringify({ 'userId': userId });
    const profileActivityModule = new ProfileActivityNetworkModule(config, this);
    await profileActivityModule.getProfile(getQueryString);
  }

  @action
  async getFreeTestResult(prefetch = false) {
    const config = { 'methodType': 'GET' };
    const goals: any = stores.goalsDataStore.getFirstCourseId();
    const getQueryString = JSON.stringify({ 'goalId': this.goalId ? this.goalId : goals });
    const homeNetworkModule = new HomeNetworkModule(config, this);
    this.userName = await getName();
    this.profileImageUrl = await getProfileImageURL();
    await homeNetworkModule.getFreeTestResult(getQueryString, prefetch);
  }

  setGoalId(goalId: any) {
    this.goalId = goalId
  }
  getGoalId() {
    return this.goalId
  }

  setTabType(tabType: any) {
    this.tabType = tabType
  }

  getTabType() {
    return this.tabType
  }

  getList() {
    return this.lastAttemptData
  }

  @action
  async onSuccess(apiId: string, response: any) {
    switch (apiId) {
      case API_IDS.GET_FREETEST_RESULT:
        if (response.code !== RESPONSE_ERROR_CODE.NO_TEST_GIVEN) {
          this.questionDataUpdate = response;
          this.shouldCardUpdate = true
        } else {
          this.questionDataUpdate = undefined;
          this.shouldCardUpdate = false
        }
        break;
      case API_IDS.GET_USER_GOALS:
        await setNewUserGoalsData(response.data);
        break;
      case API_IDS.GET_PROFILE:
        await handleSignIn(response, USER_DETAILS_TYPE.DEFAULT, true);
        break;
      default:
        break
    }
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    this.isLoading = false;
  	console.warn('onSuccessUnAuthorized ', apiId , response)
  }

  onSuccessBadRequest(apiId: any, response: any) {
    this.isLoading = false;
    console.warn('onSuccessUnAuthorized ', apiId , response)
  }

  onFailure(apiId: string, request: BaseRequest) {
    this.isLoading = false;
    console.warn('onFailure ', apiId , request)
  }

  onComplete() {
    this.isLoading = false
  }

  validateRequestParams(): boolean {
    return true
  }

  validateResponseParams(response: any): boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }
}
