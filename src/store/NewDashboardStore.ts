import { action, observable, computed } from 'mobx'
// @ts-ignore
import Navigation from 'react-navigation'

import { BaseRequest } from '../http-layer';
import {
  API_IDS
} from '../common'
import { NetworkCallbacks, DashboardNetworkModule } from '../api-layer';

const DEFAULT_SETTING = {
  navigation: undefined,
  dashboardsList: [],
  selectedDashboardId: undefined,
  dashboardsFetched: false,
  dashboardTitle: undefined,
  indexData: {},
  upcomingTests: []
};

export class NewDashboardStore implements NetworkCallbacks {

  @observable dashboardsList: any[];
  navigation: Navigation;
  @observable selectedDashboardId: string;
  @observable dashboardsFetched: boolean;
  @observable dashboardTitle: string;
  userId: string;
  @observable indexData: any;
  @observable upcomingTests: any[];

  constructor() {
    this.init();
  }

  @action
  init() {
    Object.keys(DEFAULT_SETTING).forEach((key) => {
      // @ts-ignore
			this[key] = DEFAULT_SETTING[key];
    });
  }

  @computed get selectedDashboard(): any {
    let dash = {};
    if (this.dashboardsList && this.dashboardsList.length > 0) {
      [...this.dashboardsList].some((obj): any => {
				console.log("selectedDashboard", obj._id);
        if (this.selectedDashboardId === obj._id) {
          dash = obj;
          return true;
        }
      });
    }
    return dash;
  }

  @action
  setNavigationObject(navigation: any) {
    this.navigation = navigation
  }

  @action
  onSuccess(apiId: any, response: any): void {
    this.navigatePage(apiId, response);
  }

  navigatePage(apiId: any, response: any) {
    // console.warn('TEST SCORE DATA', JSON.stringify(response, null, 2))
    switch (apiId) {
      case API_IDS.UPCOMING_TESTS:
        this.upcomingTests = response.data ? response.data.data || [] : [];
        break;
      default:
        break
    }
  }

  @action
  async fetchUpcomingTests(studentId: any) {
    const config = { 'methodType': 'GET' };
    const getDashboardDataModule = new DashboardNetworkModule(config, this);
    await getDashboardDataModule.fetchUpcomingTests(studentId);
  }

  onSuccessBadRequest(apiId: string, response: any) {
    // this.isLoading = false;
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    //
    // this.isLoading = false;
  }

  onFailure(apiId: string, request: BaseRequest) {
    // TODO OnFailure Implementation
    // this.isLoading = false;
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: any): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    // TODO generalValidationError Implementation
  }

  onComplete() {
    // TODO onComplete Implementation
  }
}
