import { action, observable, computed } from 'mobx'
// @ts-ignore
import Navigation from 'react-navigation'
import moment from 'moment'

import { BaseRequest } from '../http-layer'
import {
  API_IDS
} from '../common'
import { NetworkCallbacks, DashboardNetworkModule } from '../api-layer'
import store from '.'
import { getSelectedStudent, getSelectedGoal, setSelectedGoal } from '../utils'
import { GoalsDataStore } from './GoalsDataStore'

const DEFAULT_SETTING = {
	// @ts-ignore
  navigation: undefined,
	// @ts-ignore
	goals: [],
	// @ts-ignore
  greetingMsg: undefined,
  syllabusData: {},
	// @ts-ignore
  assignments: [],
  indexData: {},
  activeGoal: {},
  loadingAssignments: false,
  allAssignmentsFetched: false,
  lastFetchedIndex: -1,
  isSyllabusLoading: false
};

export class NewGoalsDataStore implements NetworkCallbacks {

  @observable goals: any[];
  @observable activeGoal: any;
  @observable greetingMsg: any;
  navigation: Navigation;
  userId: string;
  studentId: string;
  @observable isSyllabusLoading: boolean;
  @observable syllabusData: any;

  @observable assignments: any;
  @observable loadingAssignments: boolean;
  @observable indexData: any
  allAssignmentsFetched: boolean;
  lastFetchedIndex: number;

  constructor() {
    this.init()
  }

  @action
  init() {
    Object.keys(DEFAULT_SETTING).forEach((key) => {
      // @ts-ignore
			this[key] = DEFAULT_SETTING[key]
    })
  }

  @computed get examTimeLeft() {
    let examDate = this.activeGoal ? this.activeGoal.examDate : null;
    if (examDate) {
      let daysDiff = Math.abs(moment().diff(examDate, 'days')), isFuture = moment().isSameOrBefore(examDate);
      return `${daysDiff} Days ${isFuture ? 'Left' : 'Ago'}`
    }
    return ''
  }

  @computed get examDateText() {
    let examDate = this.activeGoal ? this.activeGoal.examDate : null;
    if (examDate) {
      let goalCode = this.activeGoal.goalCode;
      let isFuture = moment().isSameOrBefore(examDate);
      return `${goalCode} ${isFuture ? 'is' : 'was'} on ${moment(examDate).format('Do MMMM YYYY')}`;
    }
    return ''
  }

  @action
  async setActiveGoal(goalId: any) {
    if (this.activeGoal && this.activeGoal.targetId !== goalId) {
      let selectedGoal = this.goals.filter((obj) => obj.targetId === goalId);
      if (selectedGoal && selectedGoal.length > 0) {
        this.activeGoal = selectedGoal[0]
      } else {
				this.activeGoal = this.goals[0];
			}
      await setSelectedGoal(this.activeGoal)
    }
  }

  setGreetingsData(data: any) {
    this.greetingMsg = data
  }

  @action
  setNavigationObject(navigation: any) {
    this.navigation = navigation
  }

  @action
  async onSuccess(apiId: any, response: any, requestParams: any): void {
    await this.navigatePage(apiId, response, requestParams)
  }

  async navigatePage(apiId: any, response: any, requestParams: any) {
    // console.warn('TEST SCORE DATA', apiId, JSON.stringify(response, null, 2))
    switch (apiId) {
      case API_IDS.GET_NEW_GOALS:
        this.goals = response.data;
        const selectedGoal = await getSelectedGoal();
        let selectedGoalId = null;
        if(selectedGoal && selectedGoal.targetId){
          selectedGoalId = selectedGoal.targetId;
        }
        else if (this.goals[0] && this.goals[0].targetId) {
          selectedGoalId = this.goals[0].targetId;
        }
        if(selectedGoalId){
          await this.onGoalSwitch(selectedGoalId);
        }
        break;
      case API_IDS.STUDENT_SUMMARY:
        this.setGreetingsData(response.data);
        break;
      case API_IDS.SYLLABUS_CARD_DATA:
        this.syllabusData = response.data;
        this.isSyllabusLoading = false;
        break;
      case API_IDS.ASSIGNMENT_DATA:
        this.loadingAssignments = false;
				await this._formatAssignmentData(response, requestParams);
        break;
      
      case API_IDS.ACHIEVEMENT_INDEX:
        this.indexData = {
          ...this.indexData,
          'achievement': response.data
        };
        break;
      case API_IDS.SINCERITY_INDEX:
        this.indexData = {
          ...this.indexData,
          'sincerity': response.data
        };
        break;
      case API_IDS.IMPROVEMENT_INDEX:
        this.indexData = {
          ...this.indexData,
          'improvement': response.data
        };
        break;
      default:
        break
    }
  }

  async _formatAssignmentData(response: any, requestParams: any) {
		let urlParams;
		if (Object.keys(requestParams).length !== 0) {
			urlParams = JSON.parse(requestParams);
    }
    if (urlParams && urlParams.startIndex === 0) {
			this.assignments = [];
    }
		response.data.forEach((item: any) => {
      this.assignments.push(item);
    });
		if (response.data.length === 0) {
			this.allAssignmentsFetched = true
		}
		this.lastFetchedIndex = this.assignments.length;
	}

  @action
  async getGoals(studentId: any) {
    this.studentId = studentId;
    const config = { 'methodType': 'GET' };
    const getDashboardDataModule = new DashboardNetworkModule(config, this);
    await getDashboardDataModule.getNewGoalsData(studentId)
  }

  async onGoalSwitch(goalId: any) {
    await this.setActiveGoal(goalId);
    if (!this.studentId) {
      const student = await getSelectedStudent();
      this.studentId = student.id
    }
    this.fetchStudentSummary(this.studentId);
    this.fetchSyllabusData(this.studentId, goalId);
    this.fetchAssignmentData(this.studentId, goalId);
    this.getIndexCard(this.studentId, 'achievement');
    this.getIndexCard(this.studentId, 'improvement');
    this.getIndexCard(this.studentId, 'sincerity');
    store.activityCardDataStore.fetchActivityCard(this.studentId, goalId);
  }

  @action
  async fetchStudentSummary(studentId: any) {
    const config = { 'methodType': 'GET' };
    const getDashboardDataModule = new DashboardNetworkModule(config, this);
    await getDashboardDataModule.fetchStudentSummary(studentId)
  }

  @action
  async fetchSyllabusData(studentId: any, goalId: any) {
    const config = { 'methodType': 'GET' };
    const getDashboardDataModule = new DashboardNetworkModule(config, this);
    this.isSyllabusLoading = true;
    await getDashboardDataModule.fetchSyllabusCardData(studentId, goalId)
  }

  @action
  async fetchAssignmentData(studentId: any, goalId: any, startIndex = 0, limit = 10) {
    const config = { 'methodType': 'GET' };
    const getDashboardDataModule = new DashboardNetworkModule(config, this);
    await getDashboardDataModule.fetchAssignmentData(studentId, goalId, startIndex, limit)
  }

  @action
  async fetchAssignmentsPaginated() {
    if (!this.loadingAssignments && !this.allAssignmentsFetched && this.studentId) {
      this.loadingAssignments = true;
      let goalId = this.activeGoal.targetId;
      await this.fetchAssignmentData(this.studentId, goalId, this.lastFetchedIndex + 1, 10);
    }
  }

  @action
  async getIndexCard(studentId: string, indexId: string) {
    const config = { 'methodType': 'GET' }
    const getDashboardDataModule = new DashboardNetworkModule(config, this)
    await getDashboardDataModule.fetchIndexCardData(studentId, indexId)
  }

  @action
	resetAssignmentsData() {
    this.loadingAssignments = false;
    this.allAssignmentsFetched = false;
    this.lastFetchedIndex = -1
  }

  @action
	async resetActionGoals() {
  	this.activeGoal = {};
	}

  onSuccessBadRequest(apiId: string, response: any) {
    // this.isLoading = false
    switch(apiId) {
      case API_IDS.SYLLABUS_CARD_DATA:
        this.isSyllabusLoading = false;
        break;
    }
    console.log('bad', apiId)
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    //
    // this.isLoading = false
    switch(apiId) {
      case API_IDS.SYLLABUS_CARD_DATA:
        this.isSyllabusLoading = false;
        break;
    }
  }

  onFailure(apiId: string, request: BaseRequest) {
    // TODO OnFailure Implementation
    // this.isLoading = false
    switch(apiId) {
      case API_IDS.SYLLABUS_CARD_DATA:
        this.isSyllabusLoading = false;
        break;
    }
    console.log('fail', apiId)
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: any): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    // TODO generalValidationError Implementation
  }

  onComplete() {
    // TODO onComplete Implementation
  }
}
