import { action, observable, toJS, ObservableMap } from 'mobx'

import { showSnackbar } from '../services'
import { BaseRequest } from '../http-layer'
import stores from '../store'
import { NetworkCallbacks, NotificationNetworkModule } from '../api-layer'
import { API_IDS, NOTIFICATION_TYPE, TYPE, TAB_BAR_PAGE, TAB_TYPE, NOTIFICATION_TYPE_IMAGES, NOTIFICATION_TYPE_IMAGES_SMALL } from '../common';
import commonStores from '../common-library/store'
import { saveDownloadedTest } from '../persistence/utils'

const DEFAULT_SETTING = {
	totalNotificationCount: 0,
	// @ts-ignore
	notificationList: [],
	isLoading: false,
	notifEndReached: false
};

export interface Notification {
	id: string
	userImage: string
	userName: string
	notification: string,
	createdAt: any
	read: boolean
}

export class NotificationStore implements NetworkCallbacks {
	courseId: any;
	@observable totalNotificationCount: any;
	@observable notificationList: any;
	notificationData: any;
	navigation: any;
	notifEndReached: boolean;
	@observable isLoading: any;
	constructor() {
		this.init()
	}

	@action init() {
		this.totalNotificationCount = DEFAULT_SETTING.totalNotificationCount;
		this.notificationList = DEFAULT_SETTING.notificationList;
		this.notificationData = DEFAULT_SETTING.notificationList;
		this.isLoading = DEFAULT_SETTING.isLoading;
		this.notifEndReached = DEFAULT_SETTING.notifEndReached;
	}

	fillNotificationArray(notificationData: any) {
		if(notificationData.length === 0) {
			this.notifEndReached = true;
		} else {
			this.notificationList = this.notificationList.concat(notificationData.map((notificationItem: any) => this.createNotifArray(notificationItem)))
		}
	}

	createNotifArray(notificationItem: any) {
		let notificationObj: Notification;
		notificationObj = {
			id: notificationItem.id,
			userName: notificationItem.userName,
			notification: notificationItem.notification,
			userImage: notificationItem.userImage,
			createdAt: notificationItem.createdAt,
			read: notificationItem.read
		};
		return notificationObj
	}

	@action
	async onSuccess(apiId: any, response: any): void {
		console.log(apiId, response);
		this.isLoading = false;
		switch (apiId) {
			case API_IDS.GET_NOTIFICATION_LIST:
				this.fillNotificationArray(response.data);
				break;
			case API_IDS.GET_NOTIFICATION_COUNT:
				this.totalNotificationCount = response.data;
				commonStores.tabBarDataStore.setTotalNotificationCount(response.data);
				break;
			case API_IDS.SET_GLOBAL_NOTIFICATION_STATUS:
				this.totalNotificationCount = 0;
				commonStores.tabBarDataStore.setTotalNotificationCount(0);
				break;
			case API_IDS.SET_NOTIFICATION_STATUS:
				await this.getNotificationCount();
				await this.getNotificationList(true);
				break;
			default:
				break
		}
	}

	@action
	async getNotificationList(prefetch = false) {
		if(this.notifEndReached || this.isLoading) {
			return;
		}
		this.isLoading = true;
		const config = { 'methodType': 'GET' };
		const notificationNetworkModule = new NotificationNetworkModule(config, this);
		notificationNetworkModule.getNotificationList(prefetch, this.notificationList.length, 10);
	}

	@action
	async updateStatusNotification(notificationId: any, preFetch = false) {
		this.isLoading = true;
		const config = { 'methodType': 'POST' };
		const postBody = JSON.stringify({ 'notificationId': notificationId });
		const notificationNetworkModule = new NotificationNetworkModule(config, this);
		notificationNetworkModule.setNotificationStatus(postBody, true);
	}

	@action
	async updateGlobalStatusNotification() {
		this.isLoading = true;
		const config = { 'methodType': 'POST' };
		const notificationNetworkModule = new NotificationNetworkModule(config, this);
		await notificationNetworkModule.setGlobalNotificationStatus();
	}

	@action
	async getNotificationCount(prefetch = false) {
		this.isLoading = true;
		const config = { 'methodType': 'GET' };
		const notificationNetworkModule = new NotificationNetworkModule(config, this);
		notificationNetworkModule.getNotificationCount(true);
	}

	onSuccessBadRequest(apiId: string, response: any) {
		this.isLoading = false;
		showSnackbar(response.message)
		//  throw new Error("Method not implemented.");
	}

	onSuccessUnAuthorized(apiId: any, response: any) {
		this.isLoading = false
		//
	}

	onFailure(apiId: string, request: BaseRequest) {
		this.isLoading = false
		// TODO OnFailure Implementation
	}

	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: any): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		// TODO generalValidationError Implementation
	}

	onComplete() {
		// TODO onComplete Implementation
	}
	setNavigationObject(navigation) {
		this.navigation = navigation
	}
	@action
	setPushNotificationDataAndAction(notificationItem) {
		let notificationObj: Notification
		if (notificationItem.data && notificationItem.data.length) {
			notificationObj = {
				_id: notificationItem.data._id,
				title: notificationItem.fcm.title,
				body: notificationItem.fcm.body,
				action: notificationItem.action,
				bigImage: NOTIFICATION_TYPE_IMAGES.get(notificationItem.action),
				smallIcon: NOTIFICATION_TYPE_IMAGES_SMALL.get(notificationItem.action),
				userId: notificationItem.userId,
				data: notificationItem.data,
				date: notificationItem.createdAt,
				isViewed: true
			};
			this.notificationList.unshift(notificationObj)
			this.redirectPage(notificationObj)
		}
	}

	@action
	redirectPage(notification) {
		this.updateStatusNotification(notification._id, true)
		/**action: 'assignmentAssigned' ,

		 teacherId: '5b9646ca38234f74bfb3e304',

		 testId: '5ace20a59fe7b01d58073325',

		 assignmentId: 5c88b2d58f9b9c1036234dc6,

		 courseId: '5ace202e9fe7b01d5806bb84' */
		switch (notification.action) {
			case NOTIFICATION_TYPE.PRODUCT_PURCHASE:
				this.productNotificationAction(notification.data)
				break
			case NOTIFICATION_TYPE.USER_WELCOME:
				// ADD TOUR
				commonStores.genericTabBarStore.goToPage(TAB_BAR_PAGE.EXPLORE)
				break
			case NOTIFICATION_TYPE.TEST_LIVE:
				// redirect to test screen which has been live
				break
			case NOTIFICATION_TYPE.TEST_PREP:
				// redirect to Syllabus page on that test
				break
			case NOTIFICATION_TYPE.QUESTION_ASK:
				// redirect to that question
				break
			case NOTIFICATION_TYPE.ANSWER_RECEIVED:
				// this.navigateToQuestionPage(notification.data)        // Redirect to answer screen
				break
			case NOTIFICATION_TYPE.UPVOTE_QUESTION:
			case NOTIFICATION_TYPE.DOWNVOTE_QUESTION:
			case NOTIFICATION_TYPE.ANSWER_VOTE:
			case NOTIFICATION_TYPE.ANSWER_RECEIVED:
				// this.navigateToQuestionPage(notification.data)
				break;
			default:
				break
		}
	}

	productNotificationAction(notification: any) {
		this.courseId = notification.courseId;
	}
}
