import {  observable } from 'mobx'
import { fetchDownloadedTestsCountBySubCategory } from '../persistence/utils'

export class OfflineStorageProgressStore {
  @observable mainTestCount: number
  @observable conceptTestCount: number

  constructor() {
    this.init()
  }

  init() {
    this.getConceptTestCount()
    this.getMainTestCount()
  }

  async getMainTestCount() {
    const options = {
      category: 'main'
    }
    let count = await fetchDownloadedTestsCountBySubCategory(options)
    this.mainTestCount = count
    return count
  }

  async getConceptTestCount() {
    const options = {
      category: 'concept'
    }
    let count = await fetchDownloadedTestsCountBySubCategory(options)
    this.conceptTestCount = count
    return count
  }

  async refresh() {
    await this.getMainTestCount()
    await this.getConceptTestCount()
  }

}