import { action, observable, toJS, computed } from 'mobx'
import { Navigation } from 'react-navigation'

import { BaseRequest, BaseResponse } from '../http-layer'
import { } from '../common'
import { showSnackbar, navigateSimple, resetRouterSimple, getCurrentRouteName } from '../services'
import {
  API_IDS, icons, USER_INFO_TYPE, FROM_PAGE_TYPE, CONGRATS_PAGE_TYPE,
  DIALOG_DESCRIPTION, RESPONSE_ERROR_CODE
} from '../common'
import {
  setAuthToken, updateMobileDetails, updateEmailDetails, getFirstGoalID,
  getPackages, getFirstCourseId, isFiitjeeLoginType
} from '../utils'
import { showDialog } from '../services'
import { NetworkCallbacks, DashboardNetworkModule } from '../api-layer'

const DEFAULT_SETTING = {
  navigation: undefined,
  performanceList: [],
  goalProgressList: [],
  subjectName: undefined,
  score: 0,
  xStartPoint: 0,
  xEndPoint: 0,
  upcomingTestList: [],
  test: '',
  type: 'totalScore',
  goalId: undefined,
  switchType: 'month',
  showProgress: true
}

export class PerformanceStore implements NetworkCallbacks {

  @observable performanceList: any[]
  @observable upcomingTestList: any[]
  @observable goalProgressList: any[]
  @observable xStartPoint
  @observable isLoading
  @observable xEndPoint
  @observable goalName
  switchType = 'month'
  goalId
  isPurchased
  test = ''
  type = 'totalScore'
  navigation: Navigation
  showProgress

  constructor() {
    this.init()
  }

  setSwitchType(type) {
    this.switchType = type
  }

  getSwitchType() {
    return this.switchType
  }

  setCurrentTestType(testType) {
    this.type = testType
  }

  getCurrentTestType() {
    return this.type
  }

  setCurrentTest(test) {
    this.test = test
  }

  getCurrentTest() {
    return this.test
  }
  @action
  setGoalName(goalName) {
    this.goalName = goalName
  }
  getGoalName() {
    return this.goalName
  }
  setGoalId(goalId) {
    this.goalId = goalId
  }

  getGoalId() {
    return this.goalId
  }

  setPurchased(isPurchased) {
    this.isPurchased = isPurchased
  }

  getShowProgress() {
    return this.showProgress
  }
  getIsPurchased() {
    return this.isPurchased
  }

  @action
  async init() {
    this.performanceList = DEFAULT_SETTING.performanceList
    this.upcomingTestList = DEFAULT_SETTING.upcomingTestList
    this.goalProgressList = DEFAULT_SETTING.goalProgressList
    this.xEndPoint = DEFAULT_SETTING.xEndPoint
    this.xStartPoint = DEFAULT_SETTING.xStartPoint
    this.navigation = DEFAULT_SETTING.navigation
    this.test = DEFAULT_SETTING.test
    this.type = DEFAULT_SETTING.type
    this.goalId = DEFAULT_SETTING.goalId
    this.switchType = DEFAULT_SETTING.switchType
    this.isLoading = true
  }

  getValue(value: any, defaultValue: any = undefined): any {
    if (value) {
      return toJS(value)
    }
    return defaultValue
  }

  getPerformanceList(): any[] {
    const data =  this.getValue(this.performanceList)
    // console.warn(JSON.stringify(data))
    return data
  }
  getUpcomingExamList(): any[] {
    return this.getValue(this.upcomingTestList)

  }
  getGoalProgressList(): any[] {
    return this.getValue(this.goalProgressList)

  }

  @action
  setStartPoint(xStartPoint) {
    this.xStartPoint = xStartPoint
  }

  getStartPoint() {
    return this.xStartPoint
  }

  @action
  setEndPoint(xEndPoint) {
    this.xEndPoint = xEndPoint
  }
  getEndPoint() {
    return this.xEndPoint
  }
  @action
  setNavigationObject(navigation) {
    this.navigation = navigation
  }

  @action
  onSuccess(apiId: any, response: any): void {
    this.isLoading = false;
    this.navigatePage(apiId, response)
  }

  @computed get maxValue(): any {
    let maxV = 0.0
    this.performanceList.map((subjectData) => {
      let score = subjectData.score
      if ((score * 60) > maxV)
        maxV = score * 60
      if (maxV > 0) {
        maxV = maxV + maxV * .1
        maxV = Math.round(maxV)
        if (maxV % 2 !== 0)
          maxV = maxV + 1
      } else if (maxV === 0) {
        maxV = 1
      } else {
        maxV = 1 * maxV
      }
    })
    return maxV
  }
  navigatePage(apiId, response) {
    // console.warn('TEST SCORE DATA', JSON.stringify(response, null, 2))
    switch (apiId) {
      case API_IDS.GOAL_PROGRESS:
        this.goalProgressList = response.data
        this.showProgress = true
        if (this.goalProgressList && this.goalProgressList.length > 0) {
        this.goalProgressList.forEach(element => {
          element.idealProgress = element.idealProgress || 0
          element.studentProgress = element.studentProgress || 0
        })
      }
        if (this.goalProgressList.length < 7) {
          this.xStartPoint = 0
          this.xEndPoint = this.goalProgressList.length
        } else {
          this.xStartPoint = 0
          this.xEndPoint = 7
        }
        break
      case API_IDS.UPCOMING_EXAMS:
        this.upcomingTestList = response.data.test
        break
      default:
        break
    }
  }

  @action
  async getGoalProgress(type, goalId, prefetch = false) {
    const config = { 'methodType': 'GET' };
    const postBody = JSON.stringify({ 'type': type, 'goalId': goalId });
    const getPerformanceDataModule = new DashboardNetworkModule(config, this);
    await getPerformanceDataModule.getGoalProgressData(postBody, prefetch);
  }

  @action
  async getUpcomingExam(goalId, prefetch = false) {
    const isFiitJEEStudent = await isFiitjeeLoginType();
    const userType = isFiitJEEStudent === 'true' ? 'b2b' : 'b2c';
    const config = { 'methodType': 'GET' };
    const postBody = JSON.stringify({ goalId, type: userType });
    const getPerformanceDataModule = new DashboardNetworkModule(config, this);
    await getPerformanceDataModule.getUpcomingExamData(postBody, prefetch)
  }

  onSuccessBadRequest(apiId: string, response: any) {

    this.isLoading = false;
    if (apiId === API_IDS.GOAL_PROGRESS) {
      this.showProgress = response.code === RESPONSE_ERROR_CODE.BAD_REQUEST ? false : true
      this.goalProgressList = []
    }
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    //
    this.isLoading = false
  }

  onFailure(apiId: string, request: BaseRequest) {
    // TODO OnFailure Implementation
    this.isLoading = false
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: any): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    // TODO generalValidationError Implementation
  }

  onComplete() {
    // TODO onComplete Implementation
  }
}
