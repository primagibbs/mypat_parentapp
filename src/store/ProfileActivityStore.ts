import { action, observable, toJS } from 'mobx';
import { ProfileActivityNetworkModule } from '../api-layer';
import { BaseResponse, BaseRequest } from '../http-layer';
import { API_IDS, API_END_POINTS, icons } from '../common';
import store from '../store';
import { getUserId } from '../utils';
import { NetworkCallbacks } from '../api-layer';
import { DataFilterStore } from '../common-library/store';
import { findIndex } from 'lodash';
import { showSnackbar, getCurrentRouteName } from '../services';

const DEFAULT_SETTING = {
  getActivityDataResponse: undefined,
  getFollowersDataResponse: undefined,
  getFollowingDataResponse: undefined,
  getProfileDataResponse: undefined,
  lockActivityStatus: false,
  profileImageUrl: undefined,
  currentPastActivityId: undefined,
  myProfileDetails: undefined
};

const DEFAULT_OFFSET = 0;
const DEFAULT_PAGE_SIZE = 10;

export class ProfileActivityStore {
  constructor() {
    // super();
    this.init();
    // this.setPaginationProps('skip', DEFAULT_OFFSET);
    // this.setPaginationProps('limit', DEFAULT_PAGE_SIZE);
  }

	@observable profileImageUrl;
	@observable getActivityDataResponse;
	@observable getFollowersDataResponse;
	@observable getFollowingDataResponse;
	@observable getProfileDataResponse;
	@observable myProfileDetails;
	currentPastActivityId;
	callClever;

  @action init() {
    // super.init();
    this.getActivityDataResponse = DEFAULT_SETTING.getActivityDataResponse;
    this.getFollowersDataResponse = DEFAULT_SETTING.getFollowersDataResponse;
    this.getFollowingDataResponse = DEFAULT_SETTING.getFollowingDataResponse;
    this.getProfileDataResponse = DEFAULT_SETTING.getProfileDataResponse;
    this.myProfileDetails = DEFAULT_SETTING.myProfileDetails;
    this.profileImageUrl = DEFAULT_SETTING.profileImageUrl;
    this.currentPastActivityId = DEFAULT_SETTING.currentPastActivityId;
    this.callClever = false;
  }

  @action
  setSortProp(key, value) {
    this.sortProps.set(key, value)
  }

  @action
  setFilterProp(key, value) {
    this.filterProps.set(key, value)
  }

  @action
  setPaginationProp(key, value) {
    this.paginationProps.set(key, value)
  }

  @action
  resetSortProps() {
    this.sortProps.clear()
  }

  @action
  resetPaginationProps() {
    this.paginationProps.clear()
  }

  @action
  resetFilterProps() {
    this.filterProps.clear()
  }

  @action
  resetAll() {
    this.sortProps.clear()
    this.filterProps.clear()
    this.paginationProps.clear()
  }

  @action
  resetStore() {
    this.init()
  }

  async getActivityDetails() {
    const config = { 'methodType': 'GET' }
    const userId = await getUserId()
    const filterProps = this.getFilterProps()
    const paginationProps = this.getPaginationProps()
    const getQueryString = JSON.stringify({ ...filterProps, ...paginationProps, 'userId': userId })
    const profileActivityModule = new ProfileActivityNetworkModule(config, this)
    await profileActivityModule.getActivity(getQueryString)
  }

  async  removeProfilePic() {
    const userId = await getUserId()
    const config = { 'methodType': 'POST' }
    const postBody = JSON.stringify({ 'user_id': userId })
    const profileActivityModule = new ProfileActivityNetworkModule(config, this)
    await profileActivityModule.removeImage(postBody)
  }

  async getProfile() {
    const config = { 'methodType': 'GET' };
    const userId = await getUserId();
    const getQueryString = JSON.stringify({ 'userId': userId });
    const profileActivityModule = new ProfileActivityNetworkModule(config, this);
    await profileActivityModule.getProfile(getQueryString)
  }

  @action
  async uploadImage(imgExt, imageUri) {
    const config = { 'methodType': 'POST' }
    const postBody = JSON.stringify({ 'extension': imgExt, 'file': imageUri })
    const profileActivityModule = new ProfileActivityNetworkModule(config, this)
    await profileActivityModule.uploadImage(postBody)
  }

  async getLockActivity(pastActivityId: string) {
    this.currentPastActivityId = pastActivityId
    const config = { 'methodType': 'POST' }
    const getQueryString = JSON.stringify({ 'pastActivityId': pastActivityId })
    const profileActivityModule = new ProfileActivityNetworkModule(config, this)
    await profileActivityModule.getLockActivity(getQueryString)
  }

  async getUnLockActivity(pastActivityId: string) {
    this.currentPastActivityId = pastActivityId
    const config = { 'methodType': 'POST' }
    const getQueryString = JSON.stringify({ 'pastActivityId': pastActivityId })
    const profileActivityModule = new ProfileActivityNetworkModule(config, this)
    await profileActivityModule.getUnLockActivity(getQueryString)
  }

  async getMyProfileDetails(userId) {
    const config = { 'methodType': 'GET' };
    const profileActivityModule = new ProfileActivityNetworkModule(config, this);
    await profileActivityModule.getMyProfileDetails(userId);
  }

  getActivityName(activity) {
    switch (activity) {
      case '':
        return 'All Activity'
      case 'Challenges':
        return 'challenges'
      case 'Community':
        return 'Community'
      case 'Result':
        return 'Results'
      case 'Achievements':
        return 'Achievements'
      case 'Purchase':
        return 'Purchases'
      default:
        return 'All Activity'
    }
  }
  @action
  onSuccess(apiId: string, response: any) {
    switch (apiId) {
      case API_IDS.UPLOAD_IMAGE:
        this.profileImageUrl = response.url;
        store.homeDataStore.setProfileImageUrl(response.url);
        showSnackbar(response.message);
        break;
      case API_IDS.REMOVE_IMAGE:
        this.profileImageUrl = undefined
        store.homeDataStore.setProfileImageUrl(undefined)
        showSnackbar(response.message)
        break;
      case API_IDS.GET_ACTIVITY:
        const filterProps = this.getFilterProps()
        this.getActivityDataResponse = response.data;
        break;
      case API_IDS.GET_PROFILE:
        this.profileImageUrl = response.data.photoUrlThumb || undefined;
        this.getProfileDataResponse = response.data;
        break;
      case API_IDS.ACTIVITY_LOCK:
        showSnackbar('This post is private. Cannot be viewed by public');
        this.updateLockingStatus(response.code === 200);
        break;
      case API_IDS.ACTIVITY_UNLOCK:
        showSnackbar('This post is public. Can be viewed by public');
        this.updateLockingStatus(response.code === 200);
        break;
      case API_IDS.MY_PROFILE:
        this.myProfileDetails = response.data;
        break;
      default:
        break
    }
  }

  @action updateLockingStatus(lockingStatus) {
    const data = this.getActivityDataResponse.activities
    const index = findIndex(data, { _id: this.currentPastActivityId })
    if (index !== undefined && index !== -1) {
      const item = data[index]
      item.isLocked = !item.isLocked
      data.splice(index, 1, item)
      this.getActivityDataResponse.activities = data
    }
  }

  getActivityData() {
    return toJS(this.getActivityDataResponse)
  }

  getFollowersData() {
    return toJS(this.getFollowersDataResponse)
  }

  getFollowingData() {
    return toJS(this.getFollowingDataResponse)
  }

  getProfileData() {
    return toJS(this.getProfileDataResponse)
  }

  onSuccessUnAuthorized(apiId: string, response: any) {
    //
  }

  onSuccessBadRequest(apiId: any, response: any) {
    //
  }

  onFailure(apiId: string, request: BaseRequest) {
    //
  }
  onComplete() {
    //
  }
  validateRequestParams(): boolean {
    return true
  }
  validateResponseParams(response: any): boolean {
    return true
  }
  generalValidationError(type: string, error: String): void {
    //
  }

}
