// @ts-ignore
import Navigation from 'react-navigation';
import { action, observable } from 'mobx';
import {AsyncStorage, Platform} from 'react-native';
import transport from '../common-library/events';
import {
	handleSignIn,
	updateMobileDetails,
	getDeviceToken,
	setAuthToken, USER_KEY
} from '../utils';
import { resetRouterSimple, navigateSimple, showSnackbar, goBackAndRefresh } from '../services';
import {
	AuthNetworkModule,
	NetworkCallbacks,
	ProfileActivityNetworkModule,
	ProfileNetworkModule
} from '../api-layer';
import { BaseRequest, BaseResponse } from '../http-layer';
import {
	API_IDS, USER_DETAILS_TYPE
} from '../common';
import store from "./index";
import FCM from "react-native-fcm";
import commonStores from '../common-library/store';

interface Mobile {
	number: string,
	isVerified: boolean
}

interface Email {
	id: string,
	isVerified: boolean
}

const DEFAULT_SETTING = {
	// @ts-ignore
	id: undefined,
	// @ts-ignore
	name: undefined,
	// @ts-ignore
	profileImage: undefined,
	// @ts-ignore
	mobileObj: undefined,
	// @ts-ignore
	emailObj: undefined,
	isLoading: false,
	isModal: false,
	isCodeMismatch: false,
	// @ts-ignore
	emailId: undefined,
	// @ts-ignore
	mobileNumber: undefined,
	// @ts-ignore
	isVerified: undefined,
	// @ts-ignore
	enrollmentNo: undefined,
	// @ts-ignore
	countryCode: undefined,
	// @ts-ignore
	selectedStudent: undefined,
	// @ts-ignore
	isStudentAdded: undefined,
	// @ts-ignore
	isOTPWrong: undefined,
	// @ts-ignore
	profileImageUrl: undefined,
	// @ts-ignore
	userProfileDetails: undefined
};

export class ProfileDataStore implements NetworkCallbacks {
	emailId: any;
	countryCode: any;
	mobileNumber: number;
	enrollmentNo: string;
	isVerified: any;
	serverTime: any;
	@observable id: any;
	@observable name: any;
	@observable profileImage: any;
	@observable mobileObj: Mobile;
	@observable selectedStudent: any;
	@observable emailObj: Email;
	@observable isLoading: boolean = false;
	@observable isModal: boolean = false;
	@observable isCodeMismatch: boolean = false;
	@observable isStudentAdded: boolean = true;
	@observable isOTPWrong: boolean = false;
	@observable profileImageUrl: string;
	@observable userProfileDetails: any;

	userInfoType: string;
	navigation: Navigation;

	constructor() {
		this.init()
	}

	@action init() {
		this.id = DEFAULT_SETTING.id;
		this.name = DEFAULT_SETTING.name;
		this.profileImage = DEFAULT_SETTING.profileImage;
		this.mobileObj = DEFAULT_SETTING.mobileObj;
		this.emailObj = DEFAULT_SETTING.emailObj;
		this.isLoading = DEFAULT_SETTING.isLoading;
		this.isModal = DEFAULT_SETTING.isModal;
		this.emailId = DEFAULT_SETTING.emailId;
		this.mobileNumber = DEFAULT_SETTING.mobileNumber;
		this.isVerified = DEFAULT_SETTING.isVerified;
		this.enrollmentNo = DEFAULT_SETTING.enrollmentNo;
		this.countryCode = DEFAULT_SETTING.countryCode;
		this.selectedStudent = DEFAULT_SETTING.selectedStudent;
		this.isStudentAdded = DEFAULT_SETTING.isStudentAdded;
		this.isOTPWrong = DEFAULT_SETTING.isOTPWrong;
		this.profileImageUrl = DEFAULT_SETTING.profileImageUrl;
	}

	setNavigationObject(navigation: any) {
		this.navigation = navigation
	}

	setUserInfoType(userInfoType: any) {
		this.userInfoType = userInfoType
	}

	setSelectedStudent(student: any) {
		this.selectedStudent = student;
	}

	setUserProfileDetails(details: any) {
		this.userProfileDetails = details;
	}

	@action
	async uploadImage(imgExt: string, imageUri: string) {
		const config = { 'methodType': 'POST' };
		const postBody = JSON.stringify({ 'extension': imgExt, 'file': imageUri });
		const profileActivityModule = new ProfileActivityNetworkModule(config, this);
		await profileActivityModule.uploadImage(postBody)
	}

	@action
	async updateUserDataViaSignup(signUpParams: any) {
		// tslint:disable-next-line:max-line-length
		const obj = {
			'mobileNumber': signUpParams.userInfo,
			'source': Platform.OS === 'ios' ? 'ios' : 'android',
			'deviceToken': await getDeviceToken(),
			'countryCode': signUpParams.countryCode ? `+${signUpParams.countryCode}` : '+91'
		};

		const config = { methodType: 'POST' };
		const authNetworkModule = new AuthNetworkModule(config, this);
		await authNetworkModule.authUserSignup(JSON.stringify(obj));
		await updateMobileDetails(undefined, obj.mobileNumber, false, obj.countryCode);
	}

	@action
	async updateName(userDetails: any) {
		try {
			const config = {
				'methodType': 'PUT'
			};
			const postBody = JSON.stringify(userDetails);
			const profileNetworkModule = new ProfileNetworkModule(config, this);
			await profileNetworkModule.setName(postBody)
		} catch (error) {
			//
		}
	}

	@action
	async updateUser(userDetails: any) {
		try {
			const config = {
				'methodType': 'PUT'
			};
			const postBody = JSON.stringify(userDetails);
			const profileNetworkModule = new ProfileNetworkModule(config, this);
			await profileNetworkModule.setDetails(postBody)
		} catch (error) {
			//
		}
	}

	@action
	async updateEmail(userDetails: any) {
		try {
			const config = {
				'methodType': 'PUT'
			};
			const postBody = JSON.stringify(userDetails);
			const profileNetworkModule = new ProfileNetworkModule(config, this);
			await profileNetworkModule.setEmail(postBody)
		} catch (error) {
			//
		}
	}

	@action
	async updateUserDataViaLogin(loginParams: object) {
		let obj = {
			// @ts-ignore
			'userId': loginParams.userId,
			// @ts-ignore
			'mobileNumber': loginParams.userInfo,
			'source': Platform.OS === 'ios' ? 'ios' : 'android',
			'deviceToken': await getDeviceToken(),
			'countryCode': '+91'
		};

		const config = { 'methodType': 'POST' };
		await updateMobileDetails(undefined, obj.mobileNumber, obj.userId, undefined);
		const authNetworkModule = new AuthNetworkModule(config, this);
		await authNetworkModule.authUserLogin(JSON.stringify(obj));
	}

	@action
	async verifyOTP(otpParams: any) {
		const fcmToken = await FCM.getFCMToken();
		let obj = {
			mobile: otpParams.mobile,
			OTP: otpParams.OTP,
			fcmToken
		};
		if (otpParams.userId !== null) {
			// @ts-ignore
			obj.userId = otpParams.userId;
		}
		const config = { 'methodType': 'POST' };
		const authNetworkModule = new AuthNetworkModule(config, this);
		await authNetworkModule.verifyOTP(JSON.stringify(obj));
	}

	@action
	async findStudent(studentInfo: string, accessCode: string) {
		const config = { 'methodType': 'GET' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.getStudent({
			urlParams: {
				studentInfo,
				accessCode
			}
		});
	}

	@action
	async validateStudentInfo(studentInfo: string) {
		const config = { 'methodType': 'GET' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.validateStudent({
			studentInfo
		});
	}

	@action
	async findStudentByOTP(studentInfo: string, otp: string) {
		const config = { 'methodType': 'GET' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.getStudentByOTP({
			urlParams: {
				studentInfo,
				otp
			}
		});
	}

	@action
	async sendOTPForMobileChange(mobileNumber: number) {
		this.isLoading = true;
		const obj = {
			countryCode: "+91",
			mobileNumber
		};
		const config = { 'methodType': 'POST' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.sendOTPForMobileChange(JSON.stringify(obj));
	}

	@action
	async verifyOTPForMobileChange(mobile: number, otp: number) {
		const config = { 'methodType': 'POST' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.verifyOTPForMobileChange(JSON.stringify({
			mobile,
			OTP: otp
		}));
	}

	@action
	async logout() {
		const config = { 'methodType': 'POST' };
		const fcmToken = await FCM.getFCMToken();
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.logout(JSON.stringify({fcmToken}));
	}

	@action
	async addStudent(queryParams: any) {
		const config = { 'methodType': 'POST' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.addStudent(JSON.stringify(queryParams));
	}

	@action
	async deleteStudent(studentId: string) {
		const config = { 'methodType': 'DELETE' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.deleteStudent(studentId);
	}

	@action
	async sendStudentOTP(studentInfo: any) {
		this.isLoading = true;
		const obj = {
			studentInfo: studentInfo
		};
		const config = { 'methodType': 'POST' };
		const profileNetworkModule = new ProfileNetworkModule(config, this);
		await profileNetworkModule.sendStudentOTP(JSON.stringify(obj));
	}

	@action
	async onSuccess(apiId: any, response: any) {
		console.log("onSuccess", apiId, response);
		switch (apiId) {
			case API_IDS.LOGIN: {
				if (response && response.data && !response.data.sent) {
					if (response.data.message) {
						// showSnackbar(response.data.message)
					} else {
						// showSnackbar('Something went wrong')
					}
				} else {
					await this.navigateAfterLogin(response.data);
				}
				break;
			}
			case API_IDS.VERIFY_OTP: {
				if (response && response.data && !response.data.verified) {
					this.isOTPWrong = true;
				} else {
					if(response.data && response.data.profile) {
						await handleSignIn(response, USER_DETAILS_TYPE.LOGIN);
						this.setUserProfileDetails(response.data.profile);
						await this.navigateAfterOTPVerification(response);
					} else {
						showSnackbar('Something went wrong');
						this.gotoLoginScreen();
					}
				}
				break;
			}
			case API_IDS.VERIFY_PROFILE_OTP: {
				if (response && response.data && !response.data.verified) {
					this.isOTPWrong = true;
				} else {
					if (response.data && response.data.profile) {
						this.setUserProfileDetails(response.data.profile);
					}
					goBackAndRefresh(this.navigation);
				}
				break;
			}
			case API_IDS.SIGN_UP:
				if (response && response.data && !response.data.sent) {
					if (response.data.message) {
						showSnackbar(response.data.message)
					} else {
						showSnackbar('Something went wrong')
					}
				} else {
					this.navigateAfterSignup(response.data);
				}
				break;
			case API_IDS.VALIDATE_PROFILE:
				if(response.data && response.data.valid) {
					await this.navigateAfterValidateStudent(response);
				} else {
					showSnackbar(response.data.message);
				}
				break;
			case API_IDS.GET_PROFILE:
				if(response.data && response.data.profile) {
					let profileData = response.data.profile;
					this.setUserProfileDetails(profileData);
					await this.navigateAfterOTPVerification(response);
				} else {
					showSnackbar('Something went wrong');
					this.gotoLoginScreen();
				}
				break;
			case API_IDS.STUDENT_PROFILE: {
				await this.navigateAfterStudentProfile(response);
				break;
			}
			case API_IDS.SET_PROFILE:
				if(response.data && response.data) {
					this.setUserProfileDetails(response.data.profile);
					await AsyncStorage.setItem(USER_KEY, JSON.stringify(response.data));
				}
				await this.navigateAfterName(response.data);
				break;
			case API_IDS.SET_EMAIL:
				if(response.data && response.data) {
					this.setUserProfileDetails(response.data.profile);
					await AsyncStorage.setItem(USER_KEY, JSON.stringify(response.data));
				}
				await this.navigateAfterEmail(response.data);
				break;
			case API_IDS.UPDATE_PROFILE:
				if(response.data && response.data) {
					this.setUserProfileDetails(response.data.profile);
					await AsyncStorage.setItem(USER_KEY, JSON.stringify(response.data));
				}
				break;
			case API_IDS.ADD_STUDENT:
				if(response.data && response.data.profile) {
					this.setUserProfileDetails(response.data.profile);
				}
				await AsyncStorage.setItem(USER_KEY, JSON.stringify(response.data));
				this.isStudentAdded = true;
				// this.gotoHomePage();
				break;
			case API_IDS.SEND_STUDENT_OTP:
				break;
			case API_IDS.UPLOAD_IMAGE:
				this.profileImageUrl = response.data.userImage;
				this.userProfileDetails.userImage = response.data.userImage;
				store.homeDataStore.setProfileImageUrl(response.data.userImage);
				showSnackbar(response.message);
				break;
			case API_IDS.DELETE_STUDENT:
				if(response.data && response.data.studentProfiles) {
					this.userProfileDetails.studentProfiles = response.data.studentProfiles
				}
				await AsyncStorage.setItem(USER_KEY, JSON.stringify({profile: response.data}));
				break;
			case API_IDS.SEND_PROFILE_OTP:
				if (response.data && !response.data.sent) {
					showSnackbar(response.data.message);
				} else {
					navigateSimple(this.navigation, 'UpdateProfileOTPPage', {mobileNumber: response.data.mobile});
				}
				break;
			default:
				break
		}
	}

	getUserProfileImageName(userName: string) {
		if (userName) {
			let name = userName.trim().replace(/\s\s+/g, ' ').toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
			if (name && name.length > 1) {
				let regex = /(Mr|MR|Ms|Miss|Mrs|Dr|Sir)(\.?)\s/;
				let match = regex.exec(name),
					parsedName = '';
				(match !== null) ? parsedName = name.replace(match[0], '') : parsedName = name;
				const nameArray = parsedName.trim().split(/\s+/);
				if (nameArray.length > 2) {
					// const refinedName = `${nameArray[0][0]} ${nameArray[1][0]} ${nameArray[nameArray.length - 1]}`
					return `${nameArray[0]} ${nameArray[nameArray.length - 1]}`;
				} else {
					return parsedName
				}
			}
		}
		return 'UN'
	}

	navigateAfterSignup(response: any) {
		this.gotoOTPVerificationScreen(response, 'signup');
	}

	async navigateAfterLogin(response: any) {
		this.gotoOTPVerificationScreen(response, 'login');
	}

	async navigateAfterValidateStudent(response: any) {
		const { data } = response;
		this.gotoAddProfile2Screen(data);
	}

	async navigateAfterOTPVerification(response: any) {
		const { data: { profile } } = response;
		if (response.data.firstTimeLogin) {
			this.isModal = true;
		} else if (profile.name === '') {
			this.gotoAddInfoScreen();
		} else if (profile.email === '') {
			this.gotoEmailScreen();
		} else if (profile.studentProfiles && profile.studentProfiles.length === 0) {
			this.gotoProfileScreen();
		} else if (profile.studentProfiles 
					&& profile.studentProfiles.length === 1
					&& !profile.studentProfiles[0].isVerified) {
			let data1 = profile.studentProfiles[0];
			console.log("navigateAfterOTPVerification", "ViewProfileScreen");
			this.gotoViewProfileScreen({  data: data1 });
		} else {
			this.gotoHomePage();
		}
	}

	setCodeMismatch() {
		this.isCodeMismatch = false;
	}

	setIsStudentAdded() {
		this.isStudentAdded = false;
	}

	setIsOTPWrong() {
		this.isOTPWrong = false;
	}

	setIsModal() {
		this.isModal = false;
	}

	async navigateAfterStudentProfile(response: any) {
		if (response.data === null) {
			this.isCodeMismatch = true;
		} else {
			const { data } = response;
			console.log("navigate", data);
			this.gotoViewProfileScreen({
				data
			});
		}
	}

	async navigateAfterViewStudentProfile(data: any) {
		const { id, accessCode } = data;
		this.gotoGaurdianScreen({
			studentId: id,
			accessCode
		});
	}

	async navigateAfterName(response: any) {
		this.gotoEmailScreen();
	}

	async navigateAfterEmail(response: any) {
		console.log(response);
		if (response.profile && response.profile.studentProfiles && response.profile.studentProfiles.length > 0) {
			if(!response.profile.studentProfiles[0].relation){
				let data = response.profile.studentProfiles[0];
				this.gotoViewProfileScreen({  data })
			}else{
				this.gotoHomePage();
			}
		} else {
			this.gotoProfileScreen();
		}

	}

	async onSuccessUnAuthorized(apiId: any, response: any) {
		// console.log("onSuccessUnAuthorized", response)
		if (apiId === API_IDS.LOGIN) {
			this.gotoRegisterScreen();
		}
	}

	onSuccessBadRequest(apiId: any, response: any) {
		// console.log("onSuccessBadRequest", response)
		if (apiId === API_IDS.LOGIN) {
			transport.setData('Sign In', { 'Status': false, 'Device Type': Platform.OS });
			transport.post()
		} else if (apiId === API_IDS.SIGN_UP) {
			transport.setData('Sign Up', { 'Status': false, 'Device Type': Platform.OS });
			transport.post()
		}
		showSnackbar(response.message)
	}

	onFailure(apiId: string, request: BaseRequest) {
		console.log("Failed", request);
	}

	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: BaseResponse): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		//
	}

	onComplete() {
		console.log("onComplete")
		//
	}

	gotoLoginScreen() {
		navigateSimple(this.navigation, 'LoginPage')
	}

	gotoRegisterScreen() {
		navigateSimple(this.navigation, 'SignupPage')
	}

	gotoAddInfoScreen() {
		navigateSimple(this.navigation, 'AddInfoPage');
	}

	gotoEmailScreen() {
		navigateSimple(this.navigation, 'AddEmailPage');
	}

	gotoProfileScreen() {
		navigateSimple(this.navigation, 'AddProfile1Page');
	}

	gotoAddProfile2Screen(data: any) {
		navigateSimple(this.navigation, 'AddProfile2Page', {
			studentInfo: data.studentInfo,
			mobileExists: data.mobileExists,
			mobileNumber: data.mobileNumber
		});
	}

	gotoViewProfileScreen(data: any) {
		console.log("gotoViewProfileScreen", data);
		navigateSimple(this.navigation, 'AddProfileViewPage', data);
	}

	gotoGaurdianScreen(response: any) {
		navigateSimple(this.navigation, 'AddProfile3Page', response);
	}

	gotoOTPVerificationScreen(response: any, previous_screen: string) {
		navigateSimple(this.navigation, 'OTPVerificationPage', {mobile: response.mobile, previous_screen})
	}

	gotoHomePage() {
		resetRouterSimple(this.navigation, 0, 'HomePage')
	}
}
