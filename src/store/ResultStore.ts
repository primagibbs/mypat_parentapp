import { observable, action, toJS } from 'mobx'

import { FROM_PAGE_TYPE, API_IDS, TAB_TYPE } from '../common'
import { NetworkCallbacks, AssignmentNetworkModule } from '../api-layer'
import { BaseResponse, BaseRequest } from '../http-layer'

const DEFAULT_SETTING = {
  testId: undefined,
  testName: undefined,
  attempts: undefined,
  testType: undefined,
  courseId: undefined,
  navigation: undefined,
  attemptList: undefined

}

export class ResultStore implements NetworkCallbacks {

  @observable isVisible = false
  isAssignment
  assignmentId
  testName
  attempts
  @observable attemptList
  testType
  courseId
  navigation
  testId
  tabType
  constructor() {
    this.init()
  }

  @action init() {
    this.assignmentId = DEFAULT_SETTING.testId
    this.testName = DEFAULT_SETTING.testName
    this.attempts = DEFAULT_SETTING.attempts
    this.testType = DEFAULT_SETTING.testType
    this.courseId = DEFAULT_SETTING.courseId
    this.navigation = DEFAULT_SETTING.navigation
    this.testId = undefined
    this.tabType = undefined
    this.attemptList = undefined
  }

  @action showResultScreen(navigation, testType, assignmentId, testName, attempts, courseId, testId, tabType, isAssignment) {
    this.isVisible = true
    this.navigation = navigation
    this.assignmentId = assignmentId
    this.testName = testName
    this.testType = testType
    this.courseId = courseId
    this.attempts = attempts
    this.isAssignment = isAssignment
    this.testId = testId
    this.tabType = tabType
  }

  @action hideReesultScreen() {
    this.isVisible = false
  }

  // userId testId,CourseId

  @action
  async getAllAttempts() {
    try {
      const config = { 'methodType': 'GET' }
      const verifyNetworkModule = new AssignmentNetworkModule(config, this)
      await verifyNetworkModule.getAllAttempts({
        urlParams: {
          testId: this.testId,
          goalId: this.courseId
        }
      })
    } catch (error) {
      //
    }
  }

  getValue(value: any, defaultValue: any = undefined): any {
    if (value) {
      return toJS(value)
    }
    return defaultValue
  }

  getAttemptList(): any[] {
    return this.getValue(this.attemptList)
  }

  onSuccess(apiId: string, response: any) {
    switch (apiId) {
      case API_IDS.GET_ALL_ATTEMPTS:
        this.attemptList = response.data.attempts
        break
      default:
        break
    }
  }

  onSuccessUnAuthorized(apiId: string, response: any) {
    //
  }
  onSuccessBadRequest(apiId: string, response: any) {
    //
  }
  onFailure(apiId: string, request: BaseRequest) {
    //
  }
  onComplete() {
    //
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: BaseResponse): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }

}