import { Platform } from 'react-native';
import { showLoader, hideLoader, showSnackbar } from '../services';
import { GoogleSignin } from 'react-native-google-signin';
import { navigateSimple, resetRouterSimple } from '../services';
// @ts-ignore
import Navigation from 'react-navigation';
import { AuthNetworkModule } from '../api-layer';
import { BaseRequest, BaseResponse } from '../http-layer'
import { NetworkCallbacks } from '../api-layer'
import { SOCIAL_LOGIN_MODE, VERIFICATION_PAGE_TYPE, API_IDS, SHOW_TAB, USER_DETAILS_TYPE } from '../common';
import {
	getDeviceToken, setTabType, setEnrolmentNumber, handleSignIn
} from '../utils'
import commonStores from '../common-library/store';
import stores from '../store';
import FCM from "react-native-fcm";
const FBSDK = require('react-native-fbsdk'); // tslint:disable-line
const { LoginManager, AccessToken } = FBSDK;

const IOS_CLIENT_ID = '349722828062-p42p4air22mcd6mm0ellvcacr6udtu4c.apps.googleusercontent.com';
const WEB_CLIENT_ID = '349722828062-k5ksc249iv7d6ucohuhmkcio0knhp0bs.apps.googleusercontent.com';
const GRAPH_API_URL = 'https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=';

interface UserData {
  name?: string
  email?: string
  socialToken?: string
  uid?: string
  type?: string,
  photoUrl?: string,
  source?: string,
  deviceToken?: string
}

export class SocialDataStore implements NetworkCallbacks {
  isVerified: any;
  navigation?: Navigation;
  userData?: UserData = {};

	async googleSignIn() {
   	console.warn('Google login');
		let self = this;
    showLoader();
    GoogleSignin.signIn()
      .then(async (user) => {
				hideLoader();
        if (user) {
          console.warn('user', user);
          this.userData.socialToken = user.idToken;
          this.userData.uid = user.user.id;
          this.userData.name = user.user.name;
          this.userData.email = user.user.email;
          this.userData.type = SOCIAL_LOGIN_MODE.GOOGLE;
          this.userData.source = Platform.OS === 'ios' ? 'ios' : 'android';
          this.userData.deviceToken = await getDeviceToken();

					const config = { 'methodType': 'POST' };
					const authNetworkModule = new AuthNetworkModule(config, self);
					//   console.warn('this.userData', this.userData)
					const fcmToken = await FCM.getFCMToken();
					// @ts-ignore
					await authNetworkModule.authGoogleLogin(user.idToken, JSON.stringify({fcmToken, platform: Platform.OS}))
        }
      })
      .catch((err) => {
     		console.log('err', err);
        hideLoader();
      });
  }

  setNavigationObject(navigation: any) {
    this.navigation = navigation
  }

  googleSignOut() {
    GoogleSignin.revokeAccess().then(() => GoogleSignin.signOut()).then(() => {
      this.userData = {}
    });
  }

  async setupGoogleSignIn() {
 		//   console.warn('setup google sign in')
    try {
      if (Platform.OS === 'ios') {
        await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
        await GoogleSignin.configure({
          iosClientId: IOS_CLIENT_ID,
          offlineAccess: false
        })
      } else {
        await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
        await GoogleSignin.configure({
          webClientId: WEB_CLIENT_ID,
					forceConsentPrompt: true
        })
      }

    } catch (err) {
      console.log(err);
    }
  }

  fbSignIn() {
		console.warn('Facebook login');
    let self = this;
    showLoader();
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      function (result: any) {
        if (result.isCancelled) {
          hideLoader()
        } else {
          AccessToken.getCurrentAccessToken().then((data: any) => {
            const { accessToken } = data;
            fetch(GRAPH_API_URL + accessToken)
              .then((response) => response.json())
              .then(async (json) => {
                hideLoader();
                // Some user object has been set up somewhere, build that user here
                self.userData.socialToken = accessToken;
                if (json) {
                  self.userData.name = json.name;
                  self.userData.email = json.email;
                  self.userData.uid = json.id;
                  self.userData.type = SOCIAL_LOGIN_MODE.FACEBOOK;
                  self.userData.source = Platform.OS === 'ios' ? 'ios' : 'android';
                  self.userData.deviceToken = await getDeviceToken();
                }

								const config = { 'methodType': 'POST' };
								const authNetworkModule = new AuthNetworkModule(config, self);
								//   console.warn('this.userData', this.userData)
								const fcmToken = await FCM.getFCMToken();
								await authNetworkModule.authFacebookLogin(accessToken, JSON.stringify({fcmToken, platform: Platform.OS}))
              })
              .catch((error) => {
              	console.log(error);
                hideLoader()
              })
          }).done()
        }
      },
      function (error: any) {
      	console.log(error);
        hideLoader()
      }
    )
  }

  async onSuccess(apiId: any, response: any) {
    switch (apiId) {
      case API_IDS.SOCIAL_LOGIN:
        await handleSignIn(response, USER_DETAILS_TYPE.LOGIN);
        if(response.data && response.data.profile) {
          stores.profileDataStore.setUserProfileDetails(response.data.profile);
        }
        await this.navigatePage(response);
        break;
      default:
        break
    }
  }

  async navigatePage(response: any) {
		const { data: { profile } } = response;
		console.log(profile);

    if (profile.mobile === '' || profile.mobile === null) {
			this.goToLoginScreen(profile)
		} else if (profile.name === '') {
			this.gotoAddInfoScreen(profile);
		} else if (profile.email === '') {
			this.gotoEmailScreen(profile);
		} else if (profile.studentProfiles && profile.studentProfiles.length === 0) {
			this.gotoProfileScreen(profile);
		} else {
			this.gotoHomePage();
		}
  }

  goToLoginScreen(profile: any) {
		resetRouterSimple(this.navigation, 0, 'LoginPage', { 'pageType': VERIFICATION_PAGE_TYPE.LOGIN, 'userId': profile.userId })
	}

	gotoAddInfoScreen(profile: any) {
		resetRouterSimple(this.navigation, 0,'AddInfoPage', { 'userId': profile.userId });
	}

	gotoEmailScreen(profile: any) {
		resetRouterSimple(this.navigation, 0,'AddEmailPage', { 'userId': profile.userId });
	}

	gotoProfileScreen(profile: any) {
		resetRouterSimple(this.navigation, 0,'AddProfile1Page', { 'userId': profile.userId });
	}

	gotoHomePage() {
		resetRouterSimple(this.navigation, 0, 'HomePage')
	}

  onSuccessUnAuthorized(apiId: any, response: any) {
    switch (apiId) {
      case API_IDS.SOCIAL_LOGIN:
        if (response.code === 410) {
          showSnackbar(response.message);
          navigateSimple(this.navigation, 'LoginPage', { fromMyPatPage: true, userId: response.data._id })
        }
        break;
      default:
        break
    }
  }

  onSuccessBadRequest(apiId: any, response: any) {
    //
  }

  onFailure(apiId: string, request: BaseRequest) {
    //
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: any): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }

  onComplete() {
    hideLoader()
  }
}
