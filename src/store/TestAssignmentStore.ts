import { action, observable, toJS, ObservableMap, computed } from 'mobx'
import transport from '../common-library/events'
import { Platform } from 'react-native'
import { get, set, toLower, pull, _ } from 'lodash'
import { NetworkCallbacks, TestNetworkModule } from '../api-layer'
import { API_IDS, SUBJECT_TYPE, icons, USER_DETAILS_TYPE, TEST_LIST_ACTION, TYPE, TAB_TYPE, TEST_TYPE } from '../common'
import { BaseRequest, BaseResponse } from '../http-layer'
import { showDialog, hideDialog, navigateSimple, showLoader, showSnackbar } from '../services'
import {
  handleSignIn, getPackages, setTabType, getClassId, getShowTabType, getClassName, getFirstGoalID, getFirstCourseId,
  getExamName, getGoalsData, getDropDownGoalName
} from '../utils'
import moment from 'moment'
import stores from '../store'
import commonStores from '../common-library/store'
import { TestList } from './TestList'
import { ChapterList } from './ChapterList'
import { colors } from '../config'
import { isTablet } from 'react-native-device-info'
import { fetchDownloadedTestsByCategory, fetchDownloadedTestsCountByCategory } from '../persistence/utils'

const defaulPaginationProps = {
  skip: 0,
  limit: isTablet ? 10 : 5
}
const defaulfilterProps = {
  testType: [],
  testStatus: [],
  subject: [],
  attempt: []
}
const defaulsearchProps = {
  searchTerm: '',
  isSearchActive: false
}

const DEFAULT_SETTING = {
  context: {
    testType: TYPE.MAIN,
    subject: 'all',
    goalID: undefined,
    goalName: undefined,
    tabType: ''
  },
  filters: () => new ObservableMap({
    main: undefined,
    chap: undefined,
    conceptTest: undefined,
    isLocked: undefined,
    isAvailable: undefined,
    upcoming: undefined,
    live: undefined,
    past: undefined,
    attempted: undefined,
    unattempted: undefined
  }),
  isLoading: () => true,
  showTrialPage: true,
  isLoadigSearch: false,
  isMoreLoading: false,
  isInfoAccordianOpen: false,
  isSyllabusAccordianOpen: false,
  initialPage: 0,
  tests: () => [/* */]
}
export class TestAssignmentStore implements NetworkCallbacks {
  otherTabList
  @observable context
  @observable showTrialPage: boolean
  @observable courseSubjects
  @observable productData
  @observable selectedClassId
  @observable isLoading
  @observable conceptTestCount
  @observable mainTestCount
  @observable assignedCount
  @observable othersTestCount
  @observable trialTestList: any
  @observable tests: any
  @observable chapters: any
  @observable serachTests: any
  @observable searchAssignments: any
  @observable assignmentList: any
  @observable conceptTestListOffline: any
  @observable chapterTestList: any
  @observable testResult
  @observable packageId: any
  @observable testType = []
  @observable testStatus = []
  @observable filterSubjects = []
  @observable testAttempts = []
  @observable testUnAttempts = []
  @observable isAttempted = false
  @observable isSearchApllied = false
  @observable filters
  @observable selectedProductId
  @observable searchText
  @observable isLoadigSearch
  @observable isLoadingConcepts
  @observable isMoreLoading
  @observable isInfoAccordianOpen
  @observable isSyllabusAccordianOpen
  @observable initialPage
  @observable mainTestCount1
  @observable conceptTestCount1
  @observable assignmentTestCount1
  @observable slectedTabIndex = 0

  isFilterApplied = false
  attemptId
  tabType
  voucherCode
  navigation
  serverTime
  filterString
  tabBarRef
  timerRef
  skip
  limit
  assignHours
  diffHours
  remainHours
  currentTime
  ecmChapId
  ecmKey
  constructor() {
    this.init()
  }
  @action init() {
    const self = this
    getPackages().then(packages => {
      if (packages && packages.length) {
        packages.map((item) => {
          if (item.packageName.indexOf('All India Live') !== -1) {
            self.showTrialPage = DEFAULT_SETTING.showTrialPage
          } else {
            self.showTrialPage = false
          }
        })
      } else {
        self.showTrialPage = DEFAULT_SETTING.showTrialPage
      }
    })
    this.context = DEFAULT_SETTING.context
    this.filters = DEFAULT_SETTING.filters
    this.isLoading = DEFAULT_SETTING.isLoading
    this.isLoadingConcepts = true
    this.skip = defaulPaginationProps.skip
    this.limit = defaulPaginationProps.limit
    this.isInfoAccordianOpen = DEFAULT_SETTING.isInfoAccordianOpen
    this.isSyllabusAccordianOpen = DEFAULT_SETTING.isSyllabusAccordianOpen
    this.initialPage = DEFAULT_SETTING.initialPage
    this.tests = DEFAULT_SETTING.tests
  }
  setNavigationObject(navigation) {
    this.navigation = navigation
  }
  getStringFromArray(rowData) {
    return rowData
  }
  @action
  setInitialSkipAndRange() {
    this.skip = 0
    this.limit = isTablet ? 10 : 5
  }
  @action
  setInitialpage(pageNumber) {
    if (this.initialPage !== 0) {
      this.initialPage = 0
    }
    this.initialPage = pageNumber
  }
  getInitialpage() {
    return this.initialPage
  }
  @action
  async getCourseSubjects(goalId, preFetch = false) {
    let courses = []
    courses[0] = goalId
    const config = { 'methodType': 'POST' }
    const getQueryString = JSON.stringify({ courseIds: courses })
    const testNetworkModule = new TestNetworkModule(config, this)
    await testNetworkModule.getCourseSubjects(getQueryString, preFetch)
  }

  @action
  async fetchMyTrialTests(goalId, goalName, prefetch) {
    const networkStore = commonStores.networkDataStore
    if (networkStore.isNetworkConnected) {
    this.context = {
      goalID: goalId,
      goalName: goalName
    }
    try {
      this.isLoading = true
      const config = { 'methodType': 'GET' }
      const verifyNetworkModule = new TestNetworkModule(config, this)
      await verifyNetworkModule.getTrialTests({
        urlParams: {
          goalId: goalId
        }, prefetch
      })
    } catch (error) {
      this.isLoading = false
    }
  } else {
    this.isLoading = false
    const options = {
      category: 'main'
    }
   const savedData =  await fetchDownloadedTestsByCategory(options)
  //  console.warn('savedData', savedData)
   this.setMainTestList(savedData)
  }
  }
  @action
  async getActiveAssignment(goalId, goalName, actionType, preFetch = false) {
    const networkStore = commonStores.networkDataStore
    if (this.skip === 0) {
      this.isLoading = true
    } else {
      this.isMoreLoading = true
    }
    if (networkStore.isNetworkConnected) {
      const config = { methodType: 'GET' }
      this.context = {
        testType: '',
        goalID: goalId,
        goalName: goalName
      }
      const urlParam = {
        skip: this.skip,
        limit: this.limit,
        goalId: goalId,
        goalName: goalName
      }
      const jsonString = this.getActiveAssignmentPayload(actionType, urlParam)
      console.warn('jsonString ' , jsonString)
      const testNetworkModule = new TestNetworkModule(config, this)
      await testNetworkModule.getActiveAssignments({ urlParams: jsonString }, preFetch)
     // console.warn('urlParams ' , urlParams)
    } else {
      this.isLoading = false
      const options = {
        category: 'assignment'
      }
      const savedData =  await fetchDownloadedTestsByCategory(options)
      this.setOfflineAssignmentList(savedData)
    }
  }
  getActiveAssignmentPayload(actionType: string, payload: any) {
    let finalPayload = payload
    switch (actionType) {
      case TEST_LIST_ACTION.SET_PAGINATION:
        return {
          ...payload,
          skip: defaulPaginationProps.skip,
          limit: defaulPaginationProps.limit
        }
      case TEST_LIST_ACTION.APPLY_FILTER:
        if (this.testType.length !== 3 && this.testType.length) {
          finalPayload = {
            ...payload,
            testType: JSON.stringify(this.testType)
          }
        } else {
          finalPayload = payload
        }
        if (this.testStatus.length !== 3 && this.testStatus.length) {
          finalPayload = {
            ...finalPayload,
            availability: JSON.stringify(this.testStatus)
          }
        } else {
          finalPayload = finalPayload
        }
        if (this.filterSubjects.length > 0) {
          finalPayload = {
            ...finalPayload,
            subject: this.filterSubjects
          }
        } else {
          finalPayload = finalPayload
        }
        if (this.testAttempts.length && this.testAttempts.length !== 2) {
          this.getAttemptedFilter()
          finalPayload = {
            ...finalPayload,
            attempted: this.isAttempted
          }
        } else {
          finalPayload = finalPayload
        }
        return finalPayload
      default:
        return payload
    }
  }
  getTestAssignmentPayload(actionType: string, payload: any) {
    let finalPayload = payload
    switch (actionType) {
      case TEST_LIST_ACTION.SET_PAGINATION:
        return {
          ...payload,
          skip: defaulPaginationProps.skip,
          limit: defaulPaginationProps.limit
        }
      case TEST_LIST_ACTION.APPLY_FILTER:
        if (this.testStatus.length !== 3 && this.testStatus.length) {
          finalPayload = {
            ...payload,
            availability: this.testStatus
          }
        } else {
          finalPayload = finalPayload
        }
        if (this.filterSubjects.length > 0) {
          finalPayload = {
            ...finalPayload,
            subject: this.filterSubjects
          }
        } else {
          finalPayload = finalPayload
        }
        if (this.testAttempts.length && this.testAttempts.length !== 2) {
          this.getAttemptedFilter()
          finalPayload = {
            ...finalPayload,
            attempted: this.isAttempted
          }
        } else {
          finalPayload = finalPayload
        }
        return finalPayload
      default:
        return payload
    }
  }
  @action
  async setFilters(types = new ObservableMap()) {
    let typesVal = toJS(types)
    this.filters = DEFAULT_SETTING.filters()
    if (types && types.size) {
      this.isLoading = true
      Object.keys(typesVal).forEach(key => {
        this.filters.set(key, true)
      })
    }
    this.isFilterApplied = true
  }

  @action
  updateArrowAccordian(val, status) {
    switch (val) {
      case 'info':
        this.isInfoAccordianOpen = status
        break
      case 'syllabus':
        this.isSyllabusAccordianOpen = status
        break
      default:
        break
    }
    //
  }

  resetFilterOnTabChange() {
    this.filters = DEFAULT_SETTING.filters
    // this.testType = []
    this.testStatus = []
    this.isAttempted = false
    this.testAttempts = []
    this.resetSubjectFilter()
    if (this.isFilterApplied) {
      this.getCourseSubjects(this.context.goalID, true)
      if (this.tabType === TAB_TYPE.ASSIGNMENT) {
        this.getActiveAssignment(this.context.goalID, this.context.goalName, '', true)
      } else {
        this.getTestAssignemnt(this.context.goalID, this.context.goalName, this.context.type, '', true)
      }
      this.isFilterApplied = false
    }
  }
  resetFilters() {
    this.filters = DEFAULT_SETTING.filters
    this.testType = []
    this.testStatus = []
    this.isAttempted = false
    this.testAttempts = []
    this.resetSubjectFilter()
    if (this.isFilterApplied) {
      this.isLoading = true
      this.getCourseSubjects(this.context.goalID, true)
      if (this.tabType === TAB_TYPE.ASSIGNMENT) {
        this.getActiveAssignment(this.context.goalID, this.context.goalName, '', true)
      } else {
        this.getTestAssignemnt(this.context.goalID, this.context.goalName, this.context.type, '', true)
      }
      this.isFilterApplied = false
    }
  }
  getFilters() {
    return toJS(this.filters)
  }
  resetSubjectFilter() {
    if (this.courseSubjects) {
      let subjectList = toJS(this.courseSubjects)
      this.courseSubjects = new ObservableMap()
      if (subjectList) {
        Object.keys(subjectList).forEach(subject => {
          this.courseSubjects.set(subject, false)
        })
      }
    }
  }
  @action
  async getAssignmentCount() {
    return await fetchDownloadedTestsCountByCategory({category: 'assignment'})
  }
  @action
  async getTestAssignemnt(goalId, goalName, type, actionType, preFetch = false) {
    const networkStore = commonStores.networkDataStore
    if (networkStore.isNetworkConnected) {
      let requestParam
      this.isLoading = true
      this.context = {
        testType: type,
        goalID: goalId,
        goalName: goalName
      }

      const config = { 'methodType': 'POST' }
      requestParam = {
        skip: 0,
        limit: defaulPaginationProps.limit,
        goalId: goalId,
        goalName: goalName
      }
      if (type) {
        requestParam = {
          ...requestParam,
          testType: [type]
        }
      }
      const jsonString = this.getTestAssignmentPayload(actionType, requestParam)
      const postQueryString = JSON.stringify(jsonString)
     // console.warn('postQueryString ' , postQueryString)
      const testNetworkModule = new TestNetworkModule(config, this)
      await testNetworkModule.getTestAssignment(postQueryString, true)
    } else {
      this.isLoading = false
      const options = {
        category: 'main'
      }
      const savedData =  await fetchDownloadedTestsByCategory(options)
      // console.warn('DATA FROM CACHE: ', JSON.stringify(savedData, null, 2))
      this.setMainTestList(savedData)
      const concptOptions = {
        category: 'concept'
      }
       const conceptSavedData =  await fetchDownloadedTestsByCategory(concptOptions)

       this.setConceptOffLineTestList(conceptSavedData)
    }
  }
  @action
  async getChapterTestAssignment(chapterId, packageId, goalID, goalName, preFetch = false) {
    const config = { 'methodType': 'POST' }
    let requestParam
    this.isLoadingConcepts = true
    let finalParam
    if (packageId === '' ) {
       finalParam = {
        skip: 0,
        limit: this.limit,
        conceptCodes: [chapterId],
        conceptCodeType: 'ecm',
        goalId: goalID,
        goalName: goalName
    }
    } else {
      finalParam = {
        skip: 0,
        limit: this.limit,
        chapterId: chapterId,
        packageId: packageId,
        goalId: goalID,
        goalName: goalName
    }
    }
    if (this.testAttempts.length && this.testAttempts.length !== 2) {
      this.getAttemptedFilter()
      requestParam = {
        ...finalParam,
        attempted: this.isAttempted
      }
    } else {
      requestParam = finalParam
    }
    const getQueryString = JSON.stringify(requestParam)
    const testNetworkModule = new TestNetworkModule(config, this)
    // console.warn('getQueryString ' ,  getQueryString)
    await testNetworkModule.getChapterTestAssignment(getQueryString, preFetch)
  }
  @action
  async getSearchActiveAssignment(searchText, actionType, preFetch = false) {
    const config = { methodType: 'GET' }
    const urlParam = {
      skip: defaulPaginationProps.skip,
      limit: defaulPaginationProps.limit,
      goalId: this.context.goalID,
      goalName: this.context.goalName,
      searchText: searchText
    }
    this.searchText = searchText
    this.isSearchApllied = true
    this.isLoadigSearch = true
    const jsonString = this.getActiveAssignmentPayload(actionType, urlParam)
    const testNetworkModule = new TestNetworkModule(config, this)
    await testNetworkModule.getActiveAssignments({ urlParams: jsonString }, preFetch)
  }
  @action
  async getsSearchTestAssignment(searchText, actionType, preFetch = false) {
    let urlParam
    const config = { methodType: 'POST' }
    urlParam = {
      skip: defaulPaginationProps.skip,
      limit: defaulPaginationProps.limit,
      goalId: this.context.goalID,
      goalName: this.context.goalName,
      searchText: searchText
    }
    this.isSearchApllied = true
    this.searchText = searchText
    const jsonString = this.getTestAssignmentPayload(actionType, urlParam)
    const testNetworkModule = new TestNetworkModule(config, this)
    await testNetworkModule.serachTestAssignment(JSON.stringify(jsonString), preFetch)
  }
  setTestCounts(testData) {
    if (testData) {
      this.othersTestCount = testData.conceptTestCount
      this.mainTestCount = testData.mainTestCount
      this.conceptTestCount = testData.conceptTestCount
    }
  }
  setAssignmentCounts(testData) {
    this.assignedCount = testData.totalCount
  }
  @action
  setSelectedTabIndex(index) {
    this.slectedTabIndex = index
  }
  @action
  onSuccess(apiId: any, response: any): void {
    this.isLoading = false
    switch (apiId) {
      case API_IDS.GET_COURSE_SUBJECTS:
        this.courseSubjects = new ObservableMap()
        response.data[0].data.forEach(element => {
          this.courseSubjects.set(element.name, false)
        })
        break
      case API_IDS.GET_TRIAL_TESTS:
        this.setTrialTestList(response.data)
        this.setServerTime(get(response.data, 'serverTime'))
        this.isLoading = false
        break
      case API_IDS.GET_ACTIVE_ASSIGNMENTS:
        // console.warn('apiId', apiId, response)

        // tslint:disable-next-line:no-shadowed-variable
        const goal = this.context.goalID
        this.isLoadigSearch = false
        this.setAssignmentCounts(response.data)
        // when AILCT was implemented, the response structure was changed, updated the logic accordingly.
        if (response.data && response.data.serverTime) {
          this.setServerTime(response.data.serverTime)
        } else if (
          response.data.responseData &&
          response.data.responseData.length &&
          response.data.responseData[0].serverTime
        ) {
          set(response.data, 'serverTime', response.data.responseData[0].serverTime)
          this.setServerTime(response.data.serverTime)
        } else {
          this.setServerTime(new Date())
        }
         this.setPackagesId(response.data.data)
        this.setAssignmentList(response.data.data)
        // set(this.chapters, this.getTestCachePath(TYPE.ASSIGNMENT, goal), response.data.data)
        // console.warn('on success',  this.isLoading)
        this.isLoading = false
        this.isMoreLoading = false
        break
      case API_IDS.GET_TEST_ASSIGNMENT:
        const goalId = this.context.goalID
        this.isLoadigSearch = false
        // this.setTestCounts(response.data)
        // TODO: Nandani: Investigate this - done
        this.setServerTime(set(response.data.serverTime, 'serverTime'))
        this.setMainTestList(response.data.data.mainTest)
        this.setChapterList(response.data.data.chapters)
        // TODO: Nandani, investigate and tell Gaurav why is this needed - done
        this.setPackagesId(response.data.data)
        // set(this.chapters, this.getTestCachePath(TYPE.CONCEPT, goalId), response.data.data.chapters)
        // set(this.tests, this.getTestCachePath(TYPE.MAIN, goalId), response.data.data.mainTest)
        this.isLoading = false
        // console.warn('==>', JSON.stringify(this.tests, null, 2))
        break
      case API_IDS.SEARCH_TEST_ASSIGNMENT:
        this.isLoadigSearch = false
        this.setSearchTestList(response.data.data)
        break
      case API_IDS.GET_CHAPTER_TEST_ASSIGNMENT:
      this.isLoadingConcepts = false
      this.isLoadigSearch = false
        this.setChapterTestList(response.data.data)
        break
      case API_IDS.GET_TEST_RESULT:
        this.testResult = response.data
        break

      case API_IDS.GET_TEST_RESULT:
        // this.testResult = response.data
        break
      default:
        break
    }
  }
  getAttemptedTestCount(testsArray) {
    let count = 0
    testsArray.forEach(element => {
      if (element.attempted) {
        count += count + 1
      }
    })
    return count
  }
  setSearchTestList(testData) {
    const testArray = testData.mainTests.map(test => new TestList(test))
    const chaptersArray = testData.conceptTests.map(chapter => ({
      ...chapter,
      testCount: chapter.tests.length,
      type: 'chapter',
      tests: chapter.tests.map(test => new TestList(test)),
      attemptedTestCount: this.getAttemptedTestCount(chapter.tests)
    }))
    if (testArray.length > 0 && chaptersArray.length > 0) {
      this.serachTests = [...testArray, ...chaptersArray]
    } else if (testArray.length > 0) {
      this.serachTests = testArray
    } else if (chaptersArray.length > 0) {
      this.serachTests = chaptersArray
    } else {
      this.serachTests = []
    }
  }
  setOfflineAssignmentList(data) {
    this.assignmentList = []
    let tempAssignmentList = data.map(testObject => ({
      ...testObject,
      _id: testObject.id,
      assignmentId: testObject.assignmentId,
      testName: testObject.testName || '',
      duration: testObject.duration || '',
      testType: testObject.type || '',
      assignedDate: testObject.assignedDate || '',
      dueDate: testObject.dueDate || '',
      availability: testObject.status,
      isAssignment: true
    }))
    this.tabType = TAB_TYPE.ASSIGNMENT
    this.assignmentList = tempAssignmentList.map(test => new TestList(test))
  }
  setAssignmentList(data) {
    // console.warn('setAssignmentList ' , data)
        let tempAssignmentList = data.map(testObject => ({
      ...testObject,
      _id: testObject.test.id || '',
      assignmentId: testObject._id || '',
      testName: testObject.test.name || '',
      duration: testObject.test.duration || '',
      testType: testObject.test.type || '',
      assignedDate: testObject.assignedDate || '',
      dueDate: testObject.dueDate || '',
      availability: testObject.isPast ? 'Past' : (testObject.isUpcoming ? 'Upcoming' : 'Live'),
      isAssignment: true
    }))
    if (this.isSearchApllied) {
      this.searchAssignments = tempAssignmentList.map(test => new TestList(test))
    } else {
      if (this.skip !== 0) {
        this.assignmentList = [...this.assignmentList, ...(tempAssignmentList.map(test => new TestList(test)))]
      } else {
        this.assignmentList = tempAssignmentList.map(test => new TestList(test))
        // console.warn('this.assignmentList ' , this.assignmentList.length)
      }
    }
    // TODO : NANDANI preventing to open assignment tab forcefully
    if (!this.isFilterApplied && !this.isSearchApllied) {
      if (this.assignmentList) {
        this.tabType = TAB_TYPE.ASSIGNMENT
        // this.goToPage(0)
      } else {
        // this.goToPage(1)

      }
    }
  }
  setTrialTestList(data) {
    let tempTestList
    if (data.ailct) {
      tempTestList = data.ailct.myTest.map(testObject => ({
        ...testObject,
        testType: 'ailct',
        ailct: true
      }))
      this.trialTestList = tempTestList.map(test => new TestList(test))
      this.trialTestList = [...this.trialTestList, ...(data.responseData.map(test => new TestList(test)))]
    } else {
      this.trialTestList = data.responseData.map(test => new TestList(test))
    }
  }
  setConceptOffLineTestList(data) {
      this.conceptTestListOffline = data.map(test => new TestList(test))
  }
  setMainTestList(data) {
    this.tests = []
    if (this.skip !== 0 && this.isFilterApplied) {
      this.tests = [...this.tests, ...(data.map(test => new TestList(test)))]
    } else {
      this.tests = data.map(test => new TestList(test))
    }
  }
  setChapterList(data) {
    this.chapters = []
    this.chapters = data.map(chapters => new ChapterList(chapters))
  }
  getChapterListTest() {
    return  this.chapterTestList
  }
  setChapterListToBlank() {
    this.chapterTestList = []
  }
  setChapterTestList(data) {
    this.chapterTestList = data.tests.map(test => new TestList(test))
  }
  // Finds the path where the tests are placed inside the cache
  // TODO: Nandani, check if not required, get rid of this code
  // getTestCachePath(type, goalID) {
  //   switch (type) {
  //     case TYPE.CONCEPT:
  //     case TYPE.CHAP:
  //     case TYPE.MAIN:
  //       return [type, goalID]
  //     default:
  //       return ['tests', goalID]
  //   }
  // }
  async onContinueClicked(response) {
    handleSignIn(response, USER_DETAILS_TYPE.DEFAULT)
    stores.goalsDataStore.setFinalGoals(response.data.goals)
    let packages = await getPackages()
    this.setTrialPageVisibility(false)
  }

  updateSubjectFilter(subjectVal) {
    if (this.tabType === TAB_TYPE.ASSIGNMENT || this.tabType === TAB_TYPE.OTHERS) {
      let subjectList = toJS(this.courseSubjects)
      this.courseSubjects = new ObservableMap()
      Object.keys(subjectList).forEach(subject => {
        if (subjectVal === subject) {
          if (subjectList[subject] === true) {
            this.courseSubjects.set(subject, false)
          } else {
            this.courseSubjects.set(subject, true)
          }
        } else {
          this.courseSubjects.set(subject, subjectList[subject])
        }
      })
    }
  }
  @action
  getTestType(mType) {
    if (mType === 'mainTest') {
      return TEST_TYPE.MAIN_TEST
    } else if (mType === 'conceptTest' || mType === 'concept') {
      return TEST_TYPE.CONCEPT_TEST
    } else if (mType === 'chap' || mType === 'chapTest') {
      return TEST_TYPE.CHAP_TEST
    }
    return null
  }
  getPackageList() {
    return toJS(this.productData)
  }
  @action
  getTrialPageVisibility() {
    return this.showTrialPage
  }
  @action getShowTabType() {
    return this.tabType
  }
  @action setTabType(tabType) {
    this.tabType = tabType
  }
  @action
  setTrialPageVisibility(bool) {
    this.showTrialPage = bool
  }
  @action
  setLoader(bool) {
    this.isLoading = bool
  }
  @action
  setSubject(subject) {
    this.context.subject = subject
  }

  @action
  showChapterTestList(testData, self) {
    navigateSimple(this.navigation, 'ConceptTestListPage', { testData: testData, currentContext: self })
  }
  @action
  showTestDetailPage(testData, testStartedTime) {
    if (testData.type === 'ailct' || testData.ailct) {
      if (testData.attempts && testData.attempts.length >= 1) {
        const attemptedTime = new Date(testData.endDate).getTime() + (60000 * testData.resultAfter)
        if (testData.resultAfter === 0 && (testData.status).toUpperCase() === 'PAST') {
          navigateSimple(this.navigation, 'TestDetailPage', { testData: testData })
        } else if (attemptedTime > this.serverTime && (testData.status).toUpperCase() === 'PAST') {
          showSnackbar('We are calculating your result. Please check back after 11 PM to view your result.')
        } else {
          if ((testData.status).toUpperCase() === 'LIVE') {
            return
          }
          navigateSimple(this.navigation, 'TestDetailPage', { testData: testData, testStartedTime: testStartedTime })
        }
      } else {
        return
      }
    } else {
      navigateSimple(this.navigation, 'TestDetailPage', { testData: testData, testStartedTime: testStartedTime })
    }
  }
  @action
  showChapTestPage(testData) {
    navigateSimple(this.navigation, 'ChapTestInfoPage', { testData: testData })
  }
  @action
  showTestInfoPage(dataSource) {
    navigateSimple(this.navigation, 'AssignmentAttemptPopUp', { dataSource: dataSource })
  }
  @action
  async initClassAndExamId() {
    this.selectedClassId = await getClassId() || ''
    const goals: any = await getFirstGoalID() || ''
    let goalIds
    if (goals && typeof goals !== 'string') {
      goalIds = goals.map(goal => goal.goalId)
    } else {
      goalIds = goals
    }
    this.selectedProductId = goalIds
  }
  @action
  setProductId(targetExam) {
    this.selectedProductId = targetExam
  }
  async checkPackageBought() {
    const packages = await getPackages()
    let packageIDBought = false
    if (packages && packages.length) {
      let packageGoalsArray = packages.map(item => item.goalsArr)
      for (let userGoalsArr of packageGoalsArray) {
        for (let userGoal of userGoalsArr) {
          if (userGoal._id === this.context.goalID) {
            packageIDBought = true
            this.showTrialPage = false
            return true
          }
        }
      }
      this.showTrialPage = true
      return false
    }
    this.showTrialPage = true
    return packageIDBought
  }
  showColorAvailability(value) {
    if (value) {
      if (value === 'Live') {
        return '#8ece30'
      } else if (value === 'Scheduled') {
        return '#ff830f'
      } else {
        return '#999999'
      }
    }
    return colors.Green
  }
  goToPage(pageIndex) {
    if (this.tabBarRef) {
      this.tabBarRef.setPage(pageIndex)
    }
  }
  setTabBarViewRef(ref) {
    if (ref !== null) {
      this.tabBarRef = ref
    }
  }
  setAttemptId(attemptId) {
    this.attemptId = attemptId
  }
  getAttemptId() {
    return this.attemptId
  }
  setServerTime(time) {
    this.serverTime = time
  }
  getEcmChapId() {
    return this.ecmChapId
  }
  getEcmKey() {
    return this.ecmKey
  }
  getEcmPackages() {
    return this.packageId
  }
  setEcmChapId(id) {
    this.ecmChapId = id
  }
  setEcmkey(val) {
    this.ecmKey = val
  }
  setEcmPackages(data) {
    this.packageId = data
  }
  setPackagesId(data) {
    if (this.packageId === undefined) {
      this.packageId = data.packages
    }
  }
  getPackageId() {
    return this.packageId
  }
  getServerTime() {
    return this.serverTime
  }
  @action setFilteredSubject(subjects) {
    this.filterSubjects = subjects
  }
  @action checkIfAnyFilterItemSelected() {

    if (this.tabType === TAB_TYPE.ASSIGNMENT) {
      if (this.testType.length > 0 || this.testStatus.length > 0 || this.filterSubjects.length > 0 || this.testAttempts.length > 0) {
        return true
      }
    } else {
      if (this.testStatus.length > 0 || this.filterSubjects.length > 0 || this.testAttempts.length > 0) {
        return true
      }
    }
    return false
  }
  onSuccessUnAuthorized(apiId: any, response: any) {
    if (apiId === API_IDS.GET_ACTIVE_ASSIGNMENTS || apiId === API_IDS.GET_TEST_ASSIGNMENT || apiId === API_IDS.SEARCH_TEST_ASSIGNMENT) {
      this.chapters = []
      this.tests = []
    }
    if (apiId === API_IDS.GET_CHAPTER_TEST_ASSIGNMENT) {
      this.chapterTestList = []
    }
    this.isLoading = false
    this.isMoreLoading = false

    //
  }

  onSuccessBadRequest(apiId: any, response: any) {
    // console.warn('onSuccessBadRequest' , response)
    this.isMoreLoading = false
    this.isLoadigSearch = false
    this.isLoading = false
    switch (apiId) {
      case API_IDS.GET_ACTIVE_ASSIGNMENTS:
      case API_IDS.GET_TEST_ASSIGNMENT:
      case API_IDS.SEARCH_TEST_ASSIGNMENT:
        this.chapters = []
        this.tests = []
        break
      default:
        break
    }
    //
  }
  @action
  setTestTypeFilter(type, isSelect) {
    if (isSelect) {
      this.testType.push(type)
    } else {
      let index = this.testType.indexOf(type)
      this.testType.splice(index, 1)
    }
  }
  @action
  setTestList(testArray) {
    this.tests = testArray
  }
  @action
  setChapterArray(chapters) {
    this.tests = chapters
  }
  getTestTypeFilter() {
    if (this.testType.length && this.testType.length !== 3) {
      return { testType: this.testType }
    } else {
      return undefined
    }
  }
  @action
  setTestStatusFilter(type, isSelect) {
    if (isSelect) {
      this.testStatus.push(type)
    } else {
      let index = this.testStatus.indexOf(type)
      this.testStatus.splice(index, 1)
    }
  }
  @action
  setTestUnAttemptFilter(type, isSelected) {
    if (isSelected) {
      this.testUnAttempts.push(type)
    } else {
      let index = this.testUnAttempts.indexOf(type)
      this.testUnAttempts.splice(index, 1)
    }
  }
  @action
  setTestAttemptFilter(type, isSelected) {
    if (isSelected) {
      this.testAttempts.push(type)
    } else {
      let index = this.testAttempts.indexOf(type)
      this.testAttempts.splice(index, 1)
    }
  }
  getUnAttemptedFilter() {
    this.testUnAttempts.map(item => {
      if (item === 'unattempted') {
        this.isAttempted = false
      }
    })
  }
  getAttemptedFilter() {
    this.testAttempts.map(item => {
      if (item === 'attempted') {
        this.isAttempted = true
      } else {
        this.isAttempted = false
      }
    })
  }
  dateFormat(date, status) {
    if (date !== undefined) {
      const serevrTime = stores.testAssignmentStore.getServerTime()
      const formattedDate = moment(date).format('YYYY-MM-DD HH:mm:ssZZ')
      const mdate = moment(formattedDate).from(serevrTime)
      let numberPattern = /\d+/g
      if ((mdate.indexOf('year') !== -1) || (mdate.indexOf('month') !== -1)) {
        let dateVal = 'On ' + moment(date).format('DD MMM')
        return dateVal
      } else if (mdate.indexOf('hours') !== -1) {
        let val
        if (status === 'Past') {
          val = mdate.match(numberPattern) + ' Hours Ago'
        } else if (status === 'Scheduled') {
          val = mdate.match(numberPattern) + ' Hours Left'
        } else {
          val = ''
        }
        return val
      } else if (mdate.indexOf('days') !== -1) {
        let val = mdate.match(numberPattern)
        if (Number(val) > 7 || Number(val) === 7) {
          let dateVal
          if (status === 'Past') {
            dateVal = 'At ' + moment(date).format('DD MMM')
          } else {
            dateVal = 'On ' + moment(date).format('DD MMM')
          }
          return dateVal
        } else {
          let dateVal
          if (status === 'Past') {
            dateVal = val + ' Days Ago'
          } else if (status === 'Scheduled') {
            dateVal = val + ' Days Left'
          } else if (status === 'Live') {
            dateVal = 'On ' + moment(date).format('YYYYMMDD')
          } else {
            dateVal = ''
          }
          return dateVal
        }
      }
      return mdate
    }
    return ''
  }

  setCurrentTime(time) {
    this.currentTime = time
  }
  getCurrentTime() {
    return this.currentTime
  }
  setTimerRef(ref) {
    if (ref !== null) {
      this.timerRef = ref

    }
  }
  @action
   async getTestEngineType(item) {
    const goalName: any = await getDropDownGoalName()
    const isMain = goalName.indexOf('JEE (M)') >= 0 || goalName.indexOf('JEE(M)') >= 0 ? true : false
    const isAdvanced = goalName.indexOf('JEE(A)') >= 0 || goalName.indexOf('JEE (A)') >= 0 ? true : false
    const isConcept = item.type === 'concept' || item.type === 'conceptTest' ? true : false
    let testEngineType
      if (isMain && !isConcept) {
        testEngineType = 'jee-main'
      } else if (isAdvanced && !isConcept) {
        testEngineType = 'jee-advanced'
      } else {
        testEngineType = 'others'
      }
      return testEngineType
  }
  removeTestItemFormList(item) {
    if (this.tabType === TAB_TYPE.MAIN_TESTS) {
      const index = this.tests.findIndex(testItem => testItem.id === item.id)
      if (index >= 0) {
        this.tests.splice(index, 1)
        const newTestList = this.tests
        this.tests = []
        this.tests = newTestList
      }
    } else if (this.tabType === TAB_TYPE.ASSIGNMENT) {
      const index = this.assignmentList.findIndex(testItem => testItem.id === item.id)
      if (index >= 0) {
        this.assignmentList.splice(index, 1)
        const newTestList = this.assignmentList
        this.assignmentList = []
        this.assignmentList = newTestList
      }
    } else if (this.tabType === TAB_TYPE.OTHERS) {
      const index = this.conceptTestListOffline.findIndex(testItem => testItem.id === item.id)
      if (index >= 0) {
        this.conceptTestListOffline.splice(index, 1)
        const newTestList = this.conceptTestListOffline
        this.conceptTestListOffline = []
        this.conceptTestListOffline = newTestList
      }
    }
  }

  @action
  setMainCount(num) {
    this.mainTestCount1 = num
  }
  @action
  setConceptCount(num) {
    this.conceptTestCount1 = num
  }
  @action
  setAssignmentCount(num) {
    this.assignmentTestCount1 = num
  }
  getMainCount() {
    return this.mainTestCount1
  }
  getConceptCount() {
    return this.conceptTestCount1
  }
  getAssignHours(item) {
    this.assignHours = new Date(item.assignedDate).getTime() - this.getServerTime()
    return this.assignHours
  }
  getDifferenceHours(item) {
    this.diffHours = new Date(item.dueDate).getTime() - this.getServerTime()
    return this.diffHours
  }
  getRemainsHours(item, startTime, serverTime = this.getServerTime()) {
    if (item.testName.includes('CHEM-bohrs theory')) {
      // console.warn('item.duration' , item.duration)
      // console.warn('serverTime' , serverTime)
      // console.warn('startTime' , startTime)
      // console.warn('new Date(startTime).getTime()' , new Date(startTime).getTime())
      }
    this.remainHours = 60000 * item.duration - (serverTime - new Date(startTime).getTime())
    return this.remainHours
  }
  onFailure(apiId: string, request: BaseRequest): void {
    this.isLoading = false
    this.isMoreLoading = false
  }
  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: BaseResponse): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }
  onComplete() {
    //
  }
}
