import { AttemptList } from './AttemptList'
import { observable } from 'mobx'

interface TestListData {
  _id?: any
  id?: any
  testName?: string
  name?: string // in case when testName key is not comming
  chapterName?: string // in case of concept Test
  testType?: string
  availability?: string
  testDate?: Date
  duration?: number
  isAssignment?: boolean
  isLoacked?: boolean
  isAvailable?: boolean
  assignement?: any // in case of assignment true
  syllabus?: any
  chapters?: any
  concepts?: any
  scheduledId: any
  teacher?: any
  attempted?: boolean
  isBought?: boolean
  price?: any,
  courseId?: any,
  packageId?: any,
  ailct?: boolean,
  endDate?: any,
  resultAfter?: any,
  status?: any,
  resume: {
    isResume?: boolean,
    startTime?: Date,
    attemptId?: string,
    timeLeft?: number
  }
  attempts: AttemptList[]
  assignedDate: string,
  dueDate: string,
  orgId: string
  assignmentId: string
  courseName: string,
  type: string

}

export class TestList {
  @observable id: string
  @observable testName: string
  @observable chapterName: string // in case of concept Test
  @observable type: string
  @observable status: string
  @observable testDate: Date
  @observable duration: number
  @observable isAssignment: boolean
  @observable assignement: any // in case of assignment true
  @observable syllabus: any
  @observable teacher: any
  @observable chapters: any
  @observable concepts: any
  @observable isBought: boolean
  @observable price: any
  @observable attempted: boolean
  @observable courseId: any
  @observable packageId: any
  @observable resume: {
    isResume?: boolean,
    startTime?: Date,
    attemptId?: string,
    timeLeft?: number
  }
  @observable assignedDate: string
  @observable dueDate: string
  @observable orgId: string
  @observable assignmentId: string
  @observable ailct: boolean
  @observable endDate: boolean
  @observable resultAfter: any
  @observable courseName: any
  attempts: AttemptList[]
  constructor(testData: TestListData) {
    this.id = testData._id ||  testData.id || ''
    this.testName = testData.testName || testData.name || ''
    this.chapterName = testData.chapterName || null
    this.type = testData.testType || testData.type || ''
    this.status = testData.availability || testData.status || ''
    this.testDate = testData.testDate
    this.duration = testData.duration || 0
    this.isAssignment = testData.isAssignment || false
    this.assignement = testData.assignement || null
    this.syllabus = testData.syllabus || {}
    this.resume = testData.resume || { isResume: false, startTime: null, attemptId: null, timeLeft: 0 }
    this.attempts = testData.attempts || []
    this.teacher = testData.teacher
    this.concepts = testData.concepts
    this.chapters = testData.chapters
    this.attempted = testData.attempted || false
    this.assignedDate = testData.assignedDate || ''
    this.dueDate = testData.dueDate || ''
    this.orgId = testData.orgId || ''
    this.assignmentId = testData.assignmentId || ''
    this.isBought = testData.isBought || false
    this.price = testData.price || '99',
    this.courseId = testData.courseId || ''
    this.packageId = testData.packageId || ''
    this.ailct = testData.ailct || false
    this.endDate = testData.endDate || ''
    this.resultAfter = testData.resultAfter,
    this.courseName = testData.courseName || ''
  }
}