import { action, computed, ObservableMap, observable, toJS } from 'mobx'
import { concat, get, set, toLower, values } from 'lodash'
import { Navigation } from 'react-navigation'
import { API_IDS, TYPE, FROM_PAGE_TYPE, SUBJECT_TYPE, DIALOG_DESCRIPTION, icons, USER_DETAILS_TYPE } from '../common'
import { NetworkCallbacks, TestNetworkModule } from '../api-layer'
import { BaseRequest, BaseResponse } from '../http-layer'
import { resetRouterSimple, showSnackbar, showDialog, hideDialog } from '../services'
import {
	getFirstCourseId, setCompleteGoal,
	getTargetExam, getClassId, getFirstGoalID, getClassName, getExamName, getGoalsData,
	getPackages, handleSignIn, getSelectedStudent
} from '../utils'
import stores from '../store'
interface TestResult {
  testId?: any,
  testName?: string,
  attempts?: any
}

const DEFAULT_SETTING = {
  navigation: Navigation,
  trialTestList: [],
  tests: {
    [TYPE.CONCEPT]: undefined,
    [TYPE.MAIN]: undefined
  },
  context: {
    testType: TYPE.MAIN,
    subject: 'all',
    goal: undefined
  },
  testResult: undefined,
  isLoading: true,
  filters: () => new ObservableMap({
    availability: undefined,
    attempted: undefined,
    unattempted: undefined,
    search: undefined
  }),
  fullTestCount: undefined,
  partTestCount: undefined,
  combinedTestCount: undefined,
  conceptTestCount: undefined,
  showFilterSelection: false,
  showTrialPage: true,
  testDataUpdate: false
}

const livePredicate = test => test.availability === 'Live'
const isAttemptedPredicate = test => !!test.attempted
const isUnattemptedPredicate = test => !test.attempted
const searchPredicate = (test, searchTerm) => {
  if (searchTerm) {
    return toLower(test.testName).search(toLower(searchTerm)) > -1
  }
  return true
}

const predicates = {
  availability: livePredicate,
  attempted: isAttemptedPredicate,
  unattempted: isUnattemptedPredicate,
  search: searchPredicate
}

export class TestsStore implements NetworkCallbacks {

  navigation: Navigation
  @observable idHydrated = false
  // Trial test list that is to be rendered in component
  @observable trialTestList
  @observable filterDataList

  // poor man's cache for keeping purchased tests
  @observable tests = {}
  @observable context
  fullTestCount
  originalMainCount
  orignalConceptCount
  partTestCount
  combinedTestCount
  voucherCode
  @observable conceptTestCount
  @observable mainTestCount
  @observable courseSubjects

  @observable testResult
  @observable isLoading
  @observable selectedClassId
  @observable selectedClass
  @observable selectedProductId
  @observable selectedTargetExamCourseId

  @observable selectedTargetExam
  @observable targetExams
  @observable popupListRenderingData
  @observable filters
  @observable resultValues: TestResult = {
    testId: '',
    testName: '',
    attempts: []
  }
  @observable showResult: boolean
  @observable showFilterSelection: boolean
  @observable showTrialPage: boolean

  @observable productData: any
  @observable testDataUpdate: false
  attemptId
  testType
  data
  serverTime

  constructor() {
    this.init()
  }

  @action
  setResultUpdate(bool) {
    this.testDataUpdate = bool
  }
  getResultUpdate() {
    return this.testDataUpdate
  }
  @action
  setTrialPageVisibility(bool) {
    this.showTrialPage = bool
  }

  @action
  toggleFilterSelection(): void {
    this.showFilterSelection = !this.showFilterSelection
  }

  getTrialPageVisibility() {
    return this.showTrialPage
  }

  setNavigationObject(navigation) {
    this.navigation = navigation
  }
  @action
  setTestResultValues(testId: any, testName: string, attempts: any): any {
    this.resultValues.attempts = attempts
    this.resultValues.testId = testId
    this.resultValues.testName = testName
  }

  @action
  showResultScreen(show: boolean) {
    this.showResult = show
  }

  getTestResultValues(): any {
    return this.resultValues
  }

  showFilterSelectionBox(): boolean {
    return this.showFilterSelection
  }

  @action
  setFilters(types = []) {
    this.filters = DEFAULT_SETTING.filters()
    if (types && types.length) {
      this.isLoading = true
      types.forEach(type => this.filters.set(type, true))
      const testlist = this.getTestList(this.context.testType)
      this.setUpdatedTestCounts(testlist)
      this.isLoading = false
    }
  }

  setSearchFilter(searchTerm) {
    this.filters.set('search', searchTerm || false)
  }

  resetSearchFilter() {
    this.filters.set('search', false)
  }

  removeFilter(type) {
    this.filters.set(type, false)
  }

  @action
  resetFilters() {
    this.mainTestCount = this.originalMainCount
    this.conceptTestCount = this.orignalConceptCount
    this.filters = DEFAULT_SETTING.filters()
  }

  @action
  async initClassAndExamId() {
    this.selectedClassId = await getClassId() || ''
    this.selectedClass = await getClassName() || ''
    const goals: any = await getFirstGoalID()
    this.selectedTargetExamCourseId = await getFirstCourseId()
    this.selectedTargetExam = await getExamName()
    this.targetExams = await getGoalsData()
    let goalIds
    if (goals && typeof goals !== 'string') {
      goalIds = goals.map(goal => goal.goalId)
    } else {
      goalIds = goals
    }
    this.selectedProductId = goalIds
  }

  @action
  setTargetExamCourseId(targetExam) {
    this.selectedTargetExamCourseId = targetExam
  }

  @action
  setProductId(targetExam) {
    this.selectedProductId = targetExam
  }

  @action
  setExamPopupData() {
    this.popupListRenderingData = this.targetExams
  }

  @action init() {
    const self = this
    getPackages().then(packages => {
      if (packages && packages.length) {
        self.showTrialPage = false
      } else {
        self.showTrialPage = DEFAULT_SETTING.showTrialPage
      }
    })
    this.courseSubjects = [{ 'name': SUBJECT_TYPE.ALL, 'id': 'dedd' }]
    this.navigation = DEFAULT_SETTING.navigation
    this.trialTestList = DEFAULT_SETTING.trialTestList
    this.context = DEFAULT_SETTING.context
    this.testResult = DEFAULT_SETTING.testResult
    this.isLoading = DEFAULT_SETTING.isLoading
    this.filters = DEFAULT_SETTING.filters()
    this.combinedTestCount = DEFAULT_SETTING.combinedTestCount
    this.conceptTestCount = DEFAULT_SETTING.conceptTestCount
    this.fullTestCount = DEFAULT_SETTING.fullTestCount
    this.partTestCount = DEFAULT_SETTING.partTestCount
    this.originalMainCount = undefined
    this.orignalConceptCount = undefined
    this.tests = {}
  }

  getFilters() {
    return toJS(this.filters)
  }

  getTestType() {
    return this.context.testType
  }
  setAttemptId(attemptId) {
    this.attemptId = attemptId
  }
  getAttemptId() {
    return this.attemptId
  }

  isFilterApplied() {
    const filters = this.getFilters()
    return Object.keys(filters).filter(key => !!filters[key]).length
  }

  applyFilters = (filterPredicates, test) => filterPredicates.reduce((bool, predicate) => {
    return predicate(test) && bool
  }, true)

  getTestList(testType) {
    const subject = this.context.subject
    const goal = this.context.goal
    // const  = this.context.testType
    const filters = this.getFilters()
    const testList = get(this.tests, this.getTestCachePath(testType, goal, subject))
    if (testList && this.isFilterApplied()) {
      // filter implementation
      const filterPredicates = Object.keys(filters)
        .filter(key => filters[key])
        .map(key => (test) => predicates[key](test, filters[key]))
      const testListVal = testList.myTest.filter(test => this.applyFilters(filterPredicates, test))
      return {
        ...testList,
        myTest: testListVal
      }
    }
    if (testList && testList.myTest) {
      return toJS({
        ...testList,
        myTest: testList.myTest
      })
    }
    return testList
  }

  getValue(value: any, defaultValue: any = undefined): any {
    if (value) {
      return toJS(value)
    }
    return defaultValue
  }

  @action
  setTestType(testType) {
    if (testType) {
      this.context.testType = testType
      // this.resetFilters()
    }
  }

  @action
  setGoal(goal) {
    this.context.goal = goal
  }

  @action
  setSubject(subject) {
    this.context.subject = subject
  }

  @action
  setLoader(bool) {
    this.isLoading = bool
  }

  setServerTime(time) {
    this.serverTime = time
  }

  setTestCounts(test) {
    if (test) {
      this.fullTestCount = test.fullTestCount
      this.partTestCount = test.partTestCount
      this.combinedTestCount = test.combinedTestCount
      switch (this.context.testType) {
        case TYPE.MAIN:
          this.conceptTestCount = test.conceptTestCount
          break
        case TYPE.CONCEPT:
          this.conceptTestCount = test.myTest.length
          break
        default:
          break
      }
      this.mainTestCount = this.fullTestCount + this.partTestCount + this.combinedTestCount
      this.originalMainCount = this.mainTestCount
      this.orignalConceptCount = this.conceptTestCount
    }
  }

  setUpdatedTestCounts(test) {
    {
      if (test && test.myTest) {
        switch (this.context.testType) {
          case TYPE.CONCEPT:
            this.conceptTestCount = test.myTest.length
            break
          case TYPE.MAIN:
            this.mainTestCount = test.myTest.length
            break
          default:
            break
        }
      }
    }
  }

  getPackageList() {
    return toJS(this.productData)
  }

  @action
  onSuccess(apiId: any, response: any): void {
    const self = this
    switch (apiId) {
      case API_IDS.GET_COURSE_SUBJECTS:
      this.courseSubjects = [{ 'name': SUBJECT_TYPE.ALL, 'id': 'dedd' }]
      response.data[0].data.map((test, index) =>
          this.courseSubjects[++index] = test
        )
        break
      case API_IDS.GET_TRIAL_TESTS:
        this.trialTestList = response.data
        this.setServerTime(get(response.data, 'serverTime'))
        break
      case API_IDS.GET_MY_TESTS:
        const subject = this.context.subject
        const goal = this.context.goal
        const testType = this.context.testType
        this.setTestCounts(response.data)
        this.setServerTime(get(response.data, 'serverTime'))
        switch (testType) {
          case TYPE.CONCEPT:
            if (subject === SUBJECT_TYPE.ALL) {
              set(this.tests, this.getTestCachePath(testType, goal, SUBJECT_TYPE.ALL), response.data)
            } else {
              set(this.tests, this.getTestCachePath(testType, goal, subject), response.data)
            }
            break
          default:
            const combinedTests = response.data.combined || []
            const fullTests = response.data.full || []
            const partTests = response.data.part || []
            const allTests = [].concat(fullTests, partTests, combinedTests)
            set(this.tests, this.getTestCachePath(testType, goal, subject), { myTest: allTests })
            break
        }
        break
      case API_IDS.GET_TEST_RESULT:
        this.testResult = response.data
        break

      case API_IDS.GET_TEST_RESULT:
        this.testResult = response.data
        break
      default:
        break
    }
    self.isLoading = false
  }

  async onContinueClicked(response) {
    handleSignIn(response, USER_DETAILS_TYPE.DEFAULT)
    stores.goalsDataStore.setFinalGoals(response.data.goals)
    let packages = await getPackages()
    this.fetchMyTests(this.context.testType, packages[0].goalId, undefined, true)
    this.setTrialPageVisibility(false)
  }

  onSuccessUnAuthorized(apiId: any, response: any) {
    //
  }

  onSuccessBadRequest(apiId: any, response: any) {

  }

  onFailure(apiId: string, request: BaseRequest): void {
    this.isLoading = false
  }

  validateRequestParams(): Boolean {
    return true
  }

  validateResponseParams(res: BaseResponse): Boolean {
    return true
  }

  generalValidationError(type: string, error: String): void {
    //
  }

  // Finds the path where the tests are placed inside the cache
  getTestCachePath(type, goalID, subject) {
    switch (type) {
      case TYPE.CONCEPT:
      case TYPE.MAIN:
        return [type, goalID, type === TYPE.CONCEPT ? subject : undefined]
      default:
        return ['tests', goalID]
    }
  }

  @action
  async fetchMyTrialTests(prefetch) {
    try {
      this.isLoading = true
      const config = { 'methodType': 'GET' }
      const verifyNetworkModule = new TestNetworkModule(config, this)
      await verifyNetworkModule.getTrialTests({
        urlParams: {
          goalId: this.context.goal
        }, prefetch
      })
    } catch (error) {
      this.isLoading = false
    }
  }

  @action
  async getCourseSubjects(goalID, prefetch) {
    let courses = []
    courses[0] = goalID || this.context.goal
    const config = { 'methodType': 'POST' }
    const getQueryString = JSON.stringify({ courseIds: courses })
    const testNetworkModule = new TestNetworkModule(config, this)
    await testNetworkModule.getCourseSubjects(getQueryString, prefetch)
  }

  @action
  fetchMyTests(type, goalID, subject, preFetch, forceUpdate = false) {
    this.setGoal(goalID)
    this.setSubject(subject)
    this.setTestType(type)
    this.resetSearchFilter()
    let tests = null
    if (!forceUpdate) {
      tests = get(this.tests, this.getTestCachePath(type, goalID, subject))
    }
    this.isLoading = true
    if (!tests) {
      // to be able to use this in onSuccess method
      this.context = {
        testType: type,
        subject,
        goal: goalID
      }
      try {
        const config = { methodType: 'GET' }
        const testNetworkModule = new TestNetworkModule(config, this)
        let urlParam
        if (subject) {
          urlParam = {
            urlParams: {
              testType: type,
              goalId: goalID,
              subject: subject
            }
          }
        } else {
          urlParam = {
            urlParams: {
              testType: 'main',
              goalId: goalID
            }
          }
        }
        testNetworkModule.getMyTests(urlParam, preFetch)
      } catch (error) {
        this.isLoading = false
      }
    } else {
      const testlist = this.getTestList(this.context.testType)
        this.setUpdatedTestCounts(tests)
        this.isLoading = false
    }
  }

  @action
  async fetchtestResult(testId: any, attemptId: any) {
    try {
			const student = await getSelectedStudent();
			let studentId;
			if (student) {
				studentId = student.id;
			}
      const config = { 'methodType': 'GET' };
      const verifyNetworkModule = new TestNetworkModule(config, this);
      await verifyNetworkModule.getTestResult({
        urlParams: {
          testId: testId,
          attemptId: attemptId
        }
      }, studentId)
    } catch (error) {
      //
    }
  }

  onComplete() {
    //
  }
}
