import { ProfileActivityStore } from './ProfileActivityStore'
import { EditProfileDataStore } from './EditProfileDataStore'
import { DialogStore } from './DialogStore'
import { HomeDataStore } from './HomeDataStore'
import { ProfileDataStore } from './ProfileDataStore'
import { TestsStore } from './TestsStore'
import { PerformanceStore } from './PerformanceStore'
import { SocialDataStore } from './SocialDataStore'
import { GoalsDataStore } from './GoalsDataStore'
import { AssignmentStore } from './AssignmentStore'
import { ResultStore } from './ResultStore'
import { AppContextStore } from './AppContextStore'
import { AppUtilsStore } from './AppUtilsStore'
import { NotificationStore } from './NotificationStore'
import { FeedStore } from './FeedStore'
import { TestDownloadMetaStore } from './test-listing'
import { OfflineStorageProgressStore } from './OfflineStorageProgressStore'

import { NewDashboardStore } from './NewDashboardStore';
import { CustomModalStore } from './CustomModalStore';
import { NewGoalsDataStore } from './NewGoalsDataStore';
import { ActivityCardDataStore } from './ActivityCardDataStore';
import { TestAssignmentStore} from './TestAssignmentStore'
import { TestAttemptStore } from './test-attempt'

export * from './ProfileActivityStore'
export * from './EditProfileDataStore'
export * from './DialogStore'
export * from './ResultStore'
export * from './HomeDataStore'
export * from './ProfileDataStore'
export * from './TestsStore'
export * from './AssignmentStore'
export * from './PerformanceStore'
export * from './SocialDataStore'
export * from './GoalsDataStore'
export * from './AppContextStore'
export * from './AppUtilsStore'
export * from './NotificationStore'
export * from './FeedStore'
export * from './test-listing'
export * from './OfflineStorageProgressStore'
export * from '../common-library/store'
export * from './NewDashboardStore'
export * from './CustomModalStore'
export * from './NewGoalsDataStore'
export * from './ActivityCardDataStore'
export * from './TestAssignmentStore'
export * from './test-attempt'

const profileDataStore = new ProfileDataStore();
const editProfileDataStore = new EditProfileDataStore();
const dialogStore = new DialogStore();
const resultStore = new ResultStore();
const homeDataStore = new HomeDataStore();
const testsStore = new TestsStore();
const profileActivityStore = new ProfileActivityStore();
const performanceStore = new PerformanceStore();
const socialDataStore = new SocialDataStore();
const goalsDataStore = new GoalsDataStore();
const assignmentStore = new AssignmentStore();
const appContextStore = new AppContextStore();
const appUtilsStore = new AppUtilsStore();
const notificationStore = new NotificationStore();
const feedStore = new FeedStore();
const testDownloadMetaStore = new TestDownloadMetaStore();
const offlineStorageProgressStore = new OfflineStorageProgressStore();
const newDashboardStore = new NewDashboardStore();
const customModalStore = new CustomModalStore();
const newGoalsDataStore = new NewGoalsDataStore();
const activityCardDataStore = new ActivityCardDataStore();
const testAssignmentStore = new TestAssignmentStore();
const testAttemptStore = new TestAttemptStore();

export default {
  homeDataStore,
  profileDataStore,
  testsStore,
  profileActivityStore,
  editProfileDataStore,
  dialogStore,
  performanceStore,
  socialDataStore,
  goalsDataStore,
  assignmentStore,
  resultStore,
  appContextStore,
  appUtilsStore,
  notificationStore,
	feedStore,
	testAttemptStore,
  testDownloadMetaStore,
  offlineStorageProgressStore,
  newDashboardStore,
  customModalStore,
  newGoalsDataStore,
  activityCardDataStore,
	testAssignmentStore
}
