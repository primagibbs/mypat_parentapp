import { action, computed, observable, toJS, ObservableMap } from 'mobx'
import { curry, get, set, toLower, pull, chain, flatten, pick,
	isEqual, clone, startCase, isArray } from 'lodash'
import lzstring from 'lz-string'
import { NetworkCallbacks, TestNetworkModule } from '../../api-layer'
import { API_IDS, TEST_TYPE, RESPONSE_ERROR_CODE } from '../../common'
import { BaseRequest, BaseResponse } from '../../http-layer'
import { MOCK_DATA } from './mock-test-data'
import {displayData, getDropDownGoalId, getDropDownGoalName, getSelectedStudent} from '../../utils'
import commonStores from '../../common-library/store'
import RNFetchBlob from 'react-native-fetch-blob'

import {
	Timer,
	msToTime,
	INTERVAL
} from './Timer'

import {
	TestDownloadItemStore
} from './TestDownloadItemStore'

import {
	IFinishPayload,
	IQuestionSection,
	IQuestion,
	IQuestionStats,
	IQuestionNode,
	IQuestionMapItem,
	IQuestionsMap,
	ICorrectOption,
	ITest,
	ISectionsQuestionsMap,
	QuestionItemStore,
	ISubSection,
	questionCodes
} from './types'

import { navigateSimple, showSnackbar, createNotification, deleteLocalNotification, hideLoader } from '../../services'
import {
	saveTestTime, getMobileUpTime, saveTestAnswer, saveTestAttempt,
	fetchAllTestAnswers, fetchAllTestAttempts, deleteTestAnswer,
	deleteTestAttempt, fetchTestAttempts, removeTestAnswersByQId,
	// tslint:disable-next-line:max-line-length
	saveTestAttemptData, realmToPojo, getRemainingTime, deleteTest,
	updateDownloadedTestAttempted, saveDownloadedTest, isTestAttempted,
	getTestTimeData, deleteTestTime, deleteLocalTestAnswers, isTestSubmitted,
	updateDownloadedTest, getDownloadedTestItemData, updateDownloadedTestItemData, saveDownloadTest, getDownloadTestData, deleteDownloadedTest, isTestDownloaded
} from '../../persistence/utils'
import { colors } from '../../config'
import store from '..';

const fs = RNFetchBlob.fs
const APP_FOLDER = fs.dirs.SDCardApplicationDir

let isSyncing = false

function answerCheck(userAnswers, correctAnswers) {
	// console.warn('ua', JSON.stringify(userAnswers, null, 2))
	// console.warn('ca', JSON.stringify(correctAnswers, null, 2))
	if (!correctAnswers) {
		return null
	}
	// denotes the correct answer count
	let correct = 0
	// denotes the wrong answer count
	let wrong = 0
	// 1 denotes that the answer is wrong
	let isWrong = 1
	// TODO: Gaurav: this is temporary
	// correctAnswers = correctAnswers.map(item => ({
	//   ...item,
	//   id: item.value
	// }))
	if (userAnswers.length <= correctAnswers.length) {
		for (let i = 0; i < userAnswers.length; i++) {
			isWrong = 1
			const userAns = userAnswers[i]
			for (let j = 0; j < correctAnswers.length; j++) {
				const corAns = correctAnswers[j]
				const posTol = correctAnswers[j].positiveTolerance || 0.02
				const negTol = correctAnswers[j].negativeTolerance || 0.02
				if (isArray(corAns.value)) {
					corAns.value.sort()
					userAns.value.sort()
				}
				if (corAns.id && userAns.id) {
					if (corAns.id === userAns.id && isEqual(corAns.value, userAns.value)) {
						correct++
						isWrong = 0
						break
					} else if (corAns.id === userAns.id && userAns.value.length === 0) {
						isWrong = 0
						break
					}
				} else {
					if (isEqual(corAns.value, userAns.value)) {
						correct++
						isWrong = 0
						break
					} else if (posTol >= 0 && negTol >= 0 && userAns.value >= corAns.value - negTol && userAns.value <= corAns.value + posTol) {
						correct++
						isWrong = 0
						break
					}
				}
			}
			if (isWrong)
				wrong++
		}
		return { correct, wrong }
	}
	return {
		correct: 0,
		wrong: 1
	}
}

function divide(num, den) {
	if (den !== 0) {
		return num / den
	}
	return 0
}

function percentage(num, den) {
	// tslint:disable-next-line:radix
	let value = Number((divide(num, den) * 100).toFixed(2))
	return value
}

const concat = (str, value) => `${displayData(value)}${str}`
const curriedConcat = curry(concat)

function msToTimeStr(duration: number) {
	// tslint:disable:radix
	let seconds: any = parseInt('' + (duration / 1000) % 60)
	let minutes: any = parseInt('' + (duration / (1000 * 60)) % 60)
	let hours: any = parseInt('' + (duration / (1000 * 60 * 60)) % 24)
	// tslint:enable:radix
	hours = (hours < 10) ? '0' + hours : hours
	minutes = (minutes < 10) ? '0' + minutes : minutes
	seconds = (seconds < 10) ? '0' + seconds : seconds
	if (hours > 0) {
		return `${hours}h ${minutes}m ${seconds}s`
	} else if (minutes > 0) {
		return `${minutes}m ${seconds}s`
	} else {
		return `${seconds}s`
	}
}

const questionCorrectPredicate = question => question.isCorrect === true
const questionIncorrectPredicate = question => question.isCorrect === false
const isUnattemptedPredicate = question => !question.isAttempted
const subjectPredicate = question => question.subject
const esayPredicate = question => question.difficultyLevel
const mediumPredicate = question => question.difficultyLevel
const difficultPredicate = question => question.difficultyLevel

const predicates = {
	correct: {
		condition: '||',
		predicate: questionCorrectPredicate
	},
	incorrect: {
		condition: '||',
		predicate: questionIncorrectPredicate
	},
	unattempted: {
		condition: '||',
		predicate: isUnattemptedPredicate
	}
}

const setToNewTestDownloadItemStore = () => new TestDownloadItemStore()
const setToEmptyArray = () => []
const setToFalse = () => false
const setToNull = () => null
const setToEmptyObject = () => ({})

const DEFAULT_SETTING = {
	// TODO: Udhay, there should be more default settings, this is incomplete
	navigation: setToNull,
	serverTime: setToNull,
	questionsLinkedList: setToNull,
	currentQuestionInLL: setToNull,
	questionMap: setToEmptyObject,
	sectionsQuestionsMap: setToEmptyObject,
	questions: setToEmptyArray,
	testData: setToNull,
	currentQuestion: setToNull,
	questionStats: () => ({
		notAnswered: 0,
		answered: 0,
		markedForReview: 0,
		notVisited: 0,
		answeredAndMarkedForReview: 0
	}),
	userTimeTaken: () => 0,
	testEnd: setToFalse,
	currentSectionInfo: setToNull,
	questionPaletteSectionId: setToNull,
	questionPaletteSubSectionId: setToNull,
	questionPaletteView: () => 'grid',
	isQuestionPaletteLoading: setToNull,
	sectionList: setToEmptyArray,
	allPaletteData: setToNull,
	currentPaletteData: setToNull,
	testPerformance: setToNull,
	comparisonData: setToNull,
	solutionData: setToNull,
	flatList: setToNull,
	testDuration: setToNull,
	attemptId: setToNull,
	courseId: setToNull,
	testId: setToNull,
	testType: setToNull,
	downloadedTestData: setToNull,
	testDownloadItemStore: setToNewTestDownloadItemStore,
	subjectList: setToNull,
	weaknessListData: setToNull,
	filters: () => new ObservableMap({
		correct: undefined,
		incorrect: undefined,
		unattempted: undefined,
		easy: undefined,
		medium: undefined,
		diffcult: undefined,
		smcq: undefined,
		mmcq: undefined,
		trueFalse: undefined,
		matrixType: undefined,
		analytical: undefined,
		conceptual: undefined,
		intuitive: undefined,
		minTomax: undefined,
		maxTomin: undefined
	}),
	finishPayload: () => ({
		attemptData: [],
		attemptId: '',
		courseId: '',
		testId: '',
		totalTimeTaken: ''
	}),
	timer: () => new Timer(),
	answeredFirstQuestion: false,
	assignmentDueDate: setToNull,
	notificationMap: setToEmptyObject
}

export const FILTER_KEYS = {
	TIME_TAKEN: 'timeTaken',
	ACCURACY: 'accuracy',
	SPEED: 'speed',
	MARKS_SCORED: 'marksScored',
	ATTEMPT_PERC: 'attemptPercentage'
}

export class TestAttemptStore implements NetworkCallbacks {
	// TODO: default settings must be updated
	navigation;
	serverTime;
	questionsLinkedList: IQuestionNode;
	currentQuestionInLL: IQuestionNode;
	questionMap: IQuestionsMap;
	sectionsQuestionsMap: ISectionsQuestionsMap;
	questions: QuestionItemStore[];
	testData: ITest;
	@observable currentQuestion: QuestionItemStore;
	@observable questionStats: IQuestionStats;
	userTimeTaken: number = 0;
	@observable testEnd: boolean;
	// sectionId, sectionName, subSectionId, subSectionName;
	@observable currentSectionInfo;
	@observable questionPaletteSectionId: string = null;
	@observable questionPaletteSubSectionId: string = null;
	@observable questionPaletteView: string = 'grid';
	@observable isQuestionPaletteLoading: boolean;
	sectionList;
	allPaletteData;
	@observable currentPaletteData;
	@observable testPerformance;
	@observable comparisonData;
	@observable solutionData;
	@observable filters;
	@observable weaknessListData;
	@observable strengthListData;
	@observable timer;
	@observable subjectList;

	@observable selectedCategory;
	@observable downloadProgress: any = 0;
	@observable isTestDownloaded: boolean = false;

	flatList;
	testDuration;
	attemptId;
	courseId;
	testId;
	testName;
	assignmentId;
	testItemData;
	finishPayload: IFinishPayload;
	localTestAttemptData;
	testType;
	isAttemptTest = false;
	// TODO: Gaurav: temp download data holder
	downloadedTestData;
	testDownloadItemStore;
	subjectPerformanceStats;
	answeredFirstQuestion;
	assignmentDueDate;
	notificationMap = {};
	testDownloadingInProgress = false;
	studentId: any;

	constructor() {
		this.init()
	}

	@action
	init(compressedData?) {
		this.navigation = DEFAULT_SETTING.navigation()
		this.serverTime = DEFAULT_SETTING.serverTime()
		this.questionsLinkedList = DEFAULT_SETTING.questionsLinkedList()
		this.currentQuestionInLL = DEFAULT_SETTING.currentQuestionInLL()
		this.questionMap = DEFAULT_SETTING.questionMap()
		this.sectionsQuestionsMap = DEFAULT_SETTING.sectionsQuestionsMap()
		this.questions = DEFAULT_SETTING.questions()
		this.testData = DEFAULT_SETTING.testData()
		this.currentQuestion = DEFAULT_SETTING.currentQuestion()
		this.questionStats = DEFAULT_SETTING.questionStats()
		this.userTimeTaken = DEFAULT_SETTING.userTimeTaken()
		this.testEnd = DEFAULT_SETTING.testEnd()
		this.currentSectionInfo = DEFAULT_SETTING.currentSectionInfo()
		this.questionPaletteSectionId = DEFAULT_SETTING.questionPaletteSectionId()
		this.questionPaletteSubSectionId = DEFAULT_SETTING.questionPaletteSubSectionId()
		this.questionPaletteView = DEFAULT_SETTING.questionPaletteView()
		this.isQuestionPaletteLoading = DEFAULT_SETTING.isQuestionPaletteLoading()
		this.sectionList = DEFAULT_SETTING.sectionList()
		this.allPaletteData = DEFAULT_SETTING.allPaletteData()
		this.currentPaletteData = DEFAULT_SETTING.currentPaletteData()
		this.testPerformance = DEFAULT_SETTING.testPerformance()
		this.comparisonData = DEFAULT_SETTING.comparisonData()
		this.solutionData = DEFAULT_SETTING.solutionData()
		this.flatList = DEFAULT_SETTING.flatList()
		this.testDuration = DEFAULT_SETTING.testDuration()
		this.attemptId = DEFAULT_SETTING.attemptId()
		this.courseId = DEFAULT_SETTING.courseId()
		this.testId = DEFAULT_SETTING.testId()
		this.testType = DEFAULT_SETTING.testType()
		this.finishPayload = DEFAULT_SETTING.finishPayload()
		this.downloadedTestData = DEFAULT_SETTING.downloadedTestData()
		this.testDownloadItemStore = DEFAULT_SETTING.testDownloadItemStore()
		this.filters = DEFAULT_SETTING.filters()
		this.subjectList = DEFAULT_SETTING.subjectList()
		this.weaknessListData = DEFAULT_SETTING.weaknessListData()
		this.strengthListData = DEFAULT_SETTING.weaknessListData()
		this.timer = DEFAULT_SETTING.timer()
		this.notificationMap = DEFAULT_SETTING.notificationMap()
	}

	@action
	setSelectedCategory(categ) {
		this.selectedCategory = categ.key
	}
	@action
	async getTestRemaingTime() {
		const networkStore = commonStores.networkDataStore
		let startTime
		// console.warn('testItemData', this.testItemData)
		if (networkStore.isNetworkConnected) {
			this.testItemData.attempts.forEach(function (attempt) {
				if (attempt.totalTime === 0 && attempt.score === 0 && attempt.correct === 0 && attempt.incorrect === 0) {
					startTime = attempt.startTime
				}
			})
			if (startTime) {
				// Resume block
				let difference
				let serverTime
				if (networkStore.isNetworkConnected) {
					serverTime = store.testAssignmentStore.getServerTime()
				} else {
					serverTime = (new Date()).getTime()
				}

				const differenceTemp =  store.testAssignmentStore.getRemainsHours(this.testItemData , startTime, serverTime)
				if (this.testItemData.type === 'chap') {
					const differenceHours =  (new Date(this.testItemData.dueDate)).getTime() - serverTime
					difference =  differenceHours < differenceTemp ? differenceHours : differenceTemp
				} else {
					difference = differenceTemp
				}
				// console.warn('difference', difference)
				return difference
			}
		} else {
			startTime = await getRemainingTime(this.testId, this.assignmentId)
			return startTime
		}
	}
	@action
	async startTest() {
		const now = new Date()
		let remainingTime
		remainingTime = await this.getTestRemaingTime()
		// console.warn('TestAttemptStore: startTest(): remainingTime', remainingTime)
		const testAttempted = await isTestAttempted(this.testId, this.assignmentId)
		if (!remainingTime || remainingTime <= 0) {
			// Fresh Attempt
			// console.warn('Fresh Attempt')
			await deleteTestAnswer(this.testId, this.assignmentId) // Delete previous answers if any

			getMobileUpTime().then((mobileUpTime) => {
				saveTestTime(this.testId, this.assignmentId, {
					startTime: now,
					systemTime: now,
					mobileUpTime: parseInt(mobileUpTime + '', 10),
					testDuration: this.testDuration
				})
			})
			this.startTimer(this.testDuration * 1000)
			const testAttemptData = {
				assignmentId: this.assignmentId,
				correct: 0,
				improvement: 0,
				incorrect: 0,
				isSubmitted: false,
				rank: 0,
				rankPotential: '',
				score: 0,
				startTime: now,
				status: 'notFinished',
				totalScore: 0,
				totalTime: 0,
				_id: this.attemptId,
				duration: this.testDuration,
				attempted: true
			}
			await updateDownloadedTestAttempted(this.testId, this.assignmentId, testAttemptData, false)
			const networkStore = commonStores.networkDataStore
			if (networkStore.isNetworkConnected) {
				// Hit questions API
				const payload = {
					attemptId: this.attemptId,
					courseId: this.courseId,
					duration: this.testDuration,
					questionIds: [],
					testId: this.testId
				}
				this.testData.questions.forEach((question, index) => {
					payload.questionIds.push(question.qId)
				})
				const config = { 'methodType': 'POST' }
				const testNetworkModule = new TestNetworkModule(config, this)
				await testNetworkModule.questions(JSON.stringify(payload), true)
			}
		} else {
			// console.warn('remainingTime***', remainingTime)
			const testTimeData = await getTestTimeData(this.testId, this.assignmentId)
			// console.warn('$this.testDuration: ', this.testDuration)
			// console.warn('$remainingTime: ', remainingTime)
			// console.warn('$startTime Original: ', now)
			// console.warn('$startTime Mod: ', new Date(now.getTime() - ((this.testDuration - (remainingTime / 1000) * 1000))))
			// console.warn('Deduct: ', ((this.testDuration - (remainingTime / 1000)) * 1000))
			if (!testTimeData) {
				// tslint:disable-next-line: max-line-length
				// When test started on web then resumed on app, we set the startTime to the approximate startTime of test on the web, by deducting the elapse time from now.
				getMobileUpTime().then((mobileUpTime) => {
					saveTestTime(this.testId, this.assignmentId, {
						startTime: new Date(now.getTime() - ((this.testDuration - (remainingTime / 1000)) * 1000)) ,
						systemTime: now,
						mobileUpTime: parseInt(mobileUpTime + '', 10),
						testDuration: this.testDuration
					})
				})
				// console.warn('Test time data saved on resume!!!!!')
			}
			this.startTimer(remainingTime)
		}
	}

	setNavigationObject(navigation) {
		this.navigation = navigation
	}

	async answerQuestion(qId = null, offlinePayload = null) {
		let payload = offlinePayload
		if (!payload) {
			const question = this.questionMap[qId].question
			payload = JSON.stringify(question.answerPayload)
		}
		if (commonStores.networkDataStore.isNetworkConnected) {
			const config = { 'methodType': 'POST' }
			const testNetworkModule = new TestNetworkModule(config, this)
			// console.warn('*********************' , payload)
			try {
				await testNetworkModule.answerQuestion(payload, true)
			} catch (e) {
				alert('Internet Failure!')
			}
		}
		const testId = this.testData.testId
		const assignmentId = this.testData.assignmentId
		const attemptId = this.attemptId
		saveTestAnswer(testId, assignmentId, {
			attemptId,
			body: payload,
			systemTime: new Date(),
			qId
		})
	}

	@action
	async fetchTestResult(testId, attemptId) {
		const config = { methodType: 'GET' }
		const student = await getSelectedStudent();
			let studentId;
			if (student) {
				studentId = student.id;
			}
		const verifyNetworkModule = new TestNetworkModule(config, this)
		await verifyNetworkModule.getTestResult({
			urlParams: {
				testId,
				attemptId
			}
		}, studentId)
	}

	@action
	async giveSolutionRating(testId, attemptId, questionId, isRated, rating) {
		const config = { methodType: 'POST' }
		const verifyNetworkModule = new TestNetworkModule(config, this)
		await verifyNetworkModule.postSolutionRating({
			urlParams: {
				testId,
				attemptId,
				questionId,
				isRated,
				rating
			}
		})
	}

	@action
	async getSubjectFilterList(testID) {
		const config = { 'methodType': 'GET' }
		const payload = JSON.stringify({
			'testId': testID // '5bf254731e93006371927795'
		})
		const testNetworkModule = new TestNetworkModule(config, this)
		await testNetworkModule.getSubjectFiltersList(payload, true)
	}
	@action
	async getWeaknessList(testId: any, attemptId: any, courseId: any) {
		const config = { 'methodType': 'GET' };
		const payload = JSON.stringify({
			'testId': testId, // '5bec121b1e93006371924615',
			'attemptId': attemptId, // '5c9b17a2e580ed0578d57c17',
			'goalId': courseId, // '5ace202e9fe7b01d5806bb7c',
			'goalName': '' // this is optional
		});
		if (!this.studentId) {
			const student = await getSelectedStudent();
			this.studentId = student.id
		}
		// console.warn('getWeaknessList', payload)
		const testNetworkModule = new TestNetworkModule(config, this);
		await testNetworkModule.getWeaknessList(payload, this.studentId, true);
	}
	@action
	async getTestSolution(testID, attemptId, subjectId, solutionType) {
		const config = { 'methodType': 'GET' }
		const payload = JSON.stringify({
			'testId': testID,
			'attemptId': attemptId,
			'subjectId': subjectId,
			'solutionType': solutionType
		})
		const testNetworkModule = new TestNetworkModule(config, this)
		await testNetworkModule.getTestSolution(payload, true)
	}

	@action
	async getTestComparison(testID: any, courseId: any, attemptId: any) {
		if (!this.studentId) {
			const student = await getSelectedStudent();
			this.studentId = student.id
		}
		const config = { 'methodType': 'GET' };
		const payload = JSON.stringify({
			'testId': testID,
			'attemptId': attemptId,
			'courseId': courseId
		});
		const testNetworkModule = new TestNetworkModule(config, this);
		// console.warn('payload', payload)
		await testNetworkModule.getTestSolutionComparison(payload, this.studentId, true)
	}

	@action
	setDownlaodedProgress(progress) {
		this.downloadProgress = progress
	}
	@action
	getDownlaodedProgress() {
		return this.downloadProgress
	}
	startTimer(duration: number) {
		// console.warn('duration', duration)
		const navigation  = this.navigation
		const that = this
		const changeHandler = isTimeUp => {
			that.userTimeTaken += INTERVAL
			that.currentQuestion.attemptDataTimeStats.userTimeTaken += INTERVAL
			if (isTimeUp && !that.testEnd) {
				this.userTimeTaken = duration
				that.testEnd = true
				that.finish.call(that, true).then(function success() {
					that.stopTimer()
					if (!navigation) {
						// console.warn('Hola!')
					} else {
						setTimeout(() => {
							navigateSimple(navigation, 'ResultLandingScreenPage')
						}, 2000)
					}
				}, function failure(err) {
					showSnackbar(err)
				})
				// TODO: Udhay: update autoSubmit property of testData
			}
		}
		this.timer.start(duration, changeHandler)
	}

	@action
	pauseTimer() {
		// TODO: remove this
	}

	@action
	resumeTimer(remainingTime = 0) {
		this.timer.setRemainingTime(remainingTime)
	}

	@action
	stopTimer() {
		if (this.timer) {
			this.timer.stop()
		}
	}

	@action
	remainingTestTime() {
		return this.timer.remainingTime
	}

	@action
	setup() {
		/** */
	}

	// helper
	updateQuestionsInSectionsQuestionsMap(questionsMeta) {
		questionsMeta.map((question, index) => {
			const sectionId = question.section.id
			const subSectionId = question.section.subSection.id
			this.sectionsQuestionsMap[sectionId].questions.push(question.qId)
			this.sectionsQuestionsMap[sectionId].stats.notAnswered += 1
			this.sectionsQuestionsMap[sectionId].stats.notVisited += 1
			this.sectionsQuestionsMap[sectionId].stats.totalQuestions += 1

			this.sectionsQuestionsMap[sectionId].subSection[
				subSectionId
				].questions.push(question.qId)
			this.sectionsQuestionsMap[sectionId].subSection[
				subSectionId
				].stats.notAnswered += 1
			this.sectionsQuestionsMap[sectionId].subSection[
				subSectionId
				].stats.notVisited += 1
		})
	}

	// helper
	initializeSectionsQuestionsMap(sectionsMeta) {
		sectionsMeta.map((sectionMeta, sectionMetaIndex) => {
			const sectionDisplayName = sectionMeta.name
			this.sectionsQuestionsMap[sectionMeta.id] = {
				id: sectionMeta.id,
				name: sectionMeta.name,
				subSection: {},
				displayName: sectionDisplayName,
				questions: [],
				stats: {
					notAnswered: 0,
					answered: 0,
					markedForReview: 0,
					notVisited: 0,
					answeredAndMarkedForReview: 0,
					totalQuestions: 0,
					totalCorrectQuestions: 0,
					totalIncorrectQuestions: 0,
					totalMarks: 0,
					totalCorrectMarks: 0,
					totalIncorrectMarks: 0,
					timeTaken: 0
				}
			}

			const subSectionsMeta = sectionMeta.subSection
			subSectionsMeta.map((subSectionMeta, subSectionsMetaIndex) => {
				const subSectionDisplayName = 'SEC ' + (subSectionsMetaIndex + 1)
				this.sectionsQuestionsMap[sectionMeta.id].subSection[
					subSectionMeta.id
					] = {
					// TODO: Udhay: In case all the keys are to be entered, just use ...subSection
					...pick(subSectionMeta, [
						'id',
						'name',
						'isHidden',
						'partialMarks',
						'negativeMarks',
						'positiveMarks',
						'marks',
						'noOfQuestions'
					]),
					questions: [],
					displayName: subSectionDisplayName,
					stats: {
						notAnswered: 0,
						answered: 0,
						markedForReview: 0,
						notVisited: 0,
						answeredAndMarkedForReview: 0
					}
				}
			})
		})
	}

	// helper
	updateQuestionsMap(node, question) {
		this.questionMap[question.qId] = {
			questionNode: node,
			question: question
		}
	}

	// helper
	updateQuestions(question) {
		this.questions.push(question)
	}

	updateResumedData(data) {
		// console.warn('**updateResumedData CALLED: ',  data.length, JSON.stringify(data, null, 2))
		if (data.length === 0) {
			return
		}

		data.forEach((questionData) => {
			const question = this.questionMap[questionData.questionId].question
			// console.warn('@@@@Updating Answer')

			question.attemptDataTimeStats.userTimeTaken = parseFloat(questionData.timePerQuestion)
			question.attemptData.isAttempted = questionData.isAttempted
			question.attemptData.isMarked = questionData.isMarked
			question.attemptData.isVisited = questionData.isVisited
			question.attemptData.isAnsweredMarkedForReview = questionData.isAnsweredMarkedForReview
			question.attemptData.answer = questionData.answer || questionData.options

			// tslint:disable-next-line:max-line-length
			if (questionData.questionCode === questionCodes.Blanks || questionData.questionCode === questionCodes.Integer || questionData.questionCode === questionCodes.Numerical) {
				question.attemptData.answerType = 'value'
			} else {
				question.attemptData.answerType = 'id'
			}
		})

		this.questions.forEach((questionData) => {
			let isAttempted = questionData.attemptData.isAttempted
			let isMarked = questionData.attemptData.isMarked
			let isVisited = questionData.attemptData.isVisited
			let isAnsweredMarkedForReview = questionData.attemptData.isAnsweredMarkedForReview

			// TODO:udhay Cross check if stats working
			const section = this.sectionsQuestionsMap[questionData.sectionId]
			const subSection = this.sectionsQuestionsMap[questionData.sectionId].subSection[questionData.subSectionId]

			if (isAttempted) {
				this.questionStats.answered++
				section.stats.answered++
				subSection.stats.answered++
				this.questionStats.notAnswered--
				section.stats.notAnswered--
				subSection.stats.notAnswered--
			}

			// tslint:disable-next-line:max-line-length
			if (isAnsweredMarkedForReview) {
				this.questionStats.answeredAndMarkedForReview++
				section.stats.answeredAndMarkedForReview++
				subSection.stats.answeredAndMarkedForReview++
			}

			if (!isAttempted && isMarked) {
				this.questionStats.markedForReview++
				section.stats.markedForReview++
				subSection.stats.markedForReview++
			}

			if (isVisited) {
				this.questionStats.notVisited--
				section.stats.notVisited--
				subSection.stats.notVisited--
			}
		})
	}

	@action
	async processTestData(data, partial?) {
		// for downloaded api
		data = JSON.parse(data)
		// console.warn('data is being processed')
		// console.warn('processTestData DATA: startTime', data.startTime)
		// console.warn('processTestData DATA: attemptData', data.attemptData)
		let testResumedOnline = false
		if (typeof data === 'string') {
			data = JSON.parse(data)
		}
		// console.warn('&&&&&&&&&&&&&&&&&&&&&& ' , data)
		// console.warn('data', Object.keys(data.questionData))
		// console.warn('questionData', data.questionData)

		let testResumed = false
		if (data.data.hasOwnProperty('redisData')) {
			testResumedOnline = true
		}
		if (testResumedOnline) {
			this.testData = {
				...data.data.dbData,
				questionContent: data.questionData.qDatas

			}
		} else {
			this.testData = {
				...data.data,
				questionContent: data.questionData.qDatas
			}
		}

		this.testDuration = this.testData.duration
		let isAssignment = !!this.testData.assignmentId

		if (isAssignment) {
			if (this.assignmentDueDate) {
				let dueDate = (new Date(this.assignmentDueDate))
				if (dueDate) {
					let now = (new Date()).getTime()
					let dueDateDiff = Math.trunc((new Date(dueDate).getTime() - now) / 1000)
					if (dueDateDiff < this.testDuration) {
						this.testDuration = dueDateDiff
					}
				}
			}
		}

		// console.warn('ProcessTestData: ', JSON.stringify(this.testData, null, 2))
		// console.warn('ProcessTestData KEYS: ', JSON.stringify(Object.keys(this.testData), null, 2))

		const testMeta = {
			...pick(this.testData, [
				'assignmentId',
				'attemptNo',
				'challengeId',
				'courseId',
				'duration',
				'orgId',
				'teacherId',
				'testId',
				'startTime',
				'_id'
			])
		}
		// console.warn('this.testData** ' , this.testData)
		// console.warn('this.testMeta * ' ,  testMeta)
		const testId: string = testMeta._id
		const assignmentId: string = testMeta.assignmentId
		const now = new Date()
		this.attemptId = testMeta._id
		this.testId = testMeta.testId
		this.courseId = this.testData.courseId
		this.assignmentId = this.testData.assignmentId
		this.testType = this.testData.testType
		// console.warn('TEST META: ', JSON.stringify(testMeta, null, 2))
		// console.warn('this.testId: ', this.testId)
		// console.warn('this.assignmentId: ', this.assignmentId)
		if (partial) {
			return
		}
		const questions: QuestionItemStore[] = this.testData.questionContent.map(
			question => new QuestionItemStore(question, testMeta)
		)

		const sectionsMeta: IQuestionSection[] = this.testData.sections
		this.initializeSectionsQuestionsMap(sectionsMeta)

		const questionsMeta: IQuestion[] = this.testData.questions
		this.updateQuestionsInSectionsQuestionsMap(questionsMeta)

		let head = null
		let pointerInLL = null
		questions.forEach((question, index) => {
			this.updateQuestions(question)
			// update the linked list
			const node: IQuestionNode = {
				qId: question.qId,
				prev: pointerInLL,
				next: null
			}
			if (index === 0) {
				head = node
			}
			if (pointerInLL) {
				pointerInLL.next = node
			}
			if (questions.length === index + 1) {
				node.next = head
			}
			pointerInLL = node
			// update question map
			this.updateQuestionsMap(node, question)

			// find section, update questionItemStore properties from questionMeta
			questionsMeta.forEach(questionMeta => {
				if (questionMeta.qId === question.qId) {
					question.sectionId = questionMeta.section.id
					question.subSectionId = questionMeta.section.subSection.id
					question.questionNo = questionMeta.questionNo
					question.attemptData = {
						...pick(questionMeta.attemptData, [
							'isCorrect',
							'questionPartialMarks',
							'questionNegativeMarks',
							'questionTotalMarks',
							'userTimeTaken',
							'userNegativeMarks',
							'userPositiveMarks',
							'userTotalMarks',
							'answer',
							'isAttempted'
						]),
						isVisited: false,
						isAnsweredMarkedForReview: false,
						isMarked: false
					}
				}
			})
		})
		head.prev = pointerInLL
		this.questionsLinkedList = head
		// set question stats
		this.questionStats = {
			notAnswered: questions.length,
			answered: 0,
			markedForReview: 0,
			notVisited: questions.length,
			answeredAndMarkedForReview: 0
		}

		// cache the section list
		this.prepareSectionList();

		const localAttemptData = await fetchAllTestAnswers(this.testId, this.assignmentId)
		let localAttemptDataParsed = localAttemptData.map((item) => JSON.parse(item.body))
		let questionsData = localAttemptData.map((item) => JSON.parse(item.body))
		// console.warn('localAttemptDataParsed ', questionsData.length, ' ', JSON.stringify(questionsData, null, 2))

		if (testResumedOnline) {
			// console.warn('REDIS DATA ', data.data.redisData.length, ' ', JSON.stringify(data.data.redisData, null, 2))
			// if (commonStores.networkDataStore.isNetworkConnected) {
			// If online, update both online and offline attempt data
			let redisData = data.data.redisData

			redisData.forEach((questionData, index) => {
				const qId = questionData.questionId
				const exist = questionsData.filter((data) => data.questionId === qId).length > 0 ? true : false

				if (!exist) {
					// console.warn('PUSHING from REDIS DATA')
					questionsData.push(questionData)
				}
			})
			this.updateResumedData(questionsData)

			// this.updateResumedData([
			//   ...data.data.redisData,
			//   ...localAttemptDataParsed
			// ])
			// this.updateResumedData(data.data.redisData)
			// } else {
			// If offline ,update local attempt data
			// this.updateResumedData(localAttemptDataParsed)
			// }
		} else {
			const remainingTime = await getRemainingTime(this.testId, this.assignmentId)
			if (!commonStores.networkDataStore.isNetworkConnected && !!remainingTime) {
				// offline and not a fresh attempt
				// console.warn('HERE!!!!')
				this.updateResumedData(localAttemptDataParsed)
			}
		}

		// set current question
		this.setCurrentQuestion(this.questionsLinkedList.qId)
	}



	@action
	setVisited(qId: string): boolean {
		const isVisited = this.questionMap[qId].question.setVisited()
		// console.warn('hasBeenSetToVisited: ', hasBeenSetToVisited)
		if (!isVisited) {
			// updating overall stats
			this.questionStats.notVisited -= 1

			const sectionId = this.questionMap[qId].question.getSectionId()
			const subSectionId = this.questionMap[qId].question.getSubSectionId()

			// updating section stats
			this.sectionsQuestionsMap[sectionId].stats.notVisited -= 1

			// updating sub-section stats
			// tslint:disable-next-line:max-line-length
			this.sectionsQuestionsMap[sectionId].subSection[
				subSectionId
				].stats.notVisited -= 1
		}
		return isVisited
	}

	@action
	markForReview(mark: boolean = true, qId?: string): void {
		const question: QuestionItemStore = qId
			? this.questionMap[qId].question
			: this.currentQuestion
		const sectionId = question.getSectionId()
		const subSectionId = question.getSubSectionId()

		const hasBeenSet = question.setMarkedForReview(mark)
		const increment = (x, key) => (x[key] = x[key] + 1)
		const decrement = (x, key) => (x[key] = x[key] - 1)
		const update = mark ? increment : decrement

		if (hasBeenSet) {
			if (question.isAttempted()) {
				// update overall stats
				update(this.questionStats, 'answeredAndMarkedForReview')

				// update section stats
				// tslint:disable-next-line:max-line-length
				update(
					this.sectionsQuestionsMap[sectionId].stats,
					'answeredAndMarkedForReview'
				)

				// updating sub-section stats
				// tslint:disable-next-line:max-line-length
				update(
					this.sectionsQuestionsMap[sectionId].subSection[subSectionId].stats,
					'answeredAndMarkedForReview'
				)
			} else {
				update(this.questionStats, 'markedForReview')

				// update section stats
				// tslint:disable-next-line:max-line-length
				update(this.sectionsQuestionsMap[sectionId].stats, 'markedForReview')

				// updating sub-section stats
				// tslint:disable-next-line:max-line-length
				update(
					this.sectionsQuestionsMap[sectionId].subSection[subSectionId].stats,
					'markedForReview'
				)
			}
		}
	}

	@action
	async markAnswer(qId: string, answer: ICorrectOption) {
		// console.warn('actual answer', answer)
		const question: QuestionItemStore = this.questionMap[qId].question
		const isPreviouslyAttempted = question.isAttempted()
		const isPreviouslyMarked = question.attemptData.isMarked || question.attemptData.isAnsweredMarkedForReview
		question.setAnswer(answer)
		const isNowAttempted = question.isAttempted()
		const section = this.sectionsQuestionsMap[question.sectionId]
		const subSection = this.sectionsQuestionsMap[question.sectionId].subSection[question.subSectionId]
		if (isPreviouslyAttempted !== isNowAttempted) {
			if (isPreviouslyMarked) {
				if (isNowAttempted) {
					this.questionStats.markedForReview--
					this.questionStats.answeredAndMarkedForReview++
					section.stats.markedForReview--
					section.stats.answeredAndMarkedForReview++
					subSection.stats.markedForReview--
					subSection.stats.answeredAndMarkedForReview++
				} else {
					this.questionStats.markedForReview++
					this.questionStats.answeredAndMarkedForReview--
					section.stats.markedForReview++
					section.stats.answeredAndMarkedForReview--
					subSection.stats.markedForReview++
					subSection.stats.answeredAndMarkedForReview--
				}
			}
			if (isNowAttempted) {
				this.questionStats.answered++
				this.questionStats.notAnswered--
				section.stats.answered++
				section.stats.notAnswered--
				subSection.stats.answered++
				subSection.stats.notAnswered--
			} else {
				this.questionStats.answered--
				this.questionStats.notAnswered++
				section.stats.answered--
				section.stats.notAnswered++
				subSection.stats.answered--
				subSection.stats.notAnswered++
			}
		}
		// await this.answerQuestion(qId)
	}

	@action
	setCurrentQuestion(qId) {
		// console.warn('SetcurrentQuestionID called')
		const question: IQuestionMapItem = this.questionMap[qId]
		// console.warn('question content', JSON.stringify(question.question, null, 2))
		this.currentQuestion = question.question
		// console.warn('setCurrentQuestion', this.currentQuestion)
		this.currentQuestionInLL = question.questionNode
		const { sectionId, subSectionId } = toJS(this.currentQuestion)
		this.setCurrentQuestionVisited(qId)
		// update section if required
		if (
			!this.currentSectionInfo ||
			(this.currentSectionInfo &&
				(subSectionId !== this.currentSectionInfo.subSectionId ||
					sectionId !== this.currentSectionInfo.sectionId))
		) {
			const currentSectionInfo = {
				sectionId,
				subSectionId,
				name: this.getSectionName(sectionId, subSectionId)
			}
			this.currentSectionInfo = currentSectionInfo
			return currentSectionInfo
		}
		return this.currentSectionInfo
	}

	@action
	setCurrentQuestionVisited(qId?: string) {
		this.setVisited(qId)
	}

	getSubSectionsBySectionKey(sectionId) {
		return chain(this.sectionsQuestionsMap[sectionId].subSection)
			.values()
			.map((subSection: ISubSection) => ({
				...subSection,
				sectionId,
				subSectionId: subSection.id,
				name: this.getSectionName(sectionId, subSection.id)
			}))
			.value()
	}

	prepareSectionList() {
		this.sectionList = chain(this.sectionsQuestionsMap)
			.values()
			.flatMap(section => this.getSubSectionsBySectionKey(section.id))
			.value()
	}

	getSectionMarkingScheme(sectionId, subSectionId) {
		const positiveMarks = this.sectionsQuestionsMap[sectionId].subSection[
			subSectionId
			].positiveMarks
		const negativeMarks = this.sectionsQuestionsMap[sectionId].subSection[
			subSectionId
			].negativeMarks
		if (negativeMarks === 0) {
			return `Marks: +${positiveMarks} `
		} else {
			return `Marks: +${positiveMarks}, -${negativeMarks}`
		}
	}

	setSubjectPerformace() {
		let stats = []
		Object.keys(this.sectionsQuestionsMap).forEach(((sectionId) => {
			let section = this.sectionsQuestionsMap[sectionId]
			stats.push({
				name: section.name,
				stats: section.stats
			})
		}).bind(this))
		this.subjectPerformanceStats = stats
		//  console.warn('Offline Subject Performance Stats: ', JSON.stringify(this.subjectPerformanceStats, null, 2))
	}

	getSectionsStats() {
		return toJS(this.subjectPerformanceStats)
	}
	setTestStats(testName: string, testId: string, attemptId: string,
							 courseId: string, assignmentId: string, testType: string,
							 testPerformance: any, subjectPerformanceStats: any
	) {
		this.attemptId = attemptId
		this.courseId = courseId
		this.testId = testId
		this.assignmentId = assignmentId
		this.testName = testName
		this.testType = testType
		this.testPerformance = testPerformance
		if (this.subjectPerformanceStats) {
			this.subjectPerformanceStats = subjectPerformanceStats
		}
	}
	setPerformanceDataFromAPI(data) {
		let stats = []
		data.map(((result) => {
			// console.warn('RESULT: ', JSON.stringify(result, null, 2))
			stats.push({
				name: result.name,
				stats: {
					totalMarks: result.totalMarks,
					marksPercentage: result.marksPercentage,
					totalCorrectQuestions: null,
					timeTakenPercentage: result.timePercentage
				}
			})
		}).bind(this))
		this.subjectPerformanceStats = stats
	}

	getSubjectList() {
		const subjectProgress = this.getSectionsStats()
		if (subjectProgress) {
			return subjectProgress.map((section) => section.name)
		}
		return []
	}

	getCurrentQuestion() {
		return this.currentQuestion
	}

	getSectionName(sectionId, subSectionId) {
		const sectionName = this.sectionsQuestionsMap[sectionId].displayName
		const subSectionName = this.sectionsQuestionsMap[sectionId].subSection[
			subSectionId
			].displayName
		return `${sectionName} ${subSectionName}`
	}

	@action
	setCurrentQuestionByIndex(index) {
		// const qId = clone(this.questions[index].qId)
		// console.warn('qid validation', qId, this.questions[index].qId)
		// console.warn('setCurrentQuestionByIndex() called')
		if (this.questions[index]) {
			return this.setCurrentQuestion(this.questions[index].qId)
		}
		// this.answerQuestion(qId)
		return this.currentSectionInfo
	}

	goToQuestionByIndex(index) {
		const transformedIndex = index === 0 ? index : index - 1
		if (this.flatList) {
			this.flatList.scrollToIndex({ index: transformedIndex, animated: false })
		}
	}

	@action
	_updatePaletteSelection(sectionId, subSectionId) {
		this.questionPaletteSectionId = sectionId
		this.questionPaletteSubSectionId = subSectionId
		this.setCurrentPaletteData()
	}

	@action
	updatePaletteSelection(sectionId, subSectionId) {
		if (!this.isQuestionPaletteLoading) {
			this.isQuestionPaletteLoading = true
			const self = this
			setTimeout(() => {
				self._updatePaletteSelection(sectionId, subSectionId)
			}, 500)
		}
	}

	@action
	setAllPaletteData(data) {
		this.allPaletteData = data
		this.currentPaletteData = data
		this.isQuestionPaletteLoading = false
	}

	@action
	setCurrentPaletteData() {
		let paletteData
		if (!this.questionPaletteSectionId && !this.questionPaletteSubSectionId) {
			paletteData = this.allPaletteData
		} else if (
			this.questionPaletteSectionId ||
			this.questionPaletteSubSectionId
		) {
			paletteData = [
				this.allPaletteData.find(
					item =>
						item.sectionId === this.questionPaletteSectionId &&
						item.subSectionId === this.questionPaletteSubSectionId
				)
			]
		}
		this.currentPaletteData = paletteData
		this.isQuestionPaletteLoading = false
	}

	getCurrentPaletteData() {
		return toJS(this.currentPaletteData)
	}

	evaluateAnswer(question: QuestionItemStore) {
		// console.warn('evaluateAnswer() Called')
		const correctAnswer = question.getCorrectAnswer()
		// console.warn('question content', question.getQuestionContent())
		// console.warn('correctAnswer', JSON.stringify(correctAnswer, null, 2))
		// console.warn(
		//   'answer',
		//   JSON.stringify(question.getAnswer().answer, null, 2)
		// )
		// TODO: Need to handle question types other than MCQs
		const dataFromAnswerCheck = answerCheck(question.getAnswer().answer, correctAnswer.data)
		// console.warn(dataFromAnswerCheck, '###############data from check')
		// console.warn('dfac', JSON.stringify(dataFromAnswerCheck, null, 2))
		if (!dataFromAnswerCheck) {
			return null
		}
		let correctAnswerData: any = null
		if (correctAnswer && correctAnswer.data) {
			correctAnswerData = correctAnswer.data
		}
		let qlen = correctAnswer && correctAnswer.data && correctAnswer.data.length
		let isPartial = false
		// console.warn(question.attemptData.isAttempted, '*******')
		if (question.attemptData.isAttempted && question.questionCode === questionCodes.MMCQ) {
			// for MMCQ, all options should have been correct
			if (dataFromAnswerCheck.wrong > 0) {
				dataFromAnswerCheck.wrong = 1
				dataFromAnswerCheck.correct = 0
			}
			// if the answered length is less than the correct answers
			// tslint:disable-next-line:one-line
			else if (dataFromAnswerCheck.correct < correctAnswer.data.length) {
				if (question.attemptData.questionPartialMarks) {
					isPartial = true
				} else {
					dataFromAnswerCheck.wrong = 1
					dataFromAnswerCheck.correct = 0
				}
			} else {
				dataFromAnswerCheck.wrong = 0
				dataFromAnswerCheck.correct = 1
			}
			/**
			 * This is a hack, as the total marks is the actual marks
			 * However, in case of matrix type questions,
			 * actual marks is partial * number of correct options.
			 */
			qlen = 1
		}
		// question data
		// question.attemptData.isAttempted = true
		// console.warn(question.attemptData.questionTotalMarks, qlen, dataFromAnswerCheck.correct, isPartial)
		let userPositiveMarks =
			(question.attemptData.questionTotalMarks / qlen) * dataFromAnswerCheck.correct
		let userNegativeMarks =
			question.attemptData.questionNegativeMarks * dataFromAnswerCheck.wrong
		if (isPartial) {
			question.attemptData.userTotalMarks =
				question.attemptData.questionPartialMarks * dataFromAnswerCheck.correct
			question.attemptData.userPositiveMarks = question.attemptData.userTotalMarks
		} else {
			question.attemptData.userPositiveMarks = userPositiveMarks
			question.attemptData.userNegativeMarks = userNegativeMarks
			question.attemptData.userTotalMarks = userPositiveMarks - userNegativeMarks
			// console.warn('attempt data', question.attemptData.userTotalMarks, userPositiveMarks, userNegativeMarks)
		}
		question.attemptData.isCorrect = question.attemptData.userTotalMarks > 0
		// tslint:disable-next-line:max-line-length
		// console.warn('total marks', question.qId, '  ', question.questionNo, question.attemptData.userTotalMarks, ' ', question.attemptDataTimeStats.userTimeTaken)
	}

	@action
	async sendFinishPayload(offlinePayload = null) {
		// this is the case where offline sync is taking place
		const isPayloadExternallySupplied = !!offlinePayload
		const externalPayload = isPayloadExternallySupplied ? JSON.parse(offlinePayload) : null
		if (externalPayload) {
			delete externalPayload.testPerformance
			delete externalPayload.subjectPerformanceStats
		}
		const payload = offlinePayload || JSON.stringify(this.finishPayload)
		let attemptId = null
		if (offlinePayload !== null) {
			attemptId = offlinePayload.attemptId
		} else {
			attemptId = JSON.parse(payload).attemptId
		}
		const testId = isPayloadExternallySupplied ?
			externalPayload.testId :
			this.testData.testId
		const assignmentId = isPayloadExternallySupplied ?
			externalPayload.assignmentId :
			this.testData.assignmentId
		// console.warn('Payload!!', JSON.stringify(payload, null, 2))
		// console.warn('Payload!!', this.finishPayload.attemptId)
		if (!isPayloadExternallySupplied) {
			await saveTestAttempt(testId, assignmentId, {
				attemptId: attemptId,
				body: payload,
				systemTime: new Date(),
				isConsumed: false
			})
		}
		// console.warn('##finishTest AttemptId: ', this.finishPayload.attemptId)
		// console.warn('##finishTest Payload: ', JSON.stringify(payload, null, 2))
		if (commonStores.networkDataStore.isNetworkConnected) {
			const config = { 'methodType': 'POST' }
			// console.warn('test submit payload', JSON.stringify(JSON.parse(payload), null, 2))
			const testNetworkModule = new TestNetworkModule(config, this)
			await testNetworkModule.finishTest(payload)
		}
	}

	@action
	async buildFinishPayload() {
		this.finishPayload.attemptId = this.attemptId
		this.finishPayload.courseId = this.courseId
		this.finishPayload.testId = this.testId
		this.finishPayload.totalTimeTaken = '' + this.userTimeTaken / (INTERVAL * 10)

		this.questions.forEach((question) => {
			let answerPayload = question.answerPayload
			if (question.attemptData.isVisited) {
				this.finishPayload.attemptData.push({
					...pick(answerPayload, [
						'optionsMatrix',
						'optionsMcq',
						'type',
						'isMarked',
						'isVisited',
						'startTime',
						'duration',
						'sectionId',
						'subSectionId',
						'attemptId',
						'testId',
						'questionId',
						'options',
						'timePerQuestion',
						'courseId',
						'isAttempted',
						'attemptNo',
						'isAnsweredMarkedForReview'
					])
				})
			}
		})
	}

	@action
	finish(auto = false) {
		const that = this
		return new Promise<any>(async (resolve, reject) => {
			const attempts = that.questions.filter(questionItem => questionItem.isAttempted())
			// console.warn('Auto value in finish(): ', auto)
			if (!attempts.length && !auto) {
				reject('Please attempt at least 1 question')
				return
			}

			const qId = this.getCurrentQuestion().qId
			const question = this.questionMap[qId].question
			question._updateAnswerPayload()
			that.timer.stop()
			await this.answerQuestion(qId)


			// console.warn('building finish payload')
			that.buildFinishPayload()
			// console.warn('sending finish payload')
			that.sendFinishPayload()
			// console.warn('questions length', that.questions.length)
			that.questions.forEach(questionItem => {
				const answer = questionItem.getAnswer().answer
				// console.warn('Evaluating answer: ', JSON.stringify(answer, null, 2))
				if (answer && answer.length) {
					// console.warn('Evaluating answer called')
					that.evaluateAnswer.call(that, questionItem)
				}
			})
			let isAssignment = this.testData.assignmentId === null || this.testData.assignmentId === undefined ? false : true
			if (isAssignment) {
				// console.warn('** ASSIGNMENT ID: ', this.testData.assignmentId)
				// console.warn('** Is Assignment: ', isAssignment)
				await updateDownloadedTestItemData(this.testId, this.assignmentId, 'Past')
			}

			// console.warn('generating result')
			that.generateResult()
			// console.warn('generating result over')
			// console.warn('deleting local answers')
			// await deleteLocalTestAnswers(this.testId, this.assignmentId)
			await deleteTestAnswer(this.testId, this.assignmentId)
			resolve()
		})
	}

	// untested code starts
	@action
	generateResult() {
		// console.warn('generating result inside')
		let totalQuestions = 0
		let totalScore = 0
		let questionsAttempted = 0
		let correctQuestions = 0
		// number of questions attempted / number of total questions
		let attemptRate
		// the marks of correct questions / marks of total attempted questions
		let _marksOfCorrectQuestions = 0
		let _marksOfIncorrectQuestions = 0
		let _marksOfTotalAttemptedQuestions = 0
		let accuracyRate
		// total time taken
		let totalTimeTaken = 0
		// time spent on questions visited but not answered
		let wastedTime = 0
		// consider answered questions only, questions per min
		let _totalTimeTakenForCorrectAnswers = 0
		let speed
		let _totalTimeTakenForAnsweredQuestions = 0

		let maxTotalScore = 0
		// console.warn('questions', this.questions.length)
		this.questions.forEach(question => {
			let grantedMarks = question.attemptData.userTotalMarks || 0
			// console.warn('grantedMarks', grantedMarks)
			let questionTotalMarks = question.attemptData.questionTotalMarks || 0
			maxTotalScore += questionTotalMarks
			// let questionIncorrectMarks = question.attemptData. || 0
			const sectionId = question.sectionId
			const subSectionId = question.subSectionId
			const isAttempted = question.attemptData.isAttempted
			const isVisited = question.attemptData.isVisited
			const isCorrectQuestion = question.attemptData.isCorrect
			const isGrantedIncorrect = ((grantedMarks < 0) || (grantedMarks === 0 && isAttempted))
			const isGrantedCorrect = (isAttempted && grantedMarks > 0)
			// console.warn('isAttempted', isAttempted)
			// TODO: Gaurav: This should be handled better. Changing interval shouldn't affect this
			const timeTaken = (question.attemptDataTimeStats.userTimeTaken / (INTERVAL * 10))
			// console.warn('time error', question.attemptDataTimeStats.userTimeTaken, timeTaken)
			const qStats = this.sectionsQuestionsMap[sectionId].subSection[
				subSectionId
				]
			// TODO: Gaurav: partial case needs to be handled
			// if (isAttempted) {
			//   if (isCorrectQuestion) {
			//     grantedMarks = qStats.positiveMarks
			//   } else {
			//     grantedMarks = qStats.negativeMarks
			//   }
			// }
			const questionMarks = grantedMarks
			// generate general statistics
			totalQuestions++
			totalTimeTaken += timeTaken
			this.sectionsQuestionsMap[sectionId].stats.totalMarks += questionTotalMarks
			// console.warn('isAttempted: ', isAttempted)
			if (isAttempted) {
				questionsAttempted++
				totalScore += questionMarks
				_marksOfTotalAttemptedQuestions += questionTotalMarks

				if (isCorrectQuestion === true) {
					correctQuestions++
					// _marksOfCorrectQuestions += questionMarks
					_totalTimeTakenForCorrectAnswers += timeTaken
					// console.warn('now..time', _totalTimeTakenForCorrectAnswers, timeTaken)
					this.sectionsQuestionsMap[
						sectionId
						].stats.totalCorrectMarks += questionMarks
					this.sectionsQuestionsMap[
						sectionId
						].stats.totalCorrectQuestions++
				} else if (isCorrectQuestion === false) {
					this.sectionsQuestionsMap[
						sectionId
						].stats.totalIncorrectMarks += questionMarks
				}

				this.sectionsQuestionsMap[sectionId].stats.timeTaken += timeTaken
				_totalTimeTakenForAnsweredQuestions += timeTaken

				if (isGrantedCorrect) {
					_marksOfCorrectQuestions += questionTotalMarks
				} else if (isGrantedIncorrect) {
					_marksOfIncorrectQuestions += questionTotalMarks
				}

				if (isCorrectQuestion !== isGrantedCorrect) {
					// console.warn(' QID: ', question.qId, 'Diff*******************')
				}

			} else if (isVisited) {
				wastedTime += timeTaken
			}
			// tslint:disable-next-line:max-line-length
		})
		accuracyRate = percentage(_marksOfCorrectQuestions, _marksOfTotalAttemptedQuestions)
		attemptRate = percentage(_marksOfTotalAttemptedQuestions, maxTotalScore)
		speed = divide(correctQuestions, _totalTimeTakenForCorrectAnswers) * 60
		Object.keys(this.sectionsQuestionsMap).forEach(sectionId => {
			// update score
			// const score = this.sectionsQuestionsMap[sectionId].stats.totalCorrectMarks
			const totalCorrectMarks = this.sectionsQuestionsMap[sectionId].stats.totalCorrectMarks
			const totalInCorrectMarks = this.sectionsQuestionsMap[sectionId].stats.totalIncorrectMarks
			const score = totalCorrectMarks + totalInCorrectMarks
			const maxScore = this.sectionsQuestionsMap[sectionId].stats.totalMarks
			// update time taken percentage

			this.sectionsQuestionsMap[sectionId].stats.marksPercentage = percentage(score < 0 ? 0 : score, maxScore)
			this.sectionsQuestionsMap[sectionId].stats.timeTakenPercentage =
				percentage(this.sectionsQuestionsMap[sectionId].stats.timeTaken, _totalTimeTakenForAnsweredQuestions)

			// console.warn('Section TimeTaken: ', this.sectionsQuestionsMap[sectionId].stats.timeTaken)
			// console.warn('_totalTimeTakenForAnsweredQuestions: ', _totalTimeTakenForAnsweredQuestions)
		})

		this.testPerformance = {
			questionsAttempted,
			totalQuestions,
			correctQuestions,
			attemptRate,
			accuracyRate,
			totalTimeTakenInSeconds: totalTimeTaken,
			totalTimeTaken: msToTime(totalTimeTaken * 1000),
			wastedTime: msToTime(wastedTime * 1000),
			speed,
			testName: this.testData.testName,
			maxMarks: this.testData.maxTotalScore,
			marksScored: totalScore,
			// correctQuestionsPercentage: Number((correctQuestions / totalQuestions) * 100).toFixed(1)
			correctQuestionsPercentage: Number((_marksOfCorrectQuestions / maxTotalScore) * 100).toFixed(1)
		}
		// console.warn('this.testPerformance: ', JSON.stringify(this.testPerformance, null, 2))
		this.setSubjectPerformace()
		this.updateLocalTestAttempt()
	}

	async updateLocalTestAttempt () {
		const testTimeData = await getTestTimeData(this.testId, this.assignmentId)
		const testAttemptData = {
			assignmentId: this.assignmentId,
			correct: this.testPerformance.correctQuestionsPercentage,
			improvement: 0,
			// tslint:disable-next-line:max-line-length
			incorrect: (((this.testPerformance.questionsAttempted - this.testPerformance.correctQuestions) / this.testPerformance.totalQuestions) * 100).toFixed(2),
			isSubmitted: true,
			rank: 0,
			rankPotential: '',
			score: this.testPerformance.marksScored,
			startTime: testTimeData.startTime,
			status: 'finished',
			totalScore: this.testPerformance.maxMarks,
			totalTime: this.testPerformance.totalTimeTakenInSeconds,
			_id: this.attemptId,
			attempted: true,
			testPerformance: JSON.stringify(this.testPerformance),
			subjectPerformance: JSON.stringify(this.subjectPerformanceStats)
		};
		await updateDownloadedTestAttempted(this.testId, this.assignmentId, testAttemptData, true);
		await deleteTestTime(this.testId, this.assignmentId)
	}

	setTestPerformanceOnline(resultRespone) {
		const resultData = resultRespone.data.stats;
		this.testPerformance = {
			questionsAttempted : resultData.attemptNo, // Not available in resultData
			totalQuestions: null, // Not available in resultData
			correctQuestionsPercentage: resultData.correct,
			attemptRate: resultData.attemptRate,
			accuracyRate: resultData.accuracyRate,
			totalTimeTaken: msToTime(resultData.totalTimeTaken * 1000),
			wastedTime: msToTime(resultData.wastedTime * 1000),
			speed: resultData.speed * 60,
			testName: this.testName || '',
			maxMarks: resultData.totalScore,
			marksScored: resultData.totalScoreObtained
		}
	}
	getTestPerformance() {
		return toJS(this.testPerformance || {})
	}

	@action
	processWeakListData(data) {
		this.weaknessListData = data
		this.createWeaknessListData()
	}

	@action
	createWeaknessListData() {
		if (!this.weaknessListData) {
			return null
		}
		let weaknessList = []
		let strenghtList = []
		this.weaknessListData = this.weaknessListData.map(listItem => {
			const chapterName = listItem.chapterName
			const weakness = listItem.weaknes
			listItem.concept.map(item => {
				if (item.weakness < 70) {
					weaknessList.push({
						conceptName: item.conceptName,
						chapterName: chapterName,
						conceptWeakness: item.weakness ? item.weakness.toFixed(0) : 0,
						weaknessColor: colors.Red
					})
				} else {
					strenghtList.push({
						conceptName: item.conceptName,
						chapterName: chapterName,
						conceptWeakness: item.weakness ? item.weakness.toFixed() : 0,
						weaknessColor: colors.PrimaryBlue
					})
				}
			})
		})
		this.weaknessListData = weaknessList.sort((a, b) => {
			if (parseInt(a.conceptWeakness, 10) < parseInt(b.conceptWeakness, 10))
				return -1
			if (parseInt(a.conceptWeakness, 10) > parseInt(b.conceptWeakness, 10))
				return 1
			return 0
		})
		this.strengthListData = strenghtList.sort((a, b) => {
			if (parseInt(a.conceptWeakness, 10) > parseInt(b.conceptWeakness, 10))
				return -1
			if (parseInt(a.conceptWeakness, 10) < parseInt(b.conceptWeakness, 10))
				return 1
			return 0
		})
	}
	getWeaknessListData() {
		return toJS(this.weaknessListData)
	}
	getStrenghtListData() {
		return toJS(this.strengthListData)
	}
	@action
	generateSolutionsData(solutionData) {
		this.solutionData = solutionData
		this.createSolutionsData()
	}
	// TODO: Gaurav: This should ideally be resolved on the backend
	getDifficultyLevel(difficulty) {
		if (difficulty && difficulty.indexOf('ConceptIds') > -1) {
			return difficulty.split('ConceptIds')[0]
		}
		return difficulty
	}
	getConceptsIncluded(concepts) {
		if (concepts && concepts.length) {
			return concepts.map(concept => startCase(toLower(concept))).join(', ')
		}
		return ''
	}
	@action
	createSolutionsData() {
		if (!this.solutionData) {
			return null
		}
		this.solutionData = this.solutionData.map(solution => {
			const isCorrect = solution.markCalculation &&
				solution.markCalculation.marks !== null &&
				solution.markCalculation.marks > 0
			return {
				questionId: solution.id,
				isAttempted: solution.isAttempted,
				time: msToTimeStr((solution.time || 0) * 1000),
				isCorrect,
				isIncorrect: solution.isAttempted && !isCorrect,
				questionType: solution.questionType,
				difficultyLevel: this.getDifficultyLevel(solution.difficultyLevel),
				questionContent: solution.question,
				questionNo: solution.questionNo,
				subject: solution.section.name || '',
				optionContent: solution.optionContent,
				marks: solution.markCalculation.marks,
				ispartial: solution.markCalculation.ispartial,
				nigativeMarks: solution.markCalculation.negativeMarks,
				solutionContent: solution.solution.answer,
				submittedAns: solution.submittedAns,
				correctAnswer: solution.correctAnswer,
				conceptsIncluded: this.getConceptsIncluded(solution.conseptIncluded)
			}
		})
	}

	getSolutionsData() {
		return toJS(this.solutionData)
	}
	getFilters() {
		return toJS(this.filters)
	}
	applyFilters = (filterPredicates, question) => filterPredicates.reduce((bool, predicateKey: string) => {
		if (predicates[predicateKey] === '||') {
			return predicates[predicateKey](question) || bool
		} else if (predicates[predicateKey] === '&&') {
			return predicates[predicateKey](question) && bool
		}
	}, true)

	getSolutionFilteredData() {
		if (this.solutionData) {
			const filters = this.getFilters()
			const appliedFilters = Object.keys(filters)
				.filter(key => filters[key])
			const filterPredicates = appliedFilters.map(key => ({
				predicate: predicates[key],
				filter: filters[key]
			}))
			const solutionLsit = this.solutionData.filter(question => this.applyFilters(filterPredicates, question))
			return solutionLsit
		}
	}
	@action
	setFilters(types) {
		let typesVal = toJS(types)
		this.filters = DEFAULT_SETTING.filters()
		if (types && types.size) {
			Object.keys(typesVal).forEach(key => {
				this.filters.set(key, true)
			})
		}
	}
	generateComparisonData(comparisonData) {
		this.comparisonData = comparisonData // TEST_RESULT_COMPARISON_DATA.data
		this.comparisonData = this.createComparisonData()
	}

	createComparisonData() {
		if (!this.comparisonData) {
			return null
		}
		const chapterLength = this.comparisonData.you.chapterWiseDistribution.length
		// const conceptLength = this.comparisonData.you.conceptWiseDistribution.length
		const subjectLength = this.comparisonData.you.subjectWiseDistribution.length
		const greater = (a, b) => a > b
		const lesser = (a, b) => a <= b
		const categories = []
		const compareBy = [
			{
				value: 'Time Taken',
				key: 'timeTaken',
				transformer: x => msToTimeStr(x * 1000),
				comparison: lesser
			},
			{
				value: 'Accuracy Rate',
				key: 'accuracy',
				transformer: curriedConcat(''),
				comparison: greater
			},
			{
				value: 'Speed',
				key: 'speed',
				transformer: (speed) => {
					const transformedSpeed = speed * 60
					return curriedConcat('/m')(transformedSpeed)
				},
				comparison: greater
			},
			{
				value: 'Score',
				key: 'marksScored',
				maxDataKey: 'totalMarks',
				comparison: greater
			},
			{
				value: 'Attempt Rate',
				key: 'attemptPercentage',
				transformer: curriedConcat(''),
				comparison: greater
			}
		]
		if (subjectLength > 1) {
			categories.push({
				value: 'Subjects',
				key: 'subjectWiseDistribution'
			})
		}
		if (chapterLength > 1) {
			categories.push({
				value: 'Chapters',
				key: 'chapterWiseDistribution'
			})
		}
		categories.push({
			value: 'Concepts',
			key: 'conceptWiseDistribution'
		})

		// ugly handling for marksScored
		this.comparisonData = ({
			...this.comparisonData,
			average: {
				...this.comparisonData.average,
				subjectWiseDistribution: this.comparisonData.average.subjectWiseDistribution.map(item => ({
					...item,
					marksScored: displayData(item.marksScored / this.comparisonData.totalFirstAttempt),
					totalMarks: displayData(item.totalMarks / this.comparisonData.totalFirstAttempt),
					timeTaken: displayData(item.timeTaken / this.comparisonData.totalFirstAttempt),
					speed: item.speed
				})),
				conceptWiseDistribution: this.comparisonData.average.conceptWiseDistribution.map(item => ({
					...item,
					marksScored: displayData(item.marksScored / this.comparisonData.totalFirstAttempt),
					totalMarks: displayData(item.totalMarks / this.comparisonData.totalFirstAttempt),
					timeTaken: displayData(item.timeTaken / this.comparisonData.totalFirstAttempt),
					speed: item.speed
				})),
				chapterWiseDistribution: this.comparisonData.average.chapterWiseDistribution.map(item => ({
					...item,
					marksScored: displayData(item.marksScored / this.comparisonData.totalFirstAttempt),
					totalMarks: displayData(item.totalMarks / this.comparisonData.totalFirstAttempt),
					timeTaken: displayData(item.timeTaken / this.comparisonData.totalFirstAttempt),
					speed: item.speed
				}))
			}
		})

		return {
			data: toJS(this.comparisonData),
			categories,
			compareBy
		}
	}
	resetSolutionFilter() {
		this.filters = DEFAULT_SETTING.filters
	}
	resetPerformance() {
		this.testPerformance = undefined
	}
	@action
	subjectData(subjectData) {
		this.subjectList = subjectData
	}
	getSubjectFilterData() {
		return this.subjectList
	}

	async saveTestData(response) {
		let category
		const isAssignment = !!this.testItemData.assignmentId
		const isConceptTest = this.testItemData.type === 'concept'
		if (isAssignment) {
			category = 'assignment'
		} else if (isConceptTest) {
			category = 'concept'
		} else {
			category = 'main'
		}

		const testItem = {
			type: this.testItemData.type,
			status: this.testItemData.status,
			attempt: this.testItemData.attempts && this.testItemData.attempts.length ? 'attempted' : 'unattempted',
			testName: this.testItemData.testName,
			category,
			attempted: false,
			body: JSON.stringify(response)
		}
		// console.warn('SAVING with--> ID', this.testItemData.id)
		// await saveDownloadedTest(this.testItemData.id, this.testItemData, testItem)
	}

	@action
	async handleDownloadedTest(response) {
		console.log('handle dt', response)
		this.init()
		const forDecompression = !!response.data ? response.data : response
		const downloadedTest = lzstring.decompressFromEncodedURIComponent(forDecompression)
		// console.warn('Test successfully decompressed', downloadedTest)
		if (commonStores.networkDataStore.isNetworkConnected) {
			await this.processTestData(downloadedTest, true)
			// console.warn('Test data processed partially ')
			const testId = clone(this.testId)
			// console.warn('************ 1 ' , this.assignmentId)
			const assignmentId = clone(this.assignmentId)
			// console.warn('************ 2 ' , assignmentId)
			// console.warn('##### dtd', JSON.stringify(JSON.parse(this.downloadedTestData), null, 2))
			this.init()
			// console.warn('this.isAttemptTest', this.isAttemptTest)
			const updateDownloadTest = await updateDownloadedTest(testId, assignmentId, this.isAttemptTest)
			this.downloadedTestData =  updateDownloadTest || downloadedTest
			// console.warn('dtd', JSON.stringify(this.downloadedTestData, null, 2))
		} else {
			this.downloadedTestData = downloadedTest
		}
		// console.warn(this.downloadedTestData, 'gaurav')
		await this.processTestData(this.downloadedTestData)
		this.testDownloadItemStore.setTestDownloaded(true)
		this.testDownloadingInProgress = false
		this.testDownloadItemStore.setTestDownloading(false)
		hideLoader()
		//  this.deleteNotification(this.testId, this.assignmentId)
	}

	@action
	async onSuccess(apiId: string, response: any, requestParams?: any) {
		// console.warn('onSuccess', 'apiId ', apiId)
		switch (apiId) {
			case API_IDS.DOWNLOAD_TEST:
				await this.handleDownloadedTest(response)
				break
			case API_IDS.GET_SUBJECT_FILTERS:
				this.subjectData(response.data.subjects)
				break
			case API_IDS.GET_WEAKNESS_LIST:
				this.processWeakListData(response.data)
				break
			case API_IDS.GET_TEST_SOLUTION:
				this.solutionData = []
				this.generateSolutionsData(response.data)
				break
			case API_IDS.GET_COMPARISON:
				this.generateComparisonData(response.data)
				break
			case API_IDS.FINISH_TEST:
				const requestObj = JSON.parse(requestParams)
				const testIdVal = requestObj.testId
				const assignmentIdVal = requestObj.assignmentId
				const attemptId = requestObj.attemptId
				// console.warn()
				await deleteTestAnswer(testIdVal, assignmentIdVal)
				const testAttempts: any[] = await fetchTestAttempts(attemptId)
				// console.warn('test attempts', testAttempts.length, testIdVal, assignmentIdVal)
				// comment the following out, to test offline finish sync
				if (testAttempts && testAttempts.length) {
					testAttempts.forEach(async (attempt) => {
						const transformedAttempt = realmToPojo(attempt)
						transformedAttempt.isConsumed = true
						await saveTestAttempt(testIdVal, assignmentIdVal, transformedAttempt)
					})
				}
				break
			case API_IDS.ANSWER_QUESTION:
				const context = JSON.parse(requestParams)
				const questionId = context.questionId
				await removeTestAnswersByQId(questionId)
				break
			case API_IDS.GET_TEST_RESULT:
				this.setTestPerformanceOnline(response)
				this.setPerformanceDataFromAPI(response.data.distributionChart)
				break
			case API_IDS.SAVE_RATING:
				if (response.data.attemptId) {
					showSnackbar('Thanks for rating')
				}
				break
			default:
				break
		}
	}
	onSuccessBadRequest(apiId: any, response: any) {
		// TODO
		if (apiId === API_IDS.GET_TEST_SOLUTION) {
			this.solutionData = []
		}
		// console.warn(apiId, 'BAD REQ')
	}
	onSuccessUnAuthorized(apiId: any, response: any) {
		// TODO
		// console.warn('UnAuth Req: ', apiId, response)
		// console.warn('onSuccessUnAuthorized ' , apiId, response)
		switch (apiId) {
			case API_IDS.DOWNLOAD_TEST:
				showSnackbar(response.message)
				this.testDownloadItemStore.setTestDownloading(false)
				break
			default:
				break
		}

	}
	onFailure(apiId: string, request: BaseRequest): void {
		// console.warn('on failure: ', apiId)
		switch (apiId) {
			case API_IDS.DOWNLOAD_TEST:
				showSnackbar('Failed to download the test. Please try later.')
				this.testDownloadItemStore.setTestDownloading(false)
				break
			default:
				break
		}
		// TODO
	}
	validateRequestParams(): Boolean {
		return true
	}

	validateResponseParams(res: BaseResponse): Boolean {
		return true
	}

	generalValidationError(type: string, error: String): void {
		//
	}
	onComplete() {
		//
	}

	async offlineSync() {
		return new Promise(async (resolve, reject) => {
			try {
				let processableTasks = 0
				if (isSyncing || !commonStores.networkDataStore.isNetworkConnected) {
					return
				}
				isSyncing = true
				// perform sync here
				const pendingAttempts = await fetchAllTestAttempts()
				// console.warn('To be processed', pendingAttempts && pendingAttempts.length)
				if (pendingAttempts && pendingAttempts.length) {
					pendingAttempts.forEach(attempt => {
						if (!attempt.isConsumed) {
							// console.warn('*Offline Sync Payload ', JSON.stringify(JSON.parse(attempt.body), null, 2))
							this.sendFinishPayload(attempt.body)
							processableTasks++
						}
						// TODO: in the on success callback, the answers are to be deleted
					})
				}
				// console.warn('No. of tests being synced.', processableTasks)
				// const pendingAnswers = await fetchAllTestAnswers()
				// syncing ends here
				isSyncing = false
				resolve(false)
			} catch (err) {
				reject(err)
				isSyncing = false
			}
		})
	}

	getFilesBaseUrl() {
		if (this.testData && this.testData.testId) {
			const { testId, assignmentId } = this.testData
			const testKey = testId + (!assignmentId ? undefined : assignmentId)
			// console.warn('file url', `${APP_FOLDER}/${testKey}/`)
			return `file://${APP_FOLDER}/`
		}
		return `file://${APP_FOLDER}/`
	}

	deleteNotification(testId, assignmentId) {
		const key: string = testId + (!assignmentId ? undefined : assignmentId)
		// console.warn('Removing test download, notification')
		if ( this.notificationMap[key]) {
			// console.warn('Removing the key which has been fined.', this.notificationMap[key])
			deleteLocalNotification({ id: this.notificationMap[key] })
			delete this.notificationMap[key]

		}
	}

}
