import { observable, observe, computed, action } from 'mobx'
import { observer } from 'mobx-react';

export class TestDownloadItemStore {
  @observable isDownloading = false
  @observable isDownloaded = false
  testId = null
  assignmentId = null

  @action
  setTestDownloading(downloading: boolean) {
    this.isDownloading = downloading
  }
  @action
  setTestDownloaded(downloaded: boolean) {
    // console.warn('setTestDownloaded******', downloaded)
  this.isDownloaded  = downloaded
  }

  setTargetAssignment(testId, assignmentId) {
    this.testId = testId || null
    this.assignmentId = assignmentId || null
  }
  @action
  getTestDownloaded() {
    return this.isDownloaded
  }

  @action
  reset() {
    this.isDownloading = false
  }
}
