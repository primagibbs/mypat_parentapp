import { observable, observe, computed, action, toJS } from 'mobx'

export const INTERVAL = 100

function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'
 }

export function msToTime(duration: number) {
  // tslint:disable:radix
  let seconds: any = parseInt('' + (duration / 1000) % 60)
  let minutes: any = parseInt('' + (duration / (1000 * 60)) % 60)
  let hours: any = parseInt('' + (duration / (1000 * 60 * 60)) % 24)
  // tslint:enable:radix
  hours = (hours < 10) ? '0' + hours : hours
  minutes = (minutes < 10) ? '0' + minutes : minutes
  seconds = (seconds < 10) ? '0' + seconds : seconds
  return hours + ' ' + ':' + ' ' + minutes + ' ' + ':' + ' ' + seconds
}

export class Timer {
  @observable timer = 0
  @observable humanReadableRemainingTime = ''
  clearTimer
  duration = 0
  onChange
  startTime

  @computed
  get remainingTime(): number {
    return this.duration - this.timer
  }

  @action
  setHumanReadableTime() {
    if (this.duration <= toJS(this.timer)) {
     // console.warn('time update calculation error', this.duration, this.timer)
    } else {
      this.humanReadableRemainingTime = msToTime(this.duration - this.timer)
    }
  }

  @action
  setRemainingTime(remainingTime) {
    this.timer = this.duration - remainingTime
  }

  @action
  stop() {
    if (this.clearTimer) {
      clearInterval(this.clearTimer)
      this.clearTimer = null
    }
    // console.warn('stop called successfully', !!this.clearTimer, isFunction(this.clearTimer))
    // if (this.clearTimer && isFunction(this.clearTimer)) {
    //   console.warn('is function rule has been passed')
    //   this.clearTimer()
    // }
  }

  @action
  start(duration, onChange, interval = INTERVAL) {
   // console.warn('start')
    const that = this
    this.startTime = (new Date()).getTime()
    this.onChange = onChange
    this.duration = duration
    this.timer = 0
    this.stop()
    this.clearTimer = setInterval(() => {
      // console.warn('interval')
      if (that.remainingTime % 1000 === 0) {
        that.setHumanReadableTime()
      }
      that.timer = that.timer + interval
      const isTimeUp = that.timer >= that.duration
      if (isTimeUp) {
        that.stop()
      }
      if (that.onChange) {
        that.onChange(isTimeUp)
      }
    }, interval)
  }
}