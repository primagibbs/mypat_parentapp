const startTest = {
  "code":200,
  "message":"Success",
  "data":{
    "__v":0,
    "userId":"5b12835a0815c7a84e746e2e",
    "testId":"5ace202c9fe7b01d5806baa6",
    "teacherId":null,
    "assignmentId":null,
    "orgId":null,
    "courseId":"5ace202e9fe7b01d5806bb7d",
    "attemptNo":1,
    "ip":"192.168.3.177",
    "location":{
      "place":"NA"
    },
    "testType":"part",
    "duration":10800,
    "testName":"Part Test -2 (PT2) (Class XI Syllabus)-Paper 1",
    "startTime":"1970-01-18T22:41:34.329Z",
    "maxTotalScore":234,
    "status":"discarded",
    "_id":"5c6aaa79385850063eb3001d",
    "sqlId":"1120",
    "rankPotential":"Cut-off not cleared",
    "questions":[
      {
        "qId":"5aab6dbbbc1d1f3013e0ebab",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30062",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":1,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebad",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30061",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":2,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebaf",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30060",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":3,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebb1",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3005f",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":4,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebb3",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3005e",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":5,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebb5",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3005d",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":6,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebb7",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3005c",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":7,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebb9",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3005b",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":8,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebec"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebbb",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb3005a",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":9,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ebe7"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebbd",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30059",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":10,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ebe7"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebbf",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30058",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":11,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ebe7"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebc1",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30057",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":12,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ebe7"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebc3",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30056",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":13,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebc5",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30055",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":14,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebc7",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30054",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":15,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebc9",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30053",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":16,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebcb",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30052",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":17,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebcd",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30051",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":18,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ebe0"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebcf",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb30050",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":19,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ebda"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebd1",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb3004f",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":20,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ebda"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebd3",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb3004e",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":21,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ebda"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebd5",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb3004d",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":22,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ebda"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebd7",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb3004c",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":23,
        "section":{
          "name":"PHYSICS",
          "id":"5aab6dbbbc1d1f3013e0ebd9",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ebda"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebf5",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3004b",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":24,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebf7",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3004a",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":25,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebf9",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30049",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":26,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebfb",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30048",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":27,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebfd",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30047",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":28,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ebff",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30046",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":29,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec01",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30045",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":30,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec03",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb30044",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":31,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec36"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec05",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30043",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":32,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec31"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec07",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30042",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":33,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec31"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec09",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30041",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":34,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec31"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec0b",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa7a385850063eb30040",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":35,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec31"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec0d",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003f",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":36,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec0f",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003e",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":37,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec11",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003d",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":38,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec13",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003c",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":39,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec15",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003b",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":40,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec17",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa7a385850063eb3003a",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":41,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec2a"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec19",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb30039",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":42,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec24"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec1b",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb30038",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":43,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec24"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec1d",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb30037",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":44,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec24"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec1f",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa7a385850063eb30036",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":45,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec24"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec21",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb30035",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":46,
        "section":{
          "name":"CHEMISTRY",
          "id":"5aab6dbbbc1d1f3013e0ec23",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec24"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec3f",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30034",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":47,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec41",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30033",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":48,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec43",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30032",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":49,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec45",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30031",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":50,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec47",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30030",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":51,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec49",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb3002f",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":52,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec4b",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb3002e",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":53,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec4d",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb3002d",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":54,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec80"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec4f",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa79385850063eb3002c",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":55,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec7b"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec51",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa79385850063eb3002b",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":56,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec7b"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec53",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa79385850063eb3002a",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":57,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec7b"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec55",
        "questionType":"MMCQ",
        "questionCode":8,
        "_id":"5c6aaa79385850063eb30029",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":58,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Multi correct",
            "id":"5aab6dbbbc1d1f3013e0ec7b"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec57",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30028",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":59,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec59",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30027",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":60,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec5b",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30026",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":61,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec5d",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30025",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":62,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec5f",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30024",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":63,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec61",
        "questionType":"SMCQ",
        "questionCode":0,
        "_id":"5c6aaa79385850063eb30023",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":64,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Single Correct",
            "id":"5aab6dbbbc1d1f3013e0ec74"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec63",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb30022",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":65,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec6e"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec65",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb30021",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":66,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec6e"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec67",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb30020",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":67,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec6e"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec69",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb3001f",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":68,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec6e"
          }
        }
      },
      {
        "qId":"5aab6dbbbc1d1f3013e0ec6b",
        "questionType":"Integer",
        "questionCode":7,
        "_id":"5c6aaa79385850063eb3001e",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":69,
        "section":{
          "name":"MATHEMATICS",
          "id":"5aab6dbbbc1d1f3013e0ec6d",
          "subSection":{
            "name":"Integer",
            "id":"5aab6dbbbc1d1f3013e0ec6e"
          }
        }
      }
    ],
    "sections":[
      {
        "_id":"5aab6dbbbc1d1f3013e0ebd9",
        "name":"PHYSICS",
        "id":"5aab6dbbbc1d1f3013e0ebd9",
        "subSection":[
          {
            "_id":"5aab6dbbbc1d1f3013e0ebec",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ebec",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":8
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ebe7",
            "name":"Multi correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ebe7",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":4
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ebe0",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ebe0",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":6
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ebda",
            "name":"Integer",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ebda",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":5
          }
        ]
      },
      {
        "_id":"5aab6dbbbc1d1f3013e0ec23",
        "name":"CHEMISTRY",
        "id":"5aab6dbbbc1d1f3013e0ec23",
        "subSection":[
          {
            "_id":"5aab6dbbbc1d1f3013e0ec36",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec36",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":8
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec31",
            "name":"Multi correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec31",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":4
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec2a",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec2a",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":6
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec24",
            "name":"Integer",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec24",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":5
          }
        ]
      },
      {
        "_id":"5aab6dbbbc1d1f3013e0ec6d",
        "name":"MATHEMATICS",
        "id":"5aab6dbbbc1d1f3013e0ec6d",
        "subSection":[
          {
            "_id":"5aab6dbbbc1d1f3013e0ec80",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec80",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":8
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec7b",
            "name":"Multi correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec7b",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":4
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec74",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec74",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":6
          },
          {
            "_id":"5aab6dbbbc1d1f3013e0ec6e",
            "name":"Integer",
            "isHidden":false,
            "id":"5aab6dbbbc1d1f3013e0ec6e",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":5
          }
        ]
      }
    ],
    "sectionWiseResult":[

    ],
    "stats":{
      "improvement":0,
      "attemptRate":0,
      "speed":0,
      "accuracyRate":0,
      "wastedTime":0
    },
    "attemptData":{
      "userTimeTaken":0,
      "userIncorrectQuestionCount":0,
      "userCorrectQuestionCount":0,
      "userNegativeMarks":0,
      "userPositiveMarks":0,
      "correctQuestionMarks":0,
      "inCorrectQuestionMarks":0,
      "userTotalMarks":0
    },
    "isAutoSubmit":false,
    "challengeId":"",
    "needAnalysisAgain":false,
    "updatedAt":"2019-02-18T12:52:09.989Z",
    "createdAt":"2019-02-18T12:52:09.989Z",
    "courseName":"JEE(A) - 2019",
    "attemptDate":"2019-02-18T12:52:09.989Z",
    "rank":0
  },
  "assignmentInfo":null,
  "serverTime":1550494330.092
}

const startTestReattempt = {
  "code":200,
  "message":"Success",
  "data":{
    "__v":0,
    "userId":"5b12835a0815c7a84e746e2e",
    "testId":"5ace202c9fe7b01d5806ba9f",
    "courseId":"5ace202e9fe7b01d5806bb7d",
    "status":"discarded",
    "teacherId":null,
    "assignmentId":null,
    "orgId":null,
    "ip":"192.168.3.177",
    "attemptNo":2,
    "duration":10800,
    "testType":"part",
    "testName":"Part Test -1 (PT1) (Class XI Syllabus)-Paper 2",
    "location":{
      "place":"NA"
    },
    "startTime":"2019-02-18T12:55:48.491Z",
    "maxTotalScore":183,
    "_id":"5c6aab544432540620dc7dd2",
    "sqlId":"1119",
    "rankPotential":"Cut-off not cleared",
    "questions":[
      {
        "qId":"5a8ad008738197281fba58e9",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e08",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":1,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58eb",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e07",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":2,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58ed",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e06",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":3,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58ef",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e05",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":4,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58f1",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e04",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":5,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58f3",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e03",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":6,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58f5",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7e02",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":7,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba591b"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58f7",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7e01",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":8,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58f9",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7e00",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":9,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58fb",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dff",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":10,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58fd",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dfe",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":11,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba58ff",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dfd",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":12,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5901",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dfc",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":13,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5903",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dfb",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":14,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"MCQ Multi Correct",
            "id":"5a8ad008738197281fba5913"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5905",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dfa",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":15,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba590e"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5907",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df9",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":16,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba590e"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5909",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df8",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":17,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba590e"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba590b",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df7",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":18,
        "section":{
          "name":"PHYSICS",
          "id":"5a8ad008738197281fba590d",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad008738197281fba590e"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5923",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df6",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":19,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5925",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df5",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":20,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5927",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df4",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":21,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5929",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df3",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":22,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba592b",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df2",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":23,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba592d",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df1",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":24,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba592f",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7df0",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":25,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5955"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5931",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7def",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":26,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5933",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dee",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":27,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5935",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7ded",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":28,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5937",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dec",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":29,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad008738197281fba5939",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7deb",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":30,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba593b",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dea",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":31,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba593d",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7de9",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":32,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba594d"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba593f",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de8",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":33,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5948"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5941",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de7",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":34,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5948"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5943",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de6",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":35,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5948"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5945",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de5",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":36,
        "section":{
          "name":"CHEMISTRY",
          "id":"5a8ad009738197281fba5947",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5948"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba595d",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de4",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":37,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba595f",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de3",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":38,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5961",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de2",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":39,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5963",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de1",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":40,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5965",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7de0",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":41,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5967",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7ddf",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":42,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5969",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dde",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":43,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba598f"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba596b",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7ddd",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":44,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba596d",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7ddc",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":45,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba596f",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7ddb",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":46,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5971",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dda",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":47,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5973",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dd9",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":48,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5975",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dd8",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":49,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5977",
        "questionCode":8,
        "_id":"5c6aab544432540620dc7dd7",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":2,
          "questionTotalMarks":4,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":50,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Multi correct",
            "id":"5a8ad009738197281fba5987"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba5979",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dd6",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":51,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5982"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba597b",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dd5",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":52,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5982"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba597d",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dd4",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":53,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5982"
          }
        }
      },
      {
        "qId":"5a8ad009738197281fba597f",
        "questionCode":0,
        "_id":"5c6aab544432540620dc7dd3",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":0,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":54,
        "section":{
          "name":"MATHEMATICS",
          "id":"5a8ad009738197281fba5981",
          "subSection":{
            "name":"Single Correct",
            "id":"5a8ad009738197281fba5982"
          }
        }
      }
    ],
    "sections":[
      {
        "_id":"1",
        "name":"PHYSICS",
        "id":"5a8ad008738197281fba590d",
        "subSection":[
          {
            "_id":"5a8ad008738197281fba591b",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad008738197281fba591b",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad008738197281fba5913",
            "name":"MCQ Multi Correct",
            "isHidden":false,
            "id":"5a8ad008738197281fba5913",
            "partialMarks":null,
            "negativeMarks":2,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad008738197281fba590e",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad008738197281fba590e",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":4
          }
        ]
      },
      {
        "_id":"5a8ad009738197281fba5947",
        "name":"CHEMISTRY",
        "id":"5a8ad009738197281fba5947",
        "subSection":[
          {
            "_id":"5a8ad009738197281fba5955",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba5955",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad009738197281fba594d",
            "name":"Multi correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba594d",
            "partialMarks":null,
            "negativeMarks":2,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad009738197281fba5948",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba5948",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":4
          }
        ]
      },
      {
        "_id":"5a8ad009738197281fba5981",
        "name":"MATHEMATICS",
        "id":"5a8ad009738197281fba5981",
        "subSection":[
          {
            "_id":"5a8ad009738197281fba598f",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba598f",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad009738197281fba5987",
            "name":"Multi correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba5987",
            "partialMarks":null,
            "negativeMarks":2,
            "positiveMarks":4,
            "marks":0,
            "noOfQuestions":7
          },
          {
            "_id":"5a8ad009738197281fba5982",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5a8ad009738197281fba5982",
            "partialMarks":null,
            "negativeMarks":0,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":4
          }
        ]
      }
    ],
    "sectionWiseResult":[

    ],
    "stats":{
      "improvement":0,
      "attemptRate":0,
      "speed":0,
      "accuracyRate":0,
      "wastedTime":0
    },
    "attemptData":{
      "userTimeTaken":0,
      "userIncorrectQuestionCount":0,
      "userCorrectQuestionCount":0,
      "userNegativeMarks":0,
      "userPositiveMarks":0,
      "correctQuestionMarks":0,
      "inCorrectQuestionMarks":0,
      "userTotalMarks":0
    },
    "isAutoSubmit":false,
    "challengeId":"",
    "needAnalysisAgain":false,
    "updatedAt":"2019-02-18T12:55:48.717Z",
    "createdAt":"2019-02-18T12:55:48.717Z",
    "courseName":"JEE(A) - 2019",
    "attemptDate":"2019-02-18T12:55:48.717Z",
    "rank":0
  },
  "assignmentInfo":null,
  "serverTime":1550494548.762
}

const questions = {
  "qDatas":[
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A 9 kg block is originally at rest on a horizontal smooth surface. If a horizontal force <em>F</em> is applied such that it varies with time as shown in figure, the speed of block in 4 s is</p>\n\n<p><img alt=\"Description: 1\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question152118418754775.png\" /></p>\n",
              "solutionContent":"<p>Area of F-t curve = change in momentum <span class=\"math-tex\">\\(\\frac{1}{2}\\left(4+2\\right)×75=m\\left(v-u\\right)\\)</span></p>\n\n<p>225 = 9 <em>v</em></p>\n\n<p>( <em>u</em> = 0)</p>\n\n<p><em>v</em> = 25 m/s</p>\n",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebac",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>5 m/s</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>15 m/s</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>25 m/s</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>30 m/s</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebab",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Two balls of mass <em>M</em> = 9 g and <em>m</em> = 3 g are attached by massless threads <em>AO</em> and <em>OB</em>. The length <em>AB</em> is 1 m. They are set in rotational motion in a horizontal plane about a vertical axis at <em>O</em> with constant angular velocity . The ratio of length <em>AO</em> and <em>OB</em> <span class='math-tex'>\\(\\left(\\frac{AO}{OB}\\right)\\)</span> for which the tension in threads are same will be   </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187548222.png\" alt=\"Description: 2\" /></p>",
              "solutionContent":"<p><em>T</em><sub>1</sub> = <em>T</em><sub>2</sub> </p><p><span class='math-tex'>\\(M{\\omega }^{2}x=m{\\omega }^{2}\\left(l-x\\right)\\)</span></p><p><span class='math-tex'>\\(x=\\frac{m\\ l}{M+m}\\)</span></p><p><span class='math-tex'>\\(l-x=\\frac{M\\ l}{M+m}\\)</span></p><p><span class='math-tex'>\\(\\frac{AO}{OB}=\\frac{x}{l-x}=\\frac{m}{M}\\)</span> = <span class='math-tex'>\\({3 \\over 9} = \\frac{1}{3}\\)</span></p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187549639.png\" alt=\"Description: 3\" /></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebae",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{1}{3}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>3</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{2}{3}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebad",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A projectile of mass m is fired with velocity <em>v</em> from a point <em>P</em> at an angle  with horizontal as shown. Neglecting air resistance, the magnitude of change in momentum between the leaving point <em>P</em> and the highest point. </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187550234.png\" alt=\"Description: 4\" /></p>",
              "solutionContent":"<p><span class='math-tex'>\\(|m\\stackrel{\\to }{g}\\frac{T}{2}|=mg\\frac{2v\\mathrm{sin}\\theta }{g}\\)</span> = mv sin</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebb0",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>2 mv cos </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>mv cos</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>2 mv sin</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>mv sin</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebaf",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A man of mass M stands at one end of a plank of length L and mass 3 M. which lies at rest on a frictionless surface. If the man is walking wit respect to the plank with speed v then the workdone by the man be </p>",
              "solutionContent":"<p>From conservation of momentum, </p><p>M(V – V<sub>1</sub>) = 3MV<sub>1</sub>      [V<sub>1</sub> = speed of the plank.</p><p> V<sub>1</sub> = <span class='math-tex'>\\(\\frac{V}{4},\\)</span> speed of man V<sub>2</sub> = <span class='math-tex'>\\(\\frac{3V}{4}\\)</span></p><p>  Work done by man = Gain in K.E. of (man + plank)</p><p>= <span class='math-tex'>\\({1 \\over 2}M\\left( {{{3V} \\over 4}} \\right) + {1 \\over 2}3M{\\left( {{V \\over 4}} \\right)^2} = \\frac{3}{8}M{V}^{2}\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebb2",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{4}M{V}^{2}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{8}M{V}^{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{4}{3}M{V}^{2}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{2}{3}M{V}^{2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebb1",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A uniform disc of mass m and radius R is rolling without slipping up a rough inclined plane which makes an angle 30<sup>0</sup> with the horizontal. If the coefficient of static and kinetic friction are each equal  to  and the only force acting on the disc are gravitational and frictional force then find the direction and magnitude of the frictional force acting on it.</p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187551308.png\" alt=\"Description: 5\" /></p>",
              "solutionContent":"<p>Mg sin  – f<sub>r</sub> = ma</p><p>f<sub>r</sub> r = <span class='math-tex'>\\(\\frac{M\\phantom{\\rule{thickmathspace}{0ex}}{r}^{2}\\alpha }{2}=\\frac{ma}{2}r\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebb4",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{mg}{6}\\)</span> down the incline </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{mg}{6}\\)</span> up the incline </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{mg}{3}\\)</span> down the incline </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{mg}{3}\\)</span> up the incline </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebb3",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Consider elastic collision of a particle of mass m moving with a velocity u with another particle of the same mass at rest. After the collision the projectile and the struck particle move in direction making angle <sub>1</sub> and <sub>2</sub> respectively with the initial direction of motion. The sum of the angle <span class='math-tex'>\\({\\theta }_{1}+{\\theta }_{2}\\)</span> is</p>",
              "solutionContent":"<p>Property of perfectly elastic collision.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebb6",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>45<sup>0</sup> </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>90<sup>0</sup></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>135<sup>0</sup></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>180<sup>0</sup></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebb5",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A uniform rod of length ‘l’ is pivoted at point ‘A’. It is struck by a horizontal force which delivers an impulse ‘J’ at a distance ‘x’ from point ‘A’ as shown in figure, impulse delivered by pivot is zero if ‘x’ is equal to </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187552821.png\" alt=\"Description: 6\" /></p>",
              "solutionContent":"<p><span class='math-tex'>\\(\\frac{3Jx}{M{\\ell }^{2}}=\\frac{J\\left(x-\\ell /2\\right)}{M{\\ell }^{2}}×12\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebb8",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{l}{2}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{l}{3}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{2l}{3}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{3l}{4}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebb7",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A ball of mass m falls vertically from a height h and collides with a block of equal mass m moving horizontally with a velocity v on a surface. The coefficient of kinetic friction between the block and the surface is <span class='math-tex'>\\({\\mu }_{k}=0.2,\\)</span> while the coefficient of restitution (e) between the ball and the block is 0.5. There is no friction acting between the ball and the block. The velocity of the block just after the collision decreases by </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187553698.png\" alt=\"Description: 7\" /></p>",
              "solutionContent":"<p><span class='math-tex'>\\(\\int Ndt=\\frac{3MV}{2}\\)</span>  ;   <span class='math-tex'>\\(\\frac{\\int \\mu Ndt}{m}=\\mathrm{\\Delta }V\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebba",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(0.5\\sqrt{2gh}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>0</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(0.1\\sqrt{2gh}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(0.3\\sqrt{2gh}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebb9",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Two identical blocks each of mass 1 kg are join together with a compressed spring. The system at any time after releasing  is moving with unequal speed in the opposite direction as shown in figure </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187554467.png\" alt=\"Description: 8\" /></p><p>Choose the incorrect statement </p>",
              "solutionContent":"<p><span class='math-tex'>\\({v}_{cm}=\\frac{1×5+1×\\left(-3\\right)}{2}\\)</span></p><p><span class='math-tex'>\\(\\frac{2}{2}=1\\ \\ m/s\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebbc",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>it is not possible </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>whatever may be the speed of the blocks the centre of mass will remain stationary </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>the centre of mass of the system is moving with a velocity of 2ms<sup>-1</sup></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>the centre of mass of the system is moving with a velocity of 1 ms<sup>-1</sup></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebbb",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A container of large uniform cross sectional area A resting on a horizontal surface holds two immiscible non viscous and incompressible liquids of density d and 3d, each of height H/2. The lower density liquid is open to the atmosphere having pressure P<sub>0</sub>. A tiny hole of area a(a&lt;&lt;A) is punched to the vertical side of lower container at a height h (0&lt;h&lt;H/2) for which range is maximum.</p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187554150.png\" alt=\"Description: 9\" /></p>",
              "solutionContent":"<p>Bernoulli’s theorem for an orifice at depth ‘x’ in liquid ‘3d’.</p><p>P<sub>o</sub> + <span class='math-tex'>\\(\\left(dg\\frac{H}{2}+3dg.x\\right)\\)</span> = P<sub>o</sub> + <span class='math-tex'>\\(\\frac{1}{2}×3d×{v}^{z}\\)</span></p><p>…(1)</p><p><span class='math-tex'>\\(\\frac{H}{2}-x=\\frac{1}{2}g{t}^{2}\\)</span></p><p>…(2)</p><p>R = Vt</p><p>…(3)</p><p>Solve for R and apply maxima/minima.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebbe",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>h = H /3</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Range R = <span class='math-tex'>\\(\\frac{2H}{3}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Range R = <span class='math-tex'>\\(\\frac{3H}{2}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>Velocity of efflux <span class='math-tex'>\\(v=\\sqrt{\\frac{2}{3}gH}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebbd",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The momentum of a particle is given by <span class='math-tex'>\\(\\stackrel{\\to }{P}=\\left(4\\mathrm{sin}t\\stackrel{^}{i}-4\\mathrm{cos}t\\stackrel{^}{j}\\right)\\)</span> kg m/s. Select the correct alternatives(s)</p>",
              "solutionContent":"<p><span class='math-tex'>\\(\\stackrel{\\to }{P}.\\stackrel{\\to }{F} = 0\\)</span>.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebc0",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>momentum <span class='math-tex'>\\(\\stackrel{\\to }{P}\\)</span> of the particle is always parallel <span class='math-tex'>\\(\\stackrel{\\to }{F}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>momentum <span class='math-tex'>\\(\\stackrel{\\to }{P}\\)</span> of the particle is always perpendicular <span class='math-tex'>\\(\\stackrel{\\to }{F}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Magnitude of momentum <span class='math-tex'>\\(\\stackrel{\\to }{P}\\)</span> of the particle is always constant </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebbf",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A massless conical flask filled with a liquid is kept on a table in a vacuum. The force exerted by the liquid on the base of the flask is W<sub>1</sub>. The force exerted by the flask on the table is W<sub>2</sub>.</p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187555465.png\" alt=\"Description: 10\" /></p>",
              "solutionContent":"<p>Force due to liquid on base = gh Option1 [A is area of base]</p><p>= greater than total weight of liquid </p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebc2",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>W<sub>1</sub> = W<sub>2</sub> </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>W<sub>1</sub> &gt; W<sub>2</sub> </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>W<sub>1</sub> &lt; W<sub>2</sub> </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>the force exerted by the liquid on the walls of the flask is (W<sub>1</sub> – W<sub>2</sub>) </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebc1",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The angle through which the cylinder rotates before it leaves contact with the edge is</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(\\frac{3}{4}M{V}^{2}\\)</span> = mgr(1 &ndash; cos ) and Mg cos = <span class=\"math-tex\">\\(\\frac{M{V}^{2}}{r}\\)</span> cos = <span class=\"math-tex\">\\(\\frac{4}{7}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A rectangular rigid fixed block has a long horizontal edge. A solid homogeneous cylinder of radius R is placed horizontally at rest. Its length is parallel to the edge such that the axis of the cylinder and the edge of the block are in the same vertical plane. There is sufficient friction present at the edge so that a very small displacement causes the cylinder to roll off the edge without slipping</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ebc4",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\({\\mathrm{tan}}^{-1}\\frac{4}{7}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({\\mathrm{cos}}^{-1}\\frac{7}{4}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({\\mathrm{cos}}^{-1}\\frac{4}{7}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\({\\mathrm{tan}}^{-1}\\frac{7}{4}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebc3",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"c1e12817-44f1-4444-abe1-3ecc6421e39f"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The speed of the centre of mass of the cylinder before leaving contact with the edge is</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(V=\\sqrt{gR\\mathrm{cos}\\theta }=\\sqrt{\\frac{4}{7}gR}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A rectangular rigid fixed block has a long horizontal edge. A solid homogeneous cylinder of radius R is placed horizontally at rest. Its length is parallel to the edge such that the axis of the cylinder and the edge of the block are in the same vertical plane. There is sufficient friction present at the edge so that a very small displacement causes the cylinder to roll off the edge without slipping </p>",
              "_id":"5aab6dbbbc1d1f3013e0ebc6",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{\\frac{gR}{7}}\\)</span> </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{\\frac{4gR}{7}}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{\\frac{9gR}{7}}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{\\frac{3gR}{7}}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebc5",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"c1e12817-44f1-4444-abe1-3ecc6421e39f"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Velocity of point D of the rod immediately after collision will be</p>\n",
              "solutionContent":"<p>Linear momentum</p>\n\n<p>mv<sub>o</sub> = 2mv,</p>\n\n<p>v<sub>1</sub> = <span class=\"math-tex\">\\(\\frac{{v}_{o}}{2}\\)</span>.</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A thin, uniform rod of mass M and length L is at rest on a smooth horizontal surface. A particle of same mass M collides with the rod at one end perpendicular to its length with velocity V<sub>o</sub> and sticks to it. C is the middle point of the rod and D is a point at a distance of L/4 from end A.</p>\n\n<p><img alt=\"Description: 11\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187556827.png\" /></p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ebc8",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{{V}_{o}}{4}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{{V}_{o}}{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{3{V}_{o}}{4}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"v<sub>o</sub>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebc7",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"9d86caae-0499-4547-b54d-7c39295471d0"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The time taken by the rod turn through 90<sup>o</sup> after collisions</p>\n",
              "solutionContent":"<p>After collision</p>\n\n<p>About B, Angular momentum</p>\n\n<p>mv<sub>o</sub>L = <span class=\"math-tex\">\\(\\frac{3}{4}m{v}_{o}L+\\frac{5}{24}m{L}^{2}w\\)</span></p>\n\n<p>or,</p>\n\n<p>w = <span class=\"math-tex\">\\(\\frac{6{v}_{o}}{5L}\\)</span></p>\n\n<p>Now, t = <span class=\"math-tex\">\\(\\frac{\\theta }{w}=\\frac{\\left(\\pi /2\\right)}{w}=\\frac{5\\pi L}{12{v}_{o}}\\)</span></p>\n\n<p><img alt=\"Description: 12\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187558872.png\" /></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A thin, uniform rod of mass M and length L is at rest on a smooth horizontal surface.  A particle of same mass M collides with the rod at one end perpendicular to its length with velocity V<sub>o</sub> and sticks to it.  C is the middle point of the rod and D is a point at a distance of L/4 from end A. </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187556827.png\" alt=\"Description: 11\" /></p>",
              "_id":"5aab6dbbbc1d1f3013e0ebca",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{L}{{V}_{o}}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{\\pi L}{{V}_{o}}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{3\\pi L}{5{V}_{o}}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{5\\pi L}{12{V}_{o}}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebc9",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"9d86caae-0499-4547-b54d-7c39295471d0"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If a body of density &lsquo;<span class=\"math-tex\">\\(\\alpha\\)</span>&rsquo; is completely submerged inside a liquid of density <span class=\"math-tex\">\\(\\rho\\)</span>&nbsp;and floating. Then</p>\n",
              "solutionContent":"<p>Body may have cavity inside it so cannot find relation between and .</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>We know that liquids under equilibrium exerts a force perpendicular to any surface in contact with it, such as a container wall or a body immersed in the liquid. We also know that a system under equilibrium will have a net external force zero and for every force there is equal and opposite force acting on different bodies.</p>\n\n<p>Further, when we immerse a body fully or partially, inside a liquid, liquid exerts an upthrust on the body equal to the weight of the liquid displaced by the body.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ebcc",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p> =  </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p> &gt; </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p> &lt;  </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>cannot be predicted from the above information </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebcb",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"5a13978b-1bcb-4539-b77b-baa87479618e"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A beaker filled with water kept on a weighing balance weighs w<sub>1</sub>. Now an iron block of weight w<sub>2</sub> is suspended through a thread inside water. The weighing machine will now read &lsquo;w&rsquo; given</p>\n",
              "solutionContent":"<p>W = W<sub>1</sub> + Buoyant force on iron block.</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>We know that liquids under equilibrium exerts a force perpendicular to any surface in contact with it, such as a container wall or a body immersed in the liquid.  We also know that a system under equilibrium will have a net external force zero and for every force there is equal and opposite force acting on different bodies.</p><p>Further, when we immerse a body fully or partially, inside a liquid, liquid exerts an upthrust on the body equal to the weight of the liquid displaced by the body. </p>",
              "_id":"5aab6dbbbc1d1f3013e0ebce",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>w = w<sub>1</sub> + w<sub>2</sub> </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>w<sub>1</sub> &lt; w &lt; w<sub>1</sub> + w<sub>2</sub> </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>w &gt; w<sub>1</sub> + w<sub>2</sub> </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>w + w<sub>1</sub> </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebcd",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"5a13978b-1bcb-4539-b77b-baa87479618e"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ebd0",
              "questionHints":"",
              "solutionContent":"<p><span class=\"math-tex\">\\(F.x-\\mu {m}_{1}gx-\\frac{1}{2}k{x}^{2}=0\\)</span></p>\n\n<p>kx = <span class=\"math-tex\">\\(\\mu {m}_{2}g\\)</span> for just shifting m<sub>2</sub></p>\n\n<p><span class=\"math-tex\">\\(F.x-\\mu {m}_{1}gx-\\frac{1}{2}\\mu {m}_{2}gx=0\\)</span></p>\n\n<p><span class=\"math-tex\">\\(F=\\mu \\left({m}_{1}+\\frac{{m}_{2}}{2}\\right)g=0.4\\left(1+\\frac{2}{2}\\right)\\left(10\\right)\\)</span> = 8N</p>\n",
              "questionContent":"<p>Two blocks of masses m<sub>1</sub> = 1kg and m<sub>2</sub> = 2kg are connected by a non deformed light spring. They are lying on a rough horizontal surface. The coefficient of friction between the blocks and the surface is 0.4, what minimum constant force F (In N) has to be applied in horizontal direction to the block of mass m<sub>1</sub> in order to shift the other block? (g = 10 m/s<sup>2</sup>)</p>\n\n<p><img alt=\"Description: 13\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187558943.png\" /></p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebcf",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ebd2",
              "questionHints":"",
              "solutionContent":"<p>At common circular boundary three equal surface tension forces, hence  = 120<sup>0</sup></p><p>             Distance between centres = R </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187559381.png\" alt=\"Description: 14\" /></p>",
              "questionContent":"<p>Two soap bubbles of equal radii (R = 4cm) are stuck together with an intermediate film separating them. Surface tension of solution forming bubbles is 7 x 10<sup>-3</sup> N/ m. What will be the diameter (in cm) of the intermediate film. </p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebd1",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ebd4",
              "questionHints":"",
              "solutionContent":"<p>Conserving linear momentum along Y-axis, </p><p><span class='math-tex'>\\(\\alpha ×\\mathrm{sin}{60}^{0}=M×\\mathrm{cos}{30}^{0}\\)</span></p><p><span class='math-tex'>\\(\\alpha ×\\frac{\\sqrt{3}}{2}=M×\\frac{\\sqrt{3}}{2}\\)</span>  ;  M = 4</p>",
              "questionContent":"<p>An alpha particle collides with a stationary nucleus and continues on at an angle of 60<sup>0</sup> with respect the original direction of motion. The nucleus recoils at an angle of 30<sup>0</sup> with respect to this direction. Mass number of the nucleus is </p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebd3",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ebd6",
              "questionHints":"",
              "solutionContent":"<p>Consider energy losses in successive collisions, find , i.e. maximum deflection after each collision you will get <span class='math-tex'>\\(\\mathrm{cos}\\theta >\\frac{1}{2}\\)</span> will satisfy <span class='math-tex'>\\(\\theta <{60}^{0}\\)</span> this condition will be met after nth collision.</p><p>After 1<sup>st</sup> collision, K.E. = mghe<sup>2</sup> = 0.8 mgh</p><p>After 2<sup>nd</sup> collision, K.E. = 0.64 mgh</p><p>After 3<sup>rd</sup> collision,  K.E. = 0.512 mgh</p><p>After 4<sup>th</sup> collision, K.E. &lt; <span class='math-tex'>\\(\\frac{mgh}{2}\\)</span></p><p>i.e.,  &lt; 60°.  </p>",
              "questionContent":"<p>A simple pendulum is suspended from a peg on a vertical wall. The pendulum is pulled away from the wall to a horizontal position and released. The ball hits the wall, the coefficient of restitution is<span class='math-tex'>\\(\\frac{2}{\\sqrt{5}}\\)</span>. What is the minimum number of collisions after which the amplitude of oscillations becomes less than 60 degrees ?</p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187559900.png\" alt=\"Description: 15\" /></p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebd5",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ebd8",
              "questionHints":"",
              "solutionContent":"<p>Velocity of efflux = 16 × 0.25 = 4 m/s<sup>2</sup></p><p>Time of fall of the liquid = <span class='math-tex'>\\(\\sqrt{\\frac{2h}{g}}\\)</span> = 0.25 sec.</p><p>Thus, range on horizontal surface = velocity of efflux × time of fall = 2 m. </p>",
              "questionContent":"<p>A tube has two area of cross-sections as shown in figure . The diameter of the tube are 8mm and 2mm. Find range (in m) of water falling on horizontal surface, if piston is moving with a constant velocity of 0.25 m/s, h = 1.25m  (g = 10 m/s<sup>2</sup>) </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187560121.png\" alt=\"Description: 16\" /></p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebd7",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Calculate hydrolysis constant of 0.1 M NaNO<sub>2</sub> solution. ((K<sub>a</sub>HNO<sub>2</sub> = 4.5 &times; 10<sup>10</sup>)</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(NaN{O}_{2}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}N{a}^{+}+N{O}_{2}^{-}\\)</span></p>\n\n<p><span class=\"math-tex\">\\({K}_{h}=\\frac{{K}_{w}}{{K}_{a}}=\\frac{{10}^{-14}}{4.5×{10}^{-10}}=2.22×{10}^{-5}\\)</span></p>\n",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebf6",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>2.22 × 10<sup>5</sup></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>4.38 × 10<sup>5</sup></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>3.89 × 10<sup>4</sup></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>None</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebf5",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>For the reaction <span class=\"math-tex\">\\({N}_{2}\\left(g\\right)+3{H}_{2}\\left(g\\right)\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}2N{H}_{3}\\left(g\\right)\\)</span>under certain conditions of pressure and temperature of the reactants, the rate of formation of NH<sub>3</sub> is 0.001 kghr<sup>1</sup>. Calculate rate of reaction for N<sub>2</sub>.</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(\\frac{-d\\left({N}_{2}\\right)}{dt}=-\\frac{1}{3}\\frac{d\\left({H}_{2}\\right)}{dt}=\\frac{1}{2}\\frac{d\\left(N{H}_{3}\\right)}{dt}\\)</span></p>\n\n<p>So, <span class=\"math-tex\">\\(\\frac{-d\\left({N}_{2}\\right)}{dt}=\\frac{1}{2}\\frac{d\\left(N{H}_{3}\\right)}{dt}\\)</span></p>\n\n<p>= 8.12 &times; 10<sup>4</sup> kghr<sup>1</sup></p>\n",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebf8",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1.76 × 10<sup>4</sup> kghr<sup>1</sup></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>8.12 × 10<sup>4</sup> kghr<sup>1</sup></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>1.3 × 10<sup>3</sup> kghr<sup>1</sup></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>4.06 × 10<sup>4</sup> kghr<sup>1</sup></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebf7",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The rate constant for an isomerisation reaction. A  B is 4.5 × 10<sup>3</sup> min<sup>1</sup>. If the initial concentration of A is 1 M. Calculate rate of reaction after 1 hour.</p>",
              "solutionContent":"<p>A  B</p><p>For first order reaction:</p><p><span class='math-tex'>\\(K=\\frac{2.303}{t}\\mathrm{log}\\left(\\frac{a}{a-x}\\right)\\)</span></p><p>At t = 60</p><p><span class='math-tex'>\\(4.5×\\text{ }1{0}^{-\\text{ }3}\\text{ }=\\frac{2.303}{60}\\mathrm{log}\\left(\\frac{1}{a-x}\\right)\\)</span></p><p>So, a – x = 0.7634</p><p>Hence, rate after 60 minute </p><p>= K (a – x)</p><p>= 4.5 × 10<sup>3</sup> × 0.7634</p><p>= 3.4354 × 10<sup>3</sup></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebfa",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1.236 × 10<sup>3</sup></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>2.89 × 10<sup>2</sup></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>3.4354 × 10<sup>3</sup></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>8.369 × 10<sup>3</sup></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebf9",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>A mixture of NH<sub>4</sub>OH and NH<sub>4</sub>Cl solution contains 0.1 M NH<sub>4</sub>OH and 0.1 M NH<sub>4</sub>Cl. Calculate the pH of the solution (KNH<sub>4</sub>OH = 1.8 × 10<sup>5</sup>)</p>",
              "solutionContent":"<p>For basic buffer mixtures:</p><p><span class='math-tex'>\\(pOH=p{K}_{b}+\\mathrm{log}\\frac{\\left(Salt\\right)}{\\left(Base\\right)}\\)</span></p><p>Also, pH = 14 – pOH</p><p>Hence, pH = 9.25</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebfc",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>9.25</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>8.94</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>6.89</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>11.34</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebfb",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>At some temperature and under a pressure of 4 atm, PCl<sub>5</sub> is 10% dissociated. Calculate the pressure at which PCl<sub>5</sub> will be 20% dissociated temperature remaining same.</p>",
              "solutionContent":"<p> </p><p>t = 0 \t  1        0     0</p><p>t = teq (1 - )               </p><p>So, <span class='math-tex'>\\({K}_{p}=\\frac{nPC{l}_{3}×nC{l}_{2}}{nPC{l}_{5}}×{\\left(\\frac{P}{\\sum n}\\right)}^{\\mathrm{\\Delta }n}\\)</span></p><p>So, K<sub>p</sub> = 0.0404 atm</p><p>Similarly, when  = 0.2</p><p>P = 0.969 atm</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ebfe",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1.345 atm</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>2.386 atm</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>0.969 atm</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>0.483 atm</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebfd",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>For the reaction ,</p><p>If K<sub>p</sub> = K<sub>c</sub>(RT)<sup>x</sup> where the symbols have usual meaning then the value of x is: (assume ideality)</p>",
              "solutionContent":"<p>Using K<sub>p</sub> = K<sub>c</sub>(RT)<sup>ng</sup></p><p>Here, x = ng = no. of gaseous moles of product  no. of gaseous moles of reactant</p><p><span class='math-tex'>\\( = 1 - \\left( {1 + \\frac{1}{2}} \\right) = 1 - {3 \\over 2} = -\\frac{1}{2}\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec00",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(-\\frac{1}{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{1}{2}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>1</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ebff",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>AlCl<sub>3</sub> is more volatile than NaCl, this is because:</p>",
              "solutionContent":"<p>AlCl<sub>3</sub> is covalent whereas NaCl is ionic.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec02",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>The AlCl<sub>3</sub> molecules are held by weak Vander Waal’s forces whereas the NaCl species </p><p>are held by strong ionic forces in a giant lattice.</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>AlCl<sub>3</sub> unlike NaCl is dimerized.</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Sodium is more metallic than aluminium.</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>NaCl unlike AlCl<sub>3</sub> is a natural product.</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec01",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Carbon disulphide react with chlorine to give:</p>",
              "solutionContent":"<p><span class='math-tex'>\\(C{S}_{2}+3C{l}_{2}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}CC{l}_{4}+{S}_{2}C{l}_{2}\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec04",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Carbon tetrachloride and sulphur monochloride</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Carbon tetrachloride and sulphur tetrachloride</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Carbon and sulphur dichloride</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>Carbon and sulphur monochloride</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec03",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>In which of the following cases, reaction is spontaneous at all temperatures:</p>",
              "solutionContent":"<p>The reactions are:</p><p>(i) H &lt; 0, S &gt; 0 </p><p>(ii) H &lt; 0, S = 0</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec06",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>H &gt; 0, S &gt; 0</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>H &lt; 0, S &gt; 0</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>H &lt; 0, S &lt; 0</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>H &lt; 0, S = 0</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec05",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The enthalpy change for a reaction depends upon:</p>",
              "solutionContent":"<p>The enthalpy change for a reaction depends upon:</p><p>(i) physical state of reactants and products</p><p>(ii) use of different reactants for the same product</p><p>(iii) internal energies of reactants and products</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec08",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>physical state of reactants and products</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>use of different reactants for the same product</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>nature of intermediate reaction steps</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>internal energies of reactants and products</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec07",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Which among the following will react with HCl or which is basic salt?</p>",
              "solutionContent":"<p>NaH<sub>2</sub>PO<sub>2</sub>, <sub> </sub>Na<sub>2</sub>HPO<sub>3</sub> &amp; NaHCO<sub>3</sub> reacts with HCl.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec0a",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>NaH<sub>2</sub>PO<sub>2</sub></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Na<sub>2</sub>HPO<sub>3</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Na<sub>2</sub>HPO<sub>2</sub></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>NaHCO<sub>3</sub></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec09",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Which of the following will function as buffer:</p>",
              "solutionContent":"<p>Fact based</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec0c",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>CH<sub>3</sub>COOH + NaOH, if [CH<sub>3</sub>COOH] &gt; [NaOH]</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>HCl + CH<sub>3</sub>COONa, if [CH<sub>3</sub>COONa] &gt; [HCl]</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>NH<sub>4</sub>CN</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>HCN + NaCN</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec0b",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The compound &lsquo;A&rsquo; is:</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(C{a}_{2}{B}_{6}{O}_{11}+2N{a}_{2}C{O}_{3}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}2CaC{O}_{3}+N{a}_{2}{B}_{4}{O}_{7}+2NaB{O}_{2}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(N{a}_{2}{B}_{4}{O}_{7}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{\\mathrm{\\Delta }}2NaB{O}_{2}+{B}_{2}{O}_{3}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(CoO+{B}_{2}{O}_{3}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}Co{\\left(B{O}_{2}\\right)}_{2}{\\int }_{\\left(Blue\\right)}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A white precipitate (X) is formed when a mineral (A) is boiled with Na<sub>2</sub>CO<sub>3</sub> solution. The precipitate is filtered and filtrate contains two compounds (Y) and (Z). The compound (Y) is removed by crystallization and when CO<sub>2</sub> is passed through the filtrate obtained after crystallization, then (Z) changed to (Y). When compound (Y) is heated it gives two compounds (Z) and (T). The compound (T) on heating with cobalt oxide produces blue coloured substance (S).</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec0e",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Na<sub>2</sub>B<sub>6</sub>O<sub>11</sub></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>CaCO<sub>3</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Ca<sub>2</sub>B<sub>6</sub>O<sub>11</sub>5H<sub>2</sub>O</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>B<sub>2</sub>O<sub>3</sub></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec0d",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"9f575e47-816e-45fe-9351-98c006a5165f"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The compound (Y) in the filtrate when (A) is boiled with Na<sub>2</sub>CO<sub>3</sub> is:</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(C{a}_{2}{B}_{6}{O}_{11}+2N{a}_{2}C{O}_{3}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}2CaC{O}_{3}+N{a}_{2}{B}_{4}{O}_{7}+2NaB{O}_{2}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(N{a}_{2}{B}_{4}{O}_{7}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{\\mathrm{\\Delta }}2NaB{O}_{2}+{B}_{2}{O}_{3}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(CoO+{B}_{2}{O}_{3}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}Co{\\left(B{O}_{2}\\right)}_{2}{\\int }_{\\left(Blue\\right)}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>A white precipitate (X) is formed when a mineral (A) is boiled with Na<sub>2</sub>CO<sub>3</sub> solution. The precipitate is filtered and filtrate contains two compounds (Y) and (Z). The compound (Y) is removed by crystallization and when CO<sub>2</sub> is passed through the filtrate obtained after crystallization, then (Z) changed to (Y). When compound (Y) is heated it gives two compounds (Z) and (T). The compound (T) on heating with cobalt oxide produces blue coloured substance (S).</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec10",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Na<sub>2</sub>BO<sub>2</sub></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Na<sub>2</sub>B<sub>4</sub>O<sub>7</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Na<sub>3</sub>BO<sub>3</sub></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>CaO</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec0f",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"9f575e47-816e-45fe-9351-98c006a5165f"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>X is:</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(CaO+3C\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{\\mathrm{\\Delta }}Ca{C}_{2}+CO\\)</span></p>\n\n<p><span class=\"math-tex\">\\(Ca{C}_{2}+{N}_{2}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{{1100}^{\\circ }C}CaNCN+C\\)</span></p>\n\n<p><span class=\"math-tex\">\\(CaNCN+3{H}_{2}O\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}CaC{O}_{3}+2N{H}_{3}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Carbides are three types: ionic, covalent and interstitial. CaC<sub>2</sub> is one of the commercially important ionic carbide.</p>\n\n<p><span class=\"math-tex\">\\(\\begin{array}{l}X+C\\;\\xrightarrow\\triangle CaC_2+CO\\\\CaC_2+Y\\;\\xrightarrow{1100^\\circ C}Z+C\\\\Z+H_2O\\;\\xrightarrow{}NH_3CaCO_3\\\\\\end{array}\\)</span></p>\n\n<p>&nbsp;</p>\n\n<p>Z is an important nitrogenous fertilizer.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec12",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>CaO</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>CaCO<sub>3</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Ca(OH)<sub>2</sub></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>CaCl<sub>2</sub></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec11",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"7d6805ad-d8d8-49a5-ad37-a89139fa0934"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Y and Z respectively are:</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(CaO+3C\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{\\mathrm{\\Delta }}Ca{C}_{2}+CO\\)</span></p>\n\n<p><span class=\"math-tex\">\\(Ca{C}_{2}+{N}_{2}\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{{1100}^{\\circ }C}CaNCN+C\\)</span></p>\n\n<p><span class=\"math-tex\">\\(CaNCN+3{H}_{2}O\\phantom{\\rule{0pt}{0ex}}⟶{\\int }_{}^{}CaC{O}_{3}+2N{H}_{3}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Carbides are three types: ionic, covalent and interstitial. CaC<sub>2</sub> is one of the commercially important ionic carbide.</p>\n\n<p><span class=\"math-tex\">\\(\\begin{array}{l}X+C\\;\\xrightarrow\\triangle CaC_2+CO\\\\CaC_2+Y\\;\\xrightarrow{1100^\\circ C}Z+C\\\\Z+H_2O\\;\\xrightarrow{}NH_3CaCO_3\\\\\\end{array}\\)</span></p>\n\n<p>Z is an important nitrogenous fertilizer.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec14",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>N<sub>2</sub>O and Ca(CN)<sub>2</sub></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>(CN)<sub>2</sub> and CaCN<sub>2</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>N<sub>2</sub> and Ca(CN)<sub>2</sub></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>N<sub>2</sub> and CaCN<sub>2</sub></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec13",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"7d6805ad-d8d8-49a5-ad37-a89139fa0934"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>What would be the effect of the rapid forced breathing.</p>\n",
              "solutionContent":"<p>Rapid breathing would lower the concentration of CO<sub>2</sub>.</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>The pH of blood is 7.4. pH of blood should be maintained constant. Otherwise it may cause illness like increase of blood pressure or decrease of blood pressure. The buffer in the blood is formed by CO<sub>2</sub> and <span class=\"math-tex\">\\(HC{O}_{3}^{-}\\)</span>ion. The reaction available in the blood is a reversible reaction.</p>\n\n<p>The reaction is:</p>\n\n<p><span class=\"math-tex\">\\(\\begin{array}{l}CO_2+H_2O\\;\\rightleftharpoons\\;H^++HCO_3^-\\;\\;\\;\\;\\;\\;\\;\\;\\;\\;K=4.5\\times10^{-7}\\\\\\end{array}\\)</span></p>\n\n<p>[At constant body temperature 37&deg;C]</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec16",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Increase the concentration of CO<sub>2</sub></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Lower the concentration of CO<sub>2</sub></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Equilibrium constant increases</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>Equilibrium constant decreases</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec15",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"356695c5-3e5f-4c49-9ee7-9485648e4dfd"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Calculate the ratio of the conjugate base to acid necessary to maintain blood at its proper pH:</p>\n",
              "solutionContent":"<p>Given pH = 7.4 So, [H<sup>+</sup>] = 4.0 &times; 10<sup>8</sup>M</p>\n\n<p><span class=\"math-tex\">\\({{\\left( {HC{O}_{3}^{-}} \\right)} \\over {C{O_2}}} = {K \\over {\\left( {{H^ + }} \\right)}} = {{4.5 \\times {{10}^{ - 7}}} \\over {4 \\times {{10}^{ - 8}}}} = 11.25\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>The pH of blood is 7.4. pH of blood should be maintained constant. Otherwise it may cause illness like increase of blood pressure or decrease of blood pressure. The buffer in the blood is formed by CO<sub>2</sub> and <span class=\"math-tex\">\\(HC{O}_{3}^{-}\\)</span>ion. The reaction available in the blood is a reversible reaction.</p>\n\n<p>The reaction is:</p>\n\n<p><span class=\"math-tex\">\\(\\begin{array}{l}CO_2+H_2O\\;\\rightleftharpoons\\;H^++HCO_3^-\\;\\;\\;\\;\\;\\;\\;\\;\\;\\;K=4.5\\times10^{-7}\\\\\\end{array}\\)</span></p>\n\n<p>[At constant body temperature 37&deg;C]</p>\n\n<p>&nbsp;</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec18",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>4.5</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>3.75</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>11.25</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>14</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec17",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"356695c5-3e5f-4c49-9ee7-9485648e4dfd"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec1a",
              "questionHints":"",
              "solutionContent":"<p><span class='math-tex'>\\(pH=7+\\frac{1}{2}\\left(pKa+\\mathrm{log}C\\right)\\)</span></p><p><span class='math-tex'>\\(=7+\\frac{1}{2}\\left(4+\\mathrm{log}\\left(0.01\\right)\\right)\\)</span> = 7 + 1 = 8</p>",
              "questionContent":"<p>The dissociation constant of a substituted benzoic acid at 25°C is 1.0 × 10<sup>4</sup>. The pH of 0.01M solution of its sodium salt is:</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec19",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec1c",
              "questionHints":"",
              "solutionContent":"<p>t = 0\t  2\t    0     0</p><p>t = t<sub>eq\t</sub>2(1 – )        </p><p>Total moles = 2(1 – ) +  +  = 2</p>",
              "questionContent":"<p>If  is the fraction of HI dissociated at equilibrium in the, following reaction</p><p>then starting with 2 mole of HI the total number of moles of reactants and products at equilibrium is:</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec1b",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec1e",
              "questionHints":"",
              "solutionContent":"<p>Each boric acid contains 3 OH groups which can participate in two hydrogen bonds.</p>",
              "questionContent":"<p>The number of hydrogen bonds that can be formed by each boric acid molecule is:</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec1d",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec20",
              "questionHints":"",
              "solutionContent":"<p>For </p><p>G° = H°  TS°</p><p>= 90 – 400 × (0.200)</p><p>= 90 + 80 = 10 KJ</p><p>G° = 5 KJ</p>",
              "questionContent":"<p>For a gaseous reaction </p><p>H° = 90 KJ, S° = 200 JK<sup>1</sup> at 400 K</p><p>For the reaction at 400 K G° in KJ is:</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec1f",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec22",
              "questionHints":"",
              "solutionContent":"<p><span class='math-tex'>\\(\\because {q}_{p}-{q}_{v}=\\mathrm{\\Delta }ngRT\\)</span></p><p><span class='math-tex'>\\(\\therefore \\mathrm{\\Delta }hg=\\frac{{q}_{p}-{q}_{v}}{RT}=\\frac{5000}{8.314×300}=2\\)</span></p>",
              "questionContent":"<p>If for a particular reaction, the difference in the heat evolved when the reaction is carried out at constant pressure and that at constant volume at 27°C is nearly 5 KJ mol<sup>1</sup> then the difference in the number of moles of gaseous reactants and product is:</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec21",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Consider a square OABC in the argand plane, where &rsquo;O&rsquo; is origin and A A(z<sub>0</sub>). Then the equation of the circle that can be inscribed in this square is; ( vertices of square are given in anticlockwise order)</p>\n",
              "solutionContent":"<p>Center is <span class=\"math-tex\">\\(\\frac{{z}_{0}+{z}_{0}i}{2}\\)</span> and Diameter is <span class=\"math-tex\">\\(|{z}_{0}|\\)</span></p>\n",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec40",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>| z – z<sub>0</sub>(1+ i)| =|z<sub>0</sub>|</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>2<span class='math-tex'>\\(|z-\\frac{{z}_{0}\\left(1+i\\right)}{2}|=|{z}_{0}|\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(|z-\\frac{{z}_{0}\\left(1+i\\right)}{2}|=|{z}_{0}|\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none  of  these </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec3f",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If |z &ndash; 4 + 3i| <span class=\"math-tex\">\\(\\leq\\)</span>1 and <span class=\"math-tex\">\\(\\alpha\\)</span>&nbsp;and <span class=\"math-tex\">\\(\\beta\\)</span>&nbsp;be the least and greatest values of |z| and k be the least value of <span class=\"math-tex\">\\(\\frac{{x}^{4}+{x}^{2}+4}{x}\\)</span> on the interval (0, <span class=\"math-tex\">\\(\\infty\\)</span>), then k is equal to</p>\n",
              "solutionContent":"<p>Given 1 <span class=\"math-tex\">\\(\\geq\\)</span>|z &ndash; (4 &ndash;3i) | <span class=\"math-tex\">\\(\\geq\\left\\{\\begin{array}{l}\\left|z\\right|-\\left|4-3i\\right|\\\\\\left|4-3i\\right|-\\left|z\\right|\\end{array}\\right.\\)</span></p>\n\n<p><span class=\"math-tex\">\\(\\Rightarrow\\)</span>|z| <span class=\"math-tex\">\\(\\leq\\)</span>&nbsp;6 and |z| <span class=\"math-tex\">\\(\\geq\\)</span>&nbsp;4</p>\n\n<p><span class=\"math-tex\">\\(\\Rightarrow\\)</span>4 <span class=\"math-tex\">\\(\\leq\\)</span>|z| <span class=\"math-tex\">\\(\\leq\\)</span>&nbsp;6 <span class=\"math-tex\">\\(\\Rightarrow\\)</span>&nbsp;<span class=\"math-tex\">\\(\\alpha\\)</span>= 4, <span class=\"math-tex\">\\(\\beta\\)</span>&nbsp;= 6</p>\n\n<p>Let y = <span class=\"math-tex\">\\(\\frac{{x}^{4}+{x}^{2}+4}{x} = {x^3} + x + {4 \\over x} = {x^3} + x + \\frac{1}{x} + {1 \\over x} + {1 \\over x} + {1 \\over x}\\)</span></p>\n\n<p>Since x <span class=\"math-tex\">\\(\\in\\)</span>( 0, <span class=\"math-tex\">\\(\\infty\\)</span>), therefore <span class=\"math-tex\">\\(x^3,\\;x,\\frac1x,\\frac1x,\\;\\frac1x,\\frac1x\\)</span>&nbsp;are positive .</p>\n\n<p>Sum will be least when x<sup>3</sup> = x = <span class=\"math-tex\">\\({1 \\over x}\\)</span> <span class=\"math-tex\">\\(\\Rightarrow\\)</span>&nbsp;x = 1.</p>\n\n<p><span class=\"math-tex\">\\(\\Rightarrow\\)</span>k = 6</p>\n\n<p>Hence k =&nbsp;<span class=\"math-tex\">\\(\\beta\\)</span></p>\n",
              "questionHints":"",
              "_id":"5b68440e912c4c0c73ce8c69",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "serialNo":1,
                  "value":"<p><span class=\"math-tex\">\\(\\alpha\\)</span></p>\n",
                  "id":"Option1"
                },
                {
                  "serialNo":2,
                  "value":"<p><span class=\"math-tex\">\\(\\beta\\)</span></p>\n",
                  "id":"Option2"
                },
                {
                  "serialNo":3,
                  "value":"<p><span class=\"math-tex\">\\(\\alpha +\\beta\\)</span></p>\n",
                  "id":"Option3"
                },
                {
                  "serialNo":4,
                  "value":"<p>none of these</p>\n",
                  "id":"Option4"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec41",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The term independent of x in the expansion of <span class='math-tex'>\\({\\left(x+\\frac{1}{x}+2\\right)}^{m}\\)</span> is <span class='math-tex'>\\(\\left(m\\in N,\\ \\ m\\ge 10\\right)\\)</span> </p>",
              "solutionContent":"<p>Given binomial can be written as <span class='math-tex'>\\({\\left(\\sqrt{x}+\\frac{1}{\\sqrt{x}}\\right)}^{2m}\\)</span>. The term independent of x in this </p><p>expansion is <span class='math-tex'>\\(\\frac{\\left(2m!\\right)}{{\\left(m!\\right)}^{2}}\\)</span>.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec44",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{\\left(2m!\\right)}{{\\left(m!\\right)}^{2}}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{\\left(m!\\right)}{{\\left(2m!\\right)}^{2}}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{\\left(2m!\\right)}{\\left(m!\\right)}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{{2}^{m}\\left(2m!\\right)}{\\left(m!\\right)}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec43",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The complete real solution set of the inequality log<sub>0.09</sub> (x<sup>2</sup> + 2x)  log<sub>0.3</sub><span class='math-tex'>\\(\\sqrt{x+2}\\)</span> is </p>",
              "solutionContent":"<p>log<sub>0.09</sub> (x<sup>2</sup> + 2x) is defined when x<sup>2</sup> + 2x &gt; 0 </p><p> (x) (x + 2) &gt; 0  x  ( ,  2)  (0, )  … (1) </p><p>and log<sub>0.3</sub><span class='math-tex'>\\(\\sqrt{x+2}\\)</span> is defined when x + 2 &gt; 0 </p><p> x  ( 2, )  … (2) </p><p>also log<sub>0.09</sub> (x<sup>2</sup> + 2x)  log<sub>0.3</sub><span class='math-tex'>\\(\\sqrt{x+2}\\)</span></p><p> <span class='math-tex'>\\(\\frac{1}{2}\\)</span> log<sub>0.3</sub> (x<sup>2</sup> + 2x)  log<sub>0.3</sub><span class='math-tex'>\\(\\sqrt{x+2}\\)</span></p><p> log<sub>0.3</sub> (x<sup>2</sup> + 2x)  log<sub>0.3</sub> (x + 2) </p><p> x<sup>2</sup> + 2x  x + 2</p><p> x<sup>2</sup> + x  2  0 </p><p> x  [ 2, 1]  … (3) </p><p>From (1), (2), (3) solution is x  (0, 1].</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec46",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>[ 2, 1]</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>( 2, 0) </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>(0, 1]</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec45",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If <span class='math-tex'>\\({x}^{3}+3{x}^{2}+3x+2=0\\)</span> and<span class='math-tex'>\\(a{x}^{2}+bx+c=0\\)</span>, a, b, c  R, <span class='math-tex'>\\(a\\ne 0\\)</span>, have two common roots then the roots of the equation ax<sup>2</sup> + (a + b)x + c = 0 are</p>",
              "solutionContent":"<p><span class='math-tex'>\\({x}^{3}+3{x}^{2}+3x+2=\\ \\left(x+2\\right)\\left({x}^{2}+x+1\\right)\\)</span></p><p>the common roots are , <sup>2</sup></p><p> a = b = c (= 1 let)</p><p>Now, ax<sup>2</sup> + (a + b)x + c = 0</p><p> x<sup>2</sup> + 2x + 1 = 0</p><p>(x + 1)<sup>2</sup> = 0</p><p> roots are real and equal.</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec48",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>imaginary</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>real and unequal</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>real and equal</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>None of these</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec47",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If <span class='math-tex'>\\(a{z}^{2}+bz+1=0\\)</span> , where a, b,  C and |a| =<span class='math-tex'>\\(\\frac{1}{2}\\)</span>, has a root  such that || = 1 then <span class='math-tex'>\\(|a\\overline{b}-b|\\)</span> is equal to </p>",
              "solutionContent":"<p>Given a<sup>2</sup> + b + 1 = 0       …….(1)</p><p>Also <span class='math-tex'>\\(\\overline{a}\\ {\\overline{\\alpha }}^{2}+\\overline{b}\\ \\overline{\\alpha }+1=0\\)</span>       <span class='math-tex'>\\(\\frac{\\overline{a}}{{\\alpha }^{2}}+\\frac{\\overline{b}}{\\alpha }+1=0\\)</span>   (as || = 1)</p><p>       <span class='math-tex'>\\({\\alpha }^{2}+\\overline{b}\\alpha +\\overline{a}=0\\)</span>           ……..(2)    </p><p>From (1) and (2) <span class='math-tex'>\\(\\frac{{\\alpha }^{2}}{\\overline{a}\\ b-\\overline{b}}=\\frac{\\alpha }{1\\ -|a{|}^{2}}=\\frac{1}{a\\overline{b}-b}\\)</span></p><p><span class='math-tex'>\\(⇒\\ \\ \\ \\ \\ \\frac{\\overline{a}b-\\overline{b}}{1-|a{|}^{2}}=\\frac{1-|a{|}^{2}}{a\\overline{b}-b}\\)</span>    <span class='math-tex'>\\( \\Rightarrow \\,\\,\\,\\,|a\\overline{b}-b|\\,\\, = 1\\, - |a{|^2} = \\frac{3}{4}\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec4a",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{1}{4}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{1}{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{4}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{5}{4}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec49",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Value of <span class='math-tex'>\\(\\sum {\\int }_{r=0}^{n}r\\cdot \\ {\\left({}^{n}{C}_{r}\\right)}^{2}\\)</span> is equal to <span class='math-tex'>\\(\\left(n\\ge 2017,\\ \\ \\ n\\in N\\right)\\)</span> </p>",
              "solutionContent":"<p><span class='math-tex'>\\(S=\\sum r{\\left({C}_{r}\\right)}^{2}\\)</span> </p><p><span class='math-tex'>\\(S=\\sum \\left(n-r\\right){\\left({C}_{n-r}\\right)}^{2}=\\sum \\left(n-r\\right){\\left({C}_{r}\\right)}^{2}\\)</span> </p><p><span class='math-tex'>\\(=n\\sum {\\left(Cr\\right)}^{2}\\)</span>-<span class='math-tex'>\\(\\sum r{\\left(Cr\\right)}^{2}\\)</span></p><p><span class='math-tex'>\\(S=n\\ \\ .\\ {\\ }^{2n}{C}_{n}-S\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec4c",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(n\\cdot {\\ }^{2n}{C}_{n}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({{n\\cdot {\\ }^{2n}{C}_{n}} \\over 2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({n}^{2}\\cdot {\\ }^{2n}{C}_{n}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\({{{n}^{2}\\cdot {\\ }^{2n}{C}_{n}} \\over 2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec4b",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If , ,  are the roots of the equation <span class='math-tex'>\\({x}^{3}+{P}_{0}{x}^{2}+{P}_{1}x+{P}_{2}=0\\)</span> , then (1 - <sup>2</sup>) (1 - <sup>2</sup>) (1 - <sup>2</sup>) is equal to </p>",
              "solutionContent":"<p>Make the equation whose roots are <span class='math-tex'>\\(\\left(1-{\\alpha }^{2}\\right),\\ \\ \\left(1-{\\beta }^{2}\\right),\\left(1-{\\gamma }^{2}\\right)\\)</span> and then apply formula for </p><p>product of roots. </p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec4e",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\({\\left(1+{P}_{1}\\right)}^{2}-{\\left({P}_{0}+{P}_{2}\\right)}^{2}\\)</span> </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({\\left(1+{P}_{1}\\right)}^{2}+{\\left({P}_{0}+{P}_{2}\\right)}^{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({\\left(1-{P}_{1}\\right)}^{2}-{\\left({P}_{0}-{P}_{2}\\right)}^{2}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec4d",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>For <span class='math-tex'>\\(a>0,\\)</span> the roots of the equation <span class='math-tex'>\\({\\mathrm{log}}_{ax}a+{\\mathrm{log}}_{x}\\ {a}^{2}+{\\mathrm{log}}_{{a}^{2}x}{a}^{3}=0,\\)</span> are given by </p>",
              "solutionContent":"<p><span class='math-tex'>\\(\\frac{\\mathrm{log}a}{\\mathrm{log}a+\\mathrm{log}x}+\\frac{2\\mathrm{log}a}{\\mathrm{log}x}+\\frac{3\\mathrm{log}a}{2\\mathrm{log}a+\\mathrm{log}x}=0\\)</span></p><p><span class='math-tex'>\\(\\frac{1}{1+\\mathrm{log}x}+\\frac{2}{{\\mathrm{log}}_{a}x}+\\frac{3}{2+{\\mathrm{log}}_{a}x}=0\\)</span></p><p><span class='math-tex'>\\(\\left({\\mathrm{log}}_{a}x+1\\right)\\left(3{\\mathrm{log}}_{a}x+4\\right)=0\\)</span></p><p><span class='math-tex'>\\(x = {a}^{-1},x = {a^{ - 4/3}}\\)</span></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec50",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\({a}^{-\\frac{4}{3}}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({a}^{-\\frac{3}{4}}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({a}^{-\\frac{1}{2}}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\({a}^{-1}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec4f",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Let P(x) and Q(x) be two polynomials. If f(x) = P(x<sup>4</sup>) + xQ(x<sup>4</sup>) is divisible by x<sup>2</sup> +1, then </p>",
              "solutionContent":"<p>f(x) = P(x<sup>4</sup>) + xQ(x<sup>4</sup>) is divisible by (x + i) (x – i)</p><p> f(i) = P(1) + iQ(1) = 0</p><p>f(-i) = P(1) – iQ(1) = 0</p><p> P(1) = Q(1) = f(1) = 0</p><p> P(x), Q(x) and f(x) are divisible by (x - 1).</p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec52",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>P(x) is divisible by (x-1)</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Q(x) is divisible by (x-1)</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>f(x) is divisible by (x-1)</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec51",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>One  vertex, of the triangle of maximum area that can be inscribed in the curve |z – 2 i| =2,</p><p>is  2 +2i ,  remaining vertices  is/are</p>",
              "solutionContent":"<p>Clearly  the  inscribed  triangle  is  equilateral. </p><p> <span class='math-tex'>\\(\\frac{{z}_{2}-{z}_{0}}{{z}_{1}-{z}_{0}}={e}^{i\\frac{2\\pi }{3}},\\)</span> <span class='math-tex'>\\(\\frac{{z}_{3}-{z}_{0}}{{z}_{1}-{z}_{0}}={e}^{-i\\frac{2\\pi }{3}}\\)</span></p><p> z<sub>2</sub> = -1 +i(2 +<span class='math-tex'>\\(\\sqrt{3}\\)</span>) and z<sub>3</sub> = -1 + i(2 -<span class='math-tex'>\\(\\sqrt{3}\\)</span>)</p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187560798.png\" /></p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec54",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>-1+ i( 2 +<span class='math-tex'>\\(\\sqrt{3}\\)</span>)</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>–1– i( 2 +<span class='math-tex'>\\(\\sqrt{3}\\)</span>)</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>-1+ i( 2 –<span class='math-tex'>\\(\\sqrt{3}\\)</span>)</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>–1– i( 2 –<span class='math-tex'>\\(\\sqrt{3}\\)</span>)</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec53",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Let there be two parabolas with the same axis, focus of each being exterior to the other and the latera recta being 4a and 4b. The locus of the middle points of the intercepts between the parabolas made on the lines parallel to the common axis is a </p>",
              "solutionContent":"<p>Let Parabolas be <span class='math-tex'>\\({y}^{2}=4ax\\)</span> and <span class='math-tex'>\\({y}^{2}=-4bx\\)</span>. Let the line parallel to axis be y = 2 at which intersect <span class='math-tex'>\\({y}^{2}=4ax\\)</span> at A and <span class='math-tex'>\\({y}^{2}=-4bx\\)</span> at B.</p><p>Hence <span class='math-tex'>\\(A\\left(a{t}^{2},\\ 0\\right)\\)</span> and <span class='math-tex'>\\(B\\left(-\\frac{{a}^{2}}{b}{t}^{2},\\ \\ 0\\right)\\)</span>. Let mid point of A and B be P (h, k) </p><p>Hence <span class='math-tex'>\\(2h=\\left(\\frac{a-{a}^{2}}{b}\\right){t}^{2}\\)</span>    ….(i) </p><p>K = 2at   …(ii) </p><p>On eliminating t from equation (i) and (ii) </p><p>We get, <span class='math-tex'>\\(\\left(a-\\frac{{a}^{2}}{b}\\right){y}^{2}=8{a}^{2}x\\)</span></p><p>which may be a parabola <span class='math-tex'>\\(\\left( {a\\ne b} \\right)\\)</span> and S.L. (a = b) </p>",
              "questionHints":"",
              "_id":"5aab6dbbbc1d1f3013e0ec56",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>straight line if a = b </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>parabola if <span class='math-tex'>\\(a\\ne b\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>parabola for all a,b </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec55",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If the parabolas C and D intersect at a point A on the line<span class=\"math-tex\">\\({L}_{1}\\)</span>, then equation of the tangent line L at A to the parabola D is</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(C:y={x}^{2}-3,\\)</span></p>\n\n<p><span class=\"math-tex\">\\(D:y=k{x}^{2}\\)</span></p>\n\n<p>Point A lies on <span class=\"math-tex\">\\(y=k{x}^{2}⇒\\ \\ k=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right)\\)</span></p>\n\n<p>Hence parabola</p>\n\n<p><span class=\"math-tex\">\\(D:y=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right){x}^{2}\\)</span></p>\n\n<p>Equation of tangent at point A of D</p>\n\n<p><span class=\"math-tex\">\\(\\left(\\frac{y+{a}^{2}-3}{2}\\right)=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right).\\ xa\\)</span></p>\n\n<p><span class=\"math-tex\">\\( \\Rightarrow 2\\left({a}^{2}-3\\right)x-ay-{a}^{3}+3a=0\\)</span></p>\n\n<p>Now this tangent passes through point B (1, &ndash;2)</p>\n\n<p><span class=\"math-tex\">\\(⇒a=-2,\\ 1,\\ 3\\)</span></p>\n\n<p>a = 1 will be rejected as A and B coincide.</p>\n\n<p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question152118418756132.png\" /></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Consider the curves <span class=\"math-tex\">\\(C:y={x}^{2}-3\\)</span> <span class=\"math-tex\">\\(D:y=k{x}^{2}\\ \\ \\ \\ \\ \\ {L}_{1}:x=a\\)</span> <span class=\"math-tex\">\\({L}_{2}:x=1\\ \\ \\ \\ \\left(a\\ne 0\\right)\\)</span></p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec58",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(2\\left({a}^{2}-3\\right)x-ay+{a}^{3}-3a=0\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(2\\left({a}^{2}-3\\right)x-ay-{a}^{3}+3a=0\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\left({a}^{3}-3\\right)x-2ay-2{a}^{3}+6a=0\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of those </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec57",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"92f7c455-52d5-49ef-9132-0a539a68cf20"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>It the line L meets the parabola C at a point B on the line<span class=\"math-tex\">\\({L}_{2}\\)</span>, other than A, then a can be</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(C:y={x}^{2}-3,\\)</span></p>\n\n<p><span class=\"math-tex\">\\(D:y=k{x}^{2}\\)</span></p>\n\n<p>Point A lies on <span class=\"math-tex\">\\(y=k{x}^{2}⇒\\ \\ k=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right)\\)</span></p>\n\n<p>Hence parabola</p>\n\n<p><span class=\"math-tex\">\\(D:y=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right){x}^{2}\\)</span></p>\n\n<p>Equation of tangent at point A of D</p>\n\n<p><span class=\"math-tex\">\\(\\left(\\frac{y+{a}^{2}-3}{2}\\right)=\\left(\\frac{{a}^{2}-3}{{a}^{2}}\\right).\\ xa\\)</span></p>\n\n<p><span class=\"math-tex\">\\(⇒2\\left({a}^{2}-3\\right)x-ay-{a}^{3}+3a=0\\)</span></p>\n\n<p>Now this tangent passes through point B (1, &ndash;2)</p>\n\n<p><span class=\"math-tex\">\\(⇒a=-2,\\ 1,\\ 3\\)</span></p>\n\n<p>a = 1 will be rejected as A and B coincide.</p>\n\n<p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187561376.png\" /></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Consider the curves <span class=\"math-tex\">\\(C:y={x}^{2}-3\\)</span> <span class=\"math-tex\">\\(D:y=k{x}^{2}\\ \\ \\ \\ \\ \\ {L}_{1}:x=a\\)</span> <span class=\"math-tex\">\\({L}_{2}:x=1\\ \\ \\ \\ \\left(a\\ne 0\\right)\\)</span></p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec5a",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>– 3</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>– 2</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>2</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>none of these </p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec59",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"92f7c455-52d5-49ef-9132-0a539a68cf20"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If <span class=\"math-tex\">\\(\\alpha ,\\ \\beta ,\\ \\gamma \\)</span> are roots of <span class=\"math-tex\">\\({x}^{3}+q\\ x+r=0\\)</span> then <span class=\"math-tex\">\\(\\sum \\frac{1}{{\\alpha }^{2}-\\beta \\gamma }\\)</span> equals</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(\\frac{\\alpha }{{\\alpha }^{3}-\\alpha \\beta \\gamma }=\\frac{\\alpha }{-q\\alpha -r+r}=-\\frac{1}{q}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Let <span class=\"math-tex\">\\(f\\left(x\\right)={a}_{n}{x}^{n}+{a}_{n-1}{x}^{n-1}+{a}_{n-2}{x}^{n-2}+........{a}_{1}x+{a}_{0}\\)</span> is polynomial of degree n.</p>\n\n<p>Let<span class=\"math-tex\">\\({\\alpha }_{{1}_{1}},\\ {\\alpha }_{{2}_{1}},\\ {\\alpha }_{3}.............{\\alpha }_{n}\\)</span>, are roots of <span class=\"math-tex\">\\(f\\left(x\\right)\\ =0\\ \\)</span></p>\n\n<p>Then <span class=\"math-tex\">\\(\\sum {\\alpha }_{1}\\)</span>= sum of roots =<span class=\"math-tex\">\\(-\\frac{{a}_{n-1}}{{a}_{n}}\\)</span>,</p>\n\n<p><span class=\"math-tex\">\\(\\sum {\\alpha }_{1}{\\alpha }_{2}\\)</span>= sum of product of roots taken two at a time =<span class=\"math-tex\">\\(\\frac{{a}_{n-2}}{{a}_{n}}\\)</span>,</p>\n\n<p><span class=\"math-tex\">\\(\\sum {\\alpha }_{1}{\\alpha }_{2}{\\alpha }_{3}\\)</span>=sum of product of roots taken three at a time =<span class=\"math-tex\">\\(-\\frac{{a}_{n-3}}{{a}_{n}}\\)</span></p>\n\n<p>,&hellip;&hellip;.. ,</p>\n\n<p><span class=\"math-tex\">\\({\\alpha }_{1}{\\alpha }_{2}{\\alpha }_{3}\\)</span> &hellip;.. <span class=\"math-tex\">\\({\\alpha }_{n}\\)</span>= product of roots = <span class=\"math-tex\">\\({\\left(-1\\right)}^{n}\\frac{{a}_{0}}{{a}_{n}}\\)</span></p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec5c",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(-\\frac{3}{q}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{q}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{q}{3}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>None of these</p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec5b",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"4395065a-b032-4eb1-8dc8-debbd2b357df"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If the x<sup>4</sup>+bx<sup>2</sup>+cx+d=0, has three same roots (say<span class=\"math-tex\">\\(\\alpha \\)</span>), then <span class=\"math-tex\">\\(\\alpha = \\)</span></p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(3\\alpha + \\beta = 0,{\\alpha ^3} + 3{\\alpha ^2}\\beta = - c,{\\alpha ^3}\\beta = d\\)</span></p>\n\n<p><span class=\"math-tex\">\\({1 \\over \\beta } + {3 \\over \\alpha } = - {c \\over d} \\Rightarrow \\alpha = - {{8d} \\over {3c}}\\)</span></p>\n",
              "questionHints":"",
              "passageQuestion":"<p>Let <span class=\"math-tex\">\\(f\\left(x\\right)={a}_{n}{x}^{n}+{a}_{n-1}{x}^{n-1}+{a}_{n-2}{x}^{n-2}+........{a}_{1}x+{a}_{0}\\)</span> is polynomial of degree n.</p>\n\n<p>Let<span class=\"math-tex\">\\({\\alpha }_{{1}_{1}},\\ {\\alpha }_{{2}_{1}},\\ {\\alpha }_{3}.............{\\alpha }_{n}\\)</span>, are roots of <span class=\"math-tex\">\\(f\\left(x\\right)\\ =0\\ \\)</span></p>\n\n<p>Then <span class=\"math-tex\">\\(\\sum {\\alpha }_{1}\\)</span>= sum of roots =<span class=\"math-tex\">\\(-\\frac{{a}_{n-1}}{{a}_{n}}\\)</span>,</p>\n\n<p><span class=\"math-tex\">\\(\\sum {\\alpha }_{1}{\\alpha }_{2}\\)</span>= sum of product of roots taken two at a time =<span class=\"math-tex\">\\(\\frac{{a}_{n-2}}{{a}_{n}}\\)</span>,</p>\n\n<p><span class=\"math-tex\">\\(\\sum {\\alpha }_{1}{\\alpha }_{2}{\\alpha }_{3}\\)</span>=sum of product of roots taken three at a time =<span class=\"math-tex\">\\(-\\frac{{a}_{n-3}}{{a}_{n}}\\)</span></p>\n\n<p>,&hellip;&hellip;.. ,</p>\n\n<p><span class=\"math-tex\">\\({\\alpha }_{1}{\\alpha }_{2}{\\alpha }_{3}\\)</span> &hellip;.. <span class=\"math-tex\">\\({\\alpha }_{n}\\)</span>= product of roots = <span class=\"math-tex\">\\({\\left(-1\\right)}^{n}\\frac{{a}_{0}}{{a}_{n}}\\)</span></p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec5e",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\({{8d} \\over {3b}}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\( - {{8d} \\over {3c}}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({{3b} \\over {8d}}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\({{3c} \\over {8d}}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec5d",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"4395065a-b032-4eb1-8dc8-debbd2b357df"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The least value of <span class=\"math-tex\">\\(|3z-13|+|3z-11|\\)</span> equal to</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(3\\left(|z-\\frac{13}{3}|+|z-\\frac{11}{3}|\\right)\\)</span> <span class=\"math-tex\">\\(⇒3\\)</span> times the distance between <span class=\"math-tex\">\\(\\frac{13}{3}\\mathrm{&amp;}\\frac{11}{3}\\)</span>. Similarly for the other question.</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>The geometrical meaning of <span class=\"math-tex\">\\(|{z}_{1}-{z}_{2}|\\)</span> is the distance between points z<sub>1 </sub>&amp; z<sub>2</sub> in the argand plane. One of the many applications of this is in solving least value problems. The fact that sum of two sides of a triangle can never be less than the third side is also widely used.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec60",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>0</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>2</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(\\frac{12}{5}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{7}{3}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec5f",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"737f2d05-edba-43c3-8313-71351b51fad4"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The least value of <span class=\"math-tex\">\\(|z-1+i|+|z+3-5i|\\)</span> equal to</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(3\\left(|z-\\frac{13}{3}|+|z-\\frac{11}{3}|\\right)\\)</span> <span class=\"math-tex\">\\(⇒3\\)</span> times the distance between <span class=\"math-tex\">\\(\\frac{13}{3}\\mathrm{&amp;}\\frac{11}{3}\\)</span>. Similarly for the other question.</p>\n",
              "questionHints":"",
              "passageQuestion":"<p>The geometrical meaning of <span class=\"math-tex\">\\(|{z}_{1}-{z}_{2}|\\)</span> is the distance between points z<sup>1</sup> &amp; z<sup>2</sup> in the argand plane. One of the many applications of this is in solving least value problems. The fact that sum of two sides of a triangle can never be less than the third side is also widely used.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec62",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{13}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\sqrt{11}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(2\\sqrt{13}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(2\\sqrt{11}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec61",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"737f2d05-edba-43c3-8313-71351b51fad4"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "passageQuestion":"<p>The geometrical meaning of <span class=\"math-tex\">\\(|{z}_{1}-{z}_{2}|\\)</span> is the distance between points z<sup>1</sup> &amp; z<sup>2</sup> in the argand plane. One of the many applications of this is in solving least value problems. The fact that sum of two sides of a triangle can never be less than the third side is also widely used.</p>\n",
              "_id":"5aab6dbbbc1d1f3013e0ec64",
              "questionHints":"",
              "solutionContent":"<p><span class=\"math-tex\">\\(T=\\left(a{t}_{1}{t}_{2},a\\left({t}_{1}+{t}_{2}\\right)\\right)\\)</span></p>\n\n<p><span class=\"math-tex\">\\({P}^{\\prime }=\\left\\{a{t}_{3}{t}_{1},a\\left({t}_{3}+{t}_{1}\\right)\\right\\}\\)</span></p>\n\n<p><span class=\"math-tex\">\\({Q}^{\\prime }=\\left\\{a{t}_{2}{t}_{3},a\\left({t}_{2}+{t}_{3}\\right)\\right\\}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(T{P}^{\\prime }:TP=\\lambda :1=\\frac{{t}_{2}-{t}_{3}}{{t}_{1}-{t}_{2}}\\)</span></p>\n\n<p><span class=\"math-tex\">\\({{TQ'} \\over {TQ}} = {{{t_1} - {t_3}} \\over {{t_1} - {t_2}}},\\frac{T{P}^{\\prime }}{TP}+\\frac{T{Q}^{\\prime }}{TQ} = 1\\)</span></p>\n\n<p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5aa907bd9fe7b00c182aed14/Question1521184187562659.png\" /></p>\n",
              "questionContent":"<p>TP and TQ are any two tangenFts to a parabola and the tangent at a third point R cuts them in P&rsquo; and Q&rsquo; then the value of <span class=\"math-tex\">\\(\\frac{T{P}^{\\prime }}{TP}+\\frac{T{Q}^{\\prime }}{TQ}\\)</span></p>\n\n<p>must be equal to</p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec63",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec66",
              "questionHints":"",
              "solutionContent":"<p>Let f(x) = x<sup>3</sup> – 3x + [a – 1]</p><p>f(x) = 3x<sup>2</sup> – 3   f(x) = 0   x =  1</p><p>Since f(x) has all the real roots in which two are equal. Hence f(–1) f(1) = 0</p><p> ([a – 1] – 2) ([a – 1] +2) = 0  either –1  a &lt; 0 or 3  a &lt; 4</p><p>Hence a = 3</p>",
              "questionContent":"<p>The positive integral value of ‘a’ for which the equation x<sup>3</sup> – 3x + [a – 1] = 0 has all its roots real wherein two are equal is (where [.] represents GIF) </p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec65",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec68",
              "questionHints":"",
              "solutionContent":"<p><span class='math-tex'>\\(\\frac{a+b\\omega +c{\\omega }^{2}}{c+a\\omega +b{\\omega }^{2}}=\\frac{1}{\\omega }\\left(\\frac{a\\omega +b{\\omega }^{2}+c{\\omega }^{3}}{c+a\\omega +b{\\omega }^{2}}\\right)=\\frac{1}{\\omega }={\\omega }^{2}\\)</span></p><p><span class='math-tex'>\\(\\frac{a+b\\omega +c{\\omega }^{2}}{b+c\\omega +a{\\omega }^{2}}=\\frac{1}{{\\omega }^{2}}\\left(\\frac{a{\\omega }^{2}+b{\\omega }^{3}+c{\\omega }^{4}}{b+c\\omega +a{\\omega }^{2}}\\right)=\\frac{1}{{\\omega }^{2}}=\\omega \\)</span></p>",
              "questionContent":"<p>If <span class='math-tex'>\\(\\omega \\)</span> be an imaginary cube root of unity and a,b and c be some fixed complex numbers, then the value of <span class='math-tex'>\\(|\\frac{a+b\\omega +c{\\omega }^{2}}{c+a\\omega +b{\\omega }^{2}}+\\frac{a+b\\omega +c{\\omega }^{2}}{b+c\\omega +a{\\omega }^{2}}+9|\\)</span>is</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec67",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec6a",
              "questionHints":"",
              "solutionContent":"<p><span class='math-tex'>\\({\\mathrm{log}}_{3}{n}^{12}=12{\\mathrm{log}}_{3}n,\\)</span> put <span class='math-tex'>\\({\\mathrm{log}}_{3}n=t\\)</span></p>",
              "questionContent":"<p>The natural number ‘n’, for which the expression <span class='math-tex'>\\(y=5{\\left({\\mathrm{log}}_{3}n\\right)}^{2}-{\\mathrm{log}}_{3}\\left({n}^{12}\\right)+9\\)</span>, has the minimum value is </p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec69",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5aab6dbbbc1d1f3013e0ec6c",
              "questionHints":"",
              "solutionContent":"<p><span class='math-tex'>\\(\\lambda =1818/18+1=102\\)</span> which is divisible by 2, 3, 17</p>",
              "questionContent":"<p>Let <span class='math-tex'>\\(\\lambda \\)</span> be the number of terms in the expansion of <span class='math-tex'>\\({\\left({5}^{1/6}+{7}^{1/9}\\right)}^{1818}\\)</span> which are integer, then the </p><p>number of positive integral divisors of <span class='math-tex'>\\(\\lambda \\)</span>are</p>",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5aab6dbbbc1d1f3013e0ec6b",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    }
  ],
  "startTime":1550494409.131
}

const startTest2 = {
  "code":200,
  "message":"Success",
  "data":{
    "__v":0,
    "userId":"5b12835a0815c7a84e746e88",
    "testId":"5ace20a29fe7b01d58072f6e",
    "courseId":"5ace202e9fe7b01d5806bb7c",
    "status":"discarded",
    "teacherId":null,
    "assignmentId":null,
    "orgId":null,
    "ip":"192.168.3.177",
    "attemptNo":2,
    "duration":1200,
    "testType":"concept",
    "testName":"JEEM-PQ4-PHY-Ohm's Law",
    "location":{
      "place":"NA"
    },
    "startTime":"2019-03-14T13:37:58.345Z",
    "maxTotalScore":30,
    "_id":"5c8a5936bee98c2ff7496ecf",
    "sqlId":"3442",
    "rankPotential":"Cut-off not cleared",
    "questions":[
      {
        "qId":"5bffa8b97af61505b3caf814",
        "questionCode":2,
        "_id":"5c8a5936bee98c2ff7496edb",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":1,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5abcfbdd1c9af0842f10736e",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496eda",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":2,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5a8ad025738197281fba603f",
        "questionCode":6,
        "_id":"5c8a5936bee98c2ff7496ed9",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":3,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5a8ad028738197281fba6090",
        "questionCode":1,
        "_id":"5c8a5936bee98c2ff7496ed8",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":4,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5abcfbdd1c9af0842f107374",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed7",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":5,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5abcfbdd1c9af0842f107376",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed6",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":6,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5a8ad025738197281fba6031",
        "questionCode":7,
        "_id":"5c8a5936bee98c2ff7496ed5",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":7,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5a8ad000738197281fba56ea",
        "questionCode":8,
        "_id":"5c8a5936bee98c2ff7496ed4",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":8,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5a8acf5b738197281fba2e99",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed3",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":9,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5abcfbdd1c9af0842f10737e",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed2",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":10,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5ae079a81d9124604485d46f",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed1",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":11,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      },
      {
        "qId":"5ae079a81d9124604485d46d",
        "questionCode":0,
        "_id":"5c8a5936bee98c2ff7496ed0",
        "attemptData":{
          "isCorrect":2,
          "questionPartialMarks":0,
          "questionNegativeMarks":1,
          "questionTotalMarks":3,
          "userTimeTaken":0,
          "userNegativeMarks":0,
          "userPositiveMarks":0,
          "userTotalMarks":0,
          "answer":[

          ],
          "isAttempted":false
        },
        "questionNo":12,
        "section":{
          "name":"Physics",
          "id":"5abcfbdd1c9af0842f107380",
          "subSection":{
            "name":"Single Correct",
            "id":"5abcfbdd1c9af0842f107381"
          }
        }
      }
    ],
    "sections":[
      {
        "_id":"5abcfbdd1c9af0842f107380",
        "name":"Physics",
        "id":"5abcfbdd1c9af0842f107380",
        "subSection":[
          {
            "_id":"5abcfbdd1c9af0842f107381",
            "name":"Single Correct",
            "isHidden":false,
            "id":"5abcfbdd1c9af0842f107381",
            "partialMarks":null,
            "negativeMarks":1,
            "positiveMarks":3,
            "marks":0,
            "noOfQuestions":10
          }
        ]
      }
    ],
    "sectionWiseResult":[

    ],
    "stats":{
      "improvement":0,
      "attemptRate":0,
      "speed":0,
      "accuracyRate":0,
      "wastedTime":0
    },
    "attemptData":{
      "userTimeTaken":0,
      "userIncorrectQuestionCount":0,
      "userCorrectQuestionCount":0,
      "userNegativeMarks":0,
      "userPositiveMarks":0,
      "correctQuestionMarks":0,
      "inCorrectQuestionMarks":0,
      "userTotalMarks":0
    },
    "isAutoSubmit":false,
    "challengeId":"",
    "needAnalysisAgain":false,
    "updatedAt":"2019-03-14T13:37:58.350Z",
    "createdAt":"2019-03-14T13:37:58.350Z",
    "courseName":"JEE(M) - 2019",
    "attemptDate":"2019-03-14T13:37:58.350Z",
    "rank":0
  },
  "assignmentInfo":null,
  "serverTime":1552570678.379,
  "locale":"IN",
  "country":"India"
}

const questions2 = {
  "qDatas":[
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "questionHints":"",
              "solutionContent":"<p><span style=\"font-size:11.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">CONCEPT</span></span></p>\n",
              "questionContent":"<p style=\"margin-left:28.35pt; margin:0in 0in 6pt\"><span style=\"font-size:11pt\"><span style=\"tab-stops:28.35pt 48.2pt 255.15pt 275.0pt\"><span style=\"font-family:Arial,sans-serif\">All the Al-Cl bonds in <span style=\"position:relative\"><span style=\"top:5.0pt\"><span class=\"math-tex\">\\(\\rm A{{l}_{2}}C{{l}_{6}}\\)</span>&nbsp;&nbsp;</span></span>are equivalent</span></span></span></p>\n",
              "locale":"en-us",
              "_id":"5bffa90d7af61505b3caf830",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "serialNo":1,
                  "value":"True",
                  "id":"1"
                },
                {
                  "serialNo":2,
                  "value":"False",
                  "id":"2"
                }
              ]
            }
          ]
        },
        "qId":"5bffa8b97af61505b3caf814",
        "questionType":"True-False",
        "questionCode":2,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Each of the resistances in the network shown in the figure. Is equal to R. The effective resistance between the terminals A and B is </p><p><img src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5abcae359fe7b0179ce42937/Question1522334685293286.png\" alt=\"D:\\EF Data Entry\\Set_6-6-Jan-2018\\Assigned\\6-Jan-2018\\Purusarth ramani\\Batch-2\\JEEM-PHY-Current Electricity\\3_Ohm's Law\\Images\\Question_2.PNG\" /></p>",
              "solutionContent":"<p>Redraw it as a Wheatstone’s network </p>",
              "questionHints":"",
              "_id":"5abcfbdd1c9af0842f10736f",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>R </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>5R </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>3R/5  </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>4R/7</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfbdd1c9af0842f10736e",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5a8ad025738197281fba6040",
              "questionHints":"",
              "solutionContent":"<p><span class=\"math-tex\">\\(|z-1|+z+2i+\\beta =0 \\\\ \\sqrt {{{(x - 1)}^2} + {y^2}} + x + \\beta + i(y + 2) = 0 \\\\ \\Rightarrow y = - 2 \\\\ {x^2} + 1 - 2x + 4 = {x^2} + {\\beta ^2} + 2\\beta x \\\\ f(\\beta ) = x = {{5 - {\\beta ^2}} \\over {2(\\beta + 1)}},f'(x) = {{(2\\beta + 2)( - 2\\beta ) - (5 - {\\beta ^2})2} \\over {4{{(\\beta + 1)}^2}}} \\\\ = {{ - 2{\\beta ^2} - 4\\beta - 10} \\over {4{{(\\beta + 1)}^2}}} &lt; 0\\)</span></p>\n\n<p>So <span class=\"math-tex\">\\(f\\left( \\beta \\right)_{\\max } = 1\\)</span></p>\n\n<p>So <span class=\"math-tex\">\\(\\alpha =1-2i\\)</span>&nbsp;for which |z| is maximum</p>\n\n<p><span class=\"math-tex\">\\({i}^{\\alpha }={e}^{\\frac{\\pi }{2}i\\left(1-2i\\right)}={e}^{\\pi }\\cdot {e}^{\\frac{\\pi }{2}i} \\\\ \\left[{i}^{\\alpha }\\right]=\\left[{e}^{\\pi }\\right]=19\\)</span></p>\n",
              "questionContent":"<p>Let z is a complex number satisfying <span class=\"math-tex\">\\(|z-1|+z+2i+\\beta =0\\)</span> when <span class=\"math-tex\">\\(\\beta \\in \\left(1,\\mathrm{\\infty }\\right)\\)</span>. If&nbsp;<span class=\"math-tex\">\\(\\alpha\\)</span> is value of z for which |z| is maximum, then value of <span class=\"math-tex\">\\(\\left[|{i}^{\\alpha }|\\right]\\)</span> (where [.] is G.I.F) is</p>\n",
              "locale":"en-us",
              "passageQuestion":"<p><strong>Comprehension:</strong></p>\n\n<p>Let g(x) = <span class=\"math-tex\">\\({\\int }_{0}^{x}\\left({t}^{2}+t+1\\right)\\)</span>dt, f(x) is a decreasing function <span class=\"math-tex\">\\(\\mathrm{\\forall }x\\ge 0\\)</span>&nbsp;and <span class=\"math-tex\">\\(\\vec A = f(x)\\hat i + g(x)\\hat j,{\\rm{ \\vec B}} = g(x)\\hat i + f(x)\\hat j\\)</span>&nbsp;makes obtuse angle with each other <span class=\"math-tex\">\\(\\mathrm{\\forall }x&gt;0\\)</span>&nbsp;then</p>\n",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5a8ad025738197281fba603f",
        "questionType":"Numerical",
        "questionCode":6,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Match the column I with column II</p>\n",
              "solutionContent":"<p>If net external force acting on a system is zero, then linear momentum of the system will be conserved. If net external torque acting on a system is zero, then angular momentum of the system will be conserved.</p>\n",
              "questionHints":"",
              "_id":"5a8ad028738197281fba6091",
              "passageQuestion":"<p>Paragraph for Question Nos. 12 to 14</p>\n\n<p>Velocity of efflux in Torricelli&rsquo;s theorem is given by <span class=\"math-tex\">\\(v=\\sqrt{2gh}\\)</span> here h is the height of hole<br />\nfrom the top surface, After that, motion of liquid can be treated as projectile motion. Take&nbsp;g = 10 m/s<sup>2</sup>.</p>\n",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[
                  {
                    "id":"Right1",
                    "serialNo":1,
                    "value":"<p>Kinetic energy of system will change.</p>"
                  },
                  {
                    "id":"Right2",
                    "serialNo":2,
                    "value":"<p>Momentum of system is conserved.</p>"
                  },
                  {
                    "id":"Right3",
                    "serialNo":3,
                    "value":"<p>Angular momentum of system is conserved about any point.</p>"
                  },
                  {
                    "id":"Right4",
                    "serialNo":4,
                    "value":"<p>Angular momentum  of system is conserved just before and after impact only about point of impact.</p>"
                  },
                  {
                    "id":"Right5",
                    "serialNo":5,
                    "value":"<p>Energy of the system during the collision / contact remains constant</p>"
                  }
                ],
                "optionLeft":[
                  {
                    "id":"Left1",
                    "serialNo":1,
                    "value":"<p>Particle moving with speed <em>v</em><sub>0</sub> strikes to a rod placed on a smooth table and sticks to it.</p>"
                  },
                  {
                    "id":"Left2",
                    "serialNo":2,
                    "value":"<p>A thin rod of mass <em>m</em> and length <em>l </em>inclined at an angle <span class=\"math-tex\">\\(\\theta\\)</span>&nbsp;with horizontal is dropped on a smooth horizontal plane without any angular velocity. Its tip does not rebound after impact.</p>\n"
                  },
                  {
                    "id":"Left3",
                    "serialNo":3,
                    "value":"<p>A solid sphere of mass <em>m</em> and radius <em>R </em>is<em> </em>rolling with velocity <em>v</em><sub>0</sub> along a horizontal plane. It suddenly encounters an obstacle.</p>"
                  },
                  {
                    "id":"Left4",
                    "serialNo":4,
                    "value":"<p>Two cylinders of radii  <em>r<sub>1</sub></em><sub>&nbsp;</sub>and<sub>&nbsp;</sub> <em>r</em><sub>2</sub>&nbsp; rotating about their axis with angular speed <span class=\"math-tex\">\\(\\omega\\)</span><sub>1</sub> and <span class=\"math-tex\">\\(\\omega\\)</span><sub>2</sub> moved closer to touch each other keeping their axis parallel. Cylinders first slip over each other at the contact point but slipping ceases after some time due to friction.</p>\n"
                  }
                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5a8ad028738197281fba6090",
        "questionType":"Matrix",
        "questionCode":1,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5abcfbdd1c9af0842f107375",
              "questionHints":"",
              "solutionContent":"<p>The circuit is equivalent to</p>\n\n<p>Apply KVL</p>\n\n<p><img alt=\"D:\\EF Data Entry\\Set_6-6-Jan-2018\\Assigned\\6-Jan-2018\\Purusarth ramani\\Batch-2\\JEEM-PHY-Current Electricity\\3_Ohm's Law\\Images\\Solution_5.PNG\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5abcae359fe7b0179ce42937/Question1522334685295299.png\" /></p>\n",
              "questionContent":"<p>The current in branch AB is</p>\n\n<p><img alt=\"D:\\EF Data Entry\\Set_6-6-Jan-2018\\Assigned\\6-Jan-2018\\Purusarth ramani\\Batch-2\\JEEM-PHY-Current Electricity\\3_Ohm's Law\\Images\\Question_5.PNG\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5abcae359fe7b0179ce42937/Question1522334685295970.png\" /></p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>infinite </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>1.33A</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>2 A </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>1.5 A</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfbdd1c9af0842f107374",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>In the circuit shown, current (in A) through the 50 V and 30 V batteries are, respectively:</p>\n\n<p><img alt=\"D:\\EF Data Entry\\Set_6-6-Jan-2018\\Assigned\\6-Jan-2018\\Purusarth ramani\\Batch-2\\JEEM-PHY-Current Electricity\\3_Ohm's Law\\Images\\Question_6.PNG\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5abcae359fe7b0179ce42937/Question1522334685296644.png\" /></p>\n",
              "solutionContent":"<p>Apply KVL.</p>\n\n<p><img alt=\"D:\\EF Data Entry\\Set_6-6-Jan-2018\\Assigned\\6-Jan-2018\\Purusarth ramani\\Batch-2\\JEEM-PHY-Current Electricity\\3_Ohm's Law\\Images\\Solution_6.PNG\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5abcae359fe7b0179ce42937/Question1522334685296380.png\" /></p>\n",
              "questionHints":"",
              "_id":"5abcfbdd1c9af0842f107377",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>3 and 2.5</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>4.5 and 1</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>3.5 and 2</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>2.5 and 3 </p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfbdd1c9af0842f107376",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5a8ad025738197281fba6032",
              "questionHints":"",
              "solutionContent":"<p><span class=\"math-tex\">\\(\\alpha =-1,\\text{ }\\beta =2 \\\\ \\mathrm{cos}\\beta x+\\mathrm{sin}2\\alpha x+\\sqrt{\\beta }\\\\ 0\\le \\mathrm{cos}2x-\\mathrm{sin}2x+\\sqrt{2}\\le 2\\sqrt{2}\\\\ ⇒{\\left[\\mathrm{cos}2x-\\mathrm{sin}2x+\\sqrt{2}\\right]}_{max}=2\\)</span></p>\n",
              "questionContent":"<p>If <span class=\"math-tex\">\\(\\beta &gt;0\\)</span> and <span class=\"math-tex\">\\(\\alpha &lt;0\\)</span> such that <span class=\"math-tex\">\\(\\alpha +\\beta +\\frac{\\alpha }{\\beta }=\\frac{1}{2}\\)</span> and <span class=\"math-tex\">\\(\\left(\\alpha +\\beta \\right)\\frac{\\alpha }{\\beta }=-\\frac{1}{2}\\)</span>, then find the maximum value of <span class=\"math-tex\">\\(\\left[\\mathrm{cos}\\beta x+\\mathrm{sin}2\\alpha x+\\sqrt{\\beta }\\right]\\)</span>.</p>\n",
              "locale":"en-us",
              "passageQuestion":"<p><strong>Comprehension:</strong></p>\n\n<p>Let g(x) = <span class=\"math-tex\">\\({\\int }_{0}^{x}\\left({t}^{2}+t+1\\right)\\)</span>dt, f(x) is a decreasing function <span class=\"math-tex\">\\(\\mathrm{\\forall }x\\ge 0\\)</span>&nbsp;and <span class=\"math-tex\">\\(\\vec A = f(x)\\hat i + g(x)\\hat j,{\\rm{ \\vec B}} = g(x)\\hat i + f(x)\\hat j\\)</span>&nbsp;makes obtuse angle with each other <span class=\"math-tex\">\\(\\mathrm{\\forall }x&gt;0\\)</span>&nbsp;then</p>\n",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[

              ]
            }
          ]
        },
        "qId":"5a8ad025738197281fba6031",
        "questionType":"Integer",
        "questionCode":7,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Two bodies A and B of masses 5.0 kg and 10.0 kg moving in free space in opposite directions with velocities 4.0 m/s and 0.5 m/s respectively undergo a head on collision. The force F of their mutual interaction varies with time t according to the given graph. What can you conclude from the given information?</p>\n\n<p><img alt=\"fig\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5a843eee9fe7b01004ecc870/Question1519046655661208.png\" /></p>\n",
              "solutionContent":"<p><img alt=\"fig\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5a843eee9fe7b01004ecc870/Question1519046655662667.png\" /></p>\n\n<p>Area under F &ndash; t graph gives impulse</p>\n\n<p>Area = <span class=\"math-tex\">\\(\\frac{1}{2}×0.3×150=22.5\\)</span></p>\n\n<p>&nbsp;</p>\n\n<p>Impulse = 22.5 N &ndash; sec</p>\n\n<p>For 10 kg, J = 10 V<sub>1</sub> &ndash; (&minus;0.5 &times; 10)</p>\n\n<p>22.5 = 10 V<sub>1</sub> + 5, V<sub>1</sub> = 1.74 m/s</p>\n\n<p>For 5 kg,&nbsp;J = 5 V<sub>2</sub> = &minus;(&minus; 5 &times; 40)</p>\n\n<p>V<sub>2</sub> = 0.5 m/s</p>\n\n<p><span class=\"math-tex\">\\(e=\\frac{{V}_{1}+{V}_{2}}{{u}_{1}+{u}_{2}}=\\frac{1.75+0.5}{4+0.5}=\\frac{2.25}{4.5}=0.5\\)</span></p>\n\n<p>For period of deformation</p>\n\n<p>5 <span class=\"math-tex\">\\(\\times\\)</span> 4 + (&minus;0.5 &times; 10) = 5 V + 10 V&nbsp;&rArr; V = 1 m/s</p>\n\n<p><span class=\"math-tex\">\\(J=\\left(-5×1\\right)-\\left(-5×4\\right)=15\\)</span>,&nbsp;Area = <span class=\"math-tex\">\\(\\frac{1}{2}×t×150=15\\)</span>,&nbsp;T = 0.2 sec</p>\n",
              "questionHints":"",
              "_id":"5a8ad000738197281fba56eb",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>period of deformation is 0.2 sec </p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>co-efficient of restitution is 0.5</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>body A will move with velocity 0.5 m/s in the original direction </p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>body B will move with velocity 1.75 m/s in reverse direction </p>"
                }
              ]
            }
          ]
        },
        "qId":"5a8ad000738197281fba56ea",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5a8acf5b738197281fba2e9a",
              "questionHints":"",
              "solutionContent":"<p><span class=\"math-tex\">\\(\\frac{3}{8}r\\cdot \\rho \\frac{2}{3}\\pi {r}^{3}=\\frac{h}{4}\\cdot \\rho \\frac{1}{3}\\pi {r}^{2}h\\)</span></p>\n\n<p>&rArr;&nbsp;&nbsp;<span class=\"math-tex\">\\(h = \\sqrt{3}\\ r\\)</span></p>\n\n<p><img alt=\"Fig\" src=\"https://question-uploader.s3-us-west-2.amazonaws.com/TestPapers/5a843eee9fe7b01004ecc861/Question1519046491408425.png\" /></p>\n",
              "questionContent":"<p>A uniform solid hemisphere of radius r is joined to uniform solid right circular cone of base of radius r. Both have same density. The centre of mass of the composite solid lies on the common face. The height (h) of the cone is</p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>2r</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class=\"math-tex\">\\(\\sqrt{3}\\ r\\)</span></p>\n"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>3r</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(r\\sqrt{6}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5a8acf5b738197281fba2e99",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Two conductors have the same resistance at 0<sup>o</sup>C but their temperature coefficients of resistance are <span class=\"math-tex\">\\(\\alpha_1\\)</span>&nbsp;and <span class=\"math-tex\">\\(\\alpha_2\\)</span>. The respective temperature coefficients of their series and parallel combinations are nearly</p>\n",
              "solutionContent":"<p>R<sub>1</sub> + R<sub>2</sub> = R<sub>0</sub> (1 + <span class=\"math-tex\">\\(\\alpha_1\\)</span>T) + R<sub>0</sub>(1 + <span class=\"math-tex\">\\(\\alpha_2\\)</span>T) = 2R<sub>0</sub>(1 + <span class=\"math-tex\">\\(\\alpha_{eff}\\)</span>T)</p>\n\n<p>R<sub>1</sub>||R<sub>2</sub> = <span class=\"math-tex\">\\(\\frac{{R}_{1}{R}_{2}}{{R}_{1}+{R}_{2}}\\equiv \\frac{{R}_{0}}{2}\\left(1+{\\alpha }_{eff}T\\right)\\)</span>.</p>\n",
              "questionHints":"",
              "_id":"5abcfbdd1c9af0842f10737f",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{{\\alpha }_{1}+{\\alpha }_{2}}{2},\\ \\ {\\alpha }_{1}+{\\alpha }_{2}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({\\alpha }_{1}+{\\alpha }_{2},\\ \\ \\ \\frac{{\\alpha }_{1}+{\\alpha }_{2}}{2}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({\\alpha }_{1}+{\\alpha }_{2},\\ \\ \\ \\frac{{\\alpha }_{1}\\ {\\alpha }_{2}}{{\\alpha }_{1}+{\\alpha }_{2}}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{{\\alpha }_{1}+{\\alpha }_{2}}{2},\\ \\ \\ \\frac{{\\alpha }_{1}+{\\alpha }_{2}}{2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfbdd1c9af0842f10737e",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5ae079a81d9124604485d470",
              "passageQuestion":"<p>Directions (Q. 60 to 64): The following questions are based on the information given below:</p>\n\n<p>Data on 450 candidates, who took an examination in Social Science, Mathematics and Science is given below:</p>\n\n<p>Passed in all the subjects&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;167</p>\n\n<p>Failed in all the subjects&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;60</p>\n\n<p>Failed in Social Science&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 175</p>\n\n<p>Failed in Mathematics&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 199</p>\n\n<p>Failed in Science&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 191</p>\n\n<p>Passed in Social Sciences only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 62</p>\n\n<p>Passed in Mathematics only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;48</p>\n\n<p>Passed in Science only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 52</p>\n",
              "questionHints":"",
              "solutionContent":"<p>Candidates failed in one subject only</p>\n\n<p>= (Total number of candidates)-(Candidates passed in all the subjects + Candidates failed in all the subjects + Candidates passed in one subject only)</p>\n\n<p>= 450 -(167 + 60 + 62 + 48 + 52) = 450 - 389 = 61</p>\n",
              "questionContent":"<p>How many failed in one subject only ?</p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>152</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>144</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>61</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>56</p>"
                }
              ]
            }
          ]
        },
        "qId":"5ae079a81d9124604485d46f",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"00737958-82ff-481e-8383-2bcaa5430e20"
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "_id":"5ae079a81d9124604485d46e",
              "passageQuestion":"<p>Directions (Q. 60 to 64): The following questions are based on the information given below:</p>\n\n<p>Data on 450 candidates, who took an examination in Social Science, Mathematics and Science is given below:</p>\n\n<p>Passed in all the subjects&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;167</p>\n\n<p>Failed in all the subjects&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 60</p>\n\n<p>Failed in Social Science&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;175</p>\n\n<p>Failed in Mathematics&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;199</p>\n\n<p>Failed in Science&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;191</p>\n\n<p>Passed in Social Sciences only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;62</p>\n\n<p>Passed in Mathematics only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;48</p>\n\n<p>Passed in Science only&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;52</p>\n",
              "questionHints":"",
              "solutionContent":"<p>Candidates failed in Social Sciences only</p>\n\n<p>= (Candidates failed in Social Sciences) &ndash;(Candidates failed in all the subjects + Candidates passed in Science only + Candidates passed in Maths only)</p>\n\n<p>= 175-(60 + 52 + 48) = 175-160 = 15</p>\n",
              "questionContent":"<p>How many failed in Social Sciences only ?</p>\n",
              "locale":"en-us",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>15</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>21</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>30</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>42</p>"
                }
              ]
            }
          ]
        },
        "qId":"5ae079a81d9124604485d46d",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":"00737958-82ff-481e-8383-2bcaa5430e20"
      }
    }
  ],
  "startTime":1552570683.542,
  "locale":"IN",
  "country":"India"
}

const resumeTestData = {
  "data":{
    "dbData":{
      "_id":"5c62c09b054c2e75289feb54",
      "userId":"5b12835a0815c7a84e746e2c",
      "testId":"5ace20a59fe7b01d58073323",
      "courseId":"5ace202e9fe7b01d5806bb7d",
      "status":"notFinished",
      "teacherId":null,
      "assignmentId":null,
      "orgId":null,
      "ip":"192.168.3.119",
      "attemptNo":2,
      "duration":1200,
      "testType":"concept",
      "testName":"JEE-A-1617- PHY- velocity of longitudinal and tran",
      "location":{
        "place":"NA"
      },
      "startTime":"2019-03-12T07:38:37.859Z",
      "maxTotalScore":24,
      "__v":0,
      "sqlId":"6587",
      "rankPotential":"Cut-off not cleared",
      "questions":[
        {
          "qId":"5abcfdd81c9af0842f108272",
          "questionCode":0,
          "_id":"5c62c09b054c2e75289feb5c",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":1,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f108274",
          "questionCode":0,
          "_id":"5c62c09b054c2e75289feb5b",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":2,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f108276",
          "questionCode":0,
          "_id":"5c62c09b054c2e75289feb5a",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":3,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f108278",
          "questionCode":0,
          "_id":"5c62c09b054c2e75289feb59",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":4,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f10827a",
          "questionCode":0,
          "_id":"5c62c09b054c2e75289feb58",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":5,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f10827c",
          "questionCode":8,
          "_id":"5c62c09b054c2e75289feb57",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":6,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f10827e",
          "questionCode":8,
          "_id":"5c62c09b054c2e75289feb56",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":7,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        },
        {
          "qId":"5abcfdd81c9af0842f108280",
          "questionCode":8,
          "_id":"5c62c09b054c2e75289feb55",
          "attemptData":{
            "isCorrect":2,
            "questionPartialMarks":0,
            "questionNegativeMarks":1,
            "questionTotalMarks":3,
            "userTimeTaken":0,
            "userNegativeMarks":0,
            "userPositiveMarks":0,
            "userTotalMarks":0,
            "answer":[

            ],
            "isAttempted":false
          },
          "questionNo":8,
          "section":{
            "name":"Physics",
            "id":"5abcfdd81c9af0842f108282",
            "subSection":{
              "name":"Waves &amp; Sound [WS]",
              "id":"5abcfdd81c9af0842f108283"
            }
          }
        }
      ],
      "sections":[
        {
          "_id":"5abcfdd81c9af0842f108282",
          "name":"Physics",
          "id":"5abcfdd81c9af0842f108282",
          "subSection":[
            {
              "_id":"5abcfdd81c9af0842f108283",
              "name":"Waves &amp; Sound [WS]",
              "isHidden":false,
              "id":"5abcfdd81c9af0842f108283",
              "partialMarks":null,
              "negativeMarks":1,
              "positiveMarks":3,
              "marks":0,
              "noOfQuestions":8
            }
          ]
        }
      ],
      "sectionWiseResult":[

      ],
      "stats":{
        "improvement":0,
        "attemptRate":0,
        "speed":0,
        "accuracyRate":0,
        "wastedTime":0
      },
      "attemptData":{
        "userTimeTaken":0,
        "userIncorrectQuestionCount":0,
        "userCorrectQuestionCount":0,
        "userNegativeMarks":0,
        "userPositiveMarks":0,
        "correctQuestionMarks":0,
        "inCorrectQuestionMarks":0,
        "userTotalMarks":0
      },
      "isAutoSubmit":false,
      "challengeId":"",
      "needAnalysisAgain":false,
      "updatedAt":"2019-03-12T07:38:37.867Z",
      "createdAt":"2019-02-12T12:48:27.283Z",
      "courseName":"JEE(A) - 2019",
      "attemptDate":"2019-02-12T12:48:27.283Z",
      "rank":0
    },
    "redisData":[
      {
        "questionId":"5abcfdd81c9af0842f108272",
        "userId":"5b12835a0815c7a84e746e2c",
        "testId":"5ace20a59fe7b01d58073323",
        "courseId":"5ace202e9fe7b01d5806bb7d",
        "answer":[

        ],
        "timeTaken":"3",
        "isAttempted":false,
        "attemptNo":2,
        "attemptId":"5c62c09b054c2e75289feb54",
        "sectionId":"5abcfdd81c9af0842f108282",
        "subSectionId":"5abcfdd81c9af0842f108283",
        "optionsMatrix":[

        ],
        "optionsMcq":[

        ],
        "type":0,
        "isMarked":false,
        "isVisited":true,
        "duration":0,
        "isAnsweredMarkedForReview":false
      }
    ],
    "redisStartTime":1552376317.859,
    "assignmentInfo":null
  },
  "code":200,
  "message":"success",
  "serverTime":1552376952.782,
  "locale":"IN",
  "country":"India"
}

const resumeQuestionData = {
  "qDatas":[
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Sound velocity is maximum in</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\(v=\\sqrt{\\frac{\\gamma RT}{M}}\\)</span> <span class=\"math-tex\">\\(\\Rightarrow\\)</span>&nbsp;<span class=\"math-tex\">\\(v\\propto \\frac{1}{\\sqrt{M}}\\)</span>. Since &nbsp;<span class=\"math-tex\">\\(M\\)</span>&nbsp; is minimum for &nbsp;<span class=\"math-tex\">\\({H}_{2}\\)</span>&nbsp; so sound velocity is maximum in <em>H</em><sub>2</sub>.</p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f108273",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\({H}_{2}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\({N}_{2}\\)</span> </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(He\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\({O}_{2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f108272",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If at same temperature and pressure, the densities for two diatomic gases are respectively <span class=\"math-tex\">\\({d}_{1}\\)</span> and <span class=\"math-tex\">\\({d}_{2}\\)</span>, then the ratio of velocities of sound in these gases will be</p>\n",
              "solutionContent":"<p>Speed of sound <span class=\"math-tex\">\\(v=\\sqrt{\\frac{\\gamma P}{d}}\\)</span>&nbsp;<span class=\"math-tex\">\\(\\Rightarrow\\)</span>&nbsp;<span class=\"math-tex\">\\({{{v_1}} \\over {{v_2}}} = \\sqrt {{{{d}_{2}} \\over {{d}_{1}}}} \\)</span>&nbsp; &nbsp; (∵ <span class=\"math-tex\">\\(\\ P\\text{ }-\\)</span>&nbsp;constant )</p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f108275",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\sqrt {{{{d}_{2}} \\over {{d}_{1}}}} \\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\sqrt {{{{d}_{1}} \\over {{d}_{2}}}} \\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\({d}_{1}{d}_{2}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\sqrt {{d}_{1}{d}_{2}} \\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f108274",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Velocity of sound measured in hydrogen and oxygen gas at a given temperature will be in the ratio </p>",
              "solutionContent":"<p><span class='math-tex'>\\(v\\propto \\frac{1}{\\sqrt{M}}⇒\\frac{{v}_{{H}_{2}}}{{v}_{{O}_{2}}}=\\sqrt{\\frac{{M}_{{O}_{2}}}{{M}_{{H}_{2}}}}=\\sqrt{\\frac{32}{2}}⇒\\frac{{v}_{{H}_{2}}}{{v}_{{O}_{2}}}=\\frac{4}{1}\\)</span></p>",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f108277",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1 : 4</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>4 : 1</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>2 : 1</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>1 : 1</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f108276",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>The velocity of sound is <em>v<sub>s</sub></em> in air. If the density of air is increased to 4 times, then the new velocity of sound will be</p>\n",
              "solutionContent":"<p><span class=\"math-tex\">\\({v}_{sound}\\propto \\frac{1}{\\sqrt{\\rho }}⇒\\frac{{v}_{1}}{{v}_{2}}=\\sqrt{\\frac{{\\rho }_{2}}{{\\rho }_{1}}}=\\sqrt{\\frac{4}{1}}=2\\)</span>&nbsp;&nbsp;<span class=\"math-tex\">\\(⇒\\)</span>&nbsp;<span class=\"math-tex\">\\({v_2} = {{{v_1}} \\over 2} = \\frac{{v}_{s}}{2}\\)</span></p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f108279",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><span class='math-tex'>\\(\\frac{{v}_{s}}{2}\\)</span></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><span class='math-tex'>\\(\\frac{{v}_{s}}{12}\\)</span></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><span class='math-tex'>\\(12{v}_{s}\\)</span></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><span class='math-tex'>\\(\\frac{3}{2}{v}_{s}^{2}\\)</span></p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f108278",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>It takes 2.0 seconds for a sound wave to travel between two fixed points when the day temperature is <span class=\"math-tex\">\\({10}^{\\circ }C\\)</span> If the temperature rise to <span class=\"math-tex\">\\({30}^{\\circ }C\\)</span> the sound wave travels between the same fixed parts in</p>\n",
              "solutionContent":"<p>Suppose the distance between two fixed points is <em>d</em> then</p>\n\n<p><span class=\"math-tex\">\\(t=\\frac{d}{v}\\text{ }also\\;\\;v\\propto \\sqrt{T}⇒\\frac{{t}_{1}}{{t}_{2}}=\\frac{{v}_{2}}{{v}_{1}}=\\sqrt{\\frac{{T}_{2}}{{T}_{1}}}\\)</span></p>\n\n<p><span class=\"math-tex\">\\(\\Rightarrow\\;\\;\\frac{2}{{t}_{2}}=\\sqrt{\\frac{303}{283}}\\)</span>&nbsp;<span class=\"math-tex\">\\(⇒\\)</span>&nbsp;<span class=\"math-tex\">\\({t}_{2}\\)</span>= 1.9 <em>sec</em>.</p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f10827b",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>1.9 <em>sec</em></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>2.0 <em>sec</em></p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>2.1 <em>sec</em></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>2.2 <em>sec</em></p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f10827a",
        "questionType":"SMCQ",
        "questionCode":0,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If <em>v<sub>m</sub></em> is the velocity of sound in moist air, <em>v<sub>d</sub></em> is the velocity of sound in dry air, under identical conditions of pressure and temperature then which of the following is incorrect</p>\n",
              "solutionContent":"<p>The density of moist air (<em>i.e.</em> air mixed with water vapours) is less than the density of dry air</p>\n\n<p>Hence from <span class=\"math-tex\">\\(v=\\sqrt{\\frac{\\gamma P}{\\rho }}\\)</span> <span class=\"math-tex\">\\({v}_{moist\\ air}&gt;{v}_{dry\\ air}\\)</span></p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f10827d",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p><em>v<sub>m</sub></em> &gt; <em>v<sub>d</sub></em></p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p><em>v<sub>m</sub></em> &lt; <em>v<sub>d</sub></em> </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p><em>v<sub>m</sub></em> = <em>v<sub>d</sub></em></p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p><em>v<sub>m</sub>v<sub>d</sub></em> = 1</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f10827c",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>If the temperature of the atmosphere is increased the following character of the sound wave is not effected</p>\n",
              "solutionContent":"<p>Since <span class=\"math-tex\">\\(v=\\sqrt{\\frac{\\gamma \\ RT}{M}}\\)</span> <em>i.e.,&nbsp;</em><span class=\"math-tex\">\\(v\\propto \\sqrt{T}\\)</span><em>; &nbsp;&nbsp;</em><span class=\"math-tex\">\\(v=f\\lambda \\ \\ \\ \\ i.e.\\ \\ \\ \\ \\ v\\propto \\ \\lambda \\ \\ \\)</span>&nbsp;because source is responsible of change in frequency.</p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f10827f",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Amplitude</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Frequency </p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Velocity</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>Wavelength</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f10827e",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    },
    {
      "status":"200",
      "message":"Question retrieved",
      "data":{
        "qData":{
          "content":[
            {
              "locale":"en-us",
              "questionContent":"<p>Velocity of sound in air</p>\n",
              "solutionContent":"<p>Speed of sound <span class=\"math-tex\">\\(v\\propto \\sqrt{T}\\)</span>and it is independent of pressure.</p>\n",
              "questionHints":"",
              "_id":"5abcfdd81c9af0842f108281",
              "correctAnswer":{
                "answerType":"no data here"
              },
              "matrixOptionContent":{
                "optionRight":[

                ],
                "optionLeft":[

                ]
              },
              "optionsContent":[
                {
                  "id":"Option1",
                  "serialNo":1,
                  "value":"<p>Increases with temperature</p>"
                },
                {
                  "id":"Option2",
                  "serialNo":2,
                  "value":"<p>Decreases with temperature</p>"
                },
                {
                  "id":"Option3",
                  "serialNo":3,
                  "value":"<p>Is independent of pressure</p>"
                },
                {
                  "id":"Option4",
                  "serialNo":4,
                  "value":"<p>Is independent of temperature</p>"
                }
              ]
            }
          ]
        },
        "qId":"5abcfdd81c9af0842f108280",
        "questionType":"MMCQ",
        "questionCode":8,
        "passageId":null
      }
    }
  ],
  "startTime":1552377097.491,
  "locale":"IN",
  "country":"India"
}

export const MOCK_DATA = {
  startTest,
  startTestReattempt,
  questions,
  startTest2,
  questions2
}
