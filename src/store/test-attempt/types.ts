import { observable, toJS, ObservableMap } from 'mobx'
import { get, isNil, cloneDeep, pick } from 'lodash'
import { icons } from '../../common'

/**
 * The format of the question that is to be sent in the finish API.
 */
export interface IQuestionPayload {
  // question identification related data
  attemptId: string
  attemptNo: number
  courseId: string
  questionId: string
  sectionId: any
  subSectionId: any
  // test related data
  testId: any
  duration: number
  // question meta
  isAnsweredMarkedForReview: boolean
  isAttempted: boolean
  isMarked: boolean
  isVisited: boolean
  // answer related
  // TODO: finalize this
  options: IOptionsType[]
  optionsMatrix: IMatrixOptions
  optionsMcq: IMCQOptions[]
  startTime: any
  timePerQuestion: string
  // TODO: unwanted fields
  type: any
  flag: number
}

/**
 * The payload format of the finish API.
 */
export interface IFinishPayload {
  attemptData: IQuestionPayload[]
  attemptId: string
  courseId: string
  testId: string
  totalTimeTaken: string
}

export interface IQuestionAttemptTimeStats {
  userTimeTaken?: number
}

/**
 * The question attempt data to be kept in the question store.
 */
export interface IQuestionAttempt extends IQuestionAttemptTimeStats {
  isCorrect?: number
  questionPartialMarks: number
  questionNegativeMarks: number
  questionTotalMarks: number
  userNegativeMarks?: number
  userPositiveMarks?: number
  userTotalMarks?: number
  answerType?: answerType
  answer: any[]
  legacyAnswer: any
  isAttempted: boolean
  isVisited: boolean
  isAnsweredMarkedForReview: boolean
  isMarked: boolean
}

/**
 * Question section which is to be kept in the question store.
 */
export interface IQuestionSection {
  _id?: string
  id: string
  name: string
  subSection: IQuestionSubSection
}

export interface IQuestionSubSection {
  _id?: string
  id: string
  name: string
  isHidden?: boolean
  marks?: number
  positiveMarks?: number
  negativeMarks?: number
  noOfQuestions?: number
  partialMarks?: number
}

/**
 * Sections Data
 */
 export interface ISection {
   _id: string
   id: string
   name: string
   subSection: ISubSection[]
 }

 export interface ISubSection {
  _id: string
  name: string
  isHidden: boolean
  id: string
  partialMarks: number
  negativeMarks: number
  positiveMarks: number
  marks: number
  noOfQuestions: number
 }

/**
 * The format of the question store.
 */
export interface IQuestion {
  qId: string
  questionCode: number
  questionType: string
  _id: string
  attemptData: IQuestionAttempt
  questionNo: number
  section: IQuestionSection
  // declare all the answer types
  correctAnswer?: any
  questionContent?: string
  questionHints?: string
  solutionContent?: string
}

export interface IQdata {
  content: IQdataContent
}

export interface IQdataContent {
  locale: string
  questionContent: string
  solutionContent: string
  questionHints: string
  _id: string
  correctAnswer: ICorrectAnswer
  matrixOptionContent: IMatrixOptionContent
  optionsContent: IOption[]
}

export interface ICorrectAnswer {
  answerType: string
}

export interface IMatrixOptionContent {
  optionRight: any[]
  optionLeft: any[]
}

/**
 * The overall question stats for the test.
 */
export interface IQuestionStats {
  notAnswered: number
  answered: number
  markedForReview: number
  notVisited: number
  answeredAndMarkedForReview: number
}

/**
 * The question node format for the questions linked list.
 */
export interface IQuestionNode {
  qId: string
  next?: IQuestionNode
  prev?: IQuestionNode
}

/**
 * The format of the value to be stored against question IDs in the questions map.
 */
export interface IQuestionMapItem {
  questionNode: IQuestionNode
  question: QuestionItemStore
}

/**
 * The format of the questions map.
 * This map is supposed to store the questions values against the qId.
 */
export interface IQuestionsMap {
  [key: string]: IQuestionMapItem
}
// tslint:disable-next-line:no-unused-expression

/**
 * The format of the correct option data in the test.
 */
export interface ICorrectOptionData {
  id?: string
  weightage?: number
  value: any
  _id?: any
  positiveTolerance?: number
  negativeTolerance?: number
}

/**
 * The answer format to be used for the event answer payload.
 */
export interface ICorrectOption {
  answerType?: 'id' | 'value'
  data: ICorrectOptionData | ICorrectOptionData[]
}

/**
 * Listing of the options for a question.
 */
export interface IOption {
  serialNo: number
  id: string
  value: string
}

/**
 * The same is not being set anywhere yet, however, it is to be used as the question options.
 */
export interface IQuestionOptions {
  correctAnswer: ICorrectOption
}

export interface ISMCQOptions extends IQuestionOptions {
  optionsContent: IOption[]
}

export interface IMMCQOptions extends IQuestionOptions {
  optionsContent: IOption[]
}

export interface IMatrixOptions extends IQuestionOptions {
  optionsRight: IOption[]
  optionsLeft: IOption[]
}

export interface IIntegerOptions extends IQuestionOptions {
}

export interface ITrueFalseOptions extends IQuestionOptions {
  optionsContent: IOption[]
}

export type IOptionsType = ITrueFalseOptions | IIntegerOptions | IMatrixOptions | IMMCQOptions | ISMCQOptions

export type IMCQOptions = IMMCQOptions | ISMCQOptions

/**
 * The format for the test attempt stats.
 */
export interface ITestAttempt {
  userTimeTaken: number
  userIncorrectQuestionCount: number
  userCorrectQuestionCount: number
  userNegativeMarks: number
  userPositiveMarks: number
  correctQuestionMarks: number
  inCorrectQuestionMarks: number
  userTotalMarks: number
}

/**
 * The assignment format received in the test payload.
 */
export interface IAssignmentInfo {
  assignedDate: string
  dueDate: string
}

/**
 * The test payload received in the start test API.
 */
export interface ITest {
  userId: string
  testId: string
  teacherId?: string
  assignmentId?: string
  orgId?: string
  courseId: string
  attemptNo: number
  ip?: string
  location: any
  testType: string
  duration: number
  testName: string
  startTime?: string
  maxTotalScore: number
  status: string
  _id: string
  sqlId: string
  rankPotential: string
  // questions: QuestionItemStore[]
  questions: IQuestion[]
  questionContent: QuestionItemStore[]
  sections: IQuestionSection[]
  attemptData: ITestAttempt
  sectionWiseResult?: any
  stats?: any
  isAutoSubmit?: boolean
  challengeId?: string
  needAnalysisAgain?: false
  updatedAt: string
  createdAt: string
  courseName: string
  attemptDate?: string
  rank?: string
  assignmentInfo?: IAssignmentInfo
  serverTime: number
}

export interface IAnswer {
  optionsMatrix: any[]
  optionsMcq: IAnswerOption[]
  type: number
  isMarked: boolean
  isVisited: boolean
  startTime: number
  duration: number
  sectionId: string
  subSectionId: string
  attemptId: string
  testId: string
  questionId: string
  options: IAnswerOption[]
  timePerQuestion: string
  courseId: string
  isAttempted: boolean
  attemptNo: number
  isAnsweredMarkedForReview: boolean
  flag: number
}

export interface IAnswerOption {
  id?: string
  value: boolean
}

export interface IInstruction {

}

/**
 *
 * Sections questions mapping
 */
export interface ISectionStats {
  notAnswered: number
  answered: number
  markedForReview: number
  notVisited: number
  answeredAndMarkedForReview: number
  totalQuestions?: number
  totalCorrectQuestions?: number
  totalIncorrectQuestions?: number
  totalMarks?: number
  marksPercentage?: number
  totalCorrectMarks?: number
  totalIncorrectMarks?: number
  timeTaken?: number
  timeTakenPercentage?: number
}

export interface ISectionsQuestionsMap {
  [key: string]: {
    id: string
    name: string
    subSection: ISubSectionQuestionsMap
    questions: string[]
    displayName: string
    stats?: ISectionStats
  }
}

export interface ISubSectionQuestionsMap {
  [key: string]: {
    _id?: string
    id: string
    name: string
    isHidden: boolean
    partialMarks: number
    negativeMarks: number
    positiveMarks: number
    marks: number
    noOfQuestions: number
    questions: string[]
    displayName?: string
    stats: ISubSectionStats
  }
}

export interface ISubSectionStats {
  notAnswered: number
  answered: number
  markedForReview: number
  notVisited: number
  answeredAndMarkedForReview: number
}

// export interface ISectionInstruction {
//   questionCount: number
//   optionCount: string
//   optionSelectionScheme: string
//   markingScheme: ISectionMarkingScheme
// }

// export interface ISectionMarkingScheme {
//   correctScheme: string
//   inCorrectScheme: string
//   noResponseScheme: string
// }

/**
 * The observable question item store.
 * We are supposed to serve this whenever rendering a question.
 */
export class QuestionItemStore {
  qId: string
  questionCode: string
  questionType: string
  questionNo: number
  sectionId: string
  subSectionId: string
  // TODO: handle type
  // questionOptions: any
  locale?: string
  qData: IQdata
  passageId: string
  answerPayload: IAnswer
  correctAnswer: ICorrectOption
  @observable selectedOptionList: any[]
  @observable answeredValue: string
  @observable attemptData: any
  attemptDataTimeStats: IQuestionAttemptTimeStats

  constructor(question, testMeta) {
    this.qId = question.data.qId
    this.questionCode = '' + question.data.questionCode
    this.questionType = question.data.questionType
    this.questionNo = question.data.questionNo
    this.locale = question.data.locale
    this.qData = question.data.qData
    this.passageId = question.data.passageId
    this.correctAnswer = question.data.correctAnswer
    this.selectedOptionList = []
    // const attemptData = {
    //   isCorrect: 0,
    //   questionPartialMarks: 0,
    //   questionNegativeMarks: 0,
    //   questionTotalMarks: 0,
    //   userNegativeMarks: 0,
    //   userPositiveMarks: 0,
    //   userTotalMarks: 0,
    //   answer: [],
    //   isAttempted: false,
    //   isVisited: false,
    //   isAnsweredMarkedForReview: false,
    //   isMarked: false
    // }

    this.attemptData = new Map<string, any>()
    this.attemptData.set('isCorrect', 0)
    this.attemptData.set('questionPartialMarks', 0)
    this.attemptData.set('questionNegativeMarks', 0)
    this.attemptData.set('questionTotalMarks', 0)
    this.attemptData.set('userNegativeMarks', 0)
    this.attemptData.set('userPositiveMarks', 0)
    this.attemptData.set('userTotalMarks', 0)
    this.attemptData.set('answer', question.data.correctAnswer || [])
    this.attemptData.set('legacyAnswer', null)
    this.attemptData.set('answerType', '')
    this.attemptData.set('isAttempted', false)
    this.attemptData.set('isVisited', false)
    this.attemptData.set('isAnsweredMarkedForReview', false)
    this.attemptData.set('isMarked', false)
    this.attemptDataTimeStats = {
      userTimeTaken: 0
    }
    this.answerPayload = {
      optionsMatrix: [],
      optionsMcq: [],
      type: question.data.questionCode,
      isMarked: false,
      isVisited: false,
      isAttempted: false,
      isAnsweredMarkedForReview: false,
      startTime: 0,
      duration: testMeta.duration, // test duration
      sectionId: 'string',
      subSectionId: 'string',
      attemptId: testMeta._id,
      testId: testMeta.testId,
      questionId: question.data.qId,
      options: [],
      timePerQuestion: '0',
      courseId: testMeta.courseId,
      attemptNo: testMeta.attemptNo,
      flag: 1
    }
  }

  getAttemptData()/*: IQuestionAttempt*/ {
    return toJS(this.attemptData)
  }

  /**
   * Returns a string denoting the marking scheme of the question.
   */
  getMarkingScheme(): string {
    const attemptData = this.getAttemptData()
    const positiveMarks = get(attemptData, 'questionTotalMarks', 0)
    const negativemarks = get(attemptData, 'questionNegativeMarks', 0)
    if (negativemarks === 0 || negativemarks === null) {
      return `Marks: +${positiveMarks}`
    } else {
      return `Marks: +${positiveMarks}, -${negativemarks}`
    }
  }

  isAttempted() {
    return this.attemptData.isAttempted
  }

  // Returns true if visited for the first time
  setVisited(): boolean {
    const isVisited = this.attemptData.isVisited
    this.attemptData.isVisited = true
    return isVisited
  }

  setMarkedForReview(isMarked: boolean = true): boolean {
    const attemptData = this.attemptData
    let previousMarkedStatus: boolean
    if (attemptData.isAttempted) {
      previousMarkedStatus = attemptData.isAnsweredMarkedForReview
      this.attemptData.isAnsweredMarkedForReview = !previousMarkedStatus
      // this.attemptData.isAnsweredMarkedForReview = isMarked
      // this.attemptData.isMarked = isMarked
    } else {
      previousMarkedStatus = attemptData.isMarked
      this.attemptData.isMarked = !previousMarkedStatus
      // this.attemptData.isMarked = isMarked
      // this.attemptData.isAnsweredMarkedForReview = isMarked
    }
    return previousMarkedStatus !== isMarked
  }

  getMarkedForReview(): boolean {
    return this.attemptData.isMarked || this.attemptData.isAnsweredMarkedForReview
  }

  /**
   * Return Question type
   */
  getQuestionType(): string {
    return this.questionType || 'SMCQ'
  }

  /**
   * Return Question Code
   */
  getQuestionCode(): string {
    return this.questionCode
  }

  /**
   * Return question options
   */
  getOptions(): any {
    const questionType = this.getQuestionType()
    if (questionType === 'Matrix') {
      return toJS(this.qData.content[0].matrixOptionContent)
    } else {
      return toJS(this.qData.content[0].optionsContent)
    }
  }

  getCorrectAnswer(): any {
    // console.warn(JSON.stringify(this.qData.content))
    return this.qData.content[0].correctAnswer
  }

  getSectionId(): string {
    return this.sectionId
  }

  getSubSectionId(): string {
    return this.subSectionId
  }

  _updateAnswerPayload () {
    // console.warn('@@@@@@@@@@@@Answer payload called!!')
    const answer = this.getAnswer().answer

    this.answerPayload.isMarked = this.attemptData.isMarked
    this.answerPayload.isVisited = this.attemptData.isVisited
    this.answerPayload.isAttempted = this.attemptData.isAttempted
    this.answerPayload.isAnsweredMarkedForReview = this.attemptData.isAnsweredMarkedForReview
    this.answerPayload.timePerQuestion = '' + (this.attemptDataTimeStats.userTimeTaken / 1000)
    this.answerPayload.sectionId = this.sectionId
    this.answerPayload.subSectionId = this.subSectionId
    this.answerPayload.questionId = this.qId

    const optionsMcq: IAnswerOption[] = cloneDeep(this.getOptions())
    const questionCode = this.questionCode + ''
    switch (questionCode) {
      case questionCodes.SMCQ:
      case questionCodes.TrueFalse:
        this.answerPayload.optionsMcq = optionsMcq.map((option) => {
          answer.forEach((item) => {
            if (option.id === item.value) {
              option.value = true
            } else {
              option.value = false
            }
          })
          return {
            ...pick(option, [
              'id',
              'value'
            ])
          }
        })
        this.answerPayload.options = answer
        break
      case questionCodes.MMCQ:
        // populate optionsMcq and options
        this.answerPayload.optionsMcq = optionsMcq.map((option) => {
          if (typeof option.value === 'string') {
            option.value = false
          }
          answer.forEach((item) => {
            if (option.id === item.value) {
              option.value = !option.value
            }
          })
          return {
            ...pick(option, [
              'id',
              'value'
            ])
          }
        })
        this.answerPayload.options = answer
        break
      case questionCodes.Integer:
      case questionCodes.Numerical:
        // leave blank
        this.answerPayload.options = answer
        break
      case questionCodes.Matrix:
        // optionsMatrix and options
        answer.forEach((item) => {
          if (item && item.value && item.value.length > 0) {
            this.answerPayload.options.push()
          }
        })
        this.answerPayload.optionsMatrix = answer
        break
      default:
        break
    }

   // console.warn('--> AnswerPayload Set: ', this.answerPayload)
    // this._updateRealmRedisData()
  }

  /**
   * MMCQ or SMCQ response.
   * @param answer string|string[]
   */
  _setMCQAnswer(answer: any) {
    // below same in tru false and mcq
    // options -> {value: "Option1"}
    // optionsMcq -> {id: "Option1", value: true}
    // MMCQ options -> [{value: "Option1"}, {value: "Option2"}]
    // MMCQ optionsMcq -> [{id: "Option1", value: true}, {id: "Option2", value: true}, {id: "Option3", value: false},…]

    this.selectedOptionList.push(answer)
    if (!answer) {
      return
    }
    const selection: ICorrectOption = {
      answerType: 'id',
      data: []
    }
    if (!Array.isArray(answer)) {
      const { id, value } = answer
      selection.data = [{ id, value }]
    } else {
      selection.data = answer
    }

    // to deselect the options
    if (this.questionCode !== questionCodes.MMCQ && this.attemptData.answer.length > 0 ) {
      this.attemptData.answer = []
    }
    // console.warn(JSON.stringify(selection, null, 2))
    this.attemptData.answerType = selection.answerType
    this.attemptData.answer = selection.data
    // console.warn('setAnswer in type:: ', JSON.stringify(this.attemptData, null, 2))
  }

  _setValueAnswer(answer: any) {
    // console.warn('SET VALUE ANSWER CALLED ', answer)
    // console.warn(answer, 'answer')
    // numerical type options--> [{value: 234234}]
    const isAnswered = answer !== null && answer !== ''
    this.answeredValue = answer
    const data = isAnswered ? [<ICorrectOptionData>{ value: answer }] : null
    const selection: ICorrectOption = {
      answerType: 'value',
      data
    }

    // console.warn('isAnswered ', isAnswered)
    // console.warn('selection ', selection)

    this.attemptData.answerType = selection.answerType
    this.attemptData.answer = <any[]>selection.data
    // console.warn('****Set Answer -> ', {
    //   'answerType': this.attemptData.answerType,
    //   'answer': toJS(this.attemptData.answer)
    // })
  }

  _setMatrixAnswer(answer: any) {
    // TODO: Gaurav
    // options and options matrix are same
    // [{id: "Left1", value: ["Right1"]}, {id: "Left2", value: ["Right2", "Right4"]},…]
    if (!answer) {
      return
    }
    const selection: ICorrectOption = {
      answerType: 'id',
      data: []
    }

    const selectionData: ICorrectOptionData[] = <ICorrectOptionData[]> selection.data
    if (!Array.isArray(answer)) {
      const { id, value } = answer
      // tslint:disable-next-line:no-shadowed-variable
      selection.data = [{ id, value }]
    } else {
      selection.data = answer
    }

    // to deselect the options
    if (this.attemptData.answer.length > 0 ) {
      this.attemptData.answer = []
    }
    this.attemptData.answerType = selection.answerType
    this.attemptData.answer = selection.data
  }

  setAnswer(answer: any) {
    let isPreviouslyAttempted = this.attemptData.isAttempted
    const questionCode = this.questionCode + ''
    switch (questionCode) {
      case questionCodes.SMCQ:
      case questionCodes.MMCQ:
      case questionCodes.TrueFalse:
        this._setMCQAnswer(answer)
        this.attemptData.isAttempted = this.attemptData.answer && this.attemptData.answer.length > 0
        break
      case questionCodes.Integer:
      case questionCodes.Numerical:
        this._setValueAnswer(answer)
        this.attemptData.isAttempted = this.attemptData.answer !== null &&
          this.attemptData.answer.length &&
          this.attemptData.answer.data !== null &&
          this.attemptData.answer.data !== ''
        break
      case questionCodes.Matrix:
        this._setMatrixAnswer(answer)
        this.attemptData.isAttempted = this.attemptData.answer && this.attemptData.answer.length > 0
        break
      default:
        break
    }
    // update marked accordingly

    if (!isPreviouslyAttempted &&
      this.attemptData.isAttempted &&
      this.attemptData.isMarked
    ) {
      this.attemptData.isMarked = false
      this.attemptData.isAnsweredMarkedForReview = true
    } else if (isPreviouslyAttempted &&
      !this.attemptData.isAttempted &&
      this.attemptData.isAnsweredMarkedForReview
    ) {
      this.attemptData.isAnsweredMarkedForReview = false
      this.attemptData.isMarked = true
    }

    // console.warn('setAnswer: ', JSON.stringify(this.attemptData, null, 2))
    // console.warn('setAnswer: ', JSON.stringify(this.attemptData, null, 2))

    this._updateAnswerPayload()
  }

  getAnswer () {
    return {
      'answerType': this.attemptData.answerType,
      'answer': toJS(this.attemptData.answer)
    }
  }

  getQid() {
    return this.qId
  }
}

export type answerType = 'id'|'value'

// tslint:disable-next-line:variable-name
export const questionTypes = {
  Descriptive: 'Descriptive',
  Integer: 'Integer',
  MMCQ: 'MMCQ',
  Matrix: 'Matrix',
  Numerical: 'Numerical',
  SMCQ: 'SMCQ',
  TrueFalse: 'True-False',
  Blanks: 'Blanks'
}

// tslint:disable-next-line:variable-name
export const questionCodes = {
  SMCQ: '0',
  Matrix: '1',
  TrueFalse: '2',
  Blanks: '3',
  Descriptive: '4',
  Numerical: '6',
  Integer: '7',
  MMCQ: '8'
}

// tslint:disable-next-line:variable-name
export const BidirectionalQuestionTypeMap = {
  [questionTypes.Descriptive]: questionCodes.Descriptive,
  [questionTypes.Integer]: questionCodes.Integer,
  [questionTypes.MMCQ]: questionCodes.MMCQ,
  [questionTypes.Matrix]: questionCodes.Matrix,
  [questionTypes.Numerical]: questionCodes.Numerical,
  [questionTypes.SMCQ]: questionCodes.SMCQ,
  [questionTypes.TrueFalse]: questionCodes.TrueFalse,
  [questionCodes.Descriptive]: questionTypes.Descriptive,
  [questionCodes.Integer]: questionTypes.Integer,
  [questionCodes.MMCQ]: questionTypes.MMCQ,
  [questionCodes.Matrix]: questionTypes.Matrix,
  [questionCodes.Numerical]: questionTypes.Numerical,
  [questionCodes.SMCQ]: questionTypes.SMCQ,
  [questionCodes.TrueFalse]: questionTypes.TrueFalse
}
