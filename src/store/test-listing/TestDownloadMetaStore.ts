import { observable, action, toJS } from 'mobx'

const DEFAULTS = {
  progress: () => '0'
}

export class TestDownloadMetaStore {
  @observable progress: string

  constructor() {
    this.init()
  }

  @action
  init() {
    this.resetProgress()
  }

  /**
   * Updates the progress. Doesn't let you set progress to a number less than 5.
   * Also, takes care of the decimal places to be fixed to 2.
   * @param progress: The progress to be set, numeric
   */
  @action
  setProgress(progress: number): void {
    if (!isNaN(progress)) {
      let stringProgress: string
      if (progress < 5) {
        progress = 5
      }
      // if (progress >= 100) {
      //   progress = 95
      // }
      if (progress % 1 > 0) {
        stringProgress = progress.toFixed(1)
      } else {
        stringProgress = progress + ''
      }
      this.progress = stringProgress
    }
  }

  @action
  resetProgress() {
    this.progress = DEFAULTS.progress()
  }
}
