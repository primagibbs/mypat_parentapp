import React from 'react';
import { View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import ExploreComponent from '../components/tabComponents/ExploreComponent';
import MoreComponent from '../components/tabComponents/MoreComponent';
import FeedComponentPage from '../components/tabComponents/FeedComponentPage';
import { NotificationListPage } from '../screens';
import AssignmentsPage from '../components/dashboard/screens/AssignmentsPage'
import { TabBarViewComponent } from '../components/tabComponents';
import  moment from 'moment';
import { getAllCountries } from 'react-native-country-picker-modal';
const REGEX_VALID_MOBILE = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
import { validateRegex } from '.';

export const getInitialRouteName = () => 'LoginPage';

export function getTabBarTitles() {

	// TabBar Selected and NonSelected Images
	const HOME_NORMAL_ICON = require('../../images/home_normal.png');
	const HOME_SELECTED_ICON = require('../../images/home_selected.png');
	const FEED_NORMAL_ICON = require('../../images/feed_normal.png');
	const FEED_SELECTED_ICON = require('../../images/feed_selected.png');
	const HISTORY_NORMAL_ICON = require('../../images/history_normal.png');
	const HISTORY_SELECTED_ICON = require('../../images/history_selected.png');
	const MORE_NORMAL_ICON = require('../../images/more_normal.png');
	const MORE_SELECTED_ICON = require('../../images/more_selected.png');
	const NOTIFICATION_SELECTED_ICON = require('../../images/notification_selected.png');
	const NOTIFICATION_NORMAL_ICON = require('../../images/notification_normal.png');

	// TabBar Titles
	const HOME_TEXT = 'Home';
	const FEED_TEXT = 'Feed';
	const HISTORY_TEXT = 'History';
	const NOTIFICATION_TEXT = 'Notifications';
	const MORE_TEXT = 'More';

	// TabBar Items
	let TAB_ITEMS;
	{
		TAB_ITEMS = [
			{
				text: HOME_TEXT,
				key: 'explore',
				iconSource: HOME_NORMAL_ICON,
				selectedIconSource: HOME_SELECTED_ICON
			},
			{
				text: FEED_TEXT,
				key: 'feed',
				iconSource: FEED_NORMAL_ICON,
				selectedIconSource: FEED_SELECTED_ICON
			},
			{
				text: HISTORY_TEXT,
				key: 'history',
				iconSource: HISTORY_NORMAL_ICON,
				selectedIconSource: HISTORY_SELECTED_ICON
			},
			{
				text: NOTIFICATION_TEXT,
				key: 'notifications',
				iconSource: NOTIFICATION_NORMAL_ICON,
				selectedIconSource: NOTIFICATION_SELECTED_ICON
			},
			{
				text: MORE_TEXT,
				key: 'more',
				iconSource: MORE_NORMAL_ICON,
				selectedIconSource: MORE_SELECTED_ICON
			}];
		return TAB_ITEMS
	}
}

export function getTabBarViews(props: any) {
	return [
		<View key='explore'>
			<TabBarViewComponent
				navigation={props.navigation}
				tabIndex={0}
				component={ExploreComponent}
				{...props}
			/>
		</View>,
		<View key='feed'>
			<TabBarViewComponent
				navigation={props.navigation}
				tabIndex={1}
				component={FeedComponentPage}
			/>
		</View>,
		<View key='history'>
			<TabBarViewComponent
				navigation={props.navigation}
				tabIndex={2}
				component={AssignmentsPage}
			/>
		</View>,
		<View key='notifications'>
			<TabBarViewComponent
				navigation={props.navigation}
				tabIndex={3}
				component={NotificationListPage}
			/>
		</View>,
		<View key='more'>
			<TabBarViewComponent
				navigation={props.navigation}
				tabIndex={4}
				component={MoreComponent}
				{...props}
			/>
		</View>
	];
}

export function capitalizeWords(value: string) {
	return value.replace(/\w\S*/g, function (txt: string) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() })
}

export function displayPercentage(value: number) {
	return `${value}%`
}

export function findObjectIndexByKey(array: any, key: number, value: any) {
	for (let i = 0; i < array.length; i++) {
		if (array[i][key] === value) {
			return i
		}
	}
	return null
}

export function getDurationWithSubtractTime(startDate: any, endDate: any, subractTime: any) {
	if (startDate && endDate) {
		const a = moment(startDate);
		const b = moment(endDate);
		const duration = moment.duration(b.diff(a));
		const seconds = duration.asSeconds();
		return seconds - subractTime
	}
	return 'NA'
}

export function getDuration(startDate: any, endDate: any) {
	if (startDate && endDate) {
		const a = moment(startDate);
		const b = moment(endDate);
		const duration = moment.duration(b.diff(a));
		return duration.asSeconds();
	}
	return 'NA'
}

export function getTargetColorCode(target: string) {
	if (target.includes('JEE(ADVANCED)') || target.includes('JEE(A)')) {
		return '#5D82E7'
	} else if (target.includes('JEE(MAIN)') || target.includes('JEE (MAIN)') || target.includes('JEE(M)')) {
		return '#FF6969'
	} else if ( target.includes('BITSAT')) {
		return '#B371E0'
	} else if ( target.includes('KVPY')) {
		return '#71A9E0'
	} else if ( target.includes('NTSE')) {
		return '#E7A260'
	} else {
		return '#71A9E0'
	}
}

export function displayData(text: any) {
	if (isNaN(text)) {
		return text
	}
	const num = parseFloat(text);
	if (num % 1 < 1) {
		return num.toFixed(2)
	}
	return num
}

export function calculateCircleHeight(data: any, size: number) {
	const val1 = size / 100;
	return val1 * data;
}

export function isPhoneNumber(userInfo: any) {
	if (validateRegex(userInfo, REGEX_VALID_MOBILE)) {
		return  true
	} else if (userInfo.length === 0) {
		return false
	}
	return false
}

export function getDeviceCountryCode() {
	const NORTH_AMERICA = ['CA', 'MX', 'US'];
	const userLocaleCountryCode = DeviceInfo.getDeviceCountry();
	console.log("userLocaleCountryCode", userLocaleCountryCode);
	const userCountryData = getAllCountries()
		.filter(country => NORTH_AMERICA.indexOf(country.cca2))
		.filter(country => country.cca2 === userLocaleCountryCode)
		.pop();

	let callingCode = null;
	let cca2 = userLocaleCountryCode;
	if (!cca2 || !userCountryData) {
		cca2 = 'US';
		callingCode = '1'
	} else {
		callingCode = userCountryData.callingCode
	}
	return callingCode
}
