import { AsyncStorage } from 'react-native'
import { get } from 'lodash'
import _ from 'lodash'
import transport from '../common-library/events'
import stores from '../store'
import {
	FROM_PAGE_TYPE, USER_DETAILS_TYPE, USER_LOGIN_TYPE,
	BASE_URL, EXTRA_EDGE_BASE_URL, COUNTRY_CODE
} from '../common'
import { findObjectIndexByKey } from './app-utils'

export const USER_KEY = 'user-key';
export const PROFILE_KEY = 'profile-key';
export const AUTH_TOKEN_KEY = 'auth-token_key';
export const DEVICE_AUTO_LOGIN = 'fiitjee-login';
export const DEVICE_TOKEN_KEY = 'device-token_key';
export const PACKAGE_KEY = 'package-key';
export const ALL_GOALS_KEY = 'all_goals_key';
export const MOBILE_DETAILS_KEY = 'mob_details_key';
export const EMAIL_DETAILS_KEY = 'email_details_key';
export const EXAM_KEY = 'exam-key';
export const WALK_THROUGH_COMP_KEY = 'walk-through-key';
export const CART_PRODUCT_COUNT_KEY = 'cart-product-count-key';
export const GOAL_KEY = 'goal-key';
export const FIITJEE_KEY = 'fiitjee-key';
export const PASSWORD_VERIFY = 'password-key';
export const LOGIN_TYPE_KEY = 'login-type-key';
export const SELECTED_GOAL_KEY = 'selected_goal-key';
export const SELECTED_GOAL_NAME = 'selected_goal-name';
export const SELECTED_GOAL = 'selected-goal';
export const NOTIFICATION_KEY = 'noftification-key';
export const UPDATE_RECOMMENDED = 'updateRecommended';
export const NCRP_KEY = 'ncrp-key';
export const NEW_USER_GOALS = 'user-goals';
export const USER_NAME_KEY = 'user_name_key';
export const ENROLMENT_NO = 'enrolment_no';
export const SHOW_TAB_TYPE = 'show_tab';
export const ORIGIN_FIITJEE = 'origin_fiitjee';
export const PARTNER_GOALS_DATA = 'partner_goals_data';
export const FEILD_TYPE_KEY = 'profile_feild_key';
export const COUNTRY_CODE_KEY = 'country_code_key';
export const MATHJAX_DOWNLOADED = 'mathJax_downloaded';
export const SAVED_USER_ID = 'save_user_id';
export const APP_TOUR = 'app-tour';

const INFO_KEYS = [USER_KEY, PROFILE_KEY, AUTH_TOKEN_KEY, PACKAGE_KEY,
	ALL_GOALS_KEY, MOBILE_DETAILS_KEY, EMAIL_DETAILS_KEY, EXAM_KEY, LOGIN_TYPE_KEY,
	CART_PRODUCT_COUNT_KEY, GOAL_KEY, FIITJEE_KEY, PASSWORD_VERIFY, SELECTED_GOAL_KEY,
	SELECTED_GOAL_NAME, NOTIFICATION_KEY, UPDATE_RECOMMENDED, NCRP_KEY, FEILD_TYPE_KEY,
	NEW_USER_GOALS, USER_NAME_KEY, ENROLMENT_NO, SHOW_TAB_TYPE, ORIGIN_FIITJEE, PARTNER_GOALS_DATA, SELECTED_GOAL,
	DEVICE_AUTO_LOGIN, COUNTRY_CODE_KEY];

interface LoggedInUser {
	name?: string
	photoUrl?: string
	targetExam?: string
	profile?: any
	school?: any
	location?: any
	class?: any
	partner?: any
	userId?: string
	enrollmentId?: string
}

export interface MobileDetails {
	mobile?: string
	userId?: any,
	isMobileVerified?: boolean,
	countryCode?: string
}

export interface EmailDetails {
	email?: string
}

interface ExamData {
	class?: any
	goals?: any[]
}

export const getEmailVerified = async () => {
	const value = await AsyncStorage.getItem(EMAIL_DETAILS_KEY);
	if (value) {
		let emailObj = await JSON.parse(value) || false;
		return emailObj.isEmailVerified
	}
	return undefined
};

export const getUserName = async () => {
	const value = await AsyncStorage.getItem(USER_NAME_KEY)
	if (value) {
		let emailObj = await JSON.parse(value) || false
		return emailObj.UserName.name
	}
	return undefined
};

export const getMathJaxDownloaded = async () => {
	const value = await AsyncStorage.getItem(MATHJAX_DOWNLOADED);
	return !!value;
};

export const setMathjaxDownloaded =  async () => {
	await AsyncStorage.setItem(MATHJAX_DOWNLOADED, 'true')
};

export const storePasswordDetail = async (pswrdObj: string) => {
	if (pswrdObj && pswrdObj.length) {
		await AsyncStorage.setItem(PASSWORD_VERIFY, pswrdObj)
	}
};

export const storeUserName = async (userNameObj: string) => {
	await AsyncStorage.setItem(USER_NAME_KEY, userNameObj)
};

export const storeEmailDetails = async (emailObj: string) => {
	await AsyncStorage.setItem(EMAIL_DETAILS_KEY, emailObj)
};

export const storeMobileDetails = async (mobileObj: string) => {
	await AsyncStorage.setItem(MOBILE_DETAILS_KEY, mobileObj)
};

export const updateMobileDetails = async (userData: any = {}, mobile: any, userId: any, countryCode: string) => {
	let mobileDetails: MobileDetails;
	const { data } = userData;
	if (data && data.mobile) {
		mobileDetails = {
			mobile: data.mobile.number,
			userId: data.userId,
			countryCode: data.mobile.countryCode
		}
	} else {
		mobileDetails = {
			mobile: mobile,
			userId: userId,
			countryCode: countryCode
		};
		console.log(mobileDetails);
	}
	await storeMobileDetails(JSON.stringify(mobileDetails))
};

export const updateEmailDetails = async (userData: any = {}, email, isVerified) => {
//  console.warn('updateEmailDetails', userData, email, isVerified)
	let emailDetails: EmailDetails
	const { data } = userData
	if (data && data.email) {
		emailDetails = {
			email: data.email && data.email.id,
			isEmailVerified: data.email && data.email.isVerified
		}
	} else {
		emailDetails = {
			email: email,
			isEmailVerified: isVerified
		}
	}
	await storeEmailDetails(JSON.stringify(emailDetails))
};

export const getCountryCode = async () => {
	const value = await AsyncStorage.getItem(MOBILE_DETAILS_KEY)
	if (value) {
		let mobObj = await JSON.parse(value) || {}
		return mobObj.countryCode
	}
	return undefined
};

export const getMobileVerified = async () => {
	const value = await AsyncStorage.getItem(MOBILE_DETAILS_KEY);
	if (value) {
		let mobObj = await JSON.parse(value) || {}
		return mobObj.isMobileVerified
	}
	return undefined
};

// Key in Goals -{courseId,name,targetyear,rank,_id}
export const getGoalsData = async () => {
	const value = await AsyncStorage.getItem(EXAM_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.goals
	}
	return undefined
};

export const getAllGoalsData = async () => {
	const value = await AsyncStorage.getItem(ALL_GOALS_KEY)
	if (value) {
		return await JSON.parse(value) || {};
	}
	return undefined
};

export const setAllGoals = async (data: any) => {
	if (stores && stores.goalsDataStore && data.length) {
		await stores.goalsDataStore.setFinalGoals(data)
	}
	await AsyncStorage.setItem(ALL_GOALS_KEY, JSON.stringify(data))
};

export const getPartnersGoals = async () => {
	const value = await AsyncStorage.getItem(PARTNER_GOALS_DATA)
	if (value) {
		let partnerGoals = await JSON.parse(value) || {}
		return partnerGoals
	}
	return undefined
};

// Key in Goals -{courseId,name,targetyear,rank,_id}
export const getGoalNames = async () => {
	const value = await AsyncStorage.getItem(EXAM_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		let goalNames
		if (userObject.goals && userObject.goals.length) {
			goalNames = userObject.goals.map(goal => goal.name)
		}
		return goalNames
	}
	return undefined
}

export const getFirstGoalID = async () => {
	const value = await AsyncStorage.getItem(EXAM_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		if (userObject && userObject.goals && userObject.goals.length) {
			if (userObject.goals[0].courseType) {
				return userObject.goals[0].courseType.id
			}
		}
	}
	return undefined
}

export const getFirstCourseId = async () => {
	const value = await AsyncStorage.getItem(EXAM_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		if (userObject && userObject.goals && userObject.goals.length) {
			return userObject.goals[0].courseId

		}
	}
	return undefined
}

export const getFirstCourseName = async () => {
	const value = await AsyncStorage.getItem(EXAM_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		if (userObject && userObject.goals && userObject.goals.length) {
			return userObject.goals[0].name
		}
	}
	return undefined
}

export const getPackages = async () => {
	const value = await AsyncStorage.getItem(PACKAGE_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject
	}
	return undefined
};

export const getDisabledPackages = async () => {
	const value = await AsyncStorage.getItem(PACKAGE_KEY)
	let goalArrValues = []
	let index = 0
	if (value) {
		let userObject = await JSON.parse(value) || {}
		let packageGoalsArray = userObject.map(item => item.goalsArr)
		for (let userGoalsArr of packageGoalsArray) {
			for (let userGoal of userGoalsArr) {
				goalArrValues[index++] = userGoal
			}
		}
		let goalsAfterUnique = _.uniqBy(goalArrValues, '_id')
		let goalindex = findObjectIndexByKey(goalsAfterUnique, 'name', 'AILCT')
		if (goalindex !== null) {
			goalsAfterUnique.splice(goalindex, 1)
		}
		//   console.warn('goalsAfterUnique', goalsAfterUnique)
		return goalsAfterUnique
	}
	return undefined
}

export const getBaseUrl = () => BASE_URL
export const getExtraEdgeBaseUrl = () => EXTRA_EDGE_BASE_URL

export const getGoalsArrFromPackage = async () => {
	const value = await AsyncStorage.getItem(PACKAGE_KEY)
	let goalArrValues = []
	let index = 0
	if (value) {
		let userObject = await JSON.parse(value) || {}
		let packageGoalsArray = userObject.map(item => item.goalsArr)
		for (let userGoalsArr of packageGoalsArray) {
			for (let userGoal of userGoalsArr) {
				goalArrValues[index++] = userGoal
			}
		}
		const goalsArr = await getGoalsData()
		let packageGoals = goalArrValues.map((item) => ({
			...item,
			courseId: item._id
		}))
		let goalsAfterUnion = _.unionBy(goalsArr, packageGoals, 'courseId')
		return goalsAfterUnion
	}
	return undefined
}

export const setNewUserGoalsData = async (value: any) => {
	await AsyncStorage.setItem(NEW_USER_GOALS, JSON.stringify(value))
};

export const getNewUserGoalsData = async () => {
	const value = await AsyncStorage.getItem(NEW_USER_GOALS);
	if (value && value.length) {
		return await JSON.parse(value) || {};
	}
	return undefined
};

export const getFirstPackageGoalId = async () => {
	const value = await AsyncStorage.getItem(PACKAGE_KEY)
	if (value && value.length) {
		let packageObject = await JSON.parse(value) || {}
		return packageObject[0].goalId
	}
	return undefined
}

export const getEnrolmentNumber = async () => {
	return await AsyncStorage.getItem(ENROLMENT_NO);
};

export const setEnrolmentNumber = async (enrolmentNo) => {
	if (enrolmentNo && enrolmentNo.length) {
		await AsyncStorage.setItem(ENROLMENT_NO, enrolmentNo)
	}
};

export const getAuthToken = async () => {
	return await AsyncStorage.getItem(AUTH_TOKEN_KEY);
};

export const setAuthToken = async (authToken: any) => {
	console.log(authToken);
	await AsyncStorage.setItem(AUTH_TOKEN_KEY, authToken)
};

export const setIsDeviceLogin = async () => {
	AsyncStorage.setItem(DEVICE_AUTO_LOGIN, 'true')
};

export const getNotifcationData = async () => {
	return await AsyncStorage.getItem(NOTIFICATION_KEY);
};

export const setNotifcationData = async (notification: any) => {
	await AsyncStorage.setItem(NOTIFICATION_KEY, notification)
};

export const getDeviceToken = async () => {
	return await AsyncStorage.getItem(DEVICE_TOKEN_KEY);
};

export const setDeviceToken = async (deviceToken: any) => {
	await AsyncStorage.setItem(DEVICE_TOKEN_KEY, deviceToken)
};

export const getName = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.name
	}
	return undefined
};

/*
 * Update method to fetch student id
 * Remove hard coded value
 */
export const getUserId = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.profile.id
	}
	return undefined;
};

export const getStudents = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.profile.studentProfiles;
	}
	return undefined;
};

export const getSelectedStudent = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		let selectStudent;
		userObject.profile.studentProfiles.forEach((student: any) => {
			if (student.isSelected) {
				selectStudent = student;
			}
		});
		if (selectStudent) {
			return selectStudent;
		} else {
			return userObject.profile.studentProfiles[0];
		}
	}
	return undefined;
};

export const setSelectedStudent = async (studentId: any) => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		userObject.profile.studentProfiles = userObject.profile.studentProfiles.map((student: any, index: number) => {
			if (studentId) {
				student.isSelected = student.id == studentId;
			} else {
				if (index === 0) {
					student.isSelected = true;
				}
			}
			return student;
		});
		await AsyncStorage.setItem(USER_KEY, JSON.stringify(userObject))
	}
};

export const getMobileObj = async () => {
	const value = await AsyncStorage.getItem(MOBILE_DETAILS_KEY);
	if (value) {
		return await JSON.parse(value) || {};
	}
	return undefined
};

export const getMobileNumber = async () => {
	const value = await AsyncStorage.getItem(MOBILE_DETAILS_KEY);
	if (value) {
		let mobObj = await JSON.parse(value) || {};
		return mobObj.mobile
	}
	return undefined
};

export const getEmailId = async () => {
	const value = await AsyncStorage.getItem(EMAIL_DETAILS_KEY);
	if (value) {
		let emailObj = await JSON.parse(value) || {};
		return emailObj.email
	}
	return undefined
};

export const getTargetExam = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.targetExam
	}
	return undefined
};

export const getProfileImageURL = async () => {
	const value = await AsyncStorage.getItem(USER_KEY);
	if (value) {
		let userObject = await JSON.parse(value) || {};
		return userObject.photoUrl
	}
	return undefined
}

export const hasCompleteGoal = async () => {
	const value = await AsyncStorage.getItem(GOAL_KEY);
	return value || false
};

export const setCompleteGoal = async () => {
	await AsyncStorage.setItem(GOAL_KEY, 'true')
};

export const setSelectedGoal = async (goal: any) => {
	try{
		await AsyncStorage.setItem(SELECTED_GOAL, JSON.stringify(goal))
	}catch(err){
		console.error("Error while parsing selected goal", err)
	}
};

export const setDropDownGoalId = async (id) => {
	await AsyncStorage.setItem(SELECTED_GOAL_KEY, id)
};

export const getSelectedGoal = async () => {
	try{
		const value = await AsyncStorage.getItem(SELECTED_GOAL);
		return await JSON.parse(value) || undefined
	}catch(err){
		console.error("Error while parsing selected goal", err)
	}
};

export const getDropDownGoalId = async () => {
	const value = await AsyncStorage.getItem(SELECTED_GOAL_KEY);
	return value || undefined
}

export const setDropDownGoalName = (name) => {
	AsyncStorage.setItem(SELECTED_GOAL_NAME, name)
}

export const getDropDownGoalName = async () => {
	const value = await AsyncStorage.getItem(SELECTED_GOAL_NAME)
	return value || undefined
};

export const setPasswordVerified = (value) => {
	if (value === 1) {
		AsyncStorage.setItem(PASSWORD_VERIFY, JSON.stringify(value))
	}
};

export const isNcrptype = async () => {
	return await AsyncStorage.getItem(NCRP_KEY)
};

export const setNcrp = () => {
	AsyncStorage.setItem(NCRP_KEY, 'true')
};

export const getShowTabType = async () => {
	return await AsyncStorage.getItem(SHOW_TAB_TYPE);
};

export const setTabType = async (key: any) => {
	await AsyncStorage.setItem(SHOW_TAB_TYPE, key)
};

export const hasCompleteWalkThroughPage = async () => {
	const value = await AsyncStorage.getItem(WALK_THROUGH_COMP_KEY);
	return value || false
};

export const setCompleteWalkThroughPage = async () => {
	await AsyncStorage.setItem(WALK_THROUGH_COMP_KEY, 'true')
};

export const hasCompleteAppTour = async () => {
	const value = await AsyncStorage.getItem(APP_TOUR);
	let user = await AsyncStorage.getItem(USER_KEY);
	user = JSON.parse(user);
	console.log(user);
	const firstTimeLogin = user.firstTimeLogin;
	if (firstTimeLogin) {
		return false
	} else {
		return value || false
	}
};

export const setAppTourComplete = async () => {
	await AsyncStorage.setItem(APP_TOUR, 'true')
};

export const setUserClass = async (classObject: any) => {
	let userInfo: any = await AsyncStorage.getItem(USER_KEY);
	if (userInfo) {
		userInfo = JSON.parse(userInfo);
		if (userInfo) {
			userInfo = {
				...userInfo,
				class: classObject
			};
			await AsyncStorage.setItem(USER_KEY, JSON.stringify(userInfo))
		}
	}
};

export const handleSignIn = async (userData: any, userDetailsType: any, shouldHandleGoals: boolean = true) => {
	console.log(userData);
	await updateUserLoginType(userDetailsType);
	let mobileDetails: MobileDetails, emailDetails: EmailDetails, authToken = '';
	if (userData && userData.data) {
		const { data } = userData;
		switch (userDetailsType) {
			case USER_DETAILS_TYPE.LOGIN:
			case USER_DETAILS_TYPE.SIGNUP:
				mobileDetails = {
					mobile: data.profile.mobile,
					userId: data.profile.id,
					isMobileVerified: data.profile.isVerified,
					countryCode: data.profile.countryCode
				};
				emailDetails = {
					email: data.profile.email
				};
				if (data.token) {
					await setAuthToken(data.token);
				}
				break;
			case USER_DETAILS_TYPE.DEFAULT:
				if (data.profile.id) {
					mobileDetails = {
						mobile: data.profile.mobile,
						isMobileVerified: data.profile.isVerified,
						countryCode: data.profile.mobile && data.profile.countryCode
					};
					emailDetails = {
						email: data.profile.email
					};
				}
				break;
			default:
				break
		}

		const userDetails = await getUserDetails();
		// @ts-ignore
		if (userDetails === null || userDetails && userDetails.profile && userDetails.profile.studentProfiles.length !== data.profile.studentProfiles.length) {
			await AsyncStorage.setItem(USER_KEY, JSON.stringify(data));
			await storeMobileDetails(JSON.stringify(mobileDetails));
			await storeEmailDetails(JSON.stringify(emailDetails));
		}
	}
};

export const setUserId = async (userId: string) => {
	await AsyncStorage.setItem(SAVED_USER_ID, userId)
};

export const getUserLastId = async () => {
	return await AsyncStorage.getItem(SAVED_USER_ID);
};

export const getFeildTypeKey = async () => {
	const key = await AsyncStorage.getItem(FEILD_TYPE_KEY)
	return key
}
export const setCompleteProfileVerifiedFeild = (name, password, isMobileVerified, isEmailVerified, enrollmentNo) => {
	if (name === enrollmentNo || name === '') {
		AsyncStorage.setItem(FEILD_TYPE_KEY, 'name')
	} else if (password !== 1) {
		AsyncStorage.setItem(FEILD_TYPE_KEY, 'password')
	} else if (!isMobileVerified) {
		AsyncStorage.setItem(FEILD_TYPE_KEY, 'mobile')
	} else if (!isEmailVerified) {
		AsyncStorage.setItem(FEILD_TYPE_KEY, 'email')
	}
};

const updateUserLoginType = async (loginType: any) => {
	switch (loginType) {
		case USER_DETAILS_TYPE.SOCIAL_LOGIN:
			await AsyncStorage.setItem(LOGIN_TYPE_KEY, USER_LOGIN_TYPE.SOCIAL);
			break;
		case USER_DETAILS_TYPE.LOGIN:
		case USER_DETAILS_TYPE.SIGNUP:
			await AsyncStorage.setItem(LOGIN_TYPE_KEY, USER_LOGIN_TYPE.DEFAULT);
			break;
		default:
			break
	}
};

export const getUserLoginType = async () => {
	return await AsyncStorage.getItem(LOGIN_TYPE_KEY);
};

// @ts-ignore
export const setGoalsData = async (screenType, userData) => {
	let examData: ExamData;
	if (userData) {
		const { data } = userData;
		switch (screenType) {
			case FROM_PAGE_TYPE.GOALS_PAGE:
				if (data.userClass && data.userGoals && data.userGoals.length) {
					examData = {
						class: data.userClass,
						goals: data.userGoals
					}
				}
				break;
			case FROM_PAGE_TYPE.DEFAULT:
				if (data.userClass && data.goals && data.goals.length) {
					await setCompleteGoal();
					examData = {
						class: data.userClass,
						goals: data.goals.reverse()
					}
				}
				break;
			case FROM_PAGE_TYPE.LOGIN_PAGE:
				if (data.profile && data.profile.class && data.profile.goals && data.profile.goals.length) {
					await setCompleteGoal();
					const { profile } = data;
					examData = {
						class: profile.class,
						goals: profile.goals
					}
				}
				break;
			default:
				break
		}
		if (examData && examData.class && examData.goals && examData.goals.length) {
			//   console.warn(examData.goals)
			let goalindex = findObjectIndexByKey(examData.goals, 'name', 'AILCT')
			//  console.warn('goalindex', goalindex)
			if (goalindex !== null) {
				//   console.warn('Index not null')
				examData.goals.splice(goalindex, 1)
			}
			AsyncStorage.setItem(EXAM_KEY, JSON.stringify(examData))
		}
	}
}

// Key in Goals -{courseId,name,targetyear,rank,_id}
export const getExamName = async () => {
	const value = await AsyncStorage.getItem(ALL_GOALS_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		return get(userObject, '[0].name', undefined)
	}
	return undefined
};

export const getUserDetails = async () => {
	return await AsyncStorage.getItem(USER_KEY);
};

export const getClassId = async () => {
	let value = await AsyncStorage.getItem(USER_KEY);
	let userObject: any;
	if (value) {
		userObject = await JSON.parse(value) || {}
	} else {
		value = await AsyncStorage.getItem(EXAM_KEY);
		userObject = await JSON.parse(value) || {}
	}
	return userObject.class && userObject.class.classId
};

export const getClassName = async () => {
	let value = await AsyncStorage.getItem(USER_KEY)
	let userObject: any
	if (value) {
		userObject = await JSON.parse(value) || {}
	} else {
		value = await AsyncStorage.getItem(EXAM_KEY)
		userObject = await JSON.parse(value) || {}
	}
	return userObject.class && userObject.class.name
}

export const getLoggedInUser = async () => {
	return AsyncStorage.getItem(USER_KEY)
}

export const getSchool = async () => {
	const value = await AsyncStorage.getItem(USER_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		return userObject.school
	}
	return undefined
}

export const getLocation = async () => {
	const value = await AsyncStorage.getItem(USER_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		return userObject.location
	}
	return undefined
}

export const getPartner = async () => {
	const value = await AsyncStorage.getItem(USER_KEY)
	if (value) {
		let userObject = await JSON.parse(value) || {}
		return userObject.partner
	}
	return undefined
};

export const getClass = async () => {
	let value: any = await AsyncStorage.getItem(EXAM_KEY);
	let userObject: any = {}
	if (value) {
		userObject = await JSON.parse(value) || {}
	} else {
		value = await AsyncStorage.getItem(USER_KEY)
		userObject = await JSON.parse(value) || {}
	}
	return userObject.class
};

export const handleSignOut = () => {
	INFO_KEYS.forEach(key => AsyncStorage.removeItem(key))
};

export const isSignedIn = async () => {
	const user = await getLoggedInUser();
	return !!user;
};

export const getTargetExams = async () => {
	return null
};

export const getSelectedTargetExam = async () => {
	return null
};

export const setSelectedTargetExam = async (selection) => {
	return null
};

export const getProfileData = async () => {
	const promises = [
		getName(),
		getEmailId(),
		getMobileNumber(),
		getLocation(),
		getClass(),
		getSchool(),
		getPartner(),
		getEmailVerified(),
		getMobileVerified(),
		getCountryCode()
	];
	const responses = await Promise.all(promises);
	const [name, email, mobile, userLocation, userClass, school,
		partner, isEmailVerified, isMobileVerified, countryCode] = responses;
	return {
		name,
		email,
		mobile,
		userLocation,
		userClass,
		school,
		partner,
		isEmailVerified,
		isMobileVerified,
		countryCode
	}
};
