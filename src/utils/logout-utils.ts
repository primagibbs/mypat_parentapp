import { forOwn } from 'lodash'

import stores from '../store'
import commonStores from '../common-library/store'

export const cleanStores = () => {
  if (stores) {
    forOwn(stores, store => {
      if (store.__proto__ && !!store.__proto__.init) {
        store.init()
      }
    })
  }
  if (commonStores) {
    forOwn(commonStores, store => {
      if (store.__proto__ && !!store.__proto__.init) {
        store.init()
      }
    })
  }
};
